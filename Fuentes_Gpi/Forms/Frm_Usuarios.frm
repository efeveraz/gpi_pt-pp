VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "c1sizer.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Usuarios 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Usuarios"
   ClientHeight    =   4215
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6255
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4215
   ScaleWidth      =   6255
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6255
      _ExtentX        =   11033
      _ExtentY        =   635
      ButtonWidth     =   1879
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin C1SizerLibCtl.C1Tab SSTab 
      Height          =   4700
      Left            =   60
      TabIndex        =   3
      Top             =   420
      Width           =   6135
      _cx             =   10821
      _cy             =   8290
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   6579300
      ForeColor       =   -2147483634
      FrontTabColor   =   -2147483635
      BackTabColor    =   6579300
      TabOutlineColor =   -2147483632
      FrontTabForeColor=   -2147483634
      Caption         =   "&General|&Roles"
      Align           =   0
      CurrTab         =   0
      FirstTab        =   0
      Style           =   6
      Position        =   0
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   0   'False
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   -1  'True
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   0
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   -1  'True
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   4350
         Left            =   15
         ScaleHeight     =   4350
         ScaleWidth      =   6105
         TabIndex        =   5
         Top             =   330
         Width           =   6105
         Begin VB.Frame Frame_Natural 
            Height          =   3225
            Left            =   90
            TabIndex        =   6
            Top             =   0
            Width           =   6135
            Begin VB.Frame Frame3 
               Height          =   1155
               Left            =   150
               TabIndex        =   13
               Top             =   1800
               Width           =   5625
               Begin hControl2.hTextLabel Txt_Email 
                  Height          =   315
                  Left            =   150
                  TabIndex        =   14
                  Tag             =   "OBLI"
                  Top             =   240
                  Width           =   5340
                  _ExtentX        =   9419
                  _ExtentY        =   556
                  LabelWidth      =   1300
                  Caption         =   "Email"
                  Text            =   ""
                  BackColorTxt    =   12648384
                  BackColorTxt    =   12648384
                  MaxLength       =   99
               End
               Begin MSComCtl2.DTPicker dtp_FechaVigencia 
                  Height          =   315
                  Left            =   1470
                  TabIndex        =   16
                  Tag             =   "OBLI"
                  Top             =   700
                  Width           =   1695
                  _ExtentX        =   2990
                  _ExtentY        =   556
                  _Version        =   393216
                  CalendarBackColor=   16777215
                  CalendarTitleBackColor=   14737632
                  Format          =   67174401
                  CurrentDate     =   38876
               End
               Begin VB.Label Label2 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Fecha Vigencia"
                  Height          =   345
                  Left            =   150
                  TabIndex        =   15
                  Top             =   700
                  Width           =   1305
               End
            End
            Begin VB.Frame Frame2 
               Height          =   1155
               Left            =   150
               TabIndex        =   7
               Top             =   570
               Width           =   5625
               Begin TrueDBList80.TDBCombo Cmb_Asesor 
                  Height          =   345
                  Left            =   1470
                  TabIndex        =   8
                  Top             =   240
                  Width           =   4005
                  _ExtentX        =   7064
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   -1  'True
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Usuarios.frx":0000
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&,.bold=0,.fontsize=825"
                  _StyleDefs(7)   =   ":id=1,.italic=0,.underline=0,.strikethrough=0,.charset=0"
                  _StyleDefs(8)   =   ":id=1,.fontname=Arial"
                  _StyleDefs(9)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(10)  =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(11)  =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(12)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(13)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(14)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(15)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(16)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(17)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(18)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(19)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(20)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(21)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(22)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(23)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(24)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(25)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(26)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(27)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(28)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(29)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(30)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(31)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(34)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(35)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(38)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(39)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(40)  =   "Named:id=33:Normal"
                  _StyleDefs(41)  =   ":id=33,.parent=0"
                  _StyleDefs(42)  =   "Named:id=34:Heading"
                  _StyleDefs(43)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(44)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(45)  =   "Named:id=35:Footing"
                  _StyleDefs(46)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(47)  =   "Named:id=36:Selected"
                  _StyleDefs(48)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(49)  =   "Named:id=37:Caption"
                  _StyleDefs(50)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(51)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(52)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(53)  =   "Named:id=39:EvenRow"
                  _StyleDefs(54)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(55)  =   "Named:id=40:OddRow"
                  _StyleDefs(56)  =   ":id=40,.parent=33"
                  _StyleDefs(57)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(58)  =   ":id=41,.parent=34"
                  _StyleDefs(59)  =   "Named:id=42:FilterBar"
                  _StyleDefs(60)  =   ":id=42,.parent=33"
               End
               Begin TrueDBList80.TDBCombo Cmb_Estado 
                  Height          =   345
                  Left            =   1470
                  TabIndex        =   9
                  Tag             =   "OBLI=S;CAPTION=Estado"
                  Top             =   660
                  Width           =   2145
                  _ExtentX        =   3784
                  _ExtentY        =   609
                  _LayoutType     =   0
                  _RowHeight      =   -2147483647
                  _WasPersistedAsPixels=   0
                  _DropdownWidth  =   0
                  _EDITHEIGHT     =   609
                  _GAPHEIGHT      =   53
                  Columns(0)._VlistStyle=   0
                  Columns(0)._MaxComboItems=   5
                  Columns(0).DataField=   "ub_grid1"
                  Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns(1)._VlistStyle=   0
                  Columns(1)._MaxComboItems=   5
                  Columns(1).DataField=   "ub_grid2"
                  Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
                  Columns.Count   =   2
                  Splits(0)._UserFlags=   0
                  Splits(0).ExtendRightColumn=   -1  'True
                  Splits(0).AllowRowSizing=   0   'False
                  Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
                  Splits(0)._ColumnProps(0)=   "Columns.Count=2"
                  Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
                  Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
                  Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
                  Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
                  Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
                  Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
                  Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
                  Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
                  Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
                  Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
                  Splits.Count    =   1
                  Appearance      =   3
                  BorderStyle     =   1
                  ComboStyle      =   0
                  AutoCompletion  =   -1  'True
                  LimitToList     =   0   'False
                  ColumnHeaders   =   0   'False
                  ColumnFooters   =   0   'False
                  DataMode        =   5
                  DefColWidth     =   0
                  Enabled         =   -1  'True
                  HeadLines       =   1
                  FootLines       =   1
                  RowDividerStyle =   0
                  Caption         =   ""
                  EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
                  LayoutName      =   ""
                  LayoutFileName  =   ""
                  MultipleLines   =   0
                  EmptyRows       =   0   'False
                  CellTips        =   0
                  AutoSize        =   0   'False
                  ListField       =   ""
                  BoundColumn     =   ""
                  IntegralHeight  =   0   'False
                  CellTipsWidth   =   0
                  CellTipsDelay   =   1000
                  AutoDropdown    =   -1  'True
                  RowTracking     =   -1  'True
                  RightToLeft     =   0   'False
                  MouseIcon       =   0
                  MouseIcon.vt    =   3
                  MousePointer    =   0
                  MatchEntryTimeout=   2000
                  OLEDragMode     =   0
                  OLEDropMode     =   0
                  AnimateWindow   =   3
                  AnimateWindowDirection=   5
                  AnimateWindowTime=   200
                  AnimateWindowClose=   1
                  DropdownPosition=   0
                  Locked          =   0   'False
                  ScrollTrack     =   -1  'True
                  ScrollTips      =   -1  'True
                  RowDividerColor =   14215660
                  RowSubDividerColor=   14215660
                  MaxComboItems   =   10
                  AddItemSeparator=   ";"
                  _PropDict       =   $"Frm_Usuarios.frx":00AA
                  _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
                  _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
                  _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
                  _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
                  _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
                  _StyleDefs(5)   =   ":id=0,.fontname=Arial"
                  _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
                  _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
                  _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
                  _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
                  _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
                  _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
                  _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
                  _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
                  _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
                  _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
                  _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
                  _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
                  _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
                  _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
                  _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
                  _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
                  _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
                  _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
                  _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
                  _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
                  _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
                  _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
                  _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
                  _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
                  _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
                  _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
                  _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
                  _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
                  _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
                  _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
                  _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
                  _StyleDefs(38)  =   "Named:id=33:Normal"
                  _StyleDefs(39)  =   ":id=33,.parent=0"
                  _StyleDefs(40)  =   "Named:id=34:Heading"
                  _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(42)  =   ":id=34,.wraptext=-1"
                  _StyleDefs(43)  =   "Named:id=35:Footing"
                  _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
                  _StyleDefs(45)  =   "Named:id=36:Selected"
                  _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(47)  =   "Named:id=37:Caption"
                  _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
                  _StyleDefs(49)  =   "Named:id=38:HighlightRow"
                  _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
                  _StyleDefs(51)  =   "Named:id=39:EvenRow"
                  _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
                  _StyleDefs(53)  =   "Named:id=40:OddRow"
                  _StyleDefs(54)  =   ":id=40,.parent=33"
                  _StyleDefs(55)  =   "Named:id=41:RecordSelector"
                  _StyleDefs(56)  =   ":id=41,.parent=34"
                  _StyleDefs(57)  =   "Named:id=42:FilterBar"
                  _StyleDefs(58)  =   ":id=42,.parent=33"
               End
               Begin VB.Label Lbl_Asesor 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Asesor"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   345
                  Left            =   150
                  TabIndex        =   11
                  Top             =   240
                  Width           =   1305
               End
               Begin VB.Label Label1 
                  BorderStyle     =   1  'Fixed Single
                  Caption         =   "Estado"
                  Height          =   345
                  Left            =   150
                  TabIndex        =   10
                  Top             =   660
                  Width           =   1305
               End
            End
            Begin hControl2.hTextLabel Txt_Nombres 
               Height          =   315
               Left            =   300
               TabIndex        =   12
               Tag             =   "OBLI"
               Top             =   240
               Width           =   5340
               _ExtentX        =   9419
               _ExtentY        =   556
               LabelWidth      =   1300
               Caption         =   "Nombre"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               MaxLength       =   99
            End
         End
      End
      Begin VB.PictureBox Picture4 
         BorderStyle     =   0  'None
         Height          =   4350
         Left            =   6750
         ScaleHeight     =   4350
         ScaleWidth      =   6105
         TabIndex        =   4
         Top             =   330
         Width           =   6105
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Roles 
            Height          =   3200
            Left            =   120
            TabIndex        =   2
            Top             =   120
            Width           =   5865
            _cx             =   10345
            _cy             =   5644
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   3
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Usuarios.frx":0154
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
   End
End
Attribute VB_Name = "Frm_Usuarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'=========================================================================================================
'====  Formulario         : Frm_Roles
'====  Descripci�n        : Este formulario permite Agregar, Modificar y Consultar los
'====                       permisos asociados a los distintos roles.
'====  Desarrollado por   : Hans Muller R.
'====  Fecha Creaci�n     : 24/07/2006
'====  Fecha Modificaci�n :
'====  Base de Datos      : Oracle 8i     Usuario : CSBPI
'=========================================================================================================
Option Explicit

Public fKey As String
Public vg_NuevoUsuario As Boolean
Public NuevaClaveAUT As String

Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana


Private Const PROCESS_QUERY_INFORMATION = &H400
Private Const STATUS_PENDING = &H103&

Private Declare Function OpenProcess Lib "kernel32" _
  (ByVal dwDesiredAccess As Long, _
   ByVal bInheritHandle As Long, _
   ByVal dwProcessId As Long) As Long
  
Private Declare Function GetExitCodeProcess Lib "kernel32" _
  (ByVal hProcess As Long, lpExitCode As Long) As Long
  
Private Declare Function CloseHandle Lib "kernel32" _
  (ByVal hObject As Long) As Long

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With
  
  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub


Public Function Fnt_Modificar(pId_Usuario, pCod_Arbol_Sistema)
  fKey = pId_Usuario

'-----------------------------------------------------------------------
'--Setea los controles dependiendo de los permisos
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
'-----------------------------------------------------------------------
  
  Call Sub_CargarDatos

  If Trim(Txt_Nombres.Text) = "" Then
    Me.Caption = "Ingreso de Usuarios"
  Else
    Me.Caption = "Modificaci�n de Usuario: " & Txt_Nombres.Text
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_Roles_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Col = 2 Then
    Cancel = True
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_Limpia_Datos
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lLinea As Long
Dim lResult As Boolean
Dim lAsesor As String
Dim lestado As String
Dim lcUsuario As Class_Usuarios
Dim lcRel_Usuario_Roles As Class_Rel_Usuarios_Roles

  On Error GoTo ErrProcedure
  Call Sub_Bloquea_Puntero(Me)

  If ValidaCampos(Txt_Email.Text) Then
  Else
    Call MsgBox("La Direccion de correo " & Trim(Txt_Email.Text) & " no es valida", vbExclamation, App.Title)
    Exit Function
  End If
  
  'Call gDB.IniciarTransaccion

  lResult = True
  
  Dim mensaje As String

  lAsesor = Fnt_ComboSelected_KEY(Cmb_Asesor)
  lestado = Fnt_ComboSelected_KEY(Cmb_Estado)

  Rem Grabando el Usuario
  Set lcUsuario = New Class_Usuarios
  With lcUsuario
    .Campo("Id_Usuario").Valor = fKey
    .Campo("Id_Asesor").Valor = IIf(lAsesor = cCmbBLANCO, Null, lAsesor)
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    .Campo("Id_Tipo_Estado").Valor = cTEstado_Usuario
    .Campo("Cod_Estado").Valor = lestado
    .Campo("Dsc_Usuario").Valor = Txt_Nombres.Text
    .Campo("Password").Valor = ""
    .Campo("Email").Valor = Trim(Txt_Email.Text)
    .Campo("FechaVigencia").Valor = CStr(dtp_FechaVigencia.Value)

    If .Guardar Then
      Rem ID DEL Usuario
      fKey = .Campo("Id_Usuario").Valor
    Else
      Call Fnt_MsgError(.SubTipo_LOG _
                        , "Problemas al guardar el usuario (" & fKey & ") " & Txt_Nombres.Text & "." _
                        , .ErrMsg _
                        , pConLog:=True)
      lResult = False
      GoTo ErrProcedure
    End If
  End With
  Set lcUsuario = Nothing
  
  If vg_NuevoUsuario = True And lestado = "V" Then
    Set lcUsuario = New Class_Usuarios
    With lcUsuario
      If .Fnt_GuardaClaveInicial(fKey, "Creasys123") Then
        '---
      Else
        Call Fnt_MsgError(.SubTipo_LOG _
                        , "Problemas al guardar el usuario (" & fKey & ") " & Txt_Nombres.Text & "." _
                        , .ErrMsg _
                        , pConLog:=True)
        lResult = False
        GoTo ErrProcedure
      End If
    End With
    Set lcUsuario = Nothing
  End If

  If vg_NuevoUsuario = True Or lestado = "PWD" Then
    'seccion ejecuta servicio de envio email
    Set lcUsuario = New Class_Usuarios
    With lcUsuario
    ' llamada a encriptador
      'ClaveEncriptada = .ExecServicioEMAIL("", Txt_Nombres.Text)
      Dim parametros As String
      parametros = Txt_Nombres.Text & "|" & gId_Empresa
      Call Exec_EnviaClavexMail(App.Path & "\ServicioMail.exe " & parametros)
      'ClaveFiltrada = Right(ClaveEncriptada, (Len(ClaveEncriptada) - 27))
    End With
    Set lcUsuario = Nothing
  End If

  '------ ELIMINA REGISTROS EXISTENTES DE LA RELACI�N REL_ROLES_ARBOL_SISTEMA --------
  Set lcRel_Usuario_Roles = New Class_Rel_Usuarios_Roles
  With lcRel_Usuario_Roles
    .Campo("Id_Usuario").Valor = fKey
    If Not .Borrar Then
      MsgBox "Problemas al Eliminar la relaci�n entre el Rol y el Usuario del Sistema." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      lResult = False
      GoTo ErrProcedure
    End If
  End With
  Set lcRel_Usuario_Roles = Nothing

  For lLinea = 1 To Grilla_Roles.Rows - 1
    Grilla_Roles.Row = lLinea
    Grilla_Roles.Col = 1
    If Grilla_Roles.Value = vbTrue Then
      Grilla_Roles.Col = 0

      Set lcRel_Usuario_Roles = New Class_Rel_Usuarios_Roles
      With lcRel_Usuario_Roles
        .Campo("Id_Usuario").Valor = fKey
        .Campo("Id_Rol").Valor = Grilla_Roles.Text
        If Not .Guardar Then
          MsgBox "Problemas al grabar la relaci�n entre el Rol y el Usuario del Sistema." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
          lResult = False
          GoTo ErrProcedure
        End If
      End With
      Set lcRel_Usuario_Roles = Nothing
    End If
  Next lLinea
  
  If vg_NuevoUsuario = True Or lestado = "PWD" Then
    Call MsgBox("Se Guardo Correctamente el Usuario y se envio contrase�a via correo", vbInformation, App.Title)
  Else
    Call MsgBox("Se Actualizo Correctamente el Usuario", vbInformation, App.Title)
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
    
ErrProcedure:
  If Err Then
    lResult = False
  End If

  'If lResult Then
  '  Call gDB.CommitTransaccion
  'Else
  '  Call gDB.RollbackTransaccion
  'End If

  Fnt_Grabar = lResult

  Set lcUsuario = Nothing
  Set lcRel_Usuario_Roles = Nothing
  Call Sub_Desbloquea_Puntero(Me)
End Function


Private Sub Exec_EnviaClavexMail(programa As String)

    Dim handle_Process As Long
    Dim id_process As Long
    Dim lp_ExitCode As Long
      
    ' Abre el proceso con el shell
    id_process = Shell(programa, 0) '0 permite trabajar en segundo plano
      
    ' handle del proceso
    handle_Process = OpenProcess(PROCESS_QUERY_INFORMATION, False, id_process)
      
    ' Mientras lp_ExitCode = STATUS_PENDING, se ejecuta el do
    Do
  
        Call GetExitCodeProcess(handle_Process, lp_ExitCode)
          
        DoEvents
     
    Loop While lp_ExitCode = STATUS_PENDING
      
    ' fin
    ' Cierra
    Call CloseHandle(handle_Process)
  
    'MsgBox "Se cerr� el " & programa, vbInformation
  
End Sub

Public Function ValidaCampos(ByVal Email As String) As Boolean

Dim i As Integer, iLen As Integer, caracter As String
    Dim pos As Integer, bp As Boolean, iPos As Integer, iPos2 As Integer

    On Local Error GoTo Err_Sub

    Email = Trim$(Email)

    If Email = vbNullString Then
        Exit Function
    End If

    Email = LCase$(Email)
    iLen = Len(Email)

    
    For i = 1 To iLen
        caracter = Mid(Email, i, 1)

        If (Not (caracter Like "[a-z]")) And (Not (caracter Like "[0-9]")) Then
            
            If InStr(1, "_-" & "." & "@", caracter) > 0 Then
                If bp = True Then
                   Exit Function
                Else
                    bp = True
                   
                    If i = 1 Or i = iLen Then
                        Exit Function
                    End If
                    
                    If caracter = "@" Then
                        If iPos = 0 Then
                            iPos = i
                        Else
                            
                            Exit Function
                        End If
                    End If
                    If caracter = "." Then
                        iPos2 = i
                    End If
                    
                End If
            Else
                
                Exit Function
            End If
        Else
            bp = False
        End If
    Next i
    If iPos = 0 Or iPos2 = 0 Then
        Exit Function
    End If
    
    If iPos2 < iPos Then
        Exit Function
    End If

    
    ValidaCampos = True
    Exit Function
Err_Sub:
    On Local Error Resume Next
    
    ValidaCampos = False
End Function



Public Function EsClaveRepetida(ByRef IdUsuario As String) As Boolean
'    Dim seguridadLogService As New Class_Encriptacion
'    Dim dsUltimasClaves As String
'    dsUltimasClaves = seguridadLogService.RegistroClavesConsultar(IdUsuario)
''    If Not IsNothing(dsUltimasClaves) AndAlso dsUltimasClaves.Rows.Count > 0 Then
''        Return dsUltimasClaves.
''            AsEnumerable().
''            Where(Function(row)
''                      Return ClsEncriptacion.DesEncriptar(row.Item("CLAVE_USUARIO")) = password
''                  End Function).Count() > 0
''    End If
'    EsClaveRepetida = True
End Function

Private Sub Sub_CargaForm()
Dim lReg As hFields
Dim lLinea As Integer
Dim lcRoles As Class_Roles

  Grilla_Roles.Rows = 1

  Call Sub_FormControl_Color(Me.Controls)

  Call Sub_CargaCombo_Asesor(Cmb_Asesor, , , True)
  Call Sub_CargaCombo_Estados(Cmb_Estado, cTEstado_Usuario)
  
  If fKey = cNewEntidad Then
    Call Sub_Elije_EstadoDefault(Cmb_Estado) 'HAF 20070703
  End If
  
  Set lcRoles = New Class_Roles
  With lcRoles
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    If .Buscar Then
      For Each lReg In .Cursor
        lLinea = Grilla_Roles.Rows
        Grilla_Roles.Rows = lLinea + 1
        
        SetCell Grilla_Roles, lLinea, "colum_pk", lReg("id_rol").Value
        SetCell Grilla_Roles, lLinea, "chk_rol", False
        SetCell Grilla_Roles, lLinea, "dsc_rol", lReg("dsc_rol").Value
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcRoles = Nothing
  
  SSTab.CurrTab = 0
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lId_Rol As Integer
Dim lcUsuario As Class_Usuarios
Dim lcRel_Usuario_Roles As Class_Rel_Usuarios_Roles

Dim lPassword As String
Dim ClaveFiltrada As String
  'Load Me

  Set lcUsuario = New Class_Usuarios
  With lcUsuario
    .Campo("Id_Usuario").Valor = fKey
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    If .Buscar Then
      For Each lReg In .Cursor
        Txt_Nombres.Text = lReg("Dsc_Usuario").Value

        Call Sub_ComboSelectedItem(Cmb_Asesor, NVL(lReg("id_Asesor").Value, ""))

        Call Sub_ComboSelectedItem(Cmb_Estado, NVL(lReg("cod_Estado").Value, ""))
        Txt_Email.Text = NVL(lReg("EMAIL_USUARIO").Value, "")
        dtp_FechaVigencia.Value = NVL(lReg("FECHA_VIGENCIA").Value, "01/01/2000")

        Me.Refresh
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcUsuario = Nothing

  Set lcRel_Usuario_Roles = New Class_Rel_Usuarios_Roles
  With lcRel_Usuario_Roles
    .Campo("Id_Usuario").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        For lLinea = 1 To Grilla_Roles.Rows - 1
          Grilla_Roles.Row = lLinea
          Grilla_Roles.Col = 0
          lId_Rol = lReg("Id_Rol").Value
          If Not Trim(Grilla_Roles.Text) = "" Then
            If Grilla_Roles.Text = lId_Rol Then
              Grilla_Roles.Col = 1
              Grilla_Roles.CellChecked = True
            End If
          End If
        Next lLinea
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcRel_Usuario_Roles = Nothing

  SSTab.CurrTab = 0
End Sub

Private Function Fnt_ValidarDatos() As Boolean
Dim lValida As Boolean
  
  lValida = True
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    lValida = False
    GoTo ErrProcedure
  End If
  
'  If Not Txt_Password.Text = Txt_Verificacion.Text Then
'    MsgBox "El Password debe ser igual a su verificaci�n.", vbCritical, Me.Caption
'    lValida = False
'    GoTo ErrProcedure
'  End If
  
ErrProcedure:
  Fnt_ValidarDatos = lValida
  
End Function

Private Sub Sub_Limpia_Datos()
Dim lLinea As Integer
  
  Txt_Nombres.Text = ""
  'Txt_Password.Text = ""
  'Txt_Verificacion.Text = ""
  Cmb_Asesor.Text = ""
  Cmb_Estado.Text = ""
  Txt_Email.Text = ""
  dtp_FechaVigencia.Value = Format(Now, "dd/MM/yyyy")
  
  With Grilla_Roles
    For lLinea = 1 To .Rows - 1
      .Row = lLinea
      .Col = 1
      .CellChecked = False
    Next lLinea
  End With
  
End Sub

