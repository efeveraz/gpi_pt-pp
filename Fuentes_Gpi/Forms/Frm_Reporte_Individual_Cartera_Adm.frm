VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{54850C51-14EA-4470-A5E4-8C5DB32DC853}#1.0#0"; "vsprint8.ocx"
Object = "{1BCC7098-34C1-4749-B1A3-6C109878B38F}#1.0#0"; "vspdf8.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Frm_Reporte_Individual_Cartera_Adm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registro Individual por Cartera Administrada"
   ClientHeight    =   5640
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9960
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5640
   ScaleWidth      =   9960
   Begin VB.Frame Frame1 
      Caption         =   "Datos de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   795
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   9735
      Begin VB.OptionButton optSinFolio 
         Caption         =   "Sin Folio"
         Height          =   255
         Left            =   7440
         TabIndex        =   9
         Top             =   360
         Width           =   1095
      End
      Begin VB.OptionButton optConFolio 
         Caption         =   "Con Folio"
         Height          =   255
         Left            =   6120
         TabIndex        =   8
         Top             =   360
         Width           =   1455
      End
      Begin MSComCtl2.DTPicker Dtp_FechaInicio 
         Height          =   345
         Left            =   1800
         TabIndex        =   1
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   609
         _Version        =   393216
         Format          =   56033281
         CurrentDate     =   38938
      End
      Begin MSComCtl2.DTPicker Dtp_FechaTermino 
         Height          =   345
         Left            =   3240
         TabIndex        =   3
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   609
         _Version        =   393216
         Format          =   56033281
         CurrentDate     =   38938
      End
      Begin VB.Label Lbl_Fecha_Ter 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Periodo Informe"
         Height          =   345
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   1545
      End
   End
   Begin VSPrinter8LibCtl.VSPrinter vp 
      Height          =   615
      Left            =   8520
      TabIndex        =   4
      Top             =   480
      Visible         =   0   'False
      Width           =   1335
      _cx             =   2355
      _cy             =   1085
      Appearance      =   1
      BorderStyle     =   1
      Enabled         =   -1  'True
      MousePointer    =   0
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AutoRTF         =   -1  'True
      Preview         =   -1  'True
      DefaultDevice   =   0   'False
      PhysicalPage    =   -1  'True
      AbortWindow     =   -1  'True
      AbortWindowPos  =   0
      AbortCaption    =   "Printing..."
      AbortTextButton =   "Cancel"
      AbortTextDevice =   "on the %s on %s"
      AbortTextPage   =   "Now printing Page %d of"
      FileName        =   ""
      MarginLeft      =   1440
      MarginTop       =   1440
      MarginRight     =   1440
      MarginBottom    =   1440
      MarginHeader    =   0
      MarginFooter    =   0
      IndentLeft      =   0
      IndentRight     =   0
      IndentFirst     =   0
      IndentTab       =   720
      SpaceBefore     =   0
      SpaceAfter      =   0
      LineSpacing     =   100
      Columns         =   1
      ColumnSpacing   =   180
      ShowGuides      =   2
      LargeChangeHorz =   300
      LargeChangeVert =   300
      SmallChangeHorz =   30
      SmallChangeVert =   30
      Track           =   0   'False
      ProportionalBars=   -1  'True
      Zoom            =   -1.23106060606061
      ZoomMode        =   3
      ZoomMax         =   400
      ZoomMin         =   10
      ZoomStep        =   25
      EmptyColor      =   -2147483636
      TextColor       =   0
      HdrColor        =   0
      BrushColor      =   0
      BrushStyle      =   0
      PenColor        =   0
      PenStyle        =   0
      PenWidth        =   0
      PageBorder      =   0
      Header          =   ""
      Footer          =   ""
      TableSep        =   "|;"
      TableBorder     =   7
      TablePen        =   0
      TablePenLR      =   0
      TablePenTB      =   0
      NavBar          =   3
      NavBarColor     =   -2147483633
      ExportFormat    =   0
      URL             =   ""
      Navigation      =   3
      NavBarMenuText  =   "Whole &Page|Page &Width|&Two Pages|Thumb&nail"
      AutoLinkNavigate=   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   9
   End
   Begin MSComDlg.CommonDialog Cmd_AbreArchivo 
      Left            =   9480
      Top             =   720
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
      Height          =   4245
      Left            =   120
      TabIndex        =   5
      Top             =   1320
      Width           =   9315
      _cx             =   16431
      _cy             =   7488
      Appearance      =   2
      BorderStyle     =   1
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MousePointer    =   0
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      BackColorFixed  =   -2147483633
      ForeColorFixed  =   -2147483630
      BackColorSel    =   65535
      ForeColorSel    =   0
      BackColorBkg    =   -2147483643
      BackColorAlternate=   -2147483643
      GridColor       =   -2147483633
      GridColorFixed  =   -2147483632
      TreeColor       =   -2147483632
      FloodColor      =   192
      SheetBorder     =   -2147483642
      FocusRect       =   2
      HighLight       =   1
      AllowSelection  =   -1  'True
      AllowBigSelection=   -1  'True
      AllowUserResizing=   1
      SelectionMode   =   3
      GridLines       =   10
      GridLinesFixed  =   2
      GridLineWidth   =   1
      Rows            =   2
      Cols            =   9
      FixedRows       =   1
      FixedCols       =   0
      RowHeightMin    =   0
      RowHeightMax    =   0
      ColWidthMin     =   0
      ColWidthMax     =   0
      ExtendLastCol   =   -1  'True
      FormatString    =   $"Frm_Reporte_Individual_Cartera_Adm.frx":0000
      ScrollTrack     =   -1  'True
      ScrollBars      =   3
      ScrollTips      =   -1  'True
      MergeCells      =   0
      MergeCompare    =   0
      AutoResize      =   -1  'True
      AutoSizeMode    =   0
      AutoSearch      =   2
      AutoSearchDelay =   2
      MultiTotals     =   -1  'True
      SubtotalPosition=   1
      OutlineBar      =   0
      OutlineCol      =   0
      Ellipsis        =   1
      ExplorerBar     =   3
      PicturesOver    =   0   'False
      FillStyle       =   0
      RightToLeft     =   0   'False
      PictureType     =   0
      TabBehavior     =   0
      OwnerDraw       =   0
      Editable        =   2
      ShowComboButton =   1
      WordWrap        =   0   'False
      TextStyle       =   0
      TextStyleFixed  =   0
      OleDragMode     =   0
      OleDropMode     =   0
      ComboSearch     =   3
      AutoSizeMouse   =   -1  'True
      FrozenRows      =   0
      FrozenCols      =   0
      AllowUserFreezing=   0
      BackColorFrozen =   0
      ForeColorFrozen =   0
      WallPaperAlignment=   9
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   24
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   9960
      _ExtentX        =   17568
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Imprimir"
            Key             =   "PRINTER"
            Description     =   "Imprime Registro de Clientes"
            Object.ToolTipText     =   "Imprime Registro de Clientes"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   3
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXCEL"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9000
         TabIndex        =   7
         Top             =   0
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin MSComctlLib.Toolbar Toolbar_Chequeo 
      Height          =   660
      Left            =   9480
      TabIndex        =   10
      Top             =   2520
      Width           =   450
      _ExtentX        =   794
      _ExtentY        =   1164
      ButtonWidth     =   1138
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SEL_ALL"
            Description     =   "Agregar un nemotecnico a la Operaci�n"
            Object.ToolTipText     =   "Selecciona todos los items"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SEL_NOTHING"
            Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
            Object.ToolTipText     =   "Deselecciona todos los items"
         EndProperty
      EndProperty
      Begin MSComctlLib.ProgressBar ProgressBar3 
         Height          =   255
         Left            =   9420
         TabIndex        =   11
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VSPDF8LibCtl.VSPDF8 VSPDF81 
      Left            =   8760
      Top             =   1200
      Author          =   ""
      Creator         =   ""
      Title           =   ""
      Subject         =   ""
      Keywords        =   ""
      Compress        =   3
   End
End
Attribute VB_Name = "Frm_Reporte_Individual_Cartera_Adm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-------------------------------------------------------------------------------------------------
'SOURCESAFE Information:
'    $Workfile: Frm_Reporte_Individual_Cartera_Adm.frm $
'    $Author: Gbuenrostro $
'    $Date: 7-02-14 14:58 $
'    $Revision: 2 $
'-------------------------------------------------------------------------------------------------

Dim oCliente            As New Class_AdministracionCartera
Dim iLineasImpresas     As Integer
Dim iTipoSalida         As Integer
Dim iTotalPaginas       As Integer
Dim sFormatoDecimal As String


Const RColorRelleno = 38 '213   ' R
Const GColorRelleno = 7 '202   ' G
Const BColorRelleno = 115 '195   ' B


Const RColorRelleno_TITULOS = 242  ' R
Const GColorRelleno_TITULOS = 242   ' G
Const BColorRelleno_TITULOS = 242   ' B

Const glb_tamletra_titulo = 10
Const glb_tamletra_encabezado = 9
Const glb_tamletra_registros = 7.5

Const Font_Name = "Arial" '"Times New Roman"

Const COLUMNA_INICIO As String = "10mm"
Const CONS_NUMERO_LINEAS As Integer = 30   ' Numero de Lineas
Const CONS_POR_PANTALLA As Integer = 0
Const CONS_POR_IMPRESORA As Integer = 1
Const CONS_POR_ARCHIVO As Integer = 2
Const CONS_ADM_CARTERA As Integer = 1

Const FIL_ENCABEZADO = 12

Dim App_Excel       As Excel.Application      ' Excel application
Dim lLibro          As Excel.Workbook      ' Excel workbook

Public Sub Mostrar(pCod_Arbol_Sistema)
  Me.Show
End Sub

Private Sub Form_Load()

    With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("EXIT").Image = cBoton_Salir
        .Buttons("REFRESH").Image = cBoton_Refrescar
        .Buttons("PRINTER").Image = cBoton_Imprimir
    End With
    
    With Toolbar_Chequeo
     Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
        .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
        .Appearance = ccFlat
    End With
    
    Call Sub_CargaForm
    
    Me.Top = 1
    Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
    Call Sub_FormControl_Color(Me.Controls)
    
    oCliente.IdTipoCuenta = CONS_ADM_CARTERA
    Call Sub_Carga_Grilla
    Dtp_FechaInicio.Value = Fnt_FechaServidor
    Dtp_FechaTermino.Value = Fnt_FechaServidor
    optSinFolio.Value = True
    optConFolio.Value = False
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
      Call Sub_Limpiar
    Case "PRINTER"
      iTipoSalida = CONS_POR_IMPRESORA
      Call GenerarInforme(False)
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
      iTipoSalida = CONS_POR_PANTALLA
      Call GenerarInforme(False)
    Case "PDF"
      iTipoSalida = CONS_POR_PANTALLA
      Call GenerarInforme(False)
    Case "EXCEL"
      Call GenerarInforme(True)
  End Select
End Sub

Private Sub Toolbar_Chequeo_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
       Call Sub_CambiaCheck(True)
    Case "SEL_NOTHING"
       Call Sub_CambiaCheck(False)
   End Select

End Sub

Private Sub Sub_Limpiar()
    Sub_CambiaCheck (False)
    Dtp_FechaInicio.Value = Fnt_FechaServidor
    Dtp_FechaTermino.Value = Fnt_FechaServidor
    optSinFolio.Value = True
    optConFolio.Value = False
End Sub

Private Sub Sub_CambiaCheck(pValor As Boolean)
    Dim lLinea      As Long
    Dim lCol        As Long
  
  
    lCol = Grilla_Cuentas.ColIndex("CHK")
    If pValor Then
        For lLinea = 1 To Grilla_Cuentas.Rows - 1
            Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexChecked
        Next
    Else
        For lLinea = 1 To Grilla_Cuentas.Rows - 1
            Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexUnchecked
        Next
    End If
  
  
End Sub
Private Function VerificaEntrada() As Boolean
Dim bOk As Boolean
Dim i As Integer

    If Dtp_FechaInicio.Value > Dtp_FechaTermino.Value Then
        MsgBox "Fecha Inicio debe ser menor o igual a Fecha Termino", vbInformation, Me.Caption
        bOk = False
    Else
        bOk = True
        With Grilla_Cuentas
            For i = 1 To Grilla_Cuentas.Rows - 1
                Call SetCell(Grilla_Cuentas, i, "mensaje", "")
                If .Cell(flexcpChecked, i, .ColIndex("chk")) = flexChecked Then
                    bOk = True
                    Exit For
                End If
            Next
        End With
        If Not bOk Then
            MsgBox "Debe seleccionar al menos una cuenta.", vbInformation, Me.Caption
        End If
    End If
    VerificaEntrada = bOk
End Function
Private Sub GenerarInforme(ByVal XLS As Boolean)
Dim lId_Cuenta As Integer
Dim lcCuenta   As Object
Dim lForm      As Frm_Reporte_Generico
Dim i          As Integer


    Call Sub_Bloquea_Puntero(Me)
    If Not VerificaEntrada Then
        Call Sub_Desbloquea_Puntero(Me)
        Exit Sub
    End If
    oCliente.FechaInicio = Dtp_FechaInicio.Value
    oCliente.FechaTermino = Dtp_FechaTermino.Value
    iTotalPaginas = 0
    With Grilla_Cuentas
        If Not XLS Then
            Set lForm = New Frm_Reporte_Generico
        
            Call lForm.Sub_InicarDocumento(pTitulo:="" _
                                   , pTipoSalida:=ePrinter.eP_Pantalla _
                                   , pOrientacion:=orLandscape)
        End If
        For i = 1 To .Rows - 1
            If .Cell(flexcpChecked, i, .ColIndex("chk")) = flexChecked Then
                Call SetCell(Grilla_Cuentas, i, "mensaje", "Generando Informe")
                oCliente.IdCuenta = GetCell(Grilla_Cuentas, i, "colum_pk")
                oCliente.Num_Cuenta = NVL(GetCell(Grilla_Cuentas, i, "num_cuenta"), 0)
                oCliente.NumeroFolio = NVL(GetCell(Grilla_Cuentas, i, "numero_folio"), 0) + 1
                oCliente.RutCliente = GetCell(Grilla_Cuentas, i, "rut_cliente")
                oCliente.NombreCliente = GetCell(Grilla_Cuentas, i, "razon_social")
                sFormatoDecimal = Fnt_Formato_Moneda(GetCell(Grilla_Cuentas, i, "moneda"))
                If ActualizaFolio Then
                    If XLS Then
                        ProcesoGeneracionInformeXLS
                    Else
                        ProcesoGeneracionInforme lForm.VsPrinter
                    End If
                    Call SetCell(Grilla_Cuentas, i, "mensaje", "Informe Generado")
                    If optConFolio.Value = True Then
                        Call SetCell(Grilla_Cuentas, i, "numero_folio", oCliente.NumeroFolio, pAutoSize:=False)
                    End If
                Else
                    Call SetCell(Grilla_Cuentas, i, "mensaje", "Error al actualizar N�mero de Folio")
                End If
            End If
        Next
        
        If Not XLS Then
            lForm.VsPrinter.EndDoc
        End If
    End With
    
    If Not XLS Then
        Call ColocarEn(lForm, Me, eFP_Abajo)
    End If
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub
Private Function ActualizaFolio() As Boolean
Dim lcAdmCartera As Class_AdministracionCartera
Dim bResultOk As Boolean

    bResultOk = True
    If optConFolio.Value = True Then
        gDB.Parametros.Clear
        gDB.Procedimiento = "PKG_ADMINISTRACION_CARTERA$ActualizaFolio"
        gDB.Parametros.Add "pId_Cuenta", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
        gDB.Parametros.Add "pNumeroFolio", ePT_Numero, oCliente.NumeroFolio, ePD_Entrada
        If Not gDB.EjecutaSP Then
           bResultOk = False
        End If
    End If
    ActualizaFolio = bResultOk

End Function
Private Sub ProcesoGeneracionInforme(ByRef vp As VsPrinter)
    
    Call ImprimeEncabezado(vp)
    Call ImprimeEncabezadoInstrumentos(vp)
    Call ImprimeMovimientosInstrumentos(vp)
    iLineasImpresas = iLineasImpresas + 1
    If (iLineasImpresas > CONS_NUMERO_LINEAS) Then
        vp.EndTable
        vp.NewPage
        Call ImprimeEncabezado(vp)
        iLineasImpresas = iLineasImpresas + 1
    End If
    Call ImprimeMovimientosDinero(vp)
    'vp.EndDoc
End Sub

Private Sub ProcesoGeneracionInformeXLS()
    Call ImprimeEncabezadoXLS
    Call ImprimeEncabezadoInstrumentosXLS
    Call ImprimeMovimientosInstrumentosXLS

    Call ImprimeMovimientosDineroXLS
    lLibro.ActiveSheet.Columns.AutoFit
    
    lLibro.ActiveSheet.Cells(1, 1).Select
       
    lLibro.Worksheets(1).Delete
    
    App_Excel.Visible = True
    App_Excel.UserControl = True
    
    Set App_Excel = Nothing
    Set lLibro = Nothing
End Sub

Sub FormatoHoja()
    With vp
        .PaperSize = pprLetter
        .MarginLeft = COLUMNA_INICIO   ' "15mm"
        .MarginRight = "15mm"
        .MarginTop = "45mm"
        .MarginBottom = "25mm"
        .MarginFooter = "15mm"
        .MarginHeader = "0mm"
        
        .Font.Name = Font_Name
        .Font.Size = glb_tamletra_registros - 1
        
        .LineSpacing = 110
        
        .Orientation = orLandscape 'orPortrait
    End With
    
End Sub
Private Sub ImprimeEncabezado(ByRef vp As VsPrinter)
Dim sTitulo As String
Dim iFila As Integer
'-------------------------------------------------------------------
Dim sRutaImagen As String
'-------------------------------------------------------------------

    sTitulo = "REGISTRO INDIVIDUAL POR CARTERA ADMINISTRADA"
    
    On Error GoTo 0

    'Guarda valores anteriores
    sRutaImagen = App.Path & "\security.jpg"

    'Inserta Logo
    On Error Resume Next
    iLineasImpresas = 0
    With vp
        .FontBold = True
        .FontSize = glb_tamletra_titulo
        .DrawPicture LoadPicture(sRutaImagen), "200mm", "7mm", "65mm", "10mm"
        
        If optConFolio.Value = True Then
            .TextAlign = taRightTop
            .Paragraph = "N� Folio: " & oCliente.NumeroFolio
            iLineasImpresas = iLineasImpresas + 1
        End If

        .TextAlign = taLeftTop
        iLineasImpresas = iLineasImpresas + 1
        .Paragraph = sTitulo
        iLineasImpresas = iLineasImpresas + 1
        .Paragraph = "Informe desde el: " & Dtp_FechaInicio.Value & " hasta el: " & Dtp_FechaTermino.Value
        
        
        .Paragraph = ""
        .Paragraph = ""
        .FontSize = glb_tamletra_encabezado
                
        iFila = 1
        iLineasImpresas = iLineasImpresas + 1
        .FontSize = glb_tamletra_titulo
        .Paragraph = "ANTECEDENTES DEL CLIENTE"
        .FontSize = glb_tamletra_encabezado
        iLineasImpresas = iLineasImpresas + 1
        .Paragraph = "RUT      :" & oCliente.RutCliente
        iLineasImpresas = iLineasImpresas + 1
        .Paragraph = "Nombre   :" & oCliente.NombreCliente
        iLineasImpresas = iLineasImpresas + 1
        .Paragraph = "Cuenta   :" & oCliente.Num_Cuenta
                
        .FontSize = glb_tamletra_registros
        .FontBold = False
    End With
    
    iTotalPaginas = iTotalPaginas + 1
End Sub

Function ImprimeEncabezadoXLS() As Integer
    Dim intFilaInicioDatos As Integer
    Dim sTitulo As String
    Dim i           As Integer

    Set App_Excel = CreateObject("Excel.application")
    App_Excel.DisplayAlerts = False
    Set lLibro = App_Excel.Workbooks.Add
    
    App_Excel.Visible = False
    
    For i = 1 To lLibro.Worksheets.Count - 1
        lLibro.Worksheets(i).Delete
    Next
    
    lLibro.Worksheets.Add After:=lLibro.Worksheets(lLibro.Worksheets.Count)
    
    'NOMBRE HOJA
    lLibro.ActiveSheet.Name = "Reporte"
    lLibro.ActiveSheet.Cells(1, 1).Select
    lLibro.ActiveSheet.Pictures.Insert(gStrPictureEmpresa).ShapeRange.IncrementLeft 29.75
    
    App_Excel.ActiveWindow.DisplayGridlines = False
    
    With lLibro.ActiveSheet
        sTitulo = "REGISTRO INDIVIDUAL POR CARTERA ADMINISTRADA"
        
        iLineasImpresas = 5
        .Range("B5").Value = sTitulo
        .Range("B5:E5").HorizontalAlignment = xlLeft
        .Range("B5:E5").Merge
        
        .Range("B6").Value = "Informe desde el: " & Dtp_FechaInicio.Value & " hasta el: " & Dtp_FechaTermino.Value
        .Range("B6:E6").Merge
        
        If optConFolio.Value = True Then
            Range("B5").Value = "N� Folio: " & oCliente.NumeroFolio
        End If
        
        .Range("B8").Value = "ANTECEDENTES DEL CLIENTE"
        .Range("B8:E8").Merge
        .Range("B9").Value = "RUT      :" & oCliente.RutCliente
        .Range("B9:E9").Merge
        .Range("B10").Value = "Nombre   :" & oCliente.NombreCliente
        .Range("B10:E10").Merge
        .Range("B11").Value = "Cuenta   :" & oCliente.Num_Cuenta
        .Range("B11:E11").Merge
        
        .Range("B5:M" & FIL_ENCABEZADO).Font.Bold = True
    End With
    
    iLineasImpresas = FIL_ENCABEZADO ' Columna donde comienzan los datos
    
End Function

Private Sub ImprimeEncabezadoInstrumentos(ByRef vp As VsPrinter)
Dim iFila As Integer
    
    On Error GoTo 0
    
    With vp
        .FontBold = True
        .Paragraph = ""
        .FontSize = glb_tamletra_encabezado
                
        iLineasImpresas = iLineasImpresas + 1
        
        iFila = 1
        .TextAlign = taLeftTop
        .FontSize = glb_tamletra_titulo
        .Paragraph = "MOVIMIENTOS DE INSTRUMENTOS"
        
        .StartTable
        .TableBorder = tbBox
        .TableBorder = tbBoxRows
        .FontSize = glb_tamletra_encabezado
        
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        .TableCell(tcCols) = 7
        .TableCell(tcRows) = 1
        .TableCell(tcText, 1, 1) = "Fecha Operaci�n"
        .TableCell(tcText, 1, 2) = "Movimientos"
        .TableCell(tcText, 1, 3) = "Operaci�n"
        .TableCell(tcText, 1, 4) = "Instrumento"
        .TableCell(tcText, 1, 5) = "Cantidad"
        .TableCell(tcText, 1, 6) = "Precio/Tasa"
        .TableCell(tcText, 1, 7) = "Monto ($)"
        
        .TableCell(tcColWidth, 1, 1) = "25mm": .TableCell(tcAlign, 1, 1) = taCenterMiddle
        .TableCell(tcColWidth, 1, 2) = "25mm": .TableCell(tcAlign, 1, 2) = taCenterMiddle
        .TableCell(tcColWidth, 1, 3) = "20mm": .TableCell(tcAlign, 1, 3) = taCenterMiddle
        .TableCell(tcColWidth, 1, 4) = "60mm": .TableCell(tcAlign, 1, 4) = taCenterMiddle
        .TableCell(tcColWidth, 1, 5) = "35mm": .TableCell(tcAlign, 1, 5) = taCenterMiddle
        .TableCell(tcColWidth, 1, 6) = "35mm": .TableCell(tcAlign, 1, 6) = taCenterMiddle
        .TableCell(tcColWidth, 1, 7) = "35mm": .TableCell(tcAlign, 1, 7) = taCenterMiddle
                   
        
        .TableCell(tcBackColor, 1) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        .TableCell(tcBackColor, 2) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        .TableCell(tcBackColor, 3) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        .TableCell(tcBackColor, 4) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        .TableCell(tcBackColor, 5) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        .TableCell(tcBackColor, 6) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        .TableCell(tcBackColor, 7) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        
        
        iLineasImpresas = iLineasImpresas + .TableCell(tcRows) '(.LineSpacing * .TableCell(tcRows))
        .FontSize = glb_tamletra_registros
        .FontBold = False
    End With
    
    
End Sub

Private Sub ImprimeEncabezadoInstrumentosXLS()
    Dim iFila As Integer
    
    On Error GoTo 0
    With lLibro.ActiveSheet
        .Range("B" & iLineasImpresas + 1).Value = "MOVIMIENTOS DE INSTRUMENTOS"
        .Range("B" & iLineasImpresas + 1 & ":E" & iLineasImpresas + 1).Merge
        .Range("B" & iLineasImpresas + 1 & ":E" & iLineasImpresas + 1).Font.Bold = True
        
        iLineasImpresas = iLineasImpresas + 1
        
        .Cells(iLineasImpresas + 1, 2) = "Fecha Operaci�n"
        .Cells(iLineasImpresas + 1, 3) = "Movimientos"
        .Cells(iLineasImpresas + 1, 4) = "Operaci�n"
        .Cells(iLineasImpresas + 1, 5) = "Instrumento"
        .Cells(iLineasImpresas + 1, 6) = "Cantidad"
        .Cells(iLineasImpresas + 1, 7) = "Precio/Tasa"
        .Cells(iLineasImpresas + 1, 8) = "Monto ($)"
        
        .Range(.Cells(iLineasImpresas + 1, 2), .Cells(iLineasImpresas + 1, 8)).Interior.Color = RGB(255, 255, 0)
        .Range(.Cells(iLineasImpresas + 1, 2), .Cells(iLineasImpresas + 1, 8)).Font.Bold = True
        .Range(.Cells(iLineasImpresas + 1, 2), .Cells(iLineasImpresas + 1, 8)).BorderAround
        .Range(.Cells(iLineasImpresas + 1, 2), .Cells(iLineasImpresas + 1, 8)).Borders.Color = RGB(0, 0, 0)
        .Range(.Cells(iLineasImpresas + 1, 2), .Cells(iLineasImpresas + 1, 8)).HorizontalAlignment = xlCenter
        
        .Range("B:B").HorizontalAlignment = xlLeft
        .Range("C:C").HorizontalAlignment = xlRight
        .Range("D:D").HorizontalAlignment = xlLeft
        .Range("E:F").HorizontalAlignment = xlCenter
        .Range("G:G").HorizontalAlignment = xlLeft
        .Range("H:H").HorizontalAlignment = xlLeft
        .Range("I:I").HorizontalAlignment = xlLeft
                
        .Range("B" & FIL_ENCABEZADO + 1 & ":I" & FIL_ENCABEZADO + 1).Font.Bold = True
        
        .Columns("A:A").ColumnWidth = 2
        .Columns("B:B").ColumnWidth = 25
        .Columns("C:C").ColumnWidth = 13
        .Columns("D:D").ColumnWidth = 10
        .Columns("E:E").ColumnWidth = 10
        .Columns("F:F").ColumnWidth = 5
        .Columns("G:G").ColumnWidth = 15
        .Columns("H:H").ColumnWidth = 15
        .Columns("I:I").ColumnWidth = 15
        
        iLineasImpresas = iLineasImpresas + 1
    End With
    
End Sub

Private Sub ImprimeMovimientosInstrumentos(ByRef vp As VsPrinter)
Dim lcAdmCartera As Class_AdministracionCartera
Dim lcursor As hRecord
Dim lReg As hFields
Dim iFila As Integer
Dim iNumRegistro As Integer
Dim iTotalRegistros As Integer

    Set lcAdmCartera = New Class_AdministracionCartera
    With lcAdmCartera
        .Campo("id_cuenta").Valor = oCliente.IdCuenta
        .Campo("fecha_inicio").Valor = Dtp_FechaInicio.Value
        .Campo("fecha_termino").Valor = Dtp_FechaTermino.Value
        
      If .BuscarMovimientosInstrumentos Then
        Set lcursor = .Cursor
        iTotalRegistros = lcursor.Count
      Else
        MsgBox "Problemas en cargar Movimientos de Instrumentos." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
        Exit Sub
      End If
    End With
    Set lcAdmCartera = Nothing
    With vp
        iNumRegistro = 0
        .FontSize = glb_tamletra_registros
        If iTotalRegistros = 0 Then
            .EndTable
            .FontSize = glb_tamletra_encabezado
            .Paragraph = "No existen Movimientos en este periodo"
            
        Else
            For Each lReg In lcursor
                
                iLineasImpresas = iLineasImpresas + 1
                If (iLineasImpresas > CONS_NUMERO_LINEAS) Then
                    .EndTable
                    .NewPage
                    Call ImprimeEncabezado(vp)
                    Call ImprimeEncabezadoInstrumentos(vp)
                    iLineasImpresas = iLineasImpresas + 1
                End If
                .TableCell(tcRows) = .TableCell(tcRows) + 1
            
                iFila = .TableCell(tcRows)
                iNumRegistro = iNumRegistro + 1
                .TableCell(tcText, iFila, 1) = Trim(lReg("fecha_operacion").Value)
                .TableCell(tcText, iFila, 2) = NVL(Trim(lReg("dsc_tipo_movimiento").Value), "")
                .TableCell(tcText, iFila, 3) = IIf(Trim(lReg("folio").Value) <> 0, Trim(lReg("folio").Value), "-")
                .TableCell(tcText, iFila, 4) = Trim(lReg("nemotecnico").Value)
                .TableCell(tcText, iFila, 5) = FormatNumber(lReg("cantidad").Value, 4)
                .TableCell(tcText, iFila, 6) = FormatNumber(lReg("precio").Value, 2)
                .TableCell(tcText, iFila, 7) = Format(lReg("monto_bruto").Value, sFormatoDecimal)
                
                .TableCell(tcAlign, iFila, 1) = taCenterMiddle
                .TableCell(tcAlign, iFila, 2) = taLeftMiddle
                .TableCell(tcAlign, iFila, 3) = taCenterMiddle
                .TableCell(tcAlign, iFila, 4) = taLeftMiddle
                .TableCell(tcAlign, iFila, 5) = taRightMiddle
                .TableCell(tcAlign, iFila, 6) = taRightMiddle
                .TableCell(tcAlign, iFila, 7) = taRightMiddle
    
                If (iNumRegistro = iTotalRegistros) Then
                    .EndTable
                End If
            Next
        End If
     End With
End Sub

Private Sub ImprimeMovimientosInstrumentosXLS()
    Dim lcAdmCartera As Class_AdministracionCartera
    Dim lcursor As hRecord
    Dim lReg As hFields
    Dim iFila As Integer
    Dim iNumRegistro As Integer
    Dim iTotalRegistros As Integer

    Set lcAdmCartera = New Class_AdministracionCartera
    With lcAdmCartera
        .Campo("id_cuenta").Valor = oCliente.IdCuenta
        .Campo("fecha_inicio").Valor = Dtp_FechaInicio.Value
        .Campo("fecha_termino").Valor = Dtp_FechaTermino.Value
        
        If .BuscarMovimientosInstrumentos Then
            Set lcursor = .Cursor
            iTotalRegistros = lcursor.Count
        Else
            MsgBox "Problemas en cargar Movimientos de Instrumentos." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            Exit Sub
        End If
    End With
    Set lcAdmCartera = Nothing
    
    With lLibro.ActiveSheet
        If iTotalRegistros = 0 Then
            .Range("B" & iLineasImpresas + 1).Value = "No existen Movimientos en este periodo"
            .Range("B" & iLineasImpresas + 1 & ":E" & iLineasImpresas + 1).Merge
            iLineasImpresas = iLineasImpresas + 1
        Else
            For Each lReg In lcursor

                .Cells(iLineasImpresas + 1, 2) = Trim(lReg("fecha_operacion").Value)
                .Cells(iLineasImpresas + 1, 3) = NVL(Trim(lReg("dsc_tipo_movimiento").Value), "")
                .Cells(iLineasImpresas + 1, 4) = IIf(Trim(lReg("folio").Value) <> 0, Trim(lReg("folio").Value), "-")
                .Cells(iLineasImpresas + 1, 5) = Trim(lReg("nemotecnico").Value)
                .Cells(iLineasImpresas + 1, 6) = FormatNumber(lReg("cantidad").Value, 4)
                .Cells(iLineasImpresas + 1, 7) = FormatNumber(lReg("precio").Value, 2)
                .Cells(iLineasImpresas + 1, 8) = Format(lReg("monto_bruto").Value, sFormatoDecimal)
                                
                .Range("B" & iLineasImpresas + 1).HorizontalAlignment = xlCenter
                .Range("C" & iLineasImpresas + 1).HorizontalAlignment = xlLeft
                .Range("D" & iLineasImpresas + 1).HorizontalAlignment = xlCenter
                .Range("F" & iLineasImpresas + 1).HorizontalAlignment = xlLeft
                .Range("G" & iLineasImpresas + 1).HorizontalAlignment = xlRight
                .Range("H" & iLineasImpresas + 1).HorizontalAlignment = xlRight
                .Range("I" & iLineasImpresas + 1).HorizontalAlignment = xlRight
                iLineasImpresas = iLineasImpresas + 1
            Next
        End If
     End With
End Sub

Private Sub ImprimeEncabezadoDinero(ByRef vp As VsPrinter, ByVal sCaja As String)
    Dim iFila As Integer
    On Error GoTo 0
    
    With vp
        .FontBold = True
        .Paragraph = ""
        .FontSize = glb_tamletra_encabezado
                
        If ((iLineasImpresas + 5) > CONS_NUMERO_LINEAS) Then
            .NewPage
            Call ImprimeEncabezado(vp)
            
        End If
        iLineasImpresas = iLineasImpresas + 1
        
        .TextAlign = taLeftTop
        .FontSize = glb_tamletra_titulo
        .Paragraph = "MOVIMIENTOS DE DINERO - " & sCaja
        
        .StartTable
        .TableBorder = tbBox
        .TableBorder = tbBoxRows
        .FontSize = glb_tamletra_encabezado
        
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        .TableCell(tcCols) = 6
        
        .TableCell(tcText, 1, 1) = "Fecha Operaci�n"
        .TableCell(tcText, 1, 2) = "Concepto"
        .TableCell(tcText, 1, 3) = "Ingreso"
        .TableCell(tcText, 1, 4) = "Egreso"
        .TableCell(tcText, 1, 5) = "Saldo"
        .TableCell(tcText, 1, 6) = "N� Comprobante"
        
        .TableCell(tcColWidth, 1, 1) = "20mm": .TableCell(tcAlign, 1, 1) = taCenterMiddle
        .TableCell(tcColWidth, 1, 2) = "120mm": .TableCell(tcAlign, 1, 2) = taCenterMiddle
        .TableCell(tcColWidth, 1, 3) = "30mm": .TableCell(tcAlign, 1, 3) = taRightMiddle
        .TableCell(tcColWidth, 1, 4) = "30mm": .TableCell(tcAlign, 1, 4) = taRightMiddle
        .TableCell(tcColWidth, 1, 5) = "30mm": .TableCell(tcAlign, 1, 5) = taRightMiddle
        .TableCell(tcColWidth, 1, 6) = "20mm": .TableCell(tcAlign, 1, 6) = taCenterMiddle

        
        .TableCell(tcBackColor, 1) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        .TableCell(tcBackColor, 2) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        .TableCell(tcBackColor, 3) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        .TableCell(tcBackColor, 4) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        .TableCell(tcBackColor, 5) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        .TableCell(tcBackColor, 6) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        
        iLineasImpresas = iLineasImpresas + 2 '(.LineSpacing * .TableCell(tcRows))
        '.EndTable
        .FontSize = glb_tamletra_registros
        .FontBold = False
    End With
    
    
End Sub

Private Sub ImprimeEncabezadoDineroXLS(ByVal sCaja As String)
    Dim iFila As Integer
    On Error GoTo 0
    
    With lLibro.ActiveSheet
        
        iLineasImpresas = iLineasImpresas + 1
        
        .Range("B" & iLineasImpresas + 1).Value = "MOVIMIENTOS DE DINERO - " & sCaja
        .Range("B" & iLineasImpresas + 1 & ":E" & iLineasImpresas + 1).Merge
        .Range("B" & iLineasImpresas + 1 & ":E" & iLineasImpresas + 1).Font.Bold = True

        iLineasImpresas = iLineasImpresas + 1
        
        .Cells(iLineasImpresas + 1, 2) = "Fecha Operaci�n"
        .Cells(iLineasImpresas + 1, 3) = "Concepto"
        .Cells(iLineasImpresas + 1, 4) = "Ingreso"
        .Cells(iLineasImpresas + 1, 5) = "Egreso"
        .Cells(iLineasImpresas + 1, 6) = "Saldo"
        .Cells(iLineasImpresas + 1, 7) = "N� Comprobante"
        
        .Range(.Cells(iLineasImpresas + 1, 2), .Cells(iLineasImpresas + 1, 7)).Interior.Color = RGB(255, 255, 0)
        .Range(.Cells(iLineasImpresas + 1, 2), .Cells(iLineasImpresas + 1, 7)).Font.Bold = True
        .Range(.Cells(iLineasImpresas + 1, 2), .Cells(iLineasImpresas + 1, 7)).BorderAround
        .Range(.Cells(iLineasImpresas + 1, 2), .Cells(iLineasImpresas + 1, 7)).Borders.Color = RGB(0, 0, 0)
        .Range(.Cells(iLineasImpresas + 1, 2), .Cells(iLineasImpresas + 1, 7)).HorizontalAlignment = xlCenter
        
        iLineasImpresas = iLineasImpresas + 1
    End With
End Sub

Private Sub ImprimeMovimientosDinero(ByRef vp As VsPrinter)
Dim lCajas_Ctas         As Class_Cajas_Cuenta
Dim lCursor_Caj         As hRecord
Dim Cursor_Mov_Caja     As hRecord
Dim lReg_Mov_Caja       As hFields
Dim lReg_Caj            As hFields
Dim iFila               As Integer
Dim iNumRegistro        As Integer
Dim iTotalRegistros     As Integer
Dim nDecimalesCaja      As Integer
Dim sCajaActual         As String
Dim ultimosaldo         As Double
Dim anterior            As Double
    

    Set lCajas_Ctas = New Class_Cajas_Cuenta
    With lCajas_Ctas
        .Campo("id_cuenta").Valor = oCliente.IdCuenta
        If .Buscar(True) Then
            Set lCursor_Caj = .Cursor
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
        End If
    End With
    Set lCajas_Ctas = Nothing
    
    With vp
        .FontSize = glb_tamletra_registros
        For Each lReg_Caj In lCursor_Caj
            nDecimalesCaja = lReg_Caj("Decimales").Value
'--------------------------------------------------------------------------------------------------------------------------
            gDB.Parametros.Clear
            gDB.Procedimiento = "Pkg_Saldos_Caja$Buscar_Saldos_Cuenta"
            gDB.Parametros.Add "PID_CAJA_CUENTA", ePT_Numero, lReg_Caj("id_caja_cuenta").Value, ePD_Entrada
            gDB.Parametros.Add "PID_CUENTA", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
            gDB.Parametros.Add "PFecha_Cierre", ePT_Fecha, CDate(UltimoDiaDelMes(Dtp_FechaInicio.Value)) + 1, ePD_Entrada
            gDB.Parametros.Add "PSaldo", ePT_Numero, ultimosaldo, ePD_Salida
            
            
            If gDB.EjecutaSP Then
                ultimosaldo = gDB.Parametros("Psaldo").Valor
            End If
'--------------------------------------------------------------------------------------------------------------------------
            
'--------------------------------------------------------------------------------------------------------------------------
            gDB.Parametros.Clear
            gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
            gDB.Parametros.Add "Pidcajacuenta", ePT_Numero, lReg_Caj("id_caja_cuenta").Value, ePD_Entrada
            gDB.Parametros.Add "Pfecha_inicio", ePT_Fecha, CDate(UltimoDiaDelMes(Dtp_FechaInicio.Value)) + 1, ePD_Entrada
            gDB.Parametros.Add "Pfecha_termino", ePT_Fecha, Dtp_FechaTermino.Value, ePD_Entrada
            gDB.Procedimiento = "PKG_MOVIMIENTOS_CAJA_SECURITY"
            If Not gDB.EjecutaSP Then
                Exit Sub
            End If
            Set Cursor_Mov_Caja = gDB.Parametros("Pcursor").Valor
'----------------------------------------------------------------------------------------------------------------------------
            iTotalRegistros = Cursor_Mov_Caja.Count
            sCajaActual = NVL(lReg_Caj("dsc_caja_cuenta").Value, "Caja")
                
            iLineasImpresas = iLineasImpresas + 1
            If ((iLineasImpresas + 5) > CONS_NUMERO_LINEAS) Then
'                .EndTable
                .NewPage
                Call ImprimeEncabezado(vp)
                Call ImprimeEncabezadoDinero(vp, sCajaActual)
                iLineasImpresas = iLineasImpresas + 1
            Else
                Call ImprimeEncabezadoDinero(vp, sCajaActual)
            End If
            
            
            iNumRegistro = 0
            If iTotalRegistros = 0 Then
                .EndTable
                .FontSize = glb_tamletra_encabezado
                .Paragraph = "No existen Movimientos en este periodo"
                
            Else
                iLineasImpresas = iLineasImpresas + 1
                If (iLineasImpresas > CONS_NUMERO_LINEAS) Then
                    .EndTable
                    .NewPage
                    Call ImprimeEncabezado(vp)
                    Call ImprimeEncabezadoDinero(vp, sCajaActual)
                    iLineasImpresas = iLineasImpresas + 1
                End If
                
                .TableCell(tcRows) = .TableCell(tcRows) + 1
                           
                iFila = .TableCell(tcRows)
             
                .TableCell(tcText, iFila, 1) = UltimoDiaDelMes(Dtp_FechaInicio.Value)
                .TableCell(tcText, iFila, 2) = "SALDO ANTERIOR"
                .TableCell(tcText, iFila, 3) = ""
                .TableCell(tcText, iFila, 4) = ""
                .TableCell(tcText, iFila, 5) = FormatNumber(ultimosaldo, nDecimalesCaja)
                .TableCell(tcText, iFila, 6) = ""
                    
                .TableCell(tcAlign, iFila, 1) = taCenterMiddle
                .TableCell(tcAlign, iFila, 2) = taLeftMiddle
                .TableCell(tcAlign, iFila, 3) = taRightMiddle
                .TableCell(tcAlign, iFila, 4) = taRightMiddle
                .TableCell(tcAlign, iFila, 5) = taRightMiddle
                .TableCell(tcAlign, iFila, 6) = taCenterMiddle
                        
                For Each lReg_Mov_Caja In Cursor_Mov_Caja
                    iLineasImpresas = iLineasImpresas + 1
                    If (iLineasImpresas > CONS_NUMERO_LINEAS) Then
                        .EndTable
                        .NewPage
                        Call ImprimeEncabezado(vp)
                        Call ImprimeEncabezadoDinero(vp, sCajaActual)
                        iLineasImpresas = iLineasImpresas + 1
                    End If
                    
    
                    .TableCell(tcRows) = .TableCell(tcRows) + 1
                
                    iFila = .TableCell(tcRows)
                    
                    If anterior <> lReg_Mov_Caja("id_operacion").Value Or anterior = 0 Then
                        ultimosaldo = ultimosaldo + lReg_Mov_Caja("monto_movto_abono").Value - lReg_Mov_Caja("monto_movto_cargo").Value
                    End If
                
                    iNumRegistro = iNumRegistro + 1
                    .TableCell(tcText, iFila, 1) = Trim(lReg_Mov_Caja("fecha_liquidacion").Value)
                    .TableCell(tcText, iFila, 2) = Trim(lReg_Mov_Caja("descripcion").Value)
                    If lReg_Mov_Caja("monto_movto_abono").Value > 0 Then
                        .TableCell(tcText, iFila, 3) = FormatNumber(lReg_Mov_Caja("monto_movto_abono").Value, nDecimalesCaja)
                    End If
                    If lReg_Mov_Caja("monto_movto_cargo").Value > 0 Then
                        .TableCell(tcText, iFila, 4) = FormatNumber(lReg_Mov_Caja("monto_movto_cargo").Value, nDecimalesCaja)
                    End If
                    .TableCell(tcText, iFila, 5) = FormatNumber(ultimosaldo, nDecimalesCaja)
                    If lReg_Mov_Caja("id_operacion").Value <> 0 Then
                        .TableCell(tcText, iFila, 6) = lReg_Mov_Caja("id_operacion").Value
                    Else
                        .TableCell(tcText, iFila, 6) = "-"
                    End If

                    .TableCell(tcAlign, iFila, 1) = taCenterMiddle
                    .TableCell(tcAlign, iFila, 2) = taLeftMiddle
                    .TableCell(tcAlign, iFila, 3) = taRightMiddle
                    .TableCell(tcAlign, iFila, 4) = taRightMiddle
                    .TableCell(tcAlign, iFila, 5) = taRightMiddle
                    .TableCell(tcAlign, iFila, 6) = taCenterMiddle
                    anterior = lReg_Mov_Caja("id_operacion").Value
                    If (iNumRegistro = iTotalRegistros) Then
                        .EndTable
                    End If
                Next
            End If
        Next
            
    End With
End Sub

Private Sub ImprimeMovimientosDineroXLS()
    Dim lCajas_Ctas         As Class_Cajas_Cuenta
    Dim lCursor_Caj         As hRecord
    Dim Cursor_Mov_Caja     As hRecord
    Dim lReg_Mov_Caja       As hFields
    Dim lReg_Caj            As hFields
    Dim iFila               As Integer
    Dim iNumRegistro        As Integer
    Dim iTotalRegistros     As Integer
    Dim nDecimalesCaja      As Integer
    Dim sCajaActual         As String
    Dim ultimosaldo         As Double
    Dim anterior            As Double
    
    Set lCajas_Ctas = New Class_Cajas_Cuenta
    With lCajas_Ctas
        .Campo("id_cuenta").Valor = oCliente.IdCuenta
        If .Buscar(True) Then
            Set lCursor_Caj = .Cursor
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
        End If
    End With
    Set lCajas_Ctas = Nothing
    
    With lLibro.ActiveSheet
        For Each lReg_Caj In lCursor_Caj
            nDecimalesCaja = lReg_Caj("Decimales").Value
'--------------------------------------------------------------------------------------------------------------------------
            gDB.Parametros.Clear
            gDB.Procedimiento = "Pkg_Saldos_Caja$Buscar_Saldos_Cuenta"
            gDB.Parametros.Add "PID_CAJA_CUENTA", ePT_Numero, lReg_Caj("id_caja_cuenta").Value, ePD_Entrada
            gDB.Parametros.Add "PID_CUENTA", ePT_Numero, oCliente.IdCuenta, ePD_Entrada
            gDB.Parametros.Add "PFecha_Cierre", ePT_Fecha, CDate(UltimoDiaDelMes(Dtp_FechaInicio.Value)) + 1, ePD_Entrada
            gDB.Parametros.Add "PSaldo", ePT_Numero, ultimosaldo, ePD_Salida
            
            If gDB.EjecutaSP Then
                ultimosaldo = gDB.Parametros("Psaldo").Valor
            End If
'--------------------------------------------------------------------------------------------------------------------------
            
'--------------------------------------------------------------------------------------------------------------------------
            gDB.Parametros.Clear
            gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
            gDB.Parametros.Add "Pidcajacuenta", ePT_Numero, lReg_Caj("id_caja_cuenta").Value, ePD_Entrada
            gDB.Parametros.Add "Pfecha_inicio", ePT_Fecha, CDate(UltimoDiaDelMes(Dtp_FechaInicio.Value)) + 1, ePD_Entrada
            gDB.Parametros.Add "Pfecha_termino", ePT_Fecha, Dtp_FechaTermino.Value, ePD_Entrada
            gDB.Procedimiento = "PKG_MOVIMIENTOS_CAJA_SECURITY"
            If Not gDB.EjecutaSP Then
                Exit Sub
            End If
            Set Cursor_Mov_Caja = gDB.Parametros("Pcursor").Valor
'----------------------------------------------------------------------------------------------------------------------------
            iTotalRegistros = Cursor_Mov_Caja.Count
            sCajaActual = NVL(lReg_Caj("dsc_caja_cuenta").Value, "Caja")
                
            'iLineasImpresas = iLineasImpresas + 1
            Call ImprimeEncabezadoDineroXLS(sCajaActual)
            
            iNumRegistro = 0
            If iTotalRegistros = 0 Then
                .Range("B" & iLineasImpresas + 1).Value = "No existen Movimientos en este periodo"
                .Range("B" & iLineasImpresas + 1 & ":E" & iLineasImpresas + 1).Merge
                iLineasImpresas = iLineasImpresas + 1
            Else
                .Cells(iLineasImpresas + 1, 2) = UltimoDiaDelMes(Dtp_FechaInicio.Value)
                .Cells(iLineasImpresas + 1, 3) = "SALDO ANTERIOR"
                .Cells(iLineasImpresas + 1, 4) = ""
                .Cells(iLineasImpresas + 1, 5) = ""
                .Cells(iLineasImpresas + 1, 6) = FormatNumber(ultimosaldo, nDecimalesCaja)
                .Cells(iLineasImpresas + 1, 7) = ""
                    
                .Range("B:B").HorizontalAlignment = xlCenter
                .Range("C:C").HorizontalAlignment = xlLeft
                .Range("D:D").HorizontalAlignment = xlRight
                .Range("E:E").HorizontalAlignment = xlRight
                .Range("F:F").HorizontalAlignment = xlRight
                .Range("G:G").HorizontalAlignment = xlCenter
                                
                For Each lReg_Mov_Caja In Cursor_Mov_Caja
                       
                    If anterior <> lReg_Mov_Caja("id_operacion").Value Or anterior = 0 Then
                        ultimosaldo = ultimosaldo + lReg_Mov_Caja("monto_movto_abono").Value - lReg_Mov_Caja("monto_movto_cargo").Value
                    End If
                
                    .Cells(iLineasImpresas + 1, 2) = Trim(lReg_Mov_Caja("fecha_liquidacion").Value)
                    .Cells(iLineasImpresas + 1, 3) = Trim(lReg_Mov_Caja("descripcion").Value)
                    If lReg_Mov_Caja("monto_movto_abono").Value > 0 Then
                        .Cells(iLineasImpresas + 1, 4) = FormatNumber(lReg_Mov_Caja("monto_movto_abono").Value, nDecimalesCaja)
                    End If
                    If lReg_Mov_Caja("monto_movto_cargo").Value > 0 Then
                        .Cells(iLineasImpresas + 1, 5) = FormatNumber(lReg_Mov_Caja("monto_movto_cargo").Value, nDecimalesCaja)
                    End If
                    .Cells(iLineasImpresas + 1, 6) = FormatNumber(ultimosaldo, nDecimalesCaja)
                    If lReg_Mov_Caja("id_operacion").Value <> 0 Then
                        .Cells(iLineasImpresas + 1, 7) = lReg_Mov_Caja("id_operacion").Value
                    Else
                        .Cells(iLineasImpresas + 1, 8) = "-"
                    End If

                    .Range("B:B").HorizontalAlignment = xlCenter
                    .Range("C:C").HorizontalAlignment = xlLeft
                    .Range("D:D").HorizontalAlignment = xlRight
                    .Range("F:F").HorizontalAlignment = xlRight
                    .Range("G:G").HorizontalAlignment = xlRight
                    .Range("H:H").HorizontalAlignment = xlCenter
                    anterior = lReg_Mov_Caja("id_operacion").Value
                    iLineasImpresas = iLineasImpresas + 1
                Next
            End If
        Next
            
    End With
End Sub

Private Sub ColocaPiePagina()
Dim sFooter As String
Dim vColorAnt As Variant
Dim i As Integer
    
    sFooter = ""

End Sub

Private Sub DefineSalida(ByRef vp As VsPrinter)
    
    If iTipoSalida = CONS_POR_IMPRESORA Then
        vp.PrintDoc
    Else
        Dim PathAndFile As String
        PathAndFile = oCliente.CreateTempFile("RegIndiv")
    End If
  
End Sub

Private Sub haz_linea(ByRef vp As VsPrinter, pY As Variant, Optional bColor As Boolean = False)
    vp.DrawLine 0, 0, 0, 0
    vp.DrawLine 0, 0, 0, 0
    vp.PenColor = RGB(100, 100, 100)
End Sub

Private Sub Grilla_Cuentas_Click()
    Dim i As Long
        With Grilla_Cuentas
            If .Col = Grilla_Cuentas.ColIndex("chk") Then
                For i = 1 To .Rows - 1
                    .Cell(flexcpChecked, i, .ColIndex("chk")) = flexUnchecked
                Next
                .Cell(flexcpChecked, .Row, .ColIndex("chk")) = flexChecked
            End If
        End With
End Sub

Private Sub Sub_Carga_Grilla()
Dim lcAdminCartera  As Class_AdministracionCartera
Dim lcursor         As Object
Dim lReg            As Object
Dim lIdMonedaSalida As Integer
Dim iTotalRegistros As Integer

    Set lcAdminCartera = New Class_AdministracionCartera
    With lcAdminCartera
        .Campo("Id_TipoCuenta").Valor = oCliente.IdTipoCuenta
        If .BuscarCuentas Then
            Set lcursor = .Cursor
            iTotalRegistros = lcursor.Count
        Else
            MsgBox "Problemas en cargar cuentas." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            Exit Sub
        End If
    End With
    Set lcAdminCartera = Nothing
    
    Grilla_Cuentas.Rows = 1
    Dim lLinea
    
    For Each lReg In lcursor
        lLinea = Grilla_Cuentas.Rows
        Call Grilla_Cuentas.AddItem("")
        Call SetCell(Grilla_Cuentas, lLinea, "colum_pk", Trim(lReg("id_cuenta").Value), pAutoSize:=False)
        Call SetCell(Grilla_Cuentas, lLinea, "moneda", lReg("id_moneda").Value, pAutoSize:=False)
        Call SetCell(Grilla_Cuentas, lLinea, "num_cuenta", lReg("num_cuenta").Value, pAutoSize:=False)
        Call SetCell(Grilla_Cuentas, lLinea, "Rut_cliente", lReg("rut_cliente").Value, pAutoSize:=True)
        Call SetCell(Grilla_Cuentas, lLinea, "razon_social", lReg("nombre_cliente").Value, pAutoSize:=False)
        Call SetCell(Grilla_Cuentas, lLinea, "numero_folio", NVL(lReg("numero_folio").Value, 0), pAutoSize:=False)
    Next
    
End Sub

Function FormatNumber(xValor As Variant, iDecimales As Integer, Optional bPorcentaje As Boolean = False) As String
    Dim xFormato        As String
    
    On Error GoTo PError
        
    If iDecimales = 0 Then
        xFormato = "###,###,##0"
    ElseIf bPorcentaje Then
        xFormato = "##0." & String(iDecimales - 1, "#") & "0"
    Else
        xFormato = "###,###,##0." & String(iDecimales - 1, "#") & "0"
    End If
    
    GoTo Fin
    
PError:
    xValor = "0"
    
Fin:
    FormatNumber = Format(xValor, xFormato)
    On Error GoTo 0
    
End Function


