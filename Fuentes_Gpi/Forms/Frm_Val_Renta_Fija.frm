VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{54850C51-14EA-4470-A5E4-8C5DB32DC853}#1.0#0"; "vsprint8.ocx"
Object = "{C8CF160E-7278-4354-8071-850013B36892}#1.0#0"; "vsrpt8.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Val_Renta_fija 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Valoraci�n Renta Fija"
   ClientHeight    =   7860
   ClientLeft      =   3285
   ClientTop       =   885
   ClientWidth     =   11070
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7860
   ScaleWidth      =   11070
   Begin VB.CommandButton Command4 
      Caption         =   "HTML"
      Height          =   585
      Left            =   9150
      TabIndex        =   23
      Top             =   9720
      Width           =   1395
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Archivo XML"
      Height          =   645
      Left            =   9150
      TabIndex        =   22
      Top             =   9030
      Width           =   1455
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Render 2"
      Height          =   525
      Left            =   9150
      TabIndex        =   21
      Top             =   8490
      Width           =   1425
   End
   Begin VSPrinter8LibCtl.VSPrinter vp 
      Height          =   2505
      Left            =   210
      TabIndex        =   20
      Top             =   7860
      Width           =   8655
      _cx             =   15266
      _cy             =   4419
      Appearance      =   1
      BorderStyle     =   1
      Enabled         =   -1  'True
      MousePointer    =   0
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AutoRTF         =   -1  'True
      Preview         =   -1  'True
      DefaultDevice   =   0   'False
      PhysicalPage    =   -1  'True
      AbortWindow     =   -1  'True
      AbortWindowPos  =   0
      AbortCaption    =   "Printing..."
      AbortTextButton =   "Cancel"
      AbortTextDevice =   "on the %s on %s"
      AbortTextPage   =   "Now printing Page %d of"
      FileName        =   ""
      MarginLeft      =   1440
      MarginTop       =   1440
      MarginRight     =   1440
      MarginBottom    =   1440
      MarginHeader    =   0
      MarginFooter    =   0
      IndentLeft      =   0
      IndentRight     =   0
      IndentFirst     =   0
      IndentTab       =   720
      SpaceBefore     =   0
      SpaceAfter      =   0
      LineSpacing     =   100
      Columns         =   1
      ColumnSpacing   =   180
      ShowGuides      =   2
      LargeChangeHorz =   300
      LargeChangeVert =   300
      SmallChangeHorz =   30
      SmallChangeVert =   30
      Track           =   0   'False
      ProportionalBars=   -1  'True
      Zoom            =   10.7007575757576
      ZoomMode        =   3
      ZoomMax         =   400
      ZoomMin         =   10
      ZoomStep        =   25
      EmptyColor      =   -2147483636
      TextColor       =   0
      HdrColor        =   0
      BrushColor      =   0
      BrushStyle      =   0
      PenColor        =   0
      PenStyle        =   0
      PenWidth        =   0
      PageBorder      =   0
      Header          =   ""
      Footer          =   ""
      TableSep        =   "|;"
      TableBorder     =   7
      TablePen        =   0
      TablePenLR      =   0
      TablePenTB      =   0
      NavBar          =   3
      NavBarColor     =   -2147483633
      ExportFormat    =   0
      URL             =   ""
      Navigation      =   3
      NavBarMenuText  =   "Whole &Page|Page &Width|&Two Pages|Thumb&nail"
      AutoLinkNavigate=   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   9
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Render 1"
      Height          =   525
      Left            =   9150
      TabIndex        =   19
      Top             =   7920
      Width           =   1425
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11070
      _ExtentX        =   19526
      _ExtentY        =   635
      ButtonWidth     =   1561
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Excel"
            Key             =   "EXCEL"
            Description     =   "Traspasa la grilla a Excel"
            Object.ToolTipText     =   "Traspasa la grilla a Excel"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Description     =   "Salir"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   5220
         TabIndex        =   25
         Top             =   360
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin TrueDBList80.TDBCombo TDBCombo1 
      Height          =   345
      Left            =   1380
      TabIndex        =   6
      Tag             =   "OBLI"
      Top             =   0
      Width           =   4065
      _ExtentX        =   7170
      _ExtentY        =   609
      _LayoutType     =   4
      _RowHeight      =   -2147483647
      _WasPersistedAsPixels=   0
      _DropdownWidth  =   0
      _EDITHEIGHT     =   609
      _GAPHEIGHT      =   53
      Columns(0)._VlistStyle=   0
      Columns(0)._MaxComboItems=   5
      Columns(0).Caption=   "Nemot�cnico"
      Columns(0).DataField=   ""
      Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
      Columns(1)._VlistStyle=   0
      Columns(1)._MaxComboItems=   5
      Columns(1).Caption=   "Descripci�n"
      Columns(1).DataField=   ""
      Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
      Columns.Count   =   2
      Splits(0)._UserFlags=   0
      Splits(0).ExtendRightColumn=   -1  'True
      Splits(0).AllowRowSizing=   0   'False
      Splits(0).AllowColSelect=   0   'False
      Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
      Splits(0)._ColumnProps(0)=   "Columns.Count=2"
      Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
      Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
      Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
      Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
      Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
      Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
      Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
      Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
      Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
      Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
      Splits.Count    =   1
      Appearance      =   1
      BorderStyle     =   1
      ComboStyle      =   0
      AutoCompletion  =   -1  'True
      LimitToList     =   0   'False
      ColumnHeaders   =   -1  'True
      ColumnFooters   =   0   'False
      DataMode        =   5
      DefColWidth     =   0
      Enabled         =   -1  'True
      HeadLines       =   1
      FootLines       =   1
      RowDividerStyle =   6
      Caption         =   ""
      EditFont        =   "Size=8.25,Charset=0,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
      LayoutName      =   ""
      LayoutFileName  =   ""
      MultipleLines   =   0
      EmptyRows       =   0   'False
      CellTips        =   2
      AutoSize        =   -1  'True
      ListField       =   ""
      BoundColumn     =   ""
      IntegralHeight  =   0   'False
      CellTipsWidth   =   0
      CellTipsDelay   =   1000
      AutoDropdown    =   0   'False
      RowTracking     =   -1  'True
      RightToLeft     =   0   'False
      MouseIcon       =   0
      MouseIcon.vt    =   3
      MousePointer    =   0
      MatchEntryTimeout=   2000
      OLEDragMode     =   0
      OLEDropMode     =   0
      AnimateWindow   =   3
      AnimateWindowDirection=   0
      AnimateWindowTime=   200
      AnimateWindowClose=   2
      DropdownPosition=   0
      Locked          =   0   'False
      ScrollTrack     =   0   'False
      ScrollTips      =   -1  'True
      RowDividerColor =   14933984
      RowSubDividerColor=   14933984
      AddItemSeparator=   ";"
      _PropDict       =   $"Frm_Val_Renta_Fija.frx":0000
      _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
      _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
      _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
      _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
      _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=0"
      _StyleDefs(5)   =   ":id=0,.fontname=MS Sans Serif"
      _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&,.bold=0,.fontsize=825"
      _StyleDefs(7)   =   ":id=1,.italic=0,.underline=0,.strikethrough=0,.charset=0"
      _StyleDefs(8)   =   ":id=1,.fontname=Arial"
      _StyleDefs(9)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
      _StyleDefs(10)  =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
      _StyleDefs(11)  =   "FooterStyle:id=3,.parent=1,.namedParent=35"
      _StyleDefs(12)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(13)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
      _StyleDefs(14)  =   "EditorStyle:id=7,.parent=1"
      _StyleDefs(15)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
      _StyleDefs(16)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
      _StyleDefs(17)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
      _StyleDefs(18)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
      _StyleDefs(19)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
      _StyleDefs(20)  =   "Splits(0).Style:id=13,.parent=1"
      _StyleDefs(21)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
      _StyleDefs(22)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
      _StyleDefs(23)  =   "Splits(0).FooterStyle:id=15,.parent=3"
      _StyleDefs(24)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
      _StyleDefs(25)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
      _StyleDefs(26)  =   "Splits(0).EditorStyle:id=17,.parent=7"
      _StyleDefs(27)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
      _StyleDefs(28)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
      _StyleDefs(29)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
      _StyleDefs(30)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
      _StyleDefs(31)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
      _StyleDefs(32)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
      _StyleDefs(33)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
      _StyleDefs(34)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
      _StyleDefs(35)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
      _StyleDefs(36)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
      _StyleDefs(37)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
      _StyleDefs(38)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
      _StyleDefs(39)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
      _StyleDefs(40)  =   "Named:id=33:Normal"
      _StyleDefs(41)  =   ":id=33,.parent=0"
      _StyleDefs(42)  =   "Named:id=34:Heading"
      _StyleDefs(43)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(44)  =   ":id=34,.wraptext=-1"
      _StyleDefs(45)  =   "Named:id=35:Footing"
      _StyleDefs(46)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
      _StyleDefs(47)  =   "Named:id=36:Selected"
      _StyleDefs(48)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
      _StyleDefs(49)  =   "Named:id=37:Caption"
      _StyleDefs(50)  =   ":id=37,.parent=34,.alignment=2"
      _StyleDefs(51)  =   "Named:id=38:HighlightRow"
      _StyleDefs(52)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
      _StyleDefs(53)  =   "Named:id=39:EvenRow"
      _StyleDefs(54)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
      _StyleDefs(55)  =   "Named:id=40:OddRow"
      _StyleDefs(56)  =   ":id=40,.parent=33"
      _StyleDefs(57)  =   "Named:id=41:RecordSelector"
      _StyleDefs(58)  =   ":id=41,.parent=34"
      _StyleDefs(59)  =   "Named:id=42:FilterBar"
      _StyleDefs(60)  =   ":id=42,.parent=33"
      _StyleDefs(61)  =   "Named:id=43:StyleCreasys"
      _StyleDefs(62)  =   ":id=43,.parent=33"
   End
   Begin VB.Frame Frame1 
      Height          =   7395
      Left            =   60
      TabIndex        =   1
      Top             =   360
      Width           =   10935
      Begin MSComCtl2.DTPicker dtp_Fecha_Actual 
         Height          =   375
         Left            =   1650
         TabIndex        =   17
         Top             =   210
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   661
         _Version        =   393216
         Format          =   65470465
         CurrentDate     =   38877
      End
      Begin MSComDlg.CommonDialog Dialogo 
         Left            =   1260
         Top             =   6930
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         DefaultExt      =   "XLS"
         DialogTitle     =   "Grabar Grilla"
         Filter          =   ".XLS"
      End
      Begin MSComctlLib.Toolbar Toolbar3 
         Height          =   600
         Left            =   9000
         TabIndex        =   5
         Top             =   4290
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   1058
         ButtonWidth     =   1217
         ButtonHeight    =   953
         Appearance      =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Agregar"
               Key             =   "ADD"
               Description     =   "Agrega a la grilla"
               Object.ToolTipText     =   "Agrega a la grilla"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Quitar"
               Key             =   "QUITA"
               Description     =   "Quita Fila de la grila"
               Object.ToolTipText     =   "Quita Fila de la grila"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.Toolbar Toolbar2 
         Height          =   600
         Left            =   9000
         TabIndex        =   15
         Top             =   3360
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   1058
         ButtonWidth     =   1191
         ButtonHeight    =   953
         Appearance      =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Calcular"
               Key             =   "CALC"
               Description     =   "Calcula Valor del Instrumento"
               Object.ToolTipText     =   "Calcula Valor del Instrumento"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Limpiar"
               Key             =   "LIMP"
               Description     =   "Limpia el Formulario (No la Grilla)"
               Object.ToolTipText     =   "Limpia el Formulario (No la Grilla)"
            EndProperty
         EndProperty
      End
      Begin VB.Frame Frame4 
         Caption         =   "Resultados"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   2145
         Left            =   90
         TabIndex        =   4
         Top             =   2970
         Width           =   8475
         Begin hControl2.hTextLabel txt_valor_par_UM 
            Height          =   345
            Left            =   210
            TabIndex        =   32
            Top             =   360
            Width           =   3825
            _ExtentX        =   6747
            _ExtentY        =   609
            LabelWidth      =   1500
            Caption         =   "Valor Par UM"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel txt_valor_par_pesos 
            Height          =   345
            Left            =   210
            TabIndex        =   33
            Top             =   780
            Width           =   3825
            _ExtentX        =   6747
            _ExtentY        =   609
            LabelWidth      =   1500
            Caption         =   "Valor Par Pesos"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel txt_porcentaje_valor_par 
            Height          =   345
            Left            =   210
            TabIndex        =   34
            Top             =   1200
            Width           =   3825
            _ExtentX        =   6747
            _ExtentY        =   609
            LabelWidth      =   1500
            Caption         =   "% Valor Par"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel txt_duration 
            Height          =   345
            Left            =   210
            TabIndex        =   35
            Top             =   1620
            Width           =   3825
            _ExtentX        =   6747
            _ExtentY        =   609
            LabelWidth      =   1500
            Caption         =   "Duration"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel txt_valor_presente_UM 
            Height          =   345
            Left            =   4350
            TabIndex        =   36
            Top             =   360
            Width           =   3825
            _ExtentX        =   6747
            _ExtentY        =   609
            LabelWidth      =   1650
            Caption         =   "Valor Presente UM"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel txt_valor_presente_pesos 
            Height          =   345
            Left            =   4350
            TabIndex        =   37
            Top             =   780
            Width           =   3825
            _ExtentX        =   6747
            _ExtentY        =   609
            LabelWidth      =   1650
            Caption         =   "Valor Presente Pesos"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel txt_TIR 
            Height          =   345
            Left            =   4350
            TabIndex        =   38
            Top             =   1200
            Width           =   3825
            _ExtentX        =   6747
            _ExtentY        =   609
            LabelWidth      =   1650
            Caption         =   "TIR"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel txt_maduracion 
            Height          =   345
            Left            =   4350
            TabIndex        =   39
            Top             =   1620
            Width           =   3825
            _ExtentX        =   6747
            _ExtentY        =   609
            LabelWidth      =   1650
            Caption         =   "Maduraci�n"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
      End
      Begin VB.Frame Frame3 
         Height          =   2415
         Left            =   7380
         TabIndex        =   3
         Top             =   570
         Width           =   3465
         Begin hControl2.hTextLabel txt_tasa_valoracion 
            Height          =   345
            Left            =   60
            TabIndex        =   26
            Tag             =   "OBLI"
            Top             =   660
            Width           =   3285
            _ExtentX        =   5794
            _ExtentY        =   609
            LabelWidth      =   1700
            Caption         =   "Tasa Valorizaci�n"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel txt_valor_nominal 
            Height          =   345
            Left            =   60
            TabIndex        =   27
            Tag             =   "OBLI"
            Top             =   1290
            Width           =   3285
            _ExtentX        =   5794
            _ExtentY        =   609
            LabelWidth      =   1700
            Caption         =   "Valor Nominal"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel txt_presente_pesos 
            Height          =   345
            Left            =   60
            TabIndex        =   28
            Tag             =   "OBLI"
            Top             =   1920
            Width           =   3285
            _ExtentX        =   5794
            _ExtentY        =   609
            LabelWidth      =   1700
            Caption         =   "Valor Presente Pesos"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
      End
      Begin VB.Frame Frame2 
         Height          =   2415
         Left            =   90
         TabIndex        =   2
         Top             =   570
         Width           =   7245
         Begin MSComCtl2.DTPicker dtp_fecha_emision 
            Height          =   345
            Left            =   5580
            TabIndex        =   18
            Top             =   1080
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   609
            _Version        =   393216
            Format          =   65470465
            CurrentDate     =   38877
         End
         Begin MSComCtl2.DTPicker dtp_Fecha_Prox_Vcto 
            Height          =   345
            Left            =   5580
            TabIndex        =   24
            Top             =   1920
            Width           =   1605
            _ExtentX        =   2831
            _ExtentY        =   609
            _Version        =   393216
            Format          =   65470465
            CurrentDate     =   38877
         End
         Begin hControl2.hTextLabel txt_tasa_emision 
            Height          =   345
            Left            =   4050
            TabIndex        =   29
            Top             =   660
            Width           =   3105
            _ExtentX        =   5477
            _ExtentY        =   609
            LabelWidth      =   1500
            Caption         =   "Tasa Emisi�n"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel txt_cupon_vigente 
            Height          =   345
            Left            =   4050
            TabIndex        =   30
            Tag             =   "OBLI"
            Top             =   1500
            Width           =   3105
            _ExtentX        =   5477
            _ExtentY        =   609
            LabelWidth      =   1500
            Caption         =   "Cup�n Vigente"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,##0"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel txt_moneda_um 
            Height          =   345
            Left            =   90
            TabIndex        =   31
            Top             =   1920
            Width           =   3885
            _ExtentX        =   6853
            _ExtentY        =   609
            LabelWidth      =   1365
            Caption         =   "Moneda UM"
            Text            =   ""
            Locked          =   -1  'True
            BackColorTxt    =   12648447
            BackColorTxt    =   12648447
            Format          =   "#,###.##"
         End
         Begin TrueDBList80.TDBCombo Cmb_Nemotecnico 
            Height          =   345
            Left            =   1500
            TabIndex        =   41
            Tag             =   "OBLI=S;CAPTION=Nemot�cnico"
            Top             =   240
            Width           =   3975
            _ExtentX        =   7011
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   -1  'True
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Val_Renta_Fija.frx":00AA
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Emisor 
            Height          =   345
            Left            =   1500
            TabIndex        =   42
            Top             =   660
            Width           =   2505
            _ExtentX        =   4419
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   -1  'True
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Val_Renta_Fija.frx":0154
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Instrumento 
            Height          =   345
            Left            =   1500
            TabIndex        =   43
            Top             =   1080
            Width           =   2505
            _ExtentX        =   4419
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   -1  'True
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Val_Renta_Fija.frx":01FE
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Serie 
            Height          =   345
            Left            =   1500
            TabIndex        =   44
            Top             =   1500
            Width           =   2505
            _ExtentX        =   4419
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   -1  'True
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Val_Renta_Fija.frx":02A8
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label Label18 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Pr�x. Vcto."
            Height          =   345
            Left            =   4050
            TabIndex        =   14
            Top             =   1920
            Width           =   1500
         End
         Begin VB.Label Label16 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Emisi�n"
            Height          =   345
            Left            =   4050
            TabIndex        =   13
            Top             =   1080
            Width           =   1500
         End
         Begin VB.Label Label5 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Instrumento"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   105
            TabIndex        =   11
            Top             =   1080
            Width           =   1365
         End
         Begin VB.Label Label4 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Serie"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   105
            TabIndex        =   10
            Top             =   1500
            Width           =   1365
         End
         Begin VB.Label Label3 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Emisor"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   105
            TabIndex        =   9
            Top             =   660
            Width           =   1365
         End
         Begin VB.Label Label2 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Nemot�cnico"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   105
            TabIndex        =   8
            Top             =   240
            Width           =   1365
         End
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   1665
         Left            =   90
         TabIndex        =   16
         Top             =   5190
         Width           =   10725
         _cx             =   18918
         _cy             =   2937
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   8
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Val_Renta_Fija.frx":0352
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin hControl2.hTextLabel txt_total 
         Height          =   345
         Left            =   7680
         TabIndex        =   40
         Top             =   6930
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   609
         LabelWidth      =   705
         Caption         =   "Total"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin VB.Label Label6 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha de C�lculo"
         Height          =   345
         Left            =   90
         TabIndex        =   12
         Top             =   210
         Width           =   1485
      End
   End
   Begin VSReport8LibCtl.VSReport VSReport1 
      Left            =   8940
      Top             =   9750
      _rv             =   800
      ReportName      =   "Prueba"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      OnOpen          =   ""
      OnClose         =   ""
      OnNoData        =   ""
      OnPage          =   ""
      OnError         =   ""
      MaxPages        =   0
      DoEvents        =   -1  'True
      BeginProperty Layout {D853A4F1-D032-4508-909F-18F074BD547A} 
         Width           =   9024
         MarginLeft      =   1440
         MarginTop       =   1440
         MarginRight     =   1440
         MarginBottom    =   1440
         Columns         =   1
         ColumnLayout    =   0
         Orientation     =   1
         PageHeader      =   0
         PageFooter      =   0
         PictureAlign    =   7
         PictureShow     =   1
         PaperSize       =   0
      EndProperty
      BeginProperty DataSource {D1359088-0913-44EA-AE50-6A7CD77D4C50} 
         ConnectionString=   ""
         RecordSource    =   ""
         Filter          =   ""
         MaxRecords      =   0
      EndProperty
      GroupCount      =   0
      SectionCount    =   5
      BeginProperty Section0 {673CB92F-28D3-421F-86CD-1099425A5037} 
         Name            =   "Detail"
         Visible         =   -1  'True
         Height          =   500
         CanGrow         =   -1  'True
         CanShrink       =   0   'False
         KeepTogether    =   -1  'True
         ForcePageBreak  =   0
         BackColor       =   16777215
         Repeat          =   0   'False
         OnFormat        =   ""
         OnPrint         =   ""
         Object.Tag             =   ""
      EndProperty
      BeginProperty Section1 {673CB92F-28D3-421F-86CD-1099425A5037} 
         Name            =   "Header"
         Visible         =   -1  'True
         Height          =   1000
         CanGrow         =   -1  'True
         CanShrink       =   0   'False
         KeepTogether    =   -1  'True
         ForcePageBreak  =   0
         BackColor       =   16777215
         Repeat          =   0   'False
         OnFormat        =   ""
         OnPrint         =   ""
         Object.Tag             =   ""
      EndProperty
      BeginProperty Section2 {673CB92F-28D3-421F-86CD-1099425A5037} 
         Name            =   "Footer"
         Visible         =   0   'False
         Height          =   0
         CanGrow         =   -1  'True
         CanShrink       =   0   'False
         KeepTogether    =   -1  'True
         ForcePageBreak  =   0
         BackColor       =   16777215
         Repeat          =   0   'False
         OnFormat        =   ""
         OnPrint         =   ""
         Object.Tag             =   ""
      EndProperty
      BeginProperty Section3 {673CB92F-28D3-421F-86CD-1099425A5037} 
         Name            =   "Page Header"
         Visible         =   -1  'True
         Height          =   600
         CanGrow         =   -1  'True
         CanShrink       =   0   'False
         KeepTogether    =   -1  'True
         ForcePageBreak  =   0
         BackColor       =   16777215
         Repeat          =   0   'False
         OnFormat        =   ""
         OnPrint         =   ""
         Object.Tag             =   ""
      EndProperty
      BeginProperty Section4 {673CB92F-28D3-421F-86CD-1099425A5037} 
         Name            =   "Page Footer"
         Visible         =   -1  'True
         Height          =   500
         CanGrow         =   -1  'True
         CanShrink       =   0   'False
         KeepTogether    =   -1  'True
         ForcePageBreak  =   0
         BackColor       =   16777215
         Repeat          =   0   'False
         OnFormat        =   ""
         OnPrint         =   ""
         Object.Tag             =   ""
      EndProperty
      FieldCount      =   4
      BeginProperty Field0 {6AC1BBA5-107E-4F07-BCF0-DF757735D0A8} 
         Name            =   "TitleLbl"
         Text            =   "Prueba"
         Object.Left            =   0
         Object.Top             =   200
         Width           =   9024
         Height          =   600
         BackColor       =   16777215
         ForeColor       =   128
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   20.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Align           =   6
         BackStyle       =   0
         BorderColor     =   0
         BorderStyle     =   0
         CanGrow         =   0   'False
         CanShrink       =   0   'False
         Visible         =   -1  'True
         HideDuplicates  =   0   'False
         RunningSum      =   0
         Format          =   ""
         LineSlant       =   0
         LineWidth       =   0
         PictureAlign    =   0
         MarginLeft      =   0
         MarginTop       =   0
         MarginRight     =   0
         MarginBottom    =   0
         Section         =   1
         ForcePageBreak  =   0
         Calculated      =   0   'False
         WordWrap        =   -1  'True
         LineSpacing     =   0
         CheckBox        =   0
         RTF             =   0   'False
         Anchor          =   0
         ZOrder          =   0
         LinkTarget      =   ""
         Object.Tag             =   ""
      EndProperty
      BeginProperty Field1 {6AC1BBA5-107E-4F07-BCF0-DF757735D0A8} 
         Name            =   "FooterLeft"
         Text            =   "Now()"
         Object.Left            =   0
         Object.Top             =   30
         Width           =   4512
         Height          =   300
         BackColor       =   16777215
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Align           =   0
         BackStyle       =   0
         BorderColor     =   0
         BorderStyle     =   0
         CanGrow         =   0   'False
         CanShrink       =   0   'False
         Visible         =   -1  'True
         HideDuplicates  =   0   'False
         RunningSum      =   0
         Format          =   ""
         LineSlant       =   0
         LineWidth       =   0
         PictureAlign    =   0
         MarginLeft      =   0
         MarginTop       =   0
         MarginRight     =   0
         MarginBottom    =   0
         Section         =   4
         ForcePageBreak  =   0
         Calculated      =   -1  'True
         WordWrap        =   -1  'True
         LineSpacing     =   0
         CheckBox        =   0
         RTF             =   0   'False
         Anchor          =   0
         ZOrder          =   0
         LinkTarget      =   ""
         Object.Tag             =   ""
      EndProperty
      BeginProperty Field2 {6AC1BBA5-107E-4F07-BCF0-DF757735D0A8} 
         Name            =   "FooterRight"
         Text            =   """Page "" & [Page] & "" of "" & [Pages]"
         Object.Left            =   4512
         Object.Top             =   30
         Width           =   4512
         Height          =   300
         BackColor       =   16777215
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Align           =   2
         BackStyle       =   0
         BorderColor     =   0
         BorderStyle     =   0
         CanGrow         =   0   'False
         CanShrink       =   0   'False
         Visible         =   -1  'True
         HideDuplicates  =   0   'False
         RunningSum      =   0
         Format          =   ""
         LineSlant       =   0
         LineWidth       =   0
         PictureAlign    =   0
         MarginLeft      =   0
         MarginTop       =   0
         MarginRight     =   0
         MarginBottom    =   0
         Section         =   4
         ForcePageBreak  =   0
         Calculated      =   -1  'True
         WordWrap        =   -1  'True
         LineSpacing     =   0
         CheckBox        =   0
         RTF             =   0   'False
         Anchor          =   0
         ZOrder          =   0
         LinkTarget      =   ""
         Object.Tag             =   ""
      EndProperty
      BeginProperty Field3 {6AC1BBA5-107E-4F07-BCF0-DF757735D0A8} 
         Name            =   "DivLine1"
         Text            =   ""
         Object.Left            =   0
         Object.Top             =   570
         Width           =   9024
         Height          =   30
         BackColor       =   0
         ForeColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Align           =   -1
         BackStyle       =   1
         BorderColor     =   0
         BorderStyle     =   0
         CanGrow         =   0   'False
         CanShrink       =   0   'False
         Visible         =   -1  'True
         HideDuplicates  =   0   'False
         RunningSum      =   0
         Format          =   ""
         LineSlant       =   0
         LineWidth       =   0
         PictureAlign    =   0
         MarginLeft      =   0
         MarginTop       =   0
         MarginRight     =   0
         MarginBottom    =   0
         Section         =   3
         ForcePageBreak  =   0
         Calculated      =   0   'False
         WordWrap        =   -1  'True
         LineSpacing     =   0
         CheckBox        =   0
         RTF             =   0   'False
         Anchor          =   0
         ZOrder          =   0
         LinkTarget      =   ""
         Object.Tag             =   ""
      EndProperty
   End
   Begin VB.Label Label1 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Nemot�nico"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   1365
   End
End
Attribute VB_Name = "Frm_Val_Renta_fija"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fSalir As Boolean

Private Enum eNem_Colum
  eNem_nemotecnico
  eNem_Descripcion
  eNem_Id_Nemotecnico
End Enum

Private Function ValidaAgregar() As Boolean
  If Not Trim(Cmb_Emisor.Text) = "" And _
     Not Trim(Cmb_Instrumento.Text) = "" And _
     Not Trim(Cmb_Serie.Text) = "" And _
     Not Trim(txt_valor_nominal.Text) = "" And _
     Not Trim(txt_tasa_valoracion.Text) = "" And _
     Not Trim(txt_presente_pesos.Text) = "" Then
     
     ValidaAgregar = True
  Else
     ValidaAgregar = False
  End If
End Function

Sub RenderRecordset(vp As VsPrinter, rs As hRecord, ByVal maxh As Double)
   Dim arr, i%, j%, wid!
   Dim Campo As hFields
      
   Const hdr = "Nemot�cnico|Descripci�n|C�digo"
   Const fmt = "2000|7000|>1000"
    
   i = rs.Count
   If i = 0 Then Exit Sub
   
   vp.StartTable
   
   For Each Campo In rs
      'sRecord = Campo("nemotecnico").Value & "|" & Campo("dsc_nemotecnico").Value & "|" & Campo("id_nemotecnico").Value
      vp.AddTable fmt, hdr, Campo("nemotecnico").Value & "|" & Campo("dsc_nemotecnico").Value & "|" & Campo("id_nemotecnico").Value, , , True
   Next
    
   vp.TableCell(tcFontBold, 0) = True
   vp.TableCell(tcBackColor, 0) = vbYellow
   vp.TableCell(tcRowHeight, 0) = vp.TextHeight("Test") * 1.1
   vp.TableCell(tcAlign, 0) = taLeftMiddle
    
   For i = 1 To vp.TableCell(tcCols)
      wid = wid + vp.TableCell(tcColWidth, , i)
   Next
   vp.GetMargins
   If wid > vp.X2 - vp.X1 Then
      wid = (vp.X2 - vp.X1) / wid * 0.95
      For i = 1 To vp.TableCell(tcCols)
         vp.TableCell(tcColWidth, , i) = wid * vp.TableCell(tcColWidth, , i)
      Next
   End If
    
   If maxh > 0 Then
      For i = 1 To vp.TableCell(tcRows)
         If vp.TableCell(tcRowHeight, i) > maxh Then
            vp.TableCell(tcRowHeight, i) = maxh
         End If
      Next
   End If
   
   vp.EndTable
End Sub

Private Sub Calcular()
   Dim p_VparMonEmi As Double
   Dim cod_Ret As Integer
   Dim p_VparMonLoc As Double
   Dim p_VPteMonEmi As Double
   Dim p_VPteMonLoc As Double
   Dim p_durat As Double
   Dim p_madur As Double
   Dim p_PorcValPar As Double
   Dim p_tas_cal As Double
   
   If Not txt_tasa_valoracion.Text = "" Then 'Ejecutar Package con tasa de valoraci�n
      gDB.Parametros.Clear
      gDB.Procedimiento = "valrf.Pkg_Valora_Rf.sp_valseriado"
      
      gDB.Parametros.Add "p_fecha_emision", ePT_Fecha, dtp_Fecha_Emision.Value, ePD_Entrada
      gDB.Parametros.Add "p_fecha_calculo", ePT_Fecha, dtp_Fecha_Actual.Value, ePD_Entrada
      gDB.Parametros.Add "p_tasa_val", ePT_Numero, txt_tasa_valoracion.Text, ePD_Entrada
      gDB.Parametros.Add "p_valnom", ePT_Numero, txt_valor_nominal.Text, ePD_Entrada
      gDB.Parametros.Add "p_codigo_emi", ePT_Caracter, Cmb_Emisor.Tag, ePD_Entrada
      gDB.Parametros.Add "p_codigo_ins", ePT_Caracter, Cmb_Instrumento.Tag, ePD_Entrada
      gDB.Parametros.Add "p_codigo_serie", ePT_Caracter, Cmb_Serie.Text, ePD_Entrada
      gDB.Parametros.Add "p_cuponvig", ePT_Numero, txt_cupon_vigente.Text, ePD_Entrada
      
      gDB.Parametros.Add "p_VparMonEmi", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_VparMonLoc", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_VPteMonEmi", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_VPteMonLoc", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_durat", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_madur", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_PorcValPar", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_cod_ret", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_men_err", ePT_Caracter, "", ePD_Salida
      
      If gDB.EjecutaSP Then
         If (gDB.Parametros("p_cod_ret").Valor = 0) Then
             p_VparMonEmi = gDB.Parametros("p_VparMonEmi").Valor
             p_VparMonLoc = gDB.Parametros("p_VparMonLoc").Valor
             p_VPteMonEmi = gDB.Parametros("p_VPteMonEmi").Valor
             p_VPteMonLoc = gDB.Parametros("p_VPteMonLoc").Valor
             p_durat = CDbl(Replace(gDB.Parametros("p_durat").Valor, ",", "."))
             p_madur = gDB.Parametros("p_madur").Valor
             p_PorcValPar = gDB.Parametros("p_PorcValPar").Valor
             'MsgBox p_VparMonEmi & " " & p_VparMonLoc & " " & p_VPteMonEmi & " " & p_VPteMonLoc & " " & p_durat & " " & p_madur & " " & p_PorcValPar
             
             txt_valor_par_UM.Text = Format(p_VparMonEmi, "###,###,###.##")
             txt_valor_par_pesos.Text = Format(p_VparMonLoc, "###,###,###.##")
             txt_valor_presente_UM.Text = Format(p_VPteMonEmi, "###,###,###.##")
             txt_valor_presente_pesos.Text = Format(p_VPteMonLoc, "###,###,###.##")
             txt_duration.Text = Format(p_durat, "###,###,###.##")
             txt_maduracion.Text = Format(p_madur, "###,###,###.##")
             txt_porcentaje_valor_par.Text = Format(p_PorcValPar, "###,###,###.##")
             txt_TIR.Text = Format(txt_tasa_valoracion.Text, "###,###,###.##")
             
             
         Else
            MsgBox "Package " & gDB.Procedimiento & " : " & gDB.Parametros("p_men_err").Valor & vbCrLf & gDB.ErrMsg, vbCritical + vbExclamation + vbOKOnly, Me.Caption
            
         End If
      Else
         MsgBox "No se Ejecuto Package " & gDB.Procedimiento & " : " & vbCrLf & " Pkg_Func_Nemo.Sp_Desc_Nemo " & gDB.ErrMsg, vbCritical + vbOKOnly, Me.Caption
      End If
   Else
      If Not txt_presente_pesos.Text = "" Then
         'Ejecutar Package con valor presente pesos
         gDB.Parametros.Clear
         gDB.Procedimiento = "valrf.Pkg_Tir_Rf.sp_tirseriado"
         gDB.Parametros.Add "p_fec_cal", ePT_Fecha, dtp_Fecha_Actual.Value, ePD_Entrada
         gDB.Parametros.Add "p_fec_emi", ePT_Fecha, dtp_Fecha_Emision.Value, ePD_Entrada
         gDB.Parametros.Add "p_val_pte", ePT_Numero, Format(txt_presente_pesos.Text, ""), ePD_Entrada
         gDB.Parametros.Add "p_val_nom", ePT_Numero, txt_valor_nominal.Text, ePD_Entrada
         gDB.Parametros.Add "p_cod_emi", ePT_Caracter, Cmb_Emisor.Tag, ePD_Entrada
         gDB.Parametros.Add "p_cod_ins", ePT_Caracter, Cmb_Instrumento.Tag, ePD_Entrada
         gDB.Parametros.Add "p_cod_ser", ePT_Caracter, Cmb_Serie.Text, ePD_Entrada
         gDB.Parametros.Add "p_cup_vig", ePT_Numero, txt_cupon_vigente.Text, ePD_Entrada
         
         gDB.Parametros.Add "p_tas_cal", ePT_Numero, "", ePD_Salida
         gDB.Parametros.Add "p_cod_ret", ePT_Numero, "", ePD_Salida
         gDB.Parametros.Add "p_men_err", ePT_Caracter, "", ePD_Salida
         
         If gDB.EjecutaSP Then
            If (gDB.Parametros("p_cod_ret").Valor = 0) Then
               p_tas_cal = gDB.Parametros("p_tas_cal").Valor
               txt_tasa_valoracion.Text = Format(p_tas_cal, "###,###,###.##")
               txt_TIR.Text = Format(p_tas_cal, "###,###,###.##")
            Else
               MsgBox "Package " & gDB.Procedimiento & " : " & gDB.Parametros("p_men_err").Valor, vbCritical + vbExclamation + vbOKOnly, Me.Caption
            End If
         Else
            MsgBox "No se Ejecuto Package " & gDB.Procedimiento & " : " & vbCrLf & " Pkg_Func_Nemo.Sp_Desc_Nemo " & gDB.ErrMsg, vbCritical + vbOKOnly, Me.Caption
         End If
      Else
         MsgBox "Debe Ingresar la tasa de valoraci�n � Valor Presente en Pesos", vbInformation + vbOKOnly, Me.Caption
         txt_tasa_valoracion.SetFocus
      End If
   End If
End Sub

Private Sub CargaDatos()
Dim Campo As hFields
   
   Load Me
   
   dtp_Fecha_Actual.Value = Format(Fnt_FechaServidor, cFormatDate)
   dtp_Fecha_Prox_Vcto.Value = Format(Fnt_FechaServidor, cFormatDate)
   dtp_Fecha_Emision.Value = Format(Fnt_FechaServidor, cFormatDate)
   '------------------------------------------------
   '-- Carga los nemotecnicos
   '------------------------------------------------
   Call Sub_LimpiarTDBCombo(Cmb_Nemotecnico)
   With Cmb_Nemotecnico
      With .Columns.Add(eNem_nemotecnico)
         .Caption = "Nemot�cnico"
         .Visible = True
         
      End With
      With .Columns.Add(eNem_Descripcion)
         .Caption = "Descripci�n"
         .Visible = True
      End With
      With .Columns.Add(eNem_Id_Nemotecnico)
         .Caption = "id_nemotecnico"
         .Visible = False
      End With
       
      gDB.Parametros.Clear
      gDB.Procedimiento = "PKG_RENTA_FIJA.Buscar_Rf_Nemotecnico"
      gDB.Parametros.Add "Tipo", ePT_Caracter, gcPROD_RF_NAC, ePD_Entrada
      gDB.Parametros.Add "Nemo", ePT_Caracter, "%", ePD_Entrada
      gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
      If gDB.EjecutaSP Then
         For Each Campo In gDB.Parametros("Pcursor").Valor
            Call Cmb_Nemotecnico.AddItem(Campo("nemotecnico").Value & ";" & Campo("dsc_nemotecnico").Value & ";" & Campo("id_nemotecnico").Value)
         Next
      End If
   End With
   '------------------------------------------------
   'llena Combo emisores
   Cmb_Emisor.Clear
   gDB.Parametros.Clear
   gDB.Tabla = "valrf.emisorrf"
   gDB.OrderBy = "dsc_emisor"
   If gDB.EjecutaSelect("") Then
      For Each Campo In gDB.Parametros("Cursor").Valor
         Cmb_Emisor.AddItem Campo("dsc_emisor") & ";" & Campo("cod_emisor")
      Next
   End If
   
   'llena Combo instrumento
   Cmb_Instrumento.Clear
   gDB.Parametros.Clear
   gDB.Tabla = "valrf.Instrumentorf"
   gDB.OrderBy = "dsc_instrumento"
   If gDB.EjecutaSelect("") Then
      For Each Campo In gDB.Parametros("Cursor").Valor
         Cmb_Instrumento.AddItem Campo("dsc_instrumento") & ";" & Campo("cod_instrumento")
      Next
   End If
End Sub

Function DateCmp(fecha_calculo As Date, fecha_emision As Date) As Boolean
   ' retorna se la segunda fecha es anterior a la primera TRUE
   ' y FALSE en caso de la primera ser anterior o igual a la segunda
   If (fecha_calculo <= fecha_emision) Then
      DateCmp = True
   Else
      DateCmp = False
   End If
End Function

Function funExistenDatos() As Boolean
   Dim obj As Control
   
   funExistenDatos = False
   For Each obj In Me.Controls
      If TypeOf obj Is TextBox Or TypeOf obj Is ComboBox Then
         If Not obj.Name = "txt_Fecha_Actual" Then
            If Not obj.Text = "" Then
               funExistenDatos = True
            End If
         End If
      End If
   Next
End Function

Private Sub Limpiar_Form()
   Dim obj As Control
   
   For Each obj In Me.Controls
      If TypeOf obj Is hcontrol2.hTextLabel Then
         obj.Text = ""
         obj.Tag = ""
      End If
   Next
   
   Cmb_Nemotecnico.SetFocus
End Sub

Private Sub Cmb_Emisor_Click()
   Cmb_Emisor.Text = Cmb_Emisor.Text
   Cmb_Emisor.Tag = Cmb_Emisor.Columns(1).Text
End Sub

Private Sub Cmb_Emisor_KeyDown(KeyCode As Integer, Shift As Integer)
   If (KeyCode = vbKeyReturn) Then
      SendKeys "{TAB}"
   End If
End Sub

Private Sub Cmb_Instrumento_KeyDown(KeyCode As Integer, Shift As Integer)
   If (KeyCode = vbKeyReturn) Then
      SendKeys "{TAB}"
   End If
End Sub

Private Sub Cmb_Instrumento_LostFocus()
   Cmb_Instrumento.Text = Cmb_Instrumento.Text
   Cmb_Instrumento.Tag = Cmb_Instrumento.Columns(1).Text
End Sub

Private Sub Cmb_Nemotecnico_Change()
   If Len(Cmb_Nemotecnico.Text) = 0 Then
      Cmb_Emisor.Enabled = True
      Cmb_Instrumento.Enabled = True
      Cmb_Serie.Enabled = True
   Else
      Cmb_Emisor.Enabled = False
      Cmb_Instrumento.Enabled = False
      Cmb_Serie.Enabled = False
   End If
End Sub

Private Sub Cmb_Nemotecnico_KeyUp(KeyCode As Integer, Shift As Integer)
   If (KeyCode = vbKeyReturn) Then
      SendKeys "{TAB}"
   Else
      If Len(Cmb_Nemotecnico.Text) = 0 Then
         Cmb_Emisor.Enabled = True
         Cmb_Instrumento.Enabled = True
         Cmb_Serie.Enabled = True
      Else
         Cmb_Emisor.Enabled = False
         Cmb_Instrumento.Enabled = False
         Cmb_Serie.Enabled = False
      End If
      Limpiar_Form
   End If
End Sub

Private Sub Cmb_Nemotecnico_LostFocus()
   Dim lId_Nemotecnico As String
   Dim Campo As hFields
   Dim ID_Emisor_Especifico As Variant
   Dim Cod_Emisor_Especifico As Variant
   Dim Cod_Instrumento As String
   Dim Dsc_Instrumento As String
   Dim Dsc_Emisor As String
   
   Dim cod_Serie As String
   Dim cod_Moneda As String
   Dim Tasa_Emision As Double
   Dim fecha_emision As Date
   Dim Cupon_Vigente As Integer
   Dim Fecha_Prox_Venc As Date
   
   On Error GoTo errhand
      
   If Not Cmb_Nemotecnico.Text = "" Then
      lId_Nemotecnico = Cmb_Nemotecnico.Columns(eNem_Id_Nemotecnico).Text
      gDB.Parametros.Clear
      gDB.Procedimiento = "PKG_RENTA_FIJA.Buscar_Rf_Nemotecnico"
      gDB.Parametros.Add "Tipo", ePT_Caracter, gcPROD_RF_NAC, ePD_Entrada
      gDB.Parametros.Add "Nemo", ePT_Caracter, lId_Nemotecnico, ePD_Entrada
      gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
      If gDB.EjecutaSP Then
         For Each Campo In gDB.Parametros("Pcursor").Valor
            Cmb_Emisor.Text = Campo("dsc_emisor").Value
            Cmb_Emisor.Tag = Campo("cod_emisor_especifico").Value
            Cmb_Instrumento.Text = Campo("dsc_instrumento").Value
            Cmb_Instrumento.Tag = Campo("cod_instrumento_rf").Value
         Next
      Else
         MsgBox gDB.ErrMsg, vbCritical + vbOKOnly, Me.Caption
      End If
      
      gDB.Parametros.Clear
      gDB.Procedimiento = "valrf.Pkg_Func_Nemo.Sp_Desc_Nemo"
      gDB.Parametros.Add "p_nemotec", ePT_Caracter, Cmb_Nemotecnico.Text, ePD_Entrada
      gDB.Parametros.Add "p_fec_cal", ePT_Fecha, Fnt_String2Date(dtp_Fecha_Actual.Value), ePD_Entrada
      gDB.Parametros.Add "p_cod_emi", ePT_Caracter, Cmb_Emisor.Tag, ePD_Entrada
      gDB.Parametros.Add "p_cod_ins", ePT_Caracter, Cmb_Instrumento.Tag, ePD_Entrada
      
      gDB.Parametros.Add "p_cod_ser", ePT_Caracter, "", ePD_Salida
      gDB.Parametros.Add "p_cod_mon", ePT_Caracter, "", ePD_Salida
      gDB.Parametros.Add "p_tas_emi", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_fec_emi", ePT_Fecha, "", ePD_Salida
      gDB.Parametros.Add "p_cup_vig", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_fec_ven", ePT_Fecha, "", ePD_Salida
      gDB.Parametros.Add "p_cod_ret", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_men_err", ePT_Caracter, "", ePD_Salida
      If gDB.EjecutaSP Then
         If (gDB.Parametros("p_cod_ret").Valor = 0) Then
            cod_Serie = gDB.Parametros("p_cod_ser").Valor
            cod_Moneda = gDB.Parametros("p_cod_mon").Valor
            Tasa_Emision = gDB.Parametros("p_tas_emi").Valor
            fecha_emision = gDB.Parametros("p_fec_emi").Valor
            Cupon_Vigente = gDB.Parametros("p_cup_vig").Valor
            Fecha_Prox_Venc = gDB.Parametros("p_fec_ven").Valor
            
            Cmb_Serie.Text = cod_Serie
            txt_moneda_um.Text = cod_Moneda
            txt_tasa_emision.Text = Tasa_Emision
            dtp_Fecha_Emision.Value = fecha_emision
            txt_cupon_vigente.Text = Cupon_Vigente
            dtp_Fecha_Prox_Vcto.Value = Fecha_Prox_Venc
            
            If DateCmp(dtp_Fecha_Actual.Value, dtp_Fecha_Emision.Value) Then
               MsgBox "Fecha Actual debe ser Posterior a fecha de Emisi�n", vbCritical + vbOKOnly, Me.Caption
               dtp_Fecha_Actual.SetFocus
            Else
               txt_tasa_valoracion.SetFocus
            End If
         Else
            MsgBox "Package " & gDB.Procedimiento & " : " & gDB.Parametros("p_men_err").Valor, vbCritical + vbExclamation + vbOKOnly, Me.Caption
         End If
      Else
         MsgBox "No se Ejecuto Package " & gDB.Procedimiento & " : " & vbCrLf & " Pkg_Func_Nemo.Sp_Desc_Nemo " & gDB.ErrMsg, vbCritical + vbOKOnly, Me.Caption
      End If
      gDB.Parametros.Clear
   End If
      
   Exit Sub

errhand:
   MsgBox "ERROR en la Aplicaci�n " & Me.Caption & gDB.ErrMsg
End Sub

Private Sub Cmb_Serie_GotFocus()
   Dim Campo As hFields
   
   Cmb_Serie.Clear
   
   'llena Combo instrumento
   gDB.Parametros.Clear
   gDB.Tabla = "valrf.seriesrf"
   gDB.OrderBy = "cod_serie"
   gDB.Parametros.Add "COD_EMISOR", ePT_Caracter, Cmb_Emisor.Tag, ePD_Entrada
   gDB.Parametros.Add "COD_INSTRUMENTO", ePT_Caracter, Cmb_Instrumento.Tag, ePD_Entrada
   If gDB.EjecutaSelect("") Then
      For Each Campo In gDB.Parametros("Cursor").Valor
         Cmb_Serie.AddItem Campo("cod_serie")
      Next
   End If
End Sub

Private Sub Cmb_Serie_KeyDown(KeyCode As Integer, Shift As Integer)
   If (KeyCode = vbKeyReturn) Then
      SendKeys "{TAB}"
   End If
End Sub

Private Sub Cmb_Serie_LostFocus()
   Dim Campo As hFields
      
   If Not Trim(Cmb_Serie.Text) = "" Then
      gDB.Parametros.Clear
      gDB.Tabla = "valrf.seriesrf"
      gDB.Parametros.Add "COD_EMISOR", ePT_Caracter, Cmb_Emisor.Tag, ePD_Entrada
      gDB.Parametros.Add "COD_INSTRUMENTO", ePT_Caracter, Cmb_Instrumento.Tag, ePD_Entrada
      gDB.Parametros.Add "COD_SERIE", ePT_Caracter, Cmb_Serie.Text, ePD_Entrada
      If gDB.EjecutaSelect("") Then
         For Each Campo In gDB.Parametros("Cursor").Valor
            txt_moneda_um.Text = Campo("cod_moneda")
            txt_tasa_emision.Text = Campo("tasa_emision")
            dtp_Fecha_Emision.Value = Campo("fecha_emision")
         Next
      End If
      gDB.Parametros.Clear
      gDB.Procedimiento = "valrf.Pkg_Func_Rf.sp_num_cupon_vig"
      gDB.Parametros.Add "p_fec_cal", ePT_Fecha, dtp_Fecha_Actual.Value, ePD_Entrada
      gDB.Parametros.Add "p_fec_emi", ePT_Fecha, dtp_Fecha_Emision.Value, ePD_Entrada
      gDB.Parametros.Add "p_cod_emi", ePT_Caracter, Cmb_Emisor.Tag, ePD_Entrada
      gDB.Parametros.Add "p_cod_ins", ePT_Caracter, Cmb_Instrumento.Tag, ePD_Entrada
      gDB.Parametros.Add "p_cod_ser", ePT_Caracter, Cmb_Serie.Text, ePD_Entrada
      
      gDB.Parametros.Add "p_cup_vig", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_fec_ven", ePT_Fecha, "", ePD_Salida
      gDB.Parametros.Add "p_cod_ret", ePT_Numero, "", ePD_Salida
      gDB.Parametros.Add "p_men_err", ePT_Caracter, "", ePD_Salida
      
      If gDB.EjecutaSP Then
         If (gDB.Parametros("p_cod_ret").Valor = 0) Then
            txt_cupon_vigente.Text = gDB.Parametros("p_cup_vig").Valor 'campo("p_cup_vig").Value
            dtp_Fecha_Prox_Vcto.Value = gDB.Parametros("p_fec_ven").Valor 'campo("p_fec_ven").Value
         Else
            MsgBox "Error en : " & gDB.Procedimiento & vbCrLf & gDB.Parametros("p_cod_ret").Valor & " : " & gDB.ErrMsg, vbCritical + vbOKOnly, Me.Caption
         End If
      Else
         MsgBox "Error al Ejecutar Proc. " & gDB.Procedimiento, vbCritical + vbOKOnly, Me.Caption
      End If
            
      If Cmb_Instrumento.Tag = "LH" Then
         dtp_Fecha_Emision.SetFocus
         
         txt_cupon_vigente.Text = ""
         dtp_Fecha_Prox_Vcto.Value = Format(Fnt_FechaServidor, cFormatDate)
      Else
         txt_tasa_valoracion.SetFocus
      End If
   End If
End Sub

Private Sub Command1_Click()
   Const clrHeader = &HD0D0D0
   Const sHeader = "Linea|Nemot�cnico|Descripci�n|C�digo"
   Const sFormat = "1200|1800|6000|>1000"
   Dim Campo As hFields
   Dim sRecord
   Dim bAppend
   Dim a As Integer

   gDB.Parametros.Clear
   gDB.Procedimiento = "PKG_RENTA_FIJA.Buscar_Rf_Nemotecnico"
   gDB.Parametros.Add "Tipo", ePT_Caracter, gcPROD_RF_NAC, ePD_Entrada
   gDB.Parametros.Add "Nemo", ePT_Caracter, "%", ePD_Entrada
   gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
   If gDB.EjecutaSP Then
      With vp
         .StartDoc
         
         .SpaceBefore = "4pt"
         .SpaceAfter = "4pt"
                
         .FontSize = 18
         .FontBold = True
         .Paragraph = "INFORME DE PRUEBA"
         .FontBold = False
         .FontSize = 11
         .Paragraph = "Este es un informe de Prueba, No son necesariamente datos reales"
                      
         .FontBold = True
         .FontUnderline = True
         .Paragraph = "Resultados"
         .FontUnderline = False
         .FontBold = False
         
         .StartTable
         .TableCell(tcAlignCurrency) = False
                  
         a = 1
         
         For Each Campo In gDB.Parametros("Pcursor").Valor
            sRecord = "Linea " & a & "|" & Campo("nemotecnico").Value & "|" & Campo("dsc_nemotecnico").Value & "|" & Campo("id_nemotecnico").Value
            .AddTable sFormat, sHeader, sRecord, clrHeader, , bAppend
            a = a + 1
         Next
         
         .EndTable
         .EndDoc
      End With
   End If
End Sub

Private Sub Command2_Click()
   Dim s$, i%
   
   s = Command2
   i = InStr(s, ":")
   
   If i > 0 Then s = Trim(Mid(s, i + 1))
      gDB.Parametros.Clear
      gDB.Procedimiento = "PKG_RENTA_FIJA.Buscar_Rf_Nemotecnico"
      gDB.Parametros.Add "Tipo", ePT_Caracter, gcPROD_RF_NAC, ePD_Entrada
      gDB.Parametros.Add "Nemo", ePT_Caracter, "%", ePD_Entrada
      gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
      If gDB.EjecutaSP Then
         With vp
            .FontName = "Tahoma"
            .FontSize = 9
            .PenColor = RGB(190, 190, 190)
            .Footer = "PAGINA %d"
            .PageBorder = pbColBottom
            .StartDoc
            
            .Paragraph = "INFORME DE PRUEBA"
            .FontBold = True
            .Paragraph = "FECHA : " & Format(Fnt_FechaServidor, cFormatDate)
            .FontBold = False
            .Paragraph = ""
            
            RenderRecordset vp, gDB.Parametros("Pcursor").Valor, 0
            
            .EndDoc
            .ScrollIntoView 0, 0
         End With
         
         MousePointer = vbDefault
      End If
End Sub

Private Sub Command3_Click()
   VSReport1.DataSource.RecordSource = gDB.Tabla
  
   VSReport1.Load "d:\prueba\prueba.xml", "prueba"
 
   ' render report
   VSReport1.Render vp
End Sub

Private Sub Command4_Click()
   ' set name of HTML output file
   vp.ExportFile = "c:\text_m.html"
   vp.ExportFormat = vpxDHTML
   
   Call Command2_Click
   
   ' show file in browser
   ShellExecute hwnd, "open", vp.ExportFile, 0, 0, 0
   
   ' clear HTML output file
   vp.ExportFile = ""
End Sub

Private Sub dtp_fecha_emision_Change()
   gDB.Parametros.Clear
   gDB.Procedimiento = "valrf.Pkg_Func_Rf.sp_num_cupon_vig"
   gDB.Parametros.Add "p_fec_cal", ePT_Fecha, dtp_Fecha_Actual.Value, ePD_Entrada
   gDB.Parametros.Add "p_fec_emi", ePT_Fecha, dtp_Fecha_Emision.Value, ePD_Entrada
   gDB.Parametros.Add "p_cod_emi", ePT_Caracter, Cmb_Emisor.Tag, ePD_Entrada
   gDB.Parametros.Add "p_cod_ins", ePT_Caracter, Cmb_Instrumento.Tag, ePD_Entrada
   gDB.Parametros.Add "p_cod_ser", ePT_Caracter, Cmb_Serie.Text, ePD_Entrada
   
   gDB.Parametros.Add "p_cup_vig", ePT_Numero, "", ePD_Salida
   gDB.Parametros.Add "p_fec_ven", ePT_Fecha, "", ePD_Salida
   gDB.Parametros.Add "p_cod_ret", ePT_Numero, "", ePD_Salida
   gDB.Parametros.Add "p_men_err", ePT_Caracter, "", ePD_Salida
   
   If gDB.EjecutaSP Then
      If (gDB.Parametros("p_cod_ret").Valor = 0) Then
         txt_cupon_vigente.Text = gDB.Parametros("p_cup_vig").Valor 'campo("p_cup_vig").Value
         dtp_Fecha_Prox_Vcto.Value = gDB.Parametros("p_fec_ven").Valor  'campo("p_fec_ven").Value
      Else
         MsgBox "Error en : " & gDB.Procedimiento & vbCrLf & gDB.Parametros("p_cod_ret").Valor & " : " & gDB.Parametros("p_men_err").Valor, vbCritical + vbOKOnly, Me.Caption
      End If
   Else
      MsgBox "Error al Ejecutar Proc. : " & gDB.Procedimiento, vbCritical + vbOKOnly, Me.Caption
   End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
   Dim Resp As Integer
   
   If (KeyCode = vbKeyEscape) Then
      Resp = MsgBox("Esta seguro(a) que desea salir?", vbYesNo + vbQuestion, Me.Caption)
      If Resp = vbYes Then
         Unload Me
      End If
   End If
End Sub

Private Sub Form_Load()
   With Toolbar1
      Set .ImageList = MDI_Principal.ImageListGlobal16
         .Buttons("EXCEL").Image = cBoton_Grabar
         .Buttons("EXIT").Image = cBoton_Salir
   End With
   
   With Toolbar2
      Set .ImageList = MDI_Principal.ImageListGlobal16
         .Buttons("CALC").Image = cBoton_Agregar_Grilla
         .Buttons("LIMP").Image = cBoton_Eliminar_Grilla
         .Appearance = ccFlat
   End With
   
   With Toolbar3
      Set .ImageList = MDI_Principal.ImageListGlobal16
         .Buttons("ADD").Image = cBoton_Aceptar
         .Buttons("QUITA").Image = cBoton_Cancelar
         .Appearance = ccFlat
   End With
   
   Call Sub_CargaForm
   
   Call CargaDatos
   
   Me.Top = 1
   Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
  Call Sub_FormControl_Color(Me.Controls)
   
  Grilla.Rows = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar1, BarraProceso, Me)
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
   Dim FileName As String
      
   Select Case Button.Key
      Case "EXCEL"
         If Grilla.Rows > 1 Then
            Dialogo.ShowSave
            FileName = Dialogo.FileName
            If Not FileName = "" Then
               If (Not FileExists(FileName)) Then
                  Grilla.SaveGrid FileName, flexFileExcel, 3
               Else
                  If MsgBox("Archivo ya Existe. �Desea Reemplazarlo? ", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
                     Grilla.SaveGrid FileName, flexFileExcel, 3
                  End If
               End If
            End If
         Else
            MsgBox "Falta agregar datos.", vbOKOnly + vbCritical, Me.Caption
         End If
      Case "EXIT"
            fSalir = True
            Unload Me
   End Select
End Sub

Private Function FileExists(FileName As String) As Boolean
   On Error GoTo ErrorHandler
   
   ' get the attributes and ensure that it isn't a directory
   FileExists = (GetAttr(FileName) And vbDirectory) = 0
   
   Exit Function
ErrorHandler:
   ' if an error occurs, this function returns False
   FileExists = False
End Function

Private Sub Toolbar2_ButtonClick(ByVal Button As MSComctlLib.Button)
   Dim Validado As Boolean
   Dim obj As Control
      
   Validado = True
   Select Case Button
      Case "Calcular"
         If (txt_presente_pesos.Text = "" And txt_tasa_valoracion.Text = "") Then
            MsgBox "Debe Ingresar la tasa de valoraci�n � Valor Presente en Pesos", vbInformation + vbOKOnly, Me.Caption
            txt_tasa_valoracion.SetFocus
            Validado = False
         End If
         If (txt_valor_nominal.Text = "") Then
            MsgBox "Debe ingresar el valor nominal", vbInformation + vbOKOnly, Me.Caption
            txt_valor_nominal.SetFocus
            Validado = False
         End If
         If (IsDate(dtp_Fecha_Actual.Value) And IsDate(dtp_Fecha_Emision.Value)) Then
            If DateCmp(CDate(dtp_Fecha_Actual.Value), CDate(dtp_Fecha_Emision.Value)) Then
               MsgBox "Fecha Actual debe ser Posterior a fecha de Emisi�n", vbCritical + vbOKOnly, Me.Caption
               dtp_Fecha_Actual.SetFocus
               Validado = False
            Else
               txt_tasa_valoracion.SetFocus
            End If
         Else
            If dtp_Fecha_Emision = "" Then
               MsgBox "No se encuentra la Fecha de Emisi�n", vbCritical + vbOKOnly, Me.Caption
               Validado = False
            End If
         End If
         If (Cmb_Serie.Text = "") Then
            MsgBox "Debe ingresar la Serie", vbInformation + vbOKOnly, Me.Caption
            Validado = False
            Cmb_Nemotecnico.SetFocus
         End If
         If (Cmb_Instrumento.Text = "") Then
            MsgBox "Debe ingresar el Instrumento ", vbInformation + vbOKOnly, Me.Caption
            Validado = False
            Cmb_Nemotecnico.SetFocus
         End If
         If (Cmb_Emisor.Text = "") Then
            MsgBox "Debe ingresar el Emisor", vbInformation + vbOKOnly, Me.Caption
            Validado = False
            Cmb_Nemotecnico.SetFocus
         End If
         If (txt_cupon_vigente.Text = "") Then
            MsgBox "Debe ingresar el Cupon Vigente", vbInformation + vbOKOnly, Me.Caption
            Validado = False
            txt_cupon_vigente.SetFocus
         End If
         If (Validado) Then
            Calcular
         End If
               
      Case "Limpiar"
         Limpiar_Form
   End Select
End Sub

Private Sub Toolbar3_ButtonClick(ByVal Button As MSComctlLib.Button)
   Select Case Button
      Case "Agregar"
         If ValidaAgregar Then
            Grilla.AddItem Grilla.Row
            Randomize
            Grilla.TextMatrix(Grilla.Rows - 1, 1) = Cmb_Nemotecnico.Text
            Grilla.TextMatrix(Grilla.Rows - 1, 2) = Cmb_Emisor.Text
            Grilla.TextMatrix(Grilla.Rows - 1, 3) = Cmb_Instrumento.Text
            Grilla.TextMatrix(Grilla.Rows - 1, 4) = Cmb_Serie.Text
            Grilla.TextMatrix(Grilla.Rows - 1, 5) = Format(txt_valor_nominal.Text, "###,###,###.##")
            Grilla.TextMatrix(Grilla.Rows - 1, 6) = Format(txt_tasa_valoracion.Text, "###,###,###.##")
            Grilla.TextMatrix(Grilla.Rows - 1, 7) = Format(txt_valor_presente_pesos.Text, "###,###,###.##")
            Calcula_Total
         Else
            MsgBox "Falta ingresar datos.", vbCritical + vbOKOnly, Me.Caption
         End If
      Case "Quitar"
         Grilla.Col = 1
         If Grilla.Rows > 1 And Grilla.Row > 0 Then
            If MsgBox("�Desea eliminar este Item? " & Grilla.Text, vbYesNo + vbQuestion, Me.Caption) = vbYes Then
               Grilla.RemoveItem Grilla.Row
               Calcula_Total
            End If
         Else
            If Grilla.Rows > 1 Then
               If Grilla.Row = 0 Then
                  MsgBox "Debe seleccionar Item a Borrar.", vbOKOnly + vbCritical, Me.Caption
               End If
            Else
               MsgBox "Debe tener Items para esta operaci�n.", vbOKOnly + vbCritical, Me.Caption
            End If
         End If
   End Select
End Sub

Private Sub Calcula_Total()
   Dim lLinea As Long
   Dim lTotal As Double
   Dim lCol As Variant
   
   lTotal = 0
   For lLinea = 1 To (Grilla.Rows - 1)
      lCol = Grilla.ColIndex("pesos")
      If Not Grilla.TextMatrix(lLinea, lCol) = "" Then
         lTotal = lTotal + GetCell(Grilla, lLinea, "pesos")
      End If
   Next
   
   txt_total.Text = lTotal
End Sub

Private Sub txt_cupon_vigente_KeyDown(KeyCode As Integer, Shift As Integer)
   If (KeyCode = vbKeyReturn) Then
      SendKeys "{TAB}"
   End If
End Sub

Private Sub txt_cupon_vigente_LostFocus()
   Dim Fecha_Vencimiento As Date
   Dim cod_Ret As Integer
   
   If (IsDate(dtp_Fecha_Emision.Value)) Then
      If Not txt_cupon_vigente.Text = "" Then
         gDB.Parametros.Clear
         gDB.Procedimiento = "valrf.Pkg_Func_rf.sp_fecha_prox_vcto"
         
         gDB.Parametros.Add "p_fec_cal", ePT_Fecha, dtp_Fecha_Actual.Value, ePD_Entrada
         gDB.Parametros.Add "p_fec_emi", ePT_Fecha, dtp_Fecha_Emision.Value, ePD_Entrada
         gDB.Parametros.Add "p_cod_emi", ePT_Caracter, Cmb_Emisor.Tag, ePD_Entrada
         gDB.Parametros.Add "p_cod_ins", ePT_Caracter, Cmb_Instrumento.Tag, ePD_Entrada
         gDB.Parametros.Add "p_cod_ser", ePT_Caracter, Cmb_Serie.Text, ePD_Entrada
         gDB.Parametros.Add "p_num_cup", ePT_Numero, txt_cupon_vigente.Text, ePD_Entrada
         
         gDB.Parametros.Add "p_fec_ven", ePT_Fecha, "", ePD_Salida
         gDB.Parametros.Add "p_cod_ret", ePT_Numero, "", ePD_Salida
         gDB.Parametros.Add "p_men_err", ePT_Caracter, "", ePD_Salida
         
         If gDB.EjecutaSP Then
            If (gDB.Parametros("p_cod_ret").Valor = 0) Then
               Fecha_Vencimiento = gDB.Parametros("p_fec_ven").Valor
               cod_Ret = gDB.Parametros("p_cod_ret").Valor
                                       
               dtp_Fecha_Prox_Vcto.Value = Fecha_Vencimiento
               
               If DateCmp(dtp_Fecha_Actual.Value, dtp_Fecha_Emision.Value) Then
                  MsgBox "Fecha Actual debe ser Posterior a fecha de Emisi�n", vbCritical + vbOKOnly, Me.Caption
                  dtp_Fecha_Actual.SetFocus
               Else
                  txt_tasa_valoracion.SetFocus
               End If
            Else
               MsgBox "Package " & gDB.Procedimiento & vbCrLf & gDB.Parametros("p_cod_ret").Valor & " : " & gDB.Parametros("p_men_err").Valor, vbCritical + vbExclamation + vbOKOnly, Me.Caption
               txt_cupon_vigente.SetFocus
            End If
         Else
            MsgBox "No se Ejecuto Package " & gDB.Procedimiento & " : " & vbCrLf & " Pkg_Func_Nemo.Sp_Desc_Nemo " & gDB.ErrMsg, vbCritical + vbOKOnly, Me.Caption
         End If
      Else
         MsgBox "Debe Ingresar el Cupon Vigente ", vbCritical + vbOKOnly, Me.Caption
      End If
   Else
      MsgBox "Debe Ingresar la Fecha de Emisi�n ", vbCritical + vbOKOnly, Me.Caption
   End If
End Sub


Private Sub txt_tasa_valoracion_KeyDown(KeyCode As Integer, Shift As Integer)
   If (KeyCode = vbKeyReturn) Then
      SendKeys "{TAB}"
   End If
End Sub


Private Sub txt_valor_nominal_KeyDown(KeyCode As Integer, Shift As Integer)
   If (KeyCode = vbKeyReturn) Then
      SendKeys "{TAB}"
   End If
End Sub


Private Sub txt_valor_presente_pesos_Change()
   txt_presente_pesos.Text = Format(txt_valor_presente_pesos.Text, "###,###,###.##")
End Sub
