VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Observacion_Cliente 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Observación del Cliente"
   ClientHeight    =   3855
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6300
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3855
   ScaleWidth      =   6300
   Begin VB.Frame Frame1 
      Caption         =   "Observación"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3345
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   6165
      Begin VB.TextBox Txt_Observacion_Cliente 
         BackColor       =   &H00C0FFC0&
         Height          =   2985
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   240
         Width           =   5925
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6300
      _ExtentX        =   11113
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la información de forma Permanente"
            Object.ToolTipText     =   "Graba la información de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la información perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la información perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Observacion_Cliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String

Dim fId_Cliente As String

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm
End Sub

Public Function Fnt_Modificar(ByVal pkey, ByVal pId_Cliente)
  fKey = pkey
  
  Call Sub_CargarDatos
    
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso de Observación"
  Else
    Me.Caption = "Modificación de Observación: " & pkey
  End If
  
  fId_Cliente = pId_Cliente
    
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acción perderá las modificaciones." & vbLf & "¿Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lcClientes_Observaciones As Object 'Class_Clientes_Observaciones

  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Exit Function
  End If

  Set lcClientes_Observaciones = Fnt_CreateObject(cDLL_Clientes_Observaciones) 'New Class_Clientes_Observaciones
  With lcClientes_Observaciones
    Set .gDB = gDB
    .Campo("id_cliente_observacion").Valor = fKey
    .Campo("gls_cliente_observacion").Valor = Txt_Observacion_Cliente.Text
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    .Campo("id_cliente").Valor = fId_Cliente
    If Not .Guardar Then
      MsgBox "Problemas en grabar en Moneda." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      Fnt_Grabar = False
    End If
  End With
  Set lcClientes_Observaciones = Nothing
  
End Function

Private Sub Sub_CargaForm()
  Call Sub_FormControl_Color(Me.Controls)
End Sub

Private Sub Sub_CargarDatos()
Dim lClientes_Observaciones As Object 'Class_Clientes_Observaciones
  
  Load Me

  Set lClientes_Observaciones = Fnt_CreateObject(cDLL_Clientes_Observaciones) 'New Class_Clientes_Observaciones
  With lClientes_Observaciones
    Set .gDB = gDB
    .Campo("id_cliente_observacion").Valor = fKey
    If .Buscar Then
      If .Cursor.Count > 0 Then
        Txt_Observacion_Cliente.Text = "" & .Cursor(1)("gls_cliente_observacion").Value
      End If
    Else
      MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lClientes_Observaciones = Nothing
  
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = True
  
  If Fnt_PreguntaIsNull(Txt_Observacion_Cliente, "Observación") Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
End Function
