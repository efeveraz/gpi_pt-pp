VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Arbol_Clase_Instrumento 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor Clase de Instrumentos "
   ClientHeight    =   6120
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   6675
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6120
   ScaleWidth      =   6675
   Begin VB.Frame Frame1 
      Height          =   5595
      Left            =   60
      TabIndex        =   2
      Top             =   400
      Width           =   6435
      Begin MSComctlLib.TreeView Arbol 
         Height          =   4815
         Left            =   120
         TabIndex        =   3
         Top             =   600
         Width           =   6135
         _ExtentX        =   10821
         _ExtentY        =   8493
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   18
         LineStyle       =   1
         Sorted          =   -1  'True
         Style           =   7
         FullRowSelect   =   -1  'True
         BorderStyle     =   1
         Appearance      =   1
      End
      Begin MSComctlLib.Toolbar Toolbar_Arbol 
         Height          =   360
         Left            =   120
         TabIndex        =   4
         Top             =   180
         Width           =   6135
         _ExtentX        =   10821
         _ExtentY        =   635
         ButtonWidth     =   2037
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Wrappable       =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Agregar"
               Key             =   "ADD"
               Description     =   "Agrega una direcci�n al cliente."
               Object.ToolTipText     =   "Agrega una direcci�n al cliente."
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Modificar"
               Key             =   "UPDATE"
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Eliminar"
               Key             =   "DEL"
               Description     =   "Elimina la direcci�n del cliente."
               Object.ToolTipText     =   "Elimina la direcci�n del cliente."
            EndProperty
         EndProperty
         BorderStyle     =   1
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6675
      _ExtentX        =   11774
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Grabar"
            Key             =   "SAVE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Graba los cambios realizados"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   3690
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Arbol_Clase_Instrumento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana

Const cPrefijo = "K"
Const cRoot = "ROOT"

Dim fhBorrar As hRecord


Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub
'----------------------------------------------------------------------------

Private Sub Sub_Grabar()
Dim lcArbol_Clase_Instrumento As Class_Arbol_Clase_Instrumento
Dim lNode As Node
Dim lReg  As hFields
'-----------------------------------------------
Dim lResult As Boolean
Dim lId_Padre
Dim lOrden As Long
    
On Error GoTo ErrProcedure
  
  Call Sub_Bloquea_Puntero(Me)
  
  'Inicia la Transaccion
  gDB.IniciarTransaccion
    
  lResult = False
  lOrden = 0
  
  'Elimina los nodos seleccionados.
  For Each lReg In fhBorrar
    Set lcArbol_Clase_Instrumento = New Class_Arbol_Clase_Instrumento
    With lcArbol_Clase_Instrumento
      .Campo("ID_ARBOL_CLASE_INST").Valor = lReg("ID_ARBOL_CLASE_INST").Value
      If Not .Borrar Then
        Call Fnt_MsgError(.SubTipo_LOG _
                        , "Problemas en grabar." _
                        , .ErrMsg _
                        , pConLog:=True)
        lResult = False
        GoTo ErrProcedure
      End If
    End With
    Set lcArbol_Clase_Instrumento = Nothing
  Next
  
  
  'Guarda los perfiles chequeados
  For Each lNode In Arbol.Nodes
    'Primero verifica que no sea el primer elemento de menu "ROOT"
    If Not lNode.Parent Is Nothing Then
      
      Set lcArbol_Clase_Instrumento = New Class_Arbol_Clase_Instrumento
      With lcArbol_Clase_Instrumento
        'Si el padre tiene el key "ROOT" entrega null p
        lId_Padre = IIf(lNode.Parent.Key = cRoot, Null, lNode.Parent.Key)
        
        .Campo("ID_ARBOL_CLASE_INST").Valor = Mid(lNode.Key, 2)
        .Campo("ID_EMPRESA").Valor = Fnt_EmpresaActual
        .Campo("DSC_ARBOL_CLASE_INST").Valor = lNode.Text
        .Campo("ID_PADRE_ARBOL_CLASE_INST").Valor = Mid(lId_Padre, 2)
        .Campo("ORDEN").Valor = lOrden
        
        If Not .Guardar Then
          Call Fnt_MsgError(lcArbol_Clase_Instrumento.SubTipo_LOG _
                          , "Problemas en la grabaci�n del arbol" _
                          , lcArbol_Clase_Instrumento.ErrMsg _
                          , pConLog:=True)
          GoTo ExitProcedure
        End If
        lNode.Key = cPrefijo & .Campo("ID_ARBOL_CLASE_INST").Valor
      End With
      Set lcArbol_Clase_Instrumento = Nothing
      
      lOrden = lOrden + 1
    End If
  Next
    
  Call fhBorrar.LimpiarRegistros
  
  lResult = True
    
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas en la grabaci�n del arbol", Err.Description, pConLog:=True)
    lResult = False
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  If lResult Then
    gDB.CommitTransaccion
  Else
    gDB.RollbackTransaccion
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Arbol_AfterLabelEdit(Cancel As Integer, NewString As String)
  If Trim(NewString) = "" Then
    Cancel = True
    If Arbol.SelectedItem.Key = "" Then
      Call Arbol.Nodes.Remove(Arbol.SelectedItem.Index)
    End If
  End If
End Sub

 

'Private Sub Cmb_tipo_Click()
'Call Sub_CargarDatos
'End Sub

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SAVE").Image = cBoton_Grabar
    .Buttons("EXIT").Image = cBoton_Salir
    .Buttons("REFRESH").Image = cBoton_Original
  End With
  
  With Toolbar_Arbol
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("UPDATE").Image = cBoton_Modificar
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
  End With
  
  Set fhBorrar = New hRecord
  With fhBorrar
    Call .AddField("ID_ARBOL_CLASE_INST")
  End With
   
  Call Sub_CargarDatos
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargarDatos()
Dim lcArbol_Clase_Instrumento As Class_Arbol_Clase_Instrumento
Dim lReg                      As hFields
'---------------------------------------------------------------
Dim lRelacion
Dim lPadre
Dim lNode As Node
Dim lID_PADRE_ARBOL_CLASE_INST

  Call Sub_Bloquea_Puntero(Me)
  
  lID_PADRE_ARBOL_CLASE_INST = Null
  If Not Arbol.SelectedItem Is Nothing Then
    lID_PADRE_ARBOL_CLASE_INST = Arbol.SelectedItem
  End If
  
  'Limpia los que se quieren elimiar
  Call fhBorrar.LimpiarRegistros
  
  Arbol.Nodes.Clear
  Set lNode = Arbol.Nodes.Add(Key:=cRoot _
                            , Text:="Raiz")
  lNode.Expanded = True
  
  'Set Arbol.ImageList = MDI_Principal.ImageListGlobal16
  
  Rem Carga el Arbol de clasificaciones de instrumentos
  Set lcArbol_Clase_Instrumento = New Class_Arbol_Clase_Instrumento
  lcArbol_Clase_Instrumento.Campo("id_empresa").Valor = Fnt_EmpresaActual
  
  
  '---- Se Agrega ACI_tipo para diferenciar los arboles 12/12/2018 Ralvarez
  'Dim ase_tipo As Integer
  'If Cmb_tipo.ListIndex = 0 Then ase_tipo = 1
  'If Cmb_tipo.ListIndex = 1 Then ase_tipo = 2
   '  lcArbol_Clase_Instrumento.Campo("id_aci_tipo").Valor = 1
 '--------------------------------------------------------------------------
  
  
  
  If Not lcArbol_Clase_Instrumento.Buscar Then
    Call Fnt_MsgError(lcArbol_Clase_Instrumento.SubTipo_LOG _
                    , "Problemas al buscar." _
                    , lcArbol_Clase_Instrumento.ErrMsg _
                    , pConLog:=True)
    GoTo ExitProcedure
  End If
  
  For Each lReg In lcArbol_Clase_Instrumento.Cursor
    If IsNull(lReg("ID_PADRE_ARBOL_CLASE_INST").Value) Then
      lPadre = cRoot
    Else
      lPadre = cPrefijo & lReg("ID_PADRE_ARBOL_CLASE_INST").Value
    End If
    
    Set lNode = Arbol.Nodes.Add(relative:=lPadre _
                             , relationship:=tvwChild _
                             , Key:=cPrefijo & lReg("ID_ARBOL_CLASE_INST").Value _
                             , Text:=lReg("DSC_ARBOL_CLASE_INST").Value)
    lNode.Expanded = True
    'lNode.Bold = True
    'lNode.Image = "close_folder"
  Next
  Set lcArbol_Clase_Instrumento = Nothing
  
  If Not IsNull(lID_PADRE_ARBOL_CLASE_INST) Then
    On Error Resume Next
    Set Arbol.SelectedItem = Arbol.Nodes(lID_PADRE_ARBOL_CLASE_INST)
  End If
  
ExitProcedure:
  Set lcArbol_Clase_Instrumento = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

 


 
Private Sub Option1_Click(Index As Integer)
Call Sub_CargarDatos
End Sub

Private Sub Option2_Click()
Call Sub_CargarDatos
End Sub


Private Sub Toolbar_Arbol_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_AgregaNodo
    Case "UPDATE"
      Call Sub_ModificaNodo
    Case "DEL"
      Call Sub_EliminarNodo
  End Select
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      Call Sub_Grabar
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Sub Sub_AgregaNodo()
Dim lNodo_Select As Node

  If Arbol.SelectedItem Is Nothing Then
    Exit Sub
  End If
  
  With Arbol
    Set lNodo_Select = .SelectedItem
    
    Set .SelectedItem = .Nodes.Add(relative:=lNodo_Select _
                                 , relationship:=tvwChild _
                                 , Text:="Nueva Clase")
    
    .StartLabelEdit
  End With
End Sub

Private Sub Sub_ModificaNodo()
  If Arbol.SelectedItem Is Nothing Then
    Exit Sub
  End If
  
  If Arbol.SelectedItem.Key = cRoot Then
    Exit Sub
  End If
  
  Call Arbol.StartLabelEdit
End Sub

Private Sub Sub_EliminarNodo()
Dim lcNodo As Node
Dim lhBorrar As hRecord
Dim lReg As hFields


  If Arbol.SelectedItem Is Nothing Then
    Exit Sub
  End If
  
  Set lcNodo = Arbol.SelectedItem
   
  Select Case lcNodo.Key
    Case cRoot
      Exit Sub
    Case Else
      Set lhBorrar = New hRecord
      Call lhBorrar.AddField("ID")
    
      If Fnt_EliminaNodoRecursivo(pcNodo:=lcNodo _
                                , phBorrar:=lhBorrar) Then
        For Each lReg In lhBorrar
          fhBorrar.Add.Fields("ID_ARBOL_CLASE_INST").Value = lReg("id").Value
        Next
        Call Arbol.Nodes.Remove(lcNodo.Index)
      End If
      Set lhBorrar = Nothing
  End Select
End Sub

Private Function Fnt_EliminaNodoRecursivo(ByRef pcNodo As Node, ByRef phBorrar As hRecord, Optional pBorrarHijos As VbMsgBoxResult = vbNo) As Boolean
Dim lResult As Boolean
Dim lcNodo As Node

  Fnt_EliminaNodoRecursivo = False
  
  Set lcNodo = pcNodo.Child
  
  Do While Not lcNodo Is Nothing
    If pBorrarHijos = vbNo Then
      pBorrarHijos = MsgBox("La clase """ & pcNodo.Text & """ contiene clases hijas." & vbCr & vbCr & "�Desea eliminarla de todas formas?", vbYesNo + vbQuestion, Me.Caption)
    End If
  
    If pBorrarHijos = vbYes Then
      If Not Fnt_EliminaNodoRecursivo(pcNodo:=lcNodo _
                                    , phBorrar:=phBorrar _
                                    , pBorrarHijos:=pBorrarHijos) Then
        GoTo ExitProcedure
      End If
    Else
      GoTo ExitProcedure
    End If
    
    Set lcNodo = lcNodo.Next
  Loop
  
  If Not pcNodo.Key = "" Then
    phBorrar.Add.Fields("ID").Value = Mid(pcNodo.Key, 2)
    'Call Arbol.Nodes.Remove(pcNodo.Index)
  End If
  
  Fnt_EliminaNodoRecursivo = True
  
ExitProcedure:

End Function
