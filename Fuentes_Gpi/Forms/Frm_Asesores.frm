VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "c1sizer.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Asesores 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Asesor"
   ClientHeight    =   3990
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6795
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3990
   ScaleWidth      =   6795
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6795
      _ExtentX        =   11986
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin C1SizerLibCtl.C1Tab SSTab 
      Height          =   3465
      Left            =   90
      TabIndex        =   2
      Top             =   480
      Width           =   6675
      _cx             =   11774
      _cy             =   6112
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   801
      BackColor       =   6579300
      ForeColor       =   -2147483634
      FrontTabColor   =   -2147483635
      BackTabColor    =   6579300
      TabOutlineColor =   -2147483632
      FrontTabForeColor=   -2147483634
      Caption         =   "&General|&Empresas"
      Align           =   0
      CurrTab         =   1
      FirstTab        =   0
      Style           =   6
      Position        =   0
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   0   'False
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   -1  'True
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   0
      TabCaptionPos   =   4
      TabPicturePos   =   0
      CaptionEmpty    =   ""
      Separators      =   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   37
      Begin VB.Frame Frame1 
         Caption         =   "Datos Asesor"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   3120
         Left            =   -7260
         TabIndex        =   3
         Top             =   330
         Width           =   6645
         Begin VB.CheckBox chk_bloqueo 
            Caption         =   "Bloqueo"
            Height          =   285
            Left            =   90
            TabIndex        =   4
            Top             =   2640
            Width           =   1725
         End
         Begin hControl2.hTextLabel txt_nom_corto 
            Height          =   315
            Left            =   90
            TabIndex        =   5
            Top             =   1840
            Width           =   2805
            _ExtentX        =   4948
            _ExtentY        =   556
            LabelWidth      =   1275
            Caption         =   "Nombre Corto"
            Text            =   ""
            MaxLength       =   6
         End
         Begin hControl2.hTextLabel txt_rut 
            Height          =   315
            Left            =   90
            TabIndex        =   6
            Tag             =   "OBLI"
            Top             =   640
            Width           =   2805
            _ExtentX        =   4948
            _ExtentY        =   556
            LabelWidth      =   1275
            Caption         =   "Rut"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            MaxLength       =   10
         End
         Begin hControl2.hTextLabel Txt_Nombre 
            Height          =   315
            Left            =   90
            TabIndex        =   7
            Tag             =   "OBLI"
            Top             =   240
            Width           =   6200
            _ExtentX        =   10927
            _ExtentY        =   556
            LabelWidth      =   1275
            TextMinWidth    =   1200
            Caption         =   "Nombre"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            MaxLength       =   50
         End
         Begin hControl2.hTextLabel txt_email 
            Height          =   315
            Left            =   90
            TabIndex        =   8
            Top             =   1040
            Width           =   6200
            _ExtentX        =   10927
            _ExtentY        =   556
            LabelWidth      =   1275
            Caption         =   "Email"
            Text            =   ""
            MaxLength       =   50
         End
         Begin hControl2.hTextLabel txt_fono 
            Height          =   315
            Left            =   90
            TabIndex        =   9
            Top             =   1440
            Width           =   4185
            _ExtentX        =   7382
            _ExtentY        =   556
            LabelWidth      =   1275
            Caption         =   "Fono"
            Text            =   ""
            MaxLength       =   30
         End
         Begin TrueDBList80.TDBCombo Cmb_Estado 
            Height          =   345
            Left            =   1400
            TabIndex        =   10
            Tag             =   "OBLI=S;CAPTION=Estado"
            Top             =   2240
            Width           =   4900
            _ExtentX        =   8652
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Asesores.frx":0000
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label Label3 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Estado"
            Height          =   345
            Left            =   90
            TabIndex        =   11
            Top             =   2240
            Width           =   1275
         End
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Cambios 
         Height          =   3120
         Left            =   15
         TabIndex        =   12
         Top             =   330
         Width           =   6645
         _cx             =   11721
         _cy             =   5503
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   4
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Asesores.frx":00AA
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
End
Attribute VB_Name = "Frm_Asesores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String

Const fBloqueado = "S"
Const fNo_Bloqueado = "N"

Dim fFlg_Tipo_Permiso As String

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(pId_tipo_estado, pCod_Arbol_Sistema)
  fKey = pId_tipo_estado
  
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Call Sub_CargarDatos
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso de Asesor"
  Else
    Me.Caption = "Modifica Asesor: " & Txt_Nombre.Text
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        'Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub


Private Function Fnt_Grabar() As Boolean
Dim lestado               As String
Dim lblock                As String
Dim lFila                 As Long
Dim lAsesor               As Class_Asesor
Dim lcRel_Asesor_Empresa  As Class_Rel_Asesor_Empresa
Dim lid_asesor As Long

  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Exit Function
  End If

  Rem variables
  lestado = Fnt_ComboSelected_KEY(Cmb_Estado)
  If chk_bloqueo = vbChecked Then
    lblock = fBloqueado
  Else
    lblock = fNo_Bloqueado
  End If
  
  Set lAsesor = New Class_Asesor
  With lAsesor
    .Campo("id_asesor").Valor = fKey
    .Campo("Id_Tipo_Estado").Valor = cTEstado_Asesor
    .Campo("cod_estado").Valor = lestado
    .Campo("rut").Valor = txt_rut.Text
    .Campo("nombre").Valor = Txt_Nombre.Text
    .Campo("email").Valor = txt_email.Text
    .Campo("fono").Valor = txt_fono.Text
    .Campo("abr_nombre").Valor = txt_nom_corto.Text
    .Campo("flg_bloqueo").Valor = lblock
    If Not .Guardar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en guardar Asesores.", _
                        .ErrMsg, _
                        pConLog:=True)
      Fnt_Grabar = False
      GoTo ErrProcedure
      Else
      lid_asesor = .Campo("Id_asesor").Valor
    End If
  End With
  
  Rem 21/11/2009. Agregado por CGarcia.
  Rem Relaci�n ASESOR/EMPRESA
  Set lcRel_Asesor_Empresa = New Class_Rel_Asesor_Empresa
  With lcRel_Asesor_Empresa
    Rem Se eliminan registros de REL_ASESOR_EMPRESA por FKey
    .Campo("id_asesor").Valor = lid_asesor
    If Not .Borrar Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas al eliminar la relaci�n Asesor/Empresa.", _
                        .ErrMsg, _
                        pConLog:=True)
      Fnt_Grabar = False
      GoTo ErrProcedure
    End If
    
    Rem Guarda la Relaci�n ASESOR/EMPRESA
    For lFila = 1 To (Grilla_Cambios.Rows - 1)
      If Grilla_Cambios.Cell(flexcpChecked, lFila, 1) = flexChecked Then
        .Campo("id_empresa").Valor = GetCell(Grilla_Cambios, lFila, "id_empresa")
        If Not .Guardar Then
          Call Fnt_MsgError(.SubTipo_LOG, _
                            "Problemas en guardar la relaci�n Asesor/Empresa.", _
                            .ErrMsg, _
                            pConLog:=True)
          Fnt_Grabar = False
          GoTo ErrProcedure
        End If
      End If
    Next lFila
  End With

ErrProcedure:
  
  Set lAsesor = Nothing
  Set lcRel_Asesor_Empresa = Nothing
  
End Function

Private Sub Sub_CargaForm()
Dim lReg      As hFields
Dim lLinea    As Long
Dim lcEmpresa As Class_Empresas

  Call Sub_FormControl_Color(Me.Controls)

  Call Sub_CargaCombo_Estados(Cmb_Estado, cTEstado_Asesor)
  Call Sub_Elije_EstadoDefault(Cmb_Estado) 'HAF 20070703
  
  Rem 21/11/2009. Agregado por CGarcia.
  Rem Carga la grilla con las empresas
  Set lcEmpresa = New Class_Empresas
  With lcEmpresa
    If .Buscar Then
      Grilla_Cambios.Rows = 1
      For Each lReg In .Cursor
        Grilla_Cambios.AddItem ""
        lLinea = Grilla_Cambios.Rows - 1
        
        Call SetCell(Grilla_Cambios, lLinea, "Id_Empresa", lReg("ID_EMPRESA").Value)
        Grilla_Cambios.Cell(flexcpChecked, lLinea, 1) = flexUnchecked
        Call SetCell(Grilla_Cambios, lLinea, "Dsc_Empresa", lReg("DSC_EMPRESA").Value)
        Call SetCell(Grilla_Cambios, lLinea, "Abr_Empresa", lReg("ABR_EMPRESA").Value)
      Next
     End If
  End With
  
  Set lcEmpresa = Nothing
  
  SSTab.CurrTab = 0

End Sub

Private Sub Sub_CargarDatos()
Dim lReg                  As hCollection.hFields
Dim lLinea                As Long
Dim lAsesor               As Class_Asesor
Dim lcRel_Asesor_Empresa  As Class_Rel_Asesor_Empresa
  
  Load Me
  
  Set lAsesor = New Class_Asesor
  With lAsesor
    .Campo("id_asesor").Valor = fKey
    If .Buscar(True) Then
      For Each lReg In .Cursor
      
        Txt_Nombre.Text = NVL(lReg("nombre").Value, "")
        txt_rut.Text = NVL(lReg("rut").Value, "")
        txt_email.Text = NVL(lReg("email").Value, "")
        txt_fono.Text = NVL(lReg("fono").Value, "")
        txt_nom_corto.Text = NVL(lReg("abr_nombre").Value, "")
        Call Sub_ComboSelectedItem(Cmb_Estado, NVL(lReg("cod_estado").Value, ""))
        
        If lReg("flg_bloqueo").Value = fBloqueado Then
          chk_bloqueo = vbChecked
        Else
          chk_bloqueo = vbUnchecked
        End If
        
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG _
                        , "Problemas al buscar el Asesores." _
                        , .ErrMsg _
                        , pConLog:=True)
    End If
  End With
  
  Rem 21/11/2009. Agregado por CGarcia.
  Rem Carga la relaci�n entre ASESOR/EMPRESA
  Set lcRel_Asesor_Empresa = New Class_Rel_Asesor_Empresa
  lcRel_Asesor_Empresa.Campo("Id_Asesor").Valor = fKey
  If lcRel_Asesor_Empresa.Buscar Then
    With Grilla_Cambios
      For Each lReg In lcRel_Asesor_Empresa.Cursor
        lLinea = .FindRow(lReg("ID_EMPRESA").Value, , .ColIndex("Id_Empresa"))
        If lLinea > -1 Then
          .Cell(flexcpChecked, lLinea, .ColIndex("Seleccion")) = flexChecked
        End If
      Next
    End With
  Else
    Call Fnt_MsgError(lcRel_Asesor_Empresa.SubTipo_LOG, _
                      "Problemas en cargar la relaci�n ASESOR/EMPRESA.", _
                      lcRel_Asesor_Empresa.ErrMsg, _
                      pConLog:=True)
  End If
  Set lcRel_Asesor_Empresa = Nothing
  
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  
  Fnt_ValidarDatos = True
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
  If Not ValidaRut(txt_rut.Text) Then
    MsgBox "RUT No V�lido.", vbExclamation, Me.Caption
    txt_rut.SetFocus
    Fnt_ValidarDatos = False
  End If
  
  If Not Fnt_VerificaEMAIL(txt_email.Text) Then
    Fnt_ValidarDatos = False
    txt_email.SetFocus
    Exit Function
  End If
End Function

Private Sub txt_rut_LostFocus()
  If Not Trim(txt_rut.Text) = "" Then
    If Not ValidaRut(txt_rut.Text) Then
       MsgBox "RUT No V�lido.", vbExclamation, Me.Caption
       txt_rut.SetFocus
       Exit Sub
    End If
    If InStr(txt_rut.Text, "-") = 0 Then
       txt_rut.Text = Mid(Trim(txt_rut.Text), 1, Len(Trim(txt_rut.Text)) - 1) & "-" & Mid(Trim(txt_rut.Text), Len(Trim(txt_rut.Text)), 1)
    End If
  End If
End Sub
