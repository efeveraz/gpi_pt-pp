VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Operacion_Bonos_Nac_II 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Bonos Nacionales Ver 2.0"
   ClientHeight    =   9495
   ClientLeft      =   45
   ClientTop       =   540
   ClientWidth     =   8940
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9495
   ScaleWidth      =   8940
   Begin VB.Frame Frame_Nemotecnico 
      Caption         =   "Nemot�cnico"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3105
      Left            =   45
      TabIndex        =   30
      Top             =   6300
      Width           =   8820
      Begin VB.Frame Frame_Inversion 
         Caption         =   "Inversi�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1665
         Left            =   150
         TabIndex        =   38
         Top             =   1350
         Width           =   8565
         Begin VB.Frame Frame_Tasa 
            Caption         =   "Tasas"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   1035
            Left            =   5790
            TabIndex        =   45
            Top             =   150
            Width           =   2655
            Begin VB.TextBox Txt_TasaTransferencia 
               Height          =   315
               Left            =   4935
               MaxLength       =   10
               TabIndex        =   46
               Top             =   270
               Visible         =   0   'False
               Width           =   765
            End
            Begin hControl2.hTextLabel Txt_TasaCompra 
               Height          =   315
               Left            =   90
               TabIndex        =   47
               Tag             =   "OBLI"
               Top             =   240
               Width           =   2475
               _ExtentX        =   4366
               _ExtentY        =   556
               LabelWidth      =   1200
               TextMinWidth    =   870
               Caption         =   " Inversi�n"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0.00"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_TasaNominal 
               Height          =   315
               Left            =   90
               TabIndex        =   48
               Tag             =   "OBLI"
               Top             =   600
               Width           =   2475
               _ExtentX        =   4366
               _ExtentY        =   556
               LabelWidth      =   1200
               TextMinWidth    =   870
               Caption         =   " Transferencia"
               Text            =   ""
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0.00"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_Tasa_Historico 
               Height          =   315
               Left            =   90
               TabIndex        =   49
               Top             =   960
               Visible         =   0   'False
               Width           =   2475
               _ExtentX        =   4366
               _ExtentY        =   556
               LabelWidth      =   1200
               TextMinWidth    =   870
               Caption         =   "Hist�rica"
               Text            =   ""
               BackColorTxt    =   16777215
               BackColorTxt    =   16777215
               Format          =   "#,##0.00"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin VB.Label Label3 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Transferencia"
               Height          =   315
               Left            =   3795
               TabIndex        =   50
               Top             =   270
               Visible         =   0   'False
               Width           =   1110
            End
         End
         Begin VB.CheckBox Chk_Vende_Todo 
            Caption         =   "Vende Todo"
            Height          =   315
            Left            =   3510
            TabIndex        =   39
            Top             =   720
            Width           =   1185
         End
         Begin hControl2.hTextLabel Txt_CantidadInversion 
            Height          =   315
            Left            =   120
            TabIndex        =   40
            Tag             =   "OBLI"
            Top             =   360
            Width           =   3285
            _ExtentX        =   5794
            _ExtentY        =   556
            LabelWidth      =   1470
            Caption         =   " Nominales"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Utilidad 
            Height          =   315
            Left            =   120
            TabIndex        =   41
            Tag             =   "OBLI"
            Top             =   1080
            Width           =   3285
            _ExtentX        =   5794
            _ExtentY        =   556
            LabelWidth      =   1470
            Caption         =   " Utilidad"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Monto_Inversion 
            Height          =   315
            Left            =   120
            TabIndex        =   42
            Tag             =   "OBLI"
            Top             =   720
            Width           =   3285
            _ExtentX        =   5794
            _ExtentY        =   556
            LabelWidth      =   1470
            Caption         =   " Monto Operaci�n"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin MSComctlLib.Toolbar Toolbar_Operacion 
            Height          =   330
            Left            =   6120
            TabIndex        =   43
            Top             =   1230
            Width           =   2310
            _ExtentX        =   4075
            _ExtentY        =   582
            ButtonWidth     =   2037
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Aceptar"
                  Key             =   "OK"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Agregar un nemotecnico a la Operaci�n"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "CANCEL"
               EndProperty
            EndProperty
         End
         Begin MSComctlLib.Toolbar Toolbar_Valorizar 
            Height          =   330
            Left            =   3510
            TabIndex        =   44
            Top             =   360
            Width           =   1110
            _ExtentX        =   1958
            _ExtentY        =   582
            ButtonWidth     =   2064
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Valorizar"
                  Key             =   "VALORIZA"
                  Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
                  Object.ToolTipText     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
               EndProperty
            EndProperty
         End
      End
      Begin VB.CommandButton Cmb_Buscar 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3720
         Picture         =   "Frm_Operacion_Bonos_Nac_II.frx":0000
         TabIndex        =   31
         ToolTipText     =   "Presione para buscar Nemotecnico."
         Top             =   270
         Width           =   345
      End
      Begin TrueDBList80.TDBCombo Cmb_Nemotecnico 
         Height          =   345
         Left            =   3720
         TabIndex        =   32
         Tag             =   "CAPTION=Nemot�cnico"
         Top             =   600
         Visible         =   0   'False
         Width           =   1665
         _ExtentX        =   2937
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   -1  'True
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Operacion_Bonos_Nac_II.frx":030A
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin hControl2.hTextLabel Txt_Emisor 
         Height          =   315
         Left            =   150
         TabIndex        =   33
         Top             =   630
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   556
         LabelWidth      =   1350
         Caption         =   " Emisor"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Moneda 
         Height          =   315
         Left            =   150
         TabIndex        =   34
         Top             =   960
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   556
         LabelWidth      =   1350
         Caption         =   " Moneda"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Cantidad 
         Height          =   315
         Left            =   5670
         TabIndex        =   35
         Top             =   960
         Width           =   3045
         _ExtentX        =   5371
         _ExtentY        =   556
         LabelWidth      =   1500
         Caption         =   " Nominales"
         Text            =   "0.0000"
         Text            =   "0.0000"
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Format          =   "#,##0.0000"
         Tipo_TextBox    =   1
      End
      Begin hControl2.hTextLabel Txt_Fecha_Vencimiento 
         Height          =   315
         Left            =   5670
         TabIndex        =   36
         Top             =   270
         Width           =   3045
         _ExtentX        =   5371
         _ExtentY        =   556
         LabelWidth      =   1500
         Caption         =   " Fecha Vencimiento"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Fecha_Emision 
         Height          =   315
         Left            =   5670
         TabIndex        =   37
         Top             =   630
         Width           =   3045
         _ExtentX        =   5371
         _ExtentY        =   556
         LabelWidth      =   1500
         Caption         =   " Fecha Emisi�n"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Nemotecnico 
         Height          =   315
         Left            =   150
         TabIndex        =   51
         Tag             =   "OBLI=S"
         Top             =   270
         Width           =   3525
         _ExtentX        =   6218
         _ExtentY        =   556
         LabelWidth      =   1350
         Caption         =   " Nemot�cnico"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin VB.Label lbl_nemotecnico 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Nemot�nico"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   150
         TabIndex        =   52
         Top             =   270
         Width           =   1365
      End
   End
   Begin VB.Frame Frame_Principal 
      Caption         =   "Datos Operaci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4845
      Left            =   45
      TabIndex        =   5
      Top             =   1440
      Width           =   8820
      Begin VB.Frame Frame_Comisiones 
         Height          =   1005
         Left            =   120
         TabIndex        =   7
         Top             =   1710
         Width           =   8565
         Begin hControl2.hTextLabel Txt_Porcentaje_Comision 
            Height          =   315
            Left            =   120
            TabIndex        =   8
            Tag             =   "OBLI"
            Top             =   210
            Width           =   2055
            _ExtentX        =   3625
            _ExtentY        =   556
            LabelWidth      =   1400
            TextMinWidth    =   500
            Caption         =   "Comisi�n (%)"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.0000"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Gastos 
            Height          =   315
            Left            =   5370
            TabIndex        =   9
            Tag             =   "OBLI"
            Top             =   210
            Width           =   3075
            _ExtentX        =   5424
            _ExtentY        =   556
            LabelWidth      =   1400
            TextMinWidth    =   500
            Caption         =   "Gastos"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Derechos 
            Height          =   315
            Left            =   2250
            TabIndex        =   10
            Tag             =   "OBLI"
            Top             =   570
            Width           =   3045
            _ExtentX        =   5371
            _ExtentY        =   556
            LabelWidth      =   1400
            TextMinWidth    =   500
            Caption         =   "Derechos Bolsa"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Iva 
            Height          =   315
            Left            =   5370
            TabIndex        =   11
            Tag             =   "OBLI"
            Top             =   570
            Width           =   3075
            _ExtentX        =   5424
            _ExtentY        =   556
            LabelWidth      =   1400
            TextMinWidth    =   500
            Caption         =   " Iva (%)"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Comision 
            Height          =   315
            Left            =   2250
            TabIndex        =   12
            Tag             =   "OBLI"
            Top             =   210
            Width           =   3045
            _ExtentX        =   5371
            _ExtentY        =   556
            LabelWidth      =   1400
            TextMinWidth    =   500
            Caption         =   "Comisi�n a Cobrar"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Porcentaje_Derechos 
            Height          =   315
            Left            =   120
            TabIndex        =   13
            Tag             =   "OBLI"
            Top             =   570
            Width           =   2055
            _ExtentX        =   3625
            _ExtentY        =   556
            LabelWidth      =   1400
            TextMinWidth    =   500
            Caption         =   "Derechos (%)"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.0000"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
      End
      Begin VB.CheckBox chkAporteRetiro 
         Caption         =   "� Aporte/Retiro de Capital ?"
         Height          =   375
         Left            =   3480
         TabIndex        =   6
         Top             =   210
         Value           =   1  'Checked
         Width           =   1815
      End
      Begin hControl2.hTextLabel Txt_Num_Operacion 
         Height          =   315
         Left            =   150
         TabIndex        =   14
         Top             =   270
         Width           =   3285
         _ExtentX        =   5794
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   " N� Operaci�n"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_FechaIngreso_Real 
         Height          =   315
         Left            =   5490
         TabIndex        =   15
         Top             =   270
         Width           =   3165
         _ExtentX        =   5583
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Fecha Operaci�n"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   1905
         Left            =   120
         TabIndex        =   16
         Top             =   2820
         Width           =   8115
         _cx             =   14314
         _cy             =   3360
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   15
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Operacion_Bonos_Nac_II.frx":03B4
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Grilla 
         Height          =   660
         Left            =   8280
         TabIndex        =   17
         Top             =   3060
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1164
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "ADD"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Agregar un nemotecnico a la Operaci�n"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "DEL"
               Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
               Object.ToolTipText     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
            EndProperty
         EndProperty
      End
      Begin hControl2.hTextLabel Txt_MontoTotal 
         Height          =   315
         Left            =   5070
         TabIndex        =   18
         Top             =   1020
         Width           =   3600
         _ExtentX        =   6350
         _ExtentY        =   556
         LabelWidth      =   1605
         Caption         =   "Monto Total Aprox."
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin MSComCtl2.DTPicker Dtp_FechaLiquidacion 
         Height          =   345
         Left            =   7410
         TabIndex        =   19
         Tag             =   "OBLI"
         ToolTipText     =   "Permite solo dias h�biles"
         Top             =   1380
         Width           =   1275
         _ExtentX        =   2249
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   58982401
         CurrentDate     =   38768
      End
      Begin TrueDBList80.TDBCombo Cmb_Contraparte 
         Height          =   345
         Left            =   1440
         TabIndex        =   20
         Top             =   630
         Width           =   3585
         _ExtentX        =   6324
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Operacion_Bonos_Nac_II.frx":068C
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Representantes 
         Height          =   345
         Left            =   1440
         TabIndex        =   21
         Top             =   1380
         Width           =   3585
         _ExtentX        =   6324
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Operacion_Bonos_Nac_II.frx":0736
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_FechaLiquidacion 
         Height          =   345
         Left            =   6690
         TabIndex        =   22
         Top             =   1380
         Width           =   705
         _ExtentX        =   1244
         _ExtentY        =   609
         _LayoutType     =   4
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   1
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=1"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Operacion_Bonos_Nac_II.frx":07E0
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Named:id=33:Normal"
         _StyleDefs(35)  =   ":id=33,.parent=0"
         _StyleDefs(36)  =   "Named:id=34:Heading"
         _StyleDefs(37)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(38)  =   ":id=34,.wraptext=-1"
         _StyleDefs(39)  =   "Named:id=35:Footing"
         _StyleDefs(40)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(41)  =   "Named:id=36:Selected"
         _StyleDefs(42)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(43)  =   "Named:id=37:Caption"
         _StyleDefs(44)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(45)  =   "Named:id=38:HighlightRow"
         _StyleDefs(46)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=39:EvenRow"
         _StyleDefs(48)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(49)  =   "Named:id=40:OddRow"
         _StyleDefs(50)  =   ":id=40,.parent=33"
         _StyleDefs(51)  =   "Named:id=41:RecordSelector"
         _StyleDefs(52)  =   ":id=41,.parent=34"
         _StyleDefs(53)  =   "Named:id=42:FilterBar"
         _StyleDefs(54)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Traders 
         Height          =   345
         Left            =   1440
         TabIndex        =   23
         Top             =   1020
         Width           =   3585
         _ExtentX        =   6324
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Operacion_Bonos_Nac_II.frx":088A
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Operacion 
         Height          =   345
         Left            =   6810
         TabIndex        =   24
         Tag             =   "OBLI"
         Top             =   270
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   58982401
         CurrentDate     =   38768
      End
      Begin VB.Label Label5 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Contraparte"
         Height          =   345
         Left            =   150
         TabIndex        =   29
         Top             =   600
         Width           =   1275
      End
      Begin VB.Label lbl_Representante 
         BorderStyle     =   1  'Fixed Single
         Caption         =   " Representantes"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   150
         TabIndex        =   28
         Top             =   1380
         Width           =   1275
      End
      Begin VB.Label Lbl_FechaLiquidacion 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Liquidaci�n T+"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   5070
         TabIndex        =   27
         Top             =   1380
         Width           =   1605
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Trader"
         Height          =   345
         Left            =   150
         TabIndex        =   26
         Top             =   990
         Width           =   1275
      End
      Begin VB.Label lbl_fecha_ingreso 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Operaci�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   5490
         TabIndex        =   25
         Top             =   270
         Width           =   1305
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Cliente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1035
      Left            =   60
      TabIndex        =   0
      Top             =   390
      Width           =   8805
      Begin hControl2.hTextLabel Txt_Rut 
         Height          =   315
         Left            =   150
         TabIndex        =   1
         Top             =   240
         Width           =   2490
         _ExtentX        =   4392
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1200
         Caption         =   "RUT"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Nombres 
         Height          =   315
         Left            =   2700
         TabIndex        =   2
         Top             =   240
         Width           =   6015
         _ExtentX        =   10610
         _ExtentY        =   556
         LabelWidth      =   1000
         Caption         =   "Nombres"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Perfil 
         Height          =   315
         Left            =   2700
         TabIndex        =   3
         Top             =   600
         Width           =   3300
         _ExtentX        =   5821
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1200
         Caption         =   "Perfil Riesgo"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   315
         Left            =   150
         TabIndex        =   4
         Top             =   600
         Width           =   2490
         _ExtentX        =   4392
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1200
         Caption         =   "Cuenta"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   53
      Top             =   0
      Width           =   8940
      _ExtentX        =   15769
      _ExtentY        =   635
      ButtonWidth     =   1746
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   4470
         TabIndex        =   54
         Top             =   0
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Operacion_Bonos_Nac_II"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
'-------------------------------------
Rem PARA CONFIRMACION
Dim fcOperaciones As Class_Operaciones
Dim fId_Operacion As String
Dim fForm_Confirmacion As Boolean
Dim fId_Mov_Activo As String
'------------------------------------
Rem PARA CONSULTA DE OPERACION
Dim fConsulta_Operacion As Boolean
'------------------------------------
Dim fSalir               As Boolean
Dim fEstadoOK            As Boolean
Dim fFlg_Tipo_Movimiento As String
Dim fId_Cuenta           As String
Dim fId_Cliente          As Double
Dim fTir_Decimal         As Integer
Dim fValorPar_Decimal    As Integer
Dim fFormOri             As Form
Dim fTipo_Operacion      As String
Dim fCorteMin            As Double
Dim fModif_Grilla        As Boolean
Dim fId_Nemotecnico
Dim fEmisor              As Long
Dim fValor_Iva           As Double
Dim fFecha_Operacion     As Date

Const fc_Mercado = cMercado_Nacional

Private Enum eNem_Colum
  eNem_nemotecnico
  eNem_Cantidad
  eNem_Descripcion
  eNem_Id_Nemotecnico
  eNem_Id_Mov_Activo
  eNem_Corte
End Enum

Private Enum eTipo
  eT_Normal
  eT_Grande
End Enum

'Esta variable contiene el monto valorizado a la tasa nominal
Dim fMonto_Nominal As Double
'Este es la variable que contiene el codigo del instrumento que fue
'iniciado para operar
Dim fCod_Instrumento As String

Private Sub cmb_buscar_Click()
    If fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
      Rem Modificado CSM 06/12/2007, se comentaron estas lineas
      'If fTipo_Operacion = gcOPERACION_Directa Or fTipo_Operacion = gcOPERACION_Custodia Then
        fId_Nemotecnico = frm_Busca_Nemotecnicos_II.Buscar(pCod_Instrumento:=fCod_Instrumento, _
                                                        pNemotecnico:=Txt_Nemotecnico.Text, _
                                                        pCodOperacion:=fFlg_Tipo_Movimiento)
      'End If
    Else
      fId_Nemotecnico = frm_Busca_Nemotecnicos_II.Buscar(pCod_Instrumento:=fCod_Instrumento, _
                                                      pNemotecnico:=Txt_Nemotecnico.Text, _
                                                      pCodOperacion:=fFlg_Tipo_Movimiento, _
                                                      pId_Cuenta:=fId_Cuenta, _
                                                      pFecha_Movimiento:=fFecha_Operacion, _
                                                      pId_Mov_Activo:=fId_Mov_Activo)
    End If
    
    If Not IsNull(fId_Nemotecnico) Then
      If Not fModif_Grilla Then
        Call Sub_Llena_Nemotecnico(fId_Nemotecnico)
        Txt_Cantidad.Text = frm_Busca_Nemotecnicos_II.fCantidad
      End If
    End If
End Sub

Private Sub Cmb_Contraparte_ItemChange()
    Dim lId_Contraparte As String
  
    lId_Contraparte = Fnt_FindValue4Display(Cmb_Contraparte, Cmb_Contraparte.Text)
  
    If lId_Contraparte = "" Then
        'como nunca va a existir la contraparte -1 se pasa el parametro
        lId_Contraparte = "-1"
    End If
  
    Call Sub_CargaCombo_Traders(Cmb_Traders, lId_Contraparte)
End Sub

Private Sub Chk_Vende_Todo_Click()
  If Chk_Vende_Todo.Value Then
    Rem ---------------------------------------------
    Txt_CantidadInversion.Locked = True
    Txt_CantidadInversion.Text = Txt_Cantidad.Text
    Txt_CantidadInversion.BackColorTxt = fColorNoEdit
    Rem ---------------------------------------------
    Txt_Monto_Inversion.Locked = True
    Txt_Monto_Inversion.BackColorTxt = fColorNoEdit
    Rem ---------------------------------------------
    Txt_Utilidad.Locked = True
    Txt_Utilidad.BackColorTxt = fColorNoEdit
    Rem ---------------------------------------------
    Call Sub_ValorizaPapel
  Else
    Rem ---------------------------------------------
    Txt_CantidadInversion.Locked = False
    Txt_CantidadInversion.Text = 0
    Txt_CantidadInversion.BackColorTxt = fColorOBligatorio
    Rem ---------------------------------------------
    Txt_Monto_Inversion.Locked = False
    Txt_Monto_Inversion.Text = 0
    Txt_Monto_Inversion.BackColorTxt = fColorOBligatorio
    Rem ---------------------------------------------
    Txt_Utilidad.Locked = False
    Txt_Utilidad.BackColorTxt = fColorOBligatorio
  End If

End Sub
Private Sub Cmb_FechaLiquidacion_Change()
  If Not Cmb_FechaLiquidacion.Text = "" Then
    Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Cmb_FechaLiquidacion.Text)
  End If
End Sub

Private Sub Cmb_FechaLiquidacion_ItemChange()
  If Not Cmb_FechaLiquidacion.Text = "" Then
    Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Cmb_FechaLiquidacion.Text)
  End If
End Sub

Private Sub Cmb_FechaLiquidacion_KeyPress(KeyAscii As Integer)
  Rem Acepta digitos, ".", "-" y "backspace"
  If Not KeyAscii = 8 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

'Private Sub Cmb_Nemotecnico_ItemChange()
'Dim lId_Nemotecnico As String
'
'  lId_Nemotecnico = Fnt_FindValue4Display(Cmb_Nemotecnico, Cmb_Nemotecnico.Text)
'
'  If Not lId_Nemotecnico = "" Then
'    Rem Validaci�n: No se puede ingresar una instrucci�n para un mismo nemot�cnico
''    If Fnt_Busca_Nemotecnico(lId_Nemotecnico, fModif_Grilla) Then
''      MsgBox "El Nemot�cnico " & Cmb_Nemotecnico.Text & " ya existe en la grilla.", vbCritical, Me.Caption
''      Cmb_Nemotecnico.Text = ""
''      Exit Sub
''    End If
'    If Not fModif_Grilla Then
'      Call Sub_Llena_Nemotecnico(lId_Nemotecnico)
'    End If
'  End If
'End Sub

'Private Function Fnt_Busca_Nemotecnico(pId_Nemotecnico As String, _
'                                       Optional Pmodif_grilla As Boolean = False) As Boolean
'Dim lFila As Long
'
'  Fnt_Busca_Nemotecnico = False
'
'  With Grilla
'    If .Rows > 0 Then
'      If Pmodif_grilla Then
'        For lFila = 1 To (.Rows - 1)
'          If Not GetCell(Grilla, lFila, "id_nemotecnico") = fId_Nemotecnico And _
'                 GetCell(Grilla, lFila, "id_nemotecnico") = pId_Nemotecnico Then
'            Fnt_Busca_Nemotecnico = True
'            Exit For
'          End If
'        Next
'      Else
'        For lFila = 1 To (.Rows - 1)
'          If GetCell(Grilla, lFila, "id_nemotecnico") = pId_Nemotecnico Then
'            Fnt_Busca_Nemotecnico = True
'            Exit For
'          End If
'        Next
'      End If
'    End If
'  End With
'End Function

Private Sub Sub_Llena_Nemotecnico(pId_Nemotecnico)
Dim lcNemotecnico As Class_Nemotecnicos
Dim lReg As hFields
Dim lBonos As Class_Bonos_II
Dim lSaldo_Cantidad As Double
Dim lFila As Long
Dim lCod_Instrumento As String
'Dim pIdMovimientoActivo

  Set lcNemotecnico = New Class_Nemotecnicos
  
  With lcNemotecnico
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    If .BuscarView Then
      For Each lReg In .Cursor
        For lFila = 1 To Grilla.Rows - 1
            If Not lReg("id_moneda_transaccion").Value = To_Number(GetCell(Grilla, lFila, "id_moneda")) Then
                MsgBox "La moneda debe ser la misma que la de los nemot�cnicos ya ingresados.", vbCritical, Me.Caption
                Exit Sub
            End If
        Next
        
        fEmisor = lReg("id_emisor_especifico").Value
        Txt_Emisor.Text = Trim(lReg("dsc_emisor_especifico").Value)
        Txt_Nemotecnico.Text = Trim(lReg("nemotecnico").Value)
        lCod_Instrumento = Trim(lReg("Cod_Instrumento").Value)
        'pIdMovimientoActivo = lReg("nemotecnico").Value
        
        With Txt_Moneda
            .Text = NVL(lReg("dsc_moneda_transaccion").Value, "")
            .Tag = NVL(lReg("id_moneda_transaccion").Value, "")
           
            Txt_Monto_Inversion.Format = Fnt_Formato_Moneda(.Tag)
            Txt_Utilidad.Format = Fnt_Formato_Moneda(.Tag)
        End With
        
        Txt_Fecha_Vencimiento.Text = Format(lReg("fecha_vencimiento").Value, cFormatDate)
        Txt_Fecha_Emision.Text = Format(lReg("fecha_emision").Value, cFormatDate)
        fCorteMin = NVL(lReg("Corte_minimo_papel").Value, "0")
      Next
    End If
  End With

  If fFlg_Tipo_Movimiento = gcTipoOperacion_Egreso Then
    'Si se esta realizando una venta del activo
    
    Set lBonos = New Class_Bonos_II
    lSaldo_Cantidad = lBonos.Saldo_Activo_Cantidad(fId_Cuenta, pId_Nemotecnico, fId_Mov_Activo, lCod_Instrumento:=lCod_Instrumento)
    Set lBonos = Nothing
      
'    Txt_Cantidad.Text = lSaldo_Cantidad
  End If
End Sub

Private Sub Cmb_Nemotecnico_Validate(Cancel As Boolean)
  If Not Cmb_Nemotecnico.Text = "" Then
    Rem si el nemotecnico no es NULL
    If Fnt_FindValue4Display(Cmb_Nemotecnico, Cmb_Nemotecnico.Text) = "" Then
      MsgBox "El nemot�cnico ingresado no existe.", vbCritical, Me.Caption
      Cancel = True
      ' Cmb_Nemotecnico.Text = ""
    End If
  End If
End Sub

Private Sub DTP_Fecha_Operacion_Change()
  fFecha_Operacion = DTP_Fecha_Operacion.Value
  'If Not Cmb_FechaLiquidacion.Text = "" Then
    'Dtp_FechaLiquidacion.MinDate = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
    Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Cmb_FechaLiquidacion.Text)
  'End If
End Sub

Private Sub Dtp_FechaLiquidacion_Change()
  Dtp_FechaLiquidacion.Value = Fnt_Dia_Habil_MasProximo(Dtp_FechaLiquidacion.Value)
  Cmb_FechaLiquidacion.Text = Fnt_DiasHabiles_EntreFechas(fFecha_Operacion, Dtp_FechaLiquidacion)
End Sub

Private Sub Grilla_Click()
Rem EVENTO SOLO PARA CONFIRMACION
'Dim lId_Nemotecnico As String
  
  Call Sub_Bloquea_Puntero(Me)
  If Not fConsulta_Operacion Then
    With Grilla
      If .Row > 0 Then
        Call Sub_FormTama�o(eT_Grande, fTipo_Operacion)
        fId_Nemotecnico = GetCell(Grilla, .Row, "id_nemotecnico")
        Txt_Nemotecnico.Tag = fId_Nemotecnico
        Txt_Nemotecnico.Text = GetCell(Grilla, .Row, "dsc_nemotecnico")
        Cmb_Nemotecnico.Text = GetCell(Grilla, .Row, "dsc_nemotecnico")
        fId_Mov_Activo = GetCell(Grilla, .Row, "id_mov_activo")
        
        Call Sub_Llena_Nemotecnico(fId_Nemotecnico)
        
        Txt_Monto_Inversion.Text = GetCell(Grilla, .Row, "monto")
        Txt_TasaCompra.Text = GetCell(Grilla, .Row, "tasa")
        Txt_Tasa_Historico.Text = GetCell(Grilla, .Row, "tasa_historica")
        Txt_TasaNominal.Text = GetCell(Grilla, .Row, "tasa_transferencia")
        Txt_CantidadInversion.Text = GetCell(Grilla, .Row, "Cantidad")
        Txt_Cantidad.Text = GetCell(Grilla, .Row, "cantidad_saldo")
        Txt_Utilidad.Text = GetCell(Grilla, .Row, "utilidad")
        fModif_Grilla = True
      End If
    End With
  End If
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
'      .Buttons("REFRESH").Image = cBoton_Original
  End With

  With Toolbar_Grilla
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Appearance = ccFlat
  End With
  
  With Toolbar_Valorizar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("VALORIZA").Image = "boton_valorizar"
      .Appearance = ccFlat
  End With
  
  With Toolbar_Operacion
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("OK").Image = cBoton_Aceptar
      .Buttons("CANCEL").Image = cBoton_Cancelar
      .Appearance = ccFlat
  End With
  
  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      Rem PREGUNTA SI LA PANTALLA PADRE ES LA DE "CONFIRMACION DE INSTRUCCIONES"
      If fForm_Confirmacion Then
        If Fnt_Grabar_Confirmacion Then
          fEstadoOK = True
          fSalir = True
        End If
      Else
        If Fnt_Grabar Then
          'Si no hubo problemas al grabar, sale
          fEstadoOK = True
          fSalir = True
          'Unload Me
        End If
      End If
    Case "EXIT"
      fEstadoOK = False
      fSalir = True
      'Unload Me
  End Select
End Sub

Private Sub Sub_CargaForm()
'Dim lcCuenta As New Class_Cuentas
Dim lcCuenta As Object
Dim lcParametros As Object
Dim lcIva As Class_Iva
Dim lcComisiones As Class_Comisiones_Instrumentos
'---------------------------------------------------------------
Dim lcRel_Contrapartes_Instrum As Class_Rel_Contrapartes_Instrum
Dim lTexto As String
Dim lReg As hFields

  Call Sub_Bloquea_Puntero(Me)
  
  Call Sub_FormControl_Color(Me.Controls)

  Call Sub_FormTama�o(eT_Normal, fTipo_Operacion)

  Call Sub_Setea_Comisiones
  
  Call Sub_Carga_Datos_Cliente
  
  Grilla.Rows = 1
  
  '------------------------------------------------
  '-- Carga el dias liquidacion
  '------------------------------------------------
'  With Cmb_FechaLiquidacion.ComboItems
'    .Clear
'    Call .Add(, "K" & "0", "0")
'    Call .Add(, "K" & "1", "1")
'    Call .Add(, "K" & "2", "2")
'    Call .Add(, "K" & "3", "3")
'    Call .Add(, "K" & "4", "4")
'    Call .Add(, "K" & "5", "5")
'  End With
  
  With Cmb_FechaLiquidacion
    Call .AddItem("0")
    Call .AddItem("1")
    Call .AddItem("2")
    Call .AddItem("3")
    Call .AddItem("4")
    Call .AddItem("5")
    
    With .Columns(0).ValueItems
      .Add Fnt_AgregaValueItem("0", "0")
      .Add Fnt_AgregaValueItem("1", "1")
      .Add Fnt_AgregaValueItem("2", "2")
      .Add Fnt_AgregaValueItem("3", "3")
      .Add Fnt_AgregaValueItem("4", "4")
      .Add Fnt_AgregaValueItem("5", "5")
      .Translate = True
    End With
    
    .Text = ""
    .SelectedItem = 0
  End With
  
  '------------------------------------------------
  '-- Carga Sis_Parametros
  '------------------------------------------------
  
  Set lcParametros = Fnt_CreateObject(cDLL_SisParametro)

  fTir_Decimal = lcParametros.ConsultaValorNumero(0, "RF_TIR_DECIMAL")
  Txt_TasaCompra.Format = Mid(Txt_TasaCompra.Format, 1, InStr(1, Txt_TasaCompra.Format, ".")) & _
                          String(fTir_Decimal, Mid(Txt_TasaCompra.Format, InStr(1, Txt_TasaCompra.Format, ".") + 1, 1))
  Txt_TasaNominal.Format = Mid(Txt_TasaNominal.Format, 1, InStr(1, Txt_TasaNominal.Format, ".")) & _
                           String(fTir_Decimal, Mid(Txt_TasaNominal.Format, InStr(1, Txt_TasaNominal.Format, ".") + 1, 1))
  Txt_Tasa_Historico.Format = Mid(Txt_Tasa_Historico.Format, 1, InStr(1, Txt_Tasa_Historico.Format, ".")) & _
                              String(fTir_Decimal, Mid(Txt_Tasa_Historico.Format, InStr(1, Txt_Tasa_Historico.Format, ".") + 1, 1))

  Grilla.ColFormat(Grilla.ColIndex("tasa")) = Mid(Grilla.ColFormat(Grilla.ColIndex("tasa")), 1, InStr(1, Grilla.ColFormat(Grilla.ColIndex("tasa")), ".")) & _
                                              String(fTir_Decimal, Mid(Grilla.ColFormat(Grilla.ColIndex("tasa")), InStr(1, Grilla.ColFormat(Grilla.ColIndex("tasa")), ".") + 1, 1))

                          
  '------------------------------------------------
  
  '------------------------------------------------
  '-- Carga el ID del Cliente
  '------------------------------------------------
  
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  
  lcCuenta.Campo("id_cuenta").Valor = fId_Cuenta
  If lcCuenta.Buscar_Vigentes Then
    If lcCuenta.Cursor.Count > 0 Then
      fId_Cliente = lcCuenta.Cursor(1)("id_cliente").Value
    End If
  End If
  '------------------------------------------------

  '------------------------------------------------
  '-- Carga Contrapartes segun instrumento asociado
  '------------------------------------------------
  'Call Sub_CargaCombo_Contrapartes(Cmb_Contraparte)
  Call Sub_CargaCombo_Rel_Contrapartes_Instrum(Cmb_Contraparte, fCod_Instrumento)
  
'  With Cmb_Contraparte
'    .Text = ""
'    .ClearFields
'    .Clear
'    .EmptyRows = True
'
'    Call .Columns.Remove(1)
'    With .Columns(0).ValueItems
'      .Clear
'      .Translate = False
'    End With
'
'    Set lcRel_Contrapartes_Instrum = New Class_Rel_Contrapartes_Instrum
'    lcRel_Contrapartes_Instrum.Campo("cod_instrumento").Valor = fCod_Instrumento
'    If lcRel_Contrapartes_Instrum.BuscarView Then
'      For Each lReg In lcRel_Contrapartes_Instrum.Cursor
'        lTexto = ""
'
'        If Not gRelogDB Is Nothing Then
'          gRelogDB.AvanzaRelog
'        End If
'
'        lTexto = lReg("DSC_CONTRAPARTE").Value
'
'        .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("ID_CONTRAPARTE").Value, lTexto)
'
'        Call .AddItem(lTexto)
'
'      Next
'    End If
'    Set lcRel_Contrapartes_Instrum = Nothing
'  End With
  
  '------------------------------------------------
  '-- Carga Representante
  '------------------------------------------------
  Call Sub_CargaCombo_Representantes(Cmb_Representantes, fId_Cliente)
  
  If Not fTipo_Operacion = gcOPERACION_Custodia Then
    
    Rem Valor Iva del Sistema
    Set lcIva = New Class_Iva
    With lcIva
      If .Buscar(True) Then
        fValor_Iva = .Cursor(1)("valor").Value
        Txt_Iva.Caption = " Iva (a " & .Porcentaje_Iva(fValor_Iva) & "%)"
      End If
    End With
    
    Rem Comisiones
    Set lcComisiones = New Class_Comisiones_Instrumentos
    With lcComisiones
      .Campo("Id_Cuenta").Valor = fId_Cuenta
      .Campo("COD_INSTRUMENTO").Valor = fCod_Instrumento
      If .Buscar(True) Then
        If .Cursor.Count > 0 Then
          Txt_Porcentaje_Comision.Text = lcIva.Porcentaje_Iva(.Cursor(1)("COMISION").Value)
          
          Txt_Gastos.Text = Int(.Cursor(1)("GASTOS").Value)
          'fDerechosBolsa = NVL(.Cursor(1)("DERECHOS_BOLSA").Value, 0)
          Txt_Porcentaje_Derechos.Text = lcIva.Porcentaje_Iva(NVL(.Cursor(1)("DERECHOS_BOLSA").Value, 0))
        End If
      End If
    End With
    
    Set lcComisiones = Nothing
    Set lcIva = Nothing
      
  End If
  
  ' Si la operacion es custodia, muestra el Check de Aporte o Retiro
  If fTipo_Operacion = gcOPERACION_Custodia Then
    chkAporteRetiro.Value = 1
    chkAporteRetiro.Visible = True
  Else
    chkAporteRetiro.Value = 0
    chkAporteRetiro.Visible = False
  End If

      
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Carga_Datos_Cliente()
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object

'  Set lcCuenta = New Class_Cuentas
  
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  
  With lcCuenta
    .Campo("id_cuenta").Valor = fId_Cuenta
    If .Buscar_Vigentes Then
      If .Cursor.Count > 0 Then
        Txt_Rut.Text = "" & .Cursor(1)("rut_cliente").Value
        Txt_Nombres.Text = "" & .Cursor(1)("nombre_cliente").Value
        Txt_Num_Cuenta.Text = "" & .Cursor(1)("num_cuenta").Value
        Txt_Perfil.Text = "" & .Cursor(1)("dsc_perfil_riesgo").Value
      End If
    Else
      MsgBox "Problemas en cargar la Cuenta." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcCuenta = Nothing
  
End Sub

Private Sub Sub_CargarDatos()
Dim lcTipo_Liq As Class_Tipos_Liquidacion

  Call Sub_Bloquea_Puntero(Me)
  
  fCorteMin = 0
  
  '------------------------------------------------
  '-- Setea Fechas
  '------------------------------------------------
  fFecha_Operacion = Fnt_FechaServidor
  Txt_FechaIngreso_Real.Text = fFecha_Operacion
  DTP_Fecha_Operacion.Value = fFecha_Operacion
  
  If fTipo_Operacion = gcOPERACION_Custodia Then
    Dtp_FechaLiquidacion.Value = fFecha_Operacion
  Else
  
    'Dtp_FechaLiquidacion.MinDate = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
    
    Set lcTipo_Liq = New Class_Tipos_Liquidacion
    With lcTipo_Liq
      .Campo("cod_instrumento").Valor = fCod_Instrumento
      .Campo("id_empresa").Valor = Fnt_EmpresaActual
      .Campo("tipo_movimiento").Valor = fFlg_Tipo_Movimiento
      If .Buscar Then
        If .Cursor.Count > 0 Then
          Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, NVL(.Cursor(1).Fields("retencion").Value, 0))
          Cmb_FechaLiquidacion.Text = NVL(.Cursor(1).Fields("retencion").Value, 0)
        Else
          Dtp_FechaLiquidacion.Value = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
        End If
      Else
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en carga de Tipo Liquidacion.", _
                          .ErrMsg, _
                          pConLog:=True)
        Dtp_FechaLiquidacion.Value = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
      End If
    End With
    Set lcTipo_Liq = Nothing
    
  End If
    
    'Jorge Vidal
   ' Call Sub_Carga_Nemotecnicos
    
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Setea_Comisiones()
  Txt_Porcentaje_Comision.Text = 0
  Txt_Comision.Text = 0
  Txt_Iva.Text = 0
  Txt_Gastos.Text = 0
  Txt_Derechos.Text = 0
End Sub

Private Sub Sub_Carga_Nemotecnicos()
    Dim lReg As hCollection.hFields
    Dim lLinea As Long
    Dim lNemotecnicos As Class_Nemotecnicos
    Dim lSaldos_Activos As Class_Saldo_Activos
    
    '------------------------------------------------
    '-- Carga los nemotecnicos
    '------------------------------------------------
    Call Sub_LimpiarTDBCombo(Cmb_Nemotecnico)
  
  With Cmb_Nemotecnico
    With .Columns.Add(eNem_nemotecnico)
      .Caption = "Nemotecnico"
      .Visible = True
    End With
    
    With .Columns.Add(eNem_Cantidad)
      .Caption = "Cantidad"
      .Visible = (fFlg_Tipo_Movimiento = gcTipoOperacion_Egreso)
    End With
    
    With .Columns.Add(eNem_Descripcion)
      .Caption = "Descripcion"
      .Visible = True
    End With
    
    With .Columns.Add(eNem_Id_Nemotecnico)
      .Caption = "id_nemotecnico"
      .Visible = False
    End With
    With .Columns.Add(eNem_Id_Mov_Activo)
      .Caption = "id_mov_activo"
      .Visible = False
    End With
    
    With .Columns.Add(eNem_Corte)
      .Caption = "corte"
      .Visible = False
    End With
    
    If fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
    
      'Si se estan realizando COMPRAS
      Set lNemotecnicos = New Class_Nemotecnicos
      
      If fTipo_Operacion = gcOPERACION_Directa Or fTipo_Operacion = gcOPERACION_Custodia Then
        lNemotecnicos.Campo("cod_instrumento").Valor = fCod_Instrumento
        
        If lNemotecnicos.Llena_Combo() Then
        
          For Each lReg In lNemotecnicos.Cursor
                Call .AddItem(lReg("nemotecnico").Value & ";0;" & lReg("dsc_nemotecnico").Value & ";" & lReg("id_nemotecnico").Value & ";;" & NVL(lReg("corte_minimo_papel").Value, 0))
                .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("id_nemotecnico").Value, lReg("nemotecnico").Value)
                      
                If lReg.Index / 100 = Int(lReg.Index / 100) Then
                    DoEvents
                End If
          Next
          
        End If
        
      Else
      
        If lNemotecnicos.BuscarVigentes(pCod_Instrumento:=gcINST_BONOS_NAC) Then
            For Each lReg In lNemotecnicos.Cursor
                Call .AddItem(lReg("nemotecnico").Value & ";0;" & lReg("dsc_nemotecnico").Value & ";" & lReg("id_nemotecnico").Value & ";;" & NVL(lReg("corte_minimo_papel").Value, 0))
                .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("id_nemotecnico").Value, lReg("nemotecnico").Value)
            Next
          
        End If
      End If
    Else
    
      Set lSaldos_Activos = New Class_Saldo_Activos
      If lSaldos_Activos.Buscar_Ultimo_CierreCuenta(pId_Cuenta:=fId_Cuenta, pCod_Instrumento:=gcINST_BONOS_NAC) Then
        For Each lReg In lSaldos_Activos.Cursor
          Call .AddItem(lReg("nemotecnico").Value & ";" & lReg("cantidad").Value & ";" & lReg("dsc_nemotecnico").Value & ";" & lReg("id_nemotecnico").Value & ";" & lReg("id_mov_activo").Value & ";" & NVL(lReg("corte_minimo_papel").Value, 0))
          
          .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("id_nemotecnico").Value, lReg("nemotecnico").Value)
        Next
      End If
      
    End If
    
  End With
  '------------------------------------------------
End Sub

Private Function Fnt_ValidarDatos() As Boolean
Dim lLinea As Long
Dim pMsgError
Dim lcCuenta As Object
  Fnt_ValidarDatos = False
    
  Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
  
  lcCuenta.Campo("id_cuenta").Valor = fId_Cuenta
  If lcCuenta.Cuenta_Bloqueada(pMsgError) Then
    Select Case fTipo_Operacion
        Case gcOPERACION_Instruccion
            MsgBox "Cuenta Bloqueada. Motivo : " & pMsgError, vbExclamation, Me.Caption
            GoTo ExitProcedure
        Case gcOPERACION_Directa
            pMsgError = pMsgError & vbCr & vbCr & "�Desea continuar con la operaci�n?"
            If MsgBox("Cuenta Bloqueada. Motivo : " & pMsgError, vbQuestion + vbYesNo, Me.Caption) = vbNo Then
                GoTo ExitProcedure
            End If
    End Select
  End If
  
  If Not Fnt_Form_Validar(Me.Controls, Frame_Principal) Then
    GoTo ExitProcedure
  End If
    
  If Fnt_Verifica_Feriado(fFecha_Operacion) Then
    MsgBox "Solo se pueden ingresar operaciones en d�as h�biles.", vbExclamation, Me.Caption
    GoTo ExitProcedure
  End If
    
  If Txt_Porcentaje_Comision.Text < 0 Then
    MsgBox "Porcentaje Comisi�n no puede ser menor a cero.", vbExclamation, Me.Caption
    GoTo ExitProcedure
  ElseIf Txt_Comision.Text < 0 Then
    MsgBox "Comisi�n a Cobrar no puede ser menor a cero.", vbExclamation, Me.Caption
    GoTo ExitProcedure
  ElseIf Txt_Iva.Text < 0 Then
    MsgBox "El Valor Iva no puede ser menor a cero.", vbExclamation, Me.Caption
    GoTo ExitProcedure
  ElseIf Txt_Gastos.Text < 0 Then
    MsgBox "Gastos no puede ser menor a cero.", vbExclamation, Me.Caption
    GoTo ExitProcedure
  ElseIf Txt_Derechos.Text < 0 Then
    MsgBox "Derechos Gastos no puede ser menor a cero.", vbExclamation, Me.Caption
    GoTo ExitProcedure
  End If
  
  If Grilla.Rows <= 1 Then
    MsgBox "Para realizar una operacion minimo debe tener 1 detalle.", vbInformation, Me.Caption
    GoTo ExitProcedure
  End If
  
  For lLinea = 1 To (Grilla.Rows - 1)
    If Not Fnt_Validad_Operacion(pCod_Tipo_Operacion:=fTipo_Operacion _
                               , pFlg_Tipo_Movimiento:=fFlg_Tipo_Movimiento _
                               , pId_Cuenta:=fId_Cuenta _
                               , pId_Emisor:=fEmisor _
                               , pId_Nemotecnico:=GetCell(Grilla, lLinea, "id_nemotecnico") _
                               , pId_Moneda:=GetCell(Grilla, lLinea, "id_moneda") _
                               , pMonto:=GetCell(Grilla, lLinea, "monto") _
                               , pFecha_Operacion:=fFecha_Operacion _
                               , pCantidad:=GetCell(Grilla, lLinea, "Cantidad") _
                               , pMsgError:=pMsgError) Then
      If Not MsgBox(pMsgError, vbCritical + vbYesNo, Me.Caption) = vbYes Then
        GoTo ExitProcedure
      End If
    End If
  Next
  
  Fnt_ValidarDatos = True

ExitProcedure:

End Function

Public Function Mostrar(ByRef pFormOri As Form, _
                        pId_Cuenta As String, _
                        pOperacion As String, _
                        pTipo_Operacion As String, _
                        pCod_Instrumento As String, _
                        pNombreTipoOperacion As String, _
                        pOper_Fecha_Anterior As Boolean) As Boolean

  If Fnt_Verifica_Feriado(Fnt_FechaServidor) And Not pOper_Fecha_Anterior Then
    Mostrar = False
    MsgBox "Solo se pueden ingresar operaciones en d�as habiles.", vbExclamation, Me.Caption
    Unload Me
    Exit Function
  End If

  fId_Cuenta = pId_Cuenta
  fFlg_Tipo_Movimiento = pOperacion
  fTipo_Operacion = pTipo_Operacion
  fCod_Instrumento = pCod_Instrumento
 
  If Not pTipo_Operacion = gcOPERACION_Custodia Then
    Lbl_FechaLiquidacion.Visible = True
    Cmb_FechaLiquidacion.Visible = True
    Dtp_FechaLiquidacion.Visible = True
    
    Cmb_Representantes.Visible = True
    lbl_Representante.Visible = True
  Else
    Lbl_FechaLiquidacion.Visible = False
    Cmb_FechaLiquidacion.Visible = False
    Dtp_FechaLiquidacion.Visible = False
    
    Cmb_Representantes.Visible = False
    lbl_Representante.Visible = False
    
    Frame_Comisiones.Visible = False
    Grilla.Top = Grilla.Top - Frame_Comisiones.Height - lbl_Representante.Height
    Toolbar_Grilla.Top = Toolbar_Grilla.Top - Frame_Comisiones.Height - lbl_Representante.Height
    Frame_Principal.Height = Frame_Principal.Height - Frame_Comisiones.Height - lbl_Representante.Height
    Frame_Nemotecnico.Top = Frame_Nemotecnico.Top - Frame_Comisiones.Height - lbl_Representante.Height
    
    If fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
      Txt_Tasa_Historico.Visible = True
      
      ' Muestra la Tasa Hist�rica de la grilla
      Grilla.ColHidden(7) = False
      
      Frame_Nemotecnico.Height = Frame_Nemotecnico.Height + Txt_Tasa_Historico.Height
      Frame_Inversion.Height = Frame_Inversion.Height + Txt_Tasa_Historico.Height
      Frame_Tasa.Height = Frame_Tasa.Height + Txt_Tasa_Historico.Height
      Toolbar_Operacion.Top = Toolbar_Operacion.Top + Txt_Tasa_Historico.Height
    End If
  End If
    
  'Set fFormOri = pFormOri
  
  Select Case fFlg_Tipo_Movimiento
    Case gcTipoOperacion_Ingreso
      Me.Caption = "Compra de Bonos - " & pNombreTipoOperacion & "(Ver 2.0)"
      Chk_Vende_Todo.Visible = False
      Chk_Vende_Todo.Value = False
    Case gcTipoOperacion_Egreso
      Me.Caption = "Venta de Bonos - " & pNombreTipoOperacion & "(Ver 2.0)"
      Chk_Vende_Todo.Visible = True
      Chk_Vende_Todo.Value = False
  End Select
    
  Call Sub_ColocaFormAbajo
  
  Call Sub_CargarDatos
  
  If pOper_Fecha_Anterior Then
    lbl_fecha_ingreso.Visible = True
    DTP_Fecha_Operacion.Visible = True
    Txt_FechaIngreso_Real.Visible = False
    DTP_Fecha_Operacion.MaxDate = fFecha_Operacion
  Else
    lbl_fecha_ingreso.Visible = False
    DTP_Fecha_Operacion.Visible = False
    Txt_FechaIngreso_Real.Visible = True
  End If
  
  
  fEstadoOK = False
  fSalir = False
    
  Me.Show
  
  Do While Not fSalir
    DoEvents
  Loop
  
  Mostrar = fEstadoOK
  
  Unload Me
End Function

Private Sub Form_Unload(Cancel As Integer)
  fSalir = True
End Sub

Private Sub Sub_ColocaFormAbajo()
  If Not fFormOri Is Nothing Then
    Me.Top = fFormOri.Top + 2200
    Me.Left = fFormOri.Left + 200
  End If
End Sub

Private Sub Sub_FormTama�o(ByVal pTipo As eTipo, pTipo_Operacion As String)
  Cmb_Nemotecnico.Text = ""
  Txt_Nemotecnico.Text = ""
  Txt_Emisor.Text = ""
  Txt_Moneda.Text = ""
  Txt_Cantidad.Text = ""
  Txt_Fecha_Vencimiento.Text = ""
  Txt_Fecha_Emision.Text = ""
  '----------
  Txt_CantidadInversion.Text = ""
  Txt_Monto_Inversion.Text = ""
  Txt_Utilidad.Text = ""
  Txt_TasaNominal.Text = ""
  Txt_TasaCompra.Text = ""
  Txt_Tasa_Historico.Text = ""
  Chk_Vende_Todo.Value = False
  
  Select Case pTipo
    Case eT_Normal
      Frame_Nemotecnico.Visible = False
      If pTipo_Operacion = gcOPERACION_Custodia Then
        Me.Height = 6855 - Frame_Comisiones.Height - lbl_Representante.Height
      Else
        Me.Height = 6855
      End If
    Case eT_Grande
      Frame_Nemotecnico.Visible = True
      If pTipo_Operacion = gcOPERACION_Custodia Then
        If fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
          Me.Height = 9975 - Frame_Comisiones.Height - lbl_Representante.Height + Txt_Tasa_Historico.Height
        Else
          Me.Height = 9975 - Frame_Comisiones.Height - lbl_Representante.Height
        End If
      Else
        Me.Height = 9975
      End If
  End Select
End Sub

Private Sub Toolbar_Grilla_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_AgregarItem
    Case "DEL"
      Call Sub_EliminarItem
      Call Sub_Calcula_MontoTotal
  End Select
End Sub

Private Sub Sub_Calcula_MontoTotal()
Dim lLinea As Long
Dim lTotal As Double
Dim lPorc_Comision As Double
Dim lPorc_Derechos As Double
Dim lComision As Double
Dim lDerechos As Double
Dim lGastos As Double
Dim lIva As Double
  
  lTotal = 0
  For lLinea = 1 To (Grilla.Rows - 1)
    lTotal = lTotal + GetCell(Grilla, lLinea, "monto")
  Next
  
  lPorc_Comision = To_Number(Txt_Porcentaje_Comision.Text)
  lPorc_Derechos = To_Number(Txt_Porcentaje_Derechos.Text)
  lGastos = To_Number(Txt_Gastos.Text)
  
  If fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
    
    lComision = Fnt_Techo_Numero((lTotal * lPorc_Comision) / 100)
    lDerechos = Fnt_Techo_Numero((lTotal * lPorc_Derechos) / 100)
    lIva = Fnt_Techo_Numero((lComision + lGastos + lDerechos) * fValor_Iva)
  
    Txt_MontoTotal.Text = lTotal + lComision + lIva + lGastos + lDerechos
  
  ElseIf fFlg_Tipo_Movimiento = gcTipoOperacion_Egreso Then
    
    lComision = Int((lTotal * lPorc_Comision) / 100)
    lDerechos = Int((lTotal * lPorc_Derechos) / 100)
    lIva = Int((lComision + lGastos + lDerechos) * fValor_Iva)
    
    Txt_MontoTotal.Text = lTotal - (lComision + lIva + lGastos + lDerechos)
  End If
  
  Txt_Comision.Text = lComision
  Txt_Derechos.Text = lDerechos
  Txt_Iva.Text = lIva
  
End Sub

Private Sub Sub_AgregarItem()
Dim lLinea As Long
  
  lLinea = Grilla.Row
  Call Sub_FormTama�o(eT_Grande, fTipo_Operacion)
  ' Cmb_Nemotecnico.SetFocus
  fModif_Grilla = False
End Sub

Private Sub Sub_EliminarItem()
Dim lLinea As Long
  
  lLinea = Grilla.Row
  If lLinea > 0 Then
    If MsgBox("�Desea eliminar este Item?.", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
      Call Grilla.RemoveItem(lLinea)
    End If
  End If
End Sub

Private Sub Toolbar_Operacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "OK"
      If Fnt_AceptarDetalle Then
        Call Sub_FormTama�o(eT_Normal, fTipo_Operacion)
        Call Sub_Calcula_MontoTotal
        Grilla.SetFocus
      End If
    Case "CANCEL"
      Call Sub_FormTama�o(eT_Normal, fTipo_Operacion)
      'Call Sub_Calcula_MontoTotal
      Grilla.SetFocus
  End Select
End Sub

Private Sub Toolbar_Valorizar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "VALORIZA"
      Call Sub_ValorizaPapel
  End Select
End Sub

Private Sub Sub_ValorizaPapel()
Dim lcBonos As Class_Bonos
Dim lValorizacion As Double
Dim lCantidad As Double
Dim lId_Nemotecnico As Double
Dim lTasa           As Double

  If To_Number(Txt_CantidadInversion.Text) = 0 Then
    MsgBox "Debe ingresar los nominales de inversi�n para poder valorizar.", vbCritical, Me.Caption
    Exit Sub
  End If

  If To_Number(Txt_TasaCompra.Text) = 0 Then
    MsgBox "Debe ingresar la tasa de inversi�n para poder valorizar.", vbCritical, Me.Caption
    Exit Sub
  End If

  lCantidad = Txt_CantidadInversion.Text
  lTasa = Txt_TasaCompra.Text
  
  If fForm_Confirmacion Then
    lId_Nemotecnico = Txt_Nemotecnico.Tag
  Else
    lId_Nemotecnico = fId_Nemotecnico
  End If
  
  Set lcBonos = New Class_Bonos
  lValorizacion = lcBonos.ValorizaPapel(lId_Nemotecnico, fFecha_Operacion, lTasa, lCantidad)

  Txt_Monto_Inversion.Text = lValorizacion
  
  lValorizacion = lcBonos.ValorizaPapel(lId_Nemotecnico, fFecha_Operacion, Txt_TasaNominal.Text, lCantidad)
  
  fMonto_Nominal = lValorizacion
   
  Txt_Utilidad.Text = Txt_Monto_Inversion.Text - fMonto_Nominal
End Sub

Private Function Fnt_AceptarDetalle() As Boolean
Dim lLinea As Long
Dim lNemotecnico As String
Dim lId_Nemotecnico As String
Dim lOperacion As String
Dim lFlg_Vende_Todo As String
Dim lId_Mov_Activo As String
'------------------------------
Dim lcRestricc_Rel_Porc As Class_Restricciones_Rel_Porc
Dim lFila As Long
Dim lMonto_Operacion As Double

  Fnt_AceptarDetalle = False
  
  If Txt_Monto_Inversion.Text = 0 Then
    Rem Siempre valoriza antes de agregar una inversion
    Call Toolbar_Valorizar_ButtonClick(Toolbar_Valorizar.Buttons("VALORIZA"))
  End If
  
  Rem Revisa si falta algun dato para ingresar
  If fForm_Confirmacion Then
    lId_Nemotecnico = Txt_Nemotecnico.Tag
  Else
    If Not Fnt_Form_Validar(Me.Controls, Frame_Nemotecnico) Then
      Exit Function
    Else
      lId_Nemotecnico = fId_Nemotecnico
      Call Sub_Llena_Nemotecnico(lId_Nemotecnico)
    End If
  End If
  
  If Not Fnt_Form_Validar(Me.Controls, Frame_Inversion) Then
    Exit Function
  End If
  
  If Not Fnt_Form_Validar(Me.Controls, Frame_Tasa) Then
    Exit Function
  End If

  If To_Number(Txt_CantidadInversion.Text) = 0 Then
    MsgBox "Los nominales no puede ser 0.", vbCritical, Me.Caption
    Txt_CantidadInversion.SetFocus
    Exit Function
  End If

  If fFlg_Tipo_Movimiento = gcTipoOperacion_Egreso Then
    If To_Number(Txt_CantidadInversion.Text) > To_Number(Txt_Cantidad.Text) Then
      MsgBox "Los nominales a Vender (" & Format(Txt_CantidadInversion.Text, "#,##0.00") & ") no puede ser mayor a los nominales (" & Format(Txt_Cantidad.Text, "#,##0.00") & ").", vbCritical, Me.Caption
      Exit Function
    End If
  End If

  If fCorteMin > 0 Then
    Rem Si se ha definido un corte minimo, este se verifica que exista.
    If Not (To_Number(Txt_CantidadInversion.Text) Mod fCorteMin) = 0 Then
      'si el resto no es igual a 0, significa que la cantidad no es multiplo
      'del corte minimo.
      MsgBox "El nominal ingresado no corresponde al corte m�nimo definido para el Papel (" & Format(fCorteMin, "#,##0.00") & ").", vbInformation, Me.Caption
      Exit Function
    End If
  End If
  
  Rem Validacion de Restriccion porcentual Perfiles solo para Instrucciones
  Rem 22/09/2009 MMardones. El control de restricci�n es s�lo para Ingresos
  If fTipo_Operacion = gcOPERACION_Instruccion And fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then

    
    Rem Suma los montos de los otros nemotecnicos de la grilla
    If Grilla.Rows > 1 Then
      For lFila = 1 To (Grilla.Rows - 1)
        'If Not lId_Nemotecnico = GetCell(Grilla, lFila, "id_nemotecnico") Then
          lMonto_Operacion = lMonto_Operacion + To_Number(GetCell(Grilla, lFila, "monto"))
        'End If
      Next
    End If
    Rem a la suma calculada anteriormente se suma el monto ingresado por el usuario
    lMonto_Operacion = lMonto_Operacion + To_Number(Txt_Monto_Inversion.Text)
    
    'Txt_MontoTotal.Text = lMonto_Operacion
    
    Rem Valida la Restricci�n Porcentual del Perfil de Riesgo de Instrumentos y Productos
    Set lcRestricc_Rel_Porc = New Class_Restricciones_Rel_Porc
    With lcRestricc_Rel_Porc
      If Not .Fnt_Restriccion_Rel_Porc(pId_Cuenta:=fId_Cuenta, _
                                       pId_Nemotecnico:=lId_Nemotecnico, _
                                       pMonto:=lMonto_Operacion, _
                                       pId_Moneda:=Txt_Moneda.Tag, _
                                       pFecha_Operacion:=DTP_Fecha_Operacion.Value) Then
        Exit Function
      End If
    End With
    Set lcRestricc_Rel_Porc = Nothing
  End If
    
  Fnt_AceptarDetalle = True
  
  lNemotecnico = Txt_Nemotecnico.Text
  
  With Grilla
    Rem PREGUNTA SI LA PANTALLA PADRE ES "CONFIRMACI�N DE INSTRUCCIONES"
    If fForm_Confirmacion Or fModif_Grilla Then
      lLinea = .Row
    Else
      Rem Validaci�n: No se puede ingresar una instrucci�n para un mismo nemot�cnico
'      If Fnt_Busca_Nemotecnico(lId_Nemotecnico) Then
'        MsgBox "El Nemot�cnico " & Cmb_Nemotecnico.Text & " ya existe en la grilla.", vbCritical, Me.Caption
'        Exit Function
'      End If
      lLinea = .Rows
      .AddItem ""
    End If
  End With
  
  lOperacion = IIf(fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso, "Ingreso", "Egreso")
  lFlg_Vende_Todo = IIf(Chk_Vende_Todo.Value, cFlg_Vende_Todo, cFlg_No_Vende_Todo)
  
  If fForm_Confirmacion Then
    lId_Mov_Activo = fId_Mov_Activo
  Else
    lId_Mov_Activo = fId_Mov_Activo
    ' Jorge Vidal
  End If
  
  Call SetCell(Grilla, lLinea, "id_moneda", Txt_Moneda.Tag)
  Call SetCell(Grilla, lLinea, "id_nemotecnico", lId_Nemotecnico)
  Call SetCell(Grilla, lLinea, "dsc_nemotecnico", lNemotecnico)
  Call SetCell(Grilla, lLinea, "dsc_emisor", Txt_Emisor.Text)
  Call SetCell(Grilla, lLinea, "cantidad", Txt_CantidadInversion.Text)
  Call SetCell(Grilla, lLinea, "tasa", Txt_TasaCompra.Text)
  Call SetCell(Grilla, lLinea, "tasa_historica", Txt_Tasa_Historico.Text)
  Call SetCell(Grilla, lLinea, "tasa_transferencia", Txt_TasaNominal.Text)
  Call SetCell(Grilla, lLinea, "monto", Txt_Monto_Inversion.Text)
  Call SetCell(Grilla, lLinea, "monto_transferencia", fMonto_Nominal)
  Call SetCell(Grilla, lLinea, "utilidad", Txt_Utilidad.Text)
  Call SetCell(Grilla, lLinea, "Flg_Vende_Todo", lFlg_Vende_Todo)
  Call SetCell(Grilla, lLinea, "dsc_moneda", Txt_Moneda.Text)
  Call SetCell(Grilla, lLinea, "id_mov_activo", lId_Mov_Activo)
  Call SetCell(Grilla, lLinea, "cantidad_saldo", Txt_Cantidad.Text)
End Function

Private Function Fnt_Grabar() As Boolean
    Dim lBonos              As Class_Bonos_II
    Dim lLinea              As Long
    Dim lId_Contraparte     As String
    Dim lId_Trader          As String
    Dim lTipo_Precio        As String
    Dim lId_representante   As String
    Dim lFecha_Operacion    As Date
    Dim lFecha_Vigencia     As Date
    Dim lFecha_Liquidacion  As Date
    Dim lNemotecnicos       As Class_Nemotecnicos
    Dim lReg                As hCollection.hFields
    Dim lTasa_Historico     As String
    '---------------------------------
    Dim lcRestricc_Rel_Porc As Class_Restricciones_Rel_Porc
    Dim lId_Nemotecnico     As String
    '---------------------------------
    Dim lId_Caja_Cuenta     As Double
    Dim lNum_Error          As Double
    '---------------------------------
    Dim lRollback           As Boolean
    '----------------------------------
    Dim lsChkAporteRetiro As String
    
    lsChkAporteRetiro = IIf(chkAporteRetiro.Value = 1, "SI", "NO")
    

    Call Sub_Bloquea_Puntero(Me)
    Me.Enabled = False
  
    gDB.IniciarTransaccion
  
    lRollback = True
    Fnt_Grabar = False

    If Not Fnt_ValidarDatos Then
        GoTo ErrProcedure
    End If

    If fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
        Rem Si es una compra
        Rem Validar si el Instrumento y Nemot�cnicos est�n relacionados al perfil de riesgo que le corresponde a la cuenta
        If Not Fnt_Valida_Perfil_Cuenta_Instrm_Nemo(Grilla, fId_Cuenta, fCod_Instrumento) Then
            GoTo ErrProcedure
        End If
        
        Select Case fTipo_Operacion
            Case gcOPERACION_Instruccion, gcOPERACION_Directa
                lId_Caja_Cuenta = Fnt_CheckeaFinanciamiento(pId_Cuenta:=fId_Cuenta _
                                                   , pCod_Mercado:=fc_Mercado _
                                                   , pMonto:=Txt_MontoTotal.Text _
                                                   , pId_Moneda:=Txt_Moneda.Tag _
                                                   , pFecha_Liquidacion:=Dtp_FechaLiquidacion.Value _
                                                   , pNum_Error:=lNum_Error)
                                                   
                'VERITICA EL RESULTADO DE LA OPERACION
                Select Case lNum_Error
                    Case 0, eFinanciamiento_Caja.eFC_InversionDescubierta
                        'SI SON ESTOS VALORES SIGNIFICA QUE LA OPERACION SE PUEDE REALIZAR
                    Case Else
                        'Si el financiamiento tubo problemas
                        GoTo ErrProcedure
                End Select
                
            Case gcOPERACION_Custodia
                lId_Caja_Cuenta = Fnt_ValidarCaja(fId_Cuenta, fc_Mercado, Txt_Moneda.Tag)
                If lId_Caja_Cuenta = cNewEntidad Then
                    'Si el financiamiento tubo problemas
                    GoTo ErrProcedure
                End If
        End Select
    Else
        'Si es una venta
        lId_Caja_Cuenta = Fnt_ValidarCaja(fId_Cuenta, fc_Mercado, Txt_Moneda.Tag)
        If lId_Caja_Cuenta = -1 Then
            'Significa que hubo problema con la busqueda de la caja
            GoTo ErrProcedure
        End If
    End If
  
    Rem Validacion de Restriccion porcentual Perfiles solo para Instrucciones
    Rem 22/09/2009 MMardones. El control de restricci�n es s�lo para Ingresos
    If fTipo_Operacion = gcOPERACION_Instruccion And fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then

          Rem Rescata el id_nemotecnico del primer elemento de la grilla solo para ejecutar el proceso de validacion de
        Rem restriccion porcentual de perfil de riesgo, el cual requiere cod_instrumento y cod_producto
        lId_Nemotecnico = GetCell(Grilla, 1, "id_nemotecnico")
        
        Rem Valida la Restricci�n Porcentual del Perfil de Riesgo de Instrumentos y Productos
        Set lcRestricc_Rel_Porc = New Class_Restricciones_Rel_Porc
        With lcRestricc_Rel_Porc
          If Not .Fnt_Restriccion_Rel_Porc(pId_Cuenta:=fId_Cuenta, _
                                           pId_Nemotecnico:=lId_Nemotecnico, _
                                           pMonto:=To_Number(Txt_MontoTotal.Text), _
                                           pId_Moneda:=Txt_Moneda.Tag, _
                                           pFecha_Operacion:=DTP_Fecha_Operacion.Value) Then
            GoTo ErrProcedure
          End If
        End With
        Set lcRestricc_Rel_Porc = Nothing
    End If
    
  Set lBonos = New Class_Bonos_II
  For lLinea = 1 To (Grilla.Rows - 1)
    If fTipo_Operacion = gcOPERACION_Custodia And fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
      lTasa_Historico = GetCell(Grilla, lLinea, "tasa_historica")
    Else
      lTasa_Historico = ""
    End If
    
    Call lBonos.Agregar_Operaciones_Detalle(pId_Nemotecnico:=GetCell(Grilla, lLinea, "id_nemotecnico"), _
                                            pCantidad:=GetCell(Grilla, lLinea, "cantidad"), _
                                            pTasa:=GetCell(Grilla, lLinea, "tasa"), _
                                            PTasa_Gestion:=GetCell(Grilla, lLinea, "tasa_transferencia"), _
                                            pId_Moneda:=GetCell(Grilla, lLinea, "id_moneda"), _
                                            pMonto:=GetCell(Grilla, lLinea, "monto"), _
                                            pUtilidad:=GetCell(Grilla, lLinea, "utilidad"), _
                                            pFlg_Vende_Todo:=GetCell(Grilla, lLinea, "flg_vende_todo"), _
                                            pTasa_Historico:=lTasa_Historico, _
                                            pId_Mov_Activo_Compra:=GetCell(Grilla, lLinea, "id_mov_Activo"))
  Next

  lId_Contraparte = Fnt_ComboSelected_KEY(Cmb_Contraparte)
  lId_Trader = Fnt_ComboSelected_KEY(Cmb_Traders)
  lId_representante = Fnt_ComboSelected_KEY(Cmb_Representantes)
  lFecha_Operacion = DTP_Fecha_Operacion.Value ' Fnt_FechaServidor
  lFecha_Vigencia = lFecha_Operacion
  lFecha_Liquidacion = Dtp_FechaLiquidacion.Value
  
  Select Case fTipo_Operacion
    Case gcOPERACION_Directa
        If Not lBonos.Realiza_Operacion_Directa(pId_Operacion:=fId_Operacion, _
                                              pId_Cuenta:=fId_Cuenta, _
                                              pDsc_Operacion:="", _
                                              pTipoOperacion:=fFlg_Tipo_Movimiento, _
                                              pId_Contraparte:=lId_Contraparte, _
                                              pId_Representante:=lId_representante, _
                                              pId_Moneda_Operacion:=Txt_Moneda.Tag, _
                                              pFecha_Operacion:=lFecha_Operacion, _
                                              pFecha_Liquidacion:=lFecha_Liquidacion, _
                                              pId_Trader:=lId_Trader, _
                                              pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                              pComision:=Txt_Comision.Text, _
                                              pDerechos_Bolsa:=Txt_Derechos.Text, _
                                              pGastos:=Txt_Gastos.Text, _
                                              pIva:=Txt_Iva.Text, _
                                              pMonto_Operacion:=Txt_MontoTotal.Text, _
                                              pTipo_Precio:=lTipo_Precio, _
                                              pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                              pChkAporteRetiro:=lsChkAporteRetiro, pCod_Instrumento:=fCod_Instrumento) Then
        Call Fnt_MsgError(lBonos.SubTipo_LOG, _
                          "Problemas en grabar el Bono Nacional.", _
                          lBonos.ErrMsg, _
                          pConLog:=True)
        GoTo ErrProcedure
      End If
      
    Case gcOPERACION_Custodia
      If Not lBonos.Realiza_Operacion_Custodia(pId_Operacion:=fId_Operacion, _
                                              pId_Cuenta:=fId_Cuenta, _
                                              pDsc_Operacion:="", _
                                              pTipoOperacion:=fFlg_Tipo_Movimiento, _
                                              pId_Contraparte:=lId_Contraparte, _
                                              pId_Representante:=lId_representante, _
                                              pId_Moneda_Operacion:=Txt_Moneda.Tag, _
                                              pFecha_Operacion:=lFecha_Operacion, _
                                              pFecha_Liquidacion:=lFecha_Liquidacion, _
                                              pId_Trader:=lId_Trader, _
                                              pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                              pComision:=Txt_Comision.Text, _
                                              pDerechos_Bolsa:=Txt_Derechos.Text, _
                                              pGastos:=Txt_Gastos.Text, _
                                              pIva:=Txt_Iva.Text, _
                                              pMonto_Operacion:=Txt_MontoTotal.Text, _
                                              pTipo_Precio:=lTipo_Precio, _
                                              pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                              pChkAporteRetiro:=lsChkAporteRetiro) Then
                                              
        Call Fnt_MsgError(lBonos.SubTipo_LOG, _
                          "Problemas en grabar el Bono Nacional.", _
                          lBonos.ErrMsg, _
                          pConLog:=True)
        GoTo ErrProcedure
      End If
      
    Case gcOPERACION_Instruccion
      If Not lBonos.Realiza_Operacion_Instruccion(pId_Operacion:=fId_Operacion, _
                                                  pId_Cuenta:=fId_Cuenta, _
                                                  pDsc_Operacion:="", _
                                                  pTipoOperacion:=fFlg_Tipo_Movimiento, _
                                                  pId_Contraparte:=lId_Contraparte, _
                                                  pId_Representante:=lId_representante, _
                                                  pId_Moneda_Operacion:=Txt_Moneda.Tag, _
                                                  pFecha_Operacion:=lFecha_Operacion, _
                                                  pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                  pId_Trader:=lId_Trader, _
                                                  pPorc_Comision:=(Txt_Porcentaje_Comision.Text / 100), _
                                                  pComision:=Txt_Comision.Text, _
                                                  pDerechos_Bolsa:=Txt_Derechos.Text, _
                                                  pGastos:=Txt_Gastos.Text, _
                                                  pIva:=Txt_Iva.Text, _
                                                  pMonto_Operacion:=Txt_MontoTotal.Text, _
                                                  pTipo_Precio:=lTipo_Precio, _
                                                  pChkAporteRetiro:=lsChkAporteRetiro) Then
        Call Fnt_MsgError(lBonos.SubTipo_LOG, _
                          "Problemas en grabar el Bono Nacional.", _
                          lBonos.ErrMsg, _
                          pConLog:=True)
        GoTo ErrProcedure
      End If
      
    Case Else
      MsgBox "Operacion no reconocida para operar.", vbCritical, Me.Caption
      GoTo ErrProcedure
      
  End Select

    '*** GRABA EN LA REL_VENTA_COMPRA_BON_NAC RACV (Se deja en comentario por el momento 18/08/2016)
''    For lLinea = 1 To (Grilla.Rows - 1)
''        If InStr(GetCell(Grilla, lLinea, "id_mov_activo"), ",") > 0 Then
''           If Not lBonos.Rel_Compra_Venta_RFija_Guardar(lId_Operacion:=fId_Operacion, _
''                                                        lId_Cuenta:=fId_Cuenta, _
''                                                        lId_Nemotecnico:=GetCell(Grilla, lLinea, "id_nemotecnico"), _
''                                                        lStr_ID_MOV_ACTIVO:=GetCell(Grilla, lLinea, "id_mov_activo"), _
''                                                        lCantidad:=GetCell(Grilla, lLinea, "cantidad")) Then
''              Call Fnt_MsgError(lBonos.SubTipo_LOG, _
''                                "Problemas en grabar Relaci�n de Venta/Compra Bonos Nacionales.", _
''                                 lBonos.ErrMsg, _
''                                 pConLog:=True)
''              GoTo ErrProcedure
''           End If
''      End If
''    Next lLinea

  lRollback = False
  Fnt_Grabar = True

ErrProcedure:
    If lRollback Then
        gDB.RollbackTransaccion
    Else
        gDB.CommitTransaccion
        Call Fnt_EnvioEMAIL_Trader(fId_Operacion)
        If lsChkAporteRetiro = "SI" Then
        'Agrega pregunta por MMA.
            If MsgBox("Genera Comprobante ?." _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
                Frm_AporteRescate_Fechas_Anteriores.ImprimeDocWord fId_Operacion, pTipo:="O"
            End If
        End If
    End If
  
    Call Sub_Desbloquea_Puntero(Me)
    Me.Enabled = True
End Function

Private Sub Txt_Comision_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Derechos_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Derechos_LostFocus()
  Call Sub_Calcula_MontoTotal
'  If Not Txt_MontoTotal.Tag = "" Then
'    Txt_Derechos.Text = Int((Txt_MontoTotal.Tag * Txt_Porcentaje_Derechos.Text) / 100)
'    Call Txt_Iva_LostFocus
'
'    If fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
'      Txt_MontoTotal.Text = Int(Txt_MontoTotal.Tag) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
'    ElseIf fFlg_Tipo_Movimiento = gcTipoOperacion_Egreso Then
'      Txt_MontoTotal.Text = Int(Txt_MontoTotal.Tag) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
'    End If
'  End If
  
End Sub

Private Sub Txt_Gastos_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Gastos_LostFocus()
  Call Sub_Calcula_MontoTotal
'  If Not Txt_MontoTotal.Tag = "" Then
'    Call Txt_Iva_LostFocus
'
'    If fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
'      Txt_MontoTotal.Text = Int(Txt_MontoTotal.Tag) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
'    ElseIf fFlg_Tipo_Movimiento = gcTipoOperacion_Egreso Then
'      Txt_MontoTotal.Text = Int(Txt_MontoTotal.Tag) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
'    End If
'  End If

End Sub

Private Sub Txt_Iva_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Nemotecnico_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    If Txt_Nemotecnico.Text <> "" Then
      Call cmb_buscar_Click
    End If
  Else
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
  End If
End Sub

Private Sub Txt_Porcentaje_Comision_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Porcentaje_Comision_LostFocus()
  Call Sub_Calcula_MontoTotal
'  If Not Txt_MontoTotal.Tag = "" Then
'    Txt_Comision.Text = Int((Txt_MontoTotal.Tag * Txt_Porcentaje_Comision.Text) / 100)
'    Call Txt_Iva_LostFocus
'
'    If fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
'      Txt_MontoTotal.Text = Int(Txt_MontoTotal.Tag) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
'    ElseIf fFlg_Tipo_Movimiento = gcTipoOperacion_Egreso Then
'      Txt_MontoTotal.Text = Int(Txt_MontoTotal.Tag) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
'    End If
'  End If
End Sub

Private Sub Txt_Comision_LostFocus()
  Call Sub_Calcula_MontoTotal
'  If Not Txt_MontoTotal.Tag = "" Then
'    Txt_Comision.Text = Int((Txt_MontoTotal.Tag * Txt_Porcentaje_Comision.Text) / 100)
'    Call Txt_Iva_LostFocus
'
'    If fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
'      Txt_MontoTotal.Text = Int(Txt_MontoTotal.Tag) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
'    ElseIf fFlg_Tipo_Movimiento = gcTipoOperacion_Egreso Then
'      Txt_MontoTotal.Text = Int(Txt_MontoTotal.Tag) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
'    End If
'  End If
  
End Sub

Private Sub Txt_Iva_LostFocus()
  Call Sub_Calcula_MontoTotal
'  If Not Txt_MontoTotal.Tag = "" Then
'    Txt_Iva.Text = Int((Int(Txt_Comision.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)) * fValor_Iva)
'
'    If fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
'      Txt_MontoTotal.Text = Int(Txt_MontoTotal.Tag) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
'    ElseIf fFlg_Tipo_Movimiento = gcTipoOperacion_Egreso Then
'      Txt_MontoTotal.Text = Int(Txt_MontoTotal.Tag) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
'    End If
'  End If
  
End Sub

Rem FUNCION QUE ES LLAMADA DESDE LA PANTALLA DE CONFIRMACION DE INSTRUCCIONES
Public Function Confirmar(ByRef pFormOri As Form, _
                          ByVal pId_Operacion As String, _
                          ByVal pId_Cuenta As String, _
                          ByRef pMonto_Total As Double, _
                          ByVal pCod_Imstrumento As String) As Boolean
  
  fTipo_Operacion = gcOPERACION_Instruccion
  fId_Cuenta = pId_Cuenta
  fId_Operacion = pId_Operacion
  fCod_Instrumento = pCod_Imstrumento
  'Set fFormOri = pFormOri
  
  lbl_fecha_ingreso.Visible = False
  DTP_Fecha_Operacion.Visible = False
  Txt_FechaIngreso_Real.Visible = True
  
  Txt_Num_Operacion.Text = pId_Operacion
  Me.Caption = Me.Caption & " - Operaci�n N�: " & pId_Operacion
  
  '------------------------------------
  Toolbar.Buttons(1).Caption = "Confirmar"
  Toolbar.Buttons(1).ToolTipText = "Confirma la operaci�n"
  Form_Resize
  Toolbar_Grilla.Visible = False
  Txt_Cantidad.Visible = False
  Chk_Vende_Todo.Visible = False
  Txt_Nemotecnico.Visible = True
  lbl_nemotecnico.Visible = False
  Cmb_Nemotecnico.Visible = False
  Grilla.Width = 8565
  '-------------------------------------
  
  Call Sub_ColocaFormAbajo
  Call Sub_FormTama�o(eT_Normal, fTipo_Operacion)
  Call Sub_CargarDatos_Confirmacion
  
  fForm_Confirmacion = True
  fEstadoOK = False
  fSalir = False
    
  Me.Show
  
  Do While Not fSalir
    DoEvents
  Loop
  
  Confirmar = fEstadoOK
  pMonto_Total = To_Number(Txt_MontoTotal.Text)
  
  Unload Me
End Function

Rem CARGA DATOS SEGUN EL NUMERO DE OPERACION PARA LA CONFIRMACION
Private Sub Sub_CargarDatos_Confirmacion()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lDetalle As Class_Operaciones_Detalle
Dim lId_Contraparte As String
Dim lId_representante As String
Dim lNemotecnico As Class_Nemotecnicos
Dim lNombre_Nemotecnico As String
Dim lTipo_Precio As String
Dim lEmisor As String
'----------------------------------------
'Dim lcMoneda As Class_Monedas
Dim lcMoneda As Object
'----------------------------------------
Dim lDsc_Moneda As String
      
  Call Sub_Bloquea_Puntero(Me)
  
  Set fcOperaciones = New Class_Operaciones
  fcOperaciones.Campo("Id_Operacion").Valor = fId_Operacion
  
  If fcOperaciones.BuscaConDetalles Then
    fFlg_Tipo_Movimiento = fcOperaciones.Campo("Flg_Tipo_Movimiento").Valor
    fFecha_Operacion = fcOperaciones.Campo("Fecha_Operacion").Valor
    
    Txt_FechaIngreso_Real.Text = fFecha_Operacion
    
    Txt_MontoTotal.Text = "" & fcOperaciones.Campo("Monto_Operacion").Valor
    
    
    lId_Contraparte = NVL(fcOperaciones.Campo("Id_Contraparte").Valor, "")
    If Not lId_Contraparte = "" Then
      Call Sub_ComboSelectedItem(Cmb_Contraparte, lId_Contraparte)
      Call Sub_CargaCombo_Traders(Cmb_Traders, lId_Contraparte)
      Call Sub_ComboSelectedItem(Cmb_Traders, "" & fcOperaciones.Campo("id_Trader").Valor)
    End If
    
    lId_representante = NVL(fcOperaciones.Campo("Id_Representante").Valor, "")
    If Not lId_representante = "" Then
      Call Sub_ComboSelectedItem(Cmb_Representantes, lId_representante)
    End If
    
    Dtp_FechaLiquidacion.Value = fcOperaciones.Campo("fecha_liquidacion").Valor
    'Dtp_FechaLiquidacion.MinDate = fFecha_Operacion
    Cmb_FechaLiquidacion.Text = Fnt_DiasHabiles_EntreFechas(fFecha_Operacion, Dtp_FechaLiquidacion.Value)
        
    Rem Comisiones
    Txt_Porcentaje_Comision.Text = (NVL(fcOperaciones.Campo("Porc_Comision").Valor, 0) * 100)
    Txt_Comision.Text = NVL(fcOperaciones.Campo("Comision").Valor, 0)
    Txt_Iva.Text = NVL(fcOperaciones.Campo("Iva").Valor, 0)
    Txt_Gastos.Text = NVL(fcOperaciones.Campo("Gastos").Valor, 0)
    Txt_Derechos.Text = NVL(fcOperaciones.Campo("Derechos").Valor, 0)
    Txt_Porcentaje_Derechos.Text = Fnt_Divide(Txt_Derechos.Text * 100, NVL(fcOperaciones.Campo("Monto_Operacion").Valor, 0))
    
    Set lNemotecnico = New Class_Nemotecnicos
    
    For Each lDetalle In fcOperaciones.Detalles
      
      With lNemotecnico
        .Campo("id_nemotecnico").Valor = lDetalle.Campo("Id_Nemotecnico").Valor
        If .BuscarView Then
          For Each lReg In .Cursor
            lNombre_Nemotecnico = Trim(lReg("NEMOTECNICO").Value)
            Txt_Moneda.Tag = lReg("id_moneda_transaccion").Value
            lEmisor = Trim(lReg("dsc_emisor_especifico").Value)
          Next
        End If
      End With
      
'      Set lcMoneda = New Class_Monedas
      Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
      With lcMoneda
        .Campo("id_moneda").Valor = lDetalle.Campo("Id_Moneda_Pago").Valor
        If .Buscar Then
          If .Cursor.Count > 0 Then
            lDsc_Moneda = .Cursor(1).Fields("dsc_moneda").Value
          End If
        End If
      End With
            
      lLinea = Grilla.Rows
      Call Grilla.AddItem("")
      Call SetCell(Grilla, lLinea, "id_moneda", NVL(lDetalle.Campo("Id_Moneda_Pago").Valor, ""))
      Call SetCell(Grilla, lLinea, "id_nemotecnico", NVL(lDetalle.Campo("Id_Nemotecnico").Valor, ""))
      'Call SetCell(Grilla, lLinea, "monto_transferencia", fMonto_Nominal)
      Call SetCell(Grilla, lLinea, "tasa_transferencia", NVL(lDetalle.Campo("Precio_Gestion").Valor, ""))
      Call SetCell(Grilla, lLinea, "dsc_nemotecnico", lNombre_Nemotecnico)
      Call SetCell(Grilla, lLinea, "dsc_emisor", lEmisor)
      Call SetCell(Grilla, lLinea, "cantidad", NVL(lDetalle.Campo("Cantidad").Valor, ""))
      Call SetCell(Grilla, lLinea, "tasa", NVL(lDetalle.Campo("Precio").Valor, ""))
      Call SetCell(Grilla, lLinea, "monto", NVL(lDetalle.Campo("Monto_Pago").Valor, ""))
      Call SetCell(Grilla, lLinea, "utilidad", NVL(lDetalle.Campo("utilidad").Valor, ""))
      Call SetCell(Grilla, lLinea, "flg_vende_todo", NVL(lDetalle.Campo("flg_vende_todo").Valor, ""))
      Call SetCell(Grilla, lLinea, "dsc_moneda", lDsc_Moneda)
      Call SetCell(Grilla, lLinea, "tasa_historica", NVL(lDetalle.Campo("Precio_Historico_Compra").Valor, ""))
      Call SetCell(Grilla, lLinea, "id_mov_activo", NVL(lDetalle.Campo("id_mov_activo_compra").Valor, ""))
      
    Next
  End If
  
'  If Not fConsulta_Operacion Then
'    Call Sub_Carga_Nemotecnicos
'  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Function Fnt_Grabar_Confirmacion() As Boolean
    Dim lId_Caja_Cuenta     As Double
    Dim lRollback           As Boolean
    Dim lDetalle            As Class_Operaciones_Detalle
    Dim lFila               As Long
    Dim lId_Contraparte     As String
    Dim lId_Trader          As String
    Dim lId_representante   As String
    Dim lTipo_Precio        As String
    '------------------------------------
    Dim lId_Nemotecnico     As String
    Dim lcRestricc_Rel_Porc As Class_Restricciones_Rel_Porc

    Call Sub_Bloquea_Puntero(Me)
    Me.Enabled = False
    
    gDB.IniciarTransaccion
    
    lRollback = True
    Fnt_Grabar_Confirmacion = False
    
    If Not Fnt_ValidarDatos Then
        GoTo ErrProcedure
    End If

    If fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
        Rem Si es una compra
        Rem Validar si el Instrumento y Nemot�cnicos est�n relacionados al perfil de riesgo que le corresponde a la cuenta
        If Not Fnt_Valida_Perfil_Cuenta_Instrm_Nemo(Grilla, fId_Cuenta, fCod_Instrumento) Then
            GoTo ErrProcedure
        End If
        
        lId_Caja_Cuenta = Fnt_ValidarCaja(fId_Cuenta, fc_Mercado, Txt_Moneda.Tag)
        If lId_Caja_Cuenta = -1 Then
            Rem Si el financiamiento tubo problemas
            GoTo ErrProcedure
        End If
        
    Else
        Rem Si es una venta
        lId_Caja_Cuenta = Fnt_ValidarCaja(fId_Cuenta, fc_Mercado, Txt_Moneda.Tag)
        If lId_Caja_Cuenta < 0 Then
            Rem Significa que hubo problema con la busqueda de la caja
            GoTo ErrProcedure
        End If
    End If
  
    Rem Rescata el id_nemotecnico del primer elemento de la grilla solo para ejecutar el proceso de validacion de
    Rem restriccion porcentual de perfil de riesgo, el cual requiere cod_instrumento y cod_producto
    lId_Nemotecnico = GetCell(Grilla, 1, "id_nemotecnico")

    Rem Valida la Restricci�n Porcentual del Perfil de Riesgo de Instrumentos y Productos
    Set lcRestricc_Rel_Porc = New Class_Restricciones_Rel_Porc
    With lcRestricc_Rel_Porc
        If Not .Fnt_Restriccion_Rel_Porc(pId_Cuenta:=fId_Cuenta, _
                                     pId_Nemotecnico:=lId_Nemotecnico, _
                                     pMonto:=To_Number(Txt_MontoTotal.Text), _
                                     pId_Moneda:=Txt_Moneda.Tag, _
                                     pFecha_Operacion:=DTP_Fecha_Operacion.Value) Then
            GoTo ErrProcedure
        End If
    End With
    Set lcRestricc_Rel_Porc = Nothing
  
    lId_Contraparte = Fnt_ComboSelected_KEY(Cmb_Contraparte)
    lId_representante = Fnt_ComboSelected_KEY(Cmb_Representantes)
    lId_Trader = Fnt_ComboSelected_KEY(Cmb_Traders)
    '  lTipo_Precio = Fnt_ComboSelected_KEY(Cmb_TipoPrecio)
    '  lFecha_Operacion = Fnt_FechaServidor
    '  lFecha_Vigencia = lFecha_Operacion '+ To_Number(Txt_DiasVigencia.Text)
    '  lFecha_Liquidacion = lFecha_Operacion + Cmb_FormaPago.SelectedItem.Tag
      
    With fcOperaciones
        '    .Campo("Id_Operacion").valor = fId_Operacion
    '    .Campo("Id_Cuenta").valor = fId_Cuenta
    '    .Campo("Cod_Tipo_Operacion").valor = gcOPERACION_Directa
    '    .Campo("Cod_Estado").valor = cCod_Estado_Pendiente
        .Campo("Id_Contraparte").Valor = lId_Contraparte
        .Campo("Id_Representante").Valor = lId_representante
    '    .Campo("Id_Tipo_Liquidacion").Valor = lFormaPago
    '    .Campo("Id_Moneda_Operacion").valor = Txt_Moneda.Tag
    '    .Campo("Cod_Producto").valor = gcPROD_FFMM_NAC 'Esto va en duro
    '    .Campo("Cod_Instrumento").valor = fCod_Instrumento
    '    .Campo("Flg_Tipo_Movimiento").valor = fFlg_Tipo_Movimiento
    '    .Campo("Fecha_Operacion").valor = lFecha_Operacion
    '    .campo("Fecha_Vigencia").valor = Txt_FechaVigencia.Text
        .Campo("Fecha_Liquidacion").Valor = Dtp_FechaLiquidacion.Value
    '    .Campo("Dsc_Operacion").valor = pDsc_Operacion
        .Campo("Id_Trader").Valor = lId_Trader
        .Campo("Porc_Comision").Valor = (Txt_Porcentaje_Comision.Text / 100)
        .Campo("Comision").Valor = Txt_Comision.Text
        .Campo("Derechos").Valor = Txt_Derechos.Text
        .Campo("Gastos").Valor = Txt_Gastos.Text
        .Campo("Iva").Valor = Txt_Iva.Text
        .Campo("Monto_Operacion").Valor = To_Number(Txt_MontoTotal.Text)
    '    .campo("Flg_Limite_Precio").valor = lTipo_Precio
        
        lFila = 1
        For Each lDetalle In fcOperaciones.Detalles
            lDetalle.Campo("Cantidad").Valor = GetCell(Grilla, lFila, "cantidad")
            lDetalle.Campo("Precio").Valor = GetCell(Grilla, lFila, "tasa")
            lDetalle.Campo("Monto_Pago").Valor = GetCell(Grilla, lFila, "monto")
            lDetalle.Campo("Precio_gestion").Valor = GetCell(Grilla, lFila, "tasa_transferencia")
            lDetalle.Campo("utilidad").Valor = GetCell(Grilla, lFila, "utilidad")
            lDetalle.Campo("flg_vende_todo").Valor = GetCell(Grilla, lFila, "flg_vende_todo")
            lFila = lFila + 1
        Next
    
        If Not .Guardar(pConfirmacion:=True) Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                        "Error en la confirmaci�n de la operaci�n.", _
                        .ErrMsg, _
                        pConLog:=True)
            GoTo ErrProcedure
        End If
    
        If Not .Confirmar_Bonos(lId_Caja_Cuenta) Then
            Call Fnt_MsgError(.SubTipo_LOG, _
                        "Error en la confirmaci�n de la operaci�n.", _
                        .ErrMsg, _
                        pConLog:=True)
            GoTo ErrProcedure
        End If
    End With
  
    lRollback = False
    Fnt_Grabar_Confirmacion = True

ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    'MsgBox "Error en la confirmaci�n de la operaci�n.", vbCritical
  Else
    gDB.CommitTransaccion
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
End Function

Private Sub Txt_Porcentaje_Derechos_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Porcentaje_Derechos_LostFocus()
  Call Sub_Calcula_MontoTotal
'  If Not Txt_MontoTotal.Tag = "" Then
'    Txt_Derechos.Text = Int((Txt_MontoTotal.Tag * Txt_Porcentaje_Derechos.Text) / 100)
'    Call Txt_Iva_LostFocus
'
'    If fFlg_Tipo_Movimiento = gcTipoOperacion_Ingreso Then
'      Txt_MontoTotal.Text = Int(Txt_MontoTotal.Tag) + Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text)
'    ElseIf fFlg_Tipo_Movimiento = gcTipoOperacion_Egreso Then
'      Txt_MontoTotal.Text = Int(Txt_MontoTotal.Tag) - (Int(Txt_Comision.Text) + Int(Txt_Iva.Text) + Int(Txt_Gastos.Text) + Int(Txt_Derechos.Text))
'    End If
'  End If
  
End Sub

Private Sub Txt_TasaCompra_LostFocus()
  If Chk_Vende_Todo.Value Then
    Call Sub_ValorizaPapel
  End If
End Sub

Rem FUNCION QUE ES LLAMADA DESDE LA PANTALLA DE CONSULTA DE OPERACIONES
Public Sub Consulta_Operacion(ByRef pFormOri As Form, _
                                   ByVal pId_Operacion As String, _
                                   ByVal pId_Cuenta As String, _
                                   ByVal pCod_Imstrumento As String, _
                                   ByVal pTipo_Operacion As String, _
                                   ByVal pOperacion As String)
  
  fConsulta_Operacion = True
  
  fTipo_Operacion = pTipo_Operacion
  fFlg_Tipo_Movimiento = pOperacion
  fId_Cuenta = pId_Cuenta
  fId_Operacion = pId_Operacion
  fCod_Instrumento = pCod_Imstrumento
  
  lbl_fecha_ingreso.Visible = False
  DTP_Fecha_Operacion.Visible = False
  Txt_FechaIngreso_Real.Visible = True
  
  Txt_Num_Operacion.Text = pId_Operacion
  Me.Caption = Me.Caption & " - Operaci�n N�: " & pId_Operacion
  
  If Not fTipo_Operacion = gcOPERACION_Custodia Then
    Lbl_FechaLiquidacion.Visible = True
    Cmb_FechaLiquidacion.Visible = True
    Dtp_FechaLiquidacion.Visible = True
    
    Cmb_Representantes.Visible = True
    lbl_Representante.Visible = True
  Else
    Lbl_FechaLiquidacion.Visible = False
    Cmb_FechaLiquidacion.Visible = False
    Dtp_FechaLiquidacion.Visible = False
    
    Cmb_Representantes.Visible = False
    lbl_Representante.Visible = False
    
    Frame_Comisiones.Visible = False
    Grilla.Top = Grilla.Top - Frame_Comisiones.Height - lbl_Representante.Height
    Frame_Principal.Height = Frame_Principal.Height - Frame_Comisiones.Height - lbl_Representante.Height
  End If
  
  'Oculta la Fecha de Liquidacion
  Grilla.ColHidden(Grilla.ColIndex("utilidad")) = True
  '------------------------------------
  Toolbar.Buttons(1).Visible = False
  Call Form_Resize
  
  '------------------------------------
  Cmb_Traders.Enabled = False
  Cmb_Traders.BackColor = fColorNoEdit
  
  Cmb_Contraparte.Enabled = False
  Cmb_Contraparte.BackColor = fColorNoEdit
  
  Cmb_Representantes.Enabled = False
  Cmb_Representantes.BackColor = fColorNoEdit
  
  Cmb_FechaLiquidacion.Enabled = False
  Cmb_FechaLiquidacion.BackColor = fColorNoEdit
  
  Dtp_FechaLiquidacion.Enabled = False
  
  '------------------------------------
  Txt_Porcentaje_Comision.Locked = True
  Txt_Porcentaje_Comision.BackColorTxt = fColorNoEdit
  
  Txt_Porcentaje_Derechos.Locked = True
  Txt_Porcentaje_Derechos.BackColorTxt = fColorNoEdit
  
  Txt_Comision.Locked = True
  Txt_Comision.BackColorTxt = fColorNoEdit
  
  Txt_Iva.Locked = True
  Txt_Iva.BackColorTxt = fColorNoEdit
  
  Txt_Gastos.Locked = True
  Txt_Gastos.BackColorTxt = fColorNoEdit
  
  Txt_Derechos.Locked = True
  Txt_Derechos.BackColorTxt = fColorNoEdit
  
  '-------------------------------------
  Grilla.Width = 8565
  Toolbar_Grilla.Visible = False
  '-------------------------------------
  
  Call Sub_ColocaFormAbajo
  Call Sub_FormTama�o(eT_Normal, fTipo_Operacion)
  Call Sub_CargarDatos_Consulta
  
  fSalir = False
    
  Me.Show
  
  Do While Not fSalir
    DoEvents
  Loop
  
  Unload Me
End Sub

Private Sub Sub_CargarDatos_Consulta()
Dim lcOperaciones As Class_Operaciones
Dim lcOperaciones_Detalle As Class_Operaciones_Detalle
Dim lReg As hFields
Dim lcMov_Activos As Class_Mov_Activos
Dim lCursor_Mov_Activos As hRecord
Dim lReg_Mov_Activos As hFields
Dim lcNemotecnico As Class_Nemotecnicos
'---------------------------------------
'Dim lcMoneda As Class_Monedas
Dim lcMoneda As Object
'---------------------------------------
Dim lNombre_Nemotecnico As String
Dim lDsc_Moneda As String
Dim lLinea As Long
Dim lOperacion As String
Dim lId_Contraparte As String
Dim lEmisor As String
    
  Rem Carga los datos generales de la operacion
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("id_operacion").Valor = fId_Operacion
    If .Buscar Then
      For Each lReg In .Cursor
        If lReg("cod_estado").Value = cCod_Estado_Pendiente Then
          Call Sub_CargarDatos_Confirmacion
          Exit Sub
        Else
          fFlg_Tipo_Movimiento = lReg("Flg_Tipo_Movimiento").Value
          fFecha_Operacion = lReg("Fecha_Operacion").Value
          
          Txt_FechaIngreso_Real.Text = fFecha_Operacion
          
          Txt_MontoTotal.Text = "" & lReg("Monto_Operacion").Value
          
          lId_Contraparte = NVL(lReg("Id_Contraparte").Value, "")
          If Not lId_Contraparte = "" Then
            Call Sub_ComboSelectedItem(Cmb_Contraparte, lId_Contraparte)
            Call Sub_CargaCombo_Traders(Cmb_Traders, lId_Contraparte)
            Call Sub_ComboSelectedItem(Cmb_Traders, "" & lReg("id_Trader").Value)
          End If
          
          Call Sub_ComboSelectedItem(Cmb_Representantes, "" & lReg("Id_Representante").Value)
          
          Dtp_FechaLiquidacion.Value = lReg("fecha_liquidacion").Value
          'Dtp_FechaLiquidacion.MinDate = fFecha_Operacion
          Cmb_FechaLiquidacion.Text = Fnt_DiasHabiles_EntreFechas(fFecha_Operacion, Dtp_FechaLiquidacion.Value)
          
          Rem Comisiones
          Txt_Porcentaje_Comision.Text = (NVL(lReg("Porc_Comision").Value, 0) * 100)
          Txt_Comision.Text = NVL(lReg("Comision").Value, 0)
          Txt_Iva.Text = NVL(lReg("Iva").Value, 0)
          Txt_Gastos.Text = NVL(lReg("Gastos").Value, 0)
          Txt_Derechos.Text = NVL(lReg("Derechos").Value, 0)
          Txt_Porcentaje_Derechos.Text = Fnt_Divide(Txt_Derechos.Text * 100, NVL(lReg("Monto_Operacion").Value, 0))
        End If
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar la Operaci�n.", _
                        fcOperaciones.ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcOperaciones = Nothing
  
  Set lcOperaciones_Detalle = New Class_Operaciones_Detalle
  With lcOperaciones_Detalle
    .Campo("id_operacion").Valor = fId_Operacion
    If .Buscar Then
      Set lCursor_Mov_Activos = .Cursor
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar la Operaci�n Detalle.", _
                        fcOperaciones.ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcOperaciones_Detalle = Nothing
  
  Set lcMov_Activos = New Class_Mov_Activos
  With lcMov_Activos
    For Each lReg In lCursor_Mov_Activos
      .Campo("id_operacion_detalle").Valor = lReg("id_operacion_detalle").Value
      If .Buscar Then
        For Each lReg_Mov_Activos In .Cursor
          Set lcNemotecnico = New Class_Nemotecnicos
          lcNemotecnico.Campo("id_nemotecnico").Valor = lReg_Mov_Activos("Id_Nemotecnico").Value
          If lcNemotecnico.BuscarView Then
            If lcNemotecnico.Cursor.Count > 0 Then
              lNombre_Nemotecnico = lcNemotecnico.Cursor(1)("NEMOTECNICO").Value
              lEmisor = lcNemotecnico.Cursor(1)("dsc_emisor_especifico").Value
            End If
          End If
          Set lcNemotecnico = Nothing
        
'          Set lcMoneda = New Class_Monedas
          Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
          lcMoneda.Campo("id_moneda").Valor = lReg_Mov_Activos("Id_Moneda").Value
          If lcMoneda.Buscar Then
            If lcMoneda.Cursor.Count > 0 Then
              lDsc_Moneda = lcMoneda.Cursor(1).Fields("dsc_moneda").Value
            End If
          End If
          Set lcMoneda = Nothing
        
          lLinea = Grilla.Rows
          Call Grilla.AddItem("")
          'Call SetCell(Grilla, lLinea, "id_moneda", NVL(lDetalle.Campo("Id_Moneda_Pago").Valor, ""))
          'Call SetCell(Grilla, lLinea, "id_nemotecnico", NVL(lDetalle.Campo("Id_Nemotecnico").Valor, ""))
          'Call SetCell(Grilla, lLinea, "monto_transferencia", fMonto_Nominal)
          'Call SetCell(Grilla, lLinea, "tasa_transferencia", NVL(lDetalle.Campo("Precio_Gestion").Valor, ""))
          Call SetCell(Grilla, lLinea, "dsc_nemotecnico", lNombre_Nemotecnico)
          Call SetCell(Grilla, lLinea, "dsc_emisor", lEmisor)
          Call SetCell(Grilla, lLinea, "cantidad", NVL(lReg_Mov_Activos("Cantidad").Value, ""))
          Call SetCell(Grilla, lLinea, "tasa", NVL(lReg_Mov_Activos("Precio").Value, ""))
          Call SetCell(Grilla, lLinea, "monto", NVL(lReg_Mov_Activos("Monto").Value, ""))
          'Call SetCell(Grilla, lLinea, "utilidad", NVL(lReg_Mov_Activos("utilidad").Value, ""))
          'Call SetCell(Grilla, lLinea, "flg_vende_todo", NVL(lDetalle.Campo("flg_vende_todo").Valor, ""))
          Call SetCell(Grilla, lLinea, "dsc_moneda", lDsc_Moneda)
          'Call SetCell(Grilla, lLinea, "tasa_historica", NVL(lDetalle.Campo("Precio_Historico_Compra").Valor, ""))
          'Call SetCell(Grilla, lLinea, "id_mov_activo", NVL(lDetalle.Campo("id_mov_activo_compra").Valor, ""))
      
        Next
      End If
    Next
  End With
  
  Set lcMov_Activos = Nothing
  
End Sub


