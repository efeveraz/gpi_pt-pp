VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form Frm_Man_Iva 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Mantenedor de IVA"
   ClientHeight    =   3405
   ClientLeft      =   45
   ClientTop       =   540
   ClientWidth     =   5190
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3405
   ScaleWidth      =   5190
   Begin TabDlg.SSTab SSTab 
      Height          =   2925
      Left            =   90
      TabIndex        =   0
      Top             =   420
      Width           =   5025
      _ExtentX        =   8864
      _ExtentY        =   5159
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   6
      TabHeight       =   529
      WordWrap        =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "Frm_Man_Iva.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Historial"
      TabPicture(1)   =   "Frm_Man_Iva.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Grilla_Iva"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Iva 
         Height          =   2265
         Left            =   -74850
         TabIndex        =   3
         Top             =   480
         Width           =   4695
         _cx             =   8281
         _cy             =   3995
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   3
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_Iva.frx":0038
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin VB.Frame Frame1 
         Height          =   2355
         Left            =   180
         TabIndex        =   4
         Top             =   390
         Width           =   4665
         Begin MSComCtl2.DTPicker DTP_Fecha_Ter 
            Height          =   330
            Left            =   1470
            TabIndex        =   6
            Top             =   1215
            Width           =   1770
            _ExtentX        =   3122
            _ExtentY        =   582
            _Version        =   393216
            CheckBox        =   -1  'True
            DateIsNull      =   -1  'True
            Format          =   70451201
            CurrentDate     =   38898.6446875
         End
         Begin MSComCtl2.DTPicker DTP_Fecha_Ini 
            Height          =   330
            Left            =   1470
            TabIndex        =   5
            Tag             =   "OBLI"
            Top             =   750
            Width           =   1770
            _ExtentX        =   3122
            _ExtentY        =   582
            _Version        =   393216
            Format          =   70451201
            CurrentDate     =   38810
         End
         Begin hControl2.hTextLabel Txt_Valor_IVA 
            Height          =   315
            Left            =   150
            TabIndex        =   9
            Tag             =   "OBLI"
            ToolTipText     =   "Comisión de Honorarios"
            Top             =   300
            Width           =   2205
            _ExtentX        =   3889
            _ExtentY        =   556
            LabelWidth      =   1300
            TextMinWidth    =   500
            Caption         =   "Valor IVA"
            Text            =   "0"
            Text            =   "0"
         End
         Begin VB.Label Label4 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Término"
            Height          =   330
            Left            =   150
            TabIndex        =   8
            Top             =   1215
            UseMnemonic     =   0   'False
            Width           =   1305
         End
         Begin VB.Label Label1 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Inicio"
            Height          =   330
            Left            =   150
            TabIndex        =   7
            Top             =   750
            Width           =   1305
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   1
      Tag             =   "OBLI"
      Top             =   0
      Width           =   5190
      _ExtentX        =   9155
      _ExtentY        =   635
      ButtonWidth     =   1720
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la información de forma Permanente"
            Object.ToolTipText     =   "Graba la información de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   2
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_Iva"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String

Public Sub Mostrar(pCod_Arbol_Sistema)
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        MsgBox "Movimiento se ha grabado existosamente.", vbOKOnly + vbQuestion, Me.Caption
        
        Call Sub_CargarHistorial
      End If
    
    Case "PRINTER"
      Call Sub_Imprimir(ePrinter.eP_Impresora)
      
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lValorIva As Double
Dim lcIva As Class_Iva
  
  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False
  
  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Exit Function
  End If

  Set lcIva = New Class_Iva
  With lcIva
    lValorIva = .Valor_Iva(Txt_Valor_IVA.Text)
    .Campo("Valor").Valor = lValorIva
    .Campo("Fecha_Desde").Valor = Format(DTP_Fecha_Ini.Value, cFormatDate)
    .Campo("Fecha_Hasta").Valor = NVL(Format(DTP_Fecha_Ter.Value, cFormatDate), Null)
    If Not .Guardar Then
      MsgBox "Problemas al grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      Fnt_Grabar = False
    End If
  End With
  Set lcIva = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
End Function

Private Sub Sub_CargaForm()
Dim lReg As hFields

  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False
  
  Call Sub_FormControl_Color(Me.Controls)

  Call Sub_CargarHistorial
  
  SSTab.Tab = 0
  
  DTP_Fecha_Ini.Value = Format(Fnt_FechaServidor, cFormatDate)
  
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
End Sub

Private Function Fnt_ValidarDatos() As Boolean
Dim lValido As Boolean
Dim lFecha_Ini As String
Dim lFecha_Ter As String
Dim lResultado As Integer
Dim lMensaje As String

  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False

  lValido = Fnt_Form_Validar(Me.Controls)
   
  If lValido Then
    If Not IsNull(DTP_Fecha_Ter.Value) Then
      If DTP_Fecha_Ter.Value < DTP_Fecha_Ini.Value Then
        lValido = False
        MsgBox "Fecha Hasta debe ser posterior a Fecha Desde.", vbOKOnly + vbExclamation, Me.Caption
        DTP_Fecha_Ter.SetFocus
      End If
    End If
  End If
   
  Fnt_ValidarDatos = lValido
  
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
End Function

Private Sub Sub_CargarHistorial()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lIva As Class_Iva
  
  Set lIva = New Class_Iva
  
  Grilla_Iva.Rows = 1
    
  If lIva.Buscar Then
    For Each lReg In lIva.Cursor
      lLinea = Grilla_Iva.Rows
      Call Grilla_Iva.AddItem("")
      
      Call SetCell(Grilla_Iva, lLinea, "Valor", lIva.Porcentaje_Iva(NVL(lReg("Valor").Value, "0")))
      Call SetCell(Grilla_Iva, lLinea, "Fecha_Ini", NVL(lReg("Fecha_Desde").Value, ""))
      Call SetCell(Grilla_Iva, lLinea, "Fecha_Ter", NVL(lReg("Fecha_Hasta").Value, ""))
    Next
  Else
    MsgBox "Problemas al cargar el historial." & vbCr & vbCr & lIva.ErrMsg, vbCritical, Me.Caption
  End If
End Sub

Private Sub Txt_Valor_IVA_KeyPress(KeyAscii As Integer)
  If Not KeyAscii = 8 And Not KeyAscii = 46 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  
  Set lForm = New Frm_Reporte_Generico
  
  Rem Comienzo de la generación del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Historial de IVA" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orPortrait)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Fecha Consulta: " & Format(Fnt_FechaServidor, cFormatDate)
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla_Iva, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Select Case ButtonMenu.Key
    Case "SCREEN"
      Call Sub_Imprimir(ePrinter.eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(ePrinter.eP_PDF)
  End Select
End Sub

