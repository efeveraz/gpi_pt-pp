VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_CargasAutomaticas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cargas Automaticas"
   ClientHeight    =   7410
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   8355
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7410
   ScaleWidth      =   8355
   Begin VB.Frame Frm_Cuentas 
      Caption         =   "Cuentas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   2715
      Left            =   60
      TabIndex        =   7
      Top             =   1230
      Width           =   8205
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Procesos 
         Height          =   1935
         Left            =   90
         TabIndex        =   8
         Top             =   660
         Width           =   7995
         _cx             =   14102
         _cy             =   3413
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   3
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_CargasAutomaticas.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Chequeo 
         Height          =   330
         Left            =   90
         TabIndex        =   9
         Top             =   270
         Width           =   1290
         _ExtentX        =   2275
         _ExtentY        =   582
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Wrappable       =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_ALL"
               Description     =   "Selecciona todos los items"
               Object.ToolTipText     =   "Selecciona todos los items"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_NOTHING"
               Description     =   "Deselecciona todos los items"
               Object.ToolTipText     =   "Deselecciona todos los items"
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frnm_Grilla_Sucesos 
      Caption         =   "Visor de Sucesos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3255
      Left            =   60
      TabIndex        =   3
      Top             =   4050
      Width           =   8205
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Visor 
         Height          =   2865
         Left            =   120
         TabIndex        =   4
         Top             =   270
         Width           =   7965
         _cx             =   14049
         _cy             =   5054
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   3
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_CargasAutomaticas.frx":007D
         ScrollTrack     =   -1  'True
         ScrollBars      =   2
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin VB.Frame Frm_Fecha_Proceso 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   765
      Left            =   60
      TabIndex        =   0
      Top             =   420
      Width           =   8205
      Begin MSComCtl2.DTPicker DTP_Fecha_Proceso 
         Height          =   315
         Left            =   1470
         TabIndex        =   1
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   56033281
         CurrentDate     =   37732
      End
      Begin VB.Label Lbl_Fecha_Proceso 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Proceso"
         Height          =   345
         Left            =   210
         TabIndex        =   2
         Top             =   270
         Width           =   1215
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   8355
      _ExtentX        =   14737
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Ejecutar"
            Key             =   "EJECUTAR"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Ejecutar los Eventos de Capital"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   6
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_CargasAutomaticas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-------------------------------------------------------------------------------------------------
'SOURCESAFE Information:
'    $Workfile: Frm_CargasAutomaticas.frm $
'    $Author: Gbuenrostro $
'    $Date: 14-02-14 16:20 $
'    $Revision: 4 $
'-------------------------------------------------------------------------------------------------

Dim fCollection As Collection
Public fProcedimiento As String
Public fOrigen As String
Dim lPublicador As Class_Publicadores_Precio

Private Sub Form_Load()
    Dim lReg  As hCollection.hFields
    Dim lLinea As Long
    
    With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("EJECUTAR").Image = cBoton_Aceptar
        .Buttons("EXIT").Image = cBoton_Salir
    End With
    
    With Toolbar_Chequeo
        Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
        .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
        .Appearance = ccFlat
    End With
    
    Call Sub_CargaForm
    
    Me.Top = 1
    Me.Left = 1
End Sub

Private Sub Form_Resize()
    Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_Visor_CellButtonClick(ByVal Row As Long, ByVal Col As Long)
    Call Grilla_Visor_DblClick
End Sub

Private Sub Grilla_Visor_DblClick()
    Dim lMensaje As String
    
    With Grilla_Visor
        If .Row > 0 Then
            lMensaje = GetCell(Grilla_Visor, .Row, "TEXTO")
            If Not lMensaje = "" Then
                MsgBox lMensaje, vbInformation, Me.Caption
            End If
        End If
    End With
End Sub

Private Sub Grilla_Visor_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
    If Not Col = Grilla_Visor.ColIndex("combo") Then
        Cancel = True
    End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents
    
    Select Case Button.Key
        Case "EJECUTAR"
            Call Sub_Ejecutar
        Case "EXIT"
            Unload Me
    End Select
End Sub

Private Sub Toolbar_Detalle_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents
    
    Select Case Button.Key
        Case "DETALLE"
            Call Grilla_Visor_DblClick
    End Select
End Sub

Private Sub Sub_CargaForm()
  Rem Limpia la grilla
    Grilla_Visor.Rows = 1
    Grilla_Procesos.Rows = 1
    
    DTP_Fecha_Proceso.Value = Fnt_FechaServidor
    
    
    Set fCollection = New Collection
    Call fCollection.Add(New Class_Cortes_Cupon, "CORTECUPON")
      
    Call Sub_LlenaGrillaProceso
  
End Sub

Private Sub Sub_Ejecutar()
    Dim lReg    As Object
    Dim lFila   As Long
    Dim lColum  As Long
    
    Call Sub_Bloquea_Puntero(Me)
    
    Grilla_Visor.Rows = 1
    Grilla_Visor.SetFocus 'Para que el control del fecha pierda el control
    
    lFila = 0
    lColum = Grilla_Procesos.ColIndex("chk")
    Call Fnt_Escribe_Grilla(Grilla_Visor, "", "Comienzando Cargas Automaticas...")
    For Each lReg In fCollection
        lFila = lFila + 1
        
        With Grilla_Procesos
            If .Cell(flexcpChecked, lFila, lColum) = flexChecked Then
                Call Fnt_Escribe_Grilla(Grilla_Visor, "N", Space(4) & "Bucando Cortes de Cupon...")
                Call lReg.CargarAutomatica(DTP_Fecha_Proceso.Value)
                If lReg.Errnum <> 0 Then
                    Call Fnt_Escribe_Grilla(Grilla_Visor, cGrillaLog_Error, Space(8) & lReg.ErrMsg)
                End If
            End If
            If .Cell(flexcpChecked, lFila + 1, lColum) = flexChecked Then
                Call Fnt_Escribe_Grilla(Grilla_Visor, "N", Space(4) & "Cargando Valores de Moneda desde " & fOrigen & "...")
                gDB.Parametros.Clear
                gDB.Parametros.Add "Fecha", ePT_Fecha, CDate(DTP_Fecha_Proceso.Value), ePD_Entrada
                gDB.Procedimiento = fProcedimiento
                If Not gDB.EjecutaSP Then
                    Call Fnt_Escribe_Grilla(Grilla_Visor, cGrillaLog_Error, Space(8) & gDB.ErrMsg)
                End If
            End If
            If .Cell(flexcpChecked, lFila + 2, lColum) = flexChecked Then
                Call Fnt_Escribe_Grilla(Grilla_Visor, "N", Space(4) & "Cargando Valores de Duraci�n.")
                Call BuscaDuracion(gcINST_DEPOSITOS_NAC)
                Call BuscaDuracion(gcINST_BONOS_NAC)
            End If
        End With
    Next
    Call Fnt_Escribe_Grilla(Grilla_Visor, "", "Cargas Terminadas.")
    Call Sub_Desbloquea_Puntero(Me)
    
    MsgBox "Proceso Terminado.", vbInformation, Me.Caption
    
End Sub

Private Sub Toolbar_Chequeo_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents
    
    Call Sub_CambiaCheck(Grilla_Procesos, (Button.Key = "SEL_ALL"))
End Sub

Private Sub Sub_LlenaGrillaProceso()
Dim lReg As Object

  With Grilla_Procesos
    .Rows = 1
    For Each lReg In fCollection
      .AddItem ""
      .Cell(flexcpChecked, (.Rows - 1), .ColIndex("chk")) = flexChecked
      Call SetCell(Grilla_Procesos, (.Rows - 1), "PROCESOS", lReg.CaptionCargar)
    Next
    
    .AddItem ""
    .Cell(flexcpChecked, (.Rows - 1), .ColIndex("chk")) = flexChecked
    Call SetCell(Grilla_Procesos, (.Rows - 1), "PROCESOS", "Carga Tipo de Cambios")
    
    .AddItem ""
    .Cell(flexcpChecked, (.Rows - 1), .ColIndex("chk")) = flexChecked
    Call SetCell(Grilla_Procesos, (.Rows - 1), "PROCESOS", "Carga Duraci�n")
  End With
End Sub

Private Sub BuscaDuracion(ByVal CodInstrumento As String)
    Dim lcSaldo_Activo As Class_Saldo_Activos
    Dim lNemotecnico As Class_Nemotecnicos
    Dim lTasa As String
    Dim lReg As hFields
    Dim lReg1 As hFields
    
    Set lcSaldo_Activo = New Class_Saldo_Activos
    
    If BuscaNemos(CodInstrumento, lcSaldo_Activo) Then
        For Each lReg In lcSaldo_Activo.Cursor
            Set lNemotecnico = New Class_Nemotecnicos
            With lNemotecnico
                .Campo("Id_Nemotecnico").Valor = lReg("id_nemotecnico").Value
                If .BuscarView Then
                    For Each lReg1 In .Cursor
                        If lReg1("COD_INSTRUMENTO").Value = gcINST_BONOS_NAC Then
                            Call BuscaTasa(lReg("id_nemotecnico").Value, lTasa)
                        End If
                        If Not GuardarDuracion(lReg("id_nemotecnico").Value, lTasa) Then
                            Call Fnt_Escribe_Grilla(Grilla_Visor, cGrillaLog_Error, Space(8) & "Nemo: " & lReg("nemotecnico").Value & " Tasa:" & lTasa & " Importacion de Duraci�n fall�: " & gDB.ErrMsg)
                        End If
                    Next
                End If
            End With
        Next
    End If
End Sub
    
Private Function BuscaNemos(ByVal CodInstrumento As String, ByRef SaldoActivo As Object) As Boolean
    If Not SaldoActivo.Buscar_Nemotecnicos_Uc(pFecha_Cierre:=CDate(DTP_Fecha_Proceso.Value) _
        , pId_Empresa:=Null _
        , pCod_Instrumento:=CodInstrumento) Then
        BuscaNemos = False
    End If
    BuscaNemos = True
End Function

Private Sub BuscaTasa(ByVal IdNemo As Integer, ByRef Tasa As String)
    Set lPublicador = New Class_Publicadores_Precio
    With lPublicador
        .Campo("id_nemotecnico").Valor = IdNemo
        .Campo("fecha").Valor = DTP_Fecha_Proceso.Value
        If .BuscarUltimoHabil Then
            Tasa = .Tasa_Nemotecnico(IdNemo)
        Else
            Tasa = 0
        End If
    End With
    Set lPublicador = Nothing
End Sub

Private Function GuardarDuracion(ByVal IdNemo As Integer, ByVal Tasa As String) As Boolean
    Dim fClass_Entidad As Class_Entidad
    With fClass_Entidad
        gDB.Parametros.Add "pFecha", ePT_Fecha, DTP_Fecha_Proceso.Value, ePD_Entrada
        gDB.Parametros.Add "pIdNemotecnico", ePT_Numero, IdNemo, ePD_Entrada
        gDB.Parametros.Add "pPrecio", ePT_Numero, Tasa, ePD_Entrada
        gDB.Procedimiento = "PKG_PROCESOS_IMPORTADOR.CisCB_GUARDA_DURACION"
        If Not gDB.EjecutaSP Then
            GuardarDuracion = False
        Else
            GuardarDuracion = True
        End If
    End With
    gDB.Parametros.Clear
End Function
