VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Reporte_AportesRetiros_APV 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte Aportes Retiros"
   ClientHeight    =   8955
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14880
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8955
   ScaleWidth      =   14880
   Begin VB.Frame Frm_Reporte 
      Caption         =   "Aportes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   7845
      Left            =   120
      TabIndex        =   10
      Top             =   1080
      Width           =   14640
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Reporte 
         Height          =   7395
         Left            =   105
         TabIndex        =   11
         Top             =   300
         Width           =   14430
         _cx             =   25453
         _cy             =   13044
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   1
         SelectionMode   =   1
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   14
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Reporte_AportesRetiros_APV.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   0
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin VB.Frame Frm_Tipo_Reporte 
      Caption         =   "Tipo Reporte"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   720
      Left            =   5760
      TabIndex        =   7
      Top             =   360
      Width           =   9000
      Begin VB.OptionButton Op_Retiros 
         Caption         =   "Retiros / Traspaso Salida"
         Height          =   315
         Left            =   840
         TabIndex        =   9
         Top             =   320
         Width           =   2415
      End
      Begin VB.OptionButton Op_Aportes 
         Caption         =   "Aportes / Traspaso Entrada"
         Height          =   315
         Left            =   3840
         TabIndex        =   8
         Top             =   320
         Width           =   2535
      End
   End
   Begin VB.Frame Frm_Periodo_Consulta 
      Caption         =   "Per�odo Consulta"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   720
      Left            =   120
      TabIndex        =   2
      Top             =   360
      Width           =   5595
      Begin MSComCtl2.DTPicker DTP_Fecha_Desde 
         Height          =   315
         Left            =   1200
         TabIndex        =   3
         Top             =   320
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   57802753
         CurrentDate     =   37732
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Hasta 
         Height          =   315
         Left            =   3720
         TabIndex        =   4
         Top             =   320
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   57802753
         CurrentDate     =   37732
      End
      Begin VB.Label Lbl_Fecha_Desde 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Desde"
         Height          =   345
         Left            =   120
         TabIndex        =   6
         Top             =   320
         Width           =   1095
      End
      Begin VB.Label Lbl_Fecha_Hasta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Hasta"
         Height          =   345
         Left            =   2640
         TabIndex        =   5
         Top             =   320
         Width           =   1095
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   14880
      _ExtentX        =   26247
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Buscar"
            Key             =   "SEARCH"
            Description     =   "Busca Aportes y Retiros"
            Object.ToolTipText     =   "Carga Cuadros SVS en Grilla "
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Reporte"
            Key             =   "REPORT"
            Description     =   "Genera Cuadros SVS APV"
            Object.ToolTipText     =   "Genera Cuadros SVS de APV"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXCEL"
                  Text            =   "a Excel "
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9480
         TabIndex        =   1
         Top             =   0
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_AportesRetiros_APV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso As String 'Flags para el permiqso sobre la ventana
Dim fCod_Arbol_Sistema As String 'Codigo para el permiso sobre la ventana
'----------------------------------------------------------------------------

Public fApp_Excel           As Excel.Application      ' Excel application
Public fLibro               As Excel.Workbook      ' Excel workbook

Dim iFilaExcel         As Integer
Dim sTipoReporte       As String
Dim sFechaDesde        As Date
Dim sFechaHasta        As Date
Dim lCursor_Reporte    As hRecord
Dim cargaexitosa       As Boolean
Dim sTipoReporteTitulo       As String

Public Sub Mostrar(pCod_Arbol_Sistema)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  
  Call Form_Resize
  
  Load Me
End Sub
    
    
Private Sub Form_Load()
   
   Me.Top = 1
   Me.Left = 1
   With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("SEARCH").Image = cBoton_Buscar
        .Buttons("REPORT").Image = cBoton_Imprimir
        .Buttons("REFRESH").Image = cBoton_Original
        .Buttons("EXIT").Image = cBoton_Salir
   End With

   Call Sub_CargaForm

End Sub



Private Sub Op_Aportes_Click()
    If Op_Aportes.Value Then
        Grilla_Reporte.Rows = 1
    End If
End Sub

Private Sub Op_Retiros_Click()
    If Op_Retiros.Value Then
        Grilla_Reporte.Rows = 1
    End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEARCH"
        Grilla_Reporte.Rows = 1
        Call Sub_CargarDatos
        If cargaexitosa Then
            Call Sub_CargarGrilla
        End If
    Case "REFRESH"
      Call Sub_Limpiar
    Case "REPORT"
        Call Sub_CargarDatos
        If cargaexitosa Then
           Call Sub_Crea_Informe
        End If
    Case "EXIT"
      Unload Me
  End Select

End Sub
Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
        Call Sub_CargarDatos
        If cargaexitosa Then
           Call Sub_Crea_Informe
        End If
    Case "EXCEL"
        Call Sub_CargarDatos
        If cargaexitosa Then
          Call Sub_Crea_Excel
       End If
  End Select
End Sub

Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre
    Call Sub_Bloquea_Puntero(Me)
    Call Sub_FormControl_Color(Me.Controls)
    
    Grilla_Reporte.GridLines = flexGridNone
    Grilla_Reporte.Rows = 1
    
    Set lCierre = New Class_Verificaciones_Cierre
    Frm_Reporte.Caption = "Reporte"
    DTP_Fecha_Hasta.Value = lCierre.Busca_Ultima_FechaCierre + 1
    DTP_Fecha_Desde.Value = DateAdd("m", -1, lCierre.Busca_Ultima_FechaCierre)
    Op_Retiros.Value = True
    Set lCierre = Nothing
    
    

    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Form_Resize()
   Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub
Private Function ValidaEntradaDatos() As Boolean
Dim bOk As Boolean

    bOk = True
    If DTP_Fecha_Desde.Value > DTP_Fecha_Hasta.Value Then
        MsgBox "Fecha Desde debe ser menor a Fecha Hasta", vbInformation, Me.Caption
        bOk = False
    End If
    ValidaEntradaDatos = bOk
End Function

Private Sub Sub_Limpiar()
Dim lCierre As Class_Verificaciones_Cierre
Dim i As Integer

    Set lCierre = New Class_Verificaciones_Cierre
    Frm_Reporte.Caption = "Reporte"
    
    DTP_Fecha_Hasta.Value = lCierre.Busca_Ultima_FechaCierre + 1
    DTP_Fecha_Desde.Value = DateAdd("m", -1, lCierre.Busca_Ultima_FechaCierre)
    Op_Retiros.Value = True
    Grilla_Reporte.Rows = 1
    
End Sub

Private Sub Sub_CargarDatos()
    sFechaDesde = DTP_Fecha_Desde.Value
    sFechaHasta = DTP_Fecha_Hasta.Value
    sTipoReporte = IIf(Op_Aportes.Value, "A", "R")
    
    If Not ValidaEntradaDatos Then
        Exit Sub
    End If
    
    Call Sub_Bloquea_Puntero(Me)
    
    sTipoReporteTitulo = IIf(sTipoReporte = "A", "Aportes / Traspasos de Entrada", "Retiros / Traspasos de Salida")
    
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_CUENTAS$BuscarMovimientosAPV"
    gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pTipo_Reporte", ePT_Caracter, sTipoReporte, ePD_Entrada
    gDB.Parametros.Add "pFecha_Desde", ePT_Fecha, sFechaDesde, ePD_Entrada
    gDB.Parametros.Add "pFecha_Hasta", ePT_Fecha, sFechaHasta, ePD_Entrada
    
    If Not gDB.EjecutaSP Then
        MsgBox "Error en B�squeda de Movimientos APV " & gDB.ErrMsg, vbInformation, Me.Caption
        cargaexitosa = False
        Call Sub_Desbloquea_Puntero(Me)
        Exit Sub
    End If
    
    
    Set lCursor_Reporte = gDB.Parametros("Pcursor").Valor
    
    If lCursor_Reporte.Count = 0 Then
        MsgBox "No se Registran Movimientos de " & sTipoReporteTitulo, vbInformation, Me.Caption
        cargaexitosa = False
        Call Sub_Desbloquea_Puntero(Me)
        Exit Sub
    End If
    cargaexitosa = True

End Sub
Private Sub Sub_CargarGrilla()
Dim lxHoja               As Worksheet
Dim lLinea                  As Integer
Dim lReg                    As Object
Dim lDecimalesCuenta        As Integer

 
    Frm_Reporte.Caption = "Reporte " & sTipoReporteTitulo
    Grilla_Reporte.Rows = 1
    For Each lReg In lCursor_Reporte
        lLinea = Grilla_Reporte.Rows
        Call Grilla_Reporte.AddItem("")
        Call SetCell(Grilla_Reporte, lLinea, "id_cuenta", Trim(lReg("Id_Cuenta").Value), pAutoSize:=True)
        Call SetCell(Grilla_Reporte, lLinea, "id_cliente", lReg("Id_Cliente").Value, pAutoSize:=True)
        Call SetCell(Grilla_Reporte, lLinea, "Fecha_Movimiento", Format(lReg("Fecha_Movimiento").Value, "dd/mm/yyyy"), pAutoSize:=True)
        Call SetCell(Grilla_Reporte, lLinea, "Rut_Cliente", lReg("Rut_Cliente").Value, pAutoSize:=True)
        Call SetCell(Grilla_Reporte, lLinea, "Nombre_Cliente", lReg("Nombre_Cliente").Value, pAutoSize:=True)
        Call SetCell(Grilla_Reporte, lLinea, "Cuenta", lReg("Cuenta").Value, pAutoSize:=True)
        Call SetCell(Grilla_Reporte, lLinea, "Folio", lReg("Folio").Value, pAutoSize:=True)
        Call SetCell(Grilla_Reporte, lLinea, "Tipo_Ahorro", lReg("Tipo_Ahorro").Value, pAutoSize:=True)
        Call SetCell(Grilla_Reporte, lLinea, "Monto_Cuotas", FormatNumber(NVL(lReg("Monto_Cuotas").Value, 0), lDecimalesCuenta), pAutoSize:=True)
        Call SetCell(Grilla_Reporte, lLinea, "Monto_Bruto", FormatNumber(NVL(lReg("Monto_Bruto").Value, 0), lDecimalesCuenta), pAutoSize:=True)
        Call SetCell(Grilla_Reporte, lLinea, "Comision", FormatNumber(NVL(lReg("Comision").Value, 0), lDecimalesCuenta), pAutoSize:=True)
        Call SetCell(Grilla_Reporte, lLinea, "Monto_Retenido", FormatNumber(NVL(lReg("Monto_Retenido").Value, 0), lDecimalesCuenta), pAutoSize:=True)
        Call SetCell(Grilla_Reporte, lLinea, "Monto_Pagar", FormatNumber(NVL(lReg("Monto_Pagar").Value, 0), lDecimalesCuenta), pAutoSize:=True)
        Call SetCell(Grilla_Reporte, lLinea, "Tipo_Movimiento", lReg("Dsc_Tipo_Movimiento"), pAutoSize:=True)
    Next
    

    
ExitProcedure:
 Call Sub_Desbloquea_Puntero(Me)
End Sub



Private Sub Sub_Crea_Excel()

Dim nHojas, i       As Integer
Dim hoja            As Integer
Dim Index           As Integer


    Set fApp_Excel = CreateObject("Excel.application")
    fApp_Excel.DisplayAlerts = False
    Set fLibro = fApp_Excel.Workbooks.Add
    
    fApp_Excel.ActiveWindow.DisplayGridlines = False

    
    fLibro.Worksheets.Add
    
     For i = fLibro.Worksheets.Count To 2 Step -1
            fLibro.Worksheets(i).Delete
     Next i

        fLibro.Sheets(1).Select
        fLibro.ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).Select
            
        fLibro.ActiveSheet.Range("A4").Value = "Reporte " & sTipoReporteTitulo
        
        fLibro.ActiveSheet.Range("A4:B4").Font.Bold = True
        fLibro.ActiveSheet.Range("A4:B4").HorizontalAlignment = xlLeft
        
        fLibro.ActiveSheet.Range("A5").Value = "Periodo de Consulta " & DTP_Fecha_Desde.Value & " al  " & DTP_Fecha_Hasta.Value
        fLibro.ActiveSheet.Range("A5:B5").HorizontalAlignment = xlLeft
        fLibro.ActiveSheet.Range("A5:B5").Font.Bold = True
        
        
        fLibro.Worksheets.Item(1).Columns("A:A").ColumnWidth = 12
        fLibro.Worksheets.Item(1).Columns("B:B").ColumnWidth = 15
        fLibro.Worksheets.Item(1).Columns("C:C").ColumnWidth = 50
        fLibro.Worksheets.Item(1).Columns("D:D").ColumnWidth = 10
        fLibro.Worksheets.Item(1).Columns("E:E").ColumnWidth = 10
        fLibro.Worksheets.Item(1).Columns("F:F").ColumnWidth = 20
        fLibro.Worksheets.Item(1).Columns("G:G").ColumnWidth = 20
        fLibro.Worksheets.Item(1).Columns("H:H").ColumnWidth = 20
        fLibro.Worksheets.Item(1).Columns("I:I").ColumnWidth = 20
        fLibro.Worksheets.Item(1).Columns("J:J").ColumnWidth = 20
        fLibro.Worksheets.Item(1).Columns("K:K").ColumnWidth = 20
        fLibro.Worksheets.Item(1).Columns("L:L").ColumnWidth = 20
        fLibro.Worksheets.Item(1).Columns("M:M").ColumnWidth = 20
        fLibro.Worksheets.Item(1).Name = IIf(sTipoReporte = "A", "Aportes", "Retiros")
        
        iFilaExcel = 7
    
        Generar_Listado_Excel
    
        fApp_Excel.Visible = True
        fApp_Excel.UserControl = True
    
    
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Generar_Listado_Excel()
Dim lReg                 As hCollection.hFields
Dim lxHoja               As Worksheet

Dim lFilaGrilla          As Integer
Dim iFilaInicio          As Integer
Dim iColGrilla           As Integer
Dim iColExcel            As Integer
Dim sNombre_Col          As String
Dim iTotalColGrilla      As Integer


    
    
    
    iColExcel = 1
    iTotalColGrilla = Grilla_Reporte.Cols
    
    Call ImprimeEncabezado
    
    iFilaInicio = iFilaExcel
    
    Set lxHoja = fLibro.ActiveSheet


    For Each lReg In lCursor_Reporte
        iFilaExcel = iFilaExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = lReg("Fecha_Movimiento").Value
        lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
        lxHoja.Cells(iFilaExcel, iColExcel).HorizontalAlignment = xlCenter
        lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "dd/mm/yyy"
        
        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = lReg("Rut_Cliente").Value
        lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
            
        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = lReg("Nombre_Cliente").Value
        lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
            
        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = lReg("Cuenta").Value
        lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
             
        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = lReg("Folio").Value
        lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin

        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = lReg("Tipo_Ahorro").Value
        lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
     
        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = NVL(lReg("Monto_Cuotas").Value, 0)
        lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
        lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0"

        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = NVL(lReg("Monto_Bruto").Value, 0)
        lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
        lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0"
                   
        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = NVL(lReg("Comision").Value, 0)
        lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
        lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0"
        
        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = NVL(lReg("Monto_Retenido").Value, 0)
        lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
        lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0"
        
        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = NVL(lReg("Monto_Pagar").Value, 0)
        lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
        lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0"
        
        iColExcel = iColExcel + 1
        lxHoja.Cells(iFilaExcel, iColExcel).Value = lReg("Dsc_Tipo_Movimiento").Value
        lxHoja.Cells(iFilaExcel, iColExcel).BorderAround LineStyle:=xlContinuous, Weight:=xlThin
        lxHoja.Cells(iFilaExcel, iColExcel).NumberFormat = "#,##0"
        iColExcel = 1
    Next


End Sub

Private Sub ImprimeEncabezado()
Dim lxHoja          As Worksheet
Dim iColExcel       As Integer
Dim i               As Integer

    Set lxHoja = fLibro.ActiveSheet
    iFilaExcel = iFilaExcel + 1
    iColExcel = 1
    With lxHoja
        .Cells(iFilaExcel, iColExcel).Value = "Fecha"
        
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Rut"
        
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Cliente"
        
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Cuenta"
        
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Folio"
        
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Tipo Ahorro"
       
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Monto Cuotas"
        
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Monto Bruto $"
        
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Comisi�n"
        
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Monto Retenci�n"
        
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Monto a Pagar"
        
        iColExcel = iColExcel + 1
        .Cells(iFilaExcel, iColExcel).Value = "Tipo"
        
        .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).BorderAround
        .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).Borders.Color = RGB(0, 0, 0)
        .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).Interior.Color = RGB(255, 255, 0) '&HC0FFC0
        .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).Font.Bold = True
        .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).WrapText = True
        .Range(.Cells(iFilaExcel, 1), .Cells(iFilaExcel, iColExcel)).HorizontalAlignment = xlCenter
    End With
    
End Sub

Private Sub Sub_Crea_Informe()
Const clrHeader = &HD0D0D0
Dim sHeader_Clt             As String
Dim sFormat_Clt             As String
Dim lForm                   As Frm_Reporte_Generico
Dim sTitulo                 As String
Dim sRecord
Dim bAppend
Dim lDecimalesCuenta        As Integer

Dim lReg                    As hCollection.hFields
Dim lLinea                  As Integer


    lDecimalesCuenta = 0
    
    sTitulo = "Reporte " & sTipoReporteTitulo

       
     sHeader_Clt = "Fecha|Rut|Cliente|Cuenta|Folio|Tipo Ahorro|Monto Cuotas|Monto Bruto $|Comisi�n|Monto Retenci�n|Monto a Pagar|Tipo"
     sFormat_Clt = "900|>1000|2000|900|900|1500|>1300|>1300|>1300|>1300|>1300|700"
     Set lForm = New Frm_Reporte_Generico
      
     Call lForm.Sub_InicarDocumento(pTitulo:=sTitulo _
                                    , pTipoSalida:=ePrinter.eP_Pantalla _
                                    , pOrientacion:=orLandscape)
    
     lForm.VSPrinter.FontSize = 8
     lForm.VSPrinter.Paragraph = "Desde:  " & DTP_Fecha_Desde.Value & " - Hasta:  " & DTP_Fecha_Hasta.Value
     lForm.VSPrinter.Paragraph = "" 'salto de linea
     lForm.VSPrinter.FontBold = True
          
     lForm.VSPrinter.StartTable
     lForm.VSPrinter.FontSize = 7
     lForm.VSPrinter.TableCell(tcAlignCurrency) = False
    
    
     For Each lReg In lCursor_Reporte
         sRecord = Format(lReg("Fecha_Movimiento").Value, "dd/mm/yyyy") & "|" & _
                   NVL(lReg("Rut_Cliente").Value, 0) & "|" & _
                   NVL(lReg("Nombre_Cliente").Value, 0) & "|" & _
                   NVL(lReg("Cuenta").Value, 0) & "|" & _
                   NVL(lReg("Folio").Value, 0) & "|" & _
                   NVL(lReg("Tipo_Ahorro").Value, 0) & "|" & _
                   FormatNumber(NVL(lReg("Monto_Cuotas").Value, 0), lDecimalesCuenta) & "|" & _
                   FormatNumber(NVL(lReg("Monto_Bruto").Value, 0), lDecimalesCuenta) & "|" & _
                   FormatNumber(NVL(lReg("Comision").Value, 0), lDecimalesCuenta) & "|" & _
                   FormatNumber(NVL(lReg("Monto_Retenido").Value, 0), lDecimalesCuenta) & "|" & _
                   FormatNumber(NVL(lReg("Monto_Pagar").Value, 0), lDecimalesCuenta) & "|" & _
                   lReg("Dsc_Tipo_Movimiento")
          
          lForm.VSPrinter.AddTable sFormat_Clt, sHeader_Clt, sRecord, clrHeader, , bAppend

     Next
     lForm.VSPrinter.TableCell(tcFontBold, 0) = True
     lForm.VSPrinter.EndTable
     lForm.VSPrinter.EndDoc

ExitProcedure:
    Call Sub_Desbloquea_Puntero(Me)
End Sub

