VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Proceso_Cierre_Masivo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Proceso Cierre Masivo"
   ClientHeight    =   4785
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   9300
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4785
   ScaleWidth      =   9300
   Begin VB.Timer Timer 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   8460
      Top             =   2550
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   11
      Top             =   4440
      Width           =   9300
      _ExtentX        =   16404
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   159
            MinWidth        =   18
            Key             =   "PRIMERA"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   159
            MinWidth        =   18
            Key             =   "SEGUNDA"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   15981
            Key             =   "TERCERA"
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frnm_Grilla_Sucesos 
      Caption         =   "Visor de Sucesos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3255
      Left            =   60
      TabIndex        =   3
      Top             =   1140
      Width           =   9165
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   2865
         Left            =   120
         TabIndex        =   4
         Top             =   270
         Width           =   7965
         _cx             =   14049
         _cy             =   5054
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   3
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Proceso_Cierre_Masivo.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Detalle 
         Height          =   330
         Left            =   8160
         TabIndex        =   5
         Top             =   540
         Width           =   900
         _ExtentX        =   1588
         _ExtentY        =   582
         ButtonWidth     =   1588
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Detalle"
               Key             =   "DETALLE"
               Object.ToolTipText     =   "Muestra el detalle de los sucesos del Cierre"
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frm_Fecha_Cierre 
      Caption         =   "Fechas Cierre"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   705
      Left            =   60
      TabIndex        =   0
      Top             =   420
      Width           =   9165
      Begin MSComCtl2.DTPicker DTP_Fecha_Hasta 
         Height          =   315
         Left            =   4080
         TabIndex        =   1
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   65470465
         CurrentDate     =   37732
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Desde 
         Height          =   315
         Left            =   1350
         TabIndex        =   8
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   65470465
         CurrentDate     =   37732
      End
      Begin MSComctlLib.ProgressBar ProgressBar_Fecha 
         Height          =   315
         Left            =   5490
         TabIndex        =   10
         Top             =   270
         Width           =   3600
         _ExtentX        =   6350
         _ExtentY        =   556
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
      Begin VB.Label Lbl_Fecha_Desde 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Desde"
         Height          =   345
         Left            =   90
         TabIndex        =   9
         Top             =   270
         Width           =   1215
      End
      Begin VB.Label Lbl_Fecha_Hasta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Hasta"
         Height          =   345
         Left            =   2820
         TabIndex        =   2
         Top             =   270
         Width           =   1215
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   9300
      _ExtentX        =   16404
      _ExtentY        =   635
      ButtonWidth     =   2619
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Cierre Masivo"
            Key             =   "CIERRE"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Realiza el Cierre del d�a"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   7
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Proceso_Cierre_Masivo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fHoraInicio As Long

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("CIERRE").Image = cBoton_Aceptar
      .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  With Toolbar_Detalle
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("DETALLE").Image = cBoton_Agregar_Grilla
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_DblClick()
Dim lMensaje As String

  With Grilla
    If .Row > 0 Then
      lMensaje = GetCell(Grilla, .Row, "TEXTO")
      If Not lMensaje = "" Then
        MsgBox lMensaje, vbInformation, Me.Caption
      End If
    End If
  End With

End Sub

Private Sub Timer_Timer()
Dim lTiempo As Long

  lTiempo = ((GetTickCount - fHoraInicio) / 1000)
  
  Call Sub_StatusWindows(StatusBar, pSegunda:=Format(lTiempo, "#,###.##") & " seg. trans.")
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "CIERRE"
      Call Fnt_Proceso_Cierre
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_Detalle_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "DETALLE"
      Call Grilla_DblClick
  End Select
End Sub

Private Sub Sub_CargaForm()
Dim lCierre As Class_Verificaciones_Cierre

  Rem Limpia la grilla
  Grilla.Rows = 1
  
  Set lCierre = New Class_Verificaciones_Cierre
  DTP_Fecha_Desde.Value = lCierre.Busca_Ultima_FechaCierre + 1
  DTP_Fecha_Hasta.Value = Fnt_FechaServidor
  
End Sub

Private Function Fnt_Proceso_Cierre() As Boolean
Dim lCierre               As Class_Cierre
Dim lVerificacion_Cierre  As Class_Verificaciones_Cierre
Dim lTiempo               As Long
Dim lId_Log_Registro
Dim lFecha_Cierre         As Date
Dim lTiempoCuenta         As Long

  Call Sub_Bloquea_Puntero(Me)
  
  If Not DTP_Fecha_Hasta.Value >= DTP_Fecha_Desde.Value Then
    MsgBox "La Fecha Hasta debe ser mayor o igual a la Fecha Desde para realizar Cierres Masivos.", vbCritical, Me.Caption
    GoTo ErrProcedure
  End If
  
  Grilla.Rows = 1
  fHoraInicio = GetTickCount
  Timer.Enabled = True
  
  lId_Log_Registro = Fnt_Agregar_Log_Proceso(eLog_Subtipo.eLS_CierreMasivo)
  
  Set lCierre = New Class_Cierre
  lCierre.Id_Log_Proceso = lId_Log_Registro 'le paso el proceso que inica las ejecuciones
  
  Set lVerificacion_Cierre = New Class_Verificaciones_Cierre
  
  ProgressBar_Fecha.Max = (DTP_Fecha_Hasta.Value - DTP_Fecha_Desde.Value) + 1
  ProgressBar_Fecha.Value = 0
  
  lFecha_Cierre = DTP_Fecha_Desde.Value
  
  Do While lFecha_Cierre <= DTP_Fecha_Hasta.Value
    Call Sub_StatusWindows(StatusBar, pPrimera:="Fecha: " & Format(lFecha_Cierre, cFormatDate))
    ProgressBar_Fecha.Value = (lFecha_Cierre - DTP_Fecha_Desde.Value) + 1
  
    Grilla.Rows = 1
    If Not lCierre.Fnt_Proceso_Cierre(lFecha_Cierre, Grilla, BarraProceso) Then
      MsgBox "Error en el Proceso de Cierre Masivo.", vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
    
    lTiempo = ((GetTickCount - fHoraInicio) / 1000)
    Call Sub_StatusWindows(StatusBar, pTercera:="Tiempo Prom. Diario: " & Format(lTiempo / ProgressBar_Fecha.Value, "#,###.##") & " seg.")
    
    lFecha_Cierre = lFecha_Cierre + 1
    DoEvents
  Loop
  
  lTiempo = ((GetTickCount - fHoraInicio) / 1000)
  
  MsgBox "Terminado el Proceso de Cierre Masivo." & vbCr & Format(lTiempo, "#,###.##") & " segundos.", vbInformation, Me.Caption
  
ErrProcedure:
  Set lCierre = Nothing
  Set lVerificacion_Cierre = Nothing
  
  Timer.Enabled = False
  Call Sub_Desbloquea_Puntero(Me)
  Beep
End Function

