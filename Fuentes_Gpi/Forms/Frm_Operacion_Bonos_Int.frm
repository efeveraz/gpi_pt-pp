VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Operacion_Bonos_Int 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Bonos Internacionales"
   ClientHeight    =   8730
   ClientLeft      =   45
   ClientTop       =   540
   ClientWidth     =   8940
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8730
   ScaleWidth      =   8940
   Begin VB.Frame Frame2 
      Caption         =   "Cliente"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1035
      Left            =   60
      TabIndex        =   46
      Top             =   390
      Width           =   8805
      Begin hControl2.hTextLabel Txt_Rut 
         Height          =   315
         Left            =   150
         TabIndex        =   47
         Top             =   240
         Width           =   2220
         _ExtentX        =   3916
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1200
         Caption         =   "RUT"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Nombres 
         Height          =   315
         Left            =   2400
         TabIndex        =   48
         Top             =   240
         Width           =   6285
         _ExtentX        =   11086
         _ExtentY        =   556
         LabelWidth      =   1000
         Caption         =   "Nombres"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Perfil 
         Height          =   315
         Left            =   2400
         TabIndex        =   49
         Top             =   600
         Width           =   3300
         _ExtentX        =   5821
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1200
         Caption         =   "Perfil Riesgo"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   315
         Left            =   150
         TabIndex        =   50
         Top             =   600
         Width           =   2220
         _ExtentX        =   3916
         _ExtentY        =   556
         LabelWidth      =   1000
         TextMinWidth    =   1200
         Caption         =   "Cuenta"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_caja 
         Height          =   315
         Left            =   5745
         TabIndex        =   52
         Top             =   600
         Width           =   2940
         _ExtentX        =   5186
         _ExtentY        =   556
         LabelWidth      =   700
         TextMinWidth    =   1200
         Caption         =   "Caja"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
   End
   Begin VB.Frame Frame_Nemotecnico 
      Caption         =   "Nemot�cnico"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   2295
      Left            =   60
      TabIndex        =   29
      Top             =   6360
      Width           =   8820
      Begin TrueDBList80.TDBCombo Cmb_Nemotecnico 
         Height          =   345
         Left            =   1530
         TabIndex        =   11
         Tag             =   "OBLI=S;CAPTION=Nemot�cnico"
         Top             =   270
         Width           =   4035
         _ExtentX        =   7117
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   -1  'True
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Operacion_Bonos_Int.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Frame Frame_Inversion 
         Caption         =   "Inversi�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1275
         Left            =   150
         TabIndex        =   30
         Top             =   960
         Width           =   8565
         Begin VB.CheckBox Chk_Vende_Todo 
            Caption         =   "Vende Todo"
            Height          =   315
            Left            =   3420
            TabIndex        =   22
            Top             =   330
            Width           =   1185
         End
         Begin VB.Frame Frame_Precio 
            Caption         =   "Precio"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   675
            Left            =   4680
            TabIndex        =   31
            Top             =   150
            Width           =   3795
            Begin VB.TextBox Txt_TasaTransferencia 
               Height          =   315
               Left            =   4935
               MaxLength       =   10
               TabIndex        =   32
               Top             =   270
               Visible         =   0   'False
               Width           =   765
            End
            Begin hControl2.hTextLabel Txt_PrecioInversion 
               Height          =   315
               Left            =   150
               TabIndex        =   13
               Top             =   240
               Width           =   1740
               _ExtentX        =   3069
               _ExtentY        =   556
               LabelWidth      =   735
               TextMinWidth    =   870
               Caption         =   "Tasa"
               Text            =   "0.0000"
               Text            =   "0.0000"
               BackColorTxt    =   12648384
               BackColorTxt    =   12648384
               Format          =   "#,##0.0000"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin hControl2.hTextLabel Txt_PrecioCierre 
               Height          =   315
               Left            =   2010
               TabIndex        =   26
               Top             =   240
               Width           =   1710
               _ExtentX        =   3016
               _ExtentY        =   556
               LabelWidth      =   735
               TextMinWidth    =   870
               Caption         =   "Cierre"
               Text            =   "0.0000"
               Text            =   "0.0000"
               Locked          =   -1  'True
               BackColorTxt    =   12648447
               BackColorTxt    =   12648447
               Format          =   "#,##0.0000"
               Tipo_TextBox    =   1
               Alignment       =   1
            End
            Begin VB.Label Label3 
               BorderStyle     =   1  'Fixed Single
               Caption         =   "Transferencia"
               Height          =   315
               Left            =   3795
               TabIndex        =   33
               Top             =   270
               Visible         =   0   'False
               Width           =   1110
            End
         End
         Begin MSComctlLib.Toolbar Toolbar_Operacion 
            Height          =   330
            Left            =   6150
            TabIndex        =   15
            Top             =   870
            Width           =   2310
            _ExtentX        =   4075
            _ExtentY        =   582
            ButtonWidth     =   1826
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Aceptar"
                  Key             =   "OK"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Agregar un nemotecnico a la Operaci�n"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Cancelar"
                  Key             =   "CANCEL"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar2 
               Height          =   255
               Left            =   9420
               TabIndex        =   37
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
         Begin hControl2.hTextLabel Txt_Cantidad_Inversion 
            Height          =   315
            Left            =   90
            TabIndex        =   12
            Tag             =   "OBLI"
            Top             =   330
            Width           =   3255
            _ExtentX        =   5741
            _ExtentY        =   556
            LabelWidth      =   1470
            Caption         =   "Nominales"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_MontoOperacion 
            Height          =   315
            Left            =   90
            TabIndex        =   14
            Tag             =   "OBLI"
            Top             =   690
            Width           =   3255
            _ExtentX        =   5741
            _ExtentY        =   556
            LabelWidth      =   1470
            Caption         =   "Monto"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
      End
      Begin hControl2.hTextLabel Txt_Emisor 
         Height          =   315
         Left            =   150
         TabIndex        =   23
         Top             =   630
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   556
         LabelWidth      =   1350
         Caption         =   "Emisor"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Moneda 
         Height          =   315
         Left            =   5670
         TabIndex        =   24
         Top             =   630
         Width           =   3015
         _ExtentX        =   5318
         _ExtentY        =   556
         LabelWidth      =   1350
         Caption         =   "Moneda"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Cantidad 
         Height          =   315
         Left            =   5670
         TabIndex        =   25
         Top             =   270
         Width           =   3015
         _ExtentX        =   5318
         _ExtentY        =   556
         LabelWidth      =   1350
         Caption         =   "Cantidad"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
      End
      Begin hControl2.hTextLabel Txt_Nemotecnico 
         Height          =   315
         Left            =   150
         TabIndex        =   41
         Top             =   270
         Visible         =   0   'False
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   556
         LabelWidth      =   1350
         Caption         =   "Nemot�cnico"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin VB.Label lbl_nemotecnico 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Nemot�nico"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   150
         TabIndex        =   36
         Top             =   270
         Width           =   1365
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   27
      Top             =   0
      Width           =   8940
      _ExtentX        =   15769
      _ExtentY        =   635
      ButtonWidth     =   1588
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   28
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VB.Frame Frame_Principal 
      Caption         =   "Datos Operaci�n"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4935
      Left            =   60
      TabIndex        =   34
      Top             =   1410
      Width           =   8820
      Begin VB.CheckBox chkAporteRetiro 
         Caption         =   "� Aporte/Retiro de Capital ?"
         Height          =   375
         Left            =   3720
         TabIndex        =   53
         Top             =   240
         Value           =   1  'Checked
         Width           =   1695
      End
      Begin VB.Frame Frame_Comisiones 
         Height          =   705
         Left            =   150
         TabIndex        =   42
         Top             =   2130
         Width           =   8565
         Begin hControl2.hTextLabel Txt_Comision 
            Height          =   315
            Left            =   120
            TabIndex        =   6
            Tag             =   "OBLI"
            Top             =   210
            Width           =   2745
            _ExtentX        =   4842
            _ExtentY        =   556
            LabelWidth      =   1300
            TextMinWidth    =   500
            Caption         =   "COMM"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.0000"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Fees 
            Height          =   315
            Left            =   2910
            TabIndex        =   7
            Tag             =   "OBLI"
            Top             =   210
            Width           =   2745
            _ExtentX        =   4842
            _ExtentY        =   556
            LabelWidth      =   1300
            TextMinWidth    =   500
            Caption         =   "Fees"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
         Begin hControl2.hTextLabel Txt_Interest 
            Height          =   315
            Left            =   5700
            TabIndex        =   8
            Tag             =   "OBLI"
            Top             =   210
            Width           =   2745
            _ExtentX        =   4842
            _ExtentY        =   556
            LabelWidth      =   1300
            TextMinWidth    =   500
            Caption         =   "Interest"
            Text            =   ""
            BackColorTxt    =   12648384
            BackColorTxt    =   12648384
            Format          =   "#,##0.00"
            Tipo_TextBox    =   1
            Alignment       =   1
         End
      End
      Begin hControl2.hTextLabel Txt_Num_Operacion 
         Height          =   315
         Left            =   150
         TabIndex        =   16
         Top             =   270
         Width           =   3285
         _ExtentX        =   5794
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "N� Operaci�n"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   1905
         Left            =   150
         TabIndex        =   9
         Top             =   2910
         Width           =   8115
         _cx             =   14314
         _cy             =   3360
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   10
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Operacion_Bonos_Int.frx":00AA
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin hControl2.hTextLabel Txt_DiasVigencia 
         Height          =   315
         Left            =   5880
         TabIndex        =   21
         Top             =   1800
         Width           =   2235
         _ExtentX        =   3942
         _ExtentY        =   556
         LabelWidth      =   1300
         TextMinWidth    =   870
         Caption         =   "Dias Vigencia"
         Text            =   ""
         BackColorTxt    =   16777215
         BackColorTxt    =   16777215
         Format          =   "#,##0"
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_FechaVigencia 
         Height          =   315
         Left            =   5880
         TabIndex        =   18
         Top             =   660
         Width           =   2820
         _ExtentX        =   4974
         _ExtentY        =   556
         LabelWidth      =   1300
         TextMinWidth    =   1000
         Caption         =   "Fecha Vigencia"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin MSComctlLib.Toolbar Toolbar_Grilla 
         Height          =   660
         Left            =   8310
         TabIndex        =   10
         Top             =   3210
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1164
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "ADD"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Agregar un nemotecnico a la Operaci�n"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "DEL"
               Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
               Object.ToolTipText     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
            EndProperty
         EndProperty
         Begin MSComctlLib.ProgressBar ProgressBar1 
            Height          =   255
            Left            =   9420
            TabIndex        =   35
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
      Begin MSComCtl2.DTPicker Dtp_FechaLiquidacion 
         Height          =   345
         Left            =   2520
         TabIndex        =   5
         Tag             =   "OBLI"
         Top             =   1800
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   67043329
         CurrentDate     =   38768
      End
      Begin TrueDBList80.TDBCombo Cmb_Representantes 
         Height          =   345
         Left            =   1440
         TabIndex        =   3
         Top             =   1410
         Width           =   4365
         _ExtentX        =   7699
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Operacion_Bonos_Int.frx":027A
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_TipoPrecio 
         Height          =   345
         Left            =   7200
         TabIndex        =   20
         Tag             =   "OBLI=S;CAPTION=Tipo Precio"
         Top             =   1410
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Operacion_Bonos_Int.frx":0324
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_FechaLiquidacion 
         Height          =   345
         Left            =   1770
         TabIndex        =   4
         Top             =   1800
         Width           =   705
         _ExtentX        =   1244
         _ExtentY        =   609
         _LayoutType     =   4
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   1
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=1"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Operacion_Bonos_Int.frx":03CE
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Named:id=33:Normal"
         _StyleDefs(35)  =   ":id=33,.parent=0"
         _StyleDefs(36)  =   "Named:id=34:Heading"
         _StyleDefs(37)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(38)  =   ":id=34,.wraptext=-1"
         _StyleDefs(39)  =   "Named:id=35:Footing"
         _StyleDefs(40)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(41)  =   "Named:id=36:Selected"
         _StyleDefs(42)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(43)  =   "Named:id=37:Caption"
         _StyleDefs(44)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(45)  =   "Named:id=38:HighlightRow"
         _StyleDefs(46)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=39:EvenRow"
         _StyleDefs(48)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(49)  =   "Named:id=40:OddRow"
         _StyleDefs(50)  =   ":id=40,.parent=33"
         _StyleDefs(51)  =   "Named:id=41:RecordSelector"
         _StyleDefs(52)  =   ":id=41,.parent=34"
         _StyleDefs(53)  =   "Named:id=42:FilterBar"
         _StyleDefs(54)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Contraparte 
         Height          =   345
         Left            =   1440
         TabIndex        =   1
         Top             =   630
         Width           =   4365
         _ExtentX        =   7699
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Operacion_Bonos_Int.frx":0478
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Traders 
         Height          =   345
         Left            =   1440
         TabIndex        =   2
         Top             =   1020
         Width           =   4365
         _ExtentX        =   7699
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Operacion_Bonos_Int.frx":0522
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Operacion 
         Height          =   345
         Left            =   7200
         TabIndex        =   0
         Tag             =   "OBLI"
         Top             =   270
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   67043329
         CurrentDate     =   38768
      End
      Begin hControl2.hTextLabel Txt_MontoTotal 
         Height          =   315
         Left            =   5880
         TabIndex        =   19
         Top             =   1050
         Width           =   2820
         _ExtentX        =   4974
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Total Aprox."
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
         Format          =   "#,##0.00"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_FechaIngreso_Real 
         Height          =   315
         Left            =   5880
         TabIndex        =   17
         Top             =   270
         Width           =   2820
         _ExtentX        =   4974
         _ExtentY        =   556
         LabelWidth      =   1300
         Caption         =   "Fecha Operaci�n"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin VB.Label lbl_fecha_ingreso 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Operaci�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   5880
         TabIndex        =   45
         Top             =   270
         Width           =   1305
      End
      Begin VB.Label Label5 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Contraparte"
         Height          =   345
         Left            =   150
         TabIndex        =   44
         Top             =   630
         Width           =   1275
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Trader"
         Height          =   345
         Left            =   150
         TabIndex        =   43
         Top             =   1020
         Width           =   1275
      End
      Begin VB.Label lbl_tipo_precio 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Tipo Precio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   5880
         TabIndex        =   40
         Top             =   1410
         Width           =   1290
      End
      Begin VB.Label lbl_fecha_liquidacion 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Liquidaci�n T+"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   150
         TabIndex        =   39
         Top             =   1800
         Width           =   1605
      End
      Begin VB.Label Label4 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Representantes"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   150
         TabIndex        =   38
         Top             =   1410
         Width           =   1275
      End
   End
   Begin hControl2.hTextLabel hTextLabel1 
      Height          =   315
      Left            =   0
      TabIndex        =   51
      Top             =   0
      Width           =   6285
      _ExtentX        =   11086
      _ExtentY        =   556
      LabelWidth      =   1000
      Caption         =   "Nombres"
      Text            =   ""
      Locked          =   -1  'True
      BackColorTxt    =   12648447
      BackColorTxt    =   12648447
   End
End
Attribute VB_Name = "Frm_Operacion_Bonos_Int"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
'-------------------------------------
Rem PARA CONFIRMACION
Dim fErrNum As Double
Dim fErrMsg As String
Dim fOperaciones As Class_Operaciones
Dim fId_Operacion As String
Dim fForm_Confirmacion As Boolean
'------------------------------------
Rem PARA CONSULTA DE OPERACION
Dim fConsulta_Operacion As Boolean
'------------------------------------
Dim fSalir      As Boolean
Dim fEstadoOK   As Boolean
Dim fOperacion  As String
Dim fId_Cuenta  As String
Dim fId_Cliente As Double
Dim fFormOri    As Form
Dim fTipo_Operacion As String
Dim fCod_Instrumento As String
Dim fModif_Grilla As Boolean
Dim fId_Nemotecnico As String
Dim fEmisor As Long
Dim fFecha_Operacion As Date

Const fc_Mercado = cMercado_Internacional

Private Enum eNem_Colum
  eNem_nemotecnico
  eNem_Descripcion
  eNem_Id_Nemotecnico
End Enum

Private Enum eTipo
  eT_Normal
  eT_Grande
End Enum

Private Sub Cmb_Contraparte_ItemChange()
Dim lId_Contraparte As String
  
  lId_Contraparte = Fnt_FindValue4Display(Cmb_Contraparte, Cmb_Contraparte.Text)
  
  If lId_Contraparte = "" Then
    'como nunca va a existir la contraparte -1 se pasa el parametro
    lId_Contraparte = "-1"
  End If
  
  Call Sub_CargaCombo_Traders(Cmb_Traders, lId_Contraparte)
End Sub

Private Sub Chk_Vende_Todo_Click()
Dim lcBonos_Int As Class_Bonos_Int
Dim lSaldo_Cantidad As Double

  If Chk_Vende_Todo.Value Then
    Rem ---------------------------------------------
    Txt_Cantidad_Inversion.Locked = True
    
'    If fOperacion = gcTipoOperacion_Egreso Then
'      'Si se esta realizando una venta del activo
'
'      Set lcBonos_Int = New Class_Bonos_Int
'      lSaldo_Cantidad = lcBonos_Int.Saldo_Activo_Cantidad(fId_Cuenta, Fnt_FindValue4Display(Cmb_Nemotecnico, Cmb_Nemotecnico.Text))
'
'      Txt_Cantidad_Inversion.Text = lSaldo_Cantidad
'    End If
    Txt_Cantidad_Inversion.Text = Txt_Cantidad.Text
    
    Txt_Cantidad_Inversion.BackColorTxt = fColorNoEdit
    Rem ---------------------------------------------
    Txt_MontoOperacion.Locked = True
    Txt_MontoOperacion.Text = Txt_Cantidad.Text * Txt_PrecioInversion.Text
    Txt_MontoOperacion.BackColorTxt = fColorNoEdit
  Else
    Rem ---------------------------------------------
    Txt_Cantidad_Inversion.Locked = False
    Txt_Cantidad_Inversion.Text = 0
    Txt_Cantidad_Inversion.BackColorTxt = fColorOBligatorio
    Rem ---------------------------------------------
    Txt_MontoOperacion.Locked = False
    Txt_MontoOperacion.Text = 0
    Txt_MontoOperacion.BackColorTxt = fColorOBligatorio
  End If
End Sub

Private Sub Cmb_FechaLiquidacion_Change()
  If Not Cmb_FechaLiquidacion.Text = "" Then
    Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Cmb_FechaLiquidacion.Text)
  End If
End Sub

Private Sub Cmb_FechaLiquidacion_ItemChange()
  If Not Cmb_FechaLiquidacion.Text = "" Then
    Dtp_FechaLiquidacion.MinDate = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Cmb_FechaLiquidacion.Text)
    Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Cmb_FechaLiquidacion.Text)
  End If
End Sub

Private Sub Cmb_Nemotecnico_GotFocus()
  Call Sub_ComboSelectedItem(Cmb_Nemotecnico, "")
End Sub

Private Sub DTP_Fecha_Operacion_Change()
  fFecha_Operacion = DTP_Fecha_Operacion.Value
  'If Not Cmb_FechaLiquidacion.Text = "" Then
    Dtp_FechaLiquidacion.MinDate = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
    Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, Cmb_FechaLiquidacion.Text)
  'End If
End Sub

Private Sub DTP_Fecha_Operacion_Validate(Cancel As Boolean)
  If Fnt_Verifica_Feriado(DTP_Fecha_Operacion.Value) Then
    MsgBox "Solo se pueden ingresar operaciones en d�as habiles.", vbExclamation, Me.Caption
    Cancel = True
  End If
End Sub

Private Sub Dtp_FechaLiquidacion_Change()
  Dtp_FechaLiquidacion.Value = Fnt_Dia_Habil_MasProximo(Dtp_FechaLiquidacion.Value)
  Cmb_FechaLiquidacion.Text = Fnt_DiasHabiles_EntreFechas(fFecha_Operacion, Dtp_FechaLiquidacion.Value)
End Sub

Private Sub Cmb_FechaLiquidacion_KeyPress(KeyAscii As Integer)
  Rem Acepta digitos, ".", "-" y "backspace"
  If Not KeyAscii = 8 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub

Private Sub Cmb_Nemotecnico_ItemChange()
Dim lId_Nemotecnico As String

  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False

  lId_Nemotecnico = Fnt_FindValue4Display(Cmb_Nemotecnico, Cmb_Nemotecnico.Text)

  If Not lId_Nemotecnico = "" Then
    Rem Validaci�n: No se puede ingresar una instrucci�n para un mismo nemot�cnico
'    If Fnt_Busca_Nemotecnico(lId_Nemotecnico, fModif_Grilla) Then
'      MsgBox "El Nemot�cnico " & Cmb_Nemotecnico.Text & " ya existe en la grilla.", vbCritical, Me.Caption
'      Cmb_Nemotecnico.Text = ""
'      Exit Sub
'    End If
    If Not fModif_Grilla Then
      Call Sub_Llena_Nemotecnico(lId_Nemotecnico)
    End If
  Else
    'limpia para saber si no se ha elegido un nemotecnico
    Txt_Moneda.Tag = ""
  End If

  Me.Enabled = True
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Cmb_Nemotecnico_LostFocus()
  If Txt_Moneda.Tag = "" Then
    Call Cmb_Nemotecnico_ItemChange
  End If
End Sub

Private Sub Sub_Llena_Nemotecnico(pId_Nemotecnico As String)
Dim lNemotecnico As Class_Nemotecnicos
Dim lPrecio As Class_Publicadores_Precio
Dim lReg As hFields
Dim lcBonos_Int As Class_Bonos_Int
Dim lSaldo_Cantidad As Double
Dim lFila As Long

  Set lNemotecnico = New Class_Nemotecnicos
  With lNemotecnico
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    If .BuscarView Then
      For Each lReg In .Cursor
        
        For lFila = 1 To Grilla.Rows - 1
          If Not lReg("id_moneda_transaccion").Value = To_Number(GetCell(Grilla, lFila, "id_moneda")) Then
            MsgBox "La moneda debe ser la misma que la de los nemot�cnicos ya ingresados.", vbCritical, Me.Caption
            Exit Sub
          End If
        Next
      
        If fOperacion = gcTipoOperacion_Egreso Then
          'Si se esta realizando una venta del activo
          
          Set lcBonos_Int = New Class_Bonos_Int
          lSaldo_Cantidad = lcBonos_Int.Saldo_Activo_Cantidad(fId_Cuenta, pId_Nemotecnico)
            
          Txt_Cantidad.Text = lSaldo_Cantidad
        End If
        
        fEmisor = lReg("id_emisor_especifico").Value
        Txt_Emisor.Text = lReg("dsc_emisor_especifico").Value
        With Txt_Moneda
          .Text = NVL(lReg("dsc_moneda_transaccion").Value, "")
          .Tag = NVL(lReg("id_moneda_transaccion").Value, "")
          
          Txt_MontoOperacion.Format = Fnt_Formato_Moneda(.Tag)
        End With
      Next
    End If
  End With

  Set lPrecio = New Class_Publicadores_Precio
  With lPrecio
    .Campo("id_nemotecnico").Valor = pId_Nemotecnico
    .Campo("fecha").Valor = DTP_Fecha_Operacion.Value 'Fnt_FechaServidor
    If .Buscar_Ultimo_Cta_Nemotecnico(fId_Cuenta) Then
      Txt_PrecioCierre.Text = NVL(.Campo("precio").Valor, 0)
      Txt_PrecioInversion.Text = NVL(.Campo("precio").Valor, 0)
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en carga de Precio de Publicadores.", _
                        .ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lPrecio = Nothing
    
End Sub

Private Sub Cmb_Nemotecnico_Validate(Cancel As Boolean)
  If Not Cmb_Nemotecnico.Text = "" Then
    Rem si el nemotecnico no es NULL
    If Fnt_FindValue4Display(Cmb_Nemotecnico, Cmb_Nemotecnico.Text) = "" Then
      MsgBox "El nemot�cnico ingresado no existe.", vbCritical, Me.Caption
      Cancel = True
      Cmb_Nemotecnico.Text = ""
    End If
  End If
End Sub

Private Sub Form_Load()
 
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
  End With

  With Toolbar_Grilla
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("ADD").Image = cBoton_Agregar_Grilla
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Appearance = ccFlat
  End With

  With Toolbar_Operacion
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("OK").Image = cBoton_Aceptar
      .Buttons("CANCEL").Image = cBoton_Cancelar
      .Appearance = ccFlat
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  fSalir = True
End Sub

Private Sub Grilla_Click()
Dim lId_Nemotecnico As String
  
  Call Sub_Bloquea_Puntero(Me)
  If Not fConsulta_Operacion Then
    With Grilla
      If .Row > 0 Then
        Call Sub_FormTama�o(eT_Grande, fTipo_Operacion)
        lId_Nemotecnico = GetCell(Grilla, .Row, "id_nemotecnico")
        fId_Nemotecnico = lId_Nemotecnico
        Txt_Nemotecnico.Text = GetCell(Grilla, .Row, "dsc_nemotecnico")
        Cmb_Nemotecnico.Text = GetCell(Grilla, .Row, "dsc_nemotecnico")
        Call Sub_Llena_Nemotecnico(lId_Nemotecnico)
        Txt_MontoOperacion.Text = GetCell(Grilla, .Row, "monto")
        Txt_PrecioInversion.Text = GetCell(Grilla, .Row, "Precio")
        Txt_Cantidad_Inversion.Text = GetCell(Grilla, .Row, "Cantidad")
        fModif_Grilla = True
      End If
    End With
  End If
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Toolbar_Grilla_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_AgregarItem
    Case "DEL"
      Call Sub_EliminarItem
      Call Sub_Calcula_MontoTotal
  End Select
End Sub

Private Sub Toolbar_Operacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "OK"
      If Fnt_AceptarDetalle Then
        Call Sub_FormTama�o(eT_Normal, fTipo_Operacion)
        Call Sub_Calcula_MontoTotal
        Grilla.SetFocus
      End If
    Case "CANCEL"
      Call Sub_FormTama�o(eT_Normal, fTipo_Operacion)
      'Call Sub_Calcula_MontoTotal
      Grilla.SetFocus
  End Select
End Sub

Private Sub Sub_Calcula_MontoTotal()
Dim lLinea    As Long
Dim lTotal    As Double
Dim lCOMM     As Double
Dim lFees     As Double
Dim lInterest As Double
  
  lTotal = 0
  For lLinea = 1 To (Grilla.Rows - 1)
    lTotal = lTotal + GetCell(Grilla, lLinea, "monto")
  Next
  
  lCOMM = To_Number(Txt_Comision.Text)
  lFees = To_Number(Txt_Fees.Text)
  lInterest = To_Number(Txt_Interest.Text)
  
  If fOperacion = gcTipoOperacion_Ingreso Then
    Txt_MontoTotal.Text = lTotal + lCOMM + lFees + lInterest
  ElseIf fOperacion = gcTipoOperacion_Egreso Then
    Txt_MontoTotal.Text = lTotal - (lCOMM + lFees + lInterest)
  End If
  
End Sub

Private Sub Txt_Cantidad_Inversion_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Cantidad_Inversion_LostFocus()
  If Not Txt_PrecioInversion.Text = "" Then
    Txt_PrecioInversion_LostFocus
  ElseIf Not Txt_MontoOperacion.Text = "" Then
    Txt_MontoOperacion_LostFocus
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      Rem PREGUNTA SI LA PANTALLA PADRE ES LA DE "CONFIRMACION DE INSTRUCCIONES"
      If fForm_Confirmacion Then
        If Fnt_Grabar_Confirmacion Then
          fEstadoOK = True
          fSalir = True
        End If
      Else
        If Fnt_Grabar Then
          'Si no hubo problemas al grabar, sale
          fEstadoOK = True
          fSalir = True
          'Unload Me
        End If
      End If
    Case "EXIT"
      fEstadoOK = False
      fSalir = True
      'Unload Me
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lcBonos_Int As Class_Bonos_Int
Dim lLinea As Long
Dim lId_Contraparte As String
Dim lTipo_Precio As String
Dim lId_representante As String
Dim lFecha_Operacion  As Date
Dim lFecha_Vigencia As Date
Dim lFecha_Liquidacion As Date
Dim lId_Trader As String
'---------------------------------
Dim lcRestricc_Rel_Porc As Class_Restricciones_Rel_Porc
Dim lId_Nemotecnico As String
'---------------------------------
Dim lId_Caja_Cuenta As Double
Dim lNum_Error      As Double
Dim lMsg_Error      As String
'---------------------------------
Dim lRollback As Boolean
Dim lsChkAporteRetiro As String
Dim lErrOrden As String

    lsChkAporteRetiro = IIf(chkAporteRetiro.Value = 1, "SI", "NO")

  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False
  
  gDB.IniciarTransaccion
  
  lRollback = True
  Fnt_Grabar = False

  If Not Fnt_ValidarDatos Then
    GoTo ErrProcedure
  End If
  lId_Caja_Cuenta = Txt_caja.Tag
  If fOperacion = gcTipoOperacion_Ingreso Then
     Rem Si es una compra
     Rem Validar si el Instrumento y Nemot�cnicos est�n relacionados al perfil de riesgo que le corresponde a la cuenta
     If Not Fnt_Valida_Perfil_Cuenta_Instrm_Nemo(Grilla, fId_Cuenta, fCod_Instrumento) Then
        GoTo ErrProcedure
     End If

  End If
  
  Rem Validacion de Restriccion porcentual Perfiles solo para Instrucciones
  Rem 22/09/2009 MMardones. El control de restricci�n es s�lo para Ingresos
  If fTipo_Operacion = gcOPERACION_Instruccion And fOperacion = gcTipoOperacion_Ingreso Then

  
    Rem Rescata el id_nemotecnico del primer elemento de la grilla solo para ejecutar el proceso de validacion de
    Rem restriccion porcentual de perfil de riesgo, el cual requiere cod_instrumento y cod_producto
    lId_Nemotecnico = GetCell(Grilla, 1, "id_nemotecnico")
  
    Rem Valida la Restricci�n Porcentual del Perfil de Riesgo de Instrumentos y Productos
    Set lcRestricc_Rel_Porc = New Class_Restricciones_Rel_Porc
    With lcRestricc_Rel_Porc
      If Not .Fnt_Restriccion_Rel_Porc(fId_Cuenta, _
                                       lId_Nemotecnico, _
                                       To_Number(Txt_MontoOperacion.Text) + To_Number(Txt_MontoTotal.Text), _
                                       Txt_Moneda.Tag, _
                                       pFecha_Operacion:=DTP_Fecha_Operacion.Value) Then
        Exit Function
      End If
    End With
    Set lcRestricc_Rel_Porc = Nothing
  End If
    
  Set lcBonos_Int = New Class_Bonos_Int
  For lLinea = 1 To (Grilla.Rows - 1)
    Call lcBonos_Int.Agregar_Operaciones_Detalle(GetCell(Grilla, lLinea, "id_nemotecnico"), _
                                                 GetCell(Grilla, lLinea, "cantidad"), _
                                                 GetCell(Grilla, lLinea, "precio"), _
                                                 GetCell(Grilla, lLinea, "id_moneda"), _
                                                 GetCell(Grilla, lLinea, "monto"), _
                                                 GetCell(Grilla, lLinea, "Flg_Vende_Todo"))
  Next

  lId_Contraparte = Fnt_ComboSelected_KEY(Cmb_Contraparte)
  lId_representante = Fnt_ComboSelected_KEY(Cmb_Representantes)
  lFecha_Operacion = DTP_Fecha_Operacion.Value ' Fnt_FechaServidor
  lId_Trader = Fnt_ComboSelected_KEY(Cmb_Traders)
  lFecha_Vigencia = lFecha_Operacion + To_Number(Txt_DiasVigencia.Text)
  
  lFecha_Liquidacion = Dtp_FechaLiquidacion.Value
  
  lTipo_Precio = Fnt_ComboSelected_KEY(Cmb_TipoPrecio)

  Select Case fTipo_Operacion
    Case gcOPERACION_Directa
      If Not lcBonos_Int.Realiza_Operacion_Directa(pId_Operacion:=fId_Operacion, _
                                              pId_Cuenta:=fId_Cuenta, _
                                              pDsc_Operacion:="", _
                                              pTipoOperacion:=fOperacion, _
                                              pCod_Instrumento:=fCod_Instrumento, _
                                              pId_Contraparte:=lId_Contraparte, _
                                              pId_Representante:=lId_representante, _
                                              pId_Moneda_Operacion:=Txt_Moneda.Tag, _
                                              pFecha_Operacion:=lFecha_Operacion, _
                                              pFecha_Vigencia:=lFecha_Vigencia, _
                                              pFecha_Liquidacion:=lFecha_Liquidacion, _
                                              pId_Trader:=lId_Trader, _
                                              pPorc_Comision:=0, _
                                              pComision:=Txt_Comision.Text, _
                                              pDerechos_Bolsa:=Txt_Fees.Text, _
                                              pGastos:=0, _
                                              pIva:=0, _
                                              pMonto_Operacion:=Txt_MontoTotal.Text, _
                                              pTipo_Precio:=lTipo_Precio, _
                                              pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                              pChkAporteRetiro:=lsChkAporteRetiro, pInterest:=Txt_Interest.Text) Then
        Call Fnt_MsgError(lcBonos_Int.SubTipo_LOG, _
                          "Problemas en grabar Bonos Internacionales.", _
                          lcBonos_Int.ErrMsg, _
                          pConLog:=True)
        GoTo ErrProcedure
      End If
    Case gcOPERACION_Custodia
      If Not lcBonos_Int.Realiza_Operacion_Custodia(pId_Operacion:=fId_Operacion, _
                                              pId_Cuenta:=fId_Cuenta, _
                                              pDsc_Operacion:="", _
                                              pTipoOperacion:=fOperacion, _
                                              pCod_Instrumento:=fCod_Instrumento, _
                                              pId_Contraparte:=lId_Contraparte, _
                                              pId_Representante:=lId_representante, _
                                              pId_Moneda_Operacion:=Txt_Moneda.Tag, _
                                              pFecha_Operacion:=lFecha_Operacion, _
                                              pFecha_Vigencia:=lFecha_Vigencia, _
                                              pFecha_Liquidacion:=lFecha_Liquidacion, _
                                              pId_Trader:=lId_Trader, _
                                              pPorc_Comision:=0, _
                                              pComision:=Txt_Comision.Text, _
                                              pDerechos_Bolsa:=Txt_Fees.Text, _
                                              pGastos:=0, _
                                              pIva:=0, _
                                              pMonto_Operacion:=Txt_MontoTotal.Text, _
                                              pTipo_Precio:=lTipo_Precio, _
                                              pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                              pInterest:=Txt_Interest.Text, _
                                              pChkAporteRetiro:=lsChkAporteRetiro) Then
        Call Fnt_MsgError(lcBonos_Int.SubTipo_LOG, _
                          "Problemas en grabar Bonos Internacionales.", _
                          lcBonos_Int.ErrMsg, _
                          pConLog:=True)
        GoTo ErrProcedure
      End If
    Case gcOPERACION_Instruccion
      If Not lcBonos_Int.Realiza_Operacion_Instruccion(pId_Operacion:=fId_Operacion, _
                                                  pId_Cuenta:=fId_Cuenta, _
                                                  pDsc_Operacion:="", _
                                                  pTipoOperacion:=fOperacion, _
                                                  pCod_Instrumento:=fCod_Instrumento, _
                                                  pId_Contraparte:=lId_Contraparte, _
                                                  pId_Representante:=lId_representante, _
                                                  pId_Moneda_Operacion:=Txt_Moneda.Tag, _
                                                  pFecha_Operacion:=lFecha_Operacion, _
                                                  pFecha_Vigencia:=lFecha_Vigencia, _
                                                  pFecha_Liquidacion:=lFecha_Liquidacion, _
                                                  pId_Trader:=lId_Trader, _
                                                  pPorc_Comision:=0, _
                                                  pComision:=Txt_Comision.Text, _
                                                  pDerechos_Bolsa:=Txt_Fees.Text, _
                                                  pGastos:=0, _
                                                  pIva:=0, _
                                                  pMonto_Operacion:=Txt_MontoTotal.Text, _
                                                  pTipo_Precio:=lTipo_Precio, _
                                                  pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                                  pInterest:=Txt_Interest.Text) Then
        Call Fnt_MsgError(lcBonos_Int.SubTipo_LOG, _
                          "Problemas en grabar Bonos Internacionales.", _
                          lcBonos_Int.ErrMsg, _
                          pConLog:=True)
        GoTo ErrProcedure
      End If
    Case Else
      MsgBox "Operacion no reconocida para operar.", vbCritical, Me.Caption
      GoTo ErrProcedure
  End Select

  lRollback = False
  Fnt_Grabar = True

ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
  Else
    gDB.CommitTransaccion
    
    Call Fnt_EnvioEMAIL_Trader(fId_Operacion)
    lErrOrden = ""
    Call Fnt_EnvioContraparte(fId_Operacion, lErrOrden)
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
End Function

Private Sub Sub_CargaForm()
Dim lCuentas As Object
'---------------------------------------------------------------
Dim lcRel_Contrapartes_Instrum As Class_Rel_Contrapartes_Instrum
Dim lTexto As String
Dim lReg As hFields
  
  Call Sub_Bloquea_Puntero(Me)
  
  Call Sub_FormControl_Color(Me.Controls)
  
  Call Sub_FormTama�o(eT_Normal, fTipo_Operacion)
  
  Call Sub_Carga_Datos_Cliente
  
  Call Sub_Setea_Comisiones
  
  Grilla.Rows = 1
  
  '------------------------------------------------
  '-- Tipo Precio
  '------------------------------------------------
  With Cmb_TipoPrecio
    Call .AddItem("Limite")
    Call .AddItem("Mercado")
    
    Call .Columns.Remove(1)
    
    With .Columns(0).ValueItems
      .Add Fnt_AgregaValueItem(cTipo_Precio_Limite, "Limite")
      .Add Fnt_AgregaValueItem(cTipo_Precio_Mercado, "Mercado")
      .Translate = True
    End With
    
    '.Text = ""
  End With
  Call Sub_ComboSelectedItem(Cmb_TipoPrecio, cTipo_Precio_Mercado)
  
  '------------------------------------------------
  '-- Formas de Pago
  '------------------------------------------------
  With Cmb_FechaLiquidacion
    Call .AddItem("0")
    Call .AddItem("1")
    Call .AddItem("2")
    Call .AddItem("3")
    Call .AddItem("4")
    Call .AddItem("5")
    
    With .Columns(0).ValueItems
      .Add Fnt_AgregaValueItem("0", "0")
      .Add Fnt_AgregaValueItem("1", "1")
      .Add Fnt_AgregaValueItem("2", "2")
      .Add Fnt_AgregaValueItem("3", "3")
      .Add Fnt_AgregaValueItem("4", "4")
      .Add Fnt_AgregaValueItem("5", "5")
      .Translate = True
    End With
    .SelectedItem = 0
    'Call Sub_ComboSelectedItem(Cmb_FechaLiquidacion, "0")
  End With
  
  '------------------------------------------------
  '-- Carga el ID del Cliente
  '------------------------------------------------
'  lCuentas.Campo("id_cuenta").Valor = fId_Cuenta
'  If lCuentas.Buscar_Vigentes Then
'    If lCuentas.Cursor.Count > 0 Then
'      fId_Cliente = lCuentas.Cursor(1)("id_cliente").Value
'    End If
'  End If
    Set lCuentas = Fnt_CreateObject(cDLL_Cuentas)
    With lCuentas
       
        .Campo("id_cuenta").Valor = fId_Cuenta
        If .Buscar_Vigentes Then
          If .Cursor.Count > 0 Then
            fId_Cliente = lCuentas.Cursor(1)("id_cliente").Value
          End If
        End If
    End With

  '------------------------------------------------
  
  '------------------------------------------------
  '-- Carga Contrapartes segun instrumento asociado
  '------------------------------------------------
  With Cmb_Contraparte
    .Text = ""
    .ClearFields
    .Clear
    .EmptyRows = True
    
    Call .Columns.Remove(1)
    
    With .Columns(0).ValueItems
      .Clear
      .Translate = False
    End With
    
    Set lcRel_Contrapartes_Instrum = New Class_Rel_Contrapartes_Instrum
    lcRel_Contrapartes_Instrum.Campo("cod_instrumento").Valor = fCod_Instrumento
    If lcRel_Contrapartes_Instrum.BuscarView Then
      For Each lReg In lcRel_Contrapartes_Instrum.Cursor
        lTexto = ""
        
        If Not gRelogDB Is Nothing Then
          gRelogDB.AvanzaRelog
        End If
          
        lTexto = lReg("DSC_CONTRAPARTE").Value
          
        .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("ID_CONTRAPARTE").Value, lTexto)
          
        Call .AddItem(lTexto)
          
      Next
    End If
    Set lcRel_Contrapartes_Instrum = Nothing
  End With
  
  '------------------------------------------------
  '-- Carga Representante
  '------------------------------------------------
  Call Sub_CargaCombo_Representantes(Cmb_Representantes, fId_Cliente)
    
  ' Si la operacion es custodia, muestra el Check de Aporte o Retiro
  If fTipo_Operacion = gcOPERACION_Custodia Then
    chkAporteRetiro.Value = 1
    chkAporteRetiro.Visible = True
  Else
    chkAporteRetiro.Value = 0
    chkAporteRetiro.Visible = False
  End If

  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Setea_Comisiones()
  '-------------------------------
  Txt_DiasVigencia.Text = 0
  '-------------------------------
  Txt_Comision.Text = 0
  Txt_Fees.Text = 0
  Txt_Interest.Text = 0
End Sub

Private Sub Sub_Carga_Datos_Cliente()
Dim lcCuenta As Object 'Class_Cuentas

'  Set lcCuenta = New Class_Cuentas
'  With lcCuenta
'    .Campo("id_cuenta").Valor = fId_Cuenta
'    If .Buscar_Vigentes Then
'      If .Cursor.Count > 0 Then
'        Txt_Rut.Text = "" & .Cursor(1)("rut_cliente").Value
'        Txt_Nombres.Text = "" & .Cursor(1)("nombre_cliente").Value
'        Txt_Num_Cuenta.Text = "" & .Cursor(1)("num_cuenta").Value
'        Txt_Perfil.Text = "" & .Cursor(1)("dsc_perfil_riesgo").Value
'      End If
'    Else
'      MsgBox "Problemas en cargar la Cuenta." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
'    End If
'  End With
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
       
        .Campo("id_cuenta").Valor = fId_Cuenta
        If .Buscar_Vigentes Then
          If .Cursor.Count > 0 Then
            Txt_Rut.Text = "" & .Cursor(1)("rut_cliente").Value
            Txt_Nombres.Text = "" & .Cursor(1)("nombre_cliente").Value
            Txt_Num_Cuenta.Text = "" & .Cursor(1)("num_cuenta").Value
            Txt_Perfil.Text = "" & .Cursor(1)("dsc_perfil_riesgo").Value
          End If
        Else
          MsgBox "Problemas en cargar la Cuenta." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
        End If
    End With
  
  Set lcCuenta = Nothing
  
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lNemotecnicos As Class_Nemotecnicos
Dim lSaldos_Activos As Class_Saldo_Activos
Dim lcTipo_Liq As Class_Tipos_Liquidacion

  Load Me
  Call Sub_Bloquea_Puntero(Me)
  
  '------------------------------------------------
  '-- Setea Fechas
  '------------------------------------------------
  fFecha_Operacion = Fnt_FechaServidor
  Txt_FechaIngreso_Real.Text = fFecha_Operacion
  DTP_Fecha_Operacion.Value = fFecha_Operacion
  
  If fTipo_Operacion = gcOPERACION_Custodia Then
    Dtp_FechaLiquidacion.Value = fFecha_Operacion
  Else
    
    Dtp_FechaLiquidacion.MinDate = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
    
    Set lcTipo_Liq = New Class_Tipos_Liquidacion
    With lcTipo_Liq
      .Campo("cod_instrumento").Valor = fCod_Instrumento
      .Campo("id_empresa").Valor = Fnt_EmpresaActual
      .Campo("tipo_movimiento").Valor = fOperacion
      If .Buscar Then
        If .Cursor.Count > 0 Then
          Dtp_FechaLiquidacion.Value = Fnt_Calcula_Dia_Habil(fFecha_Operacion, NVL(.Cursor(1).Fields("retencion").Value, 0))
          Cmb_FechaLiquidacion.Text = NVL(.Cursor(1).Fields("retencion").Value, 0)
        Else
          Dtp_FechaLiquidacion.Value = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
        End If
      Else
        Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en carga de Tipos de Liquidacion.", _
                          .ErrMsg, _
                          pConLog:=True)
        Dtp_FechaLiquidacion.Value = Fnt_Dia_Habil_MasProximo(fFecha_Operacion)
      End If
    End With
    Set lcTipo_Liq = Nothing
    
  End If
  '------------------------------------------------
  '-- Carga los nemotecnicos
  '------------------------------------------------
  Call Sub_LimpiarTDBCombo(Cmb_Nemotecnico)
  With Cmb_Nemotecnico
    With .Columns.Add(eNem_nemotecnico)
      .Caption = "Nemotecnico"
      .Visible = True
      
    End With
    With .Columns.Add(eNem_Descripcion)
      .Caption = "Descripcion"
      .Visible = True
    End With
    With .Columns.Add(eNem_Id_Nemotecnico)
      .Caption = "id_nemotecnico"
      .Visible = False
    End With
    
    If fOperacion = gcTipoOperacion_Ingreso Then
      'Si se estan realizando COMPRAS
      Set lNemotecnicos = New Class_Nemotecnicos
      If fTipo_Operacion = gcOPERACION_Directa Or fTipo_Operacion = gcOPERACION_Custodia Then
        lNemotecnicos.Campo("cod_instrumento").Valor = fCod_Instrumento 'gcINST_ACCIONES_NAC  cambiado CSM 23/04/2006
        If lNemotecnicos.Buscar() Then
          For Each lReg In lNemotecnicos.Cursor
            Call .AddItem(lReg("nemotecnico").Value & ";" & lReg("dsc_nemotecnico").Value & ";" & lReg("id_nemotecnico").Value)
            
            .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("id_nemotecnico").Value, lReg("nemotecnico").Value)
          Next
        Else
          Call Fnt_MsgError(lNemotecnicos.SubTipo_LOG, _
                              "Problemas en carga de Nemot�cnicos.", _
                              lNemotecnicos.ErrMsg, _
                              pConLog:=True)
        End If
      Else
        'lNemotecnicos.Campo("id_nemotecnico").Valor = fId_Cuenta '  cambiado CSM 30/11/2006
        lNemotecnicos.Campo("cod_instrumento").Valor = fCod_Instrumento 'gcINST_ACCIONES_NAC  cambiado CSM 23/04/2006
        If lNemotecnicos.BuscarPerfilRiesgo(fId_Cuenta) Then
          For Each lReg In lNemotecnicos.Cursor
            Call .AddItem(lReg("nemotecnico").Value & ";" & lReg("dsc_nemotecnico").Value & ";" & lReg("id_nemotecnico").Value)
            
            .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("id_nemotecnico").Value, lReg("nemotecnico").Value)
          Next
        Else
          Call Fnt_MsgError(lNemotecnicos.SubTipo_LOG, _
                              "Problemas en carga de Nemot�cnicos.", _
                              lNemotecnicos.ErrMsg, _
                              pConLog:=True)
        End If
      End If
      Set lNemotecnicos = Nothing
    Else
      Set lSaldos_Activos = New Class_Saldo_Activos
      If lSaldos_Activos.Buscar_Ultimo_CierreCuenta(pId_Cuenta:=fId_Cuenta, pCod_Instrumento:=fCod_Instrumento) Then
        For Each lReg In lSaldos_Activos.Cursor
          Call .AddItem(lReg("nemotecnico").Value & ";" & lReg("dsc_nemotecnico").Value & ";" & lReg("id_nemotecnico").Value)
          
          .Columns(0).ValueItems.Add Fnt_AgregaValueItem(lReg("id_nemotecnico").Value, lReg("nemotecnico").Value)
        Next
      Else
        Call Fnt_MsgError(lSaldos_Activos.SubTipo_LOG, _
                          "Problemas en carga de Nemot�cnicos.", _
                          lSaldos_Activos.ErrMsg, _
                          pConLog:=True)
      End If
      Set lSaldos_Activos = Nothing
    End If
  End With
  
  Call Sub_Desbloquea_Puntero(Me)

End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Dim lLinea As Long
  
  If Not Fnt_Form_Validar(Me.Controls, Frame_Principal) Then
    Fnt_ValidarDatos = False
    Exit Function
  ElseIf Not Fnt_Form_Validar(Me.Controls, Frame_Comisiones) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
    
  If Fnt_Verifica_Feriado(fFecha_Operacion) Then
    Fnt_ValidarDatos = False
    MsgBox "Solo se pueden ingresar operaciones en d�as h�biles.", vbExclamation, Me.Caption
    Exit Function
  End If
  
  If Txt_Comision.Text < 0 Then
    MsgBox "COMM no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  ElseIf Txt_Fees.Text < 0 Then
    MsgBox "Fees no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  ElseIf Txt_Interest.Text < 0 Then
    MsgBox "Interest no puede ser menor a cero.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  End If
    
  If Grilla.Rows <= 1 Then
    MsgBox "Para realizar una operaci�n m�nimo debe tener un detalle.", vbExclamation, Me.Caption
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
  If fOperacion = gcTipoOperacion_Ingreso Then
    If fTipo_Operacion = gcOPERACION_Directa Or fTipo_Operacion = gcOPERACION_Instruccion Then
      For lLinea = 1 To (Grilla.Rows - 1)
        If Not Fnt_CompruebaRestriccionCompra(fId_Cuenta, fEmisor, GetCell(Grilla, lLinea, "id_nemotecnico")) Then
          MsgBox "Existen Nemot�cnicos que presentan restricci�n de compra asociada a la Cuenta." & vbCr & vbCr & "No se puede operar.", vbOKOnly + vbCritical, Me.Caption
          Fnt_ValidarDatos = False
          Exit Function
        End If
      Next
    End If
  Else
    'Verifica las restricciones de Venta asociadas al producto
    If fTipo_Operacion = gcOPERACION_Directa Or fTipo_Operacion = gcOPERACION_Instruccion Then
      For lLinea = 1 To (Grilla.Rows - 1)
        If Not Fnt_CompruebaRestriccionVenta(fId_Cuenta, fEmisor, GetCell(Grilla, lLinea, "id_nemotecnico"), GetCell(Grilla, lLinea, "Cantidad")) Then
          MsgBox "Existen Nemot�cnicos que presentan restricci�n de Venta asociada a la Cuenta." & vbCr & vbCr & "No se puede operar.", vbOKOnly + vbCritical, Me.Caption
          Fnt_ValidarDatos = False
          Exit Function
        End If
      Next
    End If
  End If
  
  Fnt_ValidarDatos = True
End Function

Public Function Mostrar(ByRef pFormOri As Form, _
                        pId_Cuenta As String, _
                        pOperacion As String, _
                        pTipo_Operacion As String, _
                        pCod_Instrumento As String, _
                        pNombreTipoOperacion As String, _
                        pOper_Fecha_Anterior As Boolean, _
                        pCaja As String, _
                        pId_Caja As Long) As Boolean
  
  If pOper_Fecha_Anterior = False Then
    If Fnt_Verifica_Feriado(Fnt_FechaServidor) Then
      Mostrar = False
      MsgBox "Solo se pueden ingresar operaciones en d�as habiles.", vbExclamation, Me.Caption
      Unload Me
      Exit Function
    End If
  End If
  
  fId_Cuenta = pId_Cuenta
  fOperacion = pOperacion
  fTipo_Operacion = pTipo_Operacion
  fCod_Instrumento = pCod_Instrumento

  Txt_caja.Text = pCaja
  Txt_caja.Tag = pId_Caja
    
  If fTipo_Operacion = gcOPERACION_Custodia Then
    lbl_fecha_liquidacion.Visible = False
    Cmb_FechaLiquidacion.Visible = False
    Dtp_FechaLiquidacion.Visible = False
    
    lbl_tipo_precio.Visible = False
    Cmb_TipoPrecio.Visible = False
    
    Frame_Comisiones.Visible = False
    
    Txt_DiasVigencia.Top = lbl_tipo_precio.Top
    Grilla.Top = Grilla.Top - Frame_Comisiones.Height - lbl_fecha_liquidacion.Height
    Toolbar_Grilla.Top = Toolbar_Grilla.Top - Frame_Comisiones.Height - lbl_fecha_liquidacion.Height
    Frame_Principal.Height = Frame_Principal.Height - Frame_Comisiones.Height - lbl_fecha_liquidacion.Height
    Frame_Nemotecnico.Top = Frame_Nemotecnico.Top - Frame_Comisiones.Height - lbl_fecha_liquidacion.Height
  End If
    
  'Set fFormOri = pFormOri
  
  Select Case fOperacion
    Case gcTipoOperacion_Ingreso
      Me.Caption = "Compra de Bonos Internacionales - " & pNombreTipoOperacion
      Chk_Vende_Todo.Visible = False
      Chk_Vende_Todo.Value = False
    Case gcTipoOperacion_Egreso
      Me.Caption = "Venta de Bonos Internacionales - " & pNombreTipoOperacion
      Chk_Vende_Todo.Visible = True
      Chk_Vende_Todo.Value = False
  End Select
    
'  Call Sub_ColocaFormAbajo
  Call Sub_CargarDatos
  
  If pOper_Fecha_Anterior Then
    lbl_fecha_ingreso.Visible = True
    DTP_Fecha_Operacion.Visible = True
    Txt_FechaIngreso_Real.Visible = False
    DTP_Fecha_Operacion.MaxDate = fFecha_Operacion
  Else
    lbl_fecha_ingreso.Visible = False
    DTP_Fecha_Operacion.Visible = False
    Txt_FechaIngreso_Real.Visible = True
  End If
    
  fEstadoOK = False
  fSalir = False
    
  Me.Show
  
  Do While Not fSalir
    DoEvents
  Loop
  
  Mostrar = fEstadoOK
  
  Unload Me
End Function

Private Sub Sub_ColocaFormAbajo()
  If Not fFormOri Is Nothing Then
    Me.Top = fFormOri.Top + 2200
    Me.Left = fFormOri.Left + 200
  End If
End Sub

Private Sub Sub_FormTama�o(ByVal pTipo As eTipo, pTipo_Operacion As String)

  Cmb_Nemotecnico.Text = ""
  Txt_Emisor.Text = ""
  'Txt_Moneda.Text = ""
  Txt_Cantidad.Text = ""
  '----------
  Txt_Cantidad_Inversion.Text = ""
  Txt_MontoOperacion.Text = ""
  Txt_PrecioInversion.Text = ""
  Txt_PrecioCierre.Text = ""
  Chk_Vende_Todo.Value = False
  
  Select Case pTipo
    Case eT_Normal
      Frame_Nemotecnico.Visible = False
      If pTipo_Operacion = gcOPERACION_Custodia Then
        Me.Height = 6885 - Frame_Comisiones.Height - lbl_fecha_liquidacion.Height
      Else
        Me.Height = 6885
      End If
    Case eT_Grande
      Frame_Nemotecnico.Visible = True
      If pTipo_Operacion = gcOPERACION_Custodia Then
        If fOperacion = gcTipoOperacion_Ingreso Then
          Me.Height = 9210 - Frame_Comisiones.Height - lbl_fecha_liquidacion.Height
        Else
          Me.Height = 9210 - Frame_Comisiones.Height - lbl_fecha_liquidacion.Height
        End If
      Else
        Me.Height = 9210
      End If
  End Select
  
End Sub

Private Sub Sub_EliminarItem()
Dim lLinea As Long
  
  lLinea = Grilla.Row
  If lLinea > 0 Then
    If MsgBox("�Desea eliminar este Item?.", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
      Call Grilla.RemoveItem(lLinea)
      Call Sub_FormTama�o(eT_Normal, fTipo_Operacion)
    End If
  End If
End Sub

Private Sub Sub_AgregarItem()
Dim lLinea As Long
  
  lLinea = Grilla.Row
  Call Sub_FormTama�o(eT_Grande, fTipo_Operacion)
  Cmb_Nemotecnico.SetFocus
  fModif_Grilla = False
  
End Sub

Private Function Fnt_AceptarDetalle() As Boolean
Dim lLinea As Long
Dim lNemotecnico As String
Dim lId_Nemotecnico As String
Dim lOperacion As String
Dim lFlg_Vende_Todo As String
'------------------------------------------------------
Dim lcRestricc_Rel_Porc As Class_Restricciones_Rel_Porc
Dim lMonto_Operacion As Double
Dim lFila As Long

  Fnt_AceptarDetalle = True
  
  'Call Txt_PrecioInversion_LostFocus
  
  Rem Revisa si falta algun dato para ingresar
  Rem Cuando Cmb_Nemotecnico.Visible = False, la pantalla padre es confirmacion
  If Cmb_Nemotecnico.Visible Then
    If Not Fnt_Form_Validar(Me.Controls, Frame_Nemotecnico) Then
      Fnt_AceptarDetalle = False
      Exit Function
    End If
  End If
  
  If Not Fnt_Form_Validar(Me.Controls, Frame_Inversion) Then
    Fnt_AceptarDetalle = False
    Exit Function
  End If
  
  If Not Fnt_Form_Validar(Me.Controls, Frame_Precio) Then
    Fnt_AceptarDetalle = False
    Exit Function
  End If

  If To_Number(Txt_Cantidad_Inversion.Text) = 0 Then
    MsgBox "La cantidad no puede ser 0.", vbCritical, Me.Caption
    Txt_Cantidad_Inversion.SetFocus
    Fnt_AceptarDetalle = False
    Exit Function
  End If

  If fOperacion = gcTipoOperacion_Egreso Then
    If To_Number(Txt_Cantidad_Inversion.Text) > To_Number(Txt_Cantidad.Text) Then
      MsgBox "La Cantidad a Vender (" & Format(Txt_Cantidad_Inversion.Text, "#,##0.00") & ") no puede ser mayor a la Cantidad (" & Format(Txt_Cantidad.Text, "#,##0.00") & ").", vbCritical, Me.Caption
      Fnt_AceptarDetalle = False
      Exit Function
    End If
  End If
  
  Rem la pantalla padre es confirmacion
  If Not fForm_Confirmacion Then
    lNemotecnico = Cmb_Nemotecnico.Text
    lId_Nemotecnico = Cmb_Nemotecnico.Columns(eNem_Id_Nemotecnico).Text
  Else
    lId_Nemotecnico = fId_Nemotecnico
  End If
  
  Rem Validacion de Restriccion porcentual Perfiles solo para Instrucciones
  Rem 22/09/2009 MMardones. El control de restricci�n es s�lo para Ingresos
  If fTipo_Operacion = gcOPERACION_Instruccion And fOperacion = gcTipoOperacion_Ingreso Then

    
    Rem Suma los montos de los otros nemotecnicos de la grilla
    If Grilla.Rows > 1 Then
      For lFila = 1 To (Grilla.Rows - 1)
        'If Not lId_Nemotecnico = GetCell(Grilla, lFila, "id_nemotecnico") Then
          lMonto_Operacion = lMonto_Operacion + To_Number(GetCell(Grilla, lFila, "monto"))
        'End If
      Next
    End If
    Rem a la suma calculada anteriormente se suma el monto ingresado por el usuario
    lMonto_Operacion = lMonto_Operacion + To_Number(Txt_MontoOperacion.Text)
    
    'Txt_MontoTotal.Text = lMonto_Operacion
    
    Rem Valida la Restricci�n Porcentual del Perfil de Riesgo de Instrumentos y Productos
    Set lcRestricc_Rel_Porc = New Class_Restricciones_Rel_Porc
    With lcRestricc_Rel_Porc
      If Not .Fnt_Restriccion_Rel_Porc(fId_Cuenta, _
                                       lId_Nemotecnico, _
                                       lMonto_Operacion, _
                                       Txt_Moneda.Tag, _
                                       pFecha_Operacion:=DTP_Fecha_Operacion.Value) Then
        Fnt_AceptarDetalle = False
        Exit Function
      End If
    End With
    Set lcRestricc_Rel_Porc = Nothing
  End If
    
  With Grilla
    Rem PREGUNTA SI LA PANTALLA PADRE ES "CONFIRMACI�N DE INSTRUCCIONES" o se modifica una linea de la grilla
    If fForm_Confirmacion Or fModif_Grilla Then
      lLinea = .Row
    Else
      Rem Validaci�n: No se puede ingresar una instrucci�n para un mismo nemot�cnico
'      If Fnt_Busca_Nemotecnico(lId_Nemotecnico) Then
'        MsgBox "El Nemot�cnico " & Cmb_Nemotecnico.Text & " ya existe en la grilla.", vbCritical, Me.Caption
'        Exit Function
'      End If
      lLinea = .Rows
      .AddItem ""
    End If
  End With
  
  If Cmb_Nemotecnico.Visible Then
    lOperacion = IIf(fOperacion = gcTipoOperacion_Ingreso, "Ingreso", "Egreso")
    lFlg_Vende_Todo = IIf(Chk_Vende_Todo.Value, cFlg_Vende_Todo, cFlg_No_Vende_Todo)
    
    Call SetCell(Grilla, lLinea, "id_moneda", Txt_Moneda.Tag)
    Call SetCell(Grilla, lLinea, "id_nemotecnico", lId_Nemotecnico)
    Call SetCell(Grilla, lLinea, "cod_operacion", fOperacion)
    Call SetCell(Grilla, lLinea, "dsc_operacion", lOperacion)
    Call SetCell(Grilla, lLinea, "dsc_nemotecnico", lNemotecnico)
    Call SetCell(Grilla, lLinea, "Flg_Vende_Todo", lFlg_Vende_Todo)
  End If
  
  Call SetCell(Grilla, lLinea, "cantidad", Txt_Cantidad_Inversion.Text)
  Call SetCell(Grilla, lLinea, "precio", Txt_PrecioInversion.Text)
  Call SetCell(Grilla, lLinea, "monto", Txt_MontoOperacion.Text)
  Call SetCell(Grilla, lLinea, "dsc_moneda", Txt_Moneda.Text)
  
End Function

Private Sub Txt_Comision_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Comision_LostFocus()
  Call Sub_Calcula_MontoTotal
End Sub

 

Private Sub Txt_DiasVigencia_LostFocus()
 If Trim(Txt_DiasVigencia.Text) = "" Then
     Txt_DiasVigencia.Text = 0
  End If
End Sub

Private Sub Txt_Fees_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Fees_LostFocus()
  
  Call Sub_Calcula_MontoTotal
End Sub

Private Sub Txt_Interest_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_Interest_LostFocus()
  Call Sub_Calcula_MontoTotal
End Sub

Private Sub Txt_MontoOperacion_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_MontoOperacion_LostFocus()
'  Txt_PrecioInversion.Text = Fnt_Divide(Txt_MontoOperacion.Text, Txt_Cantidad_Inversion.Text)
End Sub

Private Sub Txt_PrecioInversion_KeyPress(KeyAscii As Integer)
  If KeyAscii = vbKeyReturn Then
    SendKeys "{TAB}"
  End If
End Sub

Private Sub Txt_PrecioInversion_LostFocus()
  Txt_MontoOperacion.Text = (Txt_PrecioInversion.Text) * Txt_Cantidad_Inversion.Text
End Sub

Rem FUNCION QUE ES LLAMADA DESDE LA PANATALLA DE CONFIRMACION DE INSTRUCCIONES
Public Function Confirmar(ByRef pFormOri As Form, _
                          ByVal pId_Operacion As String, _
                          ByVal pId_Cuenta As String, _
                          ByRef pMonto_Total As Double, _
                          ByVal pCod_Imstrumento As String) As Boolean
                          
  fTipo_Operacion = gcOPERACION_Instruccion
  fId_Cuenta = pId_Cuenta
  fId_Operacion = pId_Operacion
  fCod_Instrumento = pCod_Imstrumento
  'Set fFormOri = pFormOri
  
  Me.Caption = Me.Caption & " - Operaci�n N�: " & pId_Operacion
  
  '------------------------------------
  Toolbar.Buttons(1).Caption = "Confirmar"
  Toolbar.Buttons(1).ToolTipText = "Confirma la Operaci�n Internacional"
  Call Form_Resize
  
  lbl_fecha_ingreso.Visible = False
  DTP_Fecha_Operacion.Visible = False
  Txt_FechaIngreso_Real.Visible = True
  
  Toolbar_Grilla.Visible = False
  Txt_Cantidad.Visible = False
  Chk_Vende_Todo.Visible = False
  Txt_Nemotecnico.Visible = True
  lbl_nemotecnico.Visible = False
  Cmb_Nemotecnico.Visible = False
  
  Grilla.Width = 8565
  Grilla.ColHidden(7) = True
  '-------------------------------------
  
  Call Sub_ColocaFormAbajo
  Call Sub_FormTama�o(eT_Normal, fTipo_Operacion)
  Call Sub_CargarDatos_Confirmacion
  
  fForm_Confirmacion = True
  fEstadoOK = False
  fSalir = False
    
  Me.Show
  
  Do While Not fSalir
    DoEvents
  Loop
  
  Confirmar = fEstadoOK
  pMonto_Total = To_Number(Txt_MontoTotal.Text)
  
  Unload Me
End Function

Rem CARGA DATOS SEGUN EL NUMERO DE OPERACION PARA LA CONFIRMACION
Private Sub Sub_CargarDatos_Confirmacion()
Dim lReg As hCollection.hFields
Dim lLinea As Long
Dim lDetalle As Class_Operaciones_Detalle
Dim lId_Contraparte As String
Dim lId_representante As String
Dim lOperacion As String
Dim lNemotecnico As Class_Nemotecnicos
Dim lNombre_Nemotecnico As String
Dim lTipo_Precio As String
'----------------------------------------
Dim lcMoneda As Object 'Class_Monedas
Dim lDsc_Moneda As String
      
  Set fOperaciones = New Class_Operaciones
  fOperaciones.Campo("Id_Operacion").Valor = fId_Operacion
  
  If fOperaciones.BuscaConDetalles Then
    fOperacion = fOperaciones.Campo("Flg_Tipo_Movimiento").Valor
    fFecha_Operacion = fOperaciones.Campo("Fecha_Operacion").Valor
    
    Txt_FechaIngreso_Real.Text = fFecha_Operacion
  
    lId_Contraparte = NVL(fOperaciones.Campo("Id_Contraparte").Valor, "")
    If Not lId_Contraparte = "" Then
      Call Sub_ComboSelectedItem(Cmb_Contraparte, lId_Contraparte)
      Call Sub_CargaCombo_Traders(Cmb_Traders, lId_Contraparte)
      Call Sub_ComboSelectedItem(Cmb_Traders, "" & fOperaciones.Campo("Id_Trader").Valor)
    End If
    
    Txt_FechaVigencia.Text = NVL(fOperaciones.Campo("Fecha_Vigencia").Valor, "")
    
    Txt_DiasVigencia.Text = NVL(fOperaciones.Campo("Fecha_Vigencia").Valor, "") - NVL(fOperaciones.Campo("Fecha_Operacion").Valor, "")
    Txt_MontoTotal.Text = NVL(fOperaciones.Campo("Monto_Operacion").Valor, "")
    'Txt_MontoTotal.Tag = NVL(fOperaciones.Campo("Monto_Operacion").Valor, "")
    
    lId_representante = NVL(fOperaciones.Campo("Id_Representante").Valor, "")
    If Not lId_representante = "" Then
      Call Sub_ComboSelectedItem(Cmb_Representantes, lId_representante)
    End If
    
    Dtp_FechaLiquidacion.Value = fOperaciones.Campo("fecha_liquidacion").Valor
    Dtp_FechaLiquidacion.MinDate = fFecha_Operacion
    Cmb_FechaLiquidacion.Text = Fnt_DiasHabiles_EntreFechas(fFecha_Operacion, Dtp_FechaLiquidacion.Value)
        
    lTipo_Precio = NVL(fOperaciones.Campo("Flg_Limite_Precio").Valor, "")
    If Not lTipo_Precio = "" Then
      Call Sub_ComboSelectedItem(Cmb_TipoPrecio, lTipo_Precio)
    End If
    
    Rem Comisiones
    Txt_Comision.Text = NVL(fOperaciones.Campo("Comision").Valor, 0)
    Txt_Fees.Text = NVL(fOperaciones.Campo("Derechos").Valor, 0)
    Txt_Interest.Text = NVL(fOperaciones.Campo("Interest").Valor, 0)
    
    Set lNemotecnico = New Class_Nemotecnicos
    
    For Each lDetalle In fOperaciones.Detalles
      
      With lNemotecnico
        .Campo("id_nemotecnico").Valor = lDetalle.Campo("Id_Nemotecnico").Valor
        If .Buscar Then
          For Each lReg In .Cursor
            lNombre_Nemotecnico = lReg("NEMOTECNICO").Value
            Txt_Moneda.Tag = lReg("id_moneda_transaccion").Value
          Next
        End If
      End With
      
'      Set lcMoneda = New Class_Monedas
'      With lcMoneda
'        .Campo("id_moneda").Valor = lDetalle.Campo("Id_Moneda_Pago").Valor
'        If .Buscar Then
'          If .Cursor.Count > 0 Then
'            lDsc_Moneda = .Cursor(1).Fields("dsc_moneda").Value
'          End If
'        End If
'      End With
        Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
        With lcMoneda
          .Campo("Id_moneda").Valor = lDetalle.Campo("Id_Moneda_Pago").Valor
          If .Buscar Then
            If .Cursor.Count > 0 Then
                lDsc_Moneda = .Cursor(1).Fields("dsc_moneda").Value
            End If
          End If
        End With
        Set lcMoneda = Nothing
      
      lLinea = Grilla.Rows
      Call Grilla.AddItem("")
      Call SetCell(Grilla, lLinea, "id_moneda", NVL(lDetalle.Campo("Id_Moneda_Pago").Valor, ""))
      Call SetCell(Grilla, lLinea, "id_nemotecnico", NVL(lDetalle.Campo("Id_Nemotecnico").Valor, ""))
      lOperacion = IIf(fOperacion = gcTipoOperacion_Ingreso, "Ingreso", "Egreso")
      Call SetCell(Grilla, lLinea, "cod_operacion", NVL(fOperaciones.Campo("Flg_Tipo_Movimiento").Valor, ""))
      Call SetCell(Grilla, lLinea, "dsc_operacion", lOperacion)
      Call SetCell(Grilla, lLinea, "dsc_nemotecnico", lNombre_Nemotecnico)
      Call SetCell(Grilla, lLinea, "cantidad", NVL(lDetalle.Campo("Cantidad").Valor, ""))
      Call SetCell(Grilla, lLinea, "precio", NVL(lDetalle.Campo("Precio").Valor, ""))
      Call SetCell(Grilla, lLinea, "monto", NVL(lDetalle.Campo("Monto_Pago").Valor, ""))
      Call SetCell(Grilla, lLinea, "flg_vende_todo", NVL(lDetalle.Campo("flg_vende_todo").Valor, ""))
      Call SetCell(Grilla, lLinea, "dsc_moneda", lDsc_Moneda)
      
    Next
  Else
    Call Fnt_MsgError(fOperaciones.SubTipo_LOG, _
                      "Problemas en cargar la Operaci�n.", _
                      fOperaciones.ErrMsg, _
                      pConLog:=True)
  End If
  
End Sub

Private Function Fnt_Grabar_Confirmacion() As Boolean
Dim lId_Caja_Cuenta As Double
Dim lRollback As Boolean
Dim lDetalle As Class_Operaciones_Detalle
Dim lFila As Long
Dim lId_Contraparte As String
Dim lId_representante As String
Dim lId_Trader  As String
Dim lTipo_Precio As String
'------------------------------------
Dim lId_Nemotecnico As String
Dim lcRestricc_Rel_Porc As Class_Restricciones_Rel_Porc

  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False
  
  gDB.IniciarTransaccion
  
  lRollback = True
  Fnt_Grabar_Confirmacion = False

  If Not Fnt_ValidarDatos Then
    GoTo ErrProcedure
  End If

  lId_Caja_Cuenta = Txt_caja.Tag

  If fOperacion = gcTipoOperacion_Ingreso Then
    Rem Si es una compra
    Rem Validar si el Instrumento y Nemot�cnicos est�n relacionados al perfil de riesgo que le corresponde a la cuenta
    If Not Fnt_Valida_Perfil_Cuenta_Instrm_Nemo(Grilla, fId_Cuenta, fCod_Instrumento) Then
      GoTo ErrProcedure
    End If
  End If
  
  Rem Rescata el id_nemotecnico del primer elemento de la grilla solo para ejecutar el proceso de validacion de
  Rem restriccion porcentual de perfil de riesgo, el cual requiere cod_instrumento y cod_producto
  lId_Nemotecnico = GetCell(Grilla, 1, "id_nemotecnico")

  Rem Valida la Restricci�n Porcentual del Perfil de Riesgo de Instrumentos y Productos
  Set lcRestricc_Rel_Porc = New Class_Restricciones_Rel_Porc
  With lcRestricc_Rel_Porc
    If Not .Fnt_Restriccion_Rel_Porc(fId_Cuenta, _
                                     lId_Nemotecnico, _
                                     To_Number(Txt_MontoTotal.Text), _
                                     Txt_Moneda.Tag, _
                                     pFecha_Operacion:=DTP_Fecha_Operacion.Value) Then
      Exit Function
    End If
  End With
  Set lcRestricc_Rel_Porc = Nothing
  
  lId_Contraparte = Fnt_ComboSelected_KEY(Cmb_Contraparte)
  lId_representante = Fnt_ComboSelected_KEY(Cmb_Representantes)
  lTipo_Precio = Fnt_ComboSelected_KEY(Cmb_TipoPrecio)
  lId_Trader = Fnt_ComboSelected_KEY(Cmb_Traders)
'  lFecha_Operacion = Fnt_FechaServidor
'  lFecha_Vigencia = lFecha_Operacion '+ To_Number(Txt_DiasVigencia.Text)
'  lFecha_Liquidacion = lFecha_Operacion + Cmb_FormaPago.SelectedItem.Tag
  
  
  With fOperaciones
'    .Campo("Id_Operacion").valor = fId_Operacion
'    .Campo("Id_Cuenta").valor = fId_Cuenta
'    .Campo("Cod_Tipo_Operacion").valor = gcOPERACION_Directa
'    .Campo("Cod_Estado").valor = cCod_Estado_Pendiente
    .Campo("Id_Contraparte").Valor = lId_Contraparte
    .Campo("Id_Representante").Valor = lId_representante
'    .Campo("Id_Tipo_Liquidacion").Valor = lId_Tipo_Liquidacion
'    .Campo("Id_Moneda_Operacion").valor = Txt_Moneda.Tag
'    .Campo("Cod_Producto").valor = gcPROD_FFMM_NAC 'Esto va en duro
'    .Campo("Cod_Instrumento").valor = fCod_Instrumento
'    .Campo("Flg_Tipo_Movimiento").valor = fOperacion
'    .Campo("Fecha_Operacion").valor = lFecha_Operacion
'    .Campo("Fecha_Vigencia").Valor = Txt_FechaVigencia.Text
    .Campo("Fecha_Liquidacion").Valor = Dtp_FechaLiquidacion.Value
'    .Campo("Dsc_Operacion").valor = pDsc_Operacion
    .Campo("Id_Trader").Valor = lId_Trader
    .Campo("Porc_Comision").Valor = 0
    .Campo("Comision").Valor = Txt_Comision.Text
    .Campo("Derechos").Valor = Txt_Fees.Text
    .Campo("Gastos").Valor = 0
    .Campo("Iva").Valor = 0
    .Campo("Monto_Operacion").Valor = To_Number(Txt_MontoTotal.Text)
    .Campo("Flg_Limite_Precio").Valor = lTipo_Precio
    .Campo("Interest").Valor = Txt_Interest.Text
    
    lFila = 1
    For Each lDetalle In fOperaciones.Detalles
      lDetalle.Campo("Cantidad").Valor = GetCell(Grilla, lFila, "cantidad")
      lDetalle.Campo("Precio").Valor = GetCell(Grilla, lFila, "precio")
      lDetalle.Campo("Monto_Pago").Valor = GetCell(Grilla, lFila, "monto")
      lDetalle.Campo("Monto_Bruto").Valor = GetCell(Grilla, lFila, "monto")
      lDetalle.Campo("flg_vende_todo").Valor = GetCell(Grilla, lFila, "flg_vende_todo")
      lFila = lFila + 1
    Next
        
    If Not .Guardar(pConfirmacion:=True) Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Error en la confirmaci�n de la operaci�n.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
        
    If Not .Confirmar(lId_Caja_Cuenta) Then
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Error en la confirmaci�n de la operaci�n.", _
                        .ErrMsg, _
                        pConLog:=True)
      GoTo ErrProcedure
    End If
  End With
  
  lRollback = False
  Fnt_Grabar_Confirmacion = True

ErrProcedure:
  If lRollback Then
    gDB.RollbackTransaccion
    'MsgBox "Error en la confirmaci�n de la operaci�n.", vbCritical, Me.Caption
  Else
    gDB.CommitTransaccion
  End If
  
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
End Function

Rem FUNCION QUE ES LLAMADA DESDE LA PANTALLA DE CONSULTA DE OPERACIONES
Public Sub Consulta_Operacion(ByRef pFormOri As Form, _
                                   ByVal pId_Operacion As String, _
                                   ByVal pId_Cuenta As String, _
                                   ByVal pCod_Instrumento As String, _
                                   ByVal pTipo_Operacion As String, _
                                   ByVal pOperacion As String)
  
  fConsulta_Operacion = True
  
  fTipo_Operacion = pTipo_Operacion
  fOperacion = pOperacion
  fId_Cuenta = pId_Cuenta
  fId_Operacion = pId_Operacion
  fCod_Instrumento = pCod_Instrumento
  
  Txt_Num_Operacion.Text = pId_Operacion
  Me.Caption = Me.Caption & " - Operaci�n N�: " & pId_Operacion
  
  lbl_fecha_ingreso.Visible = False
  DTP_Fecha_Operacion.Visible = False
  Txt_FechaIngreso_Real.Visible = True
  
  If fTipo_Operacion = gcOPERACION_Custodia Then
    lbl_fecha_liquidacion.Visible = False
    Cmb_FechaLiquidacion.Visible = False
    Dtp_FechaLiquidacion.Visible = False
    
    lbl_tipo_precio.Visible = False
    Cmb_TipoPrecio.Visible = False
    
    Frame_Comisiones.Visible = False
    
    Txt_DiasVigencia.Top = lbl_tipo_precio.Top
    
    Grilla.Top = Grilla.Top - Frame_Comisiones.Height - lbl_fecha_liquidacion.Height
    Frame_Principal.Height = Frame_Principal.Height - Frame_Comisiones.Height - lbl_fecha_liquidacion.Height
    
  End If
  
  '------------------------------------
  Toolbar.Buttons(1).Visible = False
  Call Form_Resize
  '------------------------------------
  
  Cmb_Traders.Enabled = False
  Cmb_Traders.BackColor = fColorNoEdit
  
  Cmb_Contraparte.Enabled = False
  Cmb_Contraparte.BackColor = fColorNoEdit
  
  Cmb_Representantes.Enabled = False
  Cmb_Representantes.BackColor = fColorNoEdit
  
  Cmb_FechaLiquidacion.Enabled = False
  Cmb_FechaLiquidacion.BackColor = fColorNoEdit
  
  Dtp_FechaLiquidacion.Enabled = False
  
  Cmb_TipoPrecio.Enabled = False
  Cmb_TipoPrecio.BackColor = fColorNoEdit
  
  Txt_DiasVigencia.Locked = True
  Txt_DiasVigencia.BackColorTxt = fColorNoEdit
  '------------------------------------
  
  Txt_Comision.Locked = True
  Txt_Comision.BackColorTxt = fColorNoEdit
  
  Txt_Fees.Locked = True
  Txt_Fees.BackColorTxt = fColorNoEdit
  
  Txt_Interest.Locked = True
  Txt_Interest.BackColorTxt = fColorNoEdit

  '-------------------------------------
  
  Toolbar_Grilla.Visible = False
  Grilla.Width = 8565
  '------------------------------------
  
  Call Sub_ColocaFormAbajo
  Call Sub_FormTama�o(eT_Normal, fTipo_Operacion)
  Call Sub_CargarDatos_Consulta
  
  fSalir = False
    
  Me.Show
  
  Do While Not fSalir
    DoEvents
  Loop
  
  Unload Me
End Sub

Private Sub Sub_CargarDatos_Consulta()
Dim lcOperaciones As Class_Operaciones
Dim lcOperaciones_Detalle As Class_Operaciones_Detalle
Dim lReg As hFields
Dim lcMov_Activos As Class_Mov_Activos
Dim lCursor_Mov_Activos As hRecord
Dim lReg_Mov_Activos As hFields
Dim lcNemotecnico As Class_Nemotecnicos
Dim lcMoneda As Object 'Class_Monedas
Dim lcCargos_Abonos As Class_Cargos_Abonos
'---------------------------------------
Dim lNombre_Nemotecnico As String
Dim lDsc_Moneda As String
Dim lLinea As Long
Dim lOperacion As String
Dim lId_Contraparte As String
    
  Rem Carga los datos generales de la operacion
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("id_operacion").Valor = fId_Operacion
    If .Buscar Then
      For Each lReg In .Cursor
        If lReg("cod_estado").Value = cCod_Estado_Pendiente Then
          Call Sub_CargarDatos_Confirmacion
          Exit Sub
        Else
          fOperacion = lReg("Flg_Tipo_Movimiento").Value
          fFecha_Operacion = lReg("Fecha_Operacion").Value
          
          Txt_FechaIngreso_Real.Text = fFecha_Operacion
        
          lId_Contraparte = NVL(lReg("Id_Contraparte").Value, "")
          If Not lId_Contraparte = "" Then
            Call Sub_ComboSelectedItem(Cmb_Contraparte, "" & lReg("Id_Contraparte").Value)
            Call Sub_CargaCombo_Traders(Cmb_Traders, lId_Contraparte)
            Call Sub_ComboSelectedItem(Cmb_Traders, "" & lReg("Id_Trader").Value)
          End If
          
          Call Sub_ComboSelectedItem(Cmb_Representantes, "" & lReg("Id_Representante").Value)
          
          Txt_FechaVigencia.Text = NVL(lReg("Fecha_Vigencia").Value, "")
          
          Txt_DiasVigencia.Text = NVL(lReg("Fecha_Vigencia").Value, "") - NVL(lReg("Fecha_Operacion").Value, "")
          Txt_MontoTotal.Text = NVL(lReg("Monto_Operacion").Value, "")
          
          Dtp_FechaLiquidacion.Value = lReg("fecha_liquidacion").Value
          Dtp_FechaLiquidacion.MinDate = fFecha_Operacion
          Cmb_FechaLiquidacion.Text = Fnt_DiasHabiles_EntreFechas(fFecha_Operacion, Dtp_FechaLiquidacion.Value)
              
          Call Sub_ComboSelectedItem(Cmb_TipoPrecio, "" & lReg("Flg_Limite_Precio").Value)
          
          Rem Comisiones
          Txt_Comision.Text = NVL(lReg("Comision").Value, 0)
          Txt_Fees.Text = NVL(lReg("Derechos").Value, 0)
          Txt_Interest.Text = NVL(lReg("Interest").Value, 0)
          
          Set lcCargos_Abonos = New Class_Cargos_Abonos
          lcCargos_Abonos.Campo("Id_Cargo_Abono").Valor = NVL(lReg("ID_CARGO_ABONO").Value, 0)
          If lcCargos_Abonos.Buscar Then
             If lcCargos_Abonos.Cursor.Count > 0 Then
                Txt_caja.Text = lcCargos_Abonos.Cursor(1)("Dsc_Caja_Cuenta").Value
             End If
          End If
          
        End If
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar la Operaci�n.", _
                        fOperaciones.ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcOperaciones = Nothing
  
  Set lcOperaciones_Detalle = New Class_Operaciones_Detalle
  With lcOperaciones_Detalle
    .Campo("id_operacion").Valor = fId_Operacion
    If .Buscar Then
      Set lCursor_Mov_Activos = .Cursor
    Else
      Call Fnt_MsgError(.SubTipo_LOG, _
                        "Problemas en cargar la Operaci�n Detalle.", _
                        fOperaciones.ErrMsg, _
                        pConLog:=True)
    End If
  End With
  Set lcOperaciones_Detalle = Nothing
  
  Set lcMov_Activos = New Class_Mov_Activos
  With lcMov_Activos
    For Each lReg In lCursor_Mov_Activos
      .Campo("id_operacion_detalle").Valor = lReg("id_operacion_detalle").Value
      If .Buscar Then
        For Each lReg_Mov_Activos In .Cursor
          Set lcNemotecnico = New Class_Nemotecnicos
          lcNemotecnico.Campo("id_nemotecnico").Valor = lReg_Mov_Activos("Id_Nemotecnico").Value
          If lcNemotecnico.Buscar Then
            If lcNemotecnico.Cursor.Count > 0 Then
              lNombre_Nemotecnico = lcNemotecnico.Cursor(1)("NEMOTECNICO").Value
            End If
          End If
          Set lcNemotecnico = Nothing
        
'          Set lcMoneda = New Class_Monedas
'          lcMoneda.Campo("id_moneda").Valor = lReg_Mov_Activos("Id_Moneda").Value
'          If lcMoneda.Buscar Then
'            If lcMoneda.Cursor.Count > 0 Then
'              lDsc_Moneda = lcMoneda.Cursor(1).Fields("dsc_moneda").Value
'            End If
'          End If
'          Set lcMoneda = Nothing
            Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
            With lcMoneda
              .Campo("Id_moneda").Valor = lReg_Mov_Activos("Id_Moneda").Value
              If .Buscar Then
                If .Cursor.Count > 0 Then
                    lDsc_Moneda = .Cursor(1).Fields("dsc_moneda").Value
                End If
              End If
            End With
            Set lcMoneda = Nothing
        
          lLinea = Grilla.Rows
          Call Grilla.AddItem("")
          'Call SetCell(Grilla, lLinea, "id_moneda", NVL(lDetalle.Campo("Id_Moneda_Pago").Valor, ""))
          'Call SetCell(Grilla, lLinea, "id_nemotecnico", NVL(lDetalle.Campo("Id_Nemotecnico").Valor, ""))
          lOperacion = IIf(fOperacion = gcTipoOperacion_Ingreso, "Ingreso", "Egreso")
          'Call SetCell(Grilla, lLinea, "cod_operacion", NVL(fOperaciones.Campo("Flg_Tipo_Movimiento").Valor, ""))
          Call SetCell(Grilla, lLinea, "dsc_operacion", lOperacion)
          Call SetCell(Grilla, lLinea, "dsc_nemotecnico", lNombre_Nemotecnico)
          Call SetCell(Grilla, lLinea, "cantidad", NVL(lReg_Mov_Activos("Cantidad").Value, ""))
          Call SetCell(Grilla, lLinea, "precio", NVL(lReg_Mov_Activos("Precio").Value, ""))
          Call SetCell(Grilla, lLinea, "monto", NVL(lReg_Mov_Activos("Monto").Value, ""))
          'Call SetCell(Grilla, lLinea, "flg_vende_todo", NVL(lDetalle.Campo("flg_vende_todo").Valor, ""))
          Call SetCell(Grilla, lLinea, "dsc_moneda", lDsc_Moneda)
          
        Next
      End If
    Next
  End With
  Set lcMov_Activos = Nothing
  
End Sub
