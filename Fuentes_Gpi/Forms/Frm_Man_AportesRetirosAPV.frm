VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Man_AportesRetirosAPV 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Aportes / Retiros / Traspasos APV"
   ClientHeight    =   7815
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   12825
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7815
   ScaleWidth      =   12825
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   7395
      Left            =   30
      TabIndex        =   0
      Top             =   360
      Width           =   12735
      Begin VB.Frame Frame3 
         Caption         =   "Datos Cuenta"
         ForeColor       =   &H000000FF&
         Height          =   1215
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   7695
         Begin VB.CommandButton cmb_buscar_ctas 
            Caption         =   "?"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   7200
            Picture         =   "Frm_Man_AportesRetirosAPV.frx":0000
            TabIndex        =   15
            Top             =   705
            Width           =   375
         End
         Begin VB.CommandButton cmb_buscar 
            Caption         =   "?"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   7200
            Picture         =   "Frm_Man_AportesRetirosAPV.frx":030A
            TabIndex        =   11
            Top             =   240
            Width           =   375
         End
         Begin TrueDBList80.TDBCombo Cmb_Cliente 
            Height          =   345
            Left            =   1620
            TabIndex        =   12
            Tag             =   "OBLI=S;CAPTION=Cliente;SOLOLECTURA=N"
            Top             =   240
            Width           =   5535
            _ExtentX        =   9763
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Man_AportesRetirosAPV.frx":0614
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin hControl2.hTextLabel Txt_Num_Cuenta 
            Height          =   345
            Left            =   120
            TabIndex        =   16
            Top             =   705
            Width           =   7020
            _ExtentX        =   12383
            _ExtentY        =   609
            LabelWidth      =   1485
            TextMinWidth    =   1200
            Caption         =   "N� de Cuenta"
            Text            =   ""
            MaxLength       =   100
         End
         Begin VB.Label Label2 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Cliente"
            Height          =   345
            Left            =   120
            TabIndex        =   13
            Top             =   240
            Width           =   1485
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   " Filtro "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1215
         Left            =   7920
         TabIndex        =   1
         Top             =   240
         Width           =   4695
         Begin VB.CheckBox Chk_Solicitudes 
            Caption         =   "S�lo Solicitudes"
            Height          =   375
            Left            =   3480
            TabIndex        =   14
            Top             =   720
            Width           =   1095
         End
         Begin MSComCtl2.DTPicker DTP_Fecha_Ter 
            Height          =   345
            Left            =   1560
            TabIndex        =   2
            Top             =   720
            Width           =   1665
            _ExtentX        =   2937
            _ExtentY        =   609
            _Version        =   393216
            Format          =   17367041
            CurrentDate     =   38938
         End
         Begin MSComCtl2.DTPicker DTP_Fecha_Ini 
            Height          =   345
            Left            =   1560
            TabIndex        =   3
            Top             =   240
            Width           =   1665
            _ExtentX        =   2937
            _ExtentY        =   609
            _Version        =   393216
            Format          =   17367041
            CurrentDate     =   38938
         End
         Begin MSComctlLib.Toolbar Toolbar_Proceso 
            Height          =   330
            Left            =   3360
            TabIndex        =   4
            Top             =   240
            Width           =   1140
            _ExtentX        =   2011
            _ExtentY        =   582
            ButtonWidth     =   1561
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Buscar"
                  Key             =   "BUSC"
                  Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
               EndProperty
            EndProperty
         End
         Begin VB.Label Lbl_Fecha_Ini 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha Inicio"
            Height          =   345
            Index           =   1
            Left            =   120
            TabIndex        =   6
            Top             =   270
            Width           =   1395
         End
         Begin VB.Label Lbl_Fecha_Ter 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Fecha T�rmino"
            Height          =   345
            Index           =   0
            Left            =   120
            TabIndex        =   5
            Top             =   720
            Width           =   1395
         End
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   5685
         Left            =   120
         TabIndex        =   7
         Top             =   1560
         Width           =   12495
         _cx             =   22040
         _cy             =   10028
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   16
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Man_AportesRetirosAPV.frx":06BE
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   12825
      _ExtentX        =   22622
      _ExtentY        =   741
      ButtonWidth     =   2619
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   8
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Ingreso"
            Key             =   "INGRESO"
            Description     =   "Ingresa Aportes o Traspasos de Entrada"
            Object.ToolTipText     =   "Ingresa Aportes o Traspasos de Entrada"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ADD_APO"
                  Object.Tag             =   "Ingresa un Aporte"
                  Text            =   "Aporte"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "TRASP_ENT"
                  Object.Tag             =   "Ingresa un Traspaso de Entrada"
                  Text            =   "Trapaso Entrada"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Solicitud R/TS"
            Key             =   "SOLIC"
            Description     =   "Ingresa Solicitud de Retiros o Traspasos Salida"
            Object.ToolTipText     =   "Ingresa Solicitud de Retiro o Traspaso de Salida"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   2
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "ADD_RES"
                  Object.Tag             =   "Ingresa un Retiro"
                  Text            =   "Retiro"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "TRASP_SAL"
                  Object.Tag             =   "Ingresa un Traspaso de Salia"
                  Text            =   "Traspaso Salida"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Anular"
            Key             =   "DEL"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Anula un Aporte/Rescate"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Modificar"
            Key             =   "UPDATE"
            Description     =   "Modifica un elemento"
            Object.ToolTipText     =   "Modifica un Aporte/Rescate"
            Object.Width           =   1000
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Confirmar"
            Key             =   "CONFIRMAR"
            Description     =   "Confirma Solicitudes de Retiro y Traspasos de Salida"
            Object.ToolTipText     =   "Confirma Solicitudes de Retiro y Traspasos de Salida"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Imprimir"
            Key             =   "PRINTER"
            Object.ToolTipText     =   "Imprimir"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   3
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "XLS"
                  Text            =   "a EXCEL"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   14640
         TabIndex        =   9
         Top             =   0
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Man_AportesRetirosAPV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Rem Flag para Fechas anteriores
Dim fFlg_Fechas_Anteriores As Boolean

Dim fFlg_Tipo_Permiso As String
Dim fCod_Arbol_Sistema As String

Dim fIdTipoAhorro As String
Dim fIdCliente    As String
Dim fIdCuenta    As String
Dim sDscTipoAhorro As String
Dim bEsConfirmacion As Boolean
Dim fHabilitadoRetiro As String
Public lId_Cuenta As Integer


Public Sub Mostrar(pCod_Arbol_Sistema, Optional pFlg_Fechas_Anteriores As Boolean = False)
  
  fFlg_Fechas_Anteriores = pFlg_Fechas_Anteriores
  
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
  
  Load Me
  
End Sub

Private Sub cmb_buscar_Click()
Dim lId_Cliente
Dim lId_Cuenta As String ''num_cuenta
Dim lPnt_Cmb_Cts As Integer ''recupera en indice

Dim lId_TipoCuenta As Integer
    lId_TipoCuenta = Fnt_BuscaTipoCuenta
    lId_Cliente = NVL(Frm_Busca_Cuentas.BuscarCliente(pId_TipoCuenta:=lId_TipoCuenta, pId_Cuenta:=lId_Cuenta), 0)
    
    If lId_Cliente <> 0 Then
        Call Sub_ComboSelectedItem(Cmb_Cliente, lId_Cliente)
        'Call Sub_CargaCombo_Cuentas_APV(Cmb_Cuentas, pTag:="tipo_ahorro", pId_Cliente:=fIdCliente)
        'Cmb_Cuentas.SelectedItem = 0
        'fIdCuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
        fIdCuenta = ""
        Txt_Num_Cuenta.Text = ""
        Txt_Num_Cuenta.Tag = ""
        
        ''AGREGADO POR MMF. 15-07-2010
        ''EN LA GRILLA DEL COMBO ELEGIR EL LID_CUENTA
        fIdCliente = Fnt_ComboSelected_KEY(Cmb_Cliente)
        If Not fIdCliente = "" Then
            'Call Sub_CargaCombo_Cuentas_APV(Cmb_Cuentas, pTag:="tipo_ahorro", pId_Cliente:=fIdCliente, pId_Cuenta:=lId_Cuenta, pPnt_Cmb_Cts:=lPnt_Cmb_Cts)
            'Cmb_Cuentas.SelectedItem = lPnt_Cmb_Cts - 1
            'fIdCuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
            lId_Cuenta = ""
            fIdCuenta = ""
            Txt_Num_Cuenta.Text = ""
            Txt_Num_Cuenta.Tag = ""
        End If
        '' FIN
    End If
   
End Sub

Private Sub Cmb_Cliente_ItemChange()
    fIdCliente = Fnt_ComboSelected_KEY(Cmb_Cliente)
    If Not fIdCliente = "" Then
        'Call Sub_CargaCombo_Cuentas_APV(Cmb_Cuentas, pTag:="tipo_ahorro", pId_Cliente:=fIdCliente)
        'Cmb_Cuentas.SelectedItem = 0
        'fIdCuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
        Txt_Num_Cuenta.Text = ""
        Txt_Num_Cuenta.Tag = ""
        fIdCuenta = ""
    End If
End Sub




Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("INGRESO").Image = cBoton_Agregar_Grilla
      .Buttons("SOLIC").Image = cBoton_Agregar_Grilla
      .Buttons("CONFIRMAR").Image = cBoton_Aceptar
      .Buttons("DEL").Image = cBoton_Eliminar_Grilla
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("UPDATE").Image = cBoton_Modificar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
      '.Buttons("XLS").Image = cBoton_Excel
  End With
  With Toolbar_Proceso
        Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("BUSC").Image = "boton_grilla_buscar"
        .Appearance = ccFlat
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.SetFocus
    DoEvents

    bEsConfirmacion = False
    Select Case Button.Key
        Case "CONFIRMAR"
            bEsConfirmacion = True
            Call Grilla_DblClick
          'Call Sub_EsperaVentanaConfirmacion(cNewEntidad, gcTipoOperacion_Traspaso_Salida)
        Case "EXIT"
            Unload Me
        Case "DEL"
          ' Call Sub_Eliminar
            Call Sub_Anular
        Case "REFRESH"
            Cmb_Cliente.Text = ""
            Txt_Num_Cuenta.Text = ""
            Txt_Num_Cuenta.Tag = ""
            Grilla.Rows = 1
        Case "UPDATE"
            Call Grilla_DblClick
        Case "PRINTER"
            Call Sub_Imprimir(ePrinter.eP_Impresora)
    End Select
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
  Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "ADD_APO"
      Call Sub_EsperaVentana(cNewEntidad, gcTipoOperacion_Aporte)
    Case "ADD_RES"
        If fHabilitadoRetiro = "S" Then
            Call Sub_EsperaVentana(cNewEntidad, gcTipoOperacion_Rescate)
        Else
            MsgBox "Tipo de Ahorro " & sDscTipoAhorro & " no est� habilitado para hacer retiro.", vbInformation, Me.Caption
        End If
    Case "TRASP_ENT"
      Call Sub_EsperaVentana(cNewEntidad, gcTipoOperacion_Traspaso_Entrada)
    Case "TRASP_SAL"
        Call Sub_EsperaVentana(cNewEntidad, gcTipoOperacion_Traspaso_Salida)
    Case "SCREEN"
      Call Sub_Imprimir(eP_Pantalla)
    Case "PDF"
      Call Sub_Imprimir(eP_PDF)
    Case "XLS"
      Call Sub_Excel
  End Select

End Sub

Private Sub Toolbar_Proceso_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim lCuenta As String
    'lCuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
    lCuenta = Txt_Num_Cuenta.Tag
    If Not lCuenta = "" Then
    Else
       MsgBox "Primero debe elegir una cuenta.", vbInformation + vbOKOnly, Me.Caption
       Exit Sub
    End If
    Me.SetFocus
    DoEvents
    Select Case Button.Key
        Case "BUSC"
            Call Sub_BuscarDatosGrilla(True)
    End Select
End Sub

Private Sub Sub_CargaForm()
    Dim lReg  As hCollection.hFields
    Dim lLinea As Long
  
    Call Sub_FormControl_Color(Me.Controls)

    Call Sub_Bloquea_Puntero(Me)
      
    Call Sub_CargaCombo_Clientes_APV(Cmb_Cliente)
    
    fIdCliente = Fnt_ComboSelected_KEY(Cmb_Cliente) ''
  
  
    Grilla.Rows = 1
    DTP_Fecha_Ini.Value = PrimerDiaMes(Fnt_FechaServidor)
    DTP_Fecha_Ter.Value = UltimoDiaDelMesEnCurso(DTP_Fecha_Ini.Value)
    
    fIdTipoAhorro = 0
    sDscTipoAhorro = ""
    fHabilitadoRetiro = ""
    bEsConfirmacion = False
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_CargarDatos()
Dim lReg    As hFields
Dim lLinea  As Long
Dim lID     As String

'-----------------------
'Dim lAporte_Rescate_Cuenta As Class_APORTE_RESCATE_CUENTA
   
    Call Sub_Bloquea_Puntero(Me)
      
    If Grilla.Row > 0 Then
      lID = GetCell(Grilla, Grilla.Row, "colum_pk")
    Else
      lID = ""
    End If
     
    Grilla.Rows = 1
    fIdTipoAhorro = 0
    sDscTipoAhorro = ""
    bEsConfirmacion = False
    fIdCuenta = Txt_Num_Cuenta.Tag  'Fnt_ComboSelected_KEY(Cmb_Cuentas)
    If Not fIdCuenta = "" Then
        fIdTipoAhorro = Fnt_TipoAhorroCuenta
        Call Sub_BuscarDatosGrilla
    End If
    
    Call Sub_Desbloquea_Puntero(Me)
  
End Sub



Private Sub Grilla_DblClick()
Dim lKey As String

   With Grilla
      If .Row > 0 Then
            If RTrim(GetCell(Grilla, .Row, "cod_estado")) = "A" Then
                MsgBox "Movimiento Anulado, no puede ser modificado.", vbInformation, Me.Caption
            Else
                If bEsConfirmacion Then
                    If RTrim(GetCell(Grilla, .Row, "cod_estado")) <> "S" Then
                        MsgBox "Movimiento no es una Solicitud de Retiro / Traspaso de Salida.", vbInformation, Me.Caption
                        bEsConfirmacion = False
                        Exit Sub
                    Else
                        lKey = GetCell(Grilla, .Row, "colum_pk")
                        fIdTipoAhorro = GetCell(Grilla, .Row, "id_tipo_ahorro")
                        sDscTipoAhorro = GetCell(Grilla, .Row, "descripcion_larga")
                        Call Sub_EsperaVentanaConfirmacion(lKey, GetCell(Grilla, .Row, "flg_tipo_movimiento"))
                    End If
                Else
                    lKey = GetCell(Grilla, .Row, "colum_pk")
                    fIdTipoAhorro = GetCell(Grilla, .Row, "id_tipo_ahorro")
                    sDscTipoAhorro = GetCell(Grilla, .Row, "descripcion_larga")
                    Call Sub_EsperaVentana(lKey, GetCell(Grilla, .Row, "flg_tipo_movimiento"))
                End If
            End If
        End If
      
   End With
End Sub

Private Sub Sub_EsperaVentana(pkey, pTipo_Movimiento As String)
Dim lForm_AporteRetiro_APV As Frm_AporteRetiro_APV
Dim lNombre As String
Dim lCuenta As String
Dim sDscTipoMovimiento As String
    
    Select Case pTipo_Movimiento
        Case gcTipoOperacion_Aporte
            sDscTipoMovimiento = "APORTE"
        Case gcTipoOperacion_Rescate
            sDscTipoMovimiento = "RESCATE"
        Case gcTipoOperacion_Traspaso_Entrada
            sDscTipoMovimiento = "TRASPASO ENTRADA"
        Case gcTipoOperacion_Traspaso_Salida
            sDscTipoMovimiento = "TRASPASO SALIDA"
    End Select
    Me.Enabled = False
    'lCuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
    lCuenta = Txt_Num_Cuenta.Tag
    If Not lCuenta = "" Then
    'If fFlg_Fechas_Anteriores Then
        If Not Fnt_ExisteVentanaKey("Frm_AporteRetiro_APV", pkey) Then
           lNombre = Me.Name
           Set lForm_AporteRetiro_APV = New Frm_AporteRetiro_APV
           Call lForm_AporteRetiro_APV.Fnt_Modificar(pkey:=pkey _
                                                    , pTipo_Movimiento:=pTipo_Movimiento _
                                                    , pId_Cuenta:=lCuenta _
                                                    , pCod_Arbol_Sistema:=fCod_Arbol_Sistema _
                                                    , pFlg_Fechas_Anteriores:=fFlg_Fechas_Anteriores _
                                                    , pId_TipoAhorro:=fIdTipoAhorro _
                                                    , pDsc_TipoAhorro:=sDscTipoAhorro _
                                                    , pDscTipoMovimiento:=sDscTipoMovimiento _
                                                    , pEsConfirmacion:=False)
        
           Do While Fnt_ExisteVentanaKey("Frm_AporteRetiro_APV", pkey)
              DoEvents
           Loop
           If Fnt_ExisteVentana(lNombre) Then
              Call Sub_CargarDatos
            Else
              Exit Sub
           End If
        End If
    Else
       MsgBox "Primero elija una cuenta.", vbInformation + vbOKOnly, Me.Caption
    End If
    Me.Enabled = True
End Sub
Private Sub Sub_EsperaVentanaConfirmacion(pkey, pTipo_Movimiento As String)
Dim lForm_AporteRetiro_APV As Frm_AporteRetiro_APV
Dim lNombre As String
Dim lCuenta As String
Dim sDscTipoMovimiento As String
    
    Select Case pTipo_Movimiento
        Case gcTipoOperacion_Rescate
            sDscTipoMovimiento = "CONFIRMACION RESCATE"
        Case gcTipoOperacion_Traspaso_Salida
            sDscTipoMovimiento = "CONFIRMACION TRASPASO SALIDA"
    End Select
    Me.Enabled = False
    'lCuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
    lCuenta = Txt_Num_Cuenta.Tag
    If Not lCuenta = "" Then
        If Not Fnt_ExisteVentanaKey("Frm_AporteRetiro_APV", pkey) Then
           lNombre = Me.Name
           Set lForm_AporteRetiro_APV = New Frm_AporteRetiro_APV
           Call lForm_AporteRetiro_APV.Fnt_Modificar(pkey:=pkey _
                                                    , pTipo_Movimiento:=pTipo_Movimiento _
                                                    , pId_Cuenta:=lCuenta _
                                                    , pCod_Arbol_Sistema:=fCod_Arbol_Sistema _
                                                    , pFlg_Fechas_Anteriores:=fFlg_Fechas_Anteriores _
                                                    , pId_TipoAhorro:=fIdTipoAhorro _
                                                    , pDsc_TipoAhorro:=sDscTipoAhorro _
                                                    , pDscTipoMovimiento:=sDscTipoMovimiento _
                                                    , pEsConfirmacion:=True)
        
           Do While Fnt_ExisteVentanaKey("Frm_AporteRetiro_APV", pkey)
              DoEvents
           Loop
           If Fnt_ExisteVentana(lNombre) Then
              Call Sub_CargarDatos
            Else
              Exit Sub
           End If
        End If
    Else
       MsgBox "Primero elija una cuenta.", vbInformation + vbOKOnly, Me.Caption
    End If
    Me.Enabled = True
End Sub

Private Sub Sub_Eliminar()
    Dim lid_aporte_rescate_cuenta    As String
    Dim lAporte_Rescate_Cuenta       As Class_APORTE_RESCATE_CUENTA
    If Grilla.Row > 0 Then
        If MsgBox("�Seguro que desea eliminar?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
            lid_aporte_rescate_cuenta = GetCell(Grilla, Grilla.Row, "colum_pk")
            If Not lid_aporte_rescate_cuenta = "" Then
                gDB.IniciarTransaccion
                Set lAporte_Rescate_Cuenta = New Class_APORTE_RESCATE_CUENTA
                With lAporte_Rescate_Cuenta
                    .Campo("Id_Apo_Res_Cuenta").Valor = lid_aporte_rescate_cuenta
                    If .Eliminar Then
                        gDB.CommitTransaccion
                        MsgBox GetCell(Grilla, Grilla.Row, "Dsc_Apo_Res_Cuenta") & " Eliminado correctamente.", vbInformation + vbOKOnly, Me.Caption
                    Else
                        gDB.RollbackTransaccion
                        Call Fnt_MsgError(.SubTipo_LOG, _
                                            "Problemas al eliminar el Aporte Rescate.", _
                                            .ErrMsg, _
                                            pConLog:=True)
                    End If
                End With
                Set lAporte_Rescate_Cuenta = Nothing
            End If
            Call Sub_CargarDatos
        End If
    End If
End Sub
Private Sub Sub_Anular()
    Dim lid_aporte_rescate_cuenta    As String
    Dim lId_Mov_Caja                 As String
    Dim lId_Mov_Caja_retencion                 As String
    Dim lAporte_Rescate_Cuenta       As Class_APORTE_RESCATE_CUENTA
    Dim oMov_caja                   As Class_Mov_Caja
    If Grilla.Row > 0 Then
        If MsgBox("�Seguro que desea Anular ?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
            lid_aporte_rescate_cuenta = GetCell(Grilla, Grilla.Row, "colum_pk")
            lId_Mov_Caja = GetCell(Grilla, Grilla.Row, "id_mov_caja")
            lId_Mov_Caja_retencion = GetCell(Grilla, Grilla.Row, "id_mov_caja_retencion")
            If (Not lid_aporte_rescate_cuenta = "") And (Not lId_Mov_Caja = "") Then
                Set oMov_caja = New Class_Mov_Caja
                With oMov_caja
                    gDB.IniciarTransaccion
                    If .Anular(lId_Mov_Caja, pId_Mov_Caja_Retencion:=lId_Mov_Caja_retencion) Then
                        gDB.CommitTransaccion
                        MsgBox GetCell(Grilla, Grilla.Row, "Dsc_Apo_Res_Cuenta") & " Anulado correctamente.", vbInformation + vbOKOnly, Me.Caption
                    Else
                        gDB.RollbackTransaccion
                        Call Fnt_MsgError(.SubTipo_LOG, _
                                            "Problemas al Anular el Aporte Rescate.", _
                                            .ErrMsg, _
                                            pConLog:=True)
                                            
                    End If
                End With
                
                Set oMov_caja = Nothing
                
            End If
            
            Call Sub_CargarDatos
        End If
    End If
    
End Sub
Private Sub Sub_Imprimir(pTipoSalida As ePrinter)
Dim lForm As Frm_Reporte_Generico
  Dim lCuenta As String

  Set lForm = New Frm_Reporte_Generico
  'lCuenta = Cmb_Cuentas.Text
  lCuenta = Txt_Num_Cuenta.Text
  Rem Comienzo de la generaci�n del reporte
  With lForm
    Call .Sub_InicarDocumento(pTitulo:="Aportes/Retiros de APV" _
                            , pTipoSalida:=pTipoSalida _
                            , pOrientacion:=orLandscape)
     
    With .VsPrinter
      .FontSize = 10
      .Paragraph = "Cuenta: " & lCuenta
      .Paragraph = "Cliente: " & Cmb_Cliente.Text
      .Paragraph = "Periodo Consulta:  desde el " & DTP_Fecha_Ini.Value & " al " & DTP_Fecha_Ter.Value
      .Paragraph = "" 'salto de linea
      .FontBold = False
      .FontSize = 9
      
      Call Sub_Grilla2VsPrinter(lForm.VsPrinter, Me.Grilla, pNoEndTable:=True)
      
      .EndTable
      .EndDoc
    End With
  End With
  
ExitProcedure:
  Set lForm = Nothing
  
End Sub

Private Sub Sub_BuscarDatosGrilla(Optional p_buscar As Boolean = False)
Dim oAporteRescate As New Class_APORTE_RESCATE_CUENTA
'----------------------------------------------------------------------------
Dim lReg        As hFields
'----------------------------------------------------------------------------
Dim lId_Cuenta  As String
Dim ldFechaIni  As String
Dim ldFechaFin  As String
'----------------------------------------------------------------------------
Dim lLinea      As Long
Dim lFormato    As String
Dim lSCodEstado As String
'----------------------------------------------------------------------------
Dim lhTotales As hRecord
Dim lfTotal As hFields
Dim sCodEstado As String
Dim sTipoMov   As String

    On Error GoTo ErrProcedure

    Call Sub_Bloquea_Puntero(Me)

    If Not Fnt_Form_Validar(Me.Controls) Then
        Exit Sub
    End If
    fIdCuenta = Txt_Num_Cuenta.Tag  'Fnt_ComboSelected_KEY(Cmb_Cuentas)
    lId_Cuenta = Txt_Num_Cuenta.Tag 'Fnt_ComboSelected_KEY(Cmb_Cuentas)
    ldFechaIni = DTP_Fecha_Ini.Value
    ldFechaFin = DTP_Fecha_Ter.Value
    sCodEstado = IIf(Chk_Solicitudes.Value, "S", "")

    Grilla.Rows = 1
    With oAporteRescate
        If Not .Buscar_AporteRescatePorFecha(lId_Cuenta, ldFechaIni, ldFechaFin, p_buscar, pCodEstado:=sCodEstado) Then
            GoTo ExitProcedure
        End If
        For Each lReg In .Cursor
            lLinea = Grilla.Rows
            Grilla.AddItem ""
            Call SetCell(Grilla, lLinea, "colum_pk", lReg("Id_Apo_Res_Cuenta").Value, True)
            Call SetCell(Grilla, lLinea, "fecha", lReg("fecha_movimiento").Value, True)
            Call SetCell(Grilla, lLinea, "monto", Format(lReg("monto").Value, Fnt_Formato_Moneda(lReg("id_moneda").Value)), True)
            Call SetCell(Grilla, lLinea, "descripcion", lReg("dsc_apo_res_cuenta").Value, True)
            Call SetCell(Grilla, lLinea, "banco", NVL(lReg("dsc_banco").Value, ""), True)
            Call SetCell(Grilla, lLinea, "dsc_estado", NVL(lReg("dsc_estado").Value, ""), True)
            Call SetCell(Grilla, lLinea, "id_moneda", NVL(lReg("id_moneda").Value, ""), True)     '=== Agregado por MMA 25/07/08
            Call SetCell(Grilla, lLinea, "dsc_moneda", NVL(lReg("dsc_moneda").Value, ""), True)
            Call SetCell(Grilla, lLinea, "dsc_caja_cuenta", NVL(lReg("dsc_caja_cuenta").Value, ""), True)
            If lReg("Flg_Tipo_Movimiento").Value = gcTipoOperacion_Aporte Or _
               lReg("Flg_Tipo_Movimiento").Value = gcTipoOperacion_Traspaso_Entrada Then
                sTipoMov = "Aporte"
            Else
                sTipoMov = "Retiro"
            End If
            
            Call SetCell(Grilla, lLinea, "Dsc_Apo_Res_Cuenta", sTipoMov, True)
            Call SetCell(Grilla, lLinea, "Flg_Tipo_Movimiento", lReg("Flg_Tipo_Movimiento").Value, True)
            Call SetCell(Grilla, lLinea, "id_mov_caja", lReg("id_mov_caja").Value, True)
            Call SetCell(Grilla, lLinea, "id_mov_caja_retencion", NVL(lReg("id_mov_caja_retencion").Value, 0), True)
            Call SetCell(Grilla, lLinea, "id_tipo_ahorro", NVL(lReg("id_tipo_ahorro").Value, ""), True)
            Call SetCell(Grilla, lLinea, "descripcion_larga", NVL(lReg("descripcion_larga").Value, ""), True)
            Call SetCell(Grilla, lLinea, "cod_estado", lReg("cod_estado").Value, True)
        Next
    End With
    
   ' Grilla.SetFocus

ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al traer los Aportes y Retiros.", Err.Description, pConLog:=False)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
    Set oAporteRescate = Nothing
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Excel()
Dim lcExcel As Excel.Application
Dim lcLibro As Excel.Workbook
Dim lcHoja  As Excel.Worksheet
Dim lNro_Libro As Long
Dim lLinea As Long
Dim lFila As Long
Dim lColumna As Long
Dim dMonto As Double

Const cLineaTitulo = 8
Const cColumna = 2

  Call Sub_Bloquea_Puntero(Me)
  
  If Not Grilla.Rows > 1 Then
    MsgBox "No hay datos en la grilla para exportar a Excel.", vbInformation, Me.Caption
    GoTo ExitProcedure
  End If
  
  Set lcExcel = New Excel.Application
  Set lcLibro = lcExcel.Workbooks.Add
  
  lcExcel.ActiveWindow.DisplayGridlines = False
  
  With lcLibro
    For lNro_Libro = .Worksheets.Count To 2 Step -1
      .Worksheets(lNro_Libro).Delete
    Next lNro_Libro
  
    .Sheets(1).Select
    .ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).ShapeRange.IncrementLeft 39.75
    .Worksheets.Item(1).Range("B6").Value = "Gesti�n de Portafolio de Inversi�n"
    .Worksheets.Item(1).Range("B6").Font.Bold = True
    .Worksheets.Item(1).Columns("A:A").ColumnWidth = 5
    .Worksheets.Item(1).Columns("B:B").ColumnWidth = 20
    
    .Worksheets.Item(1).Name = "AportesRescates"
  End With
  
  Set lcHoja = lcLibro.Sheets(1)
  
  BarraProceso.max = Grilla.Rows - 1
  
  With lcHoja
    .Cells(cLineaTitulo, cColumna).Value = "Cuenta "
    .Cells(cLineaTitulo, cColumna + 1).Value = Txt_Num_Cuenta.Text
    .Cells(cLineaTitulo + 1, cColumna).Value = "Cliente "
    .Cells(cLineaTitulo + 1, cColumna + 1).Value = Cmb_Cliente.Text
    
    .Cells(cLineaTitulo + 4, cColumna + 6).Value = "Fecha Generaci�n Reporte : " & Fnt_FechaServidor
    If Chk_Solicitudes.Value = 0 Then
        .Cells(cLineaTitulo + 4, cColumna).Value = "Aportes/Retiros/Traspasos desde el " & DTP_Fecha_Ini.Value & " al " & DTP_Fecha_Ter.Value
    Else
        .Cells(cLineaTitulo + 4, cColumna).Value = "Solicitudes de Retiros/Traspasos de Salida desde el " & DTP_Fecha_Ini.Value & " al " & DTP_Fecha_Ter.Value
    End If
    .Cells(cLineaTitulo + 4, cColumna).Font.Bold = True
    
    If Grilla.Rows = 1 Then
      .Cells(15, cColumna).Value = "Sin Datos"
      .Cells(15, cColumna).Font.Bold = True
    End If
    
    lLinea = cLineaTitulo + 6
    lColumna = cColumna
    
    .Cells(lLinea, lColumna).Value = "Fecha"
    .Cells(lLinea, lColumna + 1).Value = "Tipo Movimiento"
    .Cells(lLinea, lColumna + 2).Value = "Tipo Ahorro"
    .Cells(lLinea, lColumna + 3).Value = "Monto"
    .Cells(lLinea, lColumna + 4).Value = "Estado"
    .Cells(lLinea, lColumna + 5).Value = "Caja"
    .Cells(lLinea, lColumna + 6).Value = "Descripci�n"
    .Cells(lLinea, lColumna + 7).Value = "Moneda"
    Call Sub_Colores(lcExcel, lcLibro, "B" & CStr(lLinea), 36)
    lLinea = lLinea + 1
    For lFila = 1 To Grilla.Rows - 1
      BarraProceso.Value = lFila
      Call Sub_Interactivo(gRelogDB)
      
      .Cells(lLinea, lColumna).Value = Trim(GetCell(Grilla, lFila, "fecha"))
      .Cells(lLinea, lColumna + 1).Value = Trim(GetCell(Grilla, lFila, "Dsc_Apo_Res_Cuenta"))
      .Cells(lLinea, lColumna + 2).Value = Trim(GetCell(Grilla, lFila, "descripcion_larga"))
      dMonto = CDbl(GetCell(Grilla, lFila, "monto"))
      .Cells(lLinea, lColumna + 3).Value = dMonto
      .Cells(lLinea, lColumna + 3).NumberFormat = Fnt_Formato_Moneda(GetCell(Grilla, lFila, "id_moneda"))
      .Cells(lLinea, lColumna + 4).Value = GetCell(Grilla, lFila, "dsc_estado")
      .Cells(lLinea, lColumna + 5).Value = GetCell(Grilla, lFila, "dsc_caja_cuenta")
      .Cells(lLinea, lColumna + 6).Value = GetCell(Grilla, lFila, "descripcion")
      .Cells(lLinea, lColumna + 7).Value = GetCell(Grilla, lFila, "dsc_moneda")
      lLinea = lLinea + 1
    Next
  End With
  
  lcLibro.ActiveSheet.Columns("C:C").Select
  lcExcel.Range(lcExcel.Selection, lcExcel.Selection.End(xlToRight)).Select
  lcExcel.Selection.EntireColumn.AutoFit
  lcExcel.Range("B10:B" + CStr(lLinea)).Select
  lcExcel.Range("B10:B" + CStr(lLinea)).HorizontalAlignment = xlLeft

  lcLibro.ActiveSheet.Range("A1").Select
  
  lcExcel.Visible = True
  lcExcel.UserControl = True
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Colores(pExcel As Excel.Application, _
                        pLibro As Excel.Workbook, _
                        pRango As String, _
                        pColor As Long)
  
  With pLibro
    .Sheets(1).Select
    .Sheets(1).Select
    .Sheets(1).Activate
    
    .ActiveSheet.Range(pRango).Select
    .ActiveSheet.Range(pRango).Activate
  End With
  
  With pExcel
    .Range(.Selection, .Selection.End(xlToRight)).Select
    pLibro.ActiveSheet.Range(.Selection, .Selection.End(xlToRight)).Font.Bold = True
    
    .Selection.Borders(xlEdgeLeft).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeLeft).Weight = xlMedium
    
    .Selection.Borders(xlEdgeTop).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeTop).Weight = xlMedium
    
    .Selection.Borders(xlEdgeRight).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeRight).Weight = xlMedium
        
    .Selection.Borders(xlEdgeBottom).LineStyle = xlContinuous
    .Selection.Borders(xlEdgeBottom).Weight = xlMedium
    
    .Selection.Borders(xlInsideVertical).LineStyle = xlContinuous
    .Selection.Borders(xlInsideVertical).Weight = xlThin
    
    .Selection.Interior.ColorIndex = pColor
    .Selection.Interior.Pattern = xlSolid
  End With
  
End Sub

Private Function Fnt_TipoAhorroCuenta() As Integer
Dim lcCuenta As Object
Dim lcTipoAhorro As Class_TiposAhorroAPV
Dim lid_tipoahorro As String
Dim lId_Cuenta As String


    lid_tipoahorro = 0
    'lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
    lId_Cuenta = Txt_Num_Cuenta.Tag
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    lcCuenta.Campo("id_cuenta").Valor = lId_Cuenta
    If lcCuenta.Buscar Then
        lid_tipoahorro = lcCuenta.Cursor(1)("tipo_ahorro")
        Set lcTipoAhorro = New Class_TiposAhorroAPV
        lcTipoAhorro.Campo("id_tipo_ahorro").Valor = lid_tipoahorro
        If lcTipoAhorro.Buscar Then
            sDscTipoAhorro = lcTipoAhorro.Cursor(1)("descripcion_larga")
            fHabilitadoRetiro = lcTipoAhorro.Cursor(1)("habilitado_retiro")
        End If
        Set lcTipoAhorro = Nothing
    End If
    Set lcCuenta = Nothing
    Fnt_TipoAhorroCuenta = lid_tipoahorro
    
End Function


Private Function Fnt_BuscaTipoCuenta()
Dim lcTipoCuenta As Class_TipoCuentas
Dim lIdTipoCuenta As Integer

    lIdTipoCuenta = 0
    Set lcTipoCuenta = New Class_TipoCuentas
    With lcTipoCuenta
        .Campo("descripcion_corta").Valor = cTIPOCUENTA_APV
        If .Buscar Then
            lIdTipoCuenta = .Cursor(1)("id_tipocuenta")
        End If
    End With
    Set lcTipoCuenta = Nothing
    Fnt_BuscaTipoCuenta = lIdTipoCuenta
End Function

Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call Lpr_Buscar_Cuenta("TXT")
    End If
End Sub

Private Sub cmb_buscar_ctas_Click()
Dim lId_Cuenta As Integer
Dim lRutCliente As String
Dim lId_TipoCuenta As Integer
    
    
    If Fnt_ComboSelected_KEY(Cmb_Cliente) = "" Or Fnt_ComboSelected_KEY(Cmb_Cliente) = "0" Then
        MsgBox "Debe seleccionar alg�n Cliente.", vbInformation, Me.Caption
        Exit Sub
    End If
    
    lRutCliente = ""
    Dim lcClientes As Object
    Set lcClientes = Fnt_CreateObject(cDLL_Clientes)
    With lcClientes
        .Campo("Id_Cliente").Valor = NVL(Fnt_ComboSelected_KEY(Cmb_Cliente), 0)
        If .Buscar(True) Then
          If .Cursor.Count > 0 Then
              lRutCliente = NVL(.Cursor(1)("rut_cliente").Value, "")
          End If
        End If
    End With
    
    lId_TipoCuenta = Fnt_BuscaTipoCuenta
    
    lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(, , lRutCliente, , lId_TipoCuenta), 0)
    If lId_Cuenta <> 0 Then
        Txt_Num_Cuenta.Text = ""
        Txt_Num_Cuenta.Tag = lId_Cuenta
        Call Lpr_Buscar_Cuenta("", CStr(lId_Cuenta))
    End If
    
End Sub

Private Sub Lpr_Buscar_Cuenta(ByVal Origen As String, Optional p_Id_Cuenta As String)
Dim lcCuenta      As Object
Dim lId_TipoCuenta As Integer
Dim lRutCliente As String
    
    If Fnt_ComboSelected_KEY(Cmb_Cliente) = "" Or Fnt_ComboSelected_KEY(Cmb_Cliente) = "0" Then
        MsgBox "Debe seleccionar alg�n Cliente.", vbInformation, Me.Caption
        Exit Sub
    End If
    
    lRutCliente = ""
    Dim lcClientes As Object
    Set lcClientes = Fnt_CreateObject(cDLL_Clientes)
    With lcClientes
        .Campo("Id_Cliente").Valor = NVL(Fnt_ComboSelected_KEY(Cmb_Cliente), 0)
        If .Buscar(True) Then
          If .Cursor.Count > 0 Then
              lRutCliente = NVL(.Cursor(1)("rut_cliente").Value, "")
          End If
        End If
    End With
        
    lId_TipoCuenta = Fnt_BuscaTipoCuenta

    If Origen = "TXT" Then
        If InStr(1, Txt_Num_Cuenta.Text, "-") > 0 Then
           Txt_Num_Cuenta.Text = Trim(Left(Txt_Num_Cuenta.Text, InStr(1, Txt_Num_Cuenta.Text, "-") - 1))
        End If
        p_Id_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(Txt_Num_Cuenta.Text, , lRutCliente, , lId_TipoCuenta), 0)
    End If
    If p_Id_Cuenta <> "" Then
        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
        With lcCuenta
            .Campo("Id_Cuenta").Valor = p_Id_Cuenta
            If .Buscar Then
                If .Cursor.Count > 0 Then
                    Txt_Num_Cuenta.Tag = .Cursor(1)("id_cuenta").Value
                    Txt_Num_Cuenta.Text = .Cursor(1)("num_cuenta").Value & " - " & .Cursor(1)("Abr_cuenta").Value
                    
                    fIdCuenta = .Cursor(1)("id_cuenta").Value
                    fIdTipoAhorro = Fnt_TipoAhorroCuenta
                    Call Sub_CargarDatos
                End If
            Else
                Call Fnt_MsgError(.SubTipo_LOG, _
                          "Problemas en cargar datos de la Cuenta.", _
                          .ErrMsg, _
                          pConLog:=True)
            End If
        End With
        Else
            MsgBox "No existe informacion", vbInformation
            Set lcCuenta = Nothing
    End If
End Sub


