VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Precio_AFP 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Carga de Precios de AFP"
   ClientHeight    =   6315
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7635
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6315
   ScaleWidth      =   7635
   Begin VB.Frame Frame2 
      Height          =   2175
      Left            =   60
      TabIndex        =   4
      Top             =   420
      Width           =   7515
      Begin VB.CommandButton Cmb_BuscaFile 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   7020
         TabIndex        =   10
         ToolTipText     =   "Busca el Archivo Plano de Precios de SAFP"
         Top             =   990
         Width           =   375
      End
      Begin VB.CheckBox Chk_Cartera 
         Caption         =   "En Cartera"
         Height          =   285
         Left            =   5880
         TabIndex        =   6
         Top             =   1800
         Value           =   1  'Checked
         Width           =   1095
      End
      Begin hControl2.hTextLabel Txt_ArchivoPlano 
         Height          =   315
         Left            =   120
         TabIndex        =   5
         Tag             =   "OBLI"
         Top             =   990
         Width           =   6810
         _ExtentX        =   12012
         _ExtentY        =   556
         LabelWidth      =   1500
         TextMinWidth    =   1200
         Caption         =   "Archivo Plano"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin hControl2.hTextLabel Txt_Publicador 
         Height          =   315
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   6795
         _ExtentX        =   11986
         _ExtentY        =   556
         LabelWidth      =   1500
         Caption         =   "Publicador"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel Txt_Fecha 
         Height          =   315
         Left            =   120
         TabIndex        =   8
         Top             =   630
         Width           =   3015
         _ExtentX        =   5318
         _ExtentY        =   556
         LabelWidth      =   1500
         Caption         =   "Fecha"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin TrueDBList80.TDBCombo Cmb_Productos 
         Height          =   345
         Left            =   1620
         TabIndex        =   11
         Tag             =   "OBLI=S;CAPTION=Producto"
         Top             =   1350
         Width           =   5355
         _ExtentX        =   9446
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Precio_AFP.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Label7 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Producto"
         Height          =   345
         Left            =   120
         TabIndex        =   9
         Top             =   1350
         Width           =   1485
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Listado de Errores"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3585
      Left            =   60
      TabIndex        =   2
      Top             =   2640
      Width           =   7515
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3195
         Left            =   150
         TabIndex        =   3
         Top             =   240
         Width           =   7245
         _cx             =   12779
         _cy             =   5636
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   1
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Precio_AFP.frx":00AA
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7635
      _ExtentX        =   13467
      _ExtentY        =   635
      ButtonWidth     =   1826
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Caption         =   "Buscar"
            Key             =   "RESEARCH"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Busca el Archivo Plano de Precios de SAFP"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Cargar"
            Key             =   "LOAD"
            Object.ToolTipText     =   "Carga los precios de SAFP de Archivo Plano"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Exportar"
            Key             =   "EXPORT"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Exporta los errores de Carga en Archivo Plano"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Precio_AFP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String

Const cfId_Origen = 1 ' el origen es "1" para la Superintendencia de AFP
Const cfId_Tipo_Conversion_Instrumento = 1 ' el id_tipo_conversion es "1" para conversion a Instrumentos
Const cfId_Tipo_Conversion_Moneda = 3 ' el id_tipo_conversion es "3" para conversion a moneda

Private Enum eColumnas
  eC_Nemotecnico = 1
  eC_TipoInstrumento
  eC_UnidadReajuste
  eC_Precio
  eC_Duracion
  eC_TirValorizacion
  eC_TirTransaccion
  eC_Categoria
End Enum

Private Sub Cmb_BuscaFile_Click()
  Call Sub_Busca_Archivo_Plano
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("RESEARCH").Image = "boton_grilla_buscar"
    .Buttons("LOAD").Image = cBoton_Grabar
    .Buttons("EXIT").Image = cBoton_Salir
    .Buttons("EXPORT").Image = "document"
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(ByVal pId_Publicador As String, _
                              ByVal pDsc_Publicador As String, _
                              ByVal pFecha As String)
  fKey = pId_Publicador
      
  Me.Top = 1
  Me.Left = 1
  Me.Show
  Txt_Publicador.Text = pDsc_Publicador
  Txt_Fecha.Text = pFecha

End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "RESEARCH"
      Call Sub_Busca_Archivo_Plano
    Case "LOAD"
      Call Sub_Cargar_Archivo_Plano
    Case "EXIT"
      Unload Me
    Case "EXPORT"
      Call Sub_Exportar_Archivo_Plano
  End Select
End Sub

Private Sub Sub_CargaForm()
Dim lReg As hFields

  Call Sub_FormControl_Color(Me.Controls)

  Txt_ArchivoPlano.Text = ""
  Txt_Publicador.Text = ""
  Txt_Fecha.Text = ""
  
  Grilla.Rows = 1
  
  Call Sub_CargaCombo_Productos(Cmb_Productos, pTodos:=True)

  Call Sub_ComboSelectedItem(Cmb_Productos, cCmbKALL)

End Sub

Private Sub Sub_Busca_Archivo_Plano()
Dim i As Integer
Dim lNombreArchivo As String
Dim lPos_Punto As Integer
Dim lYear As String
Dim lMes As String
Dim lDia As String
Dim lFecha_File As String

On Error GoTo Cmd_BuscarArchivo_Err
    
  With MDI_Principal.CommonDialog
    .CancelError = True
    .InitDir = "C:\Mis documentos\"
    .Filter = "Texto (*.txt)|*.txt"
    .Flags = cdlOFNFileMustExist
  
    .ShowOpen
  End With
  
  For i = Len(MDI_Principal.CommonDialog.FileName) To 1 Step -1
    If Mid(MDI_Principal.CommonDialog.FileName, i, 1) = "." Then
      lPos_Punto = i - 2
    End If
    If Mid(MDI_Principal.CommonDialog.FileName, i, 1) = "\" Then
      Rem Extrae el nombre del archivo sin su extension
      lNombreArchivo = Mid$(MDI_Principal.CommonDialog.FileName, i + 2, lPos_Punto - i)
      i = 0
    End If
  Next i
  
  Rem Se obtiene la fecha del archivo
  lYear = Mid(lNombreArchivo, 1, 4)
  lMes = Mid(lNombreArchivo, 5, 2)
  lDia = Mid(lNombreArchivo, 7, 2)
  lFecha_File = Format(lDia & "/" & lMes & "/" & lYear, cFormatDate)
  
  If Txt_Fecha.Text = lFecha_File Then
    Rem Se coloca el path completo, que incluye el nombre de archivo
    Txt_ArchivoPlano.Text = MDI_Principal.CommonDialog.FileName
  Else
    MsgBox "La fecha del archivo no es la misma que la solicitada." & vbCr & "Verifique que el archivo sea del " & Txt_Fecha.Text, vbInformation, Me.Caption
  End If
  
  Exit Sub

Cmd_BuscarArchivo_Err:
  If Err = 32755 Then 'usuario presiona el boton Cancelar del Command Dialog
    Exit Sub
  End If
End Sub

Private Sub Sub_Cargar_Archivo_Plano()
Dim fCls_File As Object 'Class_ArchivoPlano
Dim lLinea As Double
Dim lProducto As String
Dim lCod_InstrumentoDB As String
Dim lId_Moneda As Double
Dim lId_Nemotecnico As String
Dim lId_Publicador_Precio As Double
  
  Call Sub_Bloquea_Puntero(Me)
  
  If Not Fnt_ValidarDatos Then
    Call Sub_Desbloquea_Puntero(Me)
    Exit Sub
  End If
    
  Grilla.Rows = 1
    
  Set fCls_File = CreateObject("DLLCS_ArchivoPlano.Class_ArchivoPlano") 'New Class_ArchivoPlano
  
  With fCls_File
    .Path = Mid(Txt_ArchivoPlano.Text, 1, InStr(1, Txt_ArchivoPlano.Text, "\"))
    .Name = Mid(Txt_ArchivoPlano.Text, InStr(1, Txt_ArchivoPlano.Text, "\") + 1)
    .fSeparador = ";"
    Call .OpenFile
    
    lLinea = 0
    .MoveFirst
    Do While Not .EofFile
      lLinea = lLinea + 1
      If .Fields.Count > 0 Then
        If Fnt_Validacion_Formato(lLinea, _
                                  .Fields.Count, _
                                  Trim(.Fields(eColumnas.eC_Nemotecnico).Value), _
                                  Trim(.Fields(eColumnas.eC_TipoInstrumento).Value), _
                                  Trim(.Fields(eColumnas.eC_UnidadReajuste).Value), _
                                  Trim(.Fields(eColumnas.eC_Precio).Value), _
                                  Trim(.Fields(eColumnas.eC_Duracion).Value), _
                                  Trim(.Fields(eColumnas.eC_TirValorizacion).Value), _
                                  Trim(.Fields(eColumnas.eC_Categoria).Value), _
                                  Trim(.Fields(eColumnas.eC_TirTransaccion).Value)) Then
          
          lProducto = Fnt_ComboSelected_KEY(Cmb_Productos)
          
          If Fnt_Verifica_Alias_Instrumento(lLinea, _
                                            Trim(.Fields(eC_TipoInstrumento).Value), _
                                            lProducto, _
                                            lCod_InstrumentoDB) Then
            If Fnt_Verifica_Alias_Moneda(lLinea, _
                                         Trim(.Fields(eC_UnidadReajuste).Value), _
                                         lId_Moneda) Then
              If Fnt_Verifica_Nemotecnico(lLinea, _
                                          Trim(.Fields(eC_Nemotecnico).Value), _
                                          Format(Txt_Fecha.Text, cFormatDate), _
                                          lId_Nemotecnico, _
                                          lId_Publicador_Precio) Then
                Call Sub_Ingresa_Precios(lLinea, _
                                         lId_Publicador_Precio, _
                                         fKey, _
                                         lId_Nemotecnico, _
                                         lId_Moneda, _
                                         Format(Txt_Fecha.Text, cFormatDate), _
                                         Trim(.Fields(eC_Precio).Value), _
                                         Trim(.Fields(eC_TirValorizacion).Value), _
                                         Trim(.Fields(eC_Duracion).Value))
              End If
            End If
          End If
        End If
      Else
        Grilla.AddItem "Error de formato en la l�nea: " & lLinea & " -- L�nea sin campos."
      End If
      .MoveNext
    Loop
    .CloseFile
    MsgBox "Carga de Precios de AFP Finalizada.", vbInformation, Me.Caption
  End With
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Sub Sub_Exportar_Archivo_Plano()
Dim lArchivo_Salida As Byte
Dim lNombreArchivo As String
Dim lFila As Double
  
On Error GoTo ErrProcedure
  
  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False
  
  If Grilla.Rows = 1 Then Exit Sub
  
  With MDI_Principal.CommonDialog
    .CancelError = True
    .InitDir = "C:\Mis documentos\"
    .Filter = "Texto (*.txt)|*.txt"
    .FileName = "Log_Precios_AFP"
    .ShowSave
    lNombreArchivo = .FileName
  End With
  
  If lNombreArchivo = "" Then GoTo ErrProcedure
  
  lArchivo_Salida = FreeFile
  Open lNombreArchivo For Output As #lArchivo_Salida
    For lFila = 1 To Grilla.Rows - 1
      Print #lArchivo_Salida, GetCell(Grilla, lFila, "error")
    Next
  Close #lArchivo_Salida
  MsgBox "Exportaci�n finalizada.", vbInformation, Me.Caption
  
ErrProcedure:
  Rem Usuario presiona el boton Cancelar del Command Dialog
  Rem o presiona No o Cancelar en el dialogo "ya existe archivo...desea reemplazarlo"
  If Err.Number = 32755 Or Err.Number = 1004 Then
    Me.Enabled = True
    Call Sub_Desbloquea_Puntero(Me)
    Exit Sub
  End If
  
  If Not Err.Number = 0 Then
    MsgBox "Error en la exportaci�n de datos a la planilla Excel.", vbExclamation, Me.Caption
  End If
  
  Me.Enabled = True
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function

Private Function Fnt_Validacion_Formato(pLinea As Double, _
                                        pNum_Campos As Long, _
                                        pNemotecnico As String, _
                                        pTipoInstrumento As String, _
                                        pUnidadReajuste As String, _
                                        pPrecio As String, _
                                        pDuracion As String, _
                                        pTirValorizacion As String, _
                                        pCategoria As String, _
                                        pTirTransaccion As String) As Boolean
Dim lError_Formato As Boolean

  lError_Formato = True
  If Not (pNum_Campos = eC_Categoria) Then
    Grilla.AddItem "Error de formato en la l�nea: " & pLinea & " -- Vienen " & pNum_Campos & " campos."
    lError_Formato = False
  ElseIf pNemotecnico = "" Then
    Grilla.AddItem "Error de formato en la l�nea: " & pLinea & " -- No tiene Nemot�cnico."
    lError_Formato = False
  ElseIf pTipoInstrumento = "" Then
    Grilla.AddItem "Error de formato en la l�nea: " & pLinea & " -- No tiene Tipo de Instrumento."
    lError_Formato = False
  ElseIf pUnidadReajuste = "" Then
    Grilla.AddItem "Error de formato en la l�nea: " & pLinea & " -- No tiene Unidad de Reajuste."
    lError_Formato = False
  ElseIf pPrecio = "" Then
    Grilla.AddItem "Error de formato en la l�nea: " & pLinea & " -- No tiene Precio."
    lError_Formato = False
  ElseIf Not IsNumeric(pPrecio) Then
    Grilla.AddItem "Error de formato en la l�nea: " & pLinea & " -- Valor de Precio no es num�rico."
    lError_Formato = False
  ElseIf pDuracion = "" Then
    Grilla.AddItem "Error de formato en la l�nea: " & pLinea & " -- No tiene Plazo Econ�mico."
    lError_Formato = False
  ElseIf Not IsNumeric(pDuracion) Then
    Grilla.AddItem "Error de formato en la l�nea: " & pLinea & " -- Valor de Plazo Econ�mico no es num�rico."
    lError_Formato = False
  ElseIf pTirValorizacion = "" Then
    Grilla.AddItem "Error de formato en la l�nea: " & pLinea & " -- No tiene TIR de Valorizaci�n."
    lError_Formato = False
  ElseIf Not IsNumeric(pTirValorizacion) Then
    Grilla.AddItem "Error de formato en la l�nea: " & pLinea & " -- Valor de TIR de Valorizaci�n no es num�rico."
    lError_Formato = False
  ElseIf pTirTransaccion = "" Then
    Grilla.AddItem "Error de formato en la l�nea: " & pLinea & " -- No tiene Categor�a."
    lError_Formato = False
  End If
  
  Fnt_Validacion_Formato = lError_Formato
  
End Function

Private Function Fnt_Verifica_Alias_Instrumento(pLinea As Double, _
                                                pCod_InstrumentoFile As String, _
                                                pProducto As String, _
                                                ByRef pCod_InstrumentoDB As String) As Boolean
Dim lReg As hFields

  Fnt_Verifica_Alias_Instrumento = False
  pCod_InstrumentoDB = ""
  Rem Busca si existe el codigo de instrumento leido desde el archivo
  gDB.Procedimiento = "PKG_TIPOS_CONVERSION.BuscarView"
  gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
  gDB.Parametros.Add "pID_TIPO_CONVERSION", ePT_Numero, cfId_Tipo_Conversion_Instrumento, ePD_Entrada
  gDB.Parametros.Add "pID_ORIGEN", ePT_Numero, cfId_Origen, ePD_Entrada
  gDB.Parametros.Add "pvalor", ePT_Caracter, pCod_InstrumentoFile, ePD_Entrada
  If Not pProducto = cCmbKALL Then
    gDB.Parametros.Add "pCOD_PRODUCTO", ePT_Caracter, pProducto, ePD_Entrada
  End If
  If gDB.EjecutaSP Then
    For Each lReg In gDB.Parametros("pcursor").Valor
      pCod_InstrumentoDB = lReg("cod_instrumento").Value
      Fnt_Verifica_Alias_Instrumento = True
    Next
    If Not Fnt_Verifica_Alias_Instrumento Then
      Grilla.AddItem "Error de lectura en la l�nea: " & pLinea & " -- C�digo de Instrumento '" & pCod_InstrumentoFile & "' no encontrado en el sistema."
    End If
  Else
    Grilla.AddItem "Error de lectura a la base de datos en la l�nea: " & pLinea
  End If
  gDB.Parametros.Clear

End Function

Private Function Fnt_Verifica_Alias_Moneda(pLinea As Double, _
                                           pCod_MonedaFile As String, _
                                           ByRef pId_MonedaDB As Double) As Boolean
Dim lReg As hFields

  Fnt_Verifica_Alias_Moneda = False
  pId_MonedaDB = 0
  Rem Busca si existe el codigo de moneda leido desde el archivo
  gDB.Procedimiento = "PKG_TIPOS_CONVERSION.BuscarView"
  gDB.Parametros.Add "Pcursor", ePT_Cursor, "", ePD_Ambos
  gDB.Parametros.Add "pID_TIPO_CONVERSION", ePT_Numero, cfId_Tipo_Conversion_Moneda, ePD_Entrada
  gDB.Parametros.Add "pID_ORIGEN", ePT_Numero, cfId_Origen, ePD_Entrada
  gDB.Parametros.Add "pvalor", ePT_Caracter, pCod_MonedaFile, ePD_Entrada
  If gDB.EjecutaSP Then
    For Each lReg In gDB.Parametros("pcursor").Valor
      pId_MonedaDB = lReg("ID_MONEDA").Value
      Fnt_Verifica_Alias_Moneda = True
    Next
    If Not Fnt_Verifica_Alias_Moneda Then
      Grilla.AddItem "Error de lectura en la l�nea: " & pLinea & " -- Moneda '" & pCod_MonedaFile & "' no encontrada en el sistema."
    End If
  Else
    Grilla.AddItem "Error de lectura a la base de datos en la l�nea: " & pLinea
  End If
  gDB.Parametros.Clear
  
End Function

Private Function Fnt_Verifica_Nemotecnico(pLinea As Double, _
                                          pNemotecnicoFile As String, _
                                          pFecha As Date, _
                                          ByRef pId_NemotecnicoDB, _
                                          ByRef pId_Publicador_Precio As Double) As Boolean
Dim lSaldos_Activos As Class_Saldo_Activos
Dim lNemotecnico As Class_Nemotecnicos
Dim lPublicadores_Precio As Class_Publicadores_Precio
Dim lReg As hFields
  
  Fnt_Verifica_Nemotecnico = False
  pId_NemotecnicoDB = ""
  pId_Publicador_Precio = 0
  
  If Chk_Cartera.Value = vbChecked Then
    Rem Busca el Nemot�cnico en Cartera (en Saldos_Activos)
    Set lSaldos_Activos = New Class_Saldo_Activos
    If lSaldos_Activos.Buscar_al_cierre(pFecha_Cierre:=pFecha, pNemotecnico:=pNemotecnicoFile) Then
      For Each lReg In lSaldos_Activos.Cursor
        pId_NemotecnicoDB = lSaldos_Activos.Cursor(1)("nemotecnico").Value
        Fnt_Verifica_Nemotecnico = True
      Next
      If Not Fnt_Verifica_Nemotecnico Then
        Grilla.AddItem "Error de lectura en la l�nea: " & pLinea & " -- Nemotecnico '" & pNemotecnicoFile & "' no encontrado en cartera."
      End If
    Else
      Grilla.AddItem "Error de lectura a la base de datos en la l�nea: " & pLinea
    End If
    Set lSaldos_Activos = Nothing
  Else
    Rem Busca el Nemot�cnico en el sistema
    Set lNemotecnico = New Class_Nemotecnicos
    lNemotecnico.Campo("nemotecnico").Valor = pNemotecnicoFile
    If lNemotecnico.Buscar_Nemotecnico("", False) Then
      If Not lNemotecnico.Campo("id_nemotecnico").Valor = "0" Then
        pId_NemotecnicoDB = lNemotecnico.Campo("id_nemotecnico").Valor
        Fnt_Verifica_Nemotecnico = True
      Else
        Grilla.AddItem "Error de lectura en la l�nea: " & pLinea & " -- Nemotecnico '" & pNemotecnicoFile & "' no encontrado en el sistema."
      End If
    Else
      Grilla.AddItem "Error de lectura a la base de datos en la l�nea: " & pLinea
    End If
    Set lNemotecnico = Nothing
  End If
  
  If Fnt_Verifica_Nemotecnico Then
    Rem Busca el Nemotecnico seg�n la Fecha estipulada
    Set lPublicadores_Precio = New Class_Publicadores_Precio
    With lPublicadores_Precio
      .Campo("ID_NEMOTECNICO").Valor = pId_NemotecnicoDB
      .Campo("FECHA").Valor = pFecha
      If .Buscar Then
        For Each lReg In .Cursor
          pId_Publicador_Precio = lReg("Id_Publicador_Precio").Value
        Next
      Else
        Grilla.AddItem "Error de lectura a la base de datos en la l�nea: " & pLinea
      End If
    End With
    Set lPublicadores_Precio = Nothing
  End If
  
End Function

Private Sub Sub_Ingresa_Precios(pLinea As Double, _
                                pId_Publicador_Precio As Double, _
                                pId_Publicador As String, _
                                pId_Nemotecnico As String, _
                                pId_Moneda As Double, _
                                pFecha As String, _
                                pPrecio As String, _
                                pTasa As String, _
                                pDuracion As String)
                                
Dim lPublicadores_Precio As Class_Publicadores_Precio
  
  Set lPublicadores_Precio = New Class_Publicadores_Precio
  With lPublicadores_Precio
    .Campo("Id_Publicador_Precio").Valor = pId_Publicador_Precio
    .Campo("Id_Publicador").Valor = pId_Publicador
    .Campo("Id_Nemotecnico").Valor = pId_Nemotecnico
    .Campo("Id_Moneda").Valor = pId_Moneda
    .Campo("Fecha").Valor = pFecha
    .Campo("Precio").Valor = Replace(pPrecio, ",", ".")
    .Campo("Tasa").Valor = Replace(pTasa, ",", ".")
    .Campo("Duracion").Valor = Replace(pDuracion, ",", ".")
    If Not .Guardar Then
      Grilla.AddItem "Error de Grabaci�n de Precios en la l�nea: " & pLinea & ". " & .ErrMsg
    End If
  End With
  
End Sub
