VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Reporte_Comisiones_Cuenta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte de Comisiones Devengadas"
   ClientHeight    =   6765
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   6150
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6765
   ScaleWidth      =   6150
   Begin VB.Frame Frm_Filtros 
      Caption         =   "Filtros de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   5535
      Left            =   60
      TabIndex        =   6
      Top             =   1170
      Width           =   6015
      Begin VB.CommandButton cmb_Buscar_NombreCliente 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5450
         Picture         =   "Frm_Reporte_Comisiones_Cuenta.frx":0000
         TabIndex        =   12
         Top             =   1470
         Width           =   375
      End
      Begin VB.CommandButton cmb_buscar_Rut 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3050
         Picture         =   "Frm_Reporte_Comisiones_Cuenta.frx":030A
         TabIndex        =   10
         Top             =   1080
         Width           =   375
      End
      Begin VB.CommandButton cmb_buscar_Cuenta 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3050
         Picture         =   "Frm_Reporte_Comisiones_Cuenta.frx":0614
         TabIndex        =   8
         Top             =   690
         Width           =   375
      End
      Begin hControl2.hTextLabel Txt_Num_Cuenta 
         Height          =   345
         Left            =   90
         TabIndex        =   9
         Top             =   690
         Width           =   2910
         _ExtentX        =   5133
         _ExtentY        =   609
         LabelWidth      =   1245
         TextMinWidth    =   1200
         Caption         =   "N� de Cuenta"
         Text            =   ""
         MaxLength       =   12
      End
      Begin hControl2.hTextLabel Txt_Rut_Cliente 
         Height          =   315
         Left            =   90
         TabIndex        =   11
         Top             =   1080
         Width           =   2910
         _ExtentX        =   5133
         _ExtentY        =   556
         LabelWidth      =   1245
         TextMinWidth    =   1200
         Caption         =   "RUT Cliente"
         Text            =   ""
         MaxLength       =   10
      End
      Begin hControl2.hTextLabel TxtRazonSocial 
         Height          =   315
         Left            =   90
         TabIndex        =   13
         Top             =   1470
         Width           =   5280
         _ExtentX        =   9313
         _ExtentY        =   556
         LabelWidth      =   1245
         Caption         =   "Nombre Cliente"
         Text            =   ""
         MaxLength       =   99
      End
      Begin TrueDBList80.TDBCombo Cmb_Asesor 
         Height          =   345
         Left            =   1350
         TabIndex        =   15
         Top             =   300
         Width           =   4455
         _ExtentX        =   7858
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Comisiones_Cuenta.frx":091E
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin MSComctlLib.Toolbar Toolbar_Interna 
         Height          =   360
         Left            =   0
         TabIndex        =   16
         Top             =   1890
         Width           =   6050
         _ExtentX        =   10663
         _ExtentY        =   635
         ButtonWidth     =   1746
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Wrappable       =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Agregar"
               Key             =   "ADD"
               Description     =   "Agrega informaci�n"
               Object.ToolTipText     =   "Agrega informaci�n"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Eliminar"
               Key             =   "DEL"
               Description     =   "Elimina informaci�n"
               Object.ToolTipText     =   "Elimina informaci�n"
            EndProperty
         EndProperty
         BorderStyle     =   1
         Begin MSComctlLib.ProgressBar ProgressBar1 
            Height          =   255
            Left            =   9420
            TabIndex        =   17
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
         Height          =   3000
         Left            =   90
         TabIndex        =   18
         Top             =   2295
         Width           =   5820
         _cx             =   10266
         _cy             =   5292
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   5
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Reporte_Comisiones_Cuenta.frx":09C8
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin VB.Label Lbl_Asesor 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Asesor"
         Height          =   345
         Left            =   90
         TabIndex        =   14
         Top             =   300
         Width           =   1245
      End
   End
   Begin VB.Frame Frm_Fecha_Re_Proceso 
      Caption         =   "Per�odo"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   795
      Left            =   60
      TabIndex        =   2
      Top             =   390
      Width           =   6015
      Begin MSComCtl2.DTPicker DTP_Fecha_Desde 
         Height          =   315
         Left            =   1470
         TabIndex        =   0
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   59637761
         CurrentDate     =   37732
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Hasta 
         Height          =   315
         Left            =   4500
         TabIndex        =   1
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   59637761
         CurrentDate     =   37732
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Hasta"
         Height          =   345
         Left            =   3240
         TabIndex        =   7
         Top             =   270
         Width           =   1215
      End
      Begin VB.Label Lbl_Fecha_Desde 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Desde"
         Height          =   345
         Left            =   210
         TabIndex        =   3
         Top             =   270
         Width           =   1215
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Height          =   360
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   635
      ButtonWidth     =   3016
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Generar Informe"
            Key             =   "CIERRE"
            Description     =   "Generar Informe de cuentas"
            Object.ToolTipText     =   "Generar Informe de cuentas"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la ventana perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la ventana perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   5
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Comisiones_Cuenta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fHoraInicio As Long
'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso       As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema      As String 'Codigo para el permiso sobre la ventana

Dim App_Excel       As Excel.Application      ' Excel application
Dim lLibro          As Excel.Workbook      ' Excel workbook

Private Type TCuenta
    id_cuenta       As Integer
    Num_Cuenta      As Long
    rut_cliente     As String
    dsc_cuenta      As String
    asesor_cuenta   As String
End Type

Dim aCuentas() As TCuenta

Dim lId_Cuenta      As String
Dim fId_Cuenta      As Long 'Flag para validar el agregar en grilla
Dim bMuestraCuadro  As Boolean

Public Sub Mostrar(pCod_Arbol_Sistema)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  
  Call Form_Resize
  
  Load Me
End Sub

Private Sub Cmb_Asesor_ItemChange()
    Txt_Num_Cuenta.Text = ""
    Txt_Rut_Cliente.Text = ""
    TxtRazonSocial.Text = ""
End Sub

Private Sub cmb_buscar_Cuenta_Click()
    lId_Cuenta = NVL(Frm_Busca_Cuentas.Buscar(pNum_Cuenta:=Txt_Num_Cuenta.Text), 0)
    Call Mostrar_Cuenta
End Sub

Private Sub cmb_Buscar_NombreCliente_Click()
    lId_Cuenta = NVL(Frm_Busca_Cuentas.BuscarCliente(pFiltro_NombreCliente:=TxtRazonSocial.Text, pBusca_Cuenta:=True), 0)
    Call Mostrar_Cuenta
End Sub

Private Sub cmb_buscar_Rut_Click()
    lId_Cuenta = NVL(Frm_Busca_Cuentas.BuscarCliente(pFiltro_RutCliente:=Txt_Rut_Cliente.Text, pBusca_Cuenta:=True), 0)
    Call Mostrar_Cuenta
End Sub

Private Sub Form_Load()
    Dim lReg  As hCollection.hFields
    Dim lLinea As Long

    With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
            .Buttons("CIERRE").Image = cBoton_Aceptar
            .Buttons("REFRESH").Image = cBoton_Refrescar
            .Buttons("EXIT").Image = cBoton_Salir
    End With
    
    With Toolbar_Interna
    Set .ImageList = MDI_Principal.ImageListGlobal16
        .Buttons("ADD").Image = cBoton_Agregar_Grilla
        .Buttons("DEL").Image = cBoton_Eliminar_Grilla
    End With
  
    Call Sub_CargaForm
    
    Me.Top = 1
    Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
    Dim lCierre As Class_Verificaciones_Cierre
    Dim lcCuenta As Object
    Dim lReg As hFields
    Dim lLinea As Long

    Call Sub_FormControl_Color(Me.Controls)

    Rem Limpia la grilla
    Grilla_Cuentas.Rows = 1
    fId_Cuenta = 0
    
    Txt_Num_Cuenta.Text = ""
    Txt_Rut_Cliente.Text = ""
    TxtRazonSocial.Text = ""

    Rem La hora que se propone es la del �ltimo dia de cierre
    Set lCierre = New Class_Verificaciones_Cierre
  
    DTP_Fecha_Hasta.Value = Fnt_FechaServidor
    DTP_Fecha_Desde.Value = DTP_Fecha_Hasta.Value
  
    Rem Carga los combos con el primer elemento vac�o
    Call Sub_CargaCombo_Asesor(Cmb_Asesor, pBlanco:=True)
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "CIERRE"
      Call Sub_Informe
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargaForm
      End If
  End Select

End Sub

Private Sub Sub_Agrega()
  Dim lLinea      As Long
  Dim lId_Cuenta  As String
  Dim lId_Asesor  As String
  Dim lCursor     As hCollection.hRecord
  Dim lReg        As hCollection.hFields
  Dim lCuentas    As Class_Re_Proceso_Cierre
  Dim lcCuenta    As Object
  
  lId_Cuenta = Txt_Num_Cuenta.Text
  
  lId_Asesor = Fnt_ComboSelected_KEY(Cmb_Asesor)
  lId_Asesor = IIf(lId_Asesor = cCmbBLANCO, "", lId_Asesor)
  
  ' Agrega a la grilla cuentas por asesor
  If Not lId_Asesor = "" Then
    Grilla_Cuentas.Rows = 1
    fId_Cuenta = 1
  
    gDB.Parametros.Clear
    gDB.Procedimiento = "PKG_CUENTAS$BuscarCuentasPorAsesor"
    gDB.Parametros.Add "pCursor", ePT_Cursor, "", ePD_Ambos
    gDB.Parametros.Add "pId_Asesor", ePT_Numero, lId_Asesor, ePD_Entrada
    gDB.Parametros.Add "pId_Empresa", ePT_Numero, Fnt_EmpresaActual, ePD_Entrada
    
    If Not gDB.EjecutaSP Then
      MsgBox "No se pudo obtener cuentas del asesor", vbInformation, Me.Caption
      Exit Sub
    End If
    
    Set lCursor = gDB.Parametros("pCursor").Valor
    
    For Each lReg In lCursor
      Grilla_Cuentas.Row = Grilla_Cuentas.FindRow(lReg("id_cuenta").Value, , Grilla_Cuentas.ColIndex("colum_pk"))
      If Not Grilla_Cuentas.Row = cNewEntidad Then
        MsgBox "Cuenta ya se encuentra asociada.", vbExclamation, Me.Caption
      Else
        lLinea = Grilla_Cuentas.Rows
        Call Grilla_Cuentas.AddItem("")
        Call SetCell(Grilla_Cuentas, lLinea, "colum_pk", lReg("id_cuenta").Value)
        Call SetCell(Grilla_Cuentas, lLinea, "num_cuenta", lReg("num_cuenta").Value)
        Call SetCell(Grilla_Cuentas, lLinea, "rut_cliente", lReg("rut_cliente").Value)
        Call SetCell(Grilla_Cuentas, lLinea, "dsc_cuenta", NVL(lReg("nombre_cliente").Value, ""))
        Call SetCell(Grilla_Cuentas, lLinea, "asesor_cuenta", NVL(lReg("nombre_asesor").Value, ""))
      End If
    Next
  End If
  
  ' Agrega a la grilla por cuentas
  If Not lId_Cuenta = "" Then
    
    If fId_Cuenta = 1 Then
      Grilla_Cuentas.Rows = 1
      fId_Cuenta = 0
    End If
    
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    
    With lcCuenta
      .Campo("num_cuenta").Valor = Txt_Num_Cuenta.Text
      .Campo("id_Empresa").Valor = Fnt_EmpresaActual
      
      If .Buscar_Vigentes Then
        If .Cursor.Count > 0 Then
            lId_Asesor = .Cursor(1)("nombre_asesor").Value
            lId_Cuenta = .Cursor(1)("id_cuenta").Value
        End If
      End If
    End With
    
    Grilla_Cuentas.Row = Grilla_Cuentas.FindRow(lId_Cuenta, , Grilla_Cuentas.ColIndex("colum_pk"))
    If Not Grilla_Cuentas.Row = cNewEntidad Then
      MsgBox "Cuenta ya se encuentra asociada.", vbExclamation, Me.Caption
    Else
      lLinea = Grilla_Cuentas.Rows
      Grilla_Cuentas.AddItem ("")
      SetCell Grilla_Cuentas, lLinea, "colum_pk", lId_Cuenta
      SetCell Grilla_Cuentas, lLinea, "num_cuenta", Txt_Num_Cuenta.Text
      SetCell Grilla_Cuentas, lLinea, "rut_cliente", Txt_Rut_Cliente.Text
      SetCell Grilla_Cuentas, lLinea, "dsc_cuenta", TxtRazonSocial.Text
      SetCell Grilla_Cuentas, lLinea, "asesor_cuenta", lId_Asesor
    End If
  End If
End Sub

' Elimina de la grilla
Private Sub Sub_Elimina()
  If Grilla_Cuentas.Row >= 1 Then
    Grilla_Cuentas.RemoveItem (Grilla_Cuentas.Row)
  End If
End Sub

Private Sub Toolbar_Interna_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "ADD"
      Call Sub_Agrega
    Case "DEL"
      Call Sub_Elimina
  End Select

End Sub

Private Sub Sub_Informe()
    Dim lcComision_Hono_Ase_Cuenta  As Class_Comisiones_Hono_Ase_Cuenta
    Dim lcComi_Fija_Hono_Ase_Cuenta As Class_Comi_Fija_Hono_Ase_Cuenta
    
    Dim oSaldosCaja     As Class_Saldos_Caja
    
    Dim lhCajas         As hFields
    
    Dim lFila           As Long
    Dim lId_Cuenta      As String
    Dim lFecha_Proceso  As Date
    
    Dim nFila   As Integer
    Dim i       As Integer
    
    Dim bPrimera As Boolean
    
    ReDim aCuentas(0)
    
    Call Sub_Bloquea_Puntero(Me)
    Me.Enabled = False
  
    If Grilla_Cuentas.Rows = 1 Then
        GoTo ExitProcedure
    End If
        
    For lFila = 1 To Grilla_Cuentas.Rows - 1
      nFila = UBound(aCuentas) + 1
      
      lId_Cuenta = GetCell(Grilla_Cuentas, lFila, "colum_pk")
      nFila = UBound(aCuentas) + 1
      ReDim Preserve aCuentas(nFila)
      
      aCuentas(nFila).id_cuenta = GetCell(Grilla_Cuentas, lFila, "colum_pk")
      aCuentas(nFila).Num_Cuenta = GetCell(Grilla_Cuentas, lFila, "num_cuenta")
      aCuentas(nFila).rut_cliente = GetCell(Grilla_Cuentas, lFila, "rut_cliente")
      aCuentas(nFila).dsc_cuenta = GetCell(Grilla_Cuentas, lFila, "dsc_cuenta")
      aCuentas(nFila).asesor_cuenta = GetCell(Grilla_Cuentas, lFila, "asesor_cuenta")
    Next
    
    Crea_Excel
    
ExitProcedure:
    Set oSaldosCaja = Nothing
    Call Sub_Desbloquea_Puntero(Me)

    Me.Enabled = True
End Sub

' Para cada una de las cuentas encontradas en la grilla de la aplicacion.
Private Sub Crea_Excel()
    Dim i       As Integer
    Dim hoja    As Integer
    Dim nFilaExcel As Integer
        
    Set App_Excel = CreateObject("Excel.application")
    App_Excel.DisplayAlerts = False
    Set lLibro = App_Excel.Workbooks.Add
    
    ' App_Excel.Visible = True
    
    For i = 1 To lLibro.Worksheets.Count - 1
        lLibro.Worksheets(i).Delete
    Next
        
    For i = 1 To UBound(aCuentas)
      Call Sub_Interactivo(gRelogDB)
              
      lLibro.Worksheets.Add After:=lLibro.Worksheets(lLibro.Worksheets.Count)
      
      ' Nombre de la hoja
      lLibro.ActiveSheet.Name = "Cuenta " & aCuentas(i).Num_Cuenta
      lLibro.ActiveSheet.Cells(1, 1).Select
      lLibro.ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).ShapeRange.IncrementLeft 29.75
      
      lLibro.ActiveSheet.Columns("A:A").ColumnWidth = 2
      
      nFilaExcel = HeaderXls_Informe_Portada(i)
      
      Genera_Informe i, nFilaExcel
    Next
    
    lLibro.Worksheets(1).Delete
    
    App_Excel.Visible = True
    App_Excel.UserControl = True
    
End Sub

' Header de la hojas de la aplicacion.
Function HeaderXls_Informe_Portada(ByVal hoja As Integer) As Integer
    Dim nFilaInicioDatos As Integer
    
    App_Excel.ActiveWindow.DisplayGridlines = False
    
    With lLibro.ActiveSheet
        'TITULO
        .Range("B5").Value = "Reporte Comisiones Devengadas"
        .Range("B5:H5").Font.Bold = True
        .Range("B5:H5").Font.Size = 14
        .Range("B5:H5").HorizontalAlignment = xlCenter
        .Range("B5:H5").Merge
        
        .Range("B8").Value = "Empresa: " & gDsc_Empresa
        .Range("B8:D8").Merge
        .Range("F8").Value = "Fecha Generaci�n Reporte: " & Fnt_FechaServidor
        .Range("F8:H8").Merge
        
        .Range("B11").Value = "Cuenta"
        .Range("B11:H11").HorizontalAlignment = xlCenter
        .Range("C11").Value = "Rut Cliente"
        .Range("D11").Value = "Nombre Cliente"
        .Range("E11").Value = "Fecha Movimiento"
        .Range("F11").Value = "Monto"
        .Range("G11").Value = "Item / Producto"
        .Range("H11").Value = "Asesor"
        
        .Range("B11:H11").BorderAround
        .Range("B11:H11").Borders.Color = RGB(0, 0, 0)
        .Range("B11:H11").Interior.Color = RGB(255, 255, 0)
        .Range("B11:H11").Font.Bold = True
        
        nFilaInicioDatos = 12
        
    End With
    
    HeaderXls_Informe_Portada = nFilaInicioDatos  ' Columna donde comienzan los datos
    
End Function

' Realiza informe por cuenta
Sub Genera_Informe(ByVal hoja As Integer, iFilaExcel As Integer)
    Dim lReg                As hCollection.hFields
    Dim oCuentasInforme     As Class_ComisionesCuentas
    
    Dim lxHoja          As Worksheet
    
    Dim iFila           As Integer
    Dim iCol            As Integer
    
    Dim iColExcel       As Integer
    Dim lIdCuenta       As String
    
    Dim sValor              As String
    Dim nDecimales          As Integer
        
    Dim dFechaAnterior      As String
    Dim sTipo               As String
    Dim xRango              As String
    Dim sFormato            As String
    
    On Error GoTo ErrProcedure
    
    lIdCuenta = aCuentas(hoja).id_cuenta
    Set lxHoja = lLibro.ActiveSheet
    
    Set oCuentasInforme = New Class_ComisionesCuentas
    
    With oCuentasInforme
    
        If Not .Buscar_Comisiones_Informe(pId_Cuenta:=lIdCuenta, _
                                        pFecha_ini:=DTP_Fecha_Desde.Value, _
                                        Pfecha_fin:=DTP_Fecha_Hasta.Value) Then
                                        
            GoTo ExitProcedure
        End If
        
        dFechaAnterior = ""
        
        iFila = iFilaExcel - 1
        
        If oCuentasInforme.Cursor.Count > 0 Then
        
            lxHoja.Cells(iFila + 1, 2).Value = aCuentas(hoja).Num_Cuenta
            lxHoja.Cells(iFila + 1, 3).Value = aCuentas(hoja).rut_cliente
            lxHoja.Cells(iFila + 1, 4).Value = aCuentas(hoja).dsc_cuenta
            lxHoja.Cells(iFila + 1, 8).Value = aCuentas(hoja).asesor_cuenta
        
            For Each lReg In oCuentasInforme.Cursor
                Call Sub_Interactivo(gRelogDB)
            
                sFormato = "#,##0"
                If lReg("DICIMALES_MOSTRAR").Value > 0 Then
                    sFormato = sFormato & "." & String(lReg("DICIMALES_MOSTRAR").Value, "0")
                End If
                
                iFila = iFila + 1
                
                lxHoja.Cells(iFila, 5).Value = lReg("fecha_cierre").Value
                lxHoja.Cells(iFila, 6).Value = lReg("comision_honorarios").Value
                lxHoja.Cells(iFila, 7).Value = lReg("DSC_INSTRUMENTO").Value
                
                lxHoja.Cells(iFila, 6).NumberFormat = sFormato
            Next
            
            Set oCuentasInforme = Nothing
        Else
            iFila = iFila + 1
        End If
        
        iFila = iFila + 1
        
        lxHoja.Cells(iFila, 4).Value = "Total Moneda"
        lxHoja.Cells(iFila, 4).HorizontalAlignment = xlLeft
        lxHoja.Range(lxHoja.Cells(iFila, 2), lxHoja.Cells(iFila, 3)).Merge
        lxHoja.Range(lxHoja.Cells(iFila, 4), lxHoja.Cells(iFila, 5)).Merge
        lxHoja.Range(lxHoja.Cells(iFila, 7), lxHoja.Cells(iFila, 8)).Merge
        
        For iCol = 6 To 6
            xRango = "=SUM(" & Chr(Asc("A") + iCol - 1) & iFilaExcel & ":" & Chr(Asc("A") + iCol - 1) & iFila - 1 & ")"
            lxHoja.Cells(iFila, iCol).Formula = xRango
        Next
        
        lxHoja.Range(lxHoja.Cells(iFila, 4), lxHoja.Cells(iFila, 6)).Interior.Color = RGB(127, 255, 212)
        lxHoja.Range(lxHoja.Cells(iFila, 4), lxHoja.Cells(iFila, 6)).Font.Bold = True
        
    End With
   
    With lxHoja
        'Rayas verticales
        For iCol = 2 To 8
            .Range(.Cells(iFilaExcel, iCol), .Cells(iFila, iCol)).BorderAround
            .Range(.Cells(iFilaExcel, iCol), .Cells(iFila, iCol)).Borders.Color = RGB(0, 0, 0)
            .Range(.Cells(iFilaExcel, iCol), .Cells(iFila, iCol)).Borders(xlInsideHorizontal).LineStyle = xlNone
        Next
        
        lxHoja.Range(lxHoja.Cells(iFila, 2), lxHoja.Cells(iFila, 8)).BorderAround
        lxHoja.Range(lxHoja.Cells(iFila, 2), lxHoja.Cells(iFila, 8)).Borders.Color = RGB(0, 0, 0)
        
    End With
            
    bMuestraCuadro = True
    'Cuadros anexos en la hoja
    If bMuestraCuadro Then
        iFila = 13
        ComisionesPorcentuales hoja, iFila
        ComisionesFijas hoja, iFila
    End If
    
    lxHoja.Range("C1:E1").EntireColumn.AutoFit
    lxHoja.Range("G1:O1").EntireColumn.AutoFit
    lxHoja.Cells(1, 1).Select
    
ErrProcedure:
    If Not Err.Number = 0 Then
        Call Fnt_MsgError(eLS_ErrSystem, "Problemas con el informe de Comisiones de Cuentas.", Err.Description, pConLog:=True)
        Err.Clear
        GoTo ExitProcedure
        Resume
    End If

ExitProcedure:

  Set oCuentasInforme = Nothing
  
End Sub

' Realiza el cuadro completo de las comisiones porcentuales en la hoja
Private Sub ComisionesPorcentuales(ByVal hoja As Integer, ByRef Filacomienzo)
    Dim lReg                As hCollection.hFields
    Dim oComisionesFijas    As Class_ComisionesCuentas
    
    Dim lxHoja          As Worksheet
    
        
    Dim iFila           As Integer
    Dim iFilaInicial    As Integer
        
    Dim iColExcel       As Integer
    Dim lIdCuenta       As String
    
    Dim sValor              As String
    
    Dim nDecimales          As Integer
        
    Dim dFechaAnterior      As String
    Dim sTipo               As String
    Dim xRango              As String
    
    On Error GoTo ErrProcedure
    
    lIdCuenta = aCuentas(hoja).id_cuenta
    Set lxHoja = lLibro.ActiveSheet
    
    Set oComisionesFijas = New Class_ComisionesCuentas
    
    With oComisionesFijas
        .Campo("ID_CUENTA").Valor = lIdCuenta
        
        If Not .Buscar_Comisiones_Porcentuales(lIdCuenta, DTP_Fecha_Desde.Value, DTP_Fecha_Hasta.Value) Then
            GoTo ExitProcedure
        End If

' Encabezado de cuadro
        lxHoja.Range("K11").Value = "Comisiones Porcentuales"
        lxHoja.Range("K11").Font.Bold = True
        lxHoja.Range("K11:L11").Merge
        
        lxHoja.Range("K12").Value = "Fecha Desde"
        lxHoja.Range("K12:K13").Merge
        
        lxHoja.Range("L12").Value = "Fecha Hasta"
        lxHoja.Range("L12:L13").Merge
        
        lxHoja.Range("M12").Value = "Periodicidad"
        lxHoja.Range("M12:M13").Merge

        lxHoja.Range("N12").Value = "% Comisi�n ADC"
        lxHoja.Range("N12:N13").Merge
        
        lxHoja.Range("O12").Value = "Instrumento"
        lxHoja.Range("O12:O13").Merge
        
        lxHoja.Range("K12:O13").WrapText = True
        lxHoja.Range("K12:O13").HorizontalAlignment = xlCenter
        lxHoja.Range("K12:O13").VerticalAlignment = xlCenter
        lxHoja.Range("K12:O13").ColumnWidth = 12.57
        
        lxHoja.Range("K12:O13").BorderAround
        lxHoja.Range("K12:O13").Borders.Color = RGB(0, 0, 0)
        lxHoja.Range("K12:O13").Interior.Color = RGB(255, 255, 0)
        lxHoja.Range("K12:O13").Font.Bold = True
        
        iFilaInicial = 14
        iFila = iFilaInicial
        iColExcel = 11
        
        If oComisionesFijas.Cursor.Count > 0 Then
          For Each lReg In oComisionesFijas.Cursor
            Call Sub_Interactivo(gRelogDB)
            
            iColExcel = 11
            lxHoja.Cells(iFila, iColExcel).Value = lReg("FECHA_INI").Value
            lxHoja.Cells(iFila, iColExcel + 1).Value = lReg("FECHA_TER").Value
            lxHoja.Cells(iFila, iColExcel + 2).Value = lReg("DSC_COMISION_FIJA_PERIODICIDAD").Value
            lxHoja.Cells(iFila, iColExcel + 3).Value = lReg("COMISION_HONORARIOS").Value * 100
            lxHoja.Cells(iFila, iColExcel + 4).Value = lReg("DSC_INTRUMENTO").Value
            iFila = iFila + 1
          Next
          
          lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel), lxHoja.Cells(iFila - 1, iColExcel + 4)).BorderAround
          lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel), lxHoja.Cells(iFila - 1, iColExcel + 4)).Borders.Color = RGB(0, 0, 0)
          lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel), lxHoja.Cells(iFila - 1, iColExcel + 4)).Borders(xlInsideHorizontal).LineStyle = xlNone
          lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel + 3), lxHoja.Cells(iFila - 1, iColExcel + 4)).NumberFormat = "#,##0.00"
           
        Else
          lxHoja.Cells(iFila, iColExcel).Value = "No tiene"
          lxHoja.Range(lxHoja.Cells(iFila, iColExcel), lxHoja.Cells(iFila, iColExcel + 4)).BorderAround
          lxHoja.Range(lxHoja.Cells(iFila, iColExcel), lxHoja.Cells(iFila, iColExcel + 4)).Borders.Color = RGB(0, 0, 0)
          lxHoja.Range(lxHoja.Cells(iFila, iColExcel), lxHoja.Cells(iFila, iColExcel + 4)).Borders(xlInsideVertical).LineStyle = xlNone

          iFila = iFila + 1
        End If
        
        Filacomienzo = iFila
        
    End With
    
ErrProcedure:
    If Not Err.Number = 0 Then
        Call Fnt_MsgError(eLS_ErrSystem, "Problemas con el informe de Comisiones de Cuenta.", Err.Description, pConLog:=True)
        Err.Clear
        GoTo ExitProcedure
        Resume
    End If

ExitProcedure:

  Set oComisionesFijas = Nothing

End Sub

' Realizar el cuadro completo de las comisiones fijas en la hoja
Private Sub ComisionesFijas(ByVal hoja As Integer, iFilaComienzo As Integer)
    Dim lReg                As hCollection.hFields
    Dim oComisionesFijas    As Class_ComisionesCuentas
    
    Dim lxHoja          As Worksheet
    
    Dim iFila           As Integer
    Dim iFilaInicial    As Integer
        
    Dim iColExcel       As Integer
    Dim lIdCuenta       As String
    
    Dim sValor              As String
    Dim nDecimales          As Integer
        
    Dim dFechaAnterior      As String
    Dim sTipo               As String
    Dim xRango              As String
    
    On Error GoTo ErrProcedure
    
    lIdCuenta = aCuentas(hoja).id_cuenta
    Set lxHoja = lLibro.ActiveSheet
    
    Set oComisionesFijas = New Class_ComisionesCuentas
    
    With oComisionesFijas
        .Campo("ID_CUENTA").Valor = lIdCuenta
        
        If Not .Buscar_Comisiones_Fijas(lIdCuenta, DTP_Fecha_Desde.Value, DTP_Fecha_Hasta.Value) Then
            GoTo ExitProcedure
        End If

' Encabezado de cuadro
        iFila = iFilaComienzo + 1
        
        lxHoja.Range("K" & iFila).Value = "Comisiones Fijas"
        lxHoja.Range("K" & iFila).Font.Bold = True
        lxHoja.Range("K" & iFila & ":L" & iFila).Merge
        
        iFila = iFila + 1
        lxHoja.Range("K" & iFila).Value = "Fecha Desde"
        lxHoja.Range("K" & iFila & ":K" & iFila + 1).Merge
        
        lxHoja.Range("L" & iFila).Value = "Fecha Hasta"
        lxHoja.Range("L" & iFila & ":L" & iFila + 1).Merge
        
        lxHoja.Range("M" & iFila).Value = "Periodicidad"
        lxHoja.Range("M" & iFila & ":M" & iFila + 1).Merge

        lxHoja.Range("N" & iFila).Value = "Comisi�n ADC"
        lxHoja.Range("N" & iFila & ":N" & iFila + 1).Merge
        
        lxHoja.Range("O" & iFila).Value = "Instrumento"
        lxHoja.Range("O" & iFila & ":O" & iFila + 1).Merge
        
        lxHoja.Range("K" & iFila & ":O" & iFila + 1).WrapText = True
        lxHoja.Range("K" & iFila & ":O" & iFila + 1).HorizontalAlignment = xlCenter
        lxHoja.Range("K" & iFila & ":O" & iFila + 1).VerticalAlignment = xlCenter
        lxHoja.Range("K" & iFila & ":O" & iFila + 1).ColumnWidth = 12.57
        
        lxHoja.Range("K" & iFila & ":O" & iFila + 1).BorderAround
        lxHoja.Range("K" & iFila & ":O" & iFila + 1).Borders.Color = RGB(0, 0, 0)
        lxHoja.Range("K" & iFila & ":O" & iFila + 1).Interior.Color = RGB(255, 255, 0)
        lxHoja.Range("K" & iFila & ":O" & iFila + 1).Font.Bold = True
        
        iFilaInicial = iFila + 2
        iFila = iFilaInicial
        iColExcel = 11

        If oComisionesFijas.Cursor.Count > 0 Then
            For Each lReg In oComisionesFijas.Cursor
              Call Sub_Interactivo(gRelogDB)
            
                iColExcel = 11
                lxHoja.Cells(iFila, iColExcel).Value = lReg("FECHA_INI").Value
                lxHoja.Cells(iFila, iColExcel + 1).Value = lReg("FECHA_TER").Value
                lxHoja.Cells(iFila, iColExcel + 2).Value = lReg("DSC_COMISION_FIJA_PERIODICIDAD").Value
                lxHoja.Cells(iFila, iColExcel + 3).Value = lReg("MONTO_COMISION_HONORARIO").Value
                lxHoja.Cells(iFila, iColExcel + 4).Value = lReg("DSC_INTRUMENTO").Value
                iFila = iFila + 1
            Next
            
            lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel), lxHoja.Cells(iFila - 1, iColExcel + 4)).BorderAround
            lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel), lxHoja.Cells(iFila - 1, iColExcel + 4)).Borders.Color = RGB(0, 0, 0)
            lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel), lxHoja.Cells(iFila - 1, iColExcel + 4)).Borders(xlInsideHorizontal).LineStyle = xlNone
            lxHoja.Range(lxHoja.Cells(iFilaInicial, iColExcel + 3), lxHoja.Cells(iFila - 1, iColExcel + 4)).NumberFormat = "#,##0.00"
          
        Else
            lxHoja.Cells(iFila, iColExcel).Value = "No tiene"
            lxHoja.Range(lxHoja.Cells(iFila, iColExcel), lxHoja.Cells(iFila, iColExcel + 4)).BorderAround
            lxHoja.Range(lxHoja.Cells(iFila, iColExcel), lxHoja.Cells(iFila, iColExcel + 4)).Borders.Color = RGB(0, 0, 0)
            lxHoja.Range(lxHoja.Cells(iFila, iColExcel), lxHoja.Cells(iFila, iColExcel + 4)).Borders(xlInsideVertical).LineStyle = xlNone
            iFila = iFila + 1
        End If
        
    End With
    
ErrProcedure:
    If Not Err.Number = 0 Then
        Call Fnt_MsgError(eLS_ErrSystem, "Problemas con el informe de Comisiones de Cuenta.", Err.Description, pConLog:=True)
        Err.Clear
        GoTo ExitProcedure
        Resume
    End If

ExitProcedure:

  Set oComisionesFijas = Nothing

End Sub

Private Sub Txt_Num_Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call cmb_buscar_Cuenta_Click
    End If
End Sub

Private Sub Txt_Rut_Cliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call cmb_buscar_Rut_Click
    End If
End Sub

Private Sub TxtRazonSocial_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then
       Call cmb_Buscar_NombreCliente_Click
    End If
End Sub

Public Sub Mostrar_Cuenta()
Dim pFecha              As Date
Dim lRutCliente As String
Dim lNombreCliente As String
Dim lNumCuenta As String
Dim lDesError As String
Dim lFecOperativa  As String
Dim lFechaCierre  As Date
Dim lcCuenta As Object

    lDesError = ""
    pFecha = Fnt_FechaServidor
    Call Sub_Entrega_DatosCuenta(lId_Cuenta, pRut_Cliente:=lRutCliente, pNombre_Cliente:=lNombreCliente, pNum_Cuenta:=lNumCuenta, PError:=lDesError)
    If lId_Cuenta <> "" And lId_Cuenta <> "0" Then
        Txt_Rut_Cliente.Text = lRutCliente
        TxtRazonSocial.Text = lNombreCliente
        Txt_Num_Cuenta.Text = lNumCuenta
        Call Sub_CargaCombo_Asesor(Cmb_Asesor, pBlanco:=True)
    End If
End Sub
