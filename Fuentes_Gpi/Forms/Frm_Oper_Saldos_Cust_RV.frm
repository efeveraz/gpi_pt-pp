VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Oper_Saldos_Cust_RV 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Operaci�n de Saldos de Custodia RV"
   ClientHeight    =   4755
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4755
   ScaleWidth      =   9330
   Begin VB.Frame Frnm_Grilla_Sucesos 
      Caption         =   "Visor de Sucesos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   3255
      Left            =   90
      TabIndex        =   5
      Top             =   1410
      Width           =   9165
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   2865
         Left            =   120
         TabIndex        =   6
         Top             =   270
         Width           =   7965
         _cx             =   14049
         _cy             =   5054
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   3
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Oper_Saldos_Cust_RV.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Detalle 
         Height          =   330
         Left            =   8160
         TabIndex        =   7
         Top             =   540
         Width           =   900
         _ExtentX        =   1588
         _ExtentY        =   582
         ButtonWidth     =   1588
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Detalle"
               Key             =   "DETALLE"
               Object.ToolTipText     =   "Muestra el detalle de los sucesos del Cierre"
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Height          =   915
      Left            =   90
      TabIndex        =   2
      Top             =   420
      Width           =   9165
      Begin VB.Frame Fra_Planilla 
         Caption         =   "Planilla"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   645
         Left            =   7050
         TabIndex        =   8
         Top             =   120
         Width           =   1995
         Begin VB.OptionButton Opt_FFMM 
            Caption         =   "FFMM"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   1080
            TabIndex        =   10
            Top             =   240
            Width           =   795
         End
         Begin VB.OptionButton Opt_Accion 
            Caption         =   "RV"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   90
            TabIndex        =   9
            Top             =   240
            Value           =   -1  'True
            Width           =   885
         End
      End
      Begin VB.CommandButton Cmb_BuscaFile 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   6570
         TabIndex        =   3
         ToolTipText     =   "Busca el Archivo Plano de Precios de SAFP"
         Top             =   300
         Width           =   375
      End
      Begin hControl2.hTextLabel Txt_ArchivoPlano 
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Tag             =   "OBLI"
         Top             =   300
         Width           =   6360
         _ExtentX        =   11218
         _ExtentY        =   556
         LabelWidth      =   1200
         TextMinWidth    =   1200
         Caption         =   "Archivo Plano"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9330
      _ExtentX        =   16457
      _ExtentY        =   635
      ButtonWidth     =   1588
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Oper_Saldos_Cust_RV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const fc_Mercado = "N"

Public fKey As String

Private Enum eExcel_FFMM
  eGD_rut_Cliente = 1
  eGD_Codigo_Fondo
  eGD_Cuenta
  eGD_Saldo_Cuota
  eGD_Fecha
  eGD_Valor_Cuota
  eGD_Saldo_Moneda
End Enum

Dim fSaldos_Cust_RV As Object

Public Function Fnt_Mostrar(pCod_Proceso_Componente As String)
  Fnt_Mostrar = False

  fKey = pCod_Proceso_Componente
  
  If Not Fnt_CargarDatos Then
    Unload Me
    Exit Function
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
  
  Fnt_Mostrar = True
End Function

Private Function Fnt_CargarDatos() As Boolean
Dim lcProceso_Componente  As Class_Proceso_Componente
Dim lMensaje              As String

On Error GoTo ErrProcedure

  Call Sub_Bloquea_Puntero(Me)
  
  Load Me
  Fnt_CargarDatos = False
  
  Set fSaldos_Cust_RV = Nothing
  
  'Busca el componente que corresponde al conciliador
  Set lcProceso_Componente = New Class_Proceso_Componente
  With lcProceso_Componente
    .Campo("COD_PROCESO_COMPONENTE").Valor = fKey
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    
    Set fSaldos_Cust_RV = .IniciaClass(lMensaje)
    
    If fSaldos_Cust_RV Is Nothing Then
      Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                      , lMensaje _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  Set lcProceso_Componente = Nothing
  
  Fnt_CargarDatos = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la carga del Proceso Componente (" & fKey & ")." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Function

Private Sub Cmb_BuscaFile_Click()
  
  If fSaldos_Cust_RV.Fnt_Busca_Archivo_Plano Then
    Txt_ArchivoPlano.Text = fSaldos_Cust_RV.fArchivo
  End If
  
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = "boton_grabar"
      .Buttons("EXIT").Image = "boton_salir"
  End With

  Txt_ArchivoPlano.Text = ""
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_DblClick()
Dim lMensaje As String

  With Grilla
    If .Row > 0 Then
      lMensaje = GetCell(Grilla, .Row, "TEXTO")
      If Not lMensaje = "" Then
        MsgBox lMensaje, vbInformation, Me.Caption
      End If
    End If
  End With
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
'      If Opt_Accion Then
'        If Fnt_Grabar Then
'          MsgBox "Saldos de Custodia de Renta Variable guardados correctamente.", vbInformation, Me.Caption
'        End If
'      ElseIf Opt_FFMM Then
'        If Fnt_Grabar_FFMM Then
'          MsgBox "Saldos de Custodia de Fondos Mutuos guardados correctamente.", vbInformation, Me.Caption
'        End If
'      End If
      If Opt_Accion Then
        If Fnt_Grabar_Precios_Accion Then
          MsgBox "Precios de Acciones guardados correctamente.", vbInformation, Me.Caption
        End If
      ElseIf Opt_FFMM Then
        If Fnt_GrabarValores_Cuotas_FFMM Then
          MsgBox "Valores Cuotas de Fondos Mutuos guardados correctamente.", vbInformation, Me.Caption
        End If
      End If
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Function Fnt_Grabar_Precios_Accion() As Boolean
Dim lExcel As New Excel.Application
Dim lLibro As Excel.Workbook
Dim lHoja As Excel.Worksheet
'-----------------------------------
Dim lFila As Double
Dim lFecha As Date
Dim lNemotecnico As String
Dim lId_Nemotecnico As String
Dim lPrecio As Double
'-----------------------------------
Dim lcNemotecnicos As Class_Nemotecnicos
Dim lPublicadores_Precio As Class_Publicadores_Precio

On Error GoTo ErrProcedure
  
  Grilla.Rows = 1
  
  If Txt_ArchivoPlano.Text = "" Then
    MsgBox "Debe ingresar la ruta de la planilla Excel.", vbCritical, Me.Caption
    Exit Function
  End If
  
  lExcel.Visible = False
  Rem Abre el libro excel
  Set lLibro = lExcel.Workbooks.Open(Txt_ArchivoPlano.Text, ReadOnly:=False, Editable:=True)
  Rem abre la hoja del libro a procesar
  Set lHoja = lLibro.Worksheets(1)
  
  With lHoja
    Rem Setea la linea de inicio de recorrido del excel
    lFila = 2
    Call Fnt_Escribe_Grilla(Grilla, "N", "Inicio Proceso Ingreso Precio Acciones: " & Now & ".")
    Do While Not .Cells(lFila, eExcel_FFMM.eGD_rut_Cliente) = ""
            
      lFecha = Trim(NVL(.Cells(lFila, 1), ""))
      lNemotecnico = Trim(NVL(.Cells(lFila, 2), ""))
      lPrecio = Trim(NVL(.Cells(lFila, 3), 0))
      
      If (Not lNemotecnico = "") Then
        
        lId_Nemotecnico = ""
        
        Set lcNemotecnicos = New Class_Nemotecnicos
        lcNemotecnicos.Campo("nemotecnico").Valor = lNemotecnico
        If lcNemotecnicos.Buscar_Nemotecnico(gcPROD_RV_NAC, False) Then
          lId_Nemotecnico = NVL(lcNemotecnicos.Campo("id_nemotecnico").Valor, "")
          
          If Not lId_Nemotecnico = "0" Then
            Set lPublicadores_Precio = New Class_Publicadores_Precio
            With lPublicadores_Precio
              .Campo("Id_Publicador").Valor = 2 'BOLSA
              .Campo("Id_Nemotecnico").Valor = lId_Nemotecnico
              .Campo("Id_Moneda").Valor = 1 'PESO
              .Campo("Fecha").Valor = lFecha
              '.Campo("Tasa").Valor = GetCell(Grilla, lFila, "tasa")
              .Campo("Precio").Valor = lPrecio
              '.Campo("Duracion").Valor = GetCell(Grilla, lFila, "duracion")
              
              If Not .Guardar Then
                Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": " & .ErrMsg)
                GoTo ErrProcedure
              Else
                Call Fnt_Escribe_Grilla(Grilla, "N", "Fila " & lFila & ": Ingreso correcto del Valor Cuota: '" & lNemotecnico & "' " & lPrecio)
              End If
            End With
            Set lPublicadores_Precio = Nothing
          Else
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": Nemotecnico '" & lNemotecnico & "' no existe en el sistema.")
          End If
        Else
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": " & lcNemotecnicos.ErrMsg)
          GoTo ErrProcedure
        End If
        Set lcNemotecnicos = Nothing
            
      End If
      lFila = lFila + 1
    Loop
    Call Fnt_Escribe_Grilla(Grilla, "N", "T�rmino Proceso Ingreso Precio Acciones: " & Now & ".")
    
  End With
  
  MsgBox "Terminado el proceso de carga de la Planilla Excel", vbInformation, Me.Caption
  
ErrProcedure:
  If Not Err.Number = 0 Then
    If MsgBox("Error en la carga de planilla Excel." & vbCr & vbCr & Err.Description, vbApplicationModal + vbCritical + vbOKCancel, Me.Caption) = vbRetry Then
      Resume
    End If
  End If
  
  lExcel.DisplayAlerts = False
  lExcel.Quit
End Function

Private Function Fnt_GrabarValores_Cuotas_FFMM() As Boolean
Dim lExcel As New Excel.Application
Dim lLibro As Excel.Workbook
Dim lHoja As Excel.Worksheet
'-----------------------------------
Dim lFila As Double
Dim lFecha As Date
Dim lNemotecnico As String
Dim lId_Nemotecnico As String
Dim lValor_Cuota As Double
'-----------------------------------
Dim lcNemotecnicos As Class_Nemotecnicos
Dim lPublicadores_Precio As Class_Publicadores_Precio

On Error GoTo ErrProcedure
  
  Grilla.Rows = 1
  
  If Txt_ArchivoPlano.Text = "" Then
    MsgBox "Debe ingresar la ruta de la planilla Excel.", vbCritical, Me.Caption
    Exit Function
  End If
  
  lExcel.Visible = False
  Rem Abre el libro excel
  Set lLibro = lExcel.Workbooks.Open(Txt_ArchivoPlano.Text, ReadOnly:=False, Editable:=True)
  Rem abre la hoja del libro a procesar
  Set lHoja = lLibro.Worksheets(1)
  
  With lHoja
    Rem Setea la linea de inicio de recorrido del excel
    lFila = 2
    Call Fnt_Escribe_Grilla(Grilla, "N", "Inicio Proceso Ingreso Valores_Cuotas FFMM: " & Now & ".")
    Do While Not .Cells(lFila, eExcel_FFMM.eGD_rut_Cliente) = ""
            
      lFecha = Trim(NVL(.Cells(lFila, 1), ""))
      lNemotecnico = Trim(NVL(.Cells(lFila, 2), ""))
      lValor_Cuota = Trim(NVL(.Cells(lFila, 3), 0))
      
      If (Not lNemotecnico = "") Then
        
        lId_Nemotecnico = ""
        
        Set lcNemotecnicos = New Class_Nemotecnicos
        lcNemotecnicos.Campo("nemotecnico").Valor = lNemotecnico
        If lcNemotecnicos.Buscar_Nemotecnico(gcPROD_FFMM_NAC, False) Then
          lId_Nemotecnico = NVL(lcNemotecnicos.Campo("id_nemotecnico").Valor, "")
          
          If Not lId_Nemotecnico = "0" Then
            Set lPublicadores_Precio = New Class_Publicadores_Precio
            With lPublicadores_Precio
              .Campo("Id_Publicador").Valor = 1 'FFMM
              .Campo("Id_Nemotecnico").Valor = lId_Nemotecnico
              .Campo("Id_Moneda").Valor = 1 'PESO
              .Campo("Fecha").Valor = lFecha
              '.Campo("Tasa").Valor = GetCell(Grilla, lFila, "tasa")
              .Campo("Precio").Valor = lValor_Cuota
              '.Campo("Duracion").Valor = GetCell(Grilla, lFila, "duracion")
              
              If Not .Guardar Then
                Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": " & .ErrMsg)
                GoTo ErrProcedure
              Else
                Call Fnt_Escribe_Grilla(Grilla, "N", "Fila " & lFila & ": Ingreso correcto del Valor Cuota: '" & lNemotecnico & "' " & lValor_Cuota)
              End If
            End With
            Set lPublicadores_Precio = Nothing
          Else
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": Nemotecnico '" & lNemotecnico & "' no existe en el sistema.")
          End If
        Else
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": " & lcNemotecnicos.ErrMsg)
          GoTo ErrProcedure
        End If
        Set lcNemotecnicos = Nothing
            
      End If
      lFila = lFila + 1
    Loop
    Call Fnt_Escribe_Grilla(Grilla, "N", "T�rmino Proceso Ingreso Valores_Cuotas FFMM: " & Now & ".")
    
  End With
  
  MsgBox "Terminado el proceso de carga de la Planilla Excel", vbInformation, Me.Caption
  
ErrProcedure:
  If Not Err.Number = 0 Then
    If MsgBox("Error en la carga de planilla Excel." & vbCr & vbCr & Err.Description, vbApplicationModal + vbCritical + vbOKCancel, Me.Caption) = vbRetry Then
      Resume
    End If
  End If
  
  lExcel.DisplayAlerts = False
  lExcel.Quit
End Function

Private Function Fnt_Grabar() As Boolean
Dim lCursor As hRecord
Dim lReg As hFields
Dim lFila_Detalle As Long
Dim lSeguir As Boolean
Dim lId_Nemotecnico As String
Dim lFecha As Date
Dim lRut_Cliente As String
Dim lCuenta As String
Dim lNemotecnico As String
Dim lSaldo As Double
Dim lValor_Mercado As Double
Dim lId_Cuenta As String
Dim lId_Cliente As String
Dim lId_Moneda As String
Dim lId_Caja_Cuenta As String
Dim lPrecio As Double
'----------------------------
Dim lcCliente As Object
'Dim lcCuenta As Class_Cuentas
Dim lcCuenta As Object
Dim lcNemotecnicos As Class_Nemotecnicos
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
Dim lcAcciones As Class_Acciones

  Fnt_Grabar = True
  Set lCursor = Nothing
  Grilla.Rows = 1
  
  With fSaldos_Cust_RV
    If Not .Fnt_Cargar_Saldos_RF Then
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
    
    If .Cursor.Count = 0 Then
      MsgBox "No hay datos en el Archivo Seleccionado.", vbInformation, Me.Caption
      GoTo ErrProcedure
    End If
    
    Set lCursor = .Cursor
  End With
    
  '---------------------------------
  Rem Moneda Peso = 1
  lId_Moneda = 1
  '---------------------------------
  
  lFila_Detalle = 1
  Call Fnt_Escribe_Grilla(Grilla, "N", "Inicio Proceso Carga Saldos de Custodia de Renta Variable: " & Now & ".")
  For Each lReg In lCursor
    lSeguir = True
    lFecha = lReg("FECHA").Value
    lRut_Cliente = lReg("RUT_CLIENTE").Value
    lCuenta = lReg("CUENTA").Value
    lNemotecnico = lReg("NEMOTECNICO").Value
    lSaldo = lReg("SALDO").Value
    lValor_Mercado = lReg("VALOR_MERCADO").Value
    '------------------------------------------------------------------------------------------------------------------------------
    Rem Busca el ID_Cliente segun el Rut Cliente ingresado en el archivo
    Set lcCliente = Fnt_CreateObject(cDLL_Clientes)
    With lcCliente
      Set .gDB = gDB
      .Campo("rut_cliente").Valor = lRut_Cliente
      If .Buscar(True) Then
        Select Case .Cursor.Count
          Case 1
            lId_Cliente = NVL(.Cursor(1)("id_cliente").Value, "")
          Case 0
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": No existen clientes en CSGPI con el Rut Cliente " & lRut_Cliente & ".")
            lSeguir = False
          Case Is > 1
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": Existe m�s de un cliente en CSGPI con el Rut " & lRut_Cliente & ".")
            lSeguir = False
        End Select
      Else
        Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": " & .ErrMsg)
        lSeguir = False
      End If
    End With
    Set lcCliente = Nothing
    
    If lSeguir Then
      Rem Busca las cuentas asociadas al cliente, por Moneda Peso
'      Set lcCuenta = New Class_Cuentas
      Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
      With lcCuenta
        .Campo("id_cliente").Valor = lId_Cliente
        .Campo("id_moneda").Valor = lId_Moneda
        If .Buscar Then
          If .Cursor.Count > 0 Then
            lId_Cuenta = .Cursor(1)("id_cuenta").Value
          Else
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": El Rut Cliente " & lRut_Cliente & " no tiene cuentas asociadas en CSGPI o no tiene cuentas en Moneda Peso.")
            lSeguir = False
          End If
        Else
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": " & .ErrMsg)
          lSeguir = False
        End If
      End With
      Set lcCuenta = Nothing
    End If
    '------------------------------------------------------------------------------------------------------------------------------
    Rem Busca el id_nemotecnico del Nemotecnico
    Set lcNemotecnicos = New Class_Nemotecnicos
    lcNemotecnicos.Campo("nemotecnico").Valor = lNemotecnico
    If lcNemotecnicos.Buscar_Nemotecnico(pCod_Producto:="", pMostrar_Msg:=False) Then
      lId_Nemotecnico = lcNemotecnicos.Campo("id_nemotecnico").Valor
      If lId_Nemotecnico = "0" Then
        Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": Nemot�cnico " & lNemotecnico & " no existe o est� vencido.")
        lSeguir = False
      End If
    Else
      Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": " & lcNemotecnicos.ErrMsg)
      lSeguir = False
    End If
    Set lcNemotecnicos = Nothing
    '------------------------------------------------------------------------------------------------------------------------------
    lId_Caja_Cuenta = ""
    Rem Busca la caja para la cuenta
    If lSeguir Then
      Set lcCaja_Cuenta = New Class_Cajas_Cuenta
      With lcCaja_Cuenta
        .Campo("id_cuenta").Valor = lId_Cuenta
        .Campo("id_moneda").Valor = lId_Moneda
        .Campo("cod_mercado").Valor = fc_Mercado
        If .Buscar_Caja_Para_Invertir(lId_Nemotecnico) Then
          If .Cursor.Count > 0 Then
            lId_Caja_Cuenta = NVL(.Cursor(1)("id_caja_cuenta").Value, "")
          End If
        Else
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": " & lcCaja_Cuenta.ErrMsg)
          lSeguir = False
        End If
      End With
      Set lcCaja_Cuenta = Nothing
      
      If lId_Caja_Cuenta = "" Then
        Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": No existe una caja para la cuenta del Rut Cliente " & lRut_Cliente & ".")
        lSeguir = False
      End If
    End If
    '------------------------------------------------------------------------------------------------------------------------------
    If lSeguir Then
      lPrecio = Fnt_Divide(lValor_Mercado, lSaldo)
      
      Set lcAcciones = New Class_Acciones
      With lcAcciones
        Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lId_Nemotecnico, _
                                          pCantidad:=lSaldo, _
                                          pPrecio:=lPrecio, _
                                          pId_Moneda:=lId_Moneda, _
                                          pMonto:=lValor_Mercado, _
                                          pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                          pPrecio_Historico:="")
        
        If Not .Realiza_Operacion_Custodia(pId_Operacion:=cNewEntidad, _
                                           pId_Cuenta:=lId_Cuenta, _
                                           pDsc_Operacion:="", _
                                           pTipoOperacion:=gcTipoOperacion_Ingreso, _
                                           pId_Contraparte:="", _
                                           pId_Representante:="", _
                                           pId_Moneda_Operacion:=lId_Moneda, _
                                           pFecha_Operacion:=lFecha, _
                                           pFecha_Vigencia:=lFecha, _
                                           pFecha_Liquidacion:=lFecha, _
                                           pId_Trader:="", _
                                           pPorc_Comision:=0, _
                                           pComision:=0, _
                                           pDerechos_Bolsa:=0, _
                                           pGastos:=0, _
                                           pIva:=0, _
                                           pMonto_Operacion:=lValor_Mercado, _
                                           pTipo_Precio:=cTipo_Precio_Mercado, _
                                           pId_Caja_Cuenta:=lId_Caja_Cuenta) Then
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila_Detalle & ": " & .ErrMsg)
          GoTo ErrProcedure
          Fnt_Grabar = False
        Else
          Call Fnt_Escribe_Grilla(Grilla, "N", "Fila " & lFila_Detalle & ": Ingreso correcto de la Operaci�n de Custodia.")
        End If
      End With
      
    End If
    
    lFila_Detalle = lFila_Detalle + 1
  Next
  Call Fnt_Escribe_Grilla(Grilla, "N", "T�rmino Proceso Carga Saldos de Custodia de Renta Variable: " & Now & ".")
  
ErrProcedure:
  Set lcCliente = Nothing
  Set lcCuenta = Nothing
  Set lcNemotecnicos = Nothing
  Set lcCaja_Cuenta = Nothing
  Set lcAcciones = Nothing
End Function

Private Sub Toolbar_Detalle_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "DETALLE"
      Call Grilla_DblClick
  End Select
End Sub

Private Function Fnt_Grabar_FFMM() As Boolean
Dim lcAlias As Object
'-----------------------------------
Dim lExcel As New Excel.Application
Dim lLibro As Excel.Workbook
Dim lHoja As Excel.Worksheet
'-----------------------------------
Dim lFila As Double
Dim lRut_Cliente As String
Dim lId_Cuenta As String
Dim lNombre_Cliente As String
Dim lCod_Fondo As String
Dim lNombre_Fondo As String
Dim lFecha_Movimiento As Date
Dim lFecha_Liquidacion As Date
Dim lFecha_Operativa As Date
Dim lTipo_Operacion As String
Dim lNro_Operacion As String
Dim lMonto As Double
Dim lValor_Cuota As Double
Dim lNro_Cuotas As Double
Dim lSaldo_Cuotas As Double
Dim lTipo_Mov As String
Dim lId_Moneda As String
'-----------------------------------
Dim lcNemotecnicos As Class_Nemotecnicos
Dim lId_Nemotecnico As String
Dim lCod_Instrumento As String
Dim lcFondos As Class_FondosMutuos
Dim lId_Caja_Cuenta As String
Dim lcCaja_Cuenta As Class_Cajas_Cuenta

On Error GoTo ErrProcedure
  
  Grilla.Rows = 1
  
  If Txt_ArchivoPlano.Text = "" Then
    MsgBox "Debe ingresar la ruta de la planilla Excel.", vbCritical, Me.Caption
    Exit Function
  End If
  
  lExcel.Visible = False
  Rem Abre el libro excel
  Set lLibro = lExcel.Workbooks.Open(Txt_ArchivoPlano.Text, ReadOnly:=False, Editable:=True)
  Rem abre la hoja del libro a procesar
  Set lHoja = lLibro.Worksheets(1)
  
  Rem La Moneda es Peso Nacional = 1
  lId_Moneda = 1
  
  With lHoja
    Rem Setea la linea de inicio de recorrido del excel
    lFila = 2
    Call Fnt_Escribe_Grilla(Grilla, "N", "Inicio Proceso Ingreso Saldos Custodia FFMM: " & Now & ".")
    Do While Not .Cells(lFila, eExcel_FFMM.eGD_rut_Cliente) = ""
            
      lRut_Cliente = Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_rut_Cliente), ""))
      lCod_Fondo = Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Codigo_Fondo), ""))
      
      Set lcAlias = Fnt_CreateObject(cDLL_Alias)
      Set lcAlias.gDB = gDB
      Rem Busca el alias del Rut del Cliente
      lId_Cuenta = NVL(lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                               , pCodigoCSBPI:=cTabla_Cuentas _
                                               , pValor:=lRut_Cliente), "")
      Rem Busca el alias del C�digo del Fondo Mutuo
      lId_Nemotecnico = NVL(lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_Sec_Planes _
                                                    , pCodigoCSBPI:=cTabla_Nemotecnicos _
                                                    , pValor:=lCod_Fondo), "")
      Set lcAlias = Nothing
      
      Rem Si el Id_Cuenta o Id_Nemotecnico es vacio no hace la operacion
      If (Not lId_Cuenta = "") And (Not lId_Nemotecnico = "") Then
        
        Rem Rescata los otros datos de la planilla
        lSaldo_Cuotas = To_Number(Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Saldo_Cuota), 0)))
        lFecha_Movimiento = Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Fecha), ""))
        lValor_Cuota = To_Number(Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Valor_Cuota), 0)))
        lMonto = To_Number(Trim(NVL(.Cells(lFila, eExcel_FFMM.eGD_Saldo_Moneda), 0)))
        
        Rem Busca atributos del Nemotecnico
        Set lcNemotecnicos = New Class_Nemotecnicos
        lcNemotecnicos.Campo("id_nemotecnico").Valor = lId_Nemotecnico
        If lcNemotecnicos.Buscar Then
          lCod_Instrumento = NVL(lcNemotecnicos.Cursor(1)("cod_instrumento").Value, "")
        Else
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": " & lcNemotecnicos.ErrMsg)
        End If
        Set lcNemotecnicos = Nothing
            
        lId_Caja_Cuenta = ""
        Set lcCaja_Cuenta = New Class_Cajas_Cuenta
        With lcCaja_Cuenta
          .Campo("id_cuenta").Valor = lId_Cuenta
          .Campo("id_moneda").Valor = lId_Moneda
          .Campo("cod_mercado").Valor = fc_Mercado
          If .Buscar_Caja_Para_Invertir(lId_Nemotecnico) Then
            If .Cursor.Count > 0 Then
              lId_Caja_Cuenta = NVL(.Cursor(1)("id_caja_cuenta").Value, "")
            End If
          Else
            Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": " & lcCaja_Cuenta.ErrMsg)
            GoTo ErrProcedure
          End If
        End With
        Set lcCaja_Cuenta = Nothing
            
        Set lcFondos = New Class_FondosMutuos
        Call lcFondos.Agregar_Operaciones_Detalle(pId_Nemotecnico:=lId_Nemotecnico, _
                                                  pcuota:=lSaldo_Cuotas, _
                                                  pPrecio:=lValor_Cuota, _
                                                  pId_Moneda:=lId_Moneda, _
                                                  pMonto:=lMonto, _
                                                  pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                                  pFecha_Liquidacion:=lFecha_Movimiento, _
                                                  pPrecio_Historico:="")
                                                   
        If Not lcFondos.Realiza_Operacion_Custodia(pId_Operacion:=cNewEntidad, _
                                                 pId_Cuenta:=lId_Cuenta, _
                                                 pDsc_Operacion:="", _
                                                 pTipoOperacion:=gcTipoOperacion_Ingreso, _
                                                 pId_Contraparte:="", _
                                                 pId_Representante:="", _
                                                 pId_Moneda_Operacion:=lId_Moneda, _
                                                 pFecha_Operacion:=lFecha_Movimiento, _
                                                 pFecha_Vigencia:=lFecha_Movimiento, _
                                                 pFecha_Liquidacion:=lFecha_Movimiento, _
                                                 pId_Trader:="", _
                                                 pPorc_Comision:=0, _
                                                 pComision:=0, _
                                                 pDerechos_Bolsa:=0, _
                                                 pGastos:=0, _
                                                 pIva:=0, _
                                                 pMonto_Operacion:=lMonto, _
                                                 pTipo_Precio:=cTipo_Precio_Mercado, _
                                                 pId_Caja_Cuenta:=lId_Caja_Cuenta, _
                                                 pCod_Instrumento:=lCod_Instrumento) Then
          
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": " & lcFondos.ErrMsg)
          GoTo ErrProcedure
        Else
          Call Fnt_Escribe_Grilla(Grilla, "N", "Fila " & lFila & ": Ingreso correcto de la Operaci�n de Custodia.")
        End If
        
      Else
        If lId_Cuenta = "" Then
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": El Rut " & lRut_Cliente & " no existe en el sistema.")
        ElseIf lId_Nemotecnico = "" Then
          Call Fnt_Escribe_Grilla(Grilla, "E", "Fila " & lFila & ": El Alias del C�digo " & lCod_Fondo & " no est� asociado a ningun Nemot�cnico.")
        End If
      End If

      lFila = lFila + 1
    Loop
    Call Fnt_Escribe_Grilla(Grilla, "N", "T�rmino Proceso Ingreso Saldos Custodia FFMM: " & Now & ".")
    
  End With
  
  MsgBox "Terminado el proceso de carga de la Planilla Excel", vbInformation, Me.Caption
  
ErrProcedure:
  If Not Err.Number = 0 Then
    If MsgBox("Error en la carga de planilla Excel." & vbCr & vbCr & Err.Description, vbApplicationModal + vbCritical + vbOKCancel, Me.Caption) = vbRetry Then
      Resume
    End If
  End If
  
  lExcel.DisplayAlerts = False
  lExcel.Quit
End Function
