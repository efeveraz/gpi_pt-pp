VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Nemotecnico_C 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte Nemotecnico"
   ClientHeight    =   1740
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8880
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1740
   ScaleWidth      =   8880
   Begin VB.Frame Frame1 
      Caption         =   "Datos de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1275
      Left            =   30
      TabIndex        =   2
      Top             =   420
      Width           =   8775
      Begin MSComctlLib.Toolbar Toolbar_Verificacion 
         Height          =   330
         Left            =   6960
         TabIndex        =   3
         Top             =   690
         Width           =   1650
         _ExtentX        =   2910
         _ExtentY        =   582
         ButtonWidth     =   2858
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Generar Reporte"
               Key             =   "REPORTE"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
               Object.ToolTipText     =   "Genera el Reporte de Instruccones"
            EndProperty
         EndProperty
      End
      Begin TrueDBList80.TDBCombo Cmb_Nemotecnico 
         Height          =   345
         Left            =   1590
         TabIndex        =   4
         Tag             =   "OBLI=S;CAPTION=Nemot�cnico"
         Top             =   720
         Width           =   4785
         _ExtentX        =   8440
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   -1  'True
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Nemotecnico_C.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Instrumento 
         Height          =   345
         Left            =   1590
         TabIndex        =   6
         Tag             =   "OBLI=S;CAPTION=Instrumento"
         Top             =   330
         Width           =   4785
         _ExtentX        =   8440
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Nemotecnico_C.frx":00AA
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Label8 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Instrumento"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   150
         TabIndex        =   7
         Top             =   330
         Width           =   1425
      End
      Begin VB.Label Lbl_Nemotecnico 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Nemot�cnico"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   150
         TabIndex        =   5
         Top             =   720
         Width           =   1425
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8880
      _ExtentX        =   15663
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Nemotecnico_C"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Const RColorRelleno = 213   ' R
Const GColorRelleno = 202   ' G
Const BColorRelleno = 195   ' B

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Refrescar
  End With
  
  With Toolbar_Verificacion
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("REPORTE").Image = cBoton_Modificar
  End With
  
 Call Sub_CargaForm
  


End Sub


Private Sub Sub_CargaForm()
Dim lReg As hFields
  Call Sub_CargaCombo_Instrumentos(Cmb_Instrumento, "")
  'Call Sub_CargaCombo_Nemotecnicos(Cmb_Nemotecnico)

End Sub

Private Sub Cmb_Instrumento_ItemChange()
Dim lCod_Instrumento As String
  lCod_Instrumento = Fnt_FindValue4Display(Cmb_Instrumento, Cmb_Instrumento.Text)
  Call Sub_CargaCombo_Nemotecnicos(Cmb_Nemotecnico, lCod_Instrumento)
  
End Sub


Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
     ' Call Sub_Limpiar
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_Verificacion_ButtonClick(ByVal Button As MSComctlLib.Button)

Dim lNemotecnico As Integer
lNemotecnico = Fnt_FindValue4Display(Cmb_Nemotecnico, Cmb_Nemotecnico.Text)
'MsgBox lNemotecnico
Call Sub_Generar_Reporte(lNemotecnico)

End Sub

Private Sub Sub_Generar_Reporte(lNemotecnico As Integer)
Const clrHeader = &HD0D0D0

Dim sRecord
Dim bAppend
Dim lLinea As Integer
'------------------------------------------
Dim lC_Nemotecnicos As Class_Nemotecnicos
Dim lCursor_Nemo As hRecord
Dim lReg_Nemo As hFields
Dim lc_Clasificador_Riesgo As Class_Clasificadores_Riesgo
Dim lCursor_CR_Nemo As hRecord
Dim lReg_CR_Nemo As hFields
Dim iFila As Integer
Dim a As Integer

Dim lForm As Frm_Reporte_Generico

  Call Sub_Bloquea_Puntero(Me)
  
  
  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo ErrProcedure
  End If
  
  Set lC_Nemotecnicos = New Class_Nemotecnicos
  With lC_Nemotecnicos
    .Campo("id_nemotecnico").Valor = lNemotecnico
    If .Buscar Then
      Set lCursor_Nemo = .Cursor
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With

  
  
  Rem Comienzo de la generaci�n del reporte
  Set lForm = New Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="" _
                               , pTipoSalida:=ePrinter.eP_Pantalla _
                               , pOrientacion:=orPortrait)
     
  With lForm.VsPrinter
      
      
      '.TableCell(tcRows) = IIf(lCursor_Cta.Count > 0, lCursor_Cta.Count, 0)
      For Each lReg_Nemo In lCursor_Nemo
        
        .FontSize = "12"
        .FontBold = True
        .Paragraph = UCase("Detalle del Nemot�cnico")
        .FontSize = "10"
        .FontBold = False
        .DrawLine .MarginLeft, .CurrentY, .PageWidth - .MarginRight, .CurrentY
        .Paragraph = UCase("Informaci�n del Nemot�cnico ")
        .DrawLine .MarginLeft, .CurrentY, .PageWidth - .MarginRight, .CurrentY
        .Paragraph = ""
        .StartTable
        .TableCell(tcRows) = 1
        .TableCell(tcCols) = 3
        .TableCell(tcColWidth, , 1) = "60mm"
        .TableCell(tcColWidth, , 2) = "5mm"
        .TableCell(tcColWidth, , 3) = "120mm"
        
        .TableCell(tcFontSize) = "9"
        .TableBorder = tbNone
        
        '.TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Instrumento"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = UCase(Devuelve_Instrumento(lReg_Nemo("cod_instrumento").Value))
        
        If lReg_Nemo("cod_instrumento").Value <> "ACCIONES_NAC" Then
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Sub Familia"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = UCase(Devuelve_Subfamilia(NVL(lReg_Nemo("id_subfamilia").Value, 0)))
        End If
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Nemot�cnico"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = UCase(lReg_Nemo("nemotecnico").Value)
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Descripci�n"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = UCase(NVL(lReg_Nemo("dsc_nemotecnico").Value, ""))
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Pa�s"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = UCase(Devuelve_Pais(NVL(lReg_Nemo("cod_pais").Value, "")))
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Emisor"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = UCase(NVL(Devuelve_Emisor_Especifico(lReg_Nemo("id_emisor_especifico").Value), ""))
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Moneda Origen"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = Devuelve_Moneda(NVL(lReg_Nemo("id_moneda").Value, 0))
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Moneda Transacci�n"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = UCase(Devuelve_Moneda(NVL(lReg_Nemo("id_moneda_transaccion").Value, 0)))
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Mercado Transacci�n"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = UCase(Devuelve_Mercado_Transaccion(NVL(lReg_Nemo("id_mercado_transaccion").Value, 0)))
         
        If lReg_Nemo("cod_instrumento").Value = "FMNAC_RV" Or lReg_Nemo("cod_instrumento").Value = "FFMM_EXT" Or lReg_Nemo("cod_instrumento").Value = "FMNAC_RV" Then
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "D�as de Valuta"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = NVL(lReg_Nemo("dias_liquidez").Value, "-")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Modalidad de Inversi�n"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = UCase(Devuelve_Modalidad(NVL(lReg_Nemo("flg_tipo_cuota_ingreso").Value, "")))
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Modalidad de Rescate"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = UCase(Devuelve_Modalidad(NVL(lReg_Nemo("flg_tipo_cuota_egreso").Value, "")))
        
        ElseIf lReg_Nemo("cod_instrumento").Value <> "FMNAC_RV" And lReg_Nemo("cod_instrumento").Value <> "FFMM_EXT" And lReg_Nemo("cod_instrumento").Value <> "FMNAC_RV" And lReg_Nemo("cod_instrumento").Value <> "ACCIONES_NAC" Then
        
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        
        .TableCell(tcText, a, 1) = "Tasa Emisi�n"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = NVL(lReg_Nemo("tasa_emision").Value, "-")
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Corte de Cup�n"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = NVL(lReg_Nemo("corte_minimo_papel").Value, "-")
        
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Fecha Emisi�n"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = NVL(lReg_Nemo("fecha_emision").Value, "-")
       
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Fecha Vencimiento"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = NVL(lReg_Nemo("fecha_vencimiento").Value, "-")
        
        End If
        .TableCell(tcFontBold, 1, 1, a, 3) = False
        
        .TableCell(tcRows) = .TableCell(tcRows) + 1
        
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Fecha Creaci�n"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = NVL(CDate(lReg_Nemo("fch_insert").Value), "No Registra")
         .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Usuario Creaci�n"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = Devuelve_Usuario(NVL(lReg_Nemo("id_usuario_insert").Value, 0))
         .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Fecha �ltima Modificaci�n"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = NVL(lReg_Nemo("fch_update").Value, "No Registra")
         .TableCell(tcRows) = .TableCell(tcRows) + 1
        a = .TableCell(tcRows)
        .TableCell(tcText, a, 1) = "Usuario �ltima Modificaci�n"
        .TableCell(tcText, a, 2) = ":"
        .TableCell(tcText, a, 3) = Devuelve_Usuario(NVL(lReg_Nemo("id_usuario_update").Value, 0))
     
        
       .EndTable
  
        .Paragraph = ""
        .DrawLine .MarginLeft, .CurrentY, .PageWidth - .MarginRight, .CurrentY
        .Paragraph = UCase("Clasificaciones de Riesgo")
        .DrawLine .MarginLeft, .CurrentY, .PageWidth - .MarginRight, .CurrentY
        
        .Paragraph = ""
        .StartTable
              .TableBorder = tbAll
              .TableCell(tcRows) = 1
               .TableCell(tcCols) = 2
              .TableCell(tcText, 1, 1) = "Clasificador"
              .TableCell(tcText, 1, 2) = "Clasificaci�n"
              '.TableCell(tcText, 1, 3) = "Mercado"
              .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
              Rem Busca las cajas de la cuenta
              
              .TableCell(tcColWidth, , 1) = "60mm"
              .TableCell(tcColWidth, , 2) = "50mm"
              '.TableCell(tcColWidth, , 3) = "50mm"
         Set lc_Clasificador_Riesgo = New Class_Clasificadores_Riesgo
         
          'With lc_Clasificador_Riesgo
              
              If lc_Clasificador_Riesgo.Buscar_Clasif_Riesgo_Nemo(lNemotecnico) Then
              
                Set lCursor_CR_Nemo = lc_Clasificador_Riesgo.Cursor
                For Each lReg_CR_Nemo In lCursor_CR_Nemo
                    .TableCell(tcRows) = .TableCell(tcRows) + 1
                    iFila = .TableCell(tcRows)
                    .TableCell(tcText, iFila, 1) = lReg_CR_Nemo("dsc_clasificador_riesgo").Value
                    .TableCell(tcText, iFila, 2) = lReg_CR_Nemo("cod_valor_clasificacion").Value
                Next
              End If
              
            
          'End With
          
              
              
              
              
              
              
              
              
              
              
              
        .EndTable
        
'          .StartTable
'              .TableBorder = tbAll
'              .TableCell(tcRows) = 1
'               .TableCell(tcCols) = 3
'              .TableCell(tcText, 1, 1) = "Descripci�n"
'              .TableCell(tcText, 1, 2) = "Moneda"
'              .TableCell(tcText, 1, 3) = "Mercado"
'              .TableCell(tcBackColor, 1) = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
'              Rem Busca las cajas de la cuenta
'              Set lAlias_Ctas = New Class_Alias
'
'
'              lAlias_Ctas.Campo("id_cuenta").Valor = lReg_Cta("id_cuenta").Value
'              If lCajas_Ctas.Buscar(True) Then
'
'              Set lCursor_Alias = lAlias_Ctas.Cursor
'              .TableCell(tcRows) = .TableCell(tcRows) + lCursor_Alias.Count
'
'                iFila = 2
'                For Each lReg_Alias In lCursor_Alias
'
'                    .TableCell(tcText, iFila, 1) = lReg_Alias("dsc_caja_cuenta").Value
'                    .TableCell(tcText, iFila, 2) = lReg_Alias("dsc_moneda").Value
'                    .TableCell(tcText, iFila, 3) = lReg_Alias("desc_mercado").Value
'                    iFila = iFila + 1
'                Next
'              Else
'              MsgBox lAlias_Ctas.ErrMsg, vbCritical, Me.Caption
'              GoTo ErrProcedure
'              End If
'              .TableCell(tcColWidth, , 1) = "60mm"
'              .TableCell(tcColWidth, , 2) = "50mm"
'              .TableCell(tcColWidth, , 3) = "50mm"
'        .EndTable
        
        
      Next
      
      

    'Else
    '  .Paragraph = "No hay Cuentas Vigentes en el sistema."
    'End If
    
    .EndDoc
  End With
  
  Call ColocarEn(lForm, Me, eFP_Abajo)
  
ErrProcedure:
  Call Sub_Desbloquea_Puntero(Me)
  
End Sub

Function Devuelve_Emisor_Especifico(lId_Emisor As Double)
Dim lC_Emisores As Class_Emisores_Especifico
Dim lCursor_Emisor As hRecord
Dim lReg_Emisor As hFields
Set lC_Emisores = New Class_Emisores_Especifico
  With lC_Emisores
    .Campo("id_Emisor_Especifico").Valor = lId_Emisor
    If .Buscar Then
      Set lCursor_Emisor = .Cursor
      For Each lReg_Emisor In lCursor_Emisor
         Devuelve_Emisor_Especifico = NVL(lReg_Emisor("dsc_emisor_especifico").Value, "")
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      'GoTo ErrProcedure
    End If
  End With
End Function


Function Devuelve_Instrumento(lCod_Instrumento As String)
Dim lC_Instrumento As Class_Instrumentos
Dim lCursor_Instrumento As hRecord
Dim lReg_Instrumento As hFields
Set lC_Instrumento = New Class_Instrumentos
  With lC_Instrumento
    .Campo("cod_instrumento").Valor = lCod_Instrumento
    If .Buscar Then
      Set lCursor_Instrumento = .Cursor
      For Each lReg_Instrumento In lCursor_Instrumento
         Devuelve_Instrumento = NVL(lReg_Instrumento("dsc_intrumento").Value, "")
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      'GoTo ErrProcedure
    End If
  End With
End Function

Function Devuelve_Subfamilia(lId_Subfamilia As Double)
Dim lC_Subfamilia As Class_SubFamilias
Dim lCursor_Subfamilia As hRecord
Dim lReg_Subfamilia As hFields
Set lC_Subfamilia = New Class_SubFamilias
  With lC_Subfamilia
    .Campo("id_subfamilia").Valor = lId_Subfamilia
    If .Buscar Then
      Set lCursor_Subfamilia = .Cursor
      For Each lReg_Subfamilia In lCursor_Subfamilia
         Devuelve_Subfamilia = NVL(lReg_Subfamilia("dsc_subfamilia").Value, "")
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      'GoTo ErrProcedure
    End If
  End With
End Function

Function Devuelve_Pais(lCod_Pais As String)
Dim lC_Pais As Class_Paises
Dim lCursor_Paises As hRecord
Dim lReg_Paises As hFields
Set lC_Pais = New Class_Paises
  With lC_Pais
    .Campo("cod_pais").Valor = lCod_Pais
    If .Buscar Then
      Set lCursor_Paises = .Cursor
      For Each lReg_Paises In lCursor_Paises
         Devuelve_Pais = NVL(lReg_Paises("dsc_pais").Value, "")
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      'GoTo ErrProcedure
    End If
  End With
End Function

Function Devuelve_Moneda(lId_Moneda As String)
'Dim lC_Moneda As Class_Monedas
Dim lcMoneda As Object
Dim lCursor_Monedas As hRecord
Dim lReg_Monedas As hFields
Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
  With lcMoneda
    .Campo("id_moneda").Valor = lId_Moneda
    If .Buscar Then
      Set lCursor_Monedas = .Cursor
      For Each lReg_Monedas In lCursor_Monedas
         Devuelve_Moneda = NVL(lReg_Monedas("dsc_moneda").Value, "")
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      'GoTo ErrProcedure
    End If
  End With
End Function

Function Devuelve_Mercado_Transaccion(lid_Mercado_Transaccion As String)
Dim lC_Mercado_Transaccion As Class_Mercados_Transaccion
Dim lCursor_Mercado_Transaccion As hRecord
Dim lReg_Mercado_Transaccion As hFields
Set lC_Mercado_Transaccion = New Class_Mercados_Transaccion
  With lC_Mercado_Transaccion
    .Campo("id_mercado_transaccion").Valor = lid_Mercado_Transaccion
    If .Buscar Then
      Set lCursor_Mercado_Transaccion = .Cursor
      For Each lReg_Mercado_Transaccion In lCursor_Mercado_Transaccion
         Devuelve_Mercado_Transaccion = NVL(lReg_Mercado_Transaccion("dsc_mercado_transaccion").Value, "")
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      'GoTo ErrProcedure
    End If
  End With
End Function


Function Devuelve_Usuario(lid_usuario As Integer)
If lid_usuario = 0 Then
  Devuelve_Usuario = "No Registra"
Else
Dim lC_Usuario As Class_Usuarios
Dim lCursor_Usuario As hRecord
Dim lReg_Usuario As hFields
Set lC_Usuario = New Class_Usuarios
  With lC_Usuario
    .Campo("id_usuario").Valor = lid_usuario
    If .Buscar Then
      Set lCursor_Usuario = .Cursor
      For Each lReg_Usuario In lCursor_Usuario
         Devuelve_Usuario = NVL(lReg_Usuario("dsc_usuario").Value, "")
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      'GoTo ErrProcedure
    End If
  End With
End If
End Function

Function Devuelve_Modalidad(Modalidad As String)

Select Case Modalidad
  Case "C"
    Devuelve_Modalidad = "Conocida"
  Case "D"
    Devuelve_Modalidad = "Desconocida"
  Case Else
    Devuelve_Modalidad = "-"
End Select
End Function

