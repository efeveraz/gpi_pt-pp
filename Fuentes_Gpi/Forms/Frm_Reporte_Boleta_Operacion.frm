VERSION 5.00
Object = "{54850C51-14EA-4470-A5E4-8C5DB32DC853}#1.0#0"; "vsprint8.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Boleta_Operacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte Comprobante Operaci�n"
   ClientHeight    =   1635
   ClientLeft      =   165
   ClientTop       =   330
   ClientWidth     =   7065
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1635
   ScaleWidth      =   7065
   Begin VSPrinter8LibCtl.VSPrinter vsprinter1 
      Height          =   555
      Left            =   2760
      TabIndex        =   8
      Top             =   1890
      Width           =   555
      _cx             =   979
      _cy             =   979
      Appearance      =   1
      BorderStyle     =   1
      Enabled         =   -1  'True
      MousePointer    =   0
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AutoRTF         =   -1  'True
      Preview         =   -1  'True
      DefaultDevice   =   0   'False
      PhysicalPage    =   -1  'True
      AbortWindow     =   -1  'True
      AbortWindowPos  =   0
      AbortCaption    =   "Printing..."
      AbortTextButton =   "Cancel"
      AbortTextDevice =   "on the %s on %s"
      AbortTextPage   =   "Now printing Page %d of"
      FileName        =   ""
      MarginLeft      =   1440
      MarginTop       =   1440
      MarginRight     =   1440
      MarginBottom    =   1440
      MarginHeader    =   0
      MarginFooter    =   0
      IndentLeft      =   0
      IndentRight     =   0
      IndentFirst     =   0
      IndentTab       =   720
      SpaceBefore     =   0
      SpaceAfter      =   0
      LineSpacing     =   100
      Columns         =   1
      ColumnSpacing   =   180
      ShowGuides      =   2
      LargeChangeHorz =   300
      LargeChangeVert =   300
      SmallChangeHorz =   30
      SmallChangeVert =   30
      Track           =   0   'False
      ProportionalBars=   -1  'True
      Zoom            =   -1.51515151515152
      ZoomMode        =   3
      ZoomMax         =   400
      ZoomMin         =   10
      ZoomStep        =   25
      EmptyColor      =   -2147483636
      TextColor       =   0
      HdrColor        =   0
      BrushColor      =   0
      BrushStyle      =   0
      PenColor        =   0
      PenStyle        =   0
      PenWidth        =   0
      PageBorder      =   0
      Header          =   ""
      Footer          =   ""
      TableSep        =   "|;"
      TableBorder     =   7
      TablePen        =   0
      TablePenLR      =   0
      TablePenTB      =   0
      NavBar          =   3
      NavBarColor     =   -2147483633
      ExportFormat    =   0
      URL             =   ""
      Navigation      =   3
      NavBarMenuText  =   "Whole &Page|Page &Width|&Two Pages|Thumb&nail"
      AutoLinkNavigate=   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   9
   End
   Begin VB.Frame Frame1 
      Caption         =   "Generador de Comprobante de Operaciones"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1185
      Left            =   60
      TabIndex        =   0
      Top             =   390
      Width           =   6945
      Begin MSComctlLib.Toolbar Toolbar_Verificacion 
         Height          =   330
         Left            =   5160
         TabIndex        =   1
         Top             =   270
         Width           =   1650
         _ExtentX        =   2910
         _ExtentY        =   582
         ButtonWidth     =   2858
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Generar Reporte"
               Key             =   "REPORTE"
               Description     =   "Valoriza el nemot�cnico a la tasa de inversi�n."
               Object.ToolTipText     =   "Genera el Reporte de Cargos y Abonos"
            EndProperty
         EndProperty
      End
      Begin TrueDBList80.TDBCombo Cmb_Operaciones 
         Height          =   345
         Left            =   1200
         TabIndex        =   5
         Tag             =   "OBLI=S;CAPTION=Operaciones"
         Top             =   720
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Boleta_Operacion.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin TrueDBList80.TDBCombo Cmb_Cuentas 
         Height          =   345
         Left            =   1200
         TabIndex        =   6
         Tag             =   "OBLI=S;CAPTION=Cuenta"
         Top             =   330
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Boleta_Operacion.frx":00AA
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Cuentas"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   7
         Top             =   330
         Width           =   1035
      End
      Begin VB.Label Label2 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Operaciones"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   1035
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   7065
      _ExtentX        =   12462
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   4
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Reporte_Boleta_Operacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Type datos_cliente
    Cuenta As String
    Nombre As String
    Rut As String
    Ejecutivo As String
End Type
Private Type datos_operacion
    Moneda_fondo As String
    Fecha_Operacion As Date
    Fecha_Vencimiento As Date
    Tipo_Operacion As String
    Mercado As String
    Por_Rueda As String
    Moneda_Operacion As String
    Cta_Operacion As String
    contraparte As String
    Cond_Liquidacion As String
    Monto_Operacion As String
    Iva As String
    Comision As String
    Por_Comision As String
    Derechos As String
    Gastos As String
    Otros As String
End Type
Dim Iid_Cuenta As String
Dim IId_Operacion As String

Dim glb_cliente As datos_cliente
Dim glb_operacion As datos_operacion


Private Sub Cmb_Cuentas_ItemChange()
Dim lId_Cuenta As String
'Dim Num_Cuenta As Long  -- Modificado CSM 23/06/2006 --
   
  Call Sub_Bloquea_Puntero(Me)
   
  lId_Cuenta = Fnt_ComboSelected_KEY(Cmb_Cuentas)
   
  If Not lId_Cuenta = cCmbKALL Then
    Call Sub_CargaCombo_Operaciones(Cmb_Operaciones, lId_Cuenta) ', Mid(pCombo.Text, 1, InStr(pCombo.Text, "-") - 1))  -- Modificado CSM 23/06/2006 --
  Else
    'Call Sub_CargaCombo_Operaciones(Cmb_Operaciones)
  End If

  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Refrescar
  End With
  
  With Toolbar_Verificacion
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("REPORTE").Image = cBoton_Modificar
  End With
  
  Call Carga_Combo
     
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Carga_Combo()
  Call Sub_CargaCombo_Cuentas_Vigentes(Cmb_Cuentas)
  'Call Sub_CargaCombo_Operaciones(Cmb_Operaciones)
End Sub


Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_Verificacion_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
  
  Select Case Button.Key
    Case "REPORTE"
      Call Sub_Generar_Comprobante
  End Select
End Sub

Private Sub Sub_Generar_Comprobante()
Dim IId_Operacion As String
Dim lFormulario As Object

  If Not Fnt_Form_Validar(Me.Controls) Then
    GoTo Errprocedure
  End If
  
  IId_Operacion = IIf(Fnt_ComboSelected_KEY(Cmb_Operaciones) = cCmbKALL, "", Fnt_ComboSelected_KEY(Cmb_Operaciones))
  
  Set lFormulario = Fnt_Generar_Comprobante(IId_Operacion, pTipoSalida:=ePrinter.eP_Pantalla)
  If Not lFormulario Is Nothing Then
    Call ColocarEn(lFormulario, Me, eFP_Abajo)
  End If
  
Errprocedure:

End Sub

Private Function Fnt_Generar_Comprobante(pId_Operacion As String _
                                        , ByVal pTipoSalida As ePrinter _
                                        , Optional ByRef pArchivo As String = "") As Object
Dim lcOperaciones  As Class_Operaciones
'------------------------------------------------------------
Const clrHeader = &HD0D0D0
Dim sRecord
Dim bAppend
Dim lLinea As Integer
Dim lForm As Frm_Reporte_Generico
Dim lReg As hCollection.hFields
Dim a As Integer
Dim iFila As Integer
Dim Filas As Integer

  Set Fnt_Generar_Comprobante = Nothing

  Call Sub_CargarDatos_Gen(pId_Operacion)
  Call Sub_Bloquea_Puntero(Me)
  
  Set lForm = New Frm_Reporte_Generico
  Call lForm.Sub_InicarDocumento(pTitulo:="" _
                               , pTipoSalida:=pTipoSalida _
                               , pOrientacion:=orPortrait _
                               , pArchivoSalida:=pArchivo)
     
  Select Case Fnt_DiasHabiles_EntreFechas(glb_operacion.Fecha_Operacion _
                                        , glb_operacion.Fecha_Vencimiento)
    Case 0
      glb_operacion.Cond_Liquidacion = "PH"  ' Pago Hoy
    Case 1
      glb_operacion.Cond_Liquidacion = "PM"  ' Pago Ma�ana
    Case 2
      glb_operacion.Cond_Liquidacion = "CN"  ' Contado Normal
  End Select
     
  With lForm.VsPrinter
 ' .StartDoc
  .MarginLeft = "15mm"
  .MarginRight = "15mm"
    
    .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 2, 5) = 9
      .TableCell(tcAlign, 1, 1) = taCenterMiddle
      .TableCell(tcText, 1, 1) = "COMPROBANTE DE OPERACI�N"
      .TableCell(tcColWidth, 1, 1, 7, 1) = "180mm"
    .EndTable
    
    .Paragraph = "" 'salto de linea
    
    .StartTable
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 2
      .TableCell(tcFontSize, 1, 1) = 7
      .TableCell(tcColWidth, 1, 1) = "90mm"
      .TableCell(tcText, 1, 1) = "ANTECEDENTES GENERALES"
      .TableCell(tcColWidth, 1, 2) = "90mm"
      .TableCell(tcAlign, 1, 2) = taRightMiddle
      .TableCell(tcText, 1, 2) = "N� OP -  " & pId_Operacion
    .EndTable
    
    .StartTable
      .TableCell(tcRows) = 6
      .TableCell(tcCols) = 9
      .TableCell(tcFontSize, 1, 1, 6, 9) = 6
      .TableBorder = tbBox
        
      .TableCell(tcColWidth, 1, 1) = "23mm"
      .TableCell(tcColWidth, 1, 2) = "5mm"
      .TableCell(tcColWidth, 1, 3) = "36mm"
      ' 64
      
      
      .TableCell(tcColWidth, 1, 4) = "23mm"
      .TableCell(tcColWidth, 1, 5) = "5mm"
      .TableCell(tcColWidth, 1, 6) = "25mm"
      ' 53
        
      .TableCell(tcColWidth, 1, 7) = "23mm"
      .TableCell(tcColWidth, 1, 8) = "5mm"
      .TableCell(tcColWidth, 1, 9) = "35mm"
      ' 63
      
      
      .TableCell(tcText, 1, 1) = "Fecha Operaci�n"
      .TableCell(tcText, 1, 2) = ":"
      .TableCell(tcText, 1, 3) = glb_operacion.Fecha_Operacion
      
      .TableCell(tcText, 2, 1) = "Cond. Liquidaci�n"
      .TableCell(tcText, 2, 2) = ":"
      .TableCell(tcText, 2, 3) = glb_operacion.Cond_Liquidacion
      
      .TableCell(tcText, 3, 1) = "Cod. Cuenta"
      .TableCell(tcText, 3, 2) = ":"
      .TableCell(tcText, 3, 3) = glb_cliente.Cuenta
      
      .TableCell(tcText, 4, 1) = "Rut Cliente"
      .TableCell(tcText, 4, 2) = ":"
      .TableCell(tcText, 4, 3) = FormatoRut(Trim(glb_cliente.Rut))
      
      .TableCell(tcText, 5, 1) = "Nombre Cliente"
      .TableCell(tcText, 5, 2) = ":"
      .TableCell(tcText, 5, 3) = UCase(glb_cliente.Nombre)

      .TableCell(tcText, 6, 1) = "Contraparte"
      .TableCell(tcText, 6, 2) = ":"
      .TableCell(tcText, 6, 3) = UCase(glb_operacion.contraparte)
      
      .TableCell(tcText, 1, 4) = "Fecha Vencimiento"
      .TableCell(tcText, 1, 5) = ":"
      .TableCell(tcText, 1, 6) = glb_operacion.Fecha_Vencimiento
      
      .TableCell(tcText, 2, 4) = "Moneda"
      .TableCell(tcText, 2, 5) = ":"
      .TableCell(tcText, 2, 6) = glb_operacion.Moneda_Operacion
      
      .TableCell(tcText, 3, 4) = "Cta. Movimiento"
      .TableCell(tcText, 3, 5) = ":"
      .TableCell(tcText, 3, 6) = glb_operacion.Cta_Operacion
      
      .TableCell(tcText, 4, 4) = "Ejecutivo"
      .TableCell(tcText, 4, 5) = ":"
      .TableCell(tcText, 4, 6) = UCase(glb_cliente.Ejecutivo)
      

      .TableCell(tcText, 1, 7) = "Tipo Operaci�n"
      .TableCell(tcText, 1, 8) = ":"
      .TableCell(tcText, 1, 9) = glb_operacion.Tipo_Operacion
      
      .TableCell(tcText, 2, 7) = "Mercado"
      .TableCell(tcText, 2, 8) = ":"
      .TableCell(tcText, 2, 9) = UCase(glb_operacion.Mercado)
      
      .TableCell(tcText, 3, 7) = "Por Rueda"
      .TableCell(tcText, 3, 8) = ":"
      .TableCell(tcText, 3, 9) = ""
    .EndTable
    
    .StartTable
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1) = 7
      .TableCell(tcColWidth, 1, 1) = "180mm"
      .TableCell(tcText, 1, 1) = "DETALLE OPERACI�N"
      .TableBorder = tbNone
    .EndTable
    
    
    .StartTable
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 4
      .TableCell(tcFontSize, 1, 1, 1, 4) = 7
      .TableCell(tcColWidth, 1, 1) = "64mm"
      .TableCell(tcColWidth, 1, 2) = "44mm"
      .TableCell(tcColWidth, 1, 3) = "38mm"
      .TableCell(tcColWidth, 1, 4) = "34mm"
      .TableBorder = tbBoxColumns
      .TableCell(tcAlign, 1, 1, 1, 4) = taCenterMiddle
      
      .TableCell(tcText, 1, 1) = "Instrumento"
      
      .TableCell(tcText, 1, 2) = "Cantidad o Nominal"
      .TableCell(tcText, 1, 3) = "Precio o Tasa"
      .TableCell(tcText, 1, 4) = "Monto Operaci�n"
    .EndTable
    
    .StartTable
      '.TableCell(tcRows) = 4
      .TableCell(tcCols) = 4
      .TableBorder = tbBoxColumns
      
      .TableCell(tcColWidth, 1, 1) = "64mm"
      .TableCell(tcColWidth, 1, 2) = "44mm"
      .TableCell(tcColWidth, 1, 3) = "38mm"
      .TableCell(tcColWidth, 1, 4) = "34mm"
      
        Set lcOperaciones = New Class_Operaciones
        lcOperaciones.Campo("id_operacion").Valor = pId_Operacion
        If lcOperaciones.BuscaConDetalles Then
          Filas = CInt(lcOperaciones.Cursor.Count)
          If Filas < 50 Then
            Filas = 50
          End If
          .TableCell(tcRows) = Filas
          
          a = 1
          glb_operacion.Monto_Operacion = 0
          For Each lReg In lcOperaciones.Cursor
               .TableCell(tcText, a, 1) = Entrega_Nemotecnico(lReg("id_nemotecnico").Value)
               .TableCell(tcText, a, 2) = FormatNumber(lReg("cantidad").Value)
               .TableCell(tcText, a, 3) = FormatNumber(lReg("precio").Value)
               .TableCell(tcText, a, 4) = FormatNumber(lReg("monto_pago").Value)
               glb_operacion.Monto_Operacion = CDbl(glb_operacion.Monto_Operacion) + CDbl(lReg("monto_pago").Value)
                       
          a = a + 1
          Next
          
        End If
      
    .TableCell(tcFontSize, 1, 1, Filas, 4) = 6
    .TableCell(tcAlign, 1, 2, Filas, 4) = taRightMiddle
    .TableCell(tcAlign, 1, 1) = taLeftMiddle
    .EndTable
    
    .MarginLeft = "122.95mm"
    
    .StartTable
      
      .TableBorder = tbBoxColumns
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 2
      .TableCell(tcFontSize, 1, 1, 1, 3) = 6
      .TableCell(tcColWidth, 1, 1) = "38mm"
      .TableCell(tcColWidth, 1, 2) = "34mm"
      .TableCell(tcText, 1, 1) = "SUB-TOTAL"
      .TableCell(tcText, 1, 2) = FormatNumber(glb_operacion.Monto_Operacion)
      .TableCell(tcAlign, 1, 2) = taRightMiddle

    .EndTable
    
    .Paragraph = "" 'salto de linea
    .StartTable
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1) = 6
      .TableCell(tcColWidth, 1, 1) = "72mm"
      .TableCell(tcText, 1, 1) = "DETALLE DE GASTOS"
      .TableBorder = tbNone
      
    .EndTable
    
    .StartTable
      .TableCell(tcRows) = 5
      .TableCell(tcCols) = 2
      .TableBorder = tbBoxColumns
      .TableCell(tcFontSize, 1, 1, 5, 2) = 6
      .TableCell(tcColWidth, 1, 1) = "38mm"
      .TableCell(tcColWidth, 1, 2) = "34mm"
      glb_operacion.Por_Comision = CDbl(glb_operacion.Por_Comision) * 100
      .TableCell(tcText, 1, 1) = "Comisi�n Corredor " & "( " & glb_operacion.Por_Comision & "% )"
      .TableCell(tcText, 2, 1) = "Derechos de Bolsa"
      .TableCell(tcText, 3, 1) = "Otros"
      .TableCell(tcText, 4, 1) = "Gastos de Transferencia"
      .TableCell(tcText, 5, 1) = "IVA"
      
      .TableCell(tcAlign, 1, 2) = taRightMiddle
      .TableCell(tcAlign, 2, 2) = taRightMiddle
      .TableCell(tcAlign, 3, 2) = taRightMiddle
      .TableCell(tcAlign, 4, 2) = taRightMiddle
      .TableCell(tcAlign, 5, 2) = taRightMiddle
      
      .TableCell(tcText, 1, 2) = FormatNumber(NVL(glb_operacion.Comision, 0))
      .TableCell(tcText, 2, 2) = FormatNumber(NVL(glb_operacion.Derechos, 0))
      .TableCell(tcText, 3, 2) = FormatNumber(NVL(glb_operacion.Otros, 0))
      .TableCell(tcText, 4, 2) = FormatNumber(NVL(glb_operacion.Gastos, 0))
      .TableCell(tcText, 5, 2) = FormatNumber(NVL(glb_operacion.Iva, 0))
    .EndTable

    .StartTable
      .TableBorder = tbBoxColumns
      .TableCell(tcRows) = 1
      .TableCell(tcCols) = 2
      .TableCell(tcFontSize, 1, 1, 1, 3) = 6
      .TableCell(tcColWidth, 1, 1) = "38mm"
      .TableCell(tcColWidth, 1, 2) = "34mm"
      .TableCell(tcText, 1, 1) = "TOTAL"
      .TableCell(tcAlign, 1, 2) = taRightMiddle
      .TableCell(tcText, 1, 2) = FormatNumber(CDbl(glb_operacion.Comision) + CDbl(glb_operacion.Derechos) + CDbl(glb_operacion.Gastos) + CDbl(glb_operacion.Otros) + CDbl(glb_operacion.Iva) + CDbl(glb_operacion.Monto_Operacion))
    .EndTable

    .MarginLeft = "25mm"
    
    .StartTable
      .TableBorder = tbNone
      .TableCell(tcRows) = 3
      .TableCell(tcCols) = 1
      .TableCell(tcFontSize, 1, 1, 3, 1) = 6
      .TableCell(tcColWidth, 1, 1) = "60mm"
      .TableCell(tcText, 1, 1) = "_______________________________________"
      .TableCell(tcText, 2, 1) = "        BBVA Corredores de Bolsa"
      .TableCell(tcText, 3, 1) = "Adm. de Cartera"
      .TableCell(tcAlign, 1, 1, 3, 1) = taCenterMiddle
    .EndTable
    
    .EndDoc
  End With
  
  Set Fnt_Generar_Comprobante = lForm
  
Errprocedure:
  Call Sub_Desbloquea_Puntero(Me)
End Function

Public Function Generar_PDF(pId_Operacion As String _
                          , Optional pArchivoPDF As String = "") As String
                          
  If pArchivoPDF = "" Then
    pArchivoPDF = Environ("temp") & "\Boleta_Operacion_N_" & pId_Operacion & ".pdf"
  End If
  
  If Not Fnt_Generar_Comprobante(pId_Operacion:=pId_Operacion _
                            , pTipoSalida:=ePrinter.eP_PDF_Automatico _
                            , pArchivo:=pArchivoPDF) Is Nothing Then
    Generar_PDF = pArchivoPDF
  End If
    
Errprocedure:
  
  Unload Me
End Function

Public Sub Sub_CargarDatos_Gen(pId_Operacion)
'------------------------------------------
Dim lcCuentas As Class_Cuentas
Dim lcOperaciones  As Class_Operaciones
Dim lcMoneda As Class_Monedas
Dim lcMercado As Class_Productos
Dim lcContraparte As Class_Contrapartes
'----------------------------------------------
Dim lId_Cuenta As Double

'------------------------------------------
Call Sub_Bloquea_Puntero(Me)
'---------------------------------------
     
  Set lcOperaciones = New Class_Operaciones
  With lcOperaciones
    .Campo("id_operacion").Valor = pId_Operacion
    If .Buscar Then
      If .Cursor.Count > 0 Then
        lId_Cuenta = .Cursor(1).Fields("id_cuenta").Value
        glb_operacion.Fecha_Operacion = .Cursor(1).Fields("Fecha_Operacion").Value
        glb_operacion.Moneda_Operacion = .Cursor(1).Fields("id_moneda_operacion").Value
        glb_operacion.Fecha_Vencimiento = .Cursor(1).Fields("fecha_liquidacion").Value
        glb_operacion.Mercado = Trim(.Cursor(1).Fields("cod_producto").Value)
        glb_cliente.Cuenta = Trim(.Cursor(1).Fields("id_cuenta").Value)
        glb_operacion.Tipo_Operacion = Entrega_Tipo_Operacion(Trim(.Cursor(1).Fields("COD_TIPO_OPERACION").Value))
        glb_operacion.contraparte = Entrega_Contraparte(Trim(NVL(.Cursor(1).Fields("id_contraparte").Value, "")))
        glb_operacion.Cta_Operacion = Entrega_Caja_Cargo_Abono(NVL(.Cursor(1).Fields("id_cargo_abono").Value, 0))
        
       
        'glb_operacion.Monto_Operacion = NVL(.Cursor(1).Fields("monto_operacion").Value, 0)
        glb_operacion.Comision = NVL(.Cursor(1).Fields("comision").Value, 0)
        glb_operacion.Derechos = NVL(.Cursor(1).Fields("derechos").Value, 0)
        glb_operacion.Iva = NVL(.Cursor(1).Fields("iva").Value, 0)
        glb_operacion.Por_Comision = NVL(.Cursor(1).Fields("porc_comision").Value, 0)
        glb_operacion.Otros = 0
        glb_operacion.Gastos = NVL(.Cursor(1).Fields("gastos").Value, 0)
        
      End If
    End If
  End With
  Set lcOperaciones = Nothing
    
  Set lcCuentas = New Class_Cuentas
  With lcCuentas
    .Campo("id_Cuenta").Valor = lId_Cuenta
    
    If .Buscar(True) Then
      glb_cliente.Nombre = "" & .Cursor(1)("nombre_cliente").Value
      glb_cliente.Rut = "" & .Cursor(1)("rut_cliente").Value
      glb_cliente.Ejecutivo = "" & .Cursor(1)("dsc_asesor").Value
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcCuentas = Nothing
    
    
   Set lcMercado = New Class_Productos
   With lcMercado
     .Campo("cod_producto").Valor = glb_operacion.Mercado
     If .Buscar Then
       If .Cursor.Count > 0 Then
         glb_operacion.Mercado = Trim(.Cursor(1).Fields("dsc_producto").Value)
       End If
     End If
   End With
  Set lcMercado = Nothing
    
  Set lcContraparte = New Class_Contrapartes
  With lcContraparte
    .Campo("id_contraparte").Valor = glb_operacion.contraparte
    If .Buscar Then
      If .Cursor.Count > 0 Then
        glb_operacion.contraparte = Trim(.Cursor(1).Fields("dsc_contraparte").Value)
      End If
    End If
  End With
  Set lcContraparte = Nothing
  
    
  Set lcMoneda = New Class_Monedas
  With lcMoneda
    .Campo("id_moneda").Valor = glb_operacion.Moneda_Operacion
    If .Buscar Then
      If .Cursor.Count > 0 Then
        glb_operacion.Moneda_Operacion = .Cursor(1).Fields("dsc_moneda").Value
      End If
    End If
  End With
  Set lcMoneda = Nothing
End Sub

Function Entrega_Nemotecnico(lId_Nemotecnico As String) As String
   Dim lcNemotecnicos As Class_Nemotecnicos
   Dim nemotecnico As String
   
   Set lcNemotecnicos = New Class_Nemotecnicos
    With lcNemotecnicos
      .Campo("id_nemotecnico").Valor = lId_Nemotecnico
      If .Buscar Then
        If .Cursor.Count > 0 Then
          nemotecnico = .Cursor(1).Fields("nemotecnico").Value
        End If
      End If
    End With
    
    Entrega_Nemotecnico = nemotecnico
    Set lcNemotecnicos = Nothing
    
End Function

Function Entrega_Tipo_Operacion(lCod_Tipo_Operacion As String) As String

Dim lcTipoOperacion As Class_Tipos_Operaciones
Dim operacion As String
   
   Set lcTipoOperacion = New Class_Tipos_Operaciones
    With lcTipoOperacion
      .Campo("COD_TIPO_OPERACION").Valor = lCod_Tipo_Operacion
      If .Buscar Then
        If .Cursor.Count > 0 Then
          operacion = .Cursor(1).Fields("dsc_tipo_operacion").Value
        End If
      End If
    End With
    
    Entrega_Tipo_Operacion = operacion
    Set lcTipoOperacion = Nothing
    
End Function


Function Entrega_Contraparte(lId_Contraparte As String) As String
Dim lcContraparte As Class_Contrapartes
Dim contraparte As String
   
   Set lcContraparte = New Class_Contrapartes
    With lcContraparte
      .Campo("id_contraparte").Valor = lId_Contraparte
      If .Buscar Then
        If .Cursor.Count > 0 Then
          contraparte = .Cursor(1).Fields("dsc_contraparte").Value
        End If
      End If
    End With
    
    Entrega_Contraparte = contraparte
    Set lcContraparte = Nothing


End Function
Function FormatoRut(lrut As String) As String
Dim rut_1 As String
Dim rut_2 As String
Dim rut_c As String
Dim rut_t As String
Dim dv As String
rut_1 = Len(lrut)
rut_c = Left(lrut, rut_1 - 2)
dv = Right(lrut, 1)
rut_2 = Len(rut_c)
While Len(rut_c) > 2
rut_2 = Len(rut_c)
rut_t = "." & Right(rut_c, 3) & rut_t
rut_c = Left(rut_c, rut_2 - 3)
Wend
FormatoRut = rut_c & rut_t & "-" & dv
End Function

Function Entrega_Caja_Cargo_Abono(lId_cargo_abono As String) As String
Dim lcCajaOperacion As Class_Cargos_Abonos
Dim Caja As String
   
   Set lcCajaOperacion = New Class_Cargos_Abonos
    With lcCajaOperacion
      .Campo("id_cargo_abono").Valor = lId_cargo_abono
      If .Buscar Then
        If .Cursor.Count > 0 Then
          Caja = .Cursor(1).Fields("dsc_caja_cuenta").Value
        End If
      End If
    End With
    
    Entrega_Caja_Cargo_Abono = Caja
    Set lcCajaOperacion = Nothing
End Function


