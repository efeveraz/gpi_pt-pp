VERSION 5.00
Object = "{54850C51-14EA-4470-A5E4-8C5DB32DC853}#1.0#0"; "vsprint8.ocx"
Object = "{1BCC7098-34C1-4749-B1A3-6C109878B38F}#1.0#0"; "vspdf8.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Reporte_Cartola_BBVA 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Impresi�n"
   ClientHeight    =   8475
   ClientLeft      =   180
   ClientTop       =   330
   ClientWidth     =   6210
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8475
   ScaleWidth      =   6210
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   4
      Top             =   8070
      Width           =   6210
      _ExtentX        =   10954
      _ExtentY        =   714
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
      EndProperty
   End
   Begin VB.Frame Frame 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   7605
      Left            =   45
      TabIndex        =   0
      Top             =   420
      Width           =   6075
      Begin VSPrinter8LibCtl.VSPrinter vp 
         Height          =   7275
         Left            =   135
         TabIndex        =   1
         Top             =   225
         Width           =   5805
         _cx             =   10239
         _cy             =   12832
         Appearance      =   1
         BorderStyle     =   1
         Enabled         =   -1  'True
         MousePointer    =   0
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoRTF         =   -1  'True
         Preview         =   -1  'True
         DefaultDevice   =   0   'False
         PhysicalPage    =   -1  'True
         AbortWindow     =   -1  'True
         AbortWindowPos  =   0
         AbortCaption    =   "Imprimiendo..."
         AbortTextButton =   "Cancelar"
         AbortTextDevice =   "en %s en %s"
         AbortTextPage   =   "Ahora imprimiendo P�gina %d de"
         FileName        =   ""
         MarginLeft      =   1440
         MarginTop       =   1440
         MarginRight     =   1440
         MarginBottom    =   1440
         MarginHeader    =   0
         MarginFooter    =   0
         IndentLeft      =   0
         IndentRight     =   0
         IndentFirst     =   0
         IndentTab       =   720
         SpaceBefore     =   0
         SpaceAfter      =   0
         LineSpacing     =   100
         Columns         =   1
         ColumnSpacing   =   180
         ShowGuides      =   2
         LargeChangeHorz =   300
         LargeChangeVert =   300
         SmallChangeHorz =   30
         SmallChangeVert =   30
         Track           =   0   'False
         ProportionalBars=   -1  'True
         Zoom            =   38.3793410507569
         ZoomMode        =   3
         ZoomMax         =   400
         ZoomMin         =   10
         ZoomStep        =   25
         EmptyColor      =   -2147483636
         TextColor       =   0
         HdrColor        =   0
         BrushColor      =   0
         BrushStyle      =   0
         PenColor        =   0
         PenStyle        =   0
         PenWidth        =   0
         PageBorder      =   0
         Header          =   ""
         Footer          =   ""
         TableSep        =   "|;"
         TableBorder     =   7
         TablePen        =   0
         TablePenLR      =   0
         TablePenTB      =   0
         NavBar          =   3
         NavBarColor     =   -2147483633
         ExportFormat    =   0
         URL             =   ""
         Navigation      =   7
         NavBarMenuText  =   "Whole &Page|Page &Width|&Two Pages|Thumb&nail"
         AutoLinkNavigate=   0   'False
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   9
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   6210
      _ExtentX        =   10954
      _ExtentY        =   635
      ButtonWidth     =   1773
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Min/Max"
            Key             =   "MaxMin"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "a PDF"
            Key             =   "APDF"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   3
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VSPDF8LibCtl.VSPDF8 VSPDF8 
      Left            =   0
      Top             =   0
      Author          =   ""
      Creator         =   "CSGPI"
      Title           =   ""
      Subject         =   ""
      Keywords        =   ""
      Compress        =   3
   End
End
Attribute VB_Name = "Frm_Reporte_Cartola_BBVA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fArchivoSalida  As String
Dim fTipoSalida As ePrinter
Const RColorRelleno = 213   ' R
Const GColorRelleno = 202   ' G
Const BColorRelleno = 195   ' B
Const glb_tamletra_titulo = 12
Const glb_tamletra_subtitulo1 = 8
Const glb_tamletra_subtitulo2 = 8
Const glb_tamletra_registros = 8
Const COLUMNA_INICIO As String = "200mm"
Dim FilaAux As Variant
Private Type tipo_cliente
    Cuenta As String
    Nombre As String
    Direccion As String
    Telefono As String
    Rut As String
    Ejecutivo As String
    Mail As String
    Fecha_Creacion As Date
    Moneda_fondo As String
    Fecha_Proceso As Date
End Type
Dim glb_cliente As tipo_cliente

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("APDF").Image = cBoton_Pdf
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
  Me.Width = 10260
  Me.Height = 8205
  Me.Hide
End Sub

Private Sub Sub_CargaForm()
  Call Sub_FormControl_Color(Me.Controls)
End Sub
Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
  
On Error Resume Next
  Frame.Width = (Me.ScaleWidth - Me.Left - 100)
  Frame.Height = (Me.ScaleHeight - Me.Top - StatusBar1.Height - 500)
  vp.Width = Frame.Width - vp.Left - 150
  vp.Height = Frame.Height - vp.Top - 150
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "EXIT"
      Unload Me
    Case "MaxMin"
      Me.WindowState = IIf(Me.WindowState = vbMaximized, vbNormal, vbMaximized)
    Case "APDF"
      Call Sub_ElijeArchivoPDF
  End Select
End Sub

Private Sub vp_NewPage()
    Call pon_header
    Call Indicadores
End Sub

Public Sub pon_header()
    Dim xAnt As Variant
    Dim yAnt As Variant
    Dim xtalign As Variant
    Dim xbrushcolor As Variant
    Dim xpencolor As Variant
    Dim xtextcolor As Variant
    Dim xfontsize As Variant
    Dim xmarginleft As Variant
        
    Dim xValTop As Variant
    Dim sRutaImagen As String
    
    sRutaImagen = App.Path & "\creasys Chico 2.jpg"
    
    xValTop = ""
    
    'Inserta Logo Moneda
    On Error Resume Next
    vp.DrawPicture LoadPicture(sRutaImagen), vp.MarginLeft, "10mm", "55mm", "35mm"
    On Error GoTo 0
    
    ' .Image('imagenes/logomoneda.jpg',19,12,50)
    
    ' vp.DrawPicture p1.Picture, vp.MarginLeft, "20mm"
    
    'Guarda valores anteriores
    xAnt = vp.CurrentX
    yAnt = vp.CurrentY
    xtalign = vp.TextAlign
    xbrushcolor = vp.BrushColor
    xpencolor = vp.PenColor
    xtextcolor = vp.TextColor
    xfontsize = vp.FontSize
    xmarginleft = vp.MarginLeft
    
'Asigna nuevos valores
    vp.TextAlign = taCenterTop
    vp.BrushColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
    vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = RGB(100, 100, 100)
    
    vp.FontSize = 14
    'Llena texto
    vp.TextBox "GESTI�N DE INVERSIONES", "77mm", "11mm", "123mm", "10mm", True, False, True
    'Asigna nuevos valores
    vp.TextAlign = taLeftTop
    vp.BrushColor = RGB(232, 226, 222)
    'vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = &H0&
    vp.FontSize = 8
    
    'Llena texto
    vp.DrawRectangle "77mm", "17mm", "200mm", "40mm"

    vp.MarginLeft = "79mm"
    vp.CurrentX = "79mm"
    vp.CurrentY = "18mm"
    
    vp.Text = "Nombre:" & vbCrLf & "Direcci�n:" & vbCrLf & "Tel�fono:" & vbCrLf & "Rut:" & vbCrLf & "Cuenta:"
    
    vp.MarginLeft = "94mm"
    vp.CurrentX = "94mm"
    vp.CurrentY = "18mm"
    'vp.Text = Space(10) & vbCrLf & Space(10) & vbCrLf & Space(10) & vbCrLf & Space(10) & vbCrLf & Space(10)
    vp.Text = glb_cliente.Nombre & vbCrLf & glb_cliente.Direccion & vbCrLf & glb_cliente.Telefono & vbCrLf & glb_cliente.Rut & vbCrLf & glb_cliente.Cuenta
    
    vp.MarginLeft = "158mm"
    vp.CurrentX = "158mm"
    vp.CurrentY = "18mm"
    vp.Text = "Ejecutivo:" & vbCrLf & "e-mail:" & vbCrLf & "Creaci�n:" & vbCrLf & "Fecha:" & vbCrLf & "Moneda:"
    
    vp.MarginLeft = "170mm"
    vp.CurrentX = "170mm"
    vp.CurrentY = "18mm"
    'vp.Text = Space(10) & vbCrLf & Space(10) & vbCrLf & Space(10) & vbCrLf & Space(10)
    vp.Text = glb_cliente.Ejecutivo & vbCrLf & glb_cliente.Mail & vbCrLf & glb_cliente.Fecha_Creacion & vbCrLf & "" & vbCrLf & glb_cliente.Moneda_fondo & vbCrLf
    
    'Recupera valores anteriores
    vp.CurrentX = xAnt
    vp.CurrentY = yAnt
    vp.TextAlign = xtalign
    vp.BrushColor = xbrushcolor
    vp.PenColor = xpencolor
    vp.TextColor = xtextcolor
    vp.FontSize = xfontsize
    vp.MarginLeft = xmarginleft

End Sub

Sub trae_datos_cliente(lId_Cuenta As String, Fecha_Cartola As Date)

'    Dim lcCuenta As Class_Cuentas
    Dim lcCuenta As Object
'    Dim lcMoneda As Class_Monedas
    Dim lcMoneda As Object
    Dim lCursor_Cta As hRecord
    Dim lReg_Cta As hFields
    Dim lNum_Cuenta As String
    '------------------------------------------
    Dim lId_Cliente As String
    Dim lCod_Estado As String
    Dim lId_Asesor As String
    '------------------------------------------
    Dim lNom_Clt As String
    Dim lEstado_Clt As String
    Dim lFlg_Bloqueado As String
    Dim lMoneda_Clt As String
    '------------------------------------------
    Dim lcClientes  As Object
    '------------------------------------------
    Dim lAsesor As String
    Dim lcDireccion_Cliente As Object
    Call Sub_Bloquea_Puntero(Me)
    '---------------------------------------
    Rem Busca el numero de la cuenta
'    Set lcCuenta = New Class_Cuentas
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    With lcCuenta
        .Campo("id_cuenta").Valor = lId_Cuenta
        If .Buscar(True) Then
          Set lCursor_Cta = .Cursor
        Else
          MsgBox .ErrMsg, vbCritical, Me.Caption
          'GoTo ErrProcedure
        End If
    End With
    Rem Busca el nombre del cliente
    Set lcClientes = Fnt_CreateObject(cDLL_Clientes)
    With lcClientes
        Set .gDB = gDB
        .Campo("id_Cliente").Valor = lCursor_Cta(1)("id_cliente").Value
        If .Buscar(True) Then
            glb_cliente.Nombre = NVL(.Cursor(1)("nombre_cliente").Value, "")
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            Set lcClientes = Nothing
            'GoTo ErrProcedure
        End If
    End With
    Set lcDireccion_Cliente = Fnt_CreateObject(cDLL_Direccion_Cliente)
    With lcDireccion_Cliente
      Set .gDB = gDB
      .Campo("Id_Cliente").Valor = lcClientes.Campo("id_Cliente").Valor
      If .Buscar Then
        If .Cursor.Count > 0 Then
            glb_cliente.Direccion = NVL(.Cursor(1)("Direccion").Value, "")
            glb_cliente.Telefono = NVL(.Cursor(1)("fono").Value, "")
        Else
            glb_cliente.Direccion = "Sin Direcci�n"
            glb_cliente.Telefono = "Sin Fono"
        End If
      Else
        MsgBox "Problemas en cargar la Direcci� del Cliente." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      End If
    End With
    Set lcClientes = Nothing
    Set lcDireccion_Cliente = Nothing
    
    Rem Busca el nombre de la moneda
'    Set lcMoneda = New Class_Monedas
    Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
    
    With lcMoneda
        .Campo("id_Moneda").Valor = lCursor_Cta(1)("id_moneda").Value
        If .Buscar Then
            lMoneda_Clt = NVL(.Cursor(1)("dsc_moneda").Value, "")
        Else
            MsgBox .ErrMsg, vbCritical, Me.Caption
            Set lcMoneda = Nothing
            ' GoTo ErrProcedure
        End If
    End With
    Set lcMoneda = Nothing
        
    glb_cliente.Rut = NVL(lCursor_Cta(1)("RUT_CLIENTE").Value, "")
    glb_cliente.Cuenta = NVL(lCursor_Cta(1)("NUM_CUENTA").Value, "")
    glb_cliente.Ejecutivo = NVL(lCursor_Cta(1)("DSC_ASESOR").Value, "")
    glb_cliente.Mail = ""
    glb_cliente.Fecha_Creacion = lCursor_Cta(1)("FECHA_OPERATIVA").Value
    glb_cliente.Moneda_fondo = lMoneda_Clt
    glb_cliente.Fecha_Proceso = Fecha_Cartola
    Set lCursor_Cta = Nothing
    
End Sub
Sub Indicadores()

    Dim xAnt As Variant
    Dim yAnt As Variant
    Dim xtalign As Variant
    Dim xbrushcolor As Variant
    Dim xpencolor As Variant
    Dim xtextcolor As Variant
    Dim xfontsize As Variant
    Dim xmarginleft As Variant
        
    Dim xValTop As Variant
       
    xValTop = ""
    
    'Inserta Logo Moneda
   
    ' .Image('imagenes/logomoneda.jpg',19,12,50)
    
    ' vp.DrawPicture p1.Picture, vp.MarginLeft, "20mm"
    
    'Guarda valores anteriores
    xAnt = vp.CurrentX
    yAnt = vp.CurrentY
    xtalign = vp.TextAlign
    xbrushcolor = vp.BrushColor
    xpencolor = vp.PenColor
    xtextcolor = vp.TextColor
    xfontsize = vp.FontSize
    xmarginleft = vp.MarginLeft
    
'Asigna nuevos valores
    
    vp.BrushColor = RGB(RColorRelleno, GColorRelleno, BColorRelleno)
    vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = RGB(100, 100, 100)
    
    vp.FontSize = 10
    'Llena texto
    vp.TextBox " Indicadores al " & glb_cliente.Fecha_Proceso, "205mm", "11mm", "60mm", "11mm", True, False, True
    'Asigna nuevos valores
    vp.TextAlign = taLeftMiddle
    vp.BrushColor = RGB(232, 226, 222)
    'vp.PenColor = RGB(100, 100, 100)
    vp.TextColor = &H0&
    vp.FontSize = 8
    
    'Llena texto
    vp.DrawRectangle "205mm", "17mm", "265mm", "40mm"

    vp.MarginLeft = "210mm"
    vp.CurrentX = "210mm"
    vp.CurrentY = "18mm"
    
    vp.Text = "Valor D�lar Observado" & vbCrLf & "Valor UF"
    
    vp.MarginLeft = "240mm"
    vp.CurrentX = "240mm"
    vp.CurrentY = "18mm"
    vp.TextAlign = taRightMiddle
    'vp.Text = Space(10) & vbCrLf & Space(10) & vbCrLf & Space(10) & vbCrLf & Space(10) & vbCrLf & Space(10)
    vp.Text = "580,40" & vbCrLf & "18.400,10"
    
   
    'Recupera valores anteriores
    vp.CurrentX = xAnt
    vp.CurrentY = yAnt
    vp.TextAlign = xtalign
    vp.BrushColor = xbrushcolor
    vp.PenColor = xpencolor
    vp.TextColor = xtextcolor
    vp.FontSize = xfontsize
    vp.MarginLeft = xmarginleft
End Sub

Private Function rTwips(pMM As Variant)
    Dim i As Integer, s As String, d As Double
    If VarType(pMM) <> vbString Then
        rTwips = pMM
    Else
        i = InStr(1, pMM, "mm")
        If i = 0 Then
            MsgBox "Error de conversion de mm a twips"
            End
        End If
        
        s = Mid$(pMM, 1, i - 1)
        d = CDbl(s)
        
        ' rTwips = ((((d * 254) / 1440) + 0.5) * 567) / 100 + 0.5
        rTwips = 56.52 * d
        
    End If
End Function

Private Sub Sub_ElijeArchivoPDF()
Dim lArchivo As String

On Error GoTo Cmd_BuscarArchivo_Err
    
  If fArchivoSalida = "" Then
    fArchivoSalida = VSPDF8.Title
  End If
    
  With MDI_Principal.CommonDialog
    
    If .FileName = "" Then
      .FileName = fArchivoSalida
    End If
    
    .CancelError = True
    .Filter = "Archivo PDF (*.pdf)|*.pdf|Todos los Archivos (*.*)|*.*"
    .Flags = cdlOFNExplorer + cdlOFNHideReadOnly + cdlOFNLongNames + cdlOFNNoReadOnlyReturn + cdlOFNOverwritePrompt + cdlOFNPathMustExist
  
    .ShowSave
    
    lArchivo = .FileName
  End With

  If lArchivo = "" Then
    GoTo ExitProcedure
  End If

  fArchivoSalida = lArchivo

  If Fnt_ConvertPDF Then
    If MsgBox("�Desea abrir el documento?", vbYesNo) = vbYes Then
      Call Shell("cmd /c """ & lArchivo & """")
    End If
  End If

Cmd_BuscarArchivo_Err:
  If Err.Number = 32755 Then 'usuario presiona el boton Cancelar del Command Dialog
    Exit Sub
  End If
  
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al exportar a PDF.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If
ExitProcedure:

End Sub

Private Function Fnt_ConvertPDF() As Boolean
On Error GoTo ErrProcedure
  
  Fnt_ConvertPDF = False

  If fArchivoSalida = "" Then
    fArchivoSalida = VSPDF8.Title & ".pdf"
  End If
  
  Call VSPDF8.ConvertDocument(vp, fArchivoSalida)
  
  Fnt_ConvertPDF = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas al exportar a PDF.", Err.Description, pConLog:=True)
    Err.Clear
    Resume
  End If

End Function

