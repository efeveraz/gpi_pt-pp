VERSION 5.00
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Moneda 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Datos Moneda"
   ClientHeight    =   2385
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6300
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2385
   ScaleWidth      =   6300
   Begin VB.Frame Frame1 
      Caption         =   "Datos Moneda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1905
      Left            =   60
      TabIndex        =   2
      Top             =   390
      Width           =   6165
      Begin VB.CheckBox chk_Pago 
         Caption         =   "Es Moneda de Pago"
         Height          =   315
         Left            =   1500
         TabIndex        =   5
         Top             =   1050
         Value           =   1  'Checked
         Width           =   2025
      End
      Begin hControl2.hTextLabel txt_nombre 
         Height          =   315
         Left            =   210
         TabIndex        =   4
         Top             =   690
         Width           =   5670
         _ExtentX        =   10001
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Descripcion"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   20
      End
      Begin hControl2.hTextLabel txt_codigo 
         Height          =   315
         Left            =   210
         TabIndex        =   3
         Top             =   330
         Width           =   2865
         _ExtentX        =   5054
         _ExtentY        =   556
         LabelWidth      =   1275
         Caption         =   "Codigo"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         MaxLength       =   3
      End
      Begin hControl2.hTextLabel Txt_DecimalesMostrar 
         Height          =   315
         Left            =   210
         TabIndex        =   6
         Top             =   1410
         Width           =   2520
         _ExtentX        =   4445
         _ExtentY        =   556
         LabelWidth      =   1500
         TextMinWidth    =   1000
         Caption         =   "Decimales Mostrar"
         Text            =   ""
         MaxLength       =   3
      End
      Begin hControl2.hTextLabel Txt_Simbolo 
         Height          =   315
         Left            =   3360
         TabIndex        =   7
         Top             =   1410
         Width           =   2520
         _ExtentX        =   4445
         _ExtentY        =   556
         LabelWidth      =   1500
         TextMinWidth    =   1000
         Caption         =   "Simbolo"
         Text            =   ""
         MaxLength       =   3
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6300
      _ExtentX        =   11113
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Moneda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String

Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana

Private Sub Form_Load()
  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm
End Sub

Public Function Fnt_Modificar(pId_Cliente, pCod_Arbol_Sistema)
  fKey = pId_Cliente
  
'-----------------------------------------------------------------------
'--Setea los controles dependiendo de los permisos
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
'-----------------------------------------------------------------------
  
  Call Sub_CargarDatos
    
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso de Moneda: " & txt_nombre.Text
  Else
    Me.Caption = "Modificaci�n de Moneda: " & txt_nombre.Text
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
'Dim lMonedas As Class_Monedas
Dim lcMoneda As Object

  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Exit Function
  End If

'  Set lMonedas = New Class_Monedas
  Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
  With lcMoneda
    .Campo("id_Moneda").Valor = fKey
    .Campo("cod_moneda").Valor = txt_codigo.Text
    .Campo("dsc_moneda").Valor = txt_nombre.Text
    .Campo("flg_es_moneda_pago").Valor = IIf(chk_Pago.Value = vbChecked, "S", "N")
    .Campo("DICIMALES_MOSTRAR").Valor = Txt_DecimalesMostrar.Text
    .Campo("SIMBOLO").Valor = Txt_Simbolo.Text
    If Not .Guardar Then
      Call Fnt_MsgError(.SubTipo_LOG _
                        , "Problemas en grabar en Moneda." _
                        , .ErrMsg _
                        , pConLog:=True)
      Fnt_Grabar = False
    End If
  End With
  Set lcMoneda = Nothing
  
End Function

Private Sub Sub_CargaForm()
  Call Sub_FormControl_Color(Me.Controls)
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
'Dim lMonedas As Class_Monedas
Dim lcMoneda As Object
  
  Load Me

  Set lcMoneda = Fnt_CreateObject(cDLL_Monedas)
  With lcMoneda
    .Campo("id_moneda").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        txt_codigo.Text = NVL(lReg("cod_moneda").Value, "")
        txt_nombre.Text = NVL(lReg("dsc_moneda").Value, "")
        chk_Pago.Value = IIf(lReg("flg_es_moneda_pago").Value = gcFlg_SI, vbChecked, vbUnchecked)
        Txt_DecimalesMostrar.Text = lReg("DICIMALES_MOSTRAR").Value
        Txt_Simbolo.Text = "" & lReg("SIMBOLO").Value
      Next
    Else
      Call Fnt_MsgError(.SubTipo_LOG _
                        , "Problemas al leer la moneda (" & fKey & ")." _
                        , .ErrMsg _
                        , pConLog:=True)
    End If
  End With
  Set lcMoneda = Nothing
  
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = True
  
  If Fnt_PreguntaIsNull(txt_codigo) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
  
  If Fnt_PreguntaIsNull(txt_nombre) Then
    Fnt_ValidarDatos = False
    Exit Function
  End If
End Function
