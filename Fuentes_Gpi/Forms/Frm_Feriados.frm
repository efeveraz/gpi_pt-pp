VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Begin VB.Form Frm_Feriados 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Agregar D�as Feriados"
   ClientHeight    =   1725
   ClientLeft      =   45
   ClientTop       =   540
   ClientWidth     =   5595
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1725
   ScaleWidth      =   5595
   Begin VB.Frame Frame1 
      Caption         =   "Descripci�n del Feriados"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1245
      Left            =   60
      TabIndex        =   2
      Top             =   390
      Width           =   5475
      Begin MSComCtl2.DTPicker DTP_Fecha 
         Height          =   330
         Left            =   1470
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   330
         Width           =   1770
         _ExtentX        =   3122
         _ExtentY        =   582
         _Version        =   393216
         Format          =   67698689
         CurrentDate     =   38810
      End
      Begin hControl2.hTextLabel Txt_Descripcion 
         Height          =   315
         Left            =   150
         TabIndex        =   4
         Tag             =   "OBLI"
         Top             =   750
         Width           =   5175
         _ExtentX        =   9128
         _ExtentY        =   556
         LabelWidth      =   1300
         TextMinWidth    =   500
         Caption         =   "Descripci�n"
         Text            =   ""
         MaxLength       =   99
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         Height          =   330
         Left            =   150
         TabIndex        =   5
         Top             =   330
         Width           =   1305
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Tag             =   "OBLI"
      Top             =   0
      Width           =   5595
      _ExtentX        =   9869
      _ExtentY        =   635
      ButtonWidth     =   2011
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Aceptar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Cancelar"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Feriados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As Long

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        MsgBox "D�a Festivo grabado existosamente.", vbOKOnly + vbQuestion, Me.Caption
        Unload Me
      End If
    Case "EXIT"
      If MsgBox("Esta seguro que desea salir?" & vbCr & vbCr & "Se perder�n los datos si no ha grabado el registro.", vbYesNo + vbQuestion, Me.Caption) = vbYes Then
        Unload Me
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lcDias_Festivos As Class_Dias_Festivos
  
  Call Sub_Bloquea_Puntero(Me)
  
  Me.Enabled = False
  
  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    Fnt_Grabar = False
    Exit Function
  End If
  
  Set lcDias_Festivos = New Class_Dias_Festivos
  With lcDias_Festivos
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    .Campo("fch_dia_feriado").Valor = Format(DTP_Fecha.Value, cFormatDate)
    .Campo("dsc_dia_feriado").Valor = Txt_Descripcion.Text
    If Not .Guardar Then
      MsgBox "Problemas al grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      Fnt_Grabar = False
    End If
  End With
  Set lcDias_Festivos = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
  
  Me.Enabled = True
  
End Function

Private Sub Sub_CargaForm()
  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False
  
  If fKey = 0 Then
    Call Sub_CargaDatos
  End If
  Call Sub_FormControl_Color(Me.Controls)
  
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
End Sub

Private Function Fnt_ValidarDatos() As Boolean
Dim lValido As Boolean
Dim lFecha_Ini As String
Dim lFecha_Ter As String
Dim lResultado As Integer
Dim lMensaje As String

  Call Sub_Bloquea_Puntero(Me)
  Me.Enabled = False

  lValido = Fnt_Form_Validar(Me.Controls)
   
  Fnt_ValidarDatos = lValido
  
  Call Sub_Desbloquea_Puntero(Me)
  Me.Enabled = True
End Function

Public Function Fnt_Modificar(ByVal pIndice As Long, ByVal pFecha As Date)
  Me.Top = 1
  Me.Left = 1
  fKey = pIndice
  DTP_Fecha.Value = pFecha
  
  Call Sub_CargaForm
  Me.Show
End Function
Public Sub Sub_CargaDatos()
Dim lReg    As hCollection.hFields
Dim lcDiasFestivos As Class_Dias_Festivos

  Set lcDiasFestivos = New Class_Dias_Festivos
  With lcDiasFestivos
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    .Campo("FCH_DIA_FERIADO").Valor = DTP_Fecha.Value
    If .Buscar() Then
      For Each lReg In .Cursor
        Txt_Descripcion.Text = NVL(lReg("dsc_dia_feriado").Value, "")
      Next
    End If
  End With
End Sub
