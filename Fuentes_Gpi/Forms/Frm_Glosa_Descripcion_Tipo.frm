VERSION 5.00
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Glosa_Descripcion_Tipo 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Descripción Glosa "
   ClientHeight    =   1395
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   7215
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1395
   ScaleWidth      =   7215
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   885
      Left            =   60
      TabIndex        =   2
      Top             =   420
      Width           =   7065
      Begin hControl2.hTextLabel Txt_Dsc_Glosa_Descripcion 
         Height          =   315
         Left            =   150
         TabIndex        =   3
         Top             =   300
         Width           =   6735
         _ExtentX        =   11880
         _ExtentY        =   556
         LabelWidth      =   1800
         Enabled         =   0   'False
         Caption         =   "Descripción Glosa"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   635
      ButtonWidth     =   1958
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Grabar"
            Key             =   "SAVE"
            Description     =   "Agrega un elemento"
            Object.ToolTipText     =   "Graba Glosa Descripción Tipo"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refrescar"
            Key             =   "REFRESH"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Glosa_Descripcion_Tipo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String
Dim fSalir As Boolean
Dim fId_Glosa_Descripcion_Tipo As String
Dim fDsc_Glosa_Descripcion As String

Public Function Fnt_Modificar(ByRef pId_Glosa_Descripcion, _
                              pId_Glosa_Descripcion_Tipo, _
                              ByRef pDsc_Glosa_Descripcion)
  fKey = pId_Glosa_Descripcion
  fId_Glosa_Descripcion_Tipo = pId_Glosa_Descripcion_Tipo
  
  If fKey = cNewEntidad Then
    Me.Caption = "Ingreso Glosa"
  Else
    Me.Caption = "Modificación Glosa"
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
   
  Do While Not fSalir
    DoEvents
  Loop
  
  pDsc_Glosa_Descripcion = fDsc_Glosa_Descripcion
  pId_Glosa_Descripcion = fKey
  
End Function

Private Sub Form_Load()
Dim lReg  As hCollection.hFields
Dim lLinea As Long

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("EXIT").Image = cBoton_Salir
  End With
  
  Call Sub_CargarDatos

  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
  fSalir = True
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        MsgBox "Glosa guardada correctamente.", vbInformation, Me.Caption
        Unload Me
      End If
    Case "REFRESH"
      Call Sub_CargarDatos
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Sub_CargarDatos()
Dim lcGlosa As Class_Glosa_Descripcion
Dim lReg    As hCollection.hFields

  Set lcGlosa = New Class_Glosa_Descripcion
  With lcGlosa
    .Campo("id_glosa_descripcion").Valor = fKey
    If .Buscar Then
      For Each lReg In .Cursor
        Txt_Dsc_Glosa_Descripcion.Text = lReg("dsc_glosa_descripcion").Value
      Next
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcGlosa = Nothing
  
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lcGlosa As Class_Glosa_Descripcion
  
  Call Sub_Bloquea_Puntero(Me)
    
  Fnt_Grabar = True
  
  If Txt_Dsc_Glosa_Descripcion.Text = "" Then
    MsgBox "La Descripción de la Glosa no puede ser vacía.", vbExclamation, Me.Caption
    GoTo ErrProcedure
  End If
  
  fDsc_Glosa_Descripcion = Txt_Dsc_Glosa_Descripcion.Text
  
  Set lcGlosa = New Class_Glosa_Descripcion
  With lcGlosa
    .Campo("id_glosa_descripcion").Valor = fKey
    .Campo("id_glosa_descripcion_tipo").Valor = fId_Glosa_Descripcion_Tipo
    .Campo("dsc_glosa_descripcion").Valor = fDsc_Glosa_Descripcion
    If Not .Guardar Then
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
    fKey = .Campo("id_glosa_descripcion").Valor
  
  End With
  
ExitProcedure:
  Set lcGlosa = Nothing
  Call Sub_Desbloquea_Puntero(Me)
  Exit Function
  
ErrProcedure:
  Fnt_Grabar = False
  GoTo ExitProcedure

End Function
