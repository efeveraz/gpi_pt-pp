VERSION 5.00
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Tipo_Liquidacion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Tipos Liquidacion"
   ClientHeight    =   1845
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5535
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1845
   ScaleWidth      =   5535
   Begin VB.Frame Frame1 
      Height          =   1335
      Left            =   90
      TabIndex        =   2
      Top             =   420
      Width           =   5355
      Begin TrueDBList80.TDBCombo Cmb_Instrumento 
         Height          =   345
         Left            =   1350
         TabIndex        =   10
         Tag             =   "OBLI=S;CAPTION=Instrumento"
         Top             =   270
         Width           =   3885
         _ExtentX        =   6853
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   -1  'True
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Tipo_Liquidacion.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin VB.Frame Frm_Tipo_Movimiento 
         Caption         =   "Tipo Movimiento"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   585
         Left            =   120
         TabIndex        =   5
         Top             =   630
         Width           =   2685
         Begin VB.OptionButton OptEgreso 
            Caption         =   "Egreso"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Left            =   1470
            TabIndex        =   7
            Top             =   232
            Width           =   1095
         End
         Begin VB.OptionButton OptIngreso 
            Caption         =   "Ingreso"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   150
            TabIndex        =   6
            Top             =   240
            Value           =   -1  'True
            Width           =   1245
         End
      End
      Begin hControl2.hTextLabel Txt_Dias_Retencion 
         Height          =   315
         Left            =   2880
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   750
         Width           =   2355
         _ExtentX        =   4154
         _ExtentY        =   556
         LabelWidth      =   1360
         TextMinWidth    =   500
         Caption         =   "D�as de Retenci�n"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
         Format          =   "#,##0"
         Tipo_TextBox    =   1
         Alignment       =   1
      End
      Begin hControl2.hTextLabel Txt_Instrumento 
         Height          =   345
         Left            =   120
         TabIndex        =   8
         Top             =   270
         Visible         =   0   'False
         Width           =   5115
         _ExtentX        =   9022
         _ExtentY        =   609
         LabelWidth      =   1200
         Caption         =   "Instrumento"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin hControl2.hTextLabel txt_tipo_movimiento 
         Height          =   315
         Left            =   120
         TabIndex        =   9
         Top             =   750
         Visible         =   0   'False
         Width           =   2715
         _ExtentX        =   4789
         _ExtentY        =   556
         LabelWidth      =   1200
         Caption         =   "Tipo Movimiento"
         Text            =   ""
         Locked          =   -1  'True
         BackColorTxt    =   12648447
         BackColorTxt    =   12648447
      End
      Begin VB.Label lbl_instrumento 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Instrumento"
         Height          =   345
         Left            =   120
         TabIndex        =   4
         Top             =   270
         Width           =   1200
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   635
      ButtonWidth     =   1667
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Original"
            Key             =   "REFRESH"
            Description     =   "Vuelve a cargar la informaci�n perdiendo los cambios"
            Object.ToolTipText     =   "Cargar la informaci�n perdiendo los cambios"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   1
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Tipo_Liquidacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public fKey As String

Dim fFlg_Tipo_Permiso As String 'Flags para el permiso sobre la ventana

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SAVE").Image = cBoton_Grabar
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Original
  End With

  Call Sub_CargaForm

  Me.Top = 1
  Me.Left = 1
End Sub

Public Function Fnt_Modificar(pId_Tipo_Liquidacion, pCod_Arbol_Sistema)
  fKey = pId_Tipo_Liquidacion
  
  
'-----------------------------------------------------------------------
'--Setea los controles dependiendo de los permisos
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  Call Form_Resize
'-----------------------------------------------------------------------
  
  If fKey = cNewEntidad Then
    Me.Caption = "Agregar Tipo Liquidacion"
    
    Frm_Tipo_Movimiento.Visible = True
    txt_tipo_movimiento.Visible = False
    
    Cmb_Instrumento.Visible = True
    Txt_Instrumento.Visible = False
  Else
    Me.Caption = "Modificaci�n Tipo Liquidacion"
    
    Frm_Tipo_Movimiento.Visible = False
    txt_tipo_movimiento.Visible = True
    
    Cmb_Instrumento.Visible = False
    Txt_Instrumento.Visible = True
  End If
  
  Call Sub_CargarDatos
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
  
End Function

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        'Si no hubo problemas al grabar, sale
        Unload Me
      End If
    Case "EXIT"
      Unload Me
    Case "REFRESH"
      If MsgBox("Con esta acci�n perder� las modificaciones." & vbLf & "�Volver al Original?" _
              , vbYesNo + vbQuestion _
              , Me.Caption) = vbYes Then
        Call Sub_CargarDatos
      End If
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lcTipo_Liq As Class_Tipos_Liquidacion
Dim lCod_Instrumento As String
Dim lTipo_Movimiento As String

  Call Sub_Bloquea_Puntero(Me)
  
  Fnt_Grabar = True

  If Not Fnt_ValidarDatos Then
    GoTo ErrProcedure
  End If

  lCod_Instrumento = Fnt_ComboSelected_KEY(Cmb_Instrumento)
  
  If OptIngreso Then
    lTipo_Movimiento = gcTipoOperacion_Ingreso
  ElseIf OptEgreso Then
    lTipo_Movimiento = gcTipoOperacion_Egreso
  End If
  
  Rem Si se est� agregando un nuevo Tipo Liquidaci�n, Verifica que no exista
  Rem ya la relacion entre Instrumento, empresa y tipo movimiento en la tabla
  If fKey = cNewEntidad Then
    If Fnt_Verifica_Relacion(lCod_Instrumento, Fnt_EmpresaActual, lTipo_Movimiento, fKey) Then
      If Not MsgBox("Existe un registro en el sistema asociado al Instrumento y Tipo Movimento seleccionados." & vbCr & "�Desea reemplazar el existente con estos nuevos datos?", vbYesNo + vbQuestion + vbDefaultButton2, Me.Caption) = vbYes Then
        GoTo ErrProcedure
      End If
    End If
  End If
  
  Set lcTipo_Liq = New Class_Tipos_Liquidacion
  With lcTipo_Liq
    .Campo("id_tipo_liquidacion").Valor = fKey
    .Campo("cod_instrumento").Valor = lCod_Instrumento
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    .Campo("tipo_movimiento").Valor = lTipo_Movimiento
    .Campo("retencion").Valor = Txt_Dias_Retencion.Text
    If Not .Guardar Then
      MsgBox "Problemas en grabar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
  End With
  
  Call Sub_Desbloquea_Puntero(Me)
  Exit Function
    
ErrProcedure:
  Fnt_Grabar = False
  Set lcTipo_Liq = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
  
End Function

Private Function Fnt_Verifica_Relacion(pCod_Instrumento As String, _
                                       pId_Empresa As String, _
                                       pTipo_Movimiento As String, _
                                       ByRef pId_Tipo_Liquidacion As String) As Boolean
Dim lcTipo_Liq As Class_Tipos_Liquidacion
  
  Rem Verifica que no exista ya la relacion entre Instrumento, empresa y tipo movimiento en la tabla
  Set lcTipo_Liq = New Class_Tipos_Liquidacion
  With lcTipo_Liq
    .Campo("cod_instrumento").Valor = pCod_Instrumento
    .Campo("id_empresa").Valor = pId_Empresa
    .Campo("tipo_movimiento").Valor = pTipo_Movimiento
    If .Buscar Then
      If .Cursor.Count > 0 Then
        Fnt_Verifica_Relacion = True
        Rem Puede haber solo un id_tipo_liquidacion para un mismo instrumento, empresa y tipo movimiento
        pId_Tipo_Liquidacion = .Cursor(1)("id_tipo_liquidacion").Value
      Else
        Rem Si no existe la relacion antes comentada entonces procede a guardar los datos
        Fnt_Verifica_Relacion = False
      End If
    Else
      MsgBox .ErrMsg, vbCritical, Me.Caption
      Fnt_Verifica_Relacion = False
    End If
  End With
  Set lcTipo_Liq = Nothing

End Function

Private Sub Sub_CargaForm()

  Call Sub_FormControl_Color(Me.Controls)


  Call Sub_CargaCombo_Instrumentos(Cmb_Instrumento)
      
End Sub

Private Sub Sub_CargarDatos()
Dim lReg As hCollection.hFields
Dim lcTipo_Liq As Class_Tipos_Liquidacion
  
  Call Sub_Bloquea_Puntero(Me)
  
  Load Me
  
  Call Sub_Limpiar
  
  Set lcTipo_Liq = New Class_Tipos_Liquidacion
  With lcTipo_Liq
    .Campo("id_tipo_liquidacion").Valor = fKey
    .Campo("id_empresa").Valor = Fnt_EmpresaActual
    If .Buscar Then
      For Each lReg In .Cursor
        Call Sub_ComboSelectedItem(Cmb_Instrumento, lReg("cod_instrumento").Value)
        Txt_Instrumento.Text = Cmb_Instrumento.Text
        
        Txt_Dias_Retencion.Text = lReg("retencion").Value
        
        If lReg("tipo_movimiento").Value = gcTipoOperacion_Ingreso Then
          OptIngreso.Value = True
        ElseIf lReg("tipo_movimiento").Value = gcTipoOperacion_Egreso Then
          OptEgreso.Value = True
        End If
        txt_tipo_movimiento.Text = lReg("dsc_tipo_movimiento").Value
        
      Next
    Else
      MsgBox "Problemas en cargar." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
    End If
  End With
  Set lcTipo_Liq = Nothing
  
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub Sub_Limpiar()
  Txt_Instrumento.Text = ""
  Cmb_Instrumento.Text = ""
  OptIngreso.Value = True
  OptEgreso.Value = False
  txt_tipo_movimiento.Text = ""
  Txt_Dias_Retencion.Text = 0
End Sub

Private Function Fnt_ValidarDatos() As Boolean
  Fnt_ValidarDatos = Fnt_Form_Validar(Me.Controls)
End Function

Private Sub Txt_Dias_Retencion_KeyPress(KeyAscii As Integer)
  Rem Acepta digitos
  If Not KeyAscii = 8 And (KeyAscii < 48 Or KeyAscii > 59) Then
    KeyAscii = 0
  End If
End Sub
