VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Oper_Mov_Cust_RV 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Movimientos Custodia Renta Variable"
   ClientHeight    =   7965
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   11835
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7965
   ScaleWidth      =   11835
   Begin VB.Frame Frame1 
      Caption         =   "Visor de Sucesos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   2475
      Left            =   90
      TabIndex        =   10
      Top             =   5400
      Width           =   11685
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Visor 
         Height          =   2085
         Left            =   120
         TabIndex        =   11
         Top             =   270
         Width           =   10365
         _cx             =   18283
         _cy             =   3678
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   0
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   3
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Oper_Mov_Cust_RV.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   0   'False
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Detalle 
         Height          =   330
         Left            =   10560
         TabIndex        =   12
         Top             =   540
         Width           =   1020
         _ExtentX        =   1799
         _ExtentY        =   582
         ButtonWidth     =   1588
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Detalle"
               Key             =   "DETALLE"
               Object.ToolTipText     =   "Muestra el detalle de los sucesos de Caja"
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Height          =   765
      Left            =   90
      TabIndex        =   1
      Top             =   420
      Width           =   11685
      Begin VB.CommandButton Cmb_BuscaFile 
         Caption         =   "..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   6570
         TabIndex        =   2
         ToolTipText     =   "Busca el Archivo Plano de Precios de SAFP"
         Top             =   300
         Width           =   375
      End
      Begin hControl2.hTextLabel Txt_ArchivoPlano 
         Height          =   315
         Left            =   120
         TabIndex        =   3
         Tag             =   "OBLI"
         Top             =   300
         Width           =   6360
         _ExtentX        =   11218
         _ExtentY        =   556
         LabelWidth      =   1200
         TextMinWidth    =   1200
         Caption         =   "Archivo Plano"
         Text            =   ""
         BackColorTxt    =   12648384
         BackColorTxt    =   12648384
      End
      Begin MSComctlLib.Toolbar Toolbar_Carga_Archivo 
         Height          =   330
         Left            =   7260
         TabIndex        =   9
         Top             =   300
         Width           =   1650
         _ExtentX        =   2910
         _ExtentY        =   582
         ButtonWidth     =   2328
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Cargar       "
               Key             =   "LOAD"
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frnm_Grilla_Sucesos 
      Caption         =   "Movimientos Custodia RV"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4125
      Left            =   90
      TabIndex        =   0
      Top             =   1230
      Width           =   11685
      Begin VSFlex8LCtl.VSFlexGrid Grilla 
         Height          =   3705
         Left            =   120
         TabIndex        =   6
         Top             =   300
         Width           =   10365
         _cx             =   18283
         _cy             =   6535
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   1
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   14
         FixedRows       =   1
         FixedCols       =   1
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Oper_Mov_Cust_RV.frx":0073
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   -1  'True
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   2
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Check 
         Height          =   660
         Left            =   10590
         TabIndex        =   7
         Top             =   600
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   1164
         ButtonWidth     =   1138
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   2
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_ALL"
               Description     =   "Agregar un nemotecnico a la Operaci�n"
               Object.ToolTipText     =   "Selecciona todos los items"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "SEL_NOTHING"
               Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
               Object.ToolTipText     =   "Deselecciona todos los items"
            EndProperty
         EndProperty
         Begin MSComctlLib.ProgressBar ProgressBar4 
            Height          =   255
            Left            =   9420
            TabIndex        =   8
            Top             =   30
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   11835
      _ExtentX        =   20876
      _ExtentY        =   635
      ButtonWidth     =   1588
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Grabar"
            Key             =   "SAVE"
            Description     =   "Graba la informaci�n de forma Permanente"
            Object.ToolTipText     =   "Graba la informaci�n de forma Permanente"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   5
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_Oper_Mov_Cust_RV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const fc_Mercado = "N"

Public fKey As String

Dim fMov_Cust_RV As Object

Public Function Fnt_Mostrar(pCod_Proceso_Componente As String)
  Fnt_Mostrar = False

  fKey = pCod_Proceso_Componente
  
  If Not Fnt_CargarDatos Then
    Unload Me
    Exit Function
  End If
  
  Me.Top = 1
  Me.Left = 1
  Me.Show
  
  Fnt_Mostrar = True
End Function

Private Function Fnt_CargarDatos() As Boolean
Dim lcProceso_Componente  As Class_Proceso_Componente
Dim lMensaje              As String

On Error GoTo ErrProcedure

  Call Sub_Bloquea_Puntero(Me)
  
  Load Me
  Fnt_CargarDatos = False
  
  Set fMov_Cust_RV = Nothing
  
  'Busca el componente que corresponde al conciliador
  Set lcProceso_Componente = New Class_Proceso_Componente
  With lcProceso_Componente
    .Campo("COD_PROCESO_COMPONENTE").Valor = fKey
    .Campo("Id_Empresa").Valor = Fnt_EmpresaActual
    
    Set fMov_Cust_RV = .IniciaClass(lMensaje)
    
    If fMov_Cust_RV Is Nothing Then
      Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                      , lMensaje _
                      , .ErrMsg _
                      , pConLog:=True)
      GoTo ExitProcedure
    End If
  End With
  Set lcProceso_Componente = Nothing
  
  Fnt_CargarDatos = True
  
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la carga del Proceso Componente (" & fKey & ")." _
                     , Err.Description)
    GoTo ExitProcedure
    Resume
  End If
  
ExitProcedure:
  Call Sub_Desbloquea_Puntero(Me)
End Function

Private Sub Cmb_BuscaFile_Click()
  
  If fMov_Cust_RV.Fnt_Busca_Archivo_Plano Then
    Txt_ArchivoPlano.Text = fMov_Cust_RV.fArchivo
  End If
  
End Sub

Private Sub Form_Load()

  With Toolbar
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SAVE").Image = "boton_grabar"
    .Buttons("EXIT").Image = "boton_salir"
  End With
  
  With Toolbar_Check
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
    .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
    .Appearance = ccFlat
  End With
  
  With Toolbar_Carga_Archivo
    Set .ImageList = MDI_Principal.ImageListGlobal16
    .Buttons("LOAD").Image = "boton_modificar"
  End With
  
  With Toolbar_Detalle
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("DETALLE").Image = cBoton_Agregar_Grilla
  End With
  
  Txt_ArchivoPlano.Text = ""
  Grilla.Rows = 1
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Not Col = Grilla.ColIndex("CHK") Then
    Cancel = True
  End If
End Sub

Private Sub Grilla_Visor_DblClick()
Dim lMensaje As String

  With Grilla_Visor
    If .Row > 0 Then
      lMensaje = GetCell(Grilla_Visor, .Row, "TEXTO")
      If Not lMensaje = "" Then
        MsgBox lMensaje, vbInformation, Me.Caption
      End If
    End If
  End With
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SAVE"
      If Fnt_Grabar Then
        MsgBox "Movimientos de Custodia de Renta Variable guardados correctamente.", vbInformation, Me.Caption
      End If
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Function Fnt_Grabar() As Boolean
Dim lFila_Detalle As Long
Dim lSeguir As Boolean
Dim lId_Nemotecnico As String
Dim lFecha As Date
Dim lRut_Cliente As String
Dim lCuenta As String
Dim lNemotecnico As String
Dim lId_Cuenta
Dim lId_Moneda As String
Dim lId_Caja_Cuenta As String
Dim lPrecio As Double
Dim lTipo_Movimiento As String
Dim lNro_Movimiento As String
Dim lCantidad As Double
Dim lMonto As Double
Dim lAlias_Cta As String
Dim lId_Operacion As String
Dim lMsg_Error As String
'----------------------------
Dim lcAlias As Object
Dim lcNemotecnicos As Class_Nemotecnicos
Dim lcCaja_Cuenta As Class_Cajas_Cuenta
Dim lcAcciones As Class_Acciones
Dim lcOperaciones As Class_Operaciones
Dim lDetalle As Class_Operaciones_Detalle

  If Grilla.Rows = 1 Then
    MsgBox "No hay Movimientos de Custodia Renta Variable en la grilla.", vbInformation, Me.Caption
    GoTo ErrProcedure
  End If
  
  Fnt_Grabar = True
  Grilla_Visor.Rows = 1
  
  Rem Moneda Peso = 1
  lId_Moneda = 1
  '---------------------------------
  
  For lFila_Detalle = 1 To Grilla.Rows - 1
    If Grilla.Cell(flexcpChecked, lFila_Detalle, "chk") = flexChecked Then
      lSeguir = True
      lFecha = GetCell(Grilla, lFila_Detalle, "FECHA")
      lTipo_Movimiento = GetCell(Grilla, lFila_Detalle, "flg_tipo_movimiento")
      lNro_Movimiento = GetCell(Grilla, lFila_Detalle, "nro_movimiento")
      lNemotecnico = GetCell(Grilla, lFila_Detalle, "NEMOTECNICO")
      lCantidad = GetCell(Grilla, lFila_Detalle, "CANTIDAD")
      lPrecio = GetCell(Grilla, lFila_Detalle, "PRECIO")
      lMonto = lCantidad * lPrecio
      
      If Fnt_Buscar_Rel_Nro_Oper_Detalle(lNro_Movimiento, lMsg_Error) Then
        Call Fnt_Escribe_Grilla(Grilla_Visor, "E", "Fila " & lFila_Detalle & ": " & lMsg_Error)
        lSeguir = False
      End If
      '------------------------------------------------------------------------------------------------------------------------------
      If lSeguir Then
        Rem Busca el Alias de la Cuenta por el "Rut Cliente" concatenado con la "Cuenta"
        lRut_Cliente = GetCell(Grilla, lFila_Detalle, "RUT_CLIENTE")
        lCuenta = GetCell(Grilla, lFila_Detalle, "CUENTA")
        lAlias_Cta = lRut_Cliente & "/" & lCuenta
        Set lcAlias = CreateObject(cDLL_Alias)
        Set lcAlias.gDB = gDB
        lId_Cuenta = lcAlias.AliasSYSTEM2CSBPi(pCodigoSYSTEM:=cOrigen_BAC_Fondos_BBVA _
                                             , pCodigoCSBPI:=cTabla_Cuentas _
                                             , pValor:=lAlias_Cta)
        Set lcAlias = Nothing
        If IsNull(lId_Cuenta) Then
          Call Fnt_Escribe_Grilla(Grilla_Visor, "E", "Fila " & lFila_Detalle & ": La Cuenta '" & lAlias_Cta & "' no tiene asociado un Alias en una Cuenta de GPI.")
          lSeguir = False
        End If
      End If
      '------------------------------------------------------------------------------------------------------------------------------
      If lSeguir Then
        Rem Busca el id_nemotecnico del Nemotecnico
        Set lcNemotecnicos = New Class_Nemotecnicos
        lcNemotecnicos.Campo("nemotecnico").Valor = lNemotecnico
        If lcNemotecnicos.Buscar_Nemotecnico(pCod_Producto:="", pMostrar_Msg:=False) Then
          lId_Nemotecnico = lcNemotecnicos.Campo("id_nemotecnico").Valor
          If lId_Nemotecnico = "0" Then
            Call Fnt_Escribe_Grilla(Grilla_Visor, "E", "Fila " & lFila_Detalle & ": Nemot�cnico '" & lNemotecnico & "' no existe o est� vencido.")
            lSeguir = False
          End If
        Else
          Call Fnt_Escribe_Grilla(Grilla_Visor, "E", "Fila " & lFila_Detalle & ": " & lcNemotecnicos.ErrMsg)
          lSeguir = False
        End If
        Set lcNemotecnicos = Nothing
      End If
      '------------------------------------------------------------------------------------------------------------------------------
      lId_Caja_Cuenta = ""
      Rem Busca la caja para la cuenta
      If lSeguir Then
        Set lcCaja_Cuenta = New Class_Cajas_Cuenta
        With lcCaja_Cuenta
          .Campo("id_cuenta").Valor = lId_Cuenta
          .Campo("id_moneda").Valor = lId_Moneda
          .Campo("cod_mercado").Valor = fc_Mercado
          If .Buscar_Caja_Para_Invertir(lId_Nemotecnico) Then
            If .Cursor.Count > 0 Then
              lId_Caja_Cuenta = NVL(.Cursor(1)("id_caja_cuenta").Value, "")
            End If
          Else
            Call Fnt_Escribe_Grilla(Grilla_Visor, "E", "Fila " & lFila_Detalle & ": " & lcCaja_Cuenta.ErrMsg)
            lSeguir = False
          End If
        End With
        Set lcCaja_Cuenta = Nothing
        
        If lId_Caja_Cuenta = "" Then
          Call Fnt_Escribe_Grilla(Grilla_Visor, "E", "Fila " & lFila_Detalle & ": No existe una Caja para la Cuenta '" & lAlias_Cta & "'.")
          lSeguir = False
        End If
      End If
      '------------------------------------------------------------------------------------------------------------------------------
      If lSeguir Then
        Set lcAcciones = New Class_Acciones
        With lcAcciones
          Call .Agregar_Operaciones_Detalle(pId_Nemotecnico:=lId_Nemotecnico, _
                                            pCantidad:=lCantidad, _
                                            pPrecio:=lPrecio, _
                                            pId_Moneda:=lId_Moneda, _
                                            pMonto:=lMonto, _
                                            pFlg_Vende_Todo:=cFlg_No_Vende_Todo, _
                                            pPrecio_Historico:="")
          
          If Not .Realiza_Operacion_Custodia(pId_Operacion:=lId_Operacion, _
                                             pId_Cuenta:=lId_Cuenta, _
                                             pDsc_Operacion:="", _
                                             pTipoOperacion:=lTipo_Movimiento, _
                                             pId_Contraparte:="", _
                                             pId_Representante:="", _
                                             pId_Moneda_Operacion:=lId_Moneda, _
                                             pFecha_Operacion:=lFecha, _
                                             pFecha_Vigencia:=lFecha, _
                                             pFecha_Liquidacion:=lFecha, _
                                             pId_Trader:="", _
                                             pPorc_Comision:=0, _
                                             pComision:=0, _
                                             pDerechos_Bolsa:=0, _
                                             pGastos:=0, _
                                             pIva:=0, _
                                             pMonto_Operacion:=lMonto, _
                                             pTipo_Precio:=cTipo_Precio_Mercado, _
                                             pId_Caja_Cuenta:=lId_Caja_Cuenta) Then
            Call Fnt_Escribe_Grilla(Grilla_Visor, "E", "Fila " & lFila_Detalle & ": " & .ErrMsg)
            Fnt_Grabar = False
          Else
            Call Fnt_Escribe_Grilla(Grilla_Visor, "N", "Fila " & lFila_Detalle & ": Movimiento de Custodia Renta Variable ingresado correctamente.")
          End If
        End With
        Set lcAcciones = Nothing
        
        '----------------------------------------------------------------------------
        Rem Con el nuevo id_operacion busca los detalles
        Set lcOperaciones = New Class_Operaciones
        With lcOperaciones
          .Campo("Id_Operacion").Valor = lId_Operacion
          
          Rem Busca el detalle de la operacion
          If Not .BuscaConDetalles Then
            Call Fnt_Escribe_Grilla(Grilla_Visor, "E", "Fila " & lFila_Detalle & ": " & .ErrMsg)
            Fnt_Grabar = False
          End If
            
          Rem Guarda el id_operacion_detalle y nro movimiento en la tabla rel_conversiones
          For Each lDetalle In lcOperaciones.Detalles
            If Not Fnt_Guardar_Rel_Nro_Oper_Detalle(lNro_Movimiento, _
                                                    lDetalle.Campo("id_operacion_detalle").Valor, _
                                                    lMsg_Error) Then
              Call Fnt_Escribe_Grilla(Grilla_Visor, "E", "Fila " & lFila_Detalle & ": " & lMsg_Error)
              Fnt_Grabar = False
            End If
          Next
          
        End With
        '----------------------------------------------------------------------------
        
      End If
    End If
  Next
  
ErrProcedure:
  Set lcNemotecnicos = Nothing
  Set lcCaja_Cuenta = Nothing
  Set lcAcciones = Nothing
  Set lcOperaciones = Nothing
  
End Function

Private Sub Toolbar_Carga_Archivo_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents
    
  Select Case Button.Key
    Case "LOAD"
      Call Sub_Cargar_Movimientos
  End Select
End Sub

Private Sub Toolbar_Check_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
      Call Sub_CambiaCheck(Grilla, True)
    Case "SEL_NOTHING"
      Call Sub_CambiaCheck(Grilla, False)
  End Select
End Sub

Private Sub Sub_Cargar_Movimientos()
Dim lReg As hFields
Dim lLinea As Long
Dim lTipo_Movimiento As String
  
  Grilla.Rows = 1
  
  With fMov_Cust_RV
    If Not .Fnt_Cargar_Mov_Cust_RV Then
      MsgBox .ErrMsg, vbCritical, Me.Caption
      GoTo ErrProcedure
    End If
    
    If .Cursor.Count = 0 Then
      MsgBox "No hay datos en el Archivo Seleccionado.", vbInformation, Me.Caption
      GoTo ErrProcedure
    End If
    
    For Each lReg In .Cursor
      If lReg("tipo_movimiento").Value = gcTipoOperacion_Ingreso Then
        lTipo_Movimiento = "Ingreso"
      ElseIf lReg("tipo_movimiento").Value = gcTipoOperacion_Egreso Then
        lTipo_Movimiento = "Egreso"
      End If
      
      lLinea = Grilla.Rows
      Call Grilla.AddItem("")
      Call SetCell(Grilla, lLinea, "num_fila", lLinea)
      Grilla.Cell(flexcpChecked, lLinea, "chk") = flexChecked
      Call SetCell(Grilla, lLinea, "fecha", lReg("fecha").Value)
      Call SetCell(Grilla, lLinea, "cuenta", lReg("cuenta").Value)
      Call SetCell(Grilla, lLinea, "rut_cliente", lReg("rut_cliente").Value)
      Call SetCell(Grilla, lLinea, "flg_tipo_movimiento", lReg("tipo_movimiento").Value)
      Call SetCell(Grilla, lLinea, "tipo_movimiento", lTipo_Movimiento)
      Call SetCell(Grilla, lLinea, "con_custodia", lReg("con_custodia").Value)
      Call SetCell(Grilla, lLinea, "cod_movimiento", lReg("cod_movimiento").Value)
      Call SetCell(Grilla, lLinea, "nro_movimiento", lReg("nro_movimiento").Value)
      Call SetCell(Grilla, lLinea, "nemotecnico", lReg("nemotecnico").Value)
      Call SetCell(Grilla, lLinea, "cantidad", lReg("cantidad").Value)
      Call SetCell(Grilla, lLinea, "precio", lReg("precio").Value)
      Call SetCell(Grilla, lLinea, "monto", lReg("precio").Value * lReg("cantidad").Value)
      
    Next
  End With
  
ErrProcedure:

End Sub

Private Function Fnt_Buscar_Rel_Nro_Oper_Detalle(pNro_Movimiento As Variant, ByRef pMsg_Error As String) As Boolean
Dim lcRel_Conversion As Object
    
  Fnt_Buscar_Rel_Nro_Oper_Detalle = False
  pMsg_Error = ""
  
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = Fnt_Busca_Id_Origen(cOrigen_BAC_Fondos_BBVA, False)
    .Campo("Id_Tipo_Conversion").Valor = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle, False)
    .Campo("valor").Valor = pNro_Movimiento
    If .Buscar Then
      If .Cursor.Count > 0 Then
        pMsg_Error = "Hay un ingreso de Movimiento de Renta Variable para el N�mero Movimiento '" & pNro_Movimiento & "'. No se puede ingresar la operaci�n nuevamente."
        Fnt_Buscar_Rel_Nro_Oper_Detalle = True
      End If
    Else
      pMsg_Error = "Problemas en buscar la relaci�n N�mero Movimiento y el Detalle de la Operaci�n."
      Fnt_Buscar_Rel_Nro_Oper_Detalle = True
    End If
  End With
  Set lcRel_Conversion = Nothing
  
End Function

Private Function Fnt_Guardar_Rel_Nro_Oper_Detalle(pNro_Movimiento As Variant, pId_Operacion_Detalle As String, ByRef pMsg_Error As String) As Boolean
Dim lcRel_Conversion As Object

  Fnt_Guardar_Rel_Nro_Oper_Detalle = True
  
  Set lcRel_Conversion = Fnt_CreateObject(cDLL_Rel_Conversiones)
  With lcRel_Conversion
    Set .gDB = gDB
    .Campo("Id_Origen").Valor = Fnt_Busca_Id_Origen(cOrigen_BAC_Fondos_BBVA, False)
    .Campo("Id_Tipo_Conversion").Valor = Fnt_Busca_Id_Tipo_Conversion(cTabla_Operacion_Detalle, False)
    .Campo("valor").Valor = pNro_Movimiento
    Rem En "id_entidad" se guarda el id_operacion_detalle
    .Campo("id_entidad").Valor = pId_Operacion_Detalle
    If Not .Guardar Then
      pMsg_Error = "Problemas en guardar la relaci�n N�mero Movimiento y el Detalle de la Operaci�n."
      Fnt_Guardar_Rel_Nro_Oper_Detalle = False
    End If
  End With
  Set lcRel_Conversion = Nothing
  
End Function

Private Sub Toolbar_Detalle_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "DETALLE"
      Call Grilla_Visor_DblClick
  End Select
End Sub
