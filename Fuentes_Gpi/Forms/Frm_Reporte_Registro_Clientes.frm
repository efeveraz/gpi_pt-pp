VERSION 5.00
Object = "{54850C51-14EA-4470-A5E4-8C5DB32DC853}#1.0#0"; "vsprint8.ocx"
Object = "{1BCC7098-34C1-4749-B1A3-6C109878B38F}#1.0#0"; "vspdf8.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Frm_Reporte_Registro_Clientes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registro de Clientes"
   ClientHeight    =   1740
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8145
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   1740
   ScaleWidth      =   8145
   Begin VB.Frame Frame1 
      Caption         =   "Datos de B�squeda"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   1275
      Left            =   60
      TabIndex        =   0
      Top             =   420
      Width           =   8055
      Begin VB.OptionButton OptCuentaVigente 
         Caption         =   "Vigentes"
         Height          =   345
         Left            =   4320
         TabIndex        =   11
         Top             =   750
         Width           =   1695
      End
      Begin VB.OptionButton OptCuentaCerrada 
         Caption         =   "Cerradas"
         Height          =   345
         Left            =   3000
         TabIndex        =   10
         Top             =   750
         Width           =   1815
      End
      Begin MSComCtl2.DTPicker Dtp_FechaReporte 
         Height          =   345
         Left            =   1410
         TabIndex        =   1
         Top             =   270
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   609
         _Version        =   393216
         Format          =   65601537
         CurrentDate     =   38938
      End
      Begin TrueDBList80.TDBCombo Cmb_TiposCuentas 
         Height          =   345
         Left            =   4320
         TabIndex        =   2
         Tag             =   "OBLI=S;CAPTION=Cuenta"
         Top             =   270
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   609
         _LayoutType     =   0
         _RowHeight      =   -2147483647
         _WasPersistedAsPixels=   0
         _DropdownWidth  =   0
         _EDITHEIGHT     =   609
         _GAPHEIGHT      =   53
         Columns(0)._VlistStyle=   0
         Columns(0)._MaxComboItems=   5
         Columns(0).DataField=   "ub_grid1"
         Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns(1)._VlistStyle=   0
         Columns(1)._MaxComboItems=   5
         Columns(1).DataField=   "ub_grid2"
         Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
         Columns.Count   =   2
         Splits(0)._UserFlags=   0
         Splits(0).ExtendRightColumn=   -1  'True
         Splits(0).AllowRowSizing=   0   'False
         Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
         Splits(0)._ColumnProps(0)=   "Columns.Count=2"
         Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
         Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
         Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
         Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
         Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
         Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
         Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
         Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
         Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
         Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
         Splits.Count    =   1
         Appearance      =   3
         BorderStyle     =   1
         ComboStyle      =   0
         AutoCompletion  =   -1  'True
         LimitToList     =   0   'False
         ColumnHeaders   =   0   'False
         ColumnFooters   =   0   'False
         DataMode        =   5
         DefColWidth     =   0
         Enabled         =   -1  'True
         HeadLines       =   1
         FootLines       =   1
         RowDividerStyle =   0
         Caption         =   ""
         EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
         LayoutName      =   ""
         LayoutFileName  =   ""
         MultipleLines   =   0
         EmptyRows       =   0   'False
         CellTips        =   0
         AutoSize        =   0   'False
         ListField       =   ""
         BoundColumn     =   ""
         IntegralHeight  =   0   'False
         CellTipsWidth   =   0
         CellTipsDelay   =   1000
         AutoDropdown    =   -1  'True
         RowTracking     =   -1  'True
         RightToLeft     =   0   'False
         MouseIcon       =   0
         MouseIcon.vt    =   3
         MousePointer    =   0
         MatchEntryTimeout=   2000
         OLEDragMode     =   0
         OLEDropMode     =   0
         AnimateWindow   =   3
         AnimateWindowDirection=   5
         AnimateWindowTime=   200
         AnimateWindowClose=   1
         DropdownPosition=   0
         Locked          =   0   'False
         ScrollTrack     =   -1  'True
         ScrollTips      =   -1  'True
         RowDividerColor =   14215660
         RowSubDividerColor=   14215660
         MaxComboItems   =   10
         AddItemSeparator=   ";"
         _PropDict       =   $"Frm_Reporte_Registro_Clientes.frx":0000
         _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
         _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
         _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
         _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
         _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
         _StyleDefs(5)   =   ":id=0,.fontname=Arial"
         _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&HC0FFC0&"
         _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
         _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
         _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
         _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
         _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
         _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
         _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
         _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
         _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
         _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
         _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
         _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
         _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
         _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
         _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
         _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
         _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
         _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
         _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
         _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
         _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
         _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
         _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
         _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
         _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
         _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
         _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
         _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
         _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
         _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
         _StyleDefs(38)  =   "Named:id=33:Normal"
         _StyleDefs(39)  =   ":id=33,.parent=0"
         _StyleDefs(40)  =   "Named:id=34:Heading"
         _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(42)  =   ":id=34,.wraptext=-1"
         _StyleDefs(43)  =   "Named:id=35:Footing"
         _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
         _StyleDefs(45)  =   "Named:id=36:Selected"
         _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(47)  =   "Named:id=37:Caption"
         _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
         _StyleDefs(49)  =   "Named:id=38:HighlightRow"
         _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
         _StyleDefs(51)  =   "Named:id=39:EvenRow"
         _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
         _StyleDefs(53)  =   "Named:id=40:OddRow"
         _StyleDefs(54)  =   ":id=40,.parent=33"
         _StyleDefs(55)  =   "Named:id=41:RecordSelector"
         _StyleDefs(56)  =   ":id=41,.parent=34"
         _StyleDefs(57)  =   "Named:id=42:FilterBar"
         _StyleDefs(58)  =   ":id=42,.parent=33"
      End
      Begin MSComCtl2.DTPicker Dtp_FechaHasta 
         Height          =   345
         Left            =   1410
         TabIndex        =   8
         Top             =   750
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   609
         _Version        =   393216
         Format          =   65601537
         CurrentDate     =   38938
      End
      Begin VB.Label Lbl_Fecha_Ter 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Hasta"
         Height          =   345
         Index           =   1
         Left            =   120
         TabIndex        =   9
         Top             =   750
         Width           =   1185
      End
      Begin VB.Label Lbl_TipoCuenta 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Tipo de Cuenta"
         Height          =   330
         Left            =   3030
         TabIndex        =   4
         Top             =   270
         Width           =   1170
      End
      Begin VB.Label Lbl_Fecha_Ter 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Desde"
         Height          =   345
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   270
         Width           =   1185
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   8145
      _ExtentX        =   14367
      _ExtentY        =   635
      ButtonWidth     =   2143
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Refrescar"
            Key             =   "REFRESH"
            Object.ToolTipText     =   "Limpia los controles"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Imprimir"
            Key             =   "PRINTER"
            Description     =   "Imprime Registro de Clientes"
            Object.ToolTipText     =   "Imprime Registro de Clientes"
            Style           =   5
            BeginProperty ButtonMenus {66833FEC-8583-11D1-B16A-00C0F0283628} 
               NumButtonMenus  =   3
               BeginProperty ButtonMenu1 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SCREEN"
                  Text            =   "a Pantalla"
               EndProperty
               BeginProperty ButtonMenu2 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "PDF"
                  Text            =   "a PDF"
               EndProperty
               BeginProperty ButtonMenu3 {66833FEE-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "EXCEL"
                  Text            =   "a Excel"
               EndProperty
            EndProperty
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   7080
         TabIndex        =   6
         Top             =   0
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin VSPrinter8LibCtl.VSPrinter vp 
      Height          =   615
      Left            =   120
      TabIndex        =   7
      Top             =   1680
      Visible         =   0   'False
      Width           =   1335
      _cx             =   2355
      _cy             =   1085
      Appearance      =   1
      BorderStyle     =   1
      Enabled         =   -1  'True
      MousePointer    =   0
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      AutoRTF         =   -1  'True
      Preview         =   -1  'True
      DefaultDevice   =   0   'False
      PhysicalPage    =   -1  'True
      AbortWindow     =   -1  'True
      AbortWindowPos  =   0
      AbortCaption    =   "Printing..."
      AbortTextButton =   "Cancel"
      AbortTextDevice =   "on the %s on %s"
      AbortTextPage   =   "Now printing Page %d of"
      FileName        =   ""
      MarginLeft      =   1440
      MarginTop       =   1440
      MarginRight     =   1440
      MarginBottom    =   1440
      MarginHeader    =   0
      MarginFooter    =   0
      IndentLeft      =   0
      IndentRight     =   0
      IndentFirst     =   0
      IndentTab       =   720
      SpaceBefore     =   0
      SpaceAfter      =   0
      LineSpacing     =   100
      Columns         =   1
      ColumnSpacing   =   180
      ShowGuides      =   2
      LargeChangeHorz =   300
      LargeChangeVert =   300
      SmallChangeHorz =   30
      SmallChangeVert =   30
      Track           =   0   'False
      ProportionalBars=   -1  'True
      Zoom            =   -1.23106060606061
      ZoomMode        =   3
      ZoomMax         =   400
      ZoomMin         =   10
      ZoomStep        =   25
      EmptyColor      =   -2147483636
      TextColor       =   0
      HdrColor        =   0
      BrushColor      =   0
      BrushStyle      =   0
      PenColor        =   0
      PenStyle        =   0
      PenWidth        =   0
      PageBorder      =   0
      Header          =   ""
      Footer          =   ""
      TableSep        =   "|;"
      TableBorder     =   7
      TablePen        =   0
      TablePenLR      =   0
      TablePenTB      =   0
      NavBar          =   3
      NavBarColor     =   -2147483633
      ExportFormat    =   0
      URL             =   ""
      Navigation      =   3
      NavBarMenuText  =   "Whole &Page|Page &Width|&Two Pages|Thumb&nail"
      AutoLinkNavigate=   0   'False
      AccessibleName  =   ""
      AccessibleDescription=   ""
      AccessibleValue =   ""
      AccessibleRole  =   9
   End
   Begin MSComDlg.CommonDialog Cmd_AbreArchivo 
      Left            =   1440
      Top             =   1800
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VSPDF8LibCtl.VSPDF8 VSPDF81 
      Left            =   2160
      Top             =   1800
      Author          =   ""
      Creator         =   ""
      Title           =   ""
      Subject         =   ""
      Keywords        =   ""
      Compress        =   3
   End
End
Attribute VB_Name = "Frm_Reporte_Registro_Clientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'-------------------------------------------------------------------------------------------------
'SOURCESAFE Information:
'    $Workfile: Frm_Reporte_Registro_Clientes.frm $
'    $Author: Gbuenrostro $
'    $Date: 6-02-14 14:07 $
'    $Revision: 4 $
'-------------------------------------------------------------------------------------------------

Dim oCliente            As New Class_AdministracionCartera
Dim iLineasImpresas     As Integer
Dim iTipoSalida         As Integer
Dim iTotalPaginas       As Integer
Dim iTotalRegistros     As Integer

Dim lForm               As Frm_Reporte_Cartola

Const RColorRelleno = 38 '213   ' R
Const GColorRelleno = 7 '202   ' G
Const BColorRelleno = 115 '195   ' B


Const RColorRelleno_TITULOS = 242  ' R
Const GColorRelleno_TITULOS = 242   ' G
Const BColorRelleno_TITULOS = 242   ' B

Const glb_tamletra_titulo = 10
Const glb_tamletra_encabezado = 9
Const glb_tamletra_registros = 7.5

Const Font_Name = "Microsoft Sans Serif" '"Times New Roman"

Const COLUMNA_INICIO As String = "10mm"
Const CONS_NUMERO_LINEAS As Integer = 30   ' Numero de Lineas$
Const CONS_POR_PANTALLA As Integer = 0
Const CONS_POR_IMPRESORA As Integer = 1
Const CONS_POR_ARCHIVO As Integer = 2

Const FIL_ENCABEZADO As Integer = 9
Const COL_NUMERO As Integer = 2
Const COL_RUT As Integer = 3
Const COL_NOMBRE As Integer = 4
Const COL_FECHA_INICIO_CONT As Integer = 5
Const COL_FECHA_TERMIN_CONT As Integer = 6
Const COL_OBSER As Integer = 7

Dim App_Excel       As Excel.Application      ' Excel application
Dim lLibro          As Excel.Workbook      ' Excel workbook

Public Sub Mostrar(pCod_Arbol_Sistema)
  Me.Show
End Sub

Private Sub Form_Load()

  With Toolbar
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("EXIT").Image = cBoton_Salir
      .Buttons("REFRESH").Image = cBoton_Refrescar
      .Buttons("PRINTER").Image = cBoton_Imprimir
  End With
    
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
    Call Sub_FormControl_Color(Me.Controls)

    Call Sub_CargaCombo_TipoCuentas(Cmb_TiposCuentas, pTodos:=True)
    Call Sub_ComboSelectedItem(Cmb_TiposCuentas, cCmbKALL)
          
    Me.OptCuentaVigente.Value = True
          
    Dtp_FechaReporte.Value = Fnt_FechaServidor - 1
    Dtp_FechaHasta.Value = Fnt_FechaServidor
    
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "REFRESH"
      Call Sub_Limpiar
    Case "PRINTER"
      iTipoSalida = CONS_POR_IMPRESORA
      Call Sub_Generar_Informe
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
Me.SetFocus
  DoEvents

  Select Case ButtonMenu.Key
    Case "SCREEN"
        iTipoSalida = CONS_POR_PANTALLA
        Call Sub_Generar_Informe
    Case "PDF"
        iTipoSalida = CONS_POR_PANTALLA
        Call Sub_Generar_Informe
    Case "EXCEL"
        If Cmb_TiposCuentas.Text = "" Then
            MsgBox "Debe seleccionar Tipo de Cuenta", vbInformation, Me.Caption
                Call Sub_Desbloquea_Puntero(Me)
            Exit Sub
        End If
        Call CreaExcel
  End Select
End Sub

Private Sub Sub_Limpiar()
    Dtp_FechaReporte.Value = Fnt_FechaServidor
    Cmb_TiposCuentas.Text = ""
End Sub


Private Sub Sub_Generar_Informe()
    Dim lId_Cuenta  As Integer
    Dim lcCuenta    As Object
    Dim lForm       As Frm_Reporte_Generico

    Call Sub_Bloquea_Puntero(Me)
    iTotalPaginas = 0
    
    If Cmb_TiposCuentas.Text = "" Then
       MsgBox "Debe seleccionar Tipo de Cuenta", vbInformation, Me.Caption
         Call Sub_Desbloquea_Puntero(Me)
       Exit Sub
    End If
    
    oCliente.IdTipoCuenta = IIf(Fnt_ComboSelected_KEY(Cmb_TiposCuentas) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_TiposCuentas))
    
    Set lForm = New Frm_Reporte_Generico
    Call lForm.Sub_InicarDocumento(pTitulo:="" _
                               , pTipoSalida:=ePrinter.eP_Pantalla _
                               , pOrientacion:=orLandscape)
    
    Call ImprimeEncabezado(lForm.VsPrinter)
    Call ImprimeClientes(lForm.VsPrinter)
    lForm.VsPrinter.EndDoc
    
    Call ColocarEn(lForm, Me, eFP_Abajo)

ErrProcedure:
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Private Sub ImprimeEncabezado(ByRef vp As VsPrinter)
Dim sTitulo As String
'-------------------------------------------------------------------
Dim sRutaImagen As String
'-------------------------------------------------------------------
     
    On Error GoTo 0

    'Guarda valores anteriores
    
    sRutaImagen = App.Path & "\Logo Security.jpg"

    'Inserta Logo
    On Error Resume Next
    
    With vp
        .FontBold = True
        .FontSize = glb_tamletra_titulo
        .DrawPicture LoadPicture(sRutaImagen), "200mm", "7mm", "65mm", "10mm"
        .TextAlign = taLeftTop
        .Paragraph = "Registro de Clientes con " & Me.Cmb_TiposCuentas.Text
        .Paragraph = "Informe del " & Dtp_FechaReporte.Value & " al " & Dtp_FechaHasta.Value & IIf(Me.OptCuentaVigente.Value, " (Vigentes)", " (Cerradas)")
        
        .FontSize = glb_tamletra_encabezado
                
        iLineasImpresas = iLineasImpresas + .LineSpacing
        
        .StartTable
        .TableBorder = tbBox
        .TableBorder = tbBoxRows
        
        .TableCell(tcRows) = .TableCell(tcRows) + 1
                
        .TableCell(tcCols) = IIf(Me.OptCuentaVigente.Value, COL_OBSER - 2, COL_OBSER - 1)
        .TableCell(tcText, 1, COL_NUMERO - 1) = ""
        .TableCell(tcText, 1, COL_RUT - 1) = "RUT"
        .TableCell(tcText, 1, COL_NOMBRE - 1) = "Nombre o Raz�n Social"
        .TableCell(tcText, 1, COL_FECHA_INICIO_CONT - 1) = "Fecha Inicio Contrato"
        If Me.OptCuentaVigente.Value Then
            .TableCell(tcText, 1, COL_OBSER - 2) = "Observaciones"
        Else
            .TableCell(tcText, 1, COL_FECHA_TERMIN_CONT - 1) = "Fecha T�rmino Contrato"
            .TableCell(tcText, 1, COL_OBSER - 1) = "Observaciones"
        End If
        
        .TableCell(tcColWidth, 1, COL_NUMERO - 1) = "10mm": .TableCell(tcAlign, 1, COL_NUMERO - 1) = taCenterMiddle
        .TableCell(tcColWidth, 1, COL_RUT - 1) = "20mm": .TableCell(tcAlign, 1, COL_RUT - 1) = taCenterMiddle
        .TableCell(tcColWidth, 1, COL_NOMBRE - 1) = "90mm": .TableCell(tcAlign, 1, COL_NOMBRE - 1) = taCenterMiddle
        .TableCell(tcColWidth, 1, COL_FECHA_INICIO_CONT - 1) = "30mm": .TableCell(tcAlign, 1, COL_FECHA_INICIO_CONT - 1) = taCenterMiddle
        If Me.OptCuentaVigente.Value Then
            .TableCell(tcColWidth, 1, COL_OBSER - 2) = "90mm": .TableCell(tcAlign, 1, COL_OBSER - 2) = taCenterMiddle
        Else
            .TableCell(tcColWidth, 1, COL_FECHA_TERMIN_CONT - 1) = "40mm": .TableCell(tcAlign, 1, COL_FECHA_TERMIN_CONT - 1) = taCenterMiddle
            .TableCell(tcColWidth, 1, COL_OBSER - 1) = "50mm": .TableCell(tcAlign, 1, COL_OBSER - 1) = taCenterMiddle
        End If
                   
        .TableCell(tcBackColor, COL_NUMERO - 1) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        .TableCell(tcBackColor, COL_RUT - 1) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        .TableCell(tcBackColor, COL_NOMBRE - 1) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        .TableCell(tcBackColor, COL_FECHA_INICIO_CONT - 1) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        If Me.OptCuentaVigente.Value Then
            .TableCell(tcBackColor, COL_OBSER - 2) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        Else
            .TableCell(tcBackColor, COL_FECHA_TERMIN_CONT - 1) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
            .TableCell(tcBackColor, COL_OBSER - 1) = RGB(RColorRelleno_TITULOS, GColorRelleno_TITULOS, BColorRelleno_TITULOS)
        End If
             
        iLineasImpresas = iLineasImpresas + (.LineSpacing * .TableCell(tcRows))
        .FontSize = glb_tamletra_registros
        .FontBold = False
    End With
    
    iTotalPaginas = iTotalPaginas + 1
End Sub

Private Sub ImprimeClientes(ByRef vp As VsPrinter)
Dim lcAdminCartera As Class_AdministracionCartera
Dim lcursor As hRecord
Dim lReg As hFields
Dim iFila As Integer
Dim iNumRegistro As Integer
Dim iTotalRegistros As Integer


    Set lcAdminCartera = New Class_AdministracionCartera
    With lcAdminCartera
      .Campo("Fecha_Inicio").Valor = Dtp_FechaReporte.Value
      .Campo("Fecha_Termino").Valor = Dtp_FechaHasta.Value
      .Campo("Id_TipoCuenta").Valor = oCliente.IdTipoCuenta
      .Campo("CtaVigente").Valor = IIf(OptCuentaCerrada.Value = True, 0, 1)
      If .BuscarClientes Then
        Set lcursor = .Cursor
        iTotalRegistros = lcursor.Count
      Else
        MsgBox "Problemas en cargar clientes." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
        Exit Sub
      End If
    End With
    Set lcAdminCartera = Nothing
    With vp
        iLineasImpresas = 0
        iNumRegistro = 0
        .FontSize = glb_tamletra_registros
        For Each lReg In lcursor
            
            iLineasImpresas = iLineasImpresas + .LineSpacing
            If (iLineasImpresas + 220 >= (.LineSpacing * (CONS_NUMERO_LINEAS - 3))) Then
                .EndTable
                .NewPage
                Call ImprimeEncabezado(vp)
                iLineasImpresas = .LineSpacing
            End If
            .TableCell(tcRows) = .TableCell(tcRows) + 1
        
            iFila = .TableCell(tcRows)
            
            iNumRegistro = iNumRegistro + 1
            .TableCell(tcText, iFila, COL_NUMERO - 1) = iNumRegistro
            .TableCell(tcText, iFila, COL_RUT - 1) = Trim(lReg("rut_cliente").Value)
            .TableCell(tcText, iFila, COL_NOMBRE - 1) = Trim(lReg("nombre_cliente").Value)
            .TableCell(tcText, iFila, COL_FECHA_INICIO_CONT - 1) = NVL(Trim(lReg("fecha_contrato").Value), "")
            If Me.OptCuentaVigente.Value Then
                .TableCell(tcText, iFila, COL_OBSER - 2) = NVL(Trim(lReg("observacion").Value), "")
            Else
                .TableCell(tcText, iFila, COL_FECHA_TERMIN_CONT - 1) = NVL(Trim(lReg("fecha_cierre_operativa").Value), "")
                .TableCell(tcText, iFila, COL_OBSER - 1) = NVL(Trim(lReg("observacion").Value), "")
            End If
            
            .TableCell(tcAlign, iFila, COL_NUMERO - 1) = taRightMiddle
            .TableCell(tcAlign, iFila, COL_RUT - 1) = taRightMiddle
            .TableCell(tcAlign, iFila, COL_NOMBRE - 1) = taLeftMiddle
            .TableCell(tcAlign, iFila, COL_FECHA_INICIO_CONT - 1) = taCenterMiddle
            If Me.OptCuentaVigente.Value Then
                .TableCell(tcAlign, iFila, COL_OBSER - 2) = taLeftMiddle
            Else
                .TableCell(tcAlign, iFila, COL_FECHA_TERMIN_CONT - 1) = taCenterMiddle
                .TableCell(tcAlign, iFila, COL_OBSER - 1) = taLeftMiddle
            End If
            
            If (iNumRegistro = iTotalRegistros) Then
                .EndTable
            End If
        Next
    End With
End Sub


Public Sub Sub_CargaCombo_TipoCuentas(pCombo As TDBCombo, _
                                       Optional ByVal pTag, _
                                       Optional pTodos As Boolean = False)
  
  Call Sub_CargaCombo_GENERICO_TClase(pCombo, _
                                      New Class_TipoCuentas, _
                                      "id_tipoCuenta", _
                                      "descripcion_larga", _
                                      pTag:=pTag)
                              
End Sub

Private Sub CreaExcel()
    Dim i           As Integer
    Dim hoja        As Integer
    Dim nFilaExcel  As Integer
        
    Set App_Excel = CreateObject("Excel.application")
    App_Excel.DisplayAlerts = False
    Set lLibro = App_Excel.Workbooks.Add
    
    App_Excel.Visible = False
    
    For i = 1 To lLibro.Worksheets.Count - 1
        lLibro.Worksheets(i).Delete
    Next
    
    lLibro.Worksheets.Add After:=lLibro.Worksheets(lLibro.Worksheets.Count)
    
    'NOMBRE HOJA
    lLibro.ActiveSheet.Name = "Reporte de Cuentas"
    lLibro.ActiveSheet.Cells(1, 1).Select
    lLibro.ActiveSheet.Pictures.Insert(gStrPictureEmpresa).ShapeRange.IncrementLeft 29.75
    
    nFilaExcel = HeaderXls(i)
        
    Genera_Informe_Excel i, nFilaExcel
    
    lLibro.ActiveSheet.Columns.AutoFit
    
    lLibro.ActiveSheet.Cells(1, 1).Select
       
    lLibro.Worksheets(1).Delete
    
    App_Excel.Visible = True
    App_Excel.UserControl = True
    
    Set App_Excel = Nothing
    Set lLibro = Nothing
End Sub

Private Sub Genera_Informe_Excel(ByVal hoja As Integer, ByRef iFilaExcel As Integer)
    Dim lxHoja      As Worksheet
    Dim lCursor_Cta As hRecord
    Dim lReg_Cta    As hFields
    Dim lForm       As Frm_Reporte_Generico
    Dim lcursor     As hRecord
    Dim lcAdminCartera As Class_AdministracionCartera

    Call Sub_Bloquea_Puntero(Me)
    iTotalPaginas = 0

    If Cmb_TiposCuentas.Text = "" Then
        MsgBox "Debe seleccionar Tipo de Cuenta", vbInformation, Me.Caption
            Call Sub_Desbloquea_Puntero(Me)
        Exit Sub
    End If

    oCliente.IdTipoCuenta = IIf(Fnt_ComboSelected_KEY(Cmb_TiposCuentas) = cCmbKALL, 0, Fnt_ComboSelected_KEY(Cmb_TiposCuentas))
    
    Set lcAdminCartera = New Class_AdministracionCartera
    With lcAdminCartera
        .Campo("Fecha_Inicio").Valor = Dtp_FechaReporte.Value
        .Campo("Fecha_Termino").Valor = Dtp_FechaHasta.Value
        .Campo("Id_TipoCuenta").Valor = oCliente.IdTipoCuenta
        .Campo("CtaVigente").Valor = IIf(OptCuentaCerrada.Value = True, 0, 1)

        If .BuscarClientes Then
            Set lcursor = .Cursor
            iTotalRegistros = lcursor.Count
        Else
            MsgBox "Problemas en cargar clientes." & vbCr & vbCr & .ErrMsg, vbCritical, Me.Caption
            Exit Sub
        End If
    End With

    With lLibro.ActiveSheet
        Rem Por cada cuenta encontrada
        Dim intItem As Integer
        intItem = 1
        If lcursor.Count > 0 Then
            For Each lReg_Cta In lcursor
                .Cells(iFilaExcel, COL_NUMERO).Value = intItem
                .Cells(iFilaExcel, COL_RUT).Value = NVL(lReg_Cta("RUT_CLIENTE").Value, "")
                .Cells(iFilaExcel, COL_NOMBRE).Value = NVL(lReg_Cta("NOMBRE_CLIENTE").Value, "")
                .Cells(iFilaExcel, COL_FECHA_INICIO_CONT).Value = NVL(lReg_Cta("FECHA_CONTRATO").Value, "")
                If Me.OptCuentaVigente.Value Then
                    .Cells(iFilaExcel, COL_OBSER - 1).Value = NVL(lReg_Cta("OBSERVACION").Value, "")
                Else
                    .Cells(iFilaExcel, COL_FECHA_TERMIN_CONT).Value = NVL(lReg_Cta("FECHA_CIERRE_OPERATIVA").Value, "")
                    .Cells(iFilaExcel, COL_OBSER).Value = NVL(lReg_Cta("OBSERVACION").Value, "")
                End If
                
                iFilaExcel = iFilaExcel + 1
                intItem = intItem + 1
            Next
        Else
            .Cells(iFilaExcel, 1).Value = "No hay Cuentas en el sistema."
        End If
    End With
    
ErrProcedure:
    Call Sub_Desbloquea_Puntero(Me)
End Sub

Function HeaderXls(ByVal hoja As Integer) As Integer
Dim intFilaInicioDatos As Integer
Dim sTitulo As String
    
    'lLibro.Worksheets(hoja).Select
    App_Excel.ActiveWindow.DisplayGridlines = False
    
    With lLibro.ActiveSheet
        sTitulo = "Registro de Clientes con " & Me.Cmb_TiposCuentas.Text
        
        .Range("B5").Value = sTitulo
        .Range("B5:E5").HorizontalAlignment = xlLeft
        .Range("B5:E5").Merge
        
        .Range("B7").Value = "Informe del " & Dtp_FechaReporte.Value & " al " & Dtp_FechaHasta.Value & IIf(Me.OptCuentaVigente.Value, " (Vigentes)", " (Cerradas)")
    
        .Range("B7:E7").Merge
        
        .Cells(FIL_ENCABEZADO, COL_NUMERO) = ""
        .Cells(FIL_ENCABEZADO, COL_RUT) = "RUT"
        .Cells(FIL_ENCABEZADO, COL_NOMBRE) = "Nombre o Raz�n Social"
        .Cells(FIL_ENCABEZADO, COL_FECHA_INICIO_CONT) = "Fecha Inicio Contrato"
        If Me.OptCuentaVigente.Value Then
            .Cells(FIL_ENCABEZADO, COL_OBSER - 1) = "Observaciones"
        Else
            .Cells(FIL_ENCABEZADO, COL_FECHA_TERMIN_CONT) = "Fecha T�rmino Contrato"
            .Cells(FIL_ENCABEZADO, COL_OBSER) = "Observaciones"
        End If
        
        .Range(.Cells(FIL_ENCABEZADO, COL_NUMERO), .Cells(FIL_ENCABEZADO, IIf(Me.OptCuentaVigente.Value, COL_OBSER - 1, COL_OBSER))).Interior.Color = RGB(255, 255, 0)
        .Range(.Cells(FIL_ENCABEZADO, COL_NUMERO), .Cells(FIL_ENCABEZADO, IIf(Me.OptCuentaVigente.Value, COL_OBSER - 1, COL_OBSER))).Font.Bold = True
        .Range(.Cells(FIL_ENCABEZADO, COL_NUMERO), .Cells(FIL_ENCABEZADO, IIf(Me.OptCuentaVigente.Value, COL_OBSER - 1, COL_OBSER))).BorderAround
        .Range(.Cells(FIL_ENCABEZADO, COL_NUMERO), .Cells(FIL_ENCABEZADO, IIf(Me.OptCuentaVigente.Value, COL_OBSER - 1, COL_OBSER))).Borders.Color = RGB(0, 0, 0)
        .Range(.Cells(FIL_ENCABEZADO, COL_NUMERO), .Cells(FIL_ENCABEZADO, IIf(Me.OptCuentaVigente.Value, COL_OBSER - 1, COL_OBSER))).HorizontalAlignment = xlCenter
        
        .Range("B:B").HorizontalAlignment = xlLeft
        .Range("C:C").HorizontalAlignment = xlRight
        .Range("D:D").HorizontalAlignment = xlLeft
        If Me.OptCuentaVigente.Value Then
            .Range("F:F").HorizontalAlignment = xlLeft
        Else
            .Range("E:F").HorizontalAlignment = xlCenter
            .Range("G:G").HorizontalAlignment = xlLeft
        End If
                
        .Range("B1:M" & FIL_ENCABEZADO).Font.Bold = True
        
        .Columns("A:A").ColumnWidth = 2
        .Columns("B:B").ColumnWidth = 25
        .Columns("C:C").ColumnWidth = 13
        .Columns("D:D").ColumnWidth = 10
        .Columns("E:E").ColumnWidth = 10
        If Me.OptCuentaVigente.Value Then
            .Columns("F:F").ColumnWidth = 15
        Else
            .Columns("F:F").ColumnWidth = 5
            .Columns("G:G").ColumnWidth = 15
        End If
        
         intFilaInicioDatos = FIL_ENCABEZADO + 1
    End With
    
    HeaderXls = intFilaInicioDatos ' Columna donde comienzan los datos
    
End Function
