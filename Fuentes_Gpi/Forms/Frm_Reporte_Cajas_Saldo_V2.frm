VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{CC225F54-31E2-494D-83EA-7C88B58F46B0}#8.0#0"; "tdbl8.ocx"
Begin VB.Form Frm_Reporte_Cajas_Saldo_V2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Reporte Saldos de Caja"
   ClientHeight    =   9000
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   6135
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   9000
   ScaleWidth      =   6135
   Begin VB.Frame Frm_Filtros 
      Caption         =   "B�squeda de Cuentas"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4665
      Left            =   60
      TabIndex        =   12
      Top             =   1230
      Width           =   6015
      Begin VB.Frame Frm_Propiedades_Cuentas 
         Caption         =   "Propiedades de la Cuenta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   1935
         Left            =   150
         TabIndex        =   14
         Top             =   270
         Width           =   5745
         Begin MSComctlLib.Toolbar Toolbar_Chequear_Propietario 
            Height          =   330
            Left            =   4320
            TabIndex        =   15
            Top             =   1470
            Width           =   1320
            _ExtentX        =   2328
            _ExtentY        =   582
            ButtonWidth     =   1958
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   1
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Caption         =   "Chequear"
                  Key             =   "CHK"
                  Object.ToolTipText     =   "Busca las cuentas seg�n los filtros"
               EndProperty
            EndProperty
         End
         Begin TrueDBList80.TDBCombo Cmb_Asesor 
            Height          =   345
            Left            =   1080
            TabIndex        =   2
            Top             =   300
            Width           =   4545
            _ExtentX        =   8017
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_Cajas_Saldo_V2.frx":0000
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Clientes 
            Height          =   345
            Left            =   1080
            TabIndex        =   3
            Top             =   690
            Width           =   4545
            _ExtentX        =   8017
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   0   'False
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_Cajas_Saldo_V2.frx":00AA
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin TrueDBList80.TDBCombo Cmb_Grupos_Cuentas 
            Height          =   345
            Left            =   1380
            TabIndex        =   4
            Top             =   1080
            Width           =   4245
            _ExtentX        =   7488
            _ExtentY        =   609
            _LayoutType     =   0
            _RowHeight      =   -2147483647
            _WasPersistedAsPixels=   0
            _DropdownWidth  =   0
            _EDITHEIGHT     =   609
            _GAPHEIGHT      =   53
            Columns(0)._VlistStyle=   0
            Columns(0)._MaxComboItems=   5
            Columns(0).DataField=   "ub_grid1"
            Columns(0)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns(1)._VlistStyle=   0
            Columns(1)._MaxComboItems=   5
            Columns(1).DataField=   "ub_grid2"
            Columns(1)._PropDict=   "_MaxComboItems,516,2;_VlistStyle,514,3"
            Columns.Count   =   2
            Splits(0)._UserFlags=   0
            Splits(0).ExtendRightColumn=   -1  'True
            Splits(0).AllowRowSizing=   0   'False
            Splits(0)._PropDict=   "_ColumnProps,515,0;_UserFlags,518,3"
            Splits(0)._ColumnProps(0)=   "Columns.Count=2"
            Splits(0)._ColumnProps(1)=   "Column(0).Width=2725"
            Splits(0)._ColumnProps(2)=   "Column(0).DividerColor=0"
            Splits(0)._ColumnProps(3)=   "Column(0)._WidthInPix=2646"
            Splits(0)._ColumnProps(4)=   "Column(0)._EditAlways=0"
            Splits(0)._ColumnProps(5)=   "Column(0).Order=1"
            Splits(0)._ColumnProps(6)=   "Column(1).Width=2725"
            Splits(0)._ColumnProps(7)=   "Column(1).DividerColor=0"
            Splits(0)._ColumnProps(8)=   "Column(1)._WidthInPix=2646"
            Splits(0)._ColumnProps(9)=   "Column(1)._EditAlways=0"
            Splits(0)._ColumnProps(10)=   "Column(1).Order=2"
            Splits.Count    =   1
            Appearance      =   3
            BorderStyle     =   1
            ComboStyle      =   0
            AutoCompletion  =   -1  'True
            LimitToList     =   -1  'True
            ColumnHeaders   =   0   'False
            ColumnFooters   =   0   'False
            DataMode        =   5
            DefColWidth     =   0
            Enabled         =   -1  'True
            HeadLines       =   1
            FootLines       =   1
            RowDividerStyle =   0
            Caption         =   ""
            EditFont        =   "Size=8.25,Charset=204,Weight=400,Underline=0,Italic=0,Strikethrough=0,Name=Arial"
            LayoutName      =   ""
            LayoutFileName  =   ""
            MultipleLines   =   0
            EmptyRows       =   0   'False
            CellTips        =   0
            AutoSize        =   0   'False
            ListField       =   ""
            BoundColumn     =   ""
            IntegralHeight  =   0   'False
            CellTipsWidth   =   0
            CellTipsDelay   =   1000
            AutoDropdown    =   -1  'True
            RowTracking     =   -1  'True
            RightToLeft     =   0   'False
            MouseIcon       =   0
            MouseIcon.vt    =   3
            MousePointer    =   0
            MatchEntryTimeout=   2000
            OLEDragMode     =   0
            OLEDropMode     =   0
            AnimateWindow   =   3
            AnimateWindowDirection=   5
            AnimateWindowTime=   200
            AnimateWindowClose=   1
            DropdownPosition=   0
            Locked          =   0   'False
            ScrollTrack     =   -1  'True
            ScrollTips      =   -1  'True
            RowDividerColor =   14215660
            RowSubDividerColor=   14215660
            MaxComboItems   =   10
            AddItemSeparator=   ";"
            _PropDict       =   $"Frm_Reporte_Cajas_Saldo_V2.frx":0154
            _StyleDefs(0)   =   "_StyleRoot:id=0,.parent=-1,.alignment=3,.valignment=0,.bgcolor=&H80000005&"
            _StyleDefs(1)   =   ":id=0,.fgcolor=&H80000008&,.wraptext=0,.locked=0,.transparentBmp=0"
            _StyleDefs(2)   =   ":id=0,.fgpicPosition=0,.bgpicMode=0,.appearance=0,.borderSize=0,.ellipsis=0"
            _StyleDefs(3)   =   ":id=0,.borderColor=&H80000005&,.borderType=0,.bold=0,.fontsize=825,.italic=0"
            _StyleDefs(4)   =   ":id=0,.underline=0,.strikethrough=0,.charset=204"
            _StyleDefs(5)   =   ":id=0,.fontname=Arial"
            _StyleDefs(6)   =   "Style:id=1,.parent=0,.namedParent=33,.bgcolor=&H80000005&"
            _StyleDefs(7)   =   "CaptionStyle:id=4,.parent=2,.namedParent=37"
            _StyleDefs(8)   =   "HeadingStyle:id=2,.parent=1,.namedParent=34"
            _StyleDefs(9)   =   "FooterStyle:id=3,.parent=1,.namedParent=35"
            _StyleDefs(10)  =   "InactiveStyle:id=5,.parent=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(11)  =   "SelectedStyle:id=6,.parent=1,.namedParent=36"
            _StyleDefs(12)  =   "EditorStyle:id=7,.parent=1"
            _StyleDefs(13)  =   "HighlightRowStyle:id=8,.parent=1,.namedParent=38"
            _StyleDefs(14)  =   "EvenRowStyle:id=9,.parent=1,.namedParent=39"
            _StyleDefs(15)  =   "OddRowStyle:id=10,.parent=1,.namedParent=40"
            _StyleDefs(16)  =   "RecordSelectorStyle:id=11,.parent=2,.namedParent=41"
            _StyleDefs(17)  =   "FilterBarStyle:id=12,.parent=1,.namedParent=42"
            _StyleDefs(18)  =   "Splits(0).Style:id=13,.parent=1"
            _StyleDefs(19)  =   "Splits(0).CaptionStyle:id=22,.parent=4"
            _StyleDefs(20)  =   "Splits(0).HeadingStyle:id=14,.parent=2"
            _StyleDefs(21)  =   "Splits(0).FooterStyle:id=15,.parent=3"
            _StyleDefs(22)  =   "Splits(0).InactiveStyle:id=16,.parent=5"
            _StyleDefs(23)  =   "Splits(0).SelectedStyle:id=18,.parent=6"
            _StyleDefs(24)  =   "Splits(0).EditorStyle:id=17,.parent=7"
            _StyleDefs(25)  =   "Splits(0).HighlightRowStyle:id=19,.parent=8"
            _StyleDefs(26)  =   "Splits(0).EvenRowStyle:id=20,.parent=9"
            _StyleDefs(27)  =   "Splits(0).OddRowStyle:id=21,.parent=10"
            _StyleDefs(28)  =   "Splits(0).RecordSelectorStyle:id=23,.parent=11"
            _StyleDefs(29)  =   "Splits(0).FilterBarStyle:id=24,.parent=12"
            _StyleDefs(30)  =   "Splits(0).Columns(0).Style:id=28,.parent=13"
            _StyleDefs(31)  =   "Splits(0).Columns(0).HeadingStyle:id=25,.parent=14"
            _StyleDefs(32)  =   "Splits(0).Columns(0).FooterStyle:id=26,.parent=15"
            _StyleDefs(33)  =   "Splits(0).Columns(0).EditorStyle:id=27,.parent=17"
            _StyleDefs(34)  =   "Splits(0).Columns(1).Style:id=32,.parent=13"
            _StyleDefs(35)  =   "Splits(0).Columns(1).HeadingStyle:id=29,.parent=14"
            _StyleDefs(36)  =   "Splits(0).Columns(1).FooterStyle:id=30,.parent=15"
            _StyleDefs(37)  =   "Splits(0).Columns(1).EditorStyle:id=31,.parent=17"
            _StyleDefs(38)  =   "Named:id=33:Normal"
            _StyleDefs(39)  =   ":id=33,.parent=0"
            _StyleDefs(40)  =   "Named:id=34:Heading"
            _StyleDefs(41)  =   ":id=34,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(42)  =   ":id=34,.wraptext=-1"
            _StyleDefs(43)  =   "Named:id=35:Footing"
            _StyleDefs(44)  =   ":id=35,.parent=33,.valignment=2,.bgcolor=&H8000000F&,.fgcolor=&H80000012&"
            _StyleDefs(45)  =   "Named:id=36:Selected"
            _StyleDefs(46)  =   ":id=36,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(47)  =   "Named:id=37:Caption"
            _StyleDefs(48)  =   ":id=37,.parent=34,.alignment=2"
            _StyleDefs(49)  =   "Named:id=38:HighlightRow"
            _StyleDefs(50)  =   ":id=38,.parent=33,.bgcolor=&H8000000D&,.fgcolor=&H8000000E&"
            _StyleDefs(51)  =   "Named:id=39:EvenRow"
            _StyleDefs(52)  =   ":id=39,.parent=33,.bgcolor=&HFFFF00&"
            _StyleDefs(53)  =   "Named:id=40:OddRow"
            _StyleDefs(54)  =   ":id=40,.parent=33"
            _StyleDefs(55)  =   "Named:id=41:RecordSelector"
            _StyleDefs(56)  =   ":id=41,.parent=34"
            _StyleDefs(57)  =   "Named:id=42:FilterBar"
            _StyleDefs(58)  =   ":id=42,.parent=33"
         End
         Begin VB.Label Lbl_Asesor 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Asesor"
            Height          =   345
            Left            =   90
            TabIndex        =   18
            Top             =   300
            Width           =   975
         End
         Begin VB.Label Lbl_Clientes 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Clientes"
            Height          =   345
            Left            =   90
            TabIndex        =   17
            Top             =   690
            Width           =   975
         End
         Begin VB.Label lbl_GruposCuentas 
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Grupos Cuentas"
            Height          =   345
            Left            =   90
            TabIndex        =   16
            Top             =   1080
            Width           =   1275
         End
      End
      Begin VB.Frame Frm_Cuentas 
         Caption         =   "Cuentas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   2325
         Left            =   150
         TabIndex        =   13
         Top             =   2220
         Width           =   5745
         Begin MSComctlLib.Toolbar Toolbar_Chequeo 
            Height          =   660
            Left            =   5190
            TabIndex        =   21
            Top             =   540
            Width           =   450
            _ExtentX        =   794
            _ExtentY        =   1164
            ButtonWidth     =   1138
            ButtonHeight    =   582
            AllowCustomize  =   0   'False
            Appearance      =   1
            Style           =   1
            TextAlignment   =   1
            _Version        =   393216
            BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
               NumButtons      =   2
               BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_ALL"
                  Description     =   "Agregar un nemotecnico a la Operaci�n"
                  Object.ToolTipText     =   "Selecciona todos los items"
               EndProperty
               BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
                  Key             =   "SEL_NOTHING"
                  Description     =   "Elimina el nemotecnico seleccionado de la Operaci�n"
                  Object.ToolTipText     =   "Deselecciona todos los items"
               EndProperty
            EndProperty
            Begin MSComctlLib.ProgressBar ProgressBar3 
               Height          =   255
               Left            =   9420
               TabIndex        =   22
               Top             =   30
               Width           =   780
               _ExtentX        =   1376
               _ExtentY        =   450
               _Version        =   393216
               Appearance      =   1
               Scrolling       =   1
            End
         End
         Begin VSFlex8LCtl.VSFlexGrid Grilla_Cuentas 
            Height          =   2025
            Left            =   60
            TabIndex        =   23
            Top             =   210
            Width           =   5085
            _cx             =   8969
            _cy             =   3572
            Appearance      =   2
            BorderStyle     =   1
            Enabled         =   -1  'True
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            MousePointer    =   0
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            BackColorFixed  =   -2147483633
            ForeColorFixed  =   -2147483630
            BackColorSel    =   65535
            ForeColorSel    =   0
            BackColorBkg    =   -2147483643
            BackColorAlternate=   -2147483643
            GridColor       =   -2147483633
            GridColorFixed  =   -2147483632
            TreeColor       =   -2147483632
            FloodColor      =   192
            SheetBorder     =   -2147483642
            FocusRect       =   2
            HighLight       =   1
            AllowSelection  =   -1  'True
            AllowBigSelection=   -1  'True
            AllowUserResizing=   1
            SelectionMode   =   3
            GridLines       =   10
            GridLinesFixed  =   2
            GridLineWidth   =   1
            Rows            =   2
            Cols            =   4
            FixedRows       =   1
            FixedCols       =   0
            RowHeightMin    =   0
            RowHeightMax    =   0
            ColWidthMin     =   0
            ColWidthMax     =   0
            ExtendLastCol   =   -1  'True
            FormatString    =   $"Frm_Reporte_Cajas_Saldo_V2.frx":01FE
            ScrollTrack     =   -1  'True
            ScrollBars      =   3
            ScrollTips      =   -1  'True
            MergeCells      =   0
            MergeCompare    =   0
            AutoResize      =   -1  'True
            AutoSizeMode    =   0
            AutoSearch      =   2
            AutoSearchDelay =   2
            MultiTotals     =   -1  'True
            SubtotalPosition=   1
            OutlineBar      =   0
            OutlineCol      =   0
            Ellipsis        =   1
            ExplorerBar     =   3
            PicturesOver    =   0   'False
            FillStyle       =   0
            RightToLeft     =   0   'False
            PictureType     =   0
            TabBehavior     =   0
            OwnerDraw       =   0
            Editable        =   2
            ShowComboButton =   1
            WordWrap        =   0   'False
            TextStyle       =   0
            TextStyleFixed  =   0
            OleDragMode     =   0
            OleDropMode     =   0
            ComboSearch     =   3
            AutoSizeMouse   =   -1  'True
            FrozenRows      =   0
            FrozenCols      =   0
            AllowUserFreezing=   0
            BackColorFrozen =   0
            ForeColorFrozen =   0
            WallPaperAlignment=   9
            AccessibleName  =   ""
            AccessibleDescription=   ""
            AccessibleValue =   ""
            AccessibleRole  =   24
         End
      End
   End
   Begin VB.Frame Frnm_Grilla_Sucesos 
      Caption         =   "Visor de Sucesos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   2685
      Left            =   60
      TabIndex        =   8
      Top             =   5940
      Width           =   6015
      Begin VB.Timer Timer 
         Enabled         =   0   'False
         Interval        =   1000
         Left            =   3720
         Top             =   2220
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Visor 
         Height          =   1905
         Left            =   90
         TabIndex        =   5
         Top             =   270
         Width           =   5775
         _cx             =   10186
         _cy             =   3360
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   -1  'True
         AllowBigSelection=   -1  'True
         AllowUserResizing=   0
         SelectionMode   =   3
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   3
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_Reporte_Cajas_Saldo_V2.frx":02A6
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   0   'False
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   1
         ExplorerBar     =   3
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   0
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   3
         AutoSizeMouse   =   -1  'True
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
      Begin MSComctlLib.Toolbar Toolbar_Detalle 
         Height          =   330
         Left            =   4620
         TabIndex        =   9
         Top             =   2250
         Width           =   1260
         _ExtentX        =   2223
         _ExtentY        =   582
         ButtonWidth     =   1588
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "Detalle"
               Key             =   "DETALLE"
               Object.ToolTipText     =   "Muestra el detalle de los sucesos del Cierre"
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frm_Fecha_Re_Proceso 
      Caption         =   "Fecha Re-Proceso de Cierre"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   795
      Left            =   60
      TabIndex        =   6
      Top             =   390
      Width           =   6015
      Begin MSComCtl2.DTPicker DTP_Fecha_Desde 
         Height          =   315
         Left            =   1470
         TabIndex        =   0
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   67174401
         CurrentDate     =   37732
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Hasta 
         Height          =   315
         Left            =   4500
         TabIndex        =   1
         Top             =   270
         Visible         =   0   'False
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   67174401
         CurrentDate     =   37732
      End
      Begin VB.Label Label1 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Hasta"
         Height          =   345
         Left            =   3240
         TabIndex        =   19
         Top             =   270
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label Lbl_Fecha_Desde 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha"
         Height          =   345
         Left            =   210
         TabIndex        =   7
         Top             =   270
         Width           =   1215
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   635
      ButtonWidth     =   3043
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Generar Reporte"
            Key             =   "CIERRE"
            Description     =   "Generar reporte"
            Object.ToolTipText     =   "Genera el reporte de Saldos de Caja"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   9420
         TabIndex        =   11
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   20
      Top             =   8655
      Width           =   6135
      _ExtentX        =   10821
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   159
            MinWidth        =   18
            Key             =   "PRIMERA"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   159
            MinWidth        =   18
            Key             =   "SEGUNDA"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
            Object.Width           =   159
            MinWidth        =   18
            Key             =   "TERCERA"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10213
            Key             =   "CUARTA"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Frm_Reporte_Cajas_Saldo_V2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fHoraInicio As Long

'----------------------------------------------------------------------------
Dim fFlg_Tipo_Permiso       As String 'Flags para el permiso sobre la ventana
Dim fCod_Arbol_Sistema      As String 'Codigo para el permiso sobre la ventana

Dim App_Excel       As Excel.Application      ' Excel application
Dim lLibro          As Excel.Workbook      ' Excel workbook

Private Type TCaja
    id_moneda_caja          As Long
    Cod_moneda_caja         As String
    dsc_moneda_caja         As String
    dicimales_mostrar_caja  As Integer
    simbolo_caja            As String
    monto_mon_caja          As Double
    id_moneda_cta           As Long
    monto_mon_cta           As Double
    Cod_moneda_cta          As String
    dsc_moneda_Cta          As String
    dicimales_mostrar_cta   As Integer
    simbolo_cta             As String
    dsc_caja_cuenta         As String
End Type

Private Type TCuenta
    id_cuenta       As Integer
    Num_Cuenta      As Integer
    abr_cuenta      As String
    dsc_cuenta      As String
    iTotalCajas     As Integer
    Asesor          As String
    Cajas()         As TCaja
    
End Type

Dim aCuentas() As TCuenta

Public Sub Mostrar(pCod_Arbol_Sistema)
  fCod_Arbol_Sistema = pCod_Arbol_Sistema
  fFlg_Tipo_Permiso = Fnt_CargaFormPermisos(pCod_Arbol_Sistema, Me)
  
  Call Form_Resize
  
  Load Me
  
End Sub
'----------------------------------------------------------------------------

Private Sub Form_Load()
    Dim lReg  As hCollection.hFields
    Dim lLinea As Long

    With Toolbar
        Set .ImageList = MDI_Principal.ImageListGlobal16
            .Buttons("CIERRE").Image = cBoton_Aceptar
            .Buttons("EXIT").Image = cBoton_Salir
    End With
  
  With Toolbar_Chequear_Propietario
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("CHK").Image = cBoton_Agregar_Grilla
  End With
  
  With Toolbar_Chequeo
   Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
      .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
      .Appearance = ccFlat
  End With
   
  With Toolbar_Detalle
  Set .ImageList = MDI_Principal.ImageListGlobal16
      .Buttons("DETALLE").Image = cBoton_Agregar_Grilla
  End With
  
  Call Sub_CargaForm
  
  Me.Top = 1
  Me.Left = 1
End Sub

Private Sub Sub_CargaForm()
    Dim lCierre As Class_Verificaciones_Cierre
'    Dim lcCuenta As Class_Cuentas
    Dim lcCuenta As Object
    Dim lReg As hFields
    Dim lLinea As Long


    Call Sub_FormControl_Color(Me.Controls)

    Rem Limpia las grillas
    Grilla_Cuentas.Rows = 1
    Grilla_Visor.Rows = 1

    Rem La hora que se propone es la del �ltimo dia de cierre
    Set lCierre = New Class_Verificaciones_Cierre
  
    DTP_Fecha_Hasta.Value = lCierre.Busca_Ultima_FechaCierre
    DTP_Fecha_Desde.Value = lCierre.Busca_Ultima_FechaCierre
  
    Rem Carga los combos con el primer elemento vac�o
    Call Sub_CargaCombo_Asesor(Cmb_Asesor, pBlanco:=True)
    Call Sub_CargaCombo_Clientes(Cmb_Clientes, pBlanco:=True)
    Call Sub_CargaCombo_Grupos_Cuentas(Cmb_Grupos_Cuentas, pBlanco:=True)

    Rem Carga las cuentas habilitadas y de la empresa en la grilla
    
'    Set lcCuenta = New Class_Cuentas
    
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    
    lcCuenta.Campo("Cod_Estado").Valor = cCod_Estado_Habilitada
    lcCuenta.Campo("Id_Empresa").Valor = Fnt_EmpresaActual

'    If lcCuenta.Buscar_Vigentes Then
'        lcCuenta.Campo("id_empresa").Valor = Fnt_EmpresaActual
'        For Each lReg In lcCuenta.Cursor
'            With Grilla_Cuentas
'                lLinea = .Rows
'                .AddItem ""
'                Call SetCell(Grilla_Cuentas, lLinea, "colum_pk", lReg("Id_Cuenta").Value)
'                'Call SetCell(Grilla_Cuentas, lLinea, "NUM_CUENTA", lReg("Num_Cuenta").Value & " - " & lReg("Dsc_Cuenta").Value)
'                Call SetCell(Grilla_Cuentas, lLinea, "NUM_CUENTA", lReg("Num_Cuenta").Value & "   --   " & lReg("ABR_CUENTA").Value & " ---   " & lReg("Dsc_Cuenta").Value)
'            End With
'        Next
'    Else
'        Call Fnt_MsgError(lcCuenta.SubTipo_LOG _
'                      , "Problema con buscar las cuentas vigentes." _
'                      , lcCuenta.ErrMsg _
'                      , pConLog:=True)
'        Err.Clear
'    End If
'  Set lcCuenta = Nothing
'    Set lcCuenta = New Class_Cuentas

    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    
    With lcCuenta
        .Campo("Cod_Estado").Valor = cCod_Estado_Habilitada
        .Campo("Id_Empresa").Valor = Fnt_EmpresaActual

        If .Buscar_Vigentes Then
            Call Sub_hRecord2Grilla(lcCuenta.Cursor, Grilla_Cuentas, "id_cuenta")
        Else
            Call Fnt_MsgError(.SubTipo_LOG _
                                , "Problema con buscar las cuentas vigentes." _
                                , .ErrMsg _
                                , pConLog:=True)
            Err.Clear
        End If
    End With
    Set lcCuenta = Nothing
  
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Grilla_Cuentas_BeforeEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
  If Col = Grilla_Cuentas.ColIndex("NUM_CUENTA") Then
    Cancel = True
  End If
End Sub

Private Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "CIERRE"
      Call Sub_informe
    Case "EXIT"
      Unload Me
  End Select
End Sub

Private Sub Toolbar_Chequear_Propietario_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "CHK"
      Call Sub_Busca_Cuentas_Propiedades
  End Select
End Sub

Private Sub Toolbar_Chequeo_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "SEL_ALL"
       Call Sub_CambiaCheck(True)
    Case "SEL_NOTHING"
       Call Sub_CambiaCheck(False)
       Grilla_Visor.Rows = 1
   End Select
End Sub

Private Sub Toolbar_Detalle_ButtonClick(ByVal Button As MSComctlLib.Button)
  Me.SetFocus
  DoEvents

  Select Case Button.Key
    Case "DETALLE"
      Call Grilla_Visor_DblClick
  End Select
End Sub

Private Sub Sub_Busca_Cuentas_Propiedades()
    Dim lCuentas            As Class_Re_Proceso_Cierre
    Dim lCursor             As hCollection.hRecord
    Dim lReg                As hCollection.hFields
    Dim lId_Asesor          As String
    Dim lId_Cliente         As String
    Dim lId_Grupos_Cuentas  As String
  
    Call Sub_Bloquea_Puntero(Me)
  
    lId_Asesor = Fnt_ComboSelected_KEY(Cmb_Asesor)
    lId_Asesor = IIf(lId_Asesor = cCmbBLANCO, "", lId_Asesor)
    
    lId_Cliente = Fnt_ComboSelected_KEY(Cmb_Clientes)
    lId_Cliente = IIf(lId_Cliente = cCmbBLANCO, "", lId_Cliente)
    
    lId_Grupos_Cuentas = Fnt_ComboSelected_KEY(Cmb_Grupos_Cuentas)
    lId_Grupos_Cuentas = IIf(lId_Grupos_Cuentas = cCmbBLANCO, "", lId_Grupos_Cuentas)
  
    Set lCuentas = New Class_Re_Proceso_Cierre
  
    If lCuentas.Fnt_Busca_Cuentas_Propiedades(lId_Asesor, _
                                              lId_Cliente, _
                                              lId_Grupos_Cuentas, _
                                              lCursor) Then
        For Each lReg In lCursor
            Call Sub_Llena_Grilla_Cuentas(lReg("id_cuenta").Value)
        Next
    End If

    Call Sub_Desbloquea_Puntero(Me)
    
End Sub

Private Sub Sub_CambiaCheck(pValor As Boolean)
    Dim lLinea      As Long
    Dim lCol        As Long
  
    lCol = Grilla_Cuentas.ColIndex("CHK")
    If pValor Then
        For lLinea = 1 To Grilla_Cuentas.Rows - 1
            Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexChecked
        Next
    Else
        For lLinea = 1 To Grilla_Cuentas.Rows - 1
            Grilla_Cuentas.Cell(flexcpChecked, lLinea, lCol) = flexUnchecked
        Next
    End If
  
End Sub

Private Sub Sub_Llena_Grilla_Cuentas(pId_Cuenta As String)
    Dim lFila As Long
    Dim lCol As Long
  
    With Grilla_Cuentas
        If .Rows > 0 Then
            lCol = Grilla_Cuentas.ColIndex("CHK")
            For lFila = 1 To .Rows - 1
                If GetCell(Grilla_Cuentas, lFila, "colum_pk") = pId_Cuenta Then
                    .Cell(flexcpChecked, lFila, lCol) = flexChecked
                    Exit For
                End If
            Next
        End If
  End With
  
End Sub

Private Function Fnt_Validaciones() As Boolean
    Rem Verifica que existan cuentas habilitadas
    If Grilla_Cuentas.Rows > 0 Then
        Rem Luego valida que existan cuentas chequeadas
        Fnt_Validaciones = Fnt_Cuentas_Chequeadas
    Else
        Fnt_Validaciones = False
        MsgBox "No hay cuentas habilitadas en el sistema.", vbCritical, Me.Caption
    End If
End Function

Private Function Fnt_Cuentas_Chequeadas() As Boolean
    Dim lHay_Chequeados As Boolean
    Dim lCol As Long
    Dim lFila As Long

    lHay_Chequeados = False
    lCol = Grilla_Cuentas.ColIndex("CHK")
  
    With Grilla_Cuentas
        For lFila = 1 To .Rows - 1
            If .Cell(flexcpChecked, lFila, lCol) = flexChecked Then
                lHay_Chequeados = True
                Exit For
            End If
        Next
    End With
  
    If lHay_Chequeados Then
        Fnt_Cuentas_Chequeadas = True
    Else
        Fnt_Cuentas_Chequeadas = False
        MsgBox "No hay cuentas chequeadas para mostrar en el informe.", vbCritical, Me.Caption
    End If
    
End Function

Private Sub Sub_informe()
    Dim lcComision_Hono_Ase_Cuenta  As Class_Comisiones_Hono_Ase_Cuenta
    Dim lcComi_Fija_Hono_Ase_Cuenta As Class_Comi_Fija_Hono_Ase_Cuenta
    
    Dim oSaldosCaja     As Class_Saldos_Caja
'    Dim lcCuenta         As Class_Cuentas
    Dim lcCuenta        As Object
        
    'Dim lfCuenta        As hFields
    'Dim lhCierres       As hRecord
    Dim lhCajas         As hFields
    'Dim lfCierre        As hFields
    '--------------------------------------------
    Dim lFila           As Long
    Dim lCol            As Long
    Dim lId_Cuenta      As String
    Dim lFecha_Proceso  As Date
    Dim lTiempo         As Long
    
    Dim nFila   As Integer
    Dim i       As Integer
    
    Dim bPrimera As Boolean
    Dim nCuenta As Integer
    
    ReDim Preserve aCuentas(0)

    Call Sub_Bloquea_Puntero(Me)
    Me.Enabled = False
  
    If Not Fnt_Cuentas_Chequeadas Then
        GoTo ExitProcedure
    End If
    
    Grilla_Visor.Rows = 1
    Timer.Enabled = True
    fHoraInicio = GetTickCount
    
    lCol = Grilla_Cuentas.ColIndex("CHK")
    
    Call Fnt_Escribe_Grilla(Grilla_Visor, "N", Fnt_SYSDATE & " Buscando Cuentas.")
    
    For lFila = 1 To Grilla_Cuentas.Rows - 1
        If Grilla_Cuentas.Cell(flexcpChecked, lFila, lCol) = flexChecked Then
    
            lId_Cuenta = GetCell(Grilla_Cuentas, lFila, "colum_pk")
            
            Set oSaldosCaja = New Class_Saldos_Caja
            
            With oSaldosCaja
                If Not .Buscar_Saldo_Informe(pId_Cuenta:=lId_Cuenta, pFecha_Cierre:=DTP_Fecha_Desde.Value) Then
                    Call Fnt_Escribe_Grilla(Grilla_Visor, _
                                            cEstado_Log_Error, _
                                            "Problemas en buscar las cuentas." & vbLf & vbLf & .ErrMsg)
                    GoTo ExitProcedure
                Else
                    bPrimera = True
                    
                    nFila = UBound(aCuentas) + 1
                    
                    If oSaldosCaja.Cursor.Count = 0 Then
                        ReDim Preserve aCuentas(nFila)
                        
                        aCuentas(nFila).id_cuenta = lId_Cuenta
                        
'                        Set lcCuenta = New Class_Cuentas
                        Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
                        lcCuenta.Campo("id_cuenta").Valor = lId_Cuenta
                        lcCuenta.Campo("id_Empresa").Valor = Fnt_EmpresaActual
                        
                        If lcCuenta.Buscar_Vigentes Then
                            If lcCuenta.Cursor.Count > 0 Then
                                aCuentas(nFila).dsc_cuenta = lcCuenta.Cursor(1)("dsc_cuenta").Value
                                aCuentas(nFila).Num_Cuenta = lcCuenta.Cursor(1)("num_cuenta").Value
                            Else
                                aCuentas(nFila).dsc_cuenta = "No existe Cuenta"
                                aCuentas(nFila).Num_Cuenta = "No existe"
                            End If
                        Else
                            aCuentas(nFila).dsc_cuenta = "No existe Cuenta"
                            aCuentas(nFila).Num_Cuenta = "No existe"
                        End If

                        Set lcCuenta = Nothing
                        
                        aCuentas(nFila).iTotalCajas = 0
                        
                    Else
                    
                        For Each lhCajas In oSaldosCaja.Cursor
                            If bPrimera Then
                                bPrimera = False
                                ReDim Preserve aCuentas(nFila)
                                
                                aCuentas(nFila).id_cuenta = lhCajas("id_cuenta").Value
                                aCuentas(nFila).abr_cuenta = lhCajas("abr_cuenta").Value
                                aCuentas(nFila).dsc_cuenta = lhCajas("dsc_cuenta").Value
                                aCuentas(nFila).Num_Cuenta = lhCajas("num_cuenta").Value
                                aCuentas(nFila).Asesor = lhCajas("Asesor").Value
                                
                                ReDim Preserve aCuentas(nFila).Cajas(oSaldosCaja.Cursor.Count)
                                
                                aCuentas(nFila).iTotalCajas = oSaldosCaja.Cursor.Count
                                
                                nCuenta = 0
                            End If
                            
                            aCuentas(nFila).Cajas(nCuenta).id_moneda_caja = lhCajas("id_moneda_caja").Value
                            aCuentas(nFila).Cajas(nCuenta).Cod_moneda_caja = lhCajas("Cod_moneda_caja").Value
                            aCuentas(nFila).Cajas(nCuenta).dsc_moneda_caja = lhCajas("dsc_moneda_caja").Value
                            aCuentas(nFila).Cajas(nCuenta).dicimales_mostrar_caja = lhCajas("dicimales_mostrar_caja").Value
                            aCuentas(nFila).Cajas(nCuenta).simbolo_caja = lhCajas("simbolo_caja").Value
                            aCuentas(nFila).Cajas(nCuenta).monto_mon_caja = lhCajas("monto_mon_caja").Value
                            aCuentas(nFila).Cajas(nCuenta).id_moneda_cta = lhCajas("id_moneda_cta").Value
                            aCuentas(nFila).Cajas(nCuenta).monto_mon_cta = lhCajas("monto_mon_cta").Value
                            aCuentas(nFila).Cajas(nCuenta).Cod_moneda_cta = lhCajas("Cod_moneda_cta").Value
                            aCuentas(nFila).Cajas(nCuenta).dsc_moneda_Cta = lhCajas("dsc_moneda_Cta").Value
                            aCuentas(nFila).Cajas(nCuenta).dicimales_mostrar_cta = lhCajas("dicimales_mostrar_cta").Value
                            aCuentas(nFila).Cajas(nCuenta).simbolo_cta = lhCajas("simbolo_cta").Value
                            aCuentas(nFila).Cajas(nCuenta).dsc_caja_cuenta = lhCajas("dsc_caja_cuenta").Value
                            
                            nCuenta = nCuenta + 1
                        Next
                    End If
                    
                End If
                
                Set oSaldosCaja = Nothing
                
            End With
            
        End If
    Next
    
    Call Fnt_Escribe_Grilla(Grilla_Visor, "N", "Creando Planilla Excel")
    
    Crea_Excel
    
    Call Fnt_Escribe_Grilla(Grilla_Visor, "N", Fnt_SYSDATE & " Fin de Informe Saldos Cuentas.")
  
    lTiempo = ((GetTickCount - fHoraInicio) / 1000)
 '    MsgBox "Terminado el Re-Calculo de Comisiones." & vbCr & Format(lTiempo, "#,###.##") & " segundos.", vbInformation, Me.Caption
 
ExitProcedure:
    Timer.Enabled = False
  
    Set oSaldosCaja = Nothing
    Call Sub_Desbloquea_Puntero(Me)

    Me.Enabled = True
End Sub

Private Sub Crea_Excel()
    Dim nHojas, i As Integer
    Dim hoja As Integer
    Dim Index As Integer
    
    
    
    Set App_Excel = CreateObject("Excel.application")
    App_Excel.DisplayAlerts = False
    Set lLibro = App_Excel.Workbooks.Add
    
    ' App_Excel.Visible = True
    
    For i = lLibro.Worksheets.Count To lLibro.Worksheets.Count - 1 Step -1
        lLibro.Worksheets(i).Delete
    Next i
    
    lLibro.Worksheets.Add
   
    For hoja = 1 To lLibro.Worksheets.Count
        lLibro.Sheets(hoja).Select
        lLibro.ActiveSheet.Pictures.Insert(gStrPictureEmpresaGrande).ShapeRange.IncrementLeft 39.75
        ' lLibro.Worksheets.Item(hoja).Range("B6").Value = "Saldos de Caja"
        lLibro.Worksheets.Item(hoja).Range("B6").Font.Bold = True
        lLibro.Worksheets.Item(hoja).Columns("A:A").ColumnWidth = 2
        lLibro.Worksheets.Item(hoja).Columns("B:B").ColumnWidth = 8
        lLibro.Worksheets.Item(hoja).Columns("C:C").ColumnWidth = 15
        lLibro.Worksheets.Item(hoja).Columns("D:D").ColumnWidth = 35
    Next hoja
    
'
'    For hoja = 1 To lLibro.Worksheets.Count
        HeaderXls_Informe_Portada 1
        Genera_Informe 1
'    Next hoja
    
    App_Excel.Visible = True
    App_Excel.UserControl = True
    
End Sub

Sub HeaderXls_Informe_Portada(ByVal hoja As Integer)
    Dim lFila    As Integer
    Dim lColumna As Integer
    
    lLibro.Sheets(hoja).Select
    App_Excel.ActiveWindow.DisplayGridlines = False
    
    'NOMBRE HOJA
    lLibro.Worksheets.Item(hoja).Name = "Saldos"
    
    'TITULO
    lLibro.ActiveSheet.Range("G5").Value = "Fecha Generaci�n Reporte : " & Fnt_FechaServidor
    lLibro.ActiveSheet.Range("B7").Value = "Informe de Saldos de Caja al " & DTP_Fecha_Desde.Value
    ' lLibro.ActiveSheet.Range("D8").Value = lPeriodo
    lLibro.ActiveSheet.Range("B7:D8").Font.Bold = True
    lLibro.ActiveSheet.Range("B7:G7").HorizontalAlignment = xlLeft
    lLibro.ActiveSheet.Range("B7:G7").Merge
    lLibro.ActiveSheet.Range("D8:G8").HorizontalAlignment = xlCenter
    lLibro.ActiveSheet.Range("D8:G8").Merge
    
    'ENCABEZADO

End Sub


Sub Genera_Informe(ByVal hoja As Integer)
    Dim lReg            As hCollection.hFields
    Dim lSaldos_Activos As Class_Saldo_Activos
    '----------------------------------------------------
    Dim lxHoja          As Worksheet
    '----------------------------------------------------
    Dim lFila            As Integer
    Dim lColumna         As Integer
    Dim lFechaCierre    As Variant
    
    Dim iTotalCajas As Integer
    
    Dim iFila As Integer
    Dim iCol As Integer
    
    Dim iFilaExcel As Integer
    Dim iColExcel As Integer
    
    Dim sEncabezadoColumna As String
    Dim sValor As String
    Dim nDecimales As Integer
    Dim nMaxCols As Integer
    
    Dim lRutCliente As String
    Dim lNombreCliente As String

    On Error GoTo ErrProcedure

    Set lSaldos_Activos = New Class_Saldo_Activos

    Set lxHoja = lLibro.Sheets(hoja)
    
    Call lxHoja.Select

    lColumna = 3
      
    iFilaExcel = 11
    iColExcel = 6
    
    '**************************************************************
    ' Busca la Cantidad cajas que puede poseer una cuenta
    '**************************************************************
    iTotalCajas = 0
    For iFila = 1 To UBound(aCuentas)
        If iTotalCajas < aCuentas(iFila).iTotalCajas Then
            iTotalCajas = aCuentas(iFila).iTotalCajas
            lFila = iFila
        End If
    Next

    '**************************************************************
    ' Setea los encabezados de columna
    '**************************************************************
    If iTotalCajas = 0 Then
        nMaxCols = -1
    Else
        nMaxCols = UBound(aCuentas(lFila).Cajas) - 1
    End If
    
    lxHoja.Cells(iFilaExcel, 2).Value = "Cuenta "
    lxHoja.Cells(iFilaExcel, 3).Value = "Rut Cliente"
    lxHoja.Cells(iFilaExcel, 4).Value = "Nombre Cliente"
    lxHoja.Cells(iFilaExcel, 5).Value = "Nombre Asesor"
    For iCol = 0 To nMaxCols
        sEncabezadoColumna = aCuentas(lFila).Cajas(iCol).dsc_caja_cuenta & _
                                "(" & aCuentas(lFila).Cajas(iCol).Cod_moneda_caja & ")"
                                
        lxHoja.Cells(iFilaExcel, iColExcel + iCol).Value = sEncabezadoColumna
        
    Next
    If nMaxCols = -1 Then
        lxHoja.Cells(iFilaExcel + 1, 2).Value = "No Hay Movimientos "
        iFila = 1
        
        With lxHoja
            '************************************************************************************
            ' Rayas Verticales
            '************************************************************************************
            .Range(.Cells(iFilaExcel + 1, 2), .Cells(iFilaExcel + 1, 3)).Borders.Color = RGB(0, 0, 0)
            .Range(.Cells(iFilaExcel + 1, 2), .Cells(iFilaExcel + 1, 3)).Borders(xlInsideVertical).LineStyle = xlNone
            
            .Range(.Cells(iFilaExcel, 2), .Cells(iFilaExcel, 3)).BorderAround
            .Range(.Cells(iFilaExcel, 2), .Cells(iFilaExcel, 3)).Borders.Color = RGB(0, 0, 0)
            .Range(.Cells(iFilaExcel, 2), .Cells(iFilaExcel, 3)).Interior.Color = RGB(255, 255, 0)
            .Range(.Cells(iFilaExcel, 2), .Cells(iFilaExcel, 3)).Font.Bold = True
            
            .Range(.Cells(iFilaExcel, 2), .Cells(iFilaExcel + 1, 3)).EntireColumn.AutoFit
        End With
        
    Else
        '**************************************************************
        ' Pone los valores en el libro
        '**************************************************************
        For iFila = 1 To UBound(aCuentas)
            Sub_BuscaDatosCliente aCuentas(iFila).id_cuenta, lRutCliente, lNombreCliente
            lxHoja.Cells(iFila + iFilaExcel, 2).Value = aCuentas(iFila).Num_Cuenta
            lxHoja.Cells(iFila + iFilaExcel, 3).Value = lRutCliente
            lxHoja.Cells(iFila + iFilaExcel, 4).Value = lNombreCliente
            lxHoja.Cells(iFila + iFilaExcel, 5).Value = aCuentas(iFila).Asesor
            
            For iCol = 0 To aCuentas(iFila).iTotalCajas - 1
                nDecimales = aCuentas(iFila).Cajas(iCol).dicimales_mostrar_caja
                If nDecimales = 0 Then
                     sValor = "#,##0"
                Else
                    sValor = "#,##0." & String(nDecimales, "0")
                End If
                
                lxHoja.Cells(iFila + iFilaExcel, iColExcel + iCol).Value = aCuentas(iFila).Cajas(iCol).monto_mon_caja
                lxHoja.Cells(iFila + iFilaExcel, iColExcel + iCol).NumberFormat = sValor
            Next
        Next
        
        With lxHoja
            '************************************************************************************
            ' Rayas Verticales
            '************************************************************************************
            For iCol = 2 To iColExcel + nMaxCols
                .Range(.Cells(iFilaExcel, iCol), .Cells(iFila + iFilaExcel, iCol)).BorderAround
                .Range(.Cells(iFilaExcel, iCol), .Cells(iFila + iFilaExcel, iCol)).Borders.Color = RGB(0, 0, 0)
                .Range(.Cells(iFilaExcel, iCol), .Cells(iFila + iFilaExcel, iCol)).Borders(xlInsideHorizontal).LineStyle = xlNone
            Next
            
            .Range(.Cells(iFilaExcel, 2), .Cells(iFilaExcel, iColExcel + nMaxCols)).BorderAround
            .Range(.Cells(iFilaExcel, 2), .Cells(iFilaExcel, iColExcel + nMaxCols)).Borders.Color = RGB(0, 0, 0)
            .Range(.Cells(iFilaExcel, 2), .Cells(iFilaExcel, iColExcel + nMaxCols)).Interior.Color = RGB(255, 255, 0)
            .Range(.Cells(iFilaExcel, 2), .Cells(iFilaExcel, iColExcel + nMaxCols)).Font.Bold = True
            
            .Range(.Cells(iFilaExcel, 2), .Cells(iFila + iFilaExcel, iColExcel + nMaxCols)).EntireColumn.AutoFit
            
        End With
        
    End If
    
    lxHoja.Cells(1, 1).Select
    
ErrProcedure:
  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLS_ErrSystem, "Problemas con el informe de saldos de caja.", Err.Description, pConLog:=True)
    Err.Clear
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:

  ' Set lcInforme_Control_Gestion = Nothing
End Sub
Private Sub Grilla_Visor_DblClick()
    Dim lMensaje As String

    With Grilla_Visor
      If .Row > 0 Then
        lMensaje = GetCell(Grilla_Visor, .Row, "TEXTO")
        If Not lMensaje = "" Then
          MsgBox lMensaje, vbInformation, Me.Caption
        End If
      End If
    End With

End Sub

Private Sub Timer_Timer()
    Dim lTiempo As Long
    lTiempo = ((GetTickCount - fHoraInicio) / 1000)
    Call Sub_StatusWindows(StatusBar, pTercera:=Format(lTiempo, "#,###.##") & " seg. trans.")
End Sub

Private Sub Sub_BuscaDatosCliente(pIdCuenta As Integer, ByRef pRutCliente As String, ByRef pNombreCliente As String)
Dim lcCuenta As Object
    
    Set lcCuenta = Fnt_CreateObject(cDLL_Cuentas)
    pNombreCliente = "No Registra Nombre"
    pRutCliente = "No Registra Rut"
    With lcCuenta
        .Campo("id_cuenta").Valor = pIdCuenta
        .Campo("id_Empresa").Valor = Fnt_EmpresaActual
        
        If .Buscar_Vigentes Then
          If .Cursor.Count > 0 Then
              pNombreCliente = .Cursor(1)("nombre_cliente").Value
              pRutCliente = .Cursor(1)("rut_cliente").Value
          End If
        Else
            Call Fnt_MsgError(.SubTipo_LOG _
                              , "Problemas con la busqueda de los datos de la cuenta (" & pIdCuenta & ")." _
                              , .ErrMsg _
                              , pConLog:=True)
        End If
        
    End With

    Set lcCuenta = Nothing
End Sub

