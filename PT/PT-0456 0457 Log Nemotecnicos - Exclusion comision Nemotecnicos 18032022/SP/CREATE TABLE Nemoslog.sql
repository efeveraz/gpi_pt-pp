IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE name = 'Nemoslog' AND XTYPE='U')
	DROP TABLE Nemoslog
GO
CREATE TABLE [dbo].[Nemoslog](
	[IdNemoLog] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	Nemotecnico varchar(200) null,
	[IdUsuarioInsert] [numeric](10, 0) NULL,
	[FechaInsert] [datetime] NULL,
	[IdUsuarioMod] [numeric](10, 0) NULL,
	[CodEstado] [varchar](3) NULL,
	Accion varchar(10) null,
	[Cadena] [varchar](4000) NULL,
 CONSTRAINT [PK_Nemoslog] PRIMARY KEY CLUSTERED 
(
	[IdNemoLog] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


