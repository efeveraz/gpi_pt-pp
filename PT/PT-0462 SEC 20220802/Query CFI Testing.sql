--USE [CisCB]
--GO
--/****** Object:  StoredProcedure [dbo].[AD_QRY_CB_MOVTO_ACCIONES]    Script Date: 08/24/2022 16:57:29 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

--ALTER PROCEDURE [dbo].[AD_QRY_CB_MOVTO_ACCIONES]
--@PFECHA_MOVIMIENTOS CHAR(8)  -- YYYYMMDD
--AS
--BEGIN
declare
@PFECHA_MOVIMIENTOS CHAR(8)  -- YYYYMMDD
select @PFECHA_MOVIMIENTOS = '20200203'
    SET DATEFORMAT YMD
    SET NOCOUNT ON

    DECLARE @VFECHA_MOVIMIENTOS INT, @MSGERR CHAR(80)

    SET @MSGERR = ''
    IF ISDATE(@PFECHA_MOVIMIENTOS) = 0
       SET @MSGERR = 'FECHA INVÁLIDA (' + RTRIM(@PFECHA_MOVIMIENTOS)
    IF @MSGERR=''
    BEGIN
        IF RTRIM(@PFECHA_MOVIMIENTOS) = ''
            SET @MSGERR = 'DEBE INGRESAR UNA FECHA (YYYYMMDD)'
        ELSE
            SET @VFECHA_MOVIMIENTOS = (CAST(CONVERT(DATETIME, @PFECHA_MOVIMIENTOS) AS INT)+693596)
    END

    DECLARE @VTIPO_PERSONA CHAR(2),
            @VRAZON_SOCIAL VARCHAR(120),
            @VAPELLIDO_PATERNO VARCHAR(40),
            @VAPELLIDO_MATERNO VARCHAR(40),
            @VNOMBRES VARCHAR(40)

    CREATE TABLE #TMP_ACCIONES
              (
                 T_RUT_CLIENTE       CHAR(15),
                 T_TIPO_OPERACION    CHAR(1),
                 T_NUMERO_ORDEN      INT,
                 T_CORRELATIVO       INT,
                 T_NEMOTECNICO       VARCHAR(20),
                 T_CANTIDAD          FLOAT,
                 T_PRECIO            FLOAT,
                 T_MONTO_NETO        FLOAT,
                 T_COMISION          FLOAT,
                 T_DERECHO           FLOAT,
                 T_GASTOS            FLOAT,
                 T_IVA               FLOAT,
                 T_MONTO_OPERACION   FLOAT,
                 T_FECHA_LIQUIDACION DATETIME,
                 T_ID_CIERRE         VARCHAR(10),
                 T_NOMBRE_CLIENTE    VARCHAR(120),
                 T_NUMERO_FACTURA    INT,
                 T_TIPO_LIQUIDACION  VARCHAR(2),
                 T_BOLSA             VARCHAR(1),
                 T_CUSTODIA_NOMINAL  VARCHAR(1),
                 T_MONEDA            VARCHAR(3)
              )

    IF @MSGERR = ''
    BEGIN
        DECLARE
        @ACC_RUT_CLIENTE       CHAR(15),
        @ACC_TIPO_OPERACION    CHAR(1),
        @ACC_NUMERO_ORDEN      INT,
        @ACC_CORRELATIVO       INT,
        @ACC_NEMOTECNICO       VARCHAR(20),
        @ACC_CANTIDAD          FLOAT,
        @ACC_PRECIO            FLOAT,
        @ACC_MONTO_NETO        FLOAT,
        @ACC_COMISION          FLOAT,
        @ACC_DERECHO           FLOAT,
        @ACC_GASTOS            FLOAT,
        @ACC_IVA               FLOAT,
        @ACC_MONTO_OPERACION   FLOAT,
        @ACC_FECHA_LIQUIDACION DATETIME,
        @ACC_ID_CIERRE         VARCHAR(10),
        @ACC_INROFACTURA       INT,
        @ACC_TIPO_LIQ          VARCHAR(2),
        @ACC_BOLSA             VARCHAR(1),
        @ACC_CUST_NOMINAL      VARCHAR(1),
        @ACC_MONEDA            VARCHAR(3)

        DECLARE REC_ACC CURSOR FOR
           SELECT 'RUT CLIENTE'       = BK.ID_CLIENTE,
                  'TIPO OPERACION'    = CASE BK.CONCEPTO
                                           WHEN 'IV' THEN 'V'
                                           WHEN 'IC' THEN 'C'
                                           WHEN 'IG' THEN 'I'
                                           WHEN 'RG' THEN 'R'
                                           ELSE '?'
                                        END,
                  'NUMERO ORDEN'      = ISNULL(RO.NUMERO_ORDEN,0),
                  'CORRELATIVO'       = ISNULL(RO.LINEA_ORDEN,0),
                  'NEMOTECNICO'       = BK.INSTRUMENTO,
                  'CANTIDAD'          = BK.NOMINALES,
                  'PRECIO'            = BK.PRECIO,
                  'MONTO NETO'        = (CASE
                                            WHEN FE.FACENC_SUBFACTURACION  IN ('D') THEN
                                               ROUND((BK.VALOR / (SELECT TOP 1 CONVERT(NUMERIC(16,4),(P.VALOR/POWER(10,ISNULL(P.DECIMALES,0))))
                                                                    FROM VALGEN P
                                                                   WHERE P.CODIGO_DEL_VALOR = '0'
                                                                     AND P.TIPO_DE_VALOR = 'DO'
                                                                     AND P.FECHA_DEL_VALOR <= @VFECHA_MOVIMIENTOS
                                                                   ORDER BY P.FECHA_DEL_VALOR DESC)), 4)
                                            WHEN FE.FACENC_SUBFACTURACION  = 'E' THEN
                                               ROUND((BK.VALOR / (SELECT TOP 1 CONVERT(NUMERIC(16,4),(P.VALOR/POWER(10,ISNULL(P.DECIMALES,0))))
                                                                    FROM VALGEN P
                                                                   WHERE P.CODIGO_DEL_VALOR = '0'
                                                                     AND P.TIPO_DE_VALOR = 'EU'
                                                                     AND P.FECHA_DEL_VALOR <= @VFECHA_MOVIMIENTOS
                                                                   ORDER BY P.FECHA_DEL_VALOR DESC)), 4)
                                            ELSE BK.VALOR
                                         END),
                  'COMISION'          = BK.COMISIONES,
                  'DERECHO'           = BK.DERECHOS,
                  'GASTOS'            = BK.GASTOS,
                  'IVA'               = BK.IVA,
                  'MONTO OPERACION'   = (CASE
							                WHEN FE.FACENC_SUBFACTURACION  IN ('D') THEN
							 	               ROUND((BK.VALOR / (SELECT TOP 1 CONVERT(NUMERIC(16,4),(P.VALOR/POWER(10,ISNULL(P.DECIMALES,0))))
							 			                            FROM VALGEN P
							 			                           WHERE P.CODIGO_DEL_VALOR = '0'
							 			                             AND P.TIPO_DE_VALOR = 'DO'
							 			                             AND P.FECHA_DEL_VALOR <= @VFECHA_MOVIMIENTOS
							 			                           ORDER BY P.FECHA_DEL_VALOR DESC)), 4)
							                WHEN FE.FACENC_SUBFACTURACION  = 'E' THEN
							 	               ROUND((BK.VALOR / (SELECT TOP 1 CONVERT(NUMERIC(16,4),(P.VALOR/POWER(10,ISNULL(P.DECIMALES,0))))
							 			                            FROM VALGEN P
							 			                           WHERE P.CODIGO_DEL_VALOR = '0'
							 			                             AND P.TIPO_DE_VALOR = 'EU'
							 			                             AND P.FECHA_DEL_VALOR <= @VFECHA_MOVIMIENTOS
							 		                               ORDER BY P.FECHA_DEL_VALOR DESC)), 4)
							                ELSE BK.VALOR
						                 END) +
						                 ((BK.COMISIONES + BK.DERECHOS + BK.GASTOS + BK.IVA) *
						                 CASE BK.CONCEPTO
				                            WHEN 'IV' THEN -1
				                            WHEN 'IC' THEN 1
				                            WHEN 'RG' THEN -1
				                            WHEN 'IG' THEN 1
				                            ELSE 1
			                             END),
                  'FECHA LIQUIDACION' = CASE
                                           WHEN ISNULL(BK.FECHA_LIQUIDACION,0) > 693596 THEN
                                              CONVERT(DATETIME, BK.FECHA_LIQUIDACION - 693596)
                                           ELSE NULL
                                        END,
                  'ID CIERRE'         = RO.ID_CIERRE_DE_BOLSA,
                  T_NUMERO_FACTURA    = ISNULL(FD.FACDET_FOLIO, 0),
                  T_TIPO_LIQUIDACION  = ISNULL(BK.TIPO_LIQUIDACION, ''),
                  T_BOLSA             = ISNULL(BK.BOLSA_PALO, ''),
                  T_CUSTODIA_NOMINAL  = ISNULL(BK.CUSTODIA_ORIGEN, ''),
                  T_MONEDA            = CASE
                                           WHEN FE.FACENC_SUBFACTURACION  = 'D' THEN 'DO'
                                           WHEN FE.FACENC_SUBFACTURACION  = 'E' THEN 'EU'
                                           ELSE '$$'
                                        END
             FROM BACKOFF             AS BK WITH (NOLOCK)
            INNER JOIN INSTRU         AS I  WITH (NOLOCK) ON I.ID_INSTRUMENTO             = BK.INSTRUMENTO AND
                                                             I.TIPO_INSTRUMENTO           = 'A'
            INNER JOIN PERSOCB        AS PS WITH (NOLOCK) ON PS.ID_CLIENTE                = BK.ID_CLIENTE AND
                                                             PS.ADMINISTRACION_DE_CARTERA = 'S'
             LEFT JOIN RELORD         AS RO WITH (NOLOCK) ON RO.CONCEPTO                  = BK.CONCEPTO AND
                                                             RO.ID_CONCEPTO               = BK.ID_CONCEPTO AND
                                                             RO.CONCEPTO_LINEA            = BK.ID_LINEA_CONCEPTO
             LEFT OUTER JOIN FACTUDET AS FD WITH (NOLOCK) ON FD.FACDET_CONCEPTO           = BK.CONCEPTO AND
                                                             FD.FACDET_IDCONCEPTO         = BK.ID_CONCEPTO AND
                                                             FD.FACDET_LINEACONCEPTO      = BK.ID_LINEA_CONCEPTO
             JOIN FACTUENC            AS FE WITH (NOLOCK) ON FE.FACENC_SISTEMA            = FD.FACDET_SISTEMA AND
                                                             FE.FACENC_TIPO               = FD.FACDET_TIPO  AND
                                                             FE.FACENC_AFECTA_IVA         = FD.FACDET_AFECTA_IVA AND
                                                             FE.FACENC_FOLIO              = FD.FACDET_FOLIO AND
                                                             FE.FACENC_ESTADO             = 'I'
            WHERE BK.FECHA_DE_MVTO     = @VFECHA_MOVIMIENTOS
              AND BK.ORIGEN            = 'OP'
              AND BK.CONCEPTO          IN ('IC','IV','IG','RG')
              AND BK.MODULO_DE_ENTRADA = 'RV'

        OPEN REC_ACC
        FETCH NEXT FROM REC_ACC
        INTO @ACC_RUT_CLIENTE,
             @ACC_TIPO_OPERACION,
             @ACC_NUMERO_ORDEN,
             @ACC_CORRELATIVO,
             @ACC_NEMOTECNICO,
             @ACC_CANTIDAD,
             @ACC_PRECIO,
             @ACC_MONTO_NETO,
             @ACC_COMISION,
             @ACC_DERECHO,
             @ACC_GASTOS,
             @ACC_IVA,
             @ACC_MONTO_OPERACION,
             @ACC_FECHA_LIQUIDACION,
             @ACC_ID_CIERRE,
             @ACC_INROFACTURA,
             @ACC_TIPO_LIQ,
             @ACC_BOLSA,
             @ACC_CUST_NOMINAL,
             @ACC_MONEDA

        WHILE @@FETCH_STATUS = 0
        BEGIN
           EXEC DBO.AD_RETORNA_DATOS_CLIENTE
                @ACC_RUT_CLIENTE,
                @VTIPO_PERSONA OUTPUT,
                @VRAZON_SOCIAL OUTPUT,
                @VAPELLIDO_PATERNO OUTPUT,
                @VAPELLIDO_MATERNO OUTPUT,
                @VNOMBRES OUTPUT,
                '', 0, '', 0, '', '', '', '', 0, '', 0, '', '', '', ''

           INSERT INTO #TMP_ACCIONES
           SELECT @ACC_RUT_CLIENTE,
                  @ACC_TIPO_OPERACION,
                  @ACC_NUMERO_ORDEN,
                  @ACC_CORRELATIVO,
                  @ACC_NEMOTECNICO,
                  @ACC_CANTIDAD,
                  @ACC_PRECIO,
                  @ACC_MONTO_NETO,
                  @ACC_COMISION,
                  @ACC_DERECHO,
                  @ACC_GASTOS,
                  @ACC_IVA,
                  @ACC_MONTO_OPERACION,
                  @ACC_FECHA_LIQUIDACION,
                  @ACC_ID_CIERRE,
                  CASE @VTIPO_PERSONA
                     WHEN 'PN' THEN RTRIM(@VNOMBRES) + ' ' + RTRIM(@VAPELLIDO_PATERNO) + ' ' + RTRIM(@VAPELLIDO_MATERNO)
                     ELSE @VRAZON_SOCIAL
                  END,
                  @ACC_INROFACTURA,
                  @ACC_TIPO_LIQ,
                  @ACC_BOLSA,
                  @ACC_CUST_NOMINAL,
                  @ACC_MONEDA

             FETCH NEXT FROM REC_ACC
             INTO @ACC_RUT_CLIENTE,
                  @ACC_TIPO_OPERACION,
                  @ACC_NUMERO_ORDEN,
                  @ACC_CORRELATIVO,
                  @ACC_NEMOTECNICO,
                  @ACC_CANTIDAD,
                  @ACC_PRECIO,
                  @ACC_MONTO_NETO,
                  @ACC_COMISION,
                  @ACC_DERECHO,
                  @ACC_GASTOS,
                  @ACC_IVA,
                  @ACC_MONTO_OPERACION,
                  @ACC_FECHA_LIQUIDACION,
                  @ACC_ID_CIERRE,
                  @ACC_INROFACTURA,
                  @ACC_TIPO_LIQ,
                  @ACC_BOLSA,
                  @ACC_CUST_NOMINAL,
                  @ACC_MONEDA
        END
        CLOSE REC_ACC
        DEALLOCATE REC_ACC
    END

    SELECT 'RUT_CLIENTE'             = T_RUT_CLIENTE,
           'TIPO_OPERACION'          = T_TIPO_OPERACION,
           'NUMERO_ORDEN'            = T_NUMERO_ORDEN,
           'CORRELATIVO'             = T_CORRELATIVO,
           'NEMOTECNICO'             = T_NEMOTECNICO,
           'CANTIDAD'                = T_CANTIDAD,
           'PRECIO'                  = T_PRECIO,
           'MONTO_NETO'              = T_MONTO_NETO,
           'COMISION'                = T_COMISION,
           'DERECHO'                 = T_DERECHO,
           'GASTOS'                  = T_GASTOS,
           'IVA'                     = T_IVA,
           'MONTO_OPERACION'         = T_MONTO_OPERACION,
           'FECHA_LIQUIDACION'       = T_FECHA_LIQUIDACION,
           'ID_CIERRE'               = T_ID_CIERRE,
           'NOMBRE_CLIENTE'          = T_NOMBRE_CLIENTE,
           'NUMERO_FACTURA'          = T_NUMERO_FACTURA,
           'TIPO_LIQ'                = T_TIPO_LIQUIDACION,
           'BOLSA'                   = T_BOLSA,
           'CUST_NOM'                = T_CUSTODIA_NOMINAL,
           'GARANTIAS'               = 0,
           'PRESTAMOS'               = 0,
           'SIMULTANEAS'             = 0,
           'NOMBRE_INSTRUMENTO_FECU' = ISNULL(NOMBRE_INSTRUMENTO_FECU,''),
           'MONEDA'                  = T_MONEDA
      FROM #TMP_ACCIONES WITH (NOLOCK)
      LEFT OUTER JOIN INSTRU I ON I.ID_INSTRUMENTO = T_NEMOTECNICO
      where NOMBRE_INSTRUMENTO_FECU = 'CFI'

      DROP TABLE #TMP_ACCIONES
--END
