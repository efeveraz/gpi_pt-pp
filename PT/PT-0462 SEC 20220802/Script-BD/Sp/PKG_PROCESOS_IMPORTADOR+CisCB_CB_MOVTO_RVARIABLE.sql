IF  EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[PKG_PROCESOS_IMPORTADOR$CISCB_CB_MOVTO_RVARIABLE]') AND TYPE IN (N'P', N'PC'))
   DROP PROCEDURE [DBO].[PKG_PROCESOS_IMPORTADOR$CISCB_CB_MOVTO_RVARIABLE]
GO

CREATE PROCEDURE  [DBO].[PKG_PROCESOS_IMPORTADOR$CISCB_CB_MOVTO_RVARIABLE]
(
@PFECHA_MOVIMIENTOS CHAR(8)  -- YYYYMMDD
)
AS
BEGIN

    SET DATEFORMAT YMD
    SET NOCOUNT ON

    DECLARE @LRUT_CLIENTE    CHAR(18),     @LCANTIDAD            DECIMAL,    @LID_CUENTA         INT,           @LNEMOTECNICO   CHAR(50),
            @LNEMOTECNICOFIN CHAR(50),     @LID_NEMOTECNICO      INT,        @LCODINSTRUMENTO    CHAR(15),      @LMGS           CHAR(100),
            @LNUM_FILA       INT,          @INTCOUNTER           INT,        @PMGS               VARCHAR(8000), @LNUMEROORDEN   INT,
            @LCORRELATIVO    INT,          @LACUMULA_CANTIDAD    DECIMAL,    @NUMCUENTA          VARCHAR(10),   @ID_TC_MONEDA   NUMERIC,
            @COD_ESTADO      VARCHAR(3),   @ID_ASESOR            INT,        @FLG_BLOQUEADO      VARCHAR(1),    @DSC_BLOQUEO    VARCHAR(120),
            @LERRORNOCUENTA  VARCHAR(1),   @LOUTPUTLOG_SCTAS_FLG INT,        @LGRABADO           VARCHAR(1),    @LCODRESULTADO  VARCHAR(5) ,
            @LMSGRESULTADO   VARCHAR(800), @ID_ORIGEN            NUMERIC,    @ID_TIPO_CONVERSION NUMERIC,       @LMONEDA        VARCHAR(3)

    SET @INTCOUNTER     = 0
    SET @LCODRESULTADO  = 'OK'
    SET @LERRORNOCUENTA = ''
    SET @LMSGRESULTADO  = ''
    SET @DSC_BLOQUEO    = ''
    SET @LGRABADO       = 'N'

    --****************************************************************************************************************************
    -- CREACION DE TABLA DE RECUPERACION DE DATOS
    CREATE TABLE #TABLA1  (RUT_CLIENTE              CHAR(15),      TIPO_OPERACION    CHAR(1),       NUMERO_ORDEN   INT,
                           CORRELATIVO              INT,           NEMOTECNICO       VARCHAR(20),   CANTIDAD       FLOAT,
                           PRECIO                   FLOAT,         MONTO_NETO        FLOAT,         COMISION       FLOAT,
                           DERECHO                  FLOAT,         GASTOS            FLOAT,         IVA            FLOAT,
                           MONTO_OPERACION          FLOAT,         FECHA_LIQUIDACION DATETIME,      ID_CIERRE      VARCHAR(10),
                           NOMBRE_CLIENTE           VARCHAR(120),  FACTURA           INT,           TIPO_LIQ       VARCHAR(3),
                           BOLSA                    VARCHAR(5),    CUST_NOM          VARCHAR(1),    GARANTIAS      NUMERIC(18,4),
                           PRESTAMOS                NUMERIC(18,4), SIMULTANEAS       NUMERIC(18,4),
                           NOMBRE_INSTRUMENTO_FECU  VARCHAR(10),   MONEDA            VARCHAR(3))

    -- CREACION DE TABLA DE PROCESO
    CREATE TABLE #TABLA2 (NUMERO_FILA             INT,            RUT_CLIENTE          CHAR(15),      TIPO_OPERACION       CHAR(1),
                          NUMERO_ORDEN            INT,            CORRELATIVO          INT,           NEMOTECNICO          CHAR(20),
                          CANTIDAD                FLOAT,          PRECIO               FLOAT,         MONTO_NETO           FLOAT,
                          COMISION                FLOAT,          DERECHO              FLOAT,         GASTOS               FLOAT,
                          IVA                     FLOAT,          MONTO_OPERACION      FLOAT ,        FECHA_LIQUIDACION    DATETIME,
                          ID_CIERRE               VARCHAR(10),    NOMBRE_CLIENTE       VARCHAR(120),  FACTURA              INT,
                          TIPO_LIQ                VARCHAR(3),     BOLSA                VARCHAR(5),    CUST_NOM             VARCHAR(1),
                          GARANTIAS               NUMERIC(18,4),  PRESTAMOS            NUMERIC(18,4), SIMULTANEAS          NUMERIC(18,4),
                          NOMBRE_INSTRUMENTO_FECU VARCHAR(10) ,   MONEDA               VARCHAR(3),    COD_INSTRUMENTO      CHAR(15),
                          ID_EMISOR_ESPECIFICO    INT,            ID_NEMOTECNICO       INT,           ID_CUENTA            INT,
                          NUM_CUENTA              VARCHAR(10),    ID_ASESOR            INT,           FLG_BLOQUEADO        VARCHAR(1),
                          LERRORNOCUENTA          VARCHAR(1),     LLOG_SCTAS_FLG       INT,           DSC_BLOQUEO          VARCHAR(120),
                          GRABADO                 VARCHAR(1),     LCODRESULTADO        VARCHAR(5),    LMSGRESULTADO        VARCHAR(800),
                          ID_MONEDA               NUMERIC)


     --****************************************************************************************************************************
    -- CARGA DATOS DE PROCEDIMIENTO EN TABLA TEMPORAL
    INSERT #TABLA1
    EXEC [CISCB].[DBO].[AD_QRY_CB_MOVTO_ACCIONES] @PFECHA_MOVIMIENTOS = @PFECHA_MOVIMIENTOS

    DELETE FROM #TABLA1 WHERE TIPO_OPERACION NOT IN ('C','V')

    -- CARGA DATOS DE PROCEDIMIENTO EN TABLA DE PROCESO
    INSERT INTO #TABLA2
    SELECT 1, * ,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL
    FROM #TABLA1
    ORDER BY RUT_CLIENTE, NUMERO_ORDEN, CORRELATIVO, NEMOTECNICO

    --INCREMENTA EL NUMERO DE FILA EN TABLA DE PROCESO
    UPDATE #TABLA2
    SET @INTCOUNTER = NUMERO_FILA = @INTCOUNTER + 1;

    SELECT @ID_ORIGEN = ID_ORIGEN FROM ORIGENES WHERE COD_ORIGEN = 'MAGIC_VALORES'
    SELECT @ID_TIPO_CONVERSION = ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WHERE TABLA = 'CUENTAS'
    SELECT @ID_TC_MONEDA = ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WHERE TABLA = 'MONEDAS'
     --****************************************************************************************************************************

    DECLARE CUR_OPERACION CURSOR FOR
    SELECT NUMERO_FILA,
           NEMOTECNICO,
           RUT_CLIENTE,
           NUMERO_ORDEN,
           CANTIDAD,
           CORRELATIVO,
           MONEDA
    FROM #TABLA2
    ORDER BY NUMERO_FILA

    OPEN CUR_OPERACION
    FETCH CUR_OPERACION INTO @LNUM_FILA,
                             @LNEMOTECNICO,
                             @LRUT_CLIENTE,
                             @LNUMEROORDEN,
                             @LCANTIDAD,
                             @LCORRELATIVO,
                             @LMONEDA
    WHILE (@@FETCH_STATUS = 0)
    BEGIN

        -- IDENTIFICADOR DE CUENTA **************************
        SET @INTCOUNTER     = 0
        SET @LCODRESULTADO  = 'OK'
        SET @LERRORNOCUENTA = ''
        SET @LMSGRESULTADO  = ''
        SET @DSC_BLOQUEO    = ''
        SET @LGRABADO       = 'N'
        SET @COD_ESTADO    = ''
        SET @ID_ASESOR     = ''
        SET @FLG_BLOQUEADO = ''
        SET @DSC_BLOQUEO   = ''
        SET @NUMCUENTA     = ''
        SET @LID_CUENTA    = ''
        SET @LCODINSTRUMENTO = ''
        SET @LID_NEMOTECNICO = ''
        SET @LOUTPUTLOG_SCTAS_FLG = NULL

        IF ISNULL((SELECT 1 FROM REL_CONVERSIONES
                    WHERE ID_ORIGEN = @ID_ORIGEN AND ID_TIPO_CONVERSION = @ID_TIPO_CONVERSION AND VALOR = LTRIM(RTRIM(@LRUT_CLIENTE))),0) <> 0
           BEGIN
                SELECT @LID_CUENTA = ID_ENTIDAD
                  FROM REL_CONVERSIONES
                 WHERE ID_ORIGEN = @ID_ORIGEN
                   AND ID_TIPO_CONVERSION = @ID_TIPO_CONVERSION
                   AND VALOR = LTRIM(RTRIM(@LRUT_CLIENTE))

                SELECT @COD_ESTADO    = COD_ESTADO,
                       @ID_ASESOR     = ID_ASESOR,
                       @FLG_BLOQUEADO = FLG_BLOQUEADO,
                       @DSC_BLOQUEO   = ISNULL(OBS_BLOQUEO,''),
                       @NUMCUENTA     = NUM_CUENTA
                  FROM CUENTAS
                 WHERE ID_CUENTA = @LID_CUENTA

                IF @COD_ESTADO = 'H'
                   BEGIN
                        IF @FLG_BLOQUEADO = 'S'
                           BEGIN
                                SET @FLG_BLOQUEADO = 'B'
                                SET @LERRORNOCUENTA = 'S'
                                SET @LCODRESULTADO = 'ERROR'
                                SET @LMSGRESULTADO = 'ERROR: CUENTA: ' + @NUMCUENTA  + '|RUT CLIENTE: ' + LTRIM(RTRIM(@LRUT_CLIENTE)) + '|NEMOTÉCNICO: ' +  LTRIM(RTRIM(@LNEMOTECNICO)) + '-' + @DSC_BLOQUEO
                           END
                         SET @LOUTPUTLOG_SCTAS_FLG = 0 --FALSE
                   END
                ELSE
                   BEGIN
                         SET @LERRORNOCUENTA = 'N'
                         SET @LCODRESULTADO = 'ERROR'
                         SET @LMSGRESULTADO = 'ERROR: CUENTA: ' + @NUMCUENTA  + '|RUT CLIENTE: ' + LTRIM(RTRIM(@LRUT_CLIENTE)) + '|NEMOTÉCNICO: ' +  LTRIM(RTRIM(@LNEMOTECNICO)) + '-MOVIMIENTO NO POSEE CUENTA.'
                   END
           END
        ELSE
           BEGIN
               SET @LERRORNOCUENTA = 'N'
               SET @LOUTPUTLOG_SCTAS_FLG = 1 --TRUE
               SET @ID_ASESOR     = 0
               SET @LCODRESULTADO = 'ERROR'
               SET @LMSGRESULTADO = 'ERROR: RUT CLIENTE: ' + LTRIM(RTRIM(@LRUT_CLIENTE))  + ' |NEMOTÉCNICO: ' + LTRIM(RTRIM(@LNEMOTECNICO)) + ' - MOVIMIENTO NO POSEE CUENTA.'
           END

               SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO,
                      @LCODINSTRUMENTO = COD_INSTRUMENTO
                 FROM VIEW_NEMOTECNICOS
                WHERE VIEW_NEMOTECNICOS.NEMOTECNICO = LTRIM(RTRIM(@LNEMOTECNICO))

              IF @@ROWCOUNT = 0
                 BEGIN
                     SET @LOUTPUTLOG_SCTAS_FLG = 1
                     IF @LCODRESULTADO = 'OK'
                      BEGIN
                           SET @LCODRESULTADO = 'ERROR'
                           SET @LMSGRESULTADO = 'ERROR: PROBLEMAS AL BUSCAR NEMOTÉCNICO: ' + LTRIM(RTRIM(@LNEMOTECNICO))
                      END
                     ELSE
                      BEGIN
                           SET @LCODRESULTADO = 'ERROR'
                           SET @LMSGRESULTADO = @LMSGRESULTADO + '/PROBLEMAS AL BUSCAR NEMOTÉCNICO: ' + LTRIM(RTRIM(@LNEMOTECNICO))
                      END

                 END

          UPDATE #TABLA2
          SET COD_INSTRUMENTO = @LCODINSTRUMENTO,
              ID_NEMOTECNICO  = @LID_NEMOTECNICO,
              ID_CUENTA       = @LID_CUENTA,
              NUM_CUENTA      = @NUMCUENTA,
              ID_ASESOR       = @ID_ASESOR,
              FLG_BLOQUEADO   = @FLG_BLOQUEADO,
              LERRORNOCUENTA  = @LERRORNOCUENTA,
              LLOG_SCTAS_FLG  = @LOUTPUTLOG_SCTAS_FLG,
              DSC_BLOQUEO     = @DSC_BLOQUEO,
              GRABADO         = @LGRABADO,
              LCODRESULTADO   = @LCODRESULTADO ,
              LMSGRESULTADO   = @LMSGRESULTADO,
              ID_MONEDA       = (SELECT ID_ENTIDAD FROM REL_CONVERSIONES WHERE ID_ORIGEN = @ID_ORIGEN AND ID_TIPO_CONVERSION = @ID_TC_MONEDA AND VALOR = LTRIM(RTRIM(@LMONEDA)))
          WHERE NEMOTECNICO = @LNEMOTECNICO
            AND RUT_CLIENTE = @LRUT_CLIENTE
            AND NUMERO_ORDEN = @LNUMEROORDEN
            AND CORRELATIVO  = @LCORRELATIVO

        --LECTURA DE LA SIGUIENTE FILA DE UN CURSOR
        FETCH CUR_OPERACION INTO @LNUM_FILA,
                                 @LNEMOTECNICO,
                                 @LRUT_CLIENTE,
                                 @LNUMEROORDEN,
                                 @LCANTIDAD,
                                 @LCORRELATIVO,
                                 @LMONEDA
    END

    -- CIERRA EL CURSOR
    CLOSE CUR_OPERACION
    DEALLOCATE CUR_OPERACION

    -- TABLA RESULTANTE
    SELECT NUMERO_FILA,           RUT_CLIENTE,         TIPO_OPERACION,           NUMERO_ORDEN,           CORRELATIVO,
           NEMOTECNICO,           CANTIDAD,            PRECIO,                   MONTO_NETO,             COMISION,
           DERECHO,               GASTOS,              IVA,                      MONTO_OPERACION,        FECHA_LIQUIDACION,
           NOMBRE_CLIENTE,        FACTURA,             GARANTIAS,                PRESTAMOS,              SIMULTANEAS,
           BOLSA,                 TIPO_LIQ,            CUST_NOM,                 COD_INSTRUMENTO,        ID_NEMOTECNICO,
           ID_CUENTA ,            NUM_CUENTA,          ID_ASESOR,                FLG_BLOQUEADO,          LERRORNOCUENTA,
           LLOG_SCTAS_FLG,        DSC_BLOQUEO,         GRABADO,                  LCODRESULTADO,          LMSGRESULTADO,
           ISNULL(NOMBRE_INSTRUMENTO_FECU,'') 'NOMBRE_INSTRUMENTO_FECU', ID_MONEDA
      FROM #TABLA2
     WHERE NOT EXISTS (SELECT 1 FROM REL_CONVERSIONES
                        WHERE ID_ORIGEN = (SELECT ID_ORIGEN FROM ORIGENES WHERE COD_ORIGEN = 'AD_QRY_CB_MOVTO_ACCIONES')
                          AND ID_TIPO_CONVERSION = (SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WHERE TABLA = 'OPERACIONES_DETALLE')
                          AND VALOR = RTRIM(CONVERT(VARCHAR(50),NUMERO_ORDEN)) + '/' + RTRIM(CONVERT(VARCHAR(50),CORRELATIVO)))
     ORDER BY TIPO_OPERACION,ID_CUENTA,COD_INSTRUMENTO,ID_NEMOTECNICO

   DROP TABLE #TABLA1
   DROP TABLE #TABLA2
   SET NOCOUNT OFF
END
GO

GRANT EXECUTE ON [PKG_PROCESOS_IMPORTADOR$CISCB_CB_MOVTO_RVARIABLE] TO DB_EXECUTESP
GO