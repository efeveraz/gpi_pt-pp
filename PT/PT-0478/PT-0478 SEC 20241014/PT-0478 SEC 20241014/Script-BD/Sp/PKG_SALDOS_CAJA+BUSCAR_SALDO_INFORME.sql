USE [CSGPI]
GO

ALTER PROCEDURE [DBO].[PKG_SALDOS_CAJA$BUSCAR_SALDO_INFORME](
@PID_CUENTA		NUMERIC, 
@PFECHA_CIERRE	DATETIME) AS
BEGIN

    SET NOCOUNT ON;

    CREATE TABLE #CAJAS 
    (
        ID_MONEDA     NUMERIC,
        COD_MONEDA    VARCHAR(3),
        DSC_MONEDA    VARCHAR(50),
        COD_MERCADO   CHAR(1),
        DESC_MERCADO  VARCHAR(20)
    );

    INSERT INTO #CAJAS
    SELECT DISTINCT M.ID_MONEDA, M.COD_MONEDA, M.DSC_MONEDA, CC.COD_MERCADO, ME.DESC_MERCADO
      FROM MONEDAS M, 
           CAJAS_CUENTA CC,
           MERCADOS ME
     WHERE M.ID_MONEDA           = CC.ID_MONEDA
       AND M.FLG_ES_MONEDA_PAGO  = 'S'
       AND CC.COD_MERCADO        = ME.COD_MERCADO;

    SELECT 
           C.ID_CUENTA                                                              AS ID_CUENTA,
           C.ABR_CUENTA                                                             AS ABR_CUENTA,
           C.DSC_CUENTA                                                             AS DSC_CUENTA,
           C.NUM_CUENTA                                                             AS NUM_CUENTA,
           CAJA.DSC_MONEDA                                                          AS DSC_MONEDA_CAJA,
           CAJA.COD_MERCADO                                                         AS COD_MERCADO,
           ISNULL(SC.ID_MONEDA_CAJA, CAJA.ID_MONEDA)                                AS ID_MONEDA_CAJA,
           ISNULL(MCAJA.COD_MONEDA, CAJA.COD_MONEDA)                                AS COD_MONEDA_CAJA,
           ISNULL(MCAJA.DICIMALES_MOSTRAR, 0)                                       AS DICIMALES_MOSTRAR_CAJA,
           ISNULL(MCAJA.SIMBOLO, '')                                                AS SIMBOLO_CAJA,
           ISNULL(SC.MONTO_MON_CAJA, 0)                                             AS MONTO_MON_CAJA,
           ISNULL(SC.ID_MONEDA_CTA, 0)                                              AS ID_MONEDA_CTA,
           ISNULL(SC.MONTO_MON_CTA, 0)                                              AS MONTO_MON_CTA,
           ISNULL(MCTA.COD_MONEDA, '')                                              AS COD_MONEDA_CTA,
           ISNULL(MCTA.DSC_MONEDA, '')                                              AS DSC_MONEDA_CTA,
           ISNULL(MCTA.DICIMALES_MOSTRAR, 0)                                        AS DICIMALES_MOSTRAR_CTA,
           ISNULL(MCTA.SIMBOLO, '')                                                 AS SIMBOLO_CTA,
           ISNULL(CC.DSC_CAJA_CUENTA, CAJA.DSC_MONEDA + ' ' + CAJA.DESC_MERCADO)    AS DSC_CAJA_CUENTA,
           ISNULL(A.NOMBRE, '')                                                     AS ASESOR
      FROM 
           CUENTAS C
           CROSS JOIN #CAJAS CAJA
           LEFT JOIN VIEW_CAJAS_CUENTA CC ON C.ID_CUENTA       = CC.ID_CUENTA 
                                         AND CAJA.COD_MERCADO  = CC.COD_MERCADO 
                                         AND CAJA.DSC_MONEDA   = CC.DSC_MONEDA
           LEFT JOIN SALDOS_CAJA SC ON CC.ID_CAJA_CUENTA = SC.ID_CAJA_CUENTA 
                                   AND SC.FECHA_CIERRE   = @PFECHA_CIERRE
           LEFT JOIN MONEDAS MCAJA ON MCAJA.ID_MONEDA = SC.ID_MONEDA_CAJA
           LEFT JOIN MONEDAS MCTA ON MCTA.ID_MONEDA = SC.ID_MONEDA_CTA
           LEFT JOIN ASESORES A ON C.ID_ASESOR = A.ID_ASESOR
     WHERE ((@PID_CUENTA IS NULL) OR (C.ID_CUENTA = @PID_CUENTA))
     ORDER BY C.ID_CUENTA, CAJA.ID_MONEDA, CAJA.COD_MERCADO DESC;

      DROP TABLE #CAJAS;

      SET NOCOUNT OFF;

END
GO