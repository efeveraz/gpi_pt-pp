﻿var porCuentaNroCuenta;
var porCuentaRutCliente;
var porCuentaNombreCliente;

var porClienteRutCliente;
var porClienteNombreCliente;

var porGrupoNombreGrupo;
var porGrupoNombreAsesor;

var GetModalTablaCuentas;
var GetModalTablaClientes;
var GetModalTablaGrupos;

var btnAbrirModal;

function resize() {
    height = 0;

    height = $("#content").outerHeight(true) - 85;
}

function initMasterFiltroCuenta() {
    llenarDetalleCuenta();
    porCuentaNroCuenta = document.getElementById("porCuenta_NroCuenta").value;
    porCuentaRutCliente = document.getElementById("porCuenta_RutCliente").value;
    porCuentaNombreCliente = document.getElementById("porCuenta_NombreCliente").value;

    llenarDetalleCliente();
    porClienteRutCliente = document.getElementById("porCliente_RutCliente").value;
    porClienteNombreCliente = document.getElementById("porCliente_NombreCliente").value;

    llenarDetalleGrupo();
    porGrupoNombreGrupo = document.getElementById("porGrupo_NombreGrupo").value;
    porGrupoNombreAsesor = document.getElementById("porGrupo_NombreAsesor").value;
}

function cargarEventHandlersMFiltroCuenta() {
    $("#btnBuscarCuentas").on('click', function () {
        btnAbrirModal = 'btnCuentas';
        mostrarModalCuentas();
    });
    $("#btnBuscarClientes").on('click', function () {
        btnAbrirModal = 'btnClientes';
        mostrarModalClientes();
    });
    $("#btnBuscarGrupos").on('click', function () {
        btnAbrirModal = 'btnGrupos';
        mostrarModalGrupos();
    });

    $("#btnDetalleCuentas").on('click', function () {
        mostrarModalDetalleCuentas("#modalDetalleCuenta", "#modalDetalleCliente", "#modalDetalleGrupo", "Detalle Cuenta");
    });
    $("#btnDetalleClientes").on('click', function () {
        mostrarModalDetalleCuentas("#modalDetalleCliente", "#modalDetalleCuenta", "#modalDetalleGrupo", "Detalle Cliente");
    });
    $("#btnDetalleGrupos").on('click', function () {
        mostrarModalDetalleCuentas("#modalDetalleGrupo", "#modalDetalleCuenta", "#modalDetalleCliente", "Detalle Grupo");
    });

    $('#listCuenta tbody').on('click', 'tr', function () {
        var data = GetModalTablaCuentas.row($(this)).data();
        seteaInformacionCuenta(data);
        llenarDetalleCuenta();
        $('#myModal').modal('hide');
        $("#dtFechaConsulta").change();
    });
    $('#listCliente tbody').on('click', 'tr', function () {
        var data = GetModalTablaClientes.row($(this)).data();
        seteaInformacionCliente(data);
        llenarDetalleCliente();
        $('#myModal').modal('hide');
        $("#dtFechaConsulta").change();
    });
    $('#listGrupo tbody').on('click', 'tr', function () {
        var data = GetModalTablaGrupos.row($(this)).data();
        seteaInformacionGrupo(data);
        llenarDetalleGrupo();
        $('#myModal').modal('hide');
        $("#dtFechaConsulta").change();
    });

    $("#btnLimpiarCuentas").on('click', function () {
        limpiaCuenta();
    });
    $("#btnLimpiarClientes").on('click', function () {
        limpiaCliente();
    });
    $("#btnLimpiarGrupos").on('click', function () {
        limpiaGrupo();
    });

    $('#myModal').on('shown.bs.modal', function () {
        switch (btnAbrirModal) {
            case 'btnCuentas':
                cargarCuentas();
                break;
            case 'btnClientes':
                cargarClientes();
                break;
            case 'btnGrupos':
                cargarGrupos();
                break;
        }
    });
    $('#myModal').on('hidden.bs.modal', function () {
        switch (btnAbrirModal) {
            case 'btnCuentas':
                GetModalTablaCuentas
                    .clear()
                    .draw();
                break;
            case 'btnClientes':
                GetModalTablaClientes
                    .clear()
                    .draw();
                break;
            case 'btnGrupos':
                GetModalTablaGrupos
                    .clear()
                    .draw();
                break;
        }
    });
}

function mostrarModalCuentas() {
    $('#myModal').modal({ show: true, backdrop: true });
    $('#myModal').modal('show');
    $("#modalPorCuenta").show();
    $("#modalPorCliente").hide();
    $("#modalPorGrupo").hide();
    $("#tituloModalBuscador").text("Buscar Cuentas");
}
function mostrarModalClientes() {
    $('#myModal').modal({ show: true, backdrop: true });
    $('#myModal').modal('show');
    $("#modalPorCuenta").hide();
    $("#modalPorCliente").show();
    $("#modalPorGrupo").hide();
    $("#tituloModalBuscador").text("Buscar Clientes");
}
function mostrarModalGrupos() {
    $('#myModal').modal({ show: true, backdrop: true });
    $('#myModal').modal('show');
    $("#modalPorCuenta").hide();
    $("#modalPorCliente").hide();
    $("#modalPorGrupo").show();
    $("#tituloModalBuscador").text("Buscar Grupos");
}

function mostrarModalDetalleCuentas(mostrarModal, mostrarModalHide1, mostrarModalHide2, tituloModal) {
    $('#myModalDet').modal({ show: true, backdrop: true });
    $('#myModalDet').modal('show');
    $(mostrarModal).show();
    $(mostrarModalHide1).hide();
    $(mostrarModalHide2).hide();
    $("#tituloModalDetalle").text(tituloModal);
}

function cargarCuentas() {
    GetModalTablaCuentas = $('#listCuenta').DataTable({
        data: {},
        columns: [
            { "data": "NUM_CUENTA" },
            { "data": "ABR_CUENTA" },
            { "data": "RUT_CLIENTE" },
            { "data": "NOMBRE_CLIENTE" },
            { "data": "NOMBRE_ASESOR" },
            { "data": "VALOR" }
        ],
        destroy: true,
        language: { "url": "../js/vendor/DataTables/i18n/Spanish.json" },
        lengthMenu: [[10], [10]],
        ajax: {
            url: '../Servicios/BusquedaCuClGr.asmx/BuscarCuentas',
            data: function (d) {
                return JSON.stringify({});
            },
            type: 'post',
            contentType: 'application/json',
            dataSrc: 'd',
            complete: function (jsondata, stat) {
                if (stat !== "success") {
                    alertaColor(3, JSON.parse(jsondata.responseText).Message);
                    $('#myModal').modal('hide');
                }
            }
        },
        scrollX: true,
        scrollCollapse: false,
        paging: true,
        rowCallback: function () {
            $(window).trigger('resize');
        }
    });
    $.fn.dataTable.ext.pager.numbers_length = 5;
    $.fn.dataTable.ext.errMode = 'none';
}
function cargarClientes() {

    GetModalTablaClientes = $('#listCliente').DataTable({
        data: {},
        columns: [
            { "data": "RUT_CLIENTE" },
            { "data": "NOMBRE_CLIENTE" }
        ],
        destroy: true,
        language: { "url": "../js/vendor/DataTables/i18n/Spanish.json" },
        lengthMenu: [[10], [10]],
        ajax: {
            url: '../Servicios/BusquedaCuClGr.asmx/BuscarClientes',
            data: function (d) {
                return JSON.stringify({});
            },
            type: 'post',
            contentType: 'application/json',
            dataSrc: 'd',
            complete: function (jsondata, stat) {
                if (stat !== "success") {
                    alertaColor(3, JSON.parse(jsondata.responseText).Message);
                    $('#myModal').modal('hide');
                }
            }
        },
        scrollX: true,
        scrollCollapse: false,
        paging: true,
        rowCallback: function () {
            $(window).trigger('resize');
        }
    });
    $.fn.dataTable.ext.pager.numbers_length = 5;
    $.fn.dataTable.ext.errMode = 'none';
}
function cargarGrupos() {

    GetModalTablaGrupos = $('#listGrupo').DataTable({
        data: {},
        columns: [
            { "data": "DSC_GRUPO_CUENTA" },
            { "data": "DIRECCION" },
            { "data": "NOMBRE" },
            { "data": "EMAIL" },
            { "data": "TELEFONO" },
            { "data": "FECHA_OPERATIVA" },
            { "data": "CANT_CUENTAS" }
        ],
        destroy: true,
        language: { "url": "../js/vendor/DataTables/i18n/Spanish.json" },
        lengthMenu: [[10], [10]],
        ajax: {
            url: '../Servicios/BusquedaCuClGr.asmx/BuscarGrupos',
            data: function (d) {
                return JSON.stringify({});
            },
            type: 'post',
            contentType: 'application/json',
            dataSrc: 'd',
            complete: function (jsondata, stat) {
                if (stat !== "success") {
                    alertaColor(3, JSON.parse(jsondata.responseText).Message);
                    $('#myModal').modal('hide');
                }
            }
        },
        columnDefs: [{
            render: function (data, type, row) {
                return moment(data).format("L");
            },
            targets: 5
        }],
        scrollX: true,
        scrollCollapse: false,
        paging: true,
        rowCallback: function () {
            $(window).trigger('resize');
        }
    });
    $.fn.dataTable.ext.pager.numbers_length = 5;
    $.fn.dataTable.ext.errMode = 'none';
}

function limpiaCuenta() {
    document.getElementById("porCuenta_NroCuenta").value = "";
    document.getElementById("porCuenta_RutCliente").value = "";
    document.getElementById("porCuenta_NombreCliente").value = "";
    document.getElementById("porCuenta_NombreCuenta").value = "";
    document.getElementById("porCuenta_MonedaCuenta").value = "";
    document.getElementById("porCuenta_PerfilCuenta").value = "";
    document.getElementById("porCuenta_FechaCreacion").value = "";
    document.getElementById("porCuenta_RutAsesorCuenta").value = "";
    document.getElementById("porCuenta_NombreAsesorCuenta").value = "";
    document.getElementById("porCuenta_FechaUltimoProceso").value = "";
    document.getElementById("porCuenta_DireccionCliente").value = "";
    document.getElementById("porCuenta_TelefonoCliente").value = "";

    delete localStorage.CuentaSeleccionada;
    miInformacionCuenta = null;

    $("#porCuenta_NroCuenta").change();
    $('#btnLimpiarCuentas').attr('disabled', true);
    $('#btnDetalleCuentas').attr('disabled', true);
}
function limpiaCliente() {
    document.getElementById("porCliente_Email").value = "";
    document.getElementById("porCliente_Genero").value = "";
    document.getElementById("porCliente_TipoPersona").value = "";
    document.getElementById("porCliente_RutCliente").value = "";
    document.getElementById("porCliente_NombreCliente").value = "";
    document.getElementById("porCliente_FechaCreacion").value = "";
    document.getElementById("porCliente_DireccionCliente").value = "";

    delete localStorage.ClienteSeleccionado;
    miInformacionCliente = null;

    $("#porCuenta_NroCuenta").change();
    $('#btnLimpiarClientes').attr('disabled', true);
    $('#btnDetalleClientes').attr('disabled', true);
}
function limpiaGrupo() {
    document.getElementById("porGrupo_NombreGrupo").value = "";
    document.getElementById("porGrupo_NombreAsesor").value = "";
    document.getElementById("porGrupo_Direccion").innerHTML = "";
    document.getElementById("porGrupo_Email").innerHTML = "";
    document.getElementById("porGrupo_Telefono").innerHTML = "";
    document.getElementById("porGrupo_FechaOperativa").innerHTML = "";
    document.getElementById("porGrupo_CantidadCuentas").innerHTML = "";

    delete localStorage.GrupoSeleccionado;
    miInformacionGrupo = null;

    $("#porCuenta_NroCuenta").change();
    $('#btnLimpiarGrupos').attr('disabled', true);
    $('#btnDetalleGrupos').attr('disabled', true);
}

function llenarDetalleCuenta() {
    if (miInformacionCuenta != null) {
        document.getElementById("porCuenta_NroCuenta").value = miInformacionCuenta.NroCuenta;
        document.getElementById("porCuenta_RutCliente").value = miInformacionCuenta.RutCliente;
        document.getElementById("porCuenta_NombreCliente").value = miInformacionCuenta.NombreCliente;
        $("#porCuenta_NombreCuenta").text(miInformacionCuenta.NombreCuenta);
        $("#porCuenta_MonedaCuenta").text(miInformacionCuenta.Moneda);
        $("#porCuenta_PerfilCuenta").text(miInformacionCuenta.TipoPerfil);
        $("#porCuenta_FechaCreacion").text(moment(miInformacionCuenta.FechaCreacion).startOf('day').format('L'));
        $("#porCuenta_RutAsesorCuenta").text(miInformacionCuenta.RutAsesor);
        $("#porCuenta_NombreAsesorCuenta").text(miInformacionCuenta.NombreAsesor);
        $("#porCuenta_FechaUltimoProceso").text(moment(miInformacionCuenta.FechaUltProceso).startOf('day').format('L'));
        $("#porCuenta_FechaCierreVirtual").text(miInformacionCuenta.FechaCierreVirtual == null ? '-' : moment(miInformacionCuenta.FechaCierreVirtual).startOf('day').format('L'));
        $("#porCuenta_NombreClienteDet").text(miInformacionCuenta.NombreCliente);
        $("#porCuenta_RutClienteDet").text(miInformacionCuenta.RutCliente);
        $("#porCuenta_DireccionCliente").text(miInformacionCuenta.Direccion);
        $("#porCuenta_TelefonoCliente").text(miInformacionCuenta.Telefono);
        $("#porCuenta_NroCuenta").change();
        $('#btnLimpiarCuentas').attr('disabled', false);
        $('#btnDetalleCuentas').attr('disabled', false);
    } else {
        $('#btnLimpiarCuentas').attr('disabled', true);
        $('#btnDetalleCuentas').attr('disabled', true);
    }
}
function llenarDetalleCliente() {
    if (miInformacionCliente != null) {
        document.getElementById("porCliente_NombreCliente").value = miInformacionCliente.NombreCliente;
        document.getElementById("porCliente_RutCliente").value = miInformacionCliente.RutCliente;
        $("#porCliente_NombreClienteDet").text(miInformacionCliente.NombreCliente);
        $("#porCliente_RutClienteDet").text(miInformacionCliente.RutCliente);
        $("#porCliente_Email").text(miInformacionCliente.Email);
        $("#porCliente_Genero").text(miInformacionCliente.Genero);
        $("#porCliente_TipoPersona").text(miInformacionCliente.TipoPersona);
        if (miInformacionCliente.FechaCreacion == null) {
            $("#porCliente_FechaCreacion").value = '';
        } else {
            $("#porCliente_FechaCreacion").text(moment(miInformacionCliente.FechaCreacion).startOf('day').format('L'));
        }
        $("#porCliente_DireccionCliente").text(miInformacionCliente.Direccion);
        $("#porCliente_FechaCierreVirtual").text(miInformacionCliente.FechaCierreVirtual == null ? '-' : moment(miInformacionCliente.FechaCierreVirtual).startOf('day').format('L'));
        $("#porCuenta_NroCuenta").change();
        $('#btnLimpiarClientes').attr('disabled', false);
        $('#btnDetalleClientes').attr('disabled', false);
    } else {
        $('#btnLimpiarClientes').attr('disabled', true);
        $('#btnDetalleClientes').attr('disabled', true);
    }
}
function llenarDetalleGrupo() {
    if (miInformacionGrupo != null) {
        document.getElementById("porGrupo_NombreGrupo").value = miInformacionGrupo.NombreGrupo;
        document.getElementById("porGrupo_NombreAsesor").value = miInformacionGrupo.NombreAsesor;

        $("#porGrupo_NombreGrupoDet").text(miInformacionGrupo.NombreGrupo);
        $("#porGrupo_NombreAsesorDet").text(miInformacionGrupo.NombreAsesor);

        $("#porGrupo_Email").text(miInformacionGrupo.Email);
        $("#porGrupo_Telefono").text(miInformacionGrupo.Telefono);
        $("#porGrupo_FechaOperativa").text(moment(miInformacionGrupo.FechaOperativa).startOf('day').format('L'));
        $("#porGrupo_CantidadCuentas").text(miInformacionGrupo.CantidadCuentas);
        $("#porGrupo_Direccion").text(miInformacionGrupo.Direccion);
        $("#porGrupo_FechaCierreVirtual").text(miInformacionGrupo.FechaCierreVirtual == null ? '-' : moment(miInformacionGrupo.FechaCierreVirtual).startOf('day').format('L'));
        $("#porCuenta_NroCuenta").change();
        $('#btnLimpiarGrupos').attr('disabled', false);
        $('#btnDetalleGrupos').attr('disabled', false);
    } else {
        $('#btnLimpiarGrupos').attr('disabled', true);
        $('#btnDetalleGrupos').attr('disabled', true);
    }
}

function seteaInformacionCuenta(selRowId) {

    var vFechaCierreVirtual;
    if (selRowId.FECHA_CIERRE_VIRTUAL) {
        if (selRowId.FECHA_CIERRE_CUENTA < selRowId.FECHA_CIERRE_VIRTUAL) {
            vFechaCierreVirtual = selRowId.FECHA_CIERRE_CUENTA;
        } else {
            vFechaCierreVirtual = selRowId.FECHA_CIERRE_VIRTUAL;
        }
    } else {
        vFechaCierreVirtual = selRowId.FECHA_CIERRE_CUENTA;
    }

    miInformacionCuenta = {
        IdCliente: selRowId.ID_CLIENTE,
        IdCuenta: selRowId.ID_CUENTA,
        NroCuenta: selRowId.NUM_CUENTA,
        RutCliente: capitalizeEachWord(selRowId.RUT_CLIENTE),
        NombreCliente: capitalizeEachWord(selRowId.NOMBRE_CLIENTE),
        NombreCuenta: capitalizeEachWord(selRowId.DSC_CUENTA),
        Moneda: capitalizeEachWord(selRowId.DSC_MONEDA),
        TipoPerfil: capitalizeEachWord(selRowId.DSC_PERFIL_RIESGO),
        FechaCreacion: selRowId.FECHA_OPERATIVA,
        RutAsesor: capitalizeEachWord(selRowId.RUT_ASESOR),
        NombreAsesor: capitalizeEachWord(selRowId.NOMBRE_ASESOR),
        FechaUltProceso: selRowId.FECHA_CIERRE_CUENTA,
        UltimaFechaConsulta: selRowId.FECHA_CIERRE_CUENTA,
        FechaCierreVirtual: vFechaCierreVirtual,

        Direccion: null,
        Telefono: null
    };
    traeDireccionesCliente(miInformacionCuenta.IdCliente, 'CU');
    localStorage.CuentaSeleccionada = JSON.stringify(miInformacionCuenta);
}
function seteaInformacionCliente(selRowId) {
    miInformacionCliente = {
        IdCliente: selRowId.ID_CLIENTE,
        RutCliente: capitalizeEachWord(selRowId.RUT_CLIENTE),
        NombreCliente: capitalizeEachWord(selRowId.NOMBRE_CLIENTE),
        FechaCreacion: selRowId.FECHA_CREACION,
        Email: selRowId.EMAIL,
        Genero: capitalizeEachWord(selRowId.DSC_SEXO),
        TipoPersona: capitalizeEachWord(selRowId.TIPO_ENTIDAD),
        Direccion: null,
        FechaCierreVirtual: selRowId.FECHA_CIERRE_VIRTUAL
    };
    traeDireccionesCliente(miInformacionCliente.IdCliente, 'CL');
    localStorage.ClienteSeleccionado = JSON.stringify(miInformacionCliente);
}
function traeDireccionesCliente(id, tipo) {
    $.ajax({
        url: "../Servicios/BusquedaCuClGr.asmx/BuscarDireccionesCliente",
        data: "{'idCliente':'" + id + "'}",
        dataType: "json",
        type: "post",
        async: false,
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var direcciones = JSON.parse(jsondata.responseText).d;
                if (direcciones.length) {
                    if (tipo == 'CU') {
                        miInformacionCuenta.Direccion = capitalizeEachWord(direcciones[0].DIRECCION);
                        miInformacionCuenta.Telefono = direcciones[0].FONO;
                    }
                    else {
                        miInformacionCliente.Direccion = capitalizeEachWord(direcciones[0].DIRECCION);
                    }
                }
            }
            else {
                alert(JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}
function seteaInformacionGrupo(selRowId) {
    miInformacionGrupo = {
        IdGrupo: selRowId.ID_GRUPO_CUENTA,
        NombreGrupo: capitalizeEachWord(selRowId.DSC_GRUPO_CUENTA),
        Email: selRowId.EMAIL,
        Telefono: capitalizeEachWord(selRowId.TELEFONO),
        NombreAsesor: capitalizeEachWord(selRowId.NOMBRE),
        Direccion: capitalizeEachWord(selRowId.DIRECCION),
        FechaOperativa: selRowId.FECHA_OPERATIVA,
        CantidadCuentas: selRowId.CANT_CUENTAS,
        FechaCierreVirtual: selRowId.FECHA_CIERRE_VIRTUAL
    };
    localStorage.GrupoSeleccionado = JSON.stringify(miInformacionGrupo);
}

$(document).ready(function () {

    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    initMasterFiltroCuenta();
    cargarEventHandlersMFiltroCuenta();

    resize();
});