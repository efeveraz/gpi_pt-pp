﻿var ancho;
var height;
var alto;

var rows_selectedUsu = [];
var GetTablaUsuarios;

var accion = 'guardar';
var actualizaContrasena = false;

var varEstadosUsuario = [];
var varRolesUsuario = [];
var varEmpresas = [];
var varObjEditUsuario = {};
var varCuentasAsociadas = [];
var rutValido = false;

function resize() {
    alto = $(window).height();
    ancho = $(window).width();
    height = 0;

    height = $("#content").outerHeight(true) - 85;
    var vFiltroAsesor = $('#DDFiltroCuentas').val();

    if (alto > 440) {
        $('div.dataTables_scrollBody').height(alto - 290);
    }

    var heightPnlAsociaciones = $("#content").outerHeight(true);
    var heightPermisos = heightPnlAsociaciones - 152;

    $("#lisPnlAsociar").height(heightPermisos - 70 - 5);
    $("#lisPnlDisponible").height(heightPermisos - 70 - 5);
    $("#lisUsuarioAsociar").height(heightPermisos - 104 - 5);
    if (vFiltroAsesor === 'TOD') {
        $("#lisUsuarioDisponible").height(heightPermisos - 104 - 5);
    } else {
        $("#lisUsuarioDisponible").height(heightPermisos - 104 - 44 - 5);
    }

    $("#idPnlCtas").height(heightPermisos);
}

function initMantenedorUsuario() {
    $("#idSubtituloPaginaText").text("Mantenedor Usuario");
    document.title = "Usuarios";

    consultaAsesores();
    consultarRoles();
    consultarEmpresas();
    consultaEstado();
}

function limpiarFormularioUsuario() {
    $("#txtNombreUsuario").val('');
    $("#DDAsesorAgregar").val(0).change();
    $("#txtPasswordUsuario").val('');
    $("#txtPasswordUsuarioVali").val('');
    $("#divInfoUsuarioEmpresa").empty();

    $("#txtRut").val('');
    rutValido = false;
    $('#lbltxtRut').addClass("hidden").text("");
    $("#divtxtRut").removeClass("has-error");
}

function eliminaUsuario(cadenaDscUsuario) {
    var estUsuario = $("#DDEstado").val();

    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: "post",
        url: "../Servicios/Usuario.asmx/eliminarUsuario",
        data: "{'cadenaDscUsuario':'" + cadenaDscUsuario + "'}",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                consultarUsuarios(estUsuario, cargarEventHandlersTablaUsuario);
                alertaColor(0, "Usuario eliminado correctamente");
            } else {
                alertaColor(3, "Ocurrió un error al grabar la operación");
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function consultaExisteUsuarioNombre(nombreUsuario) {
    var noExisteUsuario = false;
    var existeUsuario = true;

    $.ajax({
        url: "../Servicios/Usuario.asmx/consultaExisteUsuarioNombre",
        data: "{'nomUsuario':'" + nombreUsuario +
            "', 'rutUsuario':'" + rutUsuario + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                if (res.length === 0) {
                    sigueValidacion(noExisteUsuario, res);
                } else {
                    sigueValidacion(existeUsuario, res);
                }
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}
function consultaExisteUsuarioNombreId(nombreUsuario, rutUsuario) {
    var cadenaIdUsuario = "";
    var noExisteUsuario = false;
    var existeUsuario = true;

    $.each(varObjEditUsuario.usuarioEmpresa, function (index, value) {
        if (value.idUsuario !== null) {
            if (cadenaIdUsuario == "") {
                cadenaIdUsuario = value.idUsuario;
            } else {
                cadenaIdUsuario = cadenaIdUsuario + "," + value.idUsuario;
            }
        }
    });

    $.ajax({
        url: "../Servicios/Usuario.asmx/consultaExisteUsuarioNombreId",
        data: "{'nomUsuario':'" + nombreUsuario +
            "', 'rutUsuario':'" + rutUsuario +
            "', 'listIdUsuario':'" + cadenaIdUsuario + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                if (res.length === 0) {
                    sigueValidacion(noExisteUsuario, res);
                } else {
                    sigueValidacion(existeUsuario, res);
                }
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function sigueValidacion(pExisteUsuario, resUserExists) {
    var vTodoLoRequerido = true;
    var vNombreUsuario = $("#txtNombreUsuario").val().trim();
    var vRutUsuario = $("#txtRut").val().trim();

    var vPassword = $("#txtPasswordUsuario").val().trim();
    var vPasswordVali = $("#txtPasswordUsuarioVali").val().trim();

    if (pExisteUsuario === true) {
        lengthRut = resUserExists.filter(x => x.RUT === vRutUsuario).length
        lengthNombre = resUserExists.filter(x => x.DSC_USUARIO === vNombreUsuario).length

        if (lengthNombre > 0) {
            vTodoLoRequerido = false;
            $('#lblErrNomUsuario').removeClass("hidden").text("El nombre de usuario ya existe");
            $("#divTxtAgregarUsuario").addClass("has-error");
        }
        if (lengthRut > 0) {
            vTodoLoRequerido = false;
            $('#lbltxtRut').removeClass("hidden").text("El Rut de usuario ya existe");
            $("#divtxtRut").addClass("has-error");
        }
    }
    if (vNombreUsuario == "") {
        vTodoLoRequerido = false;
        $('#lblErrNomUsuario').removeClass("hidden").text("Debe ingresar nombre de usuario");
        $("#divTxtAgregarUsuario").addClass("has-error");
    }
    if (actualizaContrasena) {
        if (vPassword == "") {
            vTodoLoRequerido = false;
            $('#lblErrPass').removeClass("hidden").text("Debe ingresar Contraseña");
            $("#divTxtPasswordUsuario").addClass("has-error");
        }
        if (vPassword != vPasswordVali) {
            vTodoLoRequerido = false;
            $('#lblErrPassVali').removeClass("hidden").text("Las Contraseñas deben coincidir");
            $("#divTxtPasswordUsuarioVali").addClass("has-error");
        }
    }
    if (vRutUsuario === "" || !rutValido) {
        $("#txtRut").siblings('span').removeClass("hidden");
        $("#txtRut").parent().addClass("has-error");
        vRutUsuario == "" ? $("#lbltxtRut").text('Ingrese Rut') : $("#lbltxtRut").text('Ingrese Rut Válido');
        vTodoLoRequerido = false
    }

    if (vTodoLoRequerido === true) {
        if (accion === "guardar") {
            crearNuevoUsuario();
        }
        if (accion === "editar") {
            editarUsuario();
        }
    }
}

function desactivaBordetxtGuardar() {
    $('#lblErrNomUsuario').addClass("hidden").text('');
    $('#lblErrPass').addClass("hidden").text('');
    $('#lblErrPassVali').addClass("hidden").text('');
    $("#divTxtAgregarUsuario").removeClass("has-error");
    $("#divTxtPasswordUsuario").removeClass("has-error");
    $("#divTxtPasswordUsuarioVali").removeClass("has-error");
}

function crearNuevoUsuario() {
    var vDscUsuario = $("#txtNombreUsuario").val().trim();
    var vIdAsesor = $("#DDAsesorAgregar").val();
    var vPassword = $("#txtPasswordUsuario").val();

    var vEstUsuario = $("#DDEstado").val();
    var vRut = $("#txtRut").val().trim();

    var objUsuarioEmpresa = JSON.stringify(varObjEditUsuario.usuarioEmpresa);
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: "post",
        url: "../Servicios/Usuario.asmx/guardarNuevoUsuario",
        data: "{ 'pDscUsuario':'" + vDscUsuario +
            "', 'pIdAsesor':" + (vIdAsesor == -1 ? null : "" + vIdAsesor + "") +
            ", 'pPassword':'" + vPassword +
            "', 'objUsuarioEmpresa':" + objUsuarioEmpresa +
            ", 'pRut':'" + vRut + "'}",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                if (res == true) {
                    $("#panelAgregar").addClass('hidden');
                    $("#panelUsuario").removeClass('hidden');
                    consultarUsuarios(vEstUsuario, cargarEventHandlersTablaUsuario);
                    limpiarFormularioUsuario();
                    $(window).trigger('resize');
                    alertaColor(0, "Usuario creado correctamente");
                } else {
                    alertaColor(3, "Ocurrió un error al grabar la operación");
                }
            } else {
                alertaColor(3, "Ocurrió un error al grabar la operación");
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function editarUsuario() {
    var vDscUsuarioAnt = varObjEditUsuario.dscUsuario;
    var vDscUsuarioDes = $("#txtNombreUsuario").val().trim();
    var vIdAsesor = $("#DDAsesorAgregar").val();
    var vPassword = $("#txtPasswordUsuario").val();

    var objUsuarioEmpresa = JSON.stringify(varObjEditUsuario.usuarioEmpresa);
    var estUsuario = $("#DDEstado").val();
    var vRut = $("#txtRut").val().trim();

    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: "post",
        url: "../Servicios/Usuario.asmx/actualizaUsuario",
        data: "{'pDscUsuarioAnt':'" + vDscUsuarioAnt +
            "', 'pDscUsuarioDes':'" + vDscUsuarioDes +
            "', 'pIdAsesor':" + (vIdAsesor == -1 ? null : "" + vIdAsesor + "") +
            ", 'pPassword':'" + vPassword +
            "', 'actualizaContrasena':" + actualizaContrasena +
            ", 'objUsuarioEmpresa':" + objUsuarioEmpresa +
            ", 'pRut':'" + vRut + "' }",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                if (res == true) {
                    $("#panelAgregar").addClass('hidden');
                    $("#panelUsuario").removeClass('hidden');
                    consultarUsuarios(estUsuario, cargarEventHandlersTablaUsuario);
                    limpiarFormularioUsuario();
                    $(window).trigger('resize');
                    alertaColor(0, "Usuario editado correctamente");
                } else {
                    alertaColor(3, "Ocurrió un error al grabar la operación");
                }
            } else {
                alertaColor(3, "Ocurrió un error al grabar la operación");
            }
            $('#loaderProceso').modal('hide');
        }
    });
}

function consultaCuentasPermisoUsuario() {
    var cadenaHtml
    $('#loaderProceso').modal();
    $.ajax({
        url: "../Servicios/Usuario.asmx/consultaCuentasAsociadasPorDscUsuario",
        data: "{'pDscUsuario':'" + rows_selectedUsu[0] + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var cuentas = JSON.parse(jsondata.responseText).d;
                $("#ulLisCtaAsociada").empty();
                varCuentasAsociadas = [];
                cadenaHtml = "";
                $.each(cuentas, function (key, val) {
                    cadenaHtml += "<li class='list-group-item' value='" + val.ID_CUENTA + "'><div style='width: 45px;' class='numCta'>" + val.NUM_CUENTA + "</div><vr class='line'></vr><div style='width: 140px;' class='dscCta'>" + val.DSC_CUENTA + "</div><vr class='line'></vr><div class='dscEmpresa'>" + val.DSC_EMPRESA + "</div></li>";
                    varCuentasAsociadas.push(val.ID_CUENTA);
                })
                $("#ulLisCtaAsociada").append(cadenaHtml);
                styleCheckbox();

                consultaCuentasDisponiblesUsuario();
            } else {
                $('#loaderProceso').modal('hide');
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function consultaCuentasDisponiblesUsuario() {
    $('#loaderProceso').modal();
    var cadenaHtml

    $.ajax({
        url: "../Servicios/Usuario.asmx/consultaCuentasDisponiblesPorDscUsuario",
        data: "{'pDscUsuario':'" + rows_selectedUsu[0] + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var cuentas = JSON.parse(jsondata.responseText).d;

                $("#ulLisCtaDisponible").empty();
                cadenaHtml = "";

                $.each(cuentas, function (key, val) {
                    var vIdCuenta = val.ID_CUENTA;
                    var vNumCuenta = val.NUM_CUENTA;
                    var vDscCuenta = val.DSC_CUENTA;
                    var vDscEmpresa = val.DSC_EMPRESA;

                    var encontrado = false;
                    $("#ulLisCtaAsociada li").each(function (val, li) {
                        var idCtaAso = $(this).val();
                        if (idCtaAso === vIdCuenta) {
                            encontrado = true;
                            return false;
                        }
                    });

                    if (encontrado === false) {
                        cadenaHtml += "<li class='list-group-item' value='" + vIdCuenta + "'><div style='width: 45px;' class='numCta'>" + vNumCuenta + "</div><vr class='line'></vr><div style='width: 140px;' class='dscCta'>" + vDscCuenta + "</div><vr class='line'></vr><div class='dscEmpresa'>" + vDscEmpresa + "</div></li>";
                    }
                });

                $("#ulLisCtaDisponible").append(cadenaHtml);
                styleCheckbox();

                $('#loaderProceso').modal('hide');
                $("#panelUsuarioCuenta").removeClass('hidden');
                $("#panelUsuario").addClass('hidden');
            } else {
                $('#loaderProceso').modal('hide');
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function consultaCuentasAsesor() {
    var cadenaHtml
    var idAsesor = $('#DDAsesorCuenta').val();

    $.ajax({
        url: "../Servicios/Usuario.asmx/consultaCuentasAsesor",
        data: "{'pIdAsesor':" + idAsesor + ", 'pDscUsuario':'" + rows_selectedUsu[0] + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var cuentas = JSON.parse(jsondata.responseText).d;
                $("#ulLisCtaDisponible").empty();
                cadenaHtml = "";

                $.each(cuentas, function (key, val) {
                    var vIdCuenta = val.ID_CUENTA;
                    var vNumCuenta = val.NUM_CUENTA;
                    var vDscCuenta = val.DSC_CUENTA;
                    var vDscEmpresa = val.DSC_EMPRESA;

                    var encontrado = false;
                    $("#ulLisCtaAsociada li").each(function (val, li) {
                        var idCtaAso = $(this).val();
                        if (idCtaAso === vIdCuenta) {
                            encontrado = true;
                            return false;
                        }
                    });

                    if (encontrado === false) {
                        cadenaHtml += "<li class='list-group-item' value='" + vIdCuenta + "'><div style='width: 45px;' class='numCta'>" + vNumCuenta + "</div><vr class='line'></vr><div style='width: 140px;' class='dscCta'>" + vDscCuenta + "</div><vr class='line'></vr><div class='dscEmpresa'>" + vDscEmpresa + "</div></li>";
                    }
                });

                $("#ulLisCtaDisponible").append(cadenaHtml);
                styleCheckbox();
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function consultaCuentasEmpresa() {
    var cadenaHtml
    var idEmpresa = $('#DDEmpresaCuenta').val();

    $.ajax({
        url: "../Servicios/Usuario.asmx/consultaCuentasEmpresa",
        data: "{'pIdEmpresa':" + idEmpresa + ", 'pDscUsuario':'" + rows_selectedUsu[0] + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var cuentas = JSON.parse(jsondata.responseText).d;
                $("#ulLisCtaDisponible").empty();
                cadenaHtml = "";

                $.each(cuentas, function (key, val) {
                    var vIdCuenta = val.ID_CUENTA;
                    var vNumCuenta = val.NUM_CUENTA;
                    var vDscCuenta = val.DSC_CUENTA;
                    var vDscEmpresa = val.DSC_EMPRESA;

                    var encontrado = false;
                    $("#ulLisCtaAsociada li").each(function (val, li) {
                        var idCtaAso = $(this).val();
                        if (idCtaAso === vIdCuenta) {
                            encontrado = true;
                            return false;
                        }
                    });

                    if (encontrado === false) {
                        cadenaHtml += "<li class='list-group-item' value='" + vIdCuenta + "'><div style='width: 45px;' class='numCta'>" + vNumCuenta + "</div><vr class='line'></vr><div style='width: 140px;' class='dscCta'>" + vDscCuenta + "</div><vr class='line'></vr><div class='dscEmpresa'>" + vDscEmpresa + "</div></li>";
                    }
                });

                $("#ulLisCtaDisponible").append(cadenaHtml);
                styleCheckbox();
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function asociarCtas() {
    $("#ulLisCtaDisponible li.active").each(function (val, li) {
        var numCta = $(li).children('div.numCta').text().trim();
        var dscCta = $(li).children('div.dscCta').text().trim();
        var dscEmpresa = $(li).children('div.dscEmpresa').text().trim();
        $("#ulLisCtaAsociada").append('<li class="list-group-item" value=' + $(li).val() + '><div style="width: 45px;" class="numCta">' + numCta + '</div><vr class="line"></vr><div style="width: 140px;" class="dscCta">' + dscCta + '</div><vr class="line"></vr><div class="dscEmpresa">' + dscEmpresa + '</div></li>');
        $("#ulLisCtaDisponible" + " li[value='" + $(li).val() + "']").remove();
        varCuentasAsociadas.push($(li).val());
    });
    styleCheckbox();
}

function desAsociarCtas() {
    $("#ulLisCtaAsociada li.active").each(function (val, li) {
        var numCta = $(li).children('div.numCta').text().trim();
        var dscCta = $(li).children('div.dscCta').text().trim();
        var dscEmpresa = $(li).children('div.dscEmpresa').text().trim();
        $("#ulLisCtaDisponible").append('<li class="list-group-item" value="' + $(li).val() + '"><div style="width: 45px;" class="numCta">' + numCta + '</div><vr class="line"></vr><div style="width: 140px;" class="dscCta">' + dscCta + '</div><vr class="line"></vr><div class="dscEmpresa">' + dscEmpresa + '</div></li>');
        $("#ulLisCtaAsociada" + " li[value='" + $(li).val() + "']").remove();
        varCuentasAsociadas.splice(varCuentasAsociadas.indexOf($(li).val()), 1);
    });
    styleCheckbox();
}

function resetCuentas() {
    document.getElementById('DDFiltroCuentas').value = 'TOD';
    $('#txtBuscarDisponible').val('');
    $('#txtBuscarAsociados').val('');
    consultaCuentasPermisoUsuario();
    ocultarElemento('#DDAsesorCuenta');
    ocultarElemento('#DDEmpresaCuenta');
    resize();
}

function asociarCtasTodos() {
    $("#ulLisCtaDisponible li").each(function (val, li) {
        var numCta = $(li).children('div.numCta').text().trim();
        var dscCta = $(li).children('div.dscCta').text().trim();
        var dscEmpresa = $(li).children('div.dscEmpresa').text().trim();
        $("#ulLisCtaAsociada").append('<li class="list-group-item" value=' + $(li).val() + '><div style="width: 45px;" class="numCta">' + numCta + '</div><vr class="line"></vr><div style="width: 140px;" class="dscCta">' + dscCta + '</div><vr class="line"></vr><div class="dscEmpresa">' + dscEmpresa + '</div></li>');
        $("#ulLisCtaDisponible" + " li[value='" + $(li).val() + "']").remove();
        varCuentasAsociadas.push($(li).val());
    })
    styleCheckbox();
}

function desAsociarCtasTodos() {
    $("#ulLisCtaAsociada li").each(function (val, li) {
        var numCta = $(li).children('div.numCta').text().trim();
        var dscCta = $(li).children('div.dscCta').text().trim();
        var dscEmpresa = $(li).children('div.dscEmpresa').text().trim();
        $("#ulLisCtaDisponible").append('<li class="list-group-item" value="' + $(li).val() + '"><div style="width: 45px;" class="numCta">' + numCta + '</div><vr class="line"></vr><div style="width: 140px;" class="dscCta">' + dscCta + '</div><vr class="line"></vr><div class="dscEmpresa">' + dscEmpresa + '</div></li>');
        $("#ulLisCtaAsociada" + " li[value='" + $(li).val() + "']").remove();
        varCuentasAsociadas.splice(varCuentasAsociadas.indexOf($(li).val()), 1)
    })
    styleCheckbox();
}

function guardarAsociacionPermisoUsuarioCuenta() {
    var cantAsociadas = varCuentasAsociadas.length;
    var cadenaidCuentas = "";

    $('#loaderProceso').modal();
    cadenaidCuentas = varCuentasAsociadas.join();

    $.ajax({
        url: "../Servicios/Usuario.asmx/asociacionPermisoUsuarioCuenta",
        data: "{'pDscUsuario':'" + rows_selectedUsu[0] +
            "', 'pCadenaIdCuentas':'" + cadenaidCuentas + "'}",
        type: "post",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                if (res == true) {
                    alertaColor(0, "Operación grabada correctamente");
                } else {
                    alertaColor(3, "Ocurrió un error al grabar la operación");
                }
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $("#panelUsuarioCuenta").addClass('hidden');
            $("#panelUsuario").removeClass('hidden');
            $('#loaderProceso').modal('hide');
            $(window).trigger('resize');
        }
    });
}

function consultarUsuarios(estadoUsuario, callback) {
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    GetTablaUsuarios = $('#tablaUsuarios').DataTable({
        data: {},
        columns: [
            {
                "searchable": false,
                "orderable": false,
                "defaultContent": "<div class='checkbox'><input value='' type='checkbox'><label></label></div>",
                width: '1%',
                className: 'dt-body-center',
            },
            { "data": "DSC_USUARIO" },
            { "data": "RUT" },
            { "data": "ASESOR" },
            { "data": "ROLES" },
            { "data": "EMPRESAS" }
        ],
        destroy: true,
        language: { "url": "../js/vendor/DataTables/i18n/Spanish.json" },
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        order: [[1, 'asc']],
        ajax: {
            url: 'MantenedorUsuario.aspx/consultarUsuarios',
            data: function (d) {
                return JSON.stringify({ "estUsuario": estadoUsuario });
            },
            type: 'post',
            contentType: 'application/json',
            dataSrc: 'd'
        },
        columnDefs: [
            { "width": "50px", "targets": 2 }
        ],
        scrollY: alto - 250,
        scrollX: true,
        scrollCollapse: false,
        fixedColumns: false,
        //responsive: true,
        paging: true,
        rowCallback: function () {
            $(window).trigger('resize');
        }
    });
    $('#loaderProceso').modal('hide');
    rows_selectedUsu = [];
    estadosBotones();
    if (typeof callback === "function") {
        callback();
    };
}

function cargarEventHandlersTablaUsuario() {

    // Evento click "Seleccionar todo"
    $('#select_all').on('click', function (e) {
        if (this.checked) {
            $('#tablaUsuarios tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#tablaUsuarios tbody input[type="checkbox"]:checked').trigger('click');
        }
        // Evitar que el evento de clic se propague a los padres
        e.stopPropagation();
    });

    GetTablaUsuarios.on('draw', function () {
        updateDataTableSelectAllCtrl(GetTablaUsuarios);
    });
}

function updateDataTableSelectAllCtrl(table) {
    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[id="select_all"]', $table).get(0);

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        $('thead input[id="select_all"]').prop('checked', false)
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        $('thead input[id="select_all"]').prop('checked', true)
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        $('thead input[id="select_all"]').prop('checked', true)
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
}
function styleCheckbox() {
    $('.list-group.checked-list-box .list-group-item').each(function () {
        var $widget = $(this),
            $checkbox = $('<input type="checkbox" class="hidden" />'),
            color = ($widget.data('color') ? $widget.data('color') : "primary"),
            style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        $widget.css('cursor', 'pointer')
        $widget.append($checkbox);

        // Event Handlers
        $widget.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $widget.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $widget.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$widget.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $widget.addClass(style + color + ' active');
            } else {
                $widget.removeClass(style + color + ' active');
            }
        }

        // Initialization
        function init() {

            if ($widget.data('checked') == true) {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
            }

            updateDisplay();

            // Inject the icon if applicable
            if ($widget.find('.state-icon').length == 0) {
                $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '" style="padding-right: 5px;"></span>');
            }
        }
        init();
    });

    $('#get-checked-data').on('click', function (event) {
        event.preventDefault();
        var checkedItems = {}, counter = 0;
        $("#check-list-box li.active").each(function (idx, li) {
            checkedItems[counter] = $(li).text();
            counter++;
        });
        $('#display-json').html(JSON.stringify(checkedItems, null, '\t'));
    });
}

function ocultarElemento(elemento) {
    $(elemento).removeClass('in');
    $(elemento).addClass('hidden');
}
function mostrarElemento(elemento) {
    $(elemento).addClass('in');
    $(elemento).removeClass('hidden');
}

function consultaAsesores() {
    $.ajax({
        url: "MantenedorUsuario.aspx/DatosAsesor",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var asesores = JSON.parse(jsondata.responseText).d;
                $("#DDAsesorAgregar").empty();
                $.each(asesores, function (key, val) {
                    $("#DDAsesorAgregar").append('<option value="' + val.ID_ASESOR + '">' + val.NOMBRE + '</option>');
                });
                $("#DDAsesorAgregar").prepend('<option value="' + -1 + '"></option>');
                document.getElementById('DDAsesorAgregar').options.selectedIndex = 0;
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function consultaAsesoresHabilitadoConCuentas() {
    $.ajax({
        url: "MantenedorUsuario.aspx/consultaAsesorHabilitadoConCuentas",
        data: "{'pDscUsuario':'" + rows_selectedUsu[0] + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var asesores = JSON.parse(jsondata.responseText).d;

                $("#DDAsesorCuenta").empty();
                $.each(asesores, function (key, val) {
                    $("#DDAsesorCuenta").append('<option value="' + val.ID_ASESOR + '">' + val.NOMBRE + '</option>');
                });
                $("#DDAsesorCuenta").prepend('<option value="' + 0 + '">Seleccione</option>');
                document.getElementById('DDAsesorCuenta').options.selectedIndex = 0;
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function consultaEmpresasSegunDscUsuario() {
    $.ajax({
        url: "MantenedorUsuario.aspx/consultaEmpresasSegunDscUsuario",
        data: "{'pDscUsuario':'" + rows_selectedUsu[0] + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var asesores = JSON.parse(jsondata.responseText).d;

                $("#DDEmpresaCuenta").empty();
                varCuentasEmpresa = [];
                $.each(asesores, function (key, val) {
                    $("#DDEmpresaCuenta").append('<option value="' + val.ID_EMPRESA + '">' + val.DSC_EMPRESA + '</option>');
                    varCuentasEmpresa.push({ idEmpresa: val.ID_EMPRESA, cadenaCuentas: null });
                });
                $("#DDEmpresaCuenta").prepend('<option value="' + 0 + '">Seleccione</option>');
                document.getElementById('DDEmpresaCuenta').options.selectedIndex = 0;
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function consultarEmpresas() {
    $.ajax({
        url: "MantenedorUsuario.aspx/consultaEmpresas",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var empresas = JSON.parse(jsondata.responseText).d;

                varEmpresas = [];
                $.each(empresas, function (key, val) {
                    varEmpresas.push({ ID_EMPRESA: val.ID_EMPRESA, DSC_EMPRESA: val.DSC_EMPRESA, COLOR_EMPRESA: val.COLOR_EMPRESA, LOGO_EMPRESA: val.LOGO_EMPRESA });
                });
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function consultaEstado() {
    $.ajax({
        url: "MantenedorUsuario.aspx/DatosEstado",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var estados = JSON.parse(jsondata.responseText).d;

                varEstadosUsuario = [];
                $.each(estados, function (key, val) {
                    varEstadosUsuario.push({ 'COD_ESTADO': val.COD_ESTADO, 'DSC_ESTADO': val.DSC_ESTADO });
                });
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function consultarRoles() {
    $.ajax({
        url: "MantenedorUsuario.aspx/consultarRolesExistentes",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var roles = JSON.parse(jsondata.responseText).d;

                varRolesUsuario = [];
                $.each(roles, function (key, val) {
                    varRolesUsuario.push({ ID_ROL: val.ID_ROL, ID_EMPRESA: val.ID_EMPRESA, DSC_ROL: val.DSC_ROL });
                });
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function initAgregarUsuario() {

    varObjEditUsuario = {};

    varObjEditUsuario = {
        dscUsuario: null,
        idAsesor: null,
        usuarioEmpresa: []
    };

    var cadenaHtml;

    $("#divInfoUsuarioEmpresa").empty();

    cadenaHtml = "";
    $.each(varEmpresas, function (key, val) {
        cadenaHtml += agregaHtml(val);
        varObjEditUsuario.usuarioEmpresa.push({ idUsuario: null, codEstado: 'V', idEmpresa: val.ID_EMPRESA, roles: null }) //, roles: [                    
    });
    $("#divInfoUsuarioEmpresa").append(cadenaHtml);

    $.each(varEstadosUsuario, function (key, val) {
        $(".DDEstadoAgregar").append('<option value="' + val.COD_ESTADO + '">' + val.DSC_ESTADO + '</option>');
    });
    $.each(varObjEditUsuario.usuarioEmpresa, function (key, val) {
        $('#set_' + val.idEmpresa + ' .DDEstadoAgregar').val(val.codEstado).change();
    });

    $(".selectPerfiles").select2({
        theme: "bootstrap",
        width: '100%'
    });
    $.each(varObjEditUsuario.usuarioEmpresa, function (key, val) {
        consultaRoles(val.idEmpresa)
    });
    cargaEmpresasDisponibles();
}

function initEditUsuario() {
    var data = GetTablaUsuarios.row($('#tablaUsuarios tbody input[type="checkbox"]:checked').parents('tr')).data();

    $("#txtNombreUsuario").val(data.DSC_USUARIO);
    $("#txtRut").val(data.RUT).change();
    if (data.ID_ASESOR == null) {
        $("#DDAsesorAgregar").val(0).change();
    } else {
        var cadenaIdAsesor = data.ID_ASESOR;
        var idAsesor = cadenaIdAsesor.split(", ");
        $("#DDAsesorAgregar").val(idAsesor).change();
    }

    varObjEditUsuario = {};

    varObjEditUsuario = {
        dscUsuario: data.DSC_USUARIO,
        idAsesor: data.ID_ASESOR,
        usuarioEmpresa: []
    };

    var cadenaHtml;
    $.ajax({
        url: "MantenedorUsuario.aspx/consultaUsuarioEmpresas",
        data: "{'dscUsuario':'" + data.DSC_USUARIO + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var respuesta = JSON.parse(jsondata.responseText).d;

                $("#divInfoUsuarioEmpresa").empty();

                cadenaHtml = "";
                $.each(respuesta, function (key, val) {
                    cadenaHtml += agregaHtml(val);
                    varObjEditUsuario.usuarioEmpresa.push({ idUsuario: val.ID_USUARIO, codEstado: val.COD_ESTADO, idEmpresa: val.ID_EMPRESA, roles: null })
                });
                $("#divInfoUsuarioEmpresa").append(cadenaHtml);

                $.each(varEstadosUsuario, function (key, val) {
                    $(".DDEstadoAgregar").append('<option value="' + val.COD_ESTADO + '">' + val.DSC_ESTADO + '</option>');
                });
                $.each(varObjEditUsuario.usuarioEmpresa, function (key, val) {
                    $('#set_' + val.idEmpresa + ' .DDEstadoAgregar').val(val.codEstado).change();
                });

                $(".selectPerfiles").select2({
                    theme: "bootstrap",
                    width: '100%'
                });
                $.each(varObjEditUsuario.usuarioEmpresa, function (key, val) {
                    consultaRoles(val.idEmpresa)
                });
                consultaRolesUsuario(data.DSC_USUARIO)
                cargaEmpresasDisponibles();

            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function cargaEmpresasDisponibles() {
    $("#selectEmpresas").empty();
    var mostrarDivEmpresa = false;

    $.each(varEmpresas, function (key, val) {
        var idEmpresa = val.ID_EMPRESA;
        var dscEmpresa = val.DSC_EMPRESA;
        var cargarEmp = true;

        $.each(varObjEditUsuario.usuarioEmpresa, function (key, val) {
            if (val.idEmpresa == idEmpresa) {
                cargarEmp = false;
                return false;
            }
        });

        if (cargarEmp) {
            $("#selectEmpresas").append('<option value="' + idEmpresa + '">' + dscEmpresa + '</option>');
            mostrarDivEmpresa = true;
        }
    });

    if (mostrarDivEmpresa) {
        $('#divSelectEmpresas').removeClass('hidden');
        $('#divHr').removeClass('hidden');
    } else {
        $('#divSelectEmpresas').addClass('hidden');
        $('#divHr').addClass('hidden');
    }
}

function agregaHtml(val) {
    if (val.NUEVO_USUARIO) {
        var spanNuevoUsuario = "<span class='input-group-addon label-success' data-toggle='tooltip' data-placement='bottom' title='Nueva Empresa Usuario'><i class='fa fa-plus-circle fa-lg'></i></span>";
    } else {
        var spanNuevoUsuario = '';
    }

    var html =
        "<div id='set_" + val.ID_EMPRESA + "'>" +

        "<div class='col-xs-12 col-sm-3 input-group' style='margin-bottom:9px;'>" +

        "<span class='input-group-addon'>" +
        "<i class='colorEmpresa' style='background-color: rgb(" + val.COLOR_EMPRESA + "); display: inline-block; width: 20px; height: 20px; vertical-align: text-top;'></i>" +
        "</span>" +

        "<span class='input-group-addon'>" +
        "<img class='logoEmpresa' src='data:image/png;base64," + val.LOGO_EMPRESA + "' style='height: 20px;' />" +
        "</span>" +

        "<span class='input-group-addon'>" +
        "<div>" + capitalizeEachWord(val.DSC_EMPRESA) + "</div>" +
        "</span>" + spanNuevoUsuario +

        "</div>" +

        "<div class='col-xs-12 col-sm-6 col-md-4 col-lg-3'>" +
        "<label for='DDEstadoAgregar'>Estado</label>" +
        "<select  class='DDEstadoAgregar form-control input-sm' required>" +
        "</select>" +
        "</div>" +

        "<div class='col-xs-12 col-sm-6 col-md-4 col-lg-3 input-group-sm'>" +
        "<label>Roles</label>" +
        "<select class='selectPerfiles form-control input-sm' data-placeholder='Seleccione' data-minimum-results-for-search='1' multiple>" +
        "</select>" +
        "</div>" +

        "<div class='row'>" +
        "<div class='col-sm-12 margin-tb'>" +
        "</div>" +
        "</div>" +

        "</div>" +
        "<br>";

    return html;
}

function consultaRoles(pIdEmpresa) {
    $.each(varRolesUsuario, function (key, val) {
        if (val.ID_EMPRESA == pIdEmpresa) {
            $('#set_' + pIdEmpresa + ' .selectPerfiles').append('<option value="' + val.ID_ROL + '">' + val.DSC_ROL + '</option>');
        }
    });
}

function consultaRolesUsuario(pDscUsuario) {

    $.ajax({
        url: "../Servicios/Usuario.asmx/consultaRolesUsuario",
        data: "{'dscUsuario':'" + pDscUsuario + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var respuesta = JSON.parse(jsondata.responseText).d;

                $.each(varRolesUsuario, function (key, val) {
                    var idEmpresa = val.ID_EMPRESA;
                    var arrayRoles = [];
                    $.each(respuesta, function (key, val) {
                        if (val.ID_EMPRESA == idEmpresa) {
                            arrayRoles.push(val.ID_ROL);
                        }
                    })
                    $('#set_' + idEmpresa + ' .selectPerfiles').val(arrayRoles).change();
                });

                $.each(varObjEditUsuario.usuarioEmpresa, function (key, value) {
                    var selectRoles = $('#set_' + value.idEmpresa + ' .selectPerfiles').val()

                    var cadenaRoles = "";
                    var arrayRoles = selectRoles

                    if (arrayRoles == null) {
                        cadenaRoles = "";
                    } else {
                        $.each(arrayRoles, function (index, value) {
                            if (cadenaRoles == "") {
                                cadenaRoles = value;
                            } else {
                                cadenaRoles = cadenaRoles + "," + value;
                            }
                        });
                    }
                    value.roles = cadenaRoles;
                });

            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });

}

function estadosBotones() {
    var Estado = $('#DDEstado').val();
    if (rows_selectedUsu.length == 0) {
        ocultarElemento('#btnEditarUsuario');
        ocultarElemento('#btnEliminar');
        ocultarElemento('#btnPermisosCuenta');
    } else if (rows_selectedUsu.length == 1) {
        mostrarElemento('#btnEditarUsuario');
        if (Estado != 'ELI') { mostrarElemento('#btnEliminar'); }
        mostrarElemento('#btnPermisosCuenta');
    } else if (rows_selectedUsu.length > 1) {
        ocultarElemento('#btnEditarUsuario');
        if (Estado != 'ELI') { mostrarElemento('#btnEliminar'); }
        ocultarElemento('#btnPermisosCuenta');
    }
}

function cargarEventHandlersMantenedorUsuario() {

    $(".selectPerfiles").select2({
        theme: "bootstrap",
        width: '100%'
    });

    $("#selectEmpresas").select2({
        theme: "bootstrap",
        width: '100%'
    });

    $("#selectEmpresas").on("select2:select", function (e) {
        var idEmpresa = $(this).val();
        var cadenaHtml;

        $.each(varEmpresas, function (key, val) {
            if (val.ID_EMPRESA == idEmpresa) {
                cadenaHtml = "";
                cadenaHtml += agregaHtml({ ID_EMPRESA: val.ID_EMPRESA, COLOR_EMPRESA: val.COLOR_EMPRESA, LOGO_EMPRESA: val.LOGO_EMPRESA, DSC_EMPRESA: val.DSC_EMPRESA, NUEVO_USUARIO: true });

                varObjEditUsuario.usuarioEmpresa.push({ idUsuario: null, codEstado: 'V', idEmpresa: val.ID_EMPRESA });
                return false;
            }
        });

        $("#divInfoUsuarioEmpresa").append(cadenaHtml);

        $.each(varEstadosUsuario, function (key, val) {
            $('#set_' + idEmpresa + ' .DDEstadoAgregar').append('<option value="' + val.COD_ESTADO + '">' + val.DSC_ESTADO + '</option>');
        });
        $('#set_' + idEmpresa + ' .DDEstadoAgregar').val('V').change();
        $('#set_' + idEmpresa + ' [data-toggle="tooltip"]').tooltip();

        $(".selectPerfiles").select2({
            theme: "bootstrap",
            width: '100%'
        });

        consultaRoles(idEmpresa);
        cargaEmpresasDisponibles();
    });


    $("#divInfoUsuarioEmpresa").on('change', '.DDEstadoAgregar', function () {
        var vIdEmpresa = $(this).parents().eq(1).attr('id').substring(4, 5);
        var vCodEstado = $(this).val();

        $.each(varObjEditUsuario.usuarioEmpresa, function (key, val) {
            if (val.idEmpresa == vIdEmpresa) {
                val.codEstado = vCodEstado;
            }
        })
    });

    $("#divInfoUsuarioEmpresa").on("select2:select", '.selectPerfiles', function (e) {
        var vIdEmpresa = $(this).parents().eq(1).attr('id').substring(4, 5);

        $.each(varObjEditUsuario.usuarioEmpresa, function (key, value) {
            var selectRoles = $('#set_' + vIdEmpresa + ' .selectPerfiles').val()

            var cadenaRoles = "";
            var arrayRoles = selectRoles

            if (arrayRoles == null) {
                cadenaRoles = "";
            } else {
                $.each(arrayRoles, function (index, value) {
                    if (cadenaRoles == "") {
                        cadenaRoles = value;
                    } else {
                        cadenaRoles = cadenaRoles + "," + value;
                    }
                });
            }
            if (vIdEmpresa == value.idEmpresa) {
                value.roles = cadenaRoles;
            }

        });
    });

    $("#divInfoUsuarioEmpresa").on("select2:unselect", '.selectPerfiles', function (e) {
        var vIdEmpresa = $(this).parents().eq(1).attr('id').substring(4, 5);

        $.each(varObjEditUsuario.usuarioEmpresa, function (key, value) {
            var selectRoles = $('#set_' + vIdEmpresa + ' .selectPerfiles').val()

            var cadenaRoles = "";
            var arrayRoles = selectRoles

            if (arrayRoles == null) {
                cadenaRoles = "";
            } else {
                $.each(arrayRoles, function (index, value) {
                    if (cadenaRoles == "") {
                        cadenaRoles = value;
                    } else {
                        cadenaRoles = cadenaRoles + "," + value;
                    }
                });
            }
            if (vIdEmpresa == value.idEmpresa) {
                value.roles = cadenaRoles;
            }
        });
    });

    // Handle click on checkbox
    $('#tablaUsuarios tbody').on('click', 'input[type="checkbox"]', function (e) {
        var data = GetTablaUsuarios.rows($(this).parents('tr')).data();
        var vCheck = this.checked
        $.each(data, function (index, value) {
            if (vCheck) {
                rows_selectedUsu.push(value.DSC_USUARIO);
            } else if (!vCheck) {
                rows_selectedUsu.splice(rows_selectedUsu.indexOf(value.DSC_USUARIO), 1);
            }
        });

        estadosBotones();

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(GetTablaUsuarios);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on table cells with checkboxes
    $('#tablaUsuarios').on('click', 'tbody td div[class="checkbox"], thead th:first-child', function (e) {
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });

    $("#btnPermisosCuenta").on('click', function () {
        $('#DDFiltroCuentas').prop("disabled", false);
        document.getElementById('DDFiltroCuentas').options.selectedIndex = 0;
        ocultarElemento('#DDAsesorCuenta');
        ocultarElemento('#DDEmpresaCuenta');
        $('#txtBuscarAsociados').val('');
        $('#txtBuscarDisponible').val('');
        var data = GetTablaUsuarios.row($('#tablaUsuarios tbody input[type="checkbox"]:checked').parents('tr')).data();

        $("#lbUsuarioCuenta").text(data.DSC_USUARIO);
        consultaCuentasPermisoUsuario();
        consultaAsesoresHabilitadoConCuentas();
        consultaEmpresasSegunDscUsuario();
        resize();
    });

    $("#DDEstado").change(function () {
        var estUsuario = $("#DDEstado").val();
        consultarUsuarios(estUsuario, cargarEventHandlersTablaUsuario);
    });

    $("#DDFiltroCuentas").change(function () {
        var estadoFiltro = $("#DDFiltroCuentas").val();
        document.getElementById('DDAsesorCuenta').value = 0;
        document.getElementById('DDEmpresaCuenta').value = 0;

        $('#txtBuscarDisponible').val('')
        $("#ulLisCtaDisponible").empty();

        if (estadoFiltro === 'TOD') {
            ocultarElemento('#DDAsesorCuenta');
            ocultarElemento('#DDEmpresaCuenta');
            consultaCuentasDisponiblesUsuario();
        }
        if (estadoFiltro === 'ASE') {
            mostrarElemento('#DDAsesorCuenta');
            ocultarElemento('#DDEmpresaCuenta');
        }
        if (estadoFiltro === 'EMP') {
            mostrarElemento('#DDEmpresaCuenta');
            ocultarElemento('#DDAsesorCuenta');
        }
        resize();
    });

    $("#DDAsesorCuenta").change(function () {
        consultaCuentasAsesor();
    });

    $("#DDEmpresaCuenta").change(function () {
        consultaCuentasEmpresa();
    });

    $("#btnGuardarAsociar").on('click', function () {
        guardarAsociacionPermisoUsuarioCuenta();
    });
    $("#btnDesAsociarTodosV").on('click', function () {
        desAsociarCtasTodos();
    });
    $("#btnDesAsociarTodosH").on('click', function () {
        desAsociarCtasTodos();
    });
    $("#btnAsociarTodosV").on('click', function () {
        asociarCtasTodos();
    });
    $("#btnAsociarTodosH").on('click', function () {
        asociarCtasTodos();
    });
    $("#btnDesAsociarV").on('click', function () {
        desAsociarCtas();
    });
    $("#btnDesAsociarH").on('click', function () {
        desAsociarCtas();
    });
    $("#btnAsociarV").on('click', function () {
        asociarCtas();
    });
    $("#btnAsociarH").on('click', function () {
        asociarCtas();
    });
    $("#btnResetCuentas").on('click', function () {
        resetCuentas();
    });
    $("#btnResetCuentasH").on('click', function () {
        resetCuentas();
    });

    $("#txtBuscarAsociados").keyup(function () {
        $("#ulLisCtaAsociada li").css({ 'display': 'none' });
        $("#ulLisCtaAsociada li").each(function (val, li) {
            var aBuscar = ($("#txtBuscarAsociados").val()).toLowerCase();
            var cadena = ($(li).text()).toLowerCase();

            var pat = new RegExp(aBuscar);
            var res = pat.test(cadena)
            if (res == true) {
                $("#ulLisCtaAsociada li[value='" + $(li).val() + "']").css("display", "");
            }
        });
    });

    $("#btnLimpiarAso").on('click', function () {
        $("#txtBuscarAsociados").val('');
        $("#ulLisCtaAsociada li").css({ 'display': '' });
    });

    $("#btnLimpiarDes").on('click', function () {
        $("#txtBuscarDisponible").val('');
        $("#ulLisCtaDisponible li").css({ 'display': '' });
    });

    $("#txtBuscarDisponible").keyup(function () {
        $("#ulLisCtaDisponible li").css({ 'display': 'none' });
        $("#ulLisCtaDisponible li").each(function (val, li) {
            var aBuscar = ($("#txtBuscarDisponible").val()).toLowerCase();
            var cadena = ($(li).text()).toLowerCase();
            var pat = new RegExp(aBuscar);
            var res = pat.test(cadena)
            if (res == true) {
                $("#ulLisCtaDisponible li[value='" + $(li).val() + "']").css("display", "");
            }
        });
    });

    $("#btnCancelarAsignacion").on('click', function () {
        $("#panelUsuarioCuenta").addClass('hidden');
        $("#panelUsuario").removeClass('hidden');
        $(window).trigger('resize');
    });

    $("#btnNuevoUsuario").on('click', function () {
        $("#tituloNuevoEditar").text('Agregar Nuevo Usuario');
        $("#divBtnHabilitaCambioPass").addClass('hidden');
        $("#divTxtPasswordUsuario").removeClass('hidden');
        $("#divTxtPasswordUsuarioVali").removeClass('hidden');
        accion = 'guardar';
        actualizaContrasena = true;
        limpiarFormularioUsuario();
        $("#panelAgregar").removeClass('hidden');
        $("#panelUsuario").addClass('hidden');
        initAgregarUsuario();
    });

    $("#btnEditarUsuario").on('click', function () {
        $("#tituloNuevoEditar").text('Editar Usuario');
        $("#divBtnHabilitaCambioPass").removeClass('hidden');
        $("#divTxtPasswordUsuario").addClass('hidden');
        $("#divTxtPasswordUsuarioVali").addClass('hidden');
        accion = 'editar';
        actualizaContrasena = false;
        limpiarFormularioUsuario();
        $("#panelAgregar").removeClass('hidden');
        $("#panelUsuario").addClass('hidden');
        initEditUsuario();
    });

    $("#btnHabilitaCambioPass").on('click', function () {
        $("#divTxtPasswordUsuario").removeClass('hidden');
        $("#divTxtPasswordUsuarioVali").removeClass('hidden');
        $("#divBtnHabilitaCambioPass").addClass('hidden');
        actualizaContrasena = true;
    });

    $("#btnCancelar").on('click', function () {
        $("#panelAgregar").addClass('hidden');
        $("#panelUsuario").removeClass('hidden');
        desactivaBordetxtGuardar();
        limpiarFormularioUsuario();
        $(window).trigger('resize');
    });

    $("#btnEliminar").on('click', function () {
        var cant = 0;
        var confir;
        cant = rows_selectedUsu.length

        var cadenaDscUsuario = "";
        $.each(rows_selectedUsu, function (index, value) {
            if (cadenaDscUsuario == "") {
                cadenaDscUsuario = value;
            } else {
                cadenaDscUsuario = cadenaDscUsuario + "," + value;
            }
        });

        if (cant == 1) {
            confir = confirm("¿Desea eliminar Usuario " + cadenaDscUsuario + "?");
        } else if (cant > 1) {
            confir = confirm("¿Desea eliminar los " + cant + " usuarios seleccionados?");
        }

        if (confir == true) {
            eliminaUsuario(cadenaDscUsuario);
        }
    });

    $("#btnGuardarUsuario").on('click', function () {
        debugger
        desactivaBordetxtGuardar();
        var nombreUsuario = $("#txtNombreUsuario").val().trim();
        var rutUsuario = $("#txtRut").val().trim();

        var rut = $("#txtRut").val().trim();

        if (nombreUsuario != '' && rutValido) {
            if (accion === 'guardar') {
                consultaExisteUsuarioNombre(nombreUsuario);
            } else {
                consultaExisteUsuarioNombreId(nombreUsuario, rutUsuario);
            }
        } else {
            if (nombreUsuario === '') {
                $('#lblErrNomUsuario').removeClass("hidden").text("Debe ingresar nombre de usuario");
                $("#divTxtAgregarUsuario").addClass("has-error");
            }
            if (rut === '') {
                $('#lbltxtRut').removeClass("hidden").text("Debe ingresar Rut");
                $("#divtxtRut").addClass("has-error");
            }
        }
    });

    $("#txtRut").rut({ formatOn: 'keyup', validateOn: 'change keyup', dotNotation: false })
        .on('rutInvalido', function (e) {
            $("#divtxtRut").removeClass('has-success');
            $("#divtxtRut").addClass('has-error');
            $("#lbltxtRut").removeClass("hidden").text('Ingrese Rut Válido');
            rutValido = false;
        })
        .on('rutValido', function (e, rut, dv) {
            $("#divtxtRut").removeClass('has-error');
            $("#lbltxtRut").addClass("hidden");
            rutValido = true;
        });

    $("#txtNombreUsuario").keyup(function () {
        var txtNombre = $("#txtNombreUsuario").val().trim();

        if (txtNombre === "") {
            $("#divTxtAgregarUsuario").addClass("has-error");
            $('#lblErrNomUsuario').removeClass("hidden").text("Debe ingresar nombre de usuario");
        } else {
            $("#divTxtAgregarUsuario").removeClass("has-error");
            $('#lblErrNomUsuario').addClass("hidden").text("");
        }
    });
}

$(document).ready(function () {
    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    initMantenedorUsuario();
    cargarEventHandlersMantenedorUsuario();

    consultarUsuarios('V', function () {
        cargarEventHandlersTablaUsuario();
    });

    resize();
});