﻿var heightWindow;
var heightDTable;
var GetTablaClientes;

function resize() {
    heightWindow = $(window).height();
    heightDTable = $(window).height() - 268;

    if (heightWindow > 530) {
        $('div.dataTables_scrollBody').height(heightDTable);
    } else {
        $('div.dataTables_scrollBody').height(276);
    }
}

function initConsultaClientes() {
    $("#idSubtituloPaginaText").text("Consulta de Clientes");
    document.title = "Consulta de Clientes";

    cargarAsesores();

    GetTablaClientes = $('#tablaClientes').DataTable({
        destroy: true,
        language: { "url": "../js/vendor/DataTables/i18n/Spanish.json" },
        dom: "<'row'<'col-sm-6 form-inline' <'form-group'l><'form-group'B>><'col-sm-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        buttons: [
            {
                text: '<i class="fa fa-file-excel-o fa-lg">',
                className: 'btn-sm',
                action: function (e, dt, node, config) {
                    generarExcel();
                }
            }
        ],
        scrollY: heightDTable,
        scrollX: true,
        scrollCollapse: false,
        fixedColumns: false,
        paging: true,
        info: false,
        rowCallback: function () { $(window).trigger('resize'); }
    });
}

function cargarEventHandlersConsultaClientes() {

    $(window).bind('resize', function () {
        resize()
    }).trigger('resize');

    $("#BtnConsultar").button().click(function () {
        consultarClientes();
        resize();
    });
}

function cargarAsesores() {
    $('#loaderProceso').modal();
    $.ajax({
        url: "ConsultaClientes.aspx/ConsultaAsesores",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                $("#DDAsesor").empty();
                $("#DDAsesor").prepend('<option value="' + -1 + '">' + 'Todos los Asesores' + '</option>');
                $.each(res, function (key, val) {
                    $("#DDAsesor").append('<option value="' + val.ID_ASESOR + '">' + capitalizeEachWord(val.NOMBRE) + '</option>');
                });
                $("#DDAsesor").val(-1).change();
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });

}
function consultarClientes() {
    var pTipo = $("#DDTipo").val();
    var pEstado = $("#DDEstado").val();
    var pIdAsesor = $("#DDAsesor").val();

    pTipo = (pTipo == -1 ? null : pTipo);
    pEstado = (pEstado == -1 ? null : pEstado);
    pIdAsesor = (pIdAsesor == -1 ? null : pIdAsesor);

    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    GetTablaClientes = $('#tablaClientes').DataTable({
        data: {},
        columns: [
            { "data": "NOMBRE_CLIENTE" },
            { "data": "RUT_CLIENTE" },
            { "data": "ESTADO_CLIENTE" },
            { "data": "N_CUENTAS" },
            { "data": "NOM_ASESOR" },
            { "data": "DSC_SEXO" },
            { "data": "FECHA_NACIMIENTO" },
            { "data": "EMAIL" },
            { "data": "DSC_PAIS" },
            { "data": "TIPO_ENTIDAD" }
        ],
        destroy: true,
        language: { "url": "../js/vendor/DataTables/i18n/Spanish.json" },
        order: [[1, 'asc']],
        ajax: {
            url: 'ConsultaClientes.aspx/ConsultaClientes',
            data: function (d) {
                return JSON.stringify({ 'tipo': pTipo, 'estado': pEstado, 'idAsesor': pIdAsesor });
            },
            type: 'post',
            contentType: 'application/json',
            dataSrc: 'd',
            complete: function (jsondata, stat) {
                if (stat === "success") {
                    var mydata = JSON.parse(jsondata.responseText).d;
                    if (mydata.length === 0) {
                        $('#loaderProceso').modal('hide');
                    } 
                } else {
                    alertaColor(3, "Ocurrió un error en la consulta");
                }
            }

        },
        dom: "<'row'<'col-sm-6 form-inline' <'form-group'l><'form-group'B>><'col-sm-6'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        buttons: [
            {
                text: '<i class="fa fa-file-excel-o fa-lg">',
                className: 'btn-sm',
                action: function (e, dt, node, config) {
                    generarExcel();
                }
            }
        ],
        columnDefs: [
            {
                targets: 6,
                render: function (data, type, row) {
                    var fechaNac = (data ? moment(data).format("L") : "");
                    return fechaNac;
                }
            }
        ],
        scrollY: heightDTable,
        scrollX: true,
        scrollCollapse: false,
        fixedColumns: false,
        //responsive: true,
        paging: true,
        rowCallback: function () {
            $(window).trigger('resize');
            $('#loaderProceso').modal('hide');
        }
    });
}
function generarExcel() {

    var datatableColModel = [
        { name: "NOMBRE_CLIENTE", hidden: false },
        { name: "RUT_CLIENTE", hidden: false },
        { name: "ESTADO_CLIENTE", hidden: false },
        { name: "N_CUENTAS", formatter: "int", hidden: false },
        { name: "DSC_SEXO", hidden: false },
        { name: "FECHA_NACIMIENTO", formatter: "date", hidden: false },
        { name: "EMAIL", hidden: false },
        { name: "DSC_PAIS", hidden: false },
        { name: "TIPO_ENTIDAD", hidden: false }
    ]

    //var title = GetTablaClientes.columns().header();
    var datatableColumn = ["Cliente", "Rut", "Estado", "N° de Cuentas", "Sexo", "Fecha de Nacimiento", "E-mail", "País", "Tipo de Persona"];

    var datatableData = GetTablaClientes.rows().data();
    var datatableNomExcel = 'Clientes';
    var datatableTitulo = 'Clientes';
    
    DatatableAExcelASP.DatatableAExcel(datatableData, datatableColModel, datatableColumn, datatableNomExcel, datatableTitulo);
}

$(document).ready(function () {
    initConsultaClientes();
    cargarEventHandlersConsultaClientes();
    $(window).trigger('resize');
});