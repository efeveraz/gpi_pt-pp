﻿var jsfechaFormato;
var jsfechaConsulta;

function resize() {
    var height;
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#Paginador").outerHeight(true) - 138;

    if (ancho < 768) {
        $("#CarteraMesa").setGridHeight(210);
    } else {
        $("#CarteraMesa").setGridHeight(height);
    }
}

function initConsultaCarteraMesa() {
    $("#idSubtituloPaginaText").text("Consulta Cartera Mesa");
    document.title = "Consulta Cartera Mesa";

    estadoBtnConsultar();

    if (!miInformacionCuenta) {
        $(".input-group.date").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
    } else {
        $(".input-group.date").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
    }

    $("#tabsClienteCuentaGrupo").hide();
    $('.nav-tabs a[href="#tabCuenta"]').tab('show');
}
function cargarEventHandlersConsultaCarteraMesa() {

    $(window).bind('resize', function () {
        resize()
    }).trigger('resize');

    $(".input-group.date").datepicker();

    $("#CarteraMesa").jqGrid({
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID" },
        colNames: ['ID', 'Cuenta', 'Cliente', 'Rut', 'Moneda Cuenta', 'Instrumento', 'Sub-Clase', 'Nemotécnico', 'Código Instrumento SVS', '% PortFolio', '% Cartera', 'Emisor', 'Moneda', 'Nominales', 'Duración', 'Vencimiento', 'Tasa Emisión', 'Días Vcto', 'Precio/Tasa Compra', 'Monto Invertido', 'Precio/Tasa Mercado', 'Valor Mercado', 'Rentabilidad', 'Patrimonio', 'Clasificación de Riesgo', 'Fecha Compra'],
        colModel: [
            { name: "ID", index: "ID", sortable: true, width: 100, sorttype: "int", editable: false, search: true, hidden: true, align: "left" },
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: false, width: 100, sorttype: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "NOMBRE_CLIENTE", index: "NOMBRE_CLIENTE", sortable: true, width: 260, sorttype: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "RUT_CLIENTE", index: "RUT_CLIENTE", sortable: true, width: 90, sorttype: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "MONEDA_CUENTA", index: "MONEDA_CUENTA", sortable: true, width: 120, sorttype: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "DSC_INTRUMENTO", index: "DSC_INTRUMENTO", sortable: true, width: 150, sorttype: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "DSC_SUBCLASE", index: "DSC_SUBCLASE", sortable: true, width: 150, sorttype: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "NEMOTECNICO", index: "NEMOTECNICO", sortable: true, width: 150, sorttype: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "COD_INSTRUMENTO_SVS", index: "COD_INSTRUMENTO_SVS", sortable: true, width: 170, sorttype: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "PORTAFOLIO", index: "PORTAFOLIO", sortable: true, width: 100, sorttype: "int", formatter: "int", editable: false, search: true, hidden: false, align: "right" },
            { name: "CARTERA", index: "CARTERA", sortable: true, width: 90, sorttype: "int", formatter: "int", editable: false, search: true, hidden: false, align: "right" },
            { name: "COD_SVS_NEMOTECNICO", index: "COD_SVS_NEMOTECNICO", sortable: true, width: 150, sorttype: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "MONEDA_NEMO", index: "MONEDA_NEMO", sortable: true, width: 70, sorttype: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "CANTIDAD", index: "CANTIDAD", sortable: true, width: 100, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "DURACION", index: "DURACION", sortable: true, width: 100, sorttype: "text", editable: false, search: true, hidden: false, align: "right" },
            { name: "VENCIMIENTO", index: "VENCIMIENTO", sortable: true, width: 100, sorttype: "date", formatter: "date", editable: false, search: true, hidden: false, align: "center", searchoptions: { sopt: ['eq'], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "TASA_EMISION", index: "TASA_EMISION", sortable: true, width: 100, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right" },
            { name: "D_VENCIMIENTO", index: "D_VENCIMIENTO", sortable: true, width: 80, sorttype: "text", editable: false, search: true, hidden: false, align: "right" },
            { name: "PRECIO_COMPRA", index: "PRECIO_COMPRA", sortable: true, width: 140, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_VALOR_COMPRA", index: "MONTO_VALOR_COMPRA", sortable: true, width: 120, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "PRECIO", index: "PRECIO", sortable: true, width: 150, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_MON_CTA", index: "MONTO_MON_CTA", sortable: true, width: 110, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "RENTABILIDAD", index: "RENTABILIDAD", sortable: true, width: 100, sorttype: "text", editable: false, search: true, hidden: false, align: "right" },
            { name: "PATRIMONIO_MON_CUENTA", index: "PATRIMONIO_MON_CUENTA", sortable: true, width: 110, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "CLASIFICACION", index: "CLASIFICACION", sortable: true, width: 170, sorttype: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "FECHA_COMPRA", index: "FECHA_COMPRA", sortable: true, width: 110, sorttype: "date", formatter: "date", editable: false, search: true, hidden: false, align: "center", searchoptions: { sopt: ["ge", "le", "eq"], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } }
        ],
        pager: '#Paginador',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        styleUI: 'Bootstrap',
        sortname: "ID",
        sortorder: "asc",
        caption: "Cartera Mesa",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: false,
        groupingView: {
            groupField: ['NEMOTECNICO'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        }
    });

    $("#CarteraMesa").jqGrid().bindKeys().scrollingRows = true
    $("#CarteraMesa").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#CarteraMesa").jqGrid('navGrid', '#Paginador', { add: false, edit: false, del: false, excel: false, search: false });
    $("#CarteraMesa").jqGrid('navButtonAdd', '#Paginador', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('CarteraMesa', 'Cartera Mesa', 'Cartera Mesa', 'DATACOMPLETA');
        }
    });

    $("#BtnConsultar").button().click(function () {
        consultaValorCuotaPatrimonio();
    });

    $("#porCuenta_NroCuenta").on('change', function () {
        estadoBtnConsultar();
    });
}

function consultaValorCuotaPatrimonio() {
    jsfechaFormato = moment.utc($(".input-group.date").datepicker('getDate')).startOf('day').toISOString();
    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1)

    if (miInformacionCuenta == null) {
        return;
    }
    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }

    $("#CarteraMesa").jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        url: "ConsultaCarteraMesa.aspx/ConsultaCarteraMesa",
        data: "{'fechaConsulta':'" + jsfechaConsulta +
        "', 'idCuenta':" + (miInformacionCuenta == null ? "''" : miInformacionCuenta.IdCuenta) + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#CarteraMesa").setGridParam({ data: mydata });
                $("#CarteraMesa").trigger("reloadGrid");
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
};
function estadoBtnConsultar() {
    if (miInformacionCuenta == null) {
        $('#BtnConsultar').tooltip('enable').addClass('disabled');
    } else {
        $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
    }
}

$(document).ready(function () {
    initConsultaCarteraMesa();
    cargarEventHandlersConsultaCarteraMesa();
    resize();
});