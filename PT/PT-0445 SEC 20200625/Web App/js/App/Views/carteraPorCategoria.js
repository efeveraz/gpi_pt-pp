﻿
var lista = "#ListaPorCuenta";
var graficoCartera = "graficoPorCuenta";
var target = "#tabCuenta";
var urlVarAgrupadoPor = getUrlVars()["AgrupadoPor"];
var urlVarFechaConsulta = getUrlVars()["FechaConsulta"];
var jsfechaConsulta;

var graficoOpc;

var graficoPorCuenta;
var graficoPorCliente;
var graficoPorGrupo;

function resize() {
    var ancho = $(window).width();
    var height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - 37 - 84 - 22;

    if (ancho < 768) {
        $('#ListaPorCuenta').setGridHeight(220);
        $('#ListaPorCliente').setGridHeight(220);
        $('#ListaPorGrupo').setGridHeight(220);
    } else {
        $('#ListaPorCuenta').jqGrid('setGridHeight', height);
        $('#ListaPorCliente').jqGrid('setGridHeight', height);
        $('#ListaPorGrupo').jqGrid('setGridHeight', height);
    }

    $("#ListaPorCuenta").setGridWidth($("#grillaPorCuenta").width());
    $("#ListaPorCliente").setGridWidth($("#grillaPorCliente").width());
    $("#ListaPorGrupo").setGridWidth($("#grillaPorGrupo").width());

    $("#graficoPorCuenta").setGridWidth($("#grillaPorCuenta").width());
    $("#graficoPorCliente").setGridWidth($("#grillaPorCliente").width());
    $("#graficoPorGrupo").setGridWidth($("#grillaPorGrupo").width());

    if ($('#graficoPorCuenta').highcharts() != undefined) {
        $('#graficoPorCuenta').highcharts().setSize($("#grillaPorCuenta").width(), $("#grillaPorCuenta").outerHeight(true), doAnimation = false);
    }
    if ($('#graficoPorCliente').highcharts() != undefined) {
        $('#graficoPorCliente').highcharts().setSize($("#grillaPorCliente").width(), $("#grillaPorCliente").outerHeight(true), doAnimation = false);
    }
    if ($('#graficoPorGrupo').highcharts() != undefined) {
        $('#graficoPorGrupo').highcharts().setSize($("#grillaPorGrupo").width(), $("#grillaPorGrupo").outerHeight(true), doAnimation = false);
    }
}

function initCarteraPorCategoria() {
    $("#idSubtituloPaginaText").text("Cartera por Categoría");
    document.title = "Cartera por Categoría";

    if (urlVarFechaConsulta) {
        $(".input-group.date").datepicker('setDate', urlVarFechaConsulta);
    } else {
        if (!miInformacionCuenta) {
            $(".input-group.date").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
            jsfechaConsulta = moment($(".input-group.date").datepicker('getDate')).format("L");
        } else {
            $(".input-group.date").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
            jsfechaConsulta = moment($(".input-group.date").datepicker('getDate')).format("L");
        }
    }

    if (urlVarAgrupadoPor == "#ListaPorCuenta") {
        $("#contenidoPorCuenta").show();
        $("#contenidoPorCliente").hide();
        $("#contenidoPorGrupo").hide();
        $('.nav-tabs a[href="#tabCuenta"]').tab('show');
        $('#BtnConsultar').attr('data-original-title', 'Seleccione Cuenta');
        target = "#tabCuenta";
        lista = "#ListaPorCuenta";
        graficoCartera = "graficoPorCuenta";
    } else if (urlVarAgrupadoPor == "#ListaPorCliente") {
        $("#contenidoPorCuenta").hide();
        $("#contenidoPorCliente").show();
        $("#contenidoPorGrupo").hide();
        $('.nav-tabs a[href="#tabCliente"]').tab('show');
        $('#BtnConsultar').attr('data-original-title', 'Seleccione Cliente');
        target = "#tabCliente";
        lista = "#ListaPorCliente";
        graficoCartera = "graficoPorCliente";
    } else if (urlVarAgrupadoPor == "#ListaPorGrupo") {
        $("#contenidoPorCuenta").hide();
        $("#contenidoPorCliente").hide();
        $("#contenidoPorGrupo").show();
        $('.nav-tabs a[href="#tabGrupo"]').tab('show');
        $('#BtnConsultar').attr('data-original-title', 'Seleccione Grupo');
        target = "#tabGrupo";
        lista = "#ListaPorGrupo";
        graficoCartera = "graficoPorGrupo";
    } else {
        $("#contenidoPorCuenta").show();
        $("#contenidoPorCliente").hide();
        $("#contenidoPorGrupo").hide();
    }

    graficoOpc = {
        chart: {
            renderTo: 'grafico',
            plotBackgroundColor: null,
            plotBorderWidth: 1,
            plotShadow: true
        },
        title: {
            text: 'Cartera'
        },
        credits: {
            text: 'creasys.cl',
            href: 'http://www.creasys.cl'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                showInLegend: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    style: {
                        textShadow: 0
                    },
                    color: '#000000',
                    connectorColor: '#000000',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Porcentaje',
            data: []
        }],
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: [
                        {
                            text: 'Descargar como PNG',
                            onclick: function () {
                                this.exportChart();
                            }
                        }, {
                            text: 'Descargar como JPEG',
                            onclick: function () {
                                this.exportChart({
                                    type: 'image/jpeg'
                                });
                            }
                        }, {
                            text: 'Descargar como PDF',
                            onclick: function () {
                                this.exportChart({
                                    type: 'application/pdf'
                                });
                            }
                        }, {
                            text: 'Descargar como SVG',
                            onclick: function () {
                                this.exportChart({
                                    type: 'image/svg+xml'
                                });
                            }
                        }
                    ]
                }
            }
        }
    };

    graficoOpc.title.text = "Cartera Por Categoría";
    graficoOpc.chart.renderTo = "graficoPorCuenta";
    graficoPorCuenta = new Highcharts.Chart(graficoOpc);
    graficoOpc.chart.renderTo = "graficoPorCliente";
    graficoPorCliente = new Highcharts.Chart(graficoOpc);
    graficoOpc.chart.renderTo = "graficoPorGrupo";
    graficoPorGrupo = new Highcharts.Chart(graficoOpc);
}
function cargarEventHandlersCarteraPorCategoria() {

    $(".input-group.date").datepicker();

    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    var grilla = {
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID_CATEGORIA" },
        colNames: ['ID_CATEGORIA', 'Categoría', 'Porcentaje', 'Monto'],
        colModel: [
            { name: "ID_CATEGORIA", index: "ID_CATEGORIA", sortable: true, width: 70, sorttype: "int", editable: false, search: true, hidden: true },
            { name: "DSC_CATEGORIA", index: "DSC_CATEGORIA", width: 70, sortable: true, sorttype: "Text", editable: false, search: true, hidden: false, summaryType: 'count', summaryTpl: '<b>Total</b>' },
            { name: "PORC_CATEGORIA", index: "PORC_CATEGORIA", sortable: true, width: 70, sorttype: "currency", formatter: "currency", formatoptions: { suffix: '%' }, align: "right", editable: false, search: true, hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_MON_CTA", index: "MONTO_MON_CTA", sortable: true, width: 70, sorttype: "currency", formatter: "currency", align: "right", summaryType: 'sum', editable: false, search: true, hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } }
        ],
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        height: 400,
        styleUI: 'Bootstrap',
        caption: "Cartera",
        grouping: false,
        ignoreCase: true,
        regional: localeCorto,
        hidegrid: false
    }

    grilla.caption = "Cartera Por Cuenta";
    $("#ListaPorCuenta").jqGrid(grilla);
    $("#ListaPorCuenta").jqGrid().bindKeys().scrollingRows = true
    $("#ListaPorCuenta").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $(window).trigger('resize');

    grilla.caption = "Cartera Por Cliente";
    $("#ListaPorCliente").jqGrid(grilla);
    $("#ListaPorCliente").jqGrid().bindKeys().scrollingRows = true
    $("#ListaPorCliente").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $(window).trigger('resize');

    grilla.caption = "Cartera Por Grupo";
    $("#ListaPorGrupo").jqGrid(grilla);
    $("#ListaPorGrupo").jqGrid().bindKeys().scrollingRows = true
    $("#ListaPorGrupo").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $(window).trigger('resize');

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        target = $(e.target).attr("href") // activated tab

        var jsCategoria = $("#DDCategoria").val();
        estadoBtnConsultar();
        if (target == "#tabCuenta") {
            $("#contenidoPorCuenta").show();
            $("#contenidoPorCliente").hide();
            $("#contenidoPorGrupo").hide();
            lista = "#ListaPorCuenta";
            graficoCartera = "graficoPorCuenta";
            $('#BtnConsultar').attr('data-original-title', 'Seleccione Cuenta');
        }
        if (target == "#tabCliente") {
            $("#contenidoPorCuenta").hide();
            $("#contenidoPorCliente").show();
            $("#contenidoPorGrupo").hide();
            lista = "#ListaPorCliente";
            graficoCartera = "graficoPorCliente";
            $('#BtnConsultar').attr('data-original-title', 'Seleccione Cliente');
        }
        if (target == "#tabGrupo") {
            $("#contenidoPorCuenta").hide();
            $("#contenidoPorCliente").hide();
            $("#contenidoPorGrupo").show();
            lista = "#ListaPorGrupo";
            graficoCartera = "graficoPorGrupo";
            $('#BtnConsultar').attr('data-original-title', 'Seleccione Grupo');
        }
        history.pushState(null, null, '?FechaConsulta=' + jsfechaConsulta + '&AgrupadoPor=' + lista);
        $(window).trigger('resize');
    });

    $('#DDCategoria').change(function () {
        estadoBtnConsultar();
    });

    $("#BtnConsultar").on('click', function () {
        consultaCarteraPorCategoria(false);
    });

    $("#porCuenta_NroCuenta").on('change', function () {
        consultaCarteraPorCategoria(true);
    });
}

function estadoBtnConsultar() {
    if (target == "#tabCuenta") {
        if (miInformacionCuenta == null) {
            $('#BtnConsultar').tooltip('enable').addClass('disabled');
        } else {
            $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
        }
    }
    if (target == "#tabCliente") {
        if (miInformacionCliente == null) {
            $('#BtnConsultar').tooltip('enable').addClass('disabled');
        } else {
            $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
        }
    }
    if (target == "#tabGrupo") {
        if (miInformacionGrupo == null) {
            $('#BtnConsultar').tooltip('enable').addClass('disabled');
        } else {
            $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
        }
    }
}
function creaLinksLista(listaAsync) {
    var ids = $(lista).jqGrid("getDataIDs"), l = ids.length, i, rowid, status;
    for (i = 0; i < l; i++) {
        rowid = ids[i];
        vIdCategoria = $(lista).jqGrid("getCell", rowid, "ID_CATEGORIA");
        Descripcion = $(lista).jqGrid("getCell", rowid, "DSC_CATEGORIA");
        FechaConsulta = document.getElementById("dtFechaConsulta").value
        var href = 'CarteraArbolActivos.aspx?' + $("#DDCategoria option:selected").text() + "=" + vIdCategoria + "&Descripcion" + $("#DDCategoria option:selected").text() + "=" + Descripcion + "&FechaConsulta=" + FechaConsulta + "&AgrupadoPor=" + lista
        $(listaAsync).jqGrid('setCell', rowid, 'DSC_CATEGORIA', "<a href='" + href + "'>" + Descripcion + "</a>");
        $(listaAsync).jqGrid('getLocalRow', rowid).DSC_CATEGORIA = "<a href='" + href + "'>" + Descripcion + "</a>";
    }
}
function consultaCarteraPorCategoria(CargaInicial) {
    jsfechaConsulta = moment($(".input-group.date").datepicker('getDate')).format("L");
    var jsCategoria = $("#DDCategoria").val();
    var jsIdCuenta = "";
    var jsIdCliente = "";
    var jsIdGrupo = "";

    estadoBtnConsultar();

    if (target == "#tabCuenta") {
        jsIdCuenta = miInformacionCuenta == null ? "" : miInformacionCuenta.IdCuenta;
        if (!miInformacionCuenta && lista == "#ListaPorCuenta") {
            if (!CargaInicial) {
                $(lista).jqGrid('clearGridData');
                graficoPorCuenta.series[0].setData([]);
            }
            return;
        }
    }
    if (target == "#tabCliente") {
        jsIdCliente = miInformacionCliente == null ? "" : miInformacionCliente.IdCliente;
        if (!miInformacionCliente && lista == "#ListaPorCliente") {
            if (!CargaInicial) {
                $(lista).jqGrid('clearGridData');
                graficoPorCliente.series[0].setData([]);
            }
            return;
        }
    }
    if (target == "#tabGrupo") {
        jsIdGrupo = miInformacionGrupo == null ? "" : miInformacionGrupo.IdGrupo;
        if (!miInformacionGrupo && lista == "#ListaPorGrupo") {
            if (!CargaInicial) {
                $(lista).jqGrid('clearGridData');
                graficoPorGrupo.series[0].setData([]);
            }
            return;
        }
    }
    if (jsfechaConsulta === "") {
        if (!CargaInicial) {
            $(lista).jqGrid('clearGridData');
            graficoPorGrupo.series[0].setData([]);
            alert("No ha indicado fecha de consulta");
        }
        return;
    }
    if (jsCategoria == -1) {
        if (!CargaInicial) {
            $(lista).jqGrid('clearGridData');
            graficoPorGrupo.series[0].setData([]);
        }
        return;
    }
    history.pushState(null, null, '?FechaConsulta=' + jsfechaConsulta + '&AgrupadoPor=' + lista);
    var listaAsync = lista;
    $(listaAsync).jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    var jsfechaFormato = moment.utc($(".input-group.date").datepicker('getDate')).startOf('day').toISOString();
    $.ajax({
        url: "CarteraPorCategoria.aspx/ConsultaCarteraPorCategoria",
        data: "{'dfechaConsulta':'" + jsfechaFormato.substring(0, jsfechaFormato.length - 1) +
        "', 'idCuenta':'" + jsIdCuenta +
        "', 'idCliente':'" + jsIdCliente +
        "', 'idGrupo':'" + jsIdGrupo +
        "', 'categoria':'" + (jsCategoria == -1 ? null : jsCategoria) +
        "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $(listaAsync).setGridParam({ data: mydata });
                $(listaAsync).trigger("reloadGrid");
                creaLinksLista(listaAsync);
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
                var datatestt = [];
                if (mydata.d !== null) {
                    for (var i = 0; i < mydata.length; i++) {
                        datatestt[i] = [mydata[i].DSC_CATEGORIA, mydata[i].PORC_CATEGORIA];
                    }
                }

                if (target == "#tabCuenta") {
                    graficoPorCuenta.series[0].setData(datatestt);
                    graficoPorCuenta.title.text = "Cartera Por " + $("#DDCategoria option:selected").text();
                } if (target == "#tabCliente") {
                    graficoPorCliente.series[0].setData(datatestt);
                    graficoPorCliente.title.text = "Cartera Por " + $("#DDCategoria option:selected").text();
                } if (target == "#tabGrupo") {
                    graficoPorGrupo.series[0].setData(datatestt);
                    graficoPorGrupo.title.text = "Cartera Por " + $("#DDCategoria option:selected").text();
                }
                $(window).trigger('resize');
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}

$(document).ready(function () {
    initCarteraPorCategoria();
    cargarEventHandlersCarteraPorCategoria();
    consultaCarteraPorCategoria(true);
    resize();
});