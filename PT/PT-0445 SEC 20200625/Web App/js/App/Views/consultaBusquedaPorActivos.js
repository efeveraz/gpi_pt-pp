﻿var jsfechaFormato;
var jsfechaFormatoHasta;
var vColModel = [];
var vColNames = [];

function resize() {
    var height;
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#Paginador").outerHeight(true) - 180;
    $('#ListaBusAct').jqGrid('setGridHeight', height);
}

function initConsultaBusquedaPorActivos() {
    $("#idSubtituloPaginaText").text("Consulta Búsqueda por Activos");
    document.title = "Consulta Búsqueda por Activos";
    $("#DDNemotecnico").prop("disabled", true);

    consultaInstrumentos();
    estadoBtnConsultar();

    if (!miInformacionCuenta) {
        $(".input-group.date").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
    } else {
        $(".input-group.date").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
    }

    jsfechaConsulta = document.getElementById("dtFechaConsulta").value;

    creaColumnasOrden("INICIAL");
    vColNames = ["Cliente", "Rut", "Cuenta", "Asesor", "Cantidad", "Precio Compra", "Precio Mercado", "Valor Mercado", "Código"];
}

function eventHandlersConsultaBusquedaPorActivos() {
    $(".input-group.date").datepicker();

    $(window).bind('resize', function () {
        resize()
    }).trigger('resize');

    $("#DDNemotecnico").select2({
        theme: "bootstrap",
        maximumSelectionLength: 2
    });

    $("#DDInstrumento").change(function () {
        var idInstrumento = $("#DDInstrumento").val();
        $("#DDNemotecnico").empty();
        consultaNemotecnicos();

        if (idInstrumento == -1) {
            $("#DDNemotecnico").prop("disabled", true);
        } else {
            $("#DDNemotecnico").prop("disabled", false);
        }
    });

    $("#DDNemotecnico").change(function () {
        consultaCodigoInst();
    });

    $("#BtnConsultar").on('click', function () {
        $('#ListaBusAct').jqGrid('clearGridData');
        consultaBusquedaPorActivos();
        resize();
    });

    $("#DDNemotecnico").on('change', function () {
        estadoBtnConsultar();
    });

    $("#ListaBusAct").jqGrid({
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records" },
        colNames: vColNames,
        colModel: vColModel,
        pager: '#Paginador',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        height: "50%",
        styleUI: 'Bootstrap',
        sortname: "CLIENTE",
        sortorder: "asc",
        caption: "Búsqueda por Activos",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: false,
        footerrow: true,
        loadComplete: function () {
            var $self = $(this),
                sum = $self.jqGrid("getCol", "CANTIDAD", false, "sum");
            $self.jqGrid("footerData", "set", { DSC_ASESOR: "Total:", CANTIDAD: sum });

            var $self2 = $(this),
                sum2 = $self2.jqGrid("getCol", "MONTO_MON_CTA", false, "sum");
            $self2.jqGrid("footerData", "set", { MONTO_MON_CTA: sum2 });
        },
        groupingView: {
            groupField: ['DSC_NEMOTECNICO'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupColumnShow: [false],
            groupSummary: [true],
            showSummaryOnHide: true
        }
    });
    $("#ListaBusAct").jqGrid().bindKeys().scrollingRows = true
    $("#ListaBusAct").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaBusAct").jqGrid('navGrid', '#Paginador', { add: false, edit: false, del: false, excel: false, search: false });
    $("#ListaBusAct").jqGrid('navButtonAdd', '#Paginador', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaBusAct', 'Consulta Búsqueda por Activos', 'Consulta Búsqueda por Activos', '');
        }
    });
}

function consultaInstrumentos() {
    $.ajax({
        url: "../Servicios/Globales.asmx/consultaInstrumentos",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                $("#DDInstrumento").empty();
                $("#DDInstrumento").prepend('<option value="' + -1 + '">' + 'Todos los Instrumentos' + '</option>');
                $.each(res, function (key, val) {
                    $("#DDInstrumento").append('<option value="' + val.COD_INSTRUMENTO + '">' + val.DSC_INTRUMENTO + '</option>');
                });
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}
function consultaNemotecnicos() {
    jsfechaFormato = moment.utc($("#dtFechaConsulta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1);
    var codInstrumento = $("#DDInstrumento").val();

    if (codInstrumento == "-1") {
        return;
    }
    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta");
        return;
    }

    $('#loaderProceso').modal();
    $.ajax({
        url: "ConsultaBusquedaPorActivos.aspx/consultaNemotecnicos",
        data: "{'codInstrumento':'" + codInstrumento +
        "', 'fechaConsulta':'" + jsfechaConsulta + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                $("#DDNemotecnico").empty();
                $("#DDNemotecnico").prepend('<option value="' + -1 + '">' + 'Seleccione Nemotécnicos' + '</option>');
                $.each(res, function (key, val) {
                    $("#DDNemotecnico").append('<option value="' + val.ID_NEMOTECNICO + '">' + val.NEMOTECNICO + '</option>');
                });
                estadoBtnConsultar();
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}

function consultaCodigoInst() {
    var vIdNemotecnico = $("#DDNemotecnico").val();
    $.ajax({
        url: "ConsultaBusquedaPorActivos.aspx/consultaCodigoInstrumento",
        data: "{'idNemotecnico':" + vIdNemotecnico + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;

                creaColumnasOrden(res[0]);
                habilitaColumnas(res[0]);
                //estadoBtnConsultar();
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function creaColumnasOrden(codInt) {

    if (codInt == "RV" || codInt == "RVINT" || codInt == "RFINT" || codInt == "FM_INT" || codInt == "RVN" || codInt == "RVI" || codInt == "FFMM" || codInt == "CFIX" || codInt == "INICIAL") {
        vColModel = [
            { name: "NOMBRE_CLIENTE", index: "NOMBRE_CLIENTE", sortable: true, width: 280, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "RUT_CLIENTE", index: "RUT_CLIENTE", sortable: true, width: 90, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 80, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "DSC_ASESOR", index: "DSC_ASESOR", sortable: true, width: 180, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "CANTIDAD", index: "CANTIDAD", sortable: true, width: 120, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] }, summaryType: "sum" },
            { name: "PRECIO_COMPRA", index: "PRECIO_COMPRA", sortable: true, width: 120, sorttype: "number", formatoptions: { decimalPlaces: 4 }, editable: false, formatter: "number", search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "PRECIO", index: "PRECIO", sortable: true, width: 120, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_MON_CTA", index: "MONTO_MON_CTA", sortable: true, width: 140, sorttype: "currency", editable: false, formatter: "currency", search: true, align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] }, summaryType: "sum" },
            { name: "CODIGO", index: "CODIGO", sortable: true, width: 70, sorttype: "text", editable: false, search: true, align: "center", hidden: true }
        ];
    }
    if (codInt == "RF" || codInt == "RFN" || codInt == "RFI") {
        vColModel = [
            { name: "NOMBRE_CLIENTE", index: "NOMBRE_CLIENTE", sortable: true, width: 280, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "RUT_CLIENTE", index: "RUT_CLIENTE", sortable: true, width: 90, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 80, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "DSC_ASESOR", index: "DSC_ASESOR", sortable: true, width: 180, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "CANTIDAD", index: "CANTIDAD", sortable: true, width: 120, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] }, summaryType: "sum" },
            { name: "TASA_COMPRA", index: "TASA_COMPRA", sortable: true, width: 120, sorttype: "number", editable: false, search: true, formatter: "number", align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "TASA_MERCADO", index: "TASA_MERCADO", sortable: true, width: 120, sorttype: "number", editable: false, search: true, formatter: "number", align: "right", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_MON_CTA", index: "MONTO_MON_CTA", sortable: true, width: 140, sorttype: "number", editable: false, formatter: "number", search: true, align: "right", summaryType: "sum", hidden: false, searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "CODIGO", index: "CODIGO", sortable: true, width: 70, sorttype: "text", editable: false, search: true, align: "center", hidden: true }
        ];
    }
}

function habilitaColumnas(codInt) {
    if (codInt == "RV" || codInt == "RVN" || codInt == "RVI" || codInt == "RVINT" || codInt == "RFINT" || codInt == "FM_INT") {
        cambioNameColumn("Precio Compra", "Precio Mercado", "Valor Mercado")
    }
    if (codInt == "RF" || codInt == "RFN" || codInt == "RFI") {
        cambioNameColumn("Tasa Compra", "Tasa Mercado", "Valor Mercado")
    }
    if (codInt == "FFMM" || codInt == "CFI") {
        cambioNameColumn("Valor Cta. Promedio Compra", "Valor Cuota Actual", "Valor Mercado")
    }
}

function cambioNameColumn(nomCompra, nomValor, nomVMercado) {
    $("#ListaBusAct").setLabel("PRECIO_COMPRA", nomCompra);
    $("#ListaBusAct").setLabel("PRECIO", nomValor);
    $("#ListaBusAct").setLabel("MONTO_MON_CTA", nomVMercado);
    $("#ListaBusAct").setLabel("TASA_COMPRA", nomCompra);
    $("#ListaBusAct").setLabel("TASA_MERCADO", nomValor);
    $("#ListaBusAct").setLabel("MONTO_MON_CTA", nomVMercado);
}

function estadoBtnConsultar() {
    var idNemotecnico = $("#DDNemotecnico").val();

    if (idNemotecnico !== "-1") {
        console.log('prueba')
        $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
    } else {
        $('#BtnConsultar').tooltip('enable').addClass('disabled');
    }
}
function consultaBusquedaPorActivos() {
    jsfechaFormato = moment.utc($("#dtFechaConsulta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1);
    var idNemotecnico = $("#DDNemotecnico").val();

    if (idNemotecnico == "-1") {
        return;
    }
    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta");
        return;
    }

    $("#ListaBusAct").jqGrid('clearGridData');
    $('#loaderProceso').modal();
    $.ajax({
        url: "ConsultaBusquedaPorActivos.aspx/consultaBusquedaPorActivos",
        data: "{'idNemotecnico':" + idNemotecnico + ", 'fechaConsulta':'" + jsfechaConsulta + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var mydata = JSON.parse(jsondata.responseText).d;

                $("#ListaBusAct").setGridParam({ data: mydata });
                $("#ListaBusAct").trigger("reloadGrid");

                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros");
                }
            }
            else {
                alert(JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}

$(document).ready(function () {
    initConsultaBusquedaPorActivos();
    eventHandlersConsultaBusquedaPorActivos();
    resize();
});