﻿var urlVarMercado = getUrlVars()["Mercado"];
var urlVarMoneda = getUrlVars()["Moneda"];
var urlVarSector = getUrlVars()["Sector"];
var urlVarAgrupadoPor = getUrlVars()["AgrupadoPor"];
var urlVarFechaConsulta = getUrlVars()["FechaConsulta"];
var urlDescripcion = "";
var target = "#tabCuenta";
var jsfechaConsulta;
var jsfechaFormato;

var lista = urlVarAgrupadoPor == null ? "#ListaPorCuenta" : urlVarAgrupadoPor;

var ini;
var urlDescripcionMercado;
var urlDescripcionMoneda;
var urlDescripcionSector;

function resize() {
    var height;
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#paginadorPorCuenta").outerHeight(true) - $("#paginadorPorCliente").outerHeight(true) - $("#paginadorPorGrupo").outerHeight(true) - 84 - 37 - 22;

    if (ancho < 768) {
        $('#ListaPorCuenta').setGridHeight(220);
        $('#ListaPorCliente').setGridHeight(220);
        $('#ListaPorGrupo').setGridHeight(220);
    } else {
        $('#ListaPorCuenta').jqGrid('setGridHeight', height);
        $('#ListaPorCliente').jqGrid('setGridHeight', height);
        $('#ListaPorGrupo').jqGrid('setGridHeight', height);
    }

    $("#ListaPorCuenta").setGridWidth($("#grillaPorCuenta").width() - 2);
    $("#ListaPorCliente").setGridWidth($("#grillaPorCliente").width() - 2);
    $("#ListaPorGrupo").setGridWidth($("#grillaPorGrupo").width() - 2);
}


function initCarteraArbolActivos() {
    $("#idSubtituloPaginaText").text("Árbol de Activos");
    document.title = "Árbol de Activos";

    if (urlVarFechaConsulta) {
        $(".input-group.date").datepicker('setDate', urlVarFechaConsulta);
    } else {
        if (!miInformacionCuenta) {
            $(".input-group.date").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
            jsfechaConsulta = moment($(".input-group.date").datepicker('getDate')).format("L");
        } else {
            $(".input-group.date").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
        }
        jsfechaConsulta = moment($(".input-group.date").datepicker('getDate')).format("L");
        urlVarFechaConsulta = jsfechaConsulta;
    }

    if (urlVarMercado) {
        urlDescripcionMercado = getUrlVars()["DescripcionMercado"];
        $("#idSubtituloPaginaText").text("Arbol de Activos por Mercado: " + urlDescripcionMercado);
        document.title = "Arbol de Activos por Mercado: " + urlDescripcionMercado;
    } else {
        urlVarMercado = "";
    }
    if (urlVarMoneda) {
        urlDescripcionMoneda = getUrlVars()["DescripcionMoneda"];
        $("#idSubtituloPaginaText").text("Arbol de Activos por Moneda: " + urlDescripcionMoneda);
        document.title = "Arbol de Activos por Moneda: " + urlDescripcionMoneda;
    } else {
        urlVarMoneda = "";
    }
    if (urlVarSector) {
        urlDescripcionSector = getUrlVars()["DescripcionSector"];
        $("#idSubtituloPaginaText").text("Arbol de Activos por Sector: " + urlDescripcionSector);
        document.title = "Arbol de Activos por Sector: " + urlDescripcionSector;
    } else {
        urlVarSector = "";
    }

    ini = urlVarMercado + urlVarMoneda + urlVarSector

    if (urlVarAgrupadoPor == "#ListaPorCuenta") {
        $("#contenidoPorCuenta").show();
        $("#contenidoPorCliente").hide();
        $("#contenidoPorGrupo").hide();
        target = "#tabCuenta";
        $('#BtnConsultar').attr('data-original-title', 'Seleccione Cuenta');
        if (ini) {
            $("#tabsClienteCuentaGrupo").hide();
        }
        $('.nav-tabs a[href="#tabCuenta"]').tab('show');
    } else if (urlVarAgrupadoPor == "#ListaPorCliente") {
        $("#contenidoPorCuenta").hide();
        $("#contenidoPorCliente").show();
        $("#contenidoPorGrupo").hide();
        target = "#tabCliente";
        $('#BtnConsultar').attr('data-original-title', 'Seleccione Cliente');
        if (ini) {
            $("#tabsClienteCuentaGrupo").hide();
        }
        $('.nav-tabs a[href="#tabCliente"]').tab('show');
    } else if (urlVarAgrupadoPor == "#ListaPorGrupo") {
        $("#contenidoPorCuenta").hide();
        $("#contenidoPorCliente").hide();
        $("#contenidoPorGrupo").show();
        target = "#tabGrupo"
        $('#BtnConsultar').attr('data-original-title', 'Seleccione Grupo');
        if (ini) {
            $("#tabsClienteCuentaGrupo").hide();
        }
        $('.nav-tabs a[href="#tabGrupo"]').tab('show');
    } else {
        $("#contenidoPorCuenta").show();
        $("#contenidoPorCliente").hide();
        $("#contenidoPorGrupo").hide();
    }
}

function cargarEventHandlersCarteraArbolActivos() {

    $(".input-group.date").datepicker();

    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        target = $(e.target).attr("href") // activated tab
        if (target == "#tabCuenta") {
            $("#contenidoPorCuenta").show();
            $("#contenidoPorCliente").hide();
            $("#contenidoPorGrupo").hide();
            lista = "#ListaPorCuenta";
            $('#BtnConsultar').attr('data-original-title', 'Seleccione Cuenta');
        }
        if (target == "#tabCliente") {
            $("#contenidoPorCuenta").hide();
            $("#contenidoPorCliente").show();
            $("#contenidoPorGrupo").hide();
            lista = "#ListaPorCliente";
            $('#BtnConsultar').attr('data-original-title', 'Seleccione Cliente');
        }
        if (target == "#tabGrupo") {
            $("#contenidoPorCuenta").hide();
            $("#contenidoPorCliente").hide();
            $("#contenidoPorGrupo").show();
            lista = "#ListaPorGrupo";
            $('#BtnConsultar').attr('data-original-title', 'Seleccione Grupo');
        }
        if (!ini) {
            history.pushState(null, null, '?FechaConsulta=' + jsfechaConsulta + '&AgrupadoPor=' + lista);
        }
        $(window).trigger('resize');
        consultaCartera(true);
    });

    var grilla = {
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "NumeroRegistro" },
        colNames: ['ID_ARBOL_CLASE_INST', 'Codigo', 'Descripción', 'Porcentaje', 'Monto', 'Nivel', 'NivelMax'],
        colModel: [{ name: "ID_ARBOL_CLASE_INST", index: "ID_ARBOL_CLASE_INST", width: 80, sortable: false, sorttype: "int", editable: false, search: true, hidden: true },
        { name: "CODIGO", index: "Codigo", width: 70, sortable: false, sorttype: "int", editable: false, search: true, hidden: true },
        { name: "DSC_ARBOL_CLASE_INST", index: "DSC_ARBOL_CLASE_INST", sortable: false, width: 70, sorttype: "text", editable: false, search: true },
        { name: "PORCENTAJE", index: "PORCENTAJE", sortable: false, width: 70, sorttype: "number", formatter: 'number', formatoptions: { suffix: '%' }, editable: false, search: true, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "MONTO", index: "MONTO", width: 110, sortable: false, sorttype: "number", formatter: 'number', editable: false, search: true, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
        { name: "NIVEL", index: "NIVEL", width: 80, sortable: false, sorttype: "int", editable: false, search: true, summaryType: 'sum', hidden: true },
        { name: "NIVEL_MAX", index: "NIVEL_MAX", width: 80, sortable: false, sorttype: "int", editable: false, search: true, summaryType: 'sum', hidden: true }
        ],
        pager: '#Paginador',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        width: 600,
        height: 800,
        styleUI: 'Bootstrap',
        caption: "Total Patrimonio",
        grouping: false,
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        groupingView: {
            groupField: ['Codigo'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        },
        gridComplete: function () {
            $(lista).trigger('reloadGrid');
            formatoArbol();
        }
    }

    grilla.pager = "#paginadorPorCuenta";
    grilla.caption = "Patrimonio Por Cuenta";
    $("#ListaPorCuenta").jqGrid(grilla);
    grilla.pager = "#paginadorPorCliente";
    grilla.caption = "Patrimonio Por Cliente";
    $("#ListaPorCliente").jqGrid(grilla);
    grilla.pager = "#paginadorPorGrupo";
    grilla.caption = "Patrimonio Por Grupo";
    $("#ListaPorGrupo").jqGrid(grilla);

    $("#ListaPorCuenta").jqGrid().bindKeys().scrollingRows = true
    $("#ListaPorCuenta").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaPorCuenta").jqGrid('navGrid', '#paginadorPorCuenta', { add: false, edit: false, del: false, excel: true, search: false });
    $("#ListaPorCuenta").jqGrid('navButtonAdd', '#paginadorPorCuenta', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaPorCuenta', 'ListaPorCuenta', 'List. Por Cuenta', '');
        }
    });

    $("#ListaPorCliente").jqGrid().bindKeys().scrollingRows = true
    $("#ListaPorCliente").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaPorCliente").jqGrid('navGrid', '#paginadorPorCliente', { add: false, edit: false, del: false, excel: true, search: false });
    $("#ListaPorCliente").jqGrid('navButtonAdd', '#paginadorPorCliente', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaPorCliente', 'ListaPorCliente', 'List. Por Cliente', '');
        }
    });

    $("#ListaPorGrupo").jqGrid().bindKeys().scrollingRows = true
    $("#ListaPorGrupo").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaPorGrupo").jqGrid('navGrid', '#paginadorPorGrupo', { add: false, edit: false, del: false, excel: true, search: false });
    $("#ListaPorGrupo").jqGrid('navButtonAdd', '#paginadorPorGrupo', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaPorGrupo', 'ListaPorGrupo', 'List. Por Grupo', '');
        }
    });

    $("#BtnConsultar").on('click', function () {
        consultaCartera(false);
    });

    $("#porCuenta_NroCuenta").on('change', function () {
        estadoBtnConsultar();
    });
}

function formatoArbol() {
    var ids = $(lista).jqGrid("getDataIDs"),
        l = ids.length,
        i,
        rowid;
    var rowid = "";
    var base = 20;
    var indent = 0;
    for (i = 0; i < l; i++) {
        rowid = ids[i];
        nivel = $(lista).jqGrid("getCell", rowid, "NIVEL");
        nivelMax = $(lista).jqGrid("getCell", rowid, "NIVEL_MAX");
        indent = base * nivel;
        $(lista + '> > #' + $.jgrid.jqID(rowid)).css('text-indent', indent + 'px');
        if (nivel == 0) {
            $(lista + '> > #' + $.jgrid.jqID(rowid)).addClass('TotalPatrimonio');
        } else {
            if (nivel == 1) {
                $(lista + '> > #' + $.jgrid.jqID(rowid)).addClass('FilaGruposJqgrid');
            } else {
                if (nivel < nivelMax) {
                    $(lista + '> > #' + $.jgrid.jqID(rowid)).addClass('FilaBaseJqgrid');
                }
            }
        }
    }
}
function formatoLista() {
    var ids = $(lista).jqGrid("getDataIDs"), l = ids.length, i, rowid, status;
    var rowid = "";
    var monto = "0";
    var codigo = "";
    var Descripcion = "";
    var tipo = ""
    for (i = 0; i < l; i++) {
        rowid = ids[i];
        nivel = $(lista).jqGrid("getCell", rowid, "NIVEL");
        nivelMax = $(lista).jqGrid("getCell", rowid, "NIVELMAX");
        if (nivel > 1 && nivelMax <= nivel) {
            monto = $(lista).jqGrid("getCell", rowid, "MONTO");
            tipo = $(lista).jqGrid("getCell", rowid, "CODIGO");
            codigo = $(lista).jqGrid("getCell", rowid, "ID_ARBOL_CLASE_INST");
            Descripcion = $(lista).jqGrid("getCell", rowid, "DSC_ARBOL_CLASE_INST");
            if (monto > 0) {
                urlVarFechaConsulta = document.getElementById("dtFechaConsulta").value
                var href = "";
                if (urlVarMercado != "") {
                    href = "CarteraDetalleInstrumento.aspx?tipo=" + tipo + "&Codigo=" + codigo + "&Descripcion=" + Descripcion + "&Mercado=" + urlVarMercado + "&DescripcionMercado=" + urlDescripcionMercado + "&FechaConsulta=" + urlVarFechaConsulta + "&AgrupadoPor=" + lista
                } else if (urlVarMoneda != "") {
                    href = "CarteraDetalleInstrumento.aspx?tipo=" + tipo + "&Codigo=" + codigo + "&Descripcion=" + Descripcion + "&Moneda=" + urlVarMoneda + "&DescripcionMoneda=" + urlDescripcionMoneda + "&FechaConsulta=" + urlVarFechaConsulta + "&AgrupadoPor=" + lista
                } else if (urlVarSector != "") {
                    href = "CarteraDetalleInstrumento.aspx?tipo=" + tipo + "&Codigo=" + codigo + "&Descripcion=" + Descripcion + "&Sector=" + urlVarSector + "&DescripcionSector=" + urlDescripcionSector + "&FechaConsulta=" + urlVarFechaConsulta + "&AgrupadoPor=" + lista
                } else {
                    href = "CarteraDetalleInstrumento.aspx?tipo=" + tipo + "&Codigo=" + codigo + "&Descripcion=" + Descripcion + "&FechaConsulta=" + urlVarFechaConsulta + "&AgrupadoPor=" + lista
                }
                $(lista).jqGrid('setCell', rowid, 'DSC_ARBOL_CLASE_INST', "<a href='" + href + "'>" + Descripcion + "</a>");
                $(lista).jqGrid('getLocalRow', rowid).Descripcion = "<a href='" + href + "'>" + Descripcion + "</a>";
            }
        }
    }
}
function consultaCartera(CargaInicial) {
    jsfechaConsulta = moment($(".input-group.date").datepicker('getDate')).format("L");
    var jsIdCuenta = "";
    var jsIdCliente = "";
    var jsIdGrupo = "";
    estadoBtnConsultar();
    if (target == "#tabCuenta") {
        jsIdCuenta = miInformacionCuenta == null ? "" : miInformacionCuenta.IdCuenta;
        if (!miInformacionCuenta && lista == "#ListaPorCuenta") {
            if (!CargaInicial) {
                $(lista).jqGrid('clearGridData');
            }
            return;
        }
    }
    if (target == "#tabCliente") {
        jsIdCliente = miInformacionCliente == null ? "" : miInformacionCliente.IdCliente;
        if (!miInformacionCliente && lista == "#ListaPorCliente") {
            if (!CargaInicial) {
                $(lista).jqGrid('clearGridData');
            }
            return;
        }
    }
    if (target == "#tabGrupo") {
        jsIdGrupo = miInformacionGrupo == null ? "" : miInformacionGrupo.IdGrupo;
        if (!miInformacionGrupo && lista == "#ListaPorGrupo") {
            if (!CargaInicial) {
                $(lista).jqGrid('clearGridData');
            }
            return;
        }
    }
    if (jsfechaConsulta === "") {
        if (!CargaInicial) {
            $(lista).jqGrid('clearGridData');
            alert("No ha indicado fecha de consulta");
        }
        return;
    }
    $(lista).jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    jsfechaFormato = moment.utc($(".input-group.date").datepicker('getDate')).startOf('day').toISOString();
    $.ajax({
        url: "CarteraArbolActivos.aspx/ConsultaCartera",
        data: "{'dfechaConsulta':'" + jsfechaFormato.substring(0, jsfechaFormato.length - 1) +
        "', 'idCuenta':'" + jsIdCuenta +
        "', 'idCliente':'" + jsIdCliente +
        "', 'idGrupo':'" + jsIdGrupo +
        "', 'intMercado':'" + urlVarMercado +
        "', 'intMoneda':'" + urlVarMoneda +
        "', 'intSector':'" + urlVarSector + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $(lista).setGridParam({ data: mydata });
                $(lista).trigger("reloadGrid");
                formatoLista();
                if (!ini) {
                    history.pushState(null, null, '?FechaConsulta=' + jsfechaConsulta + '&AgrupadoPor=' + lista);
                }
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
            $(window).trigger('resize');
        }
    });
}
function estadoBtnConsultar() {

    if (target == "#tabCuenta") {
        if (miInformacionCuenta == null) {
            $('#BtnConsultar').tooltip('enable').addClass('disabled');
        } else {
            $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
        }
    }
    if (target == "#tabCliente") {
        if (miInformacionCliente == null) {
            $('#BtnConsultar').tooltip('enable').addClass('disabled');
        } else {
            $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
        }
    }
    if (target == "#tabGrupo") {
        if (miInformacionGrupo == null) {
            $('#BtnConsultar').tooltip('enable').addClass('disabled');
        } else {
            $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
        }
    }
}

$(document).ready(function () {
    initCarteraArbolActivos();
    cargarEventHandlersCarteraArbolActivos();

    consultaCartera(true);
    resize();
});