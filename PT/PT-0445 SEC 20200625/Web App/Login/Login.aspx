﻿<%@ Page Language="vb" MasterPageFile="~/Login/Login.Master" AutoEventWireup="false"
    CodeBehind="Login.aspx.vb" Inherits="AplicacionWeb.Login1" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/Login/Login.Master" %>
<asp:Content ID="LoginUsuario" ContentPlaceHolderID="miLogin" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            localStorage.clear();
            var empresas
            function cargaEmpresas() {
                $.ajax({
                    url: "../Servicios/Empresas.asmx/ConsultaNombreEmpresas",
                    data: "{}",
                    datatype: "json",
                    type: "post",
                    contentType: "application/json; charset=utf-8",
                    complete: function (jsondata, stat) {
                        if (stat == "success") {
                            empresas = JSON.parse(jsondata.responseText).d;
                            $("#DDEmpresa").html('');
                            $.each(empresas, function (key, val) {
                                $("#DDEmpresa").append('<option value="' + val.ID_EMPRESA + '">' + val.DSC_EMPRESA + '</option>');
                            })
                            cambioColor();
                        }
                        else {
                            alert(JSON.parse(jsondata.responseText).Message);
                        }
                        $('.loading').hide();
                    }
                });
            };

            function validarCredenciales() {
                $.ajax({
                    url: "../Servicios/Login.asmx/ValidarCredenciales",
                    data: "{'usuario':'" + $("#txtNombreUsuario").val() +
                        "', 'contraseña':'" + $("#txtContraseña").val() +
                        "', 'empresa':" + $("#DDEmpresa").val() + "}",
                    datatype: "json",
                    type: "post",
                    contentType: "application/json; charset=utf-8",
                    complete: function (jsondata, stat) {
                        if (stat == "success") {
                            var mensaje = JSON.parse(jsondata.responseText).d;
                            if (mensaje == "") {
                                localStorage.setItem("sesionCerrada", "False");
                                window.location.href = '../Sistema/PreLoader.aspx';
                            } else {
                                $("#mensaje").text(mensaje);
                                $("#alert").fadeIn('slow');
                            }
                        }
                        else {
                            alert(JSON.parse(jsondata.responseText).Message);
                        }
                        $('.loading').hide();
                    }
                });
            };

            function cambioColor () {
                var model = empresas;
                var empresa = $.grep(model, function(e){ return e.ID_EMPRESA == $('#DDEmpresa').val(); });
                $('#PanelSubtitulo').css('background-color', 'rgb(' + empresa[0].COLOR_EMPRESA + ')' );
            }

            $('#DDEmpresa').change(function () {
                cambioColor();
            });

            $("#logIn").on('click', function () {
                validarCredenciales();
            })

            $(document).on('click', function (event) {
                if (!$(event.target).closest('#alert').length) {
                    $("#alert").hide();
                }
            });

            $('#txtContraseña').keypress(function (e) {
                var key = e.which;
                if (key == 13)  // the enter key code
                {
                    validarCredenciales();
                }
            });

            $("[data-hide]").on("click", function () {
                $(this).closest("." + $(this).attr("data-hide")).hide();
            });

            cargaEmpresas();
            
        });

    </script>
    <div id="idLoginUsuario" >
        <div style="text-align: center; height: 25vh;">
            <h3 style="margin: auto; color: #A6A9B2; font-weight: bold; display: table-cell; vertical-align: middle; height: inherit; width: 100vw;">Gestor de Portafolios de Inversión</h3>
        </div>
        <div class="col-xs-12 col-sm-offset-3 col-sm-6 col-lg-offset-4 col-lg-4">
            <div class="panel panel-default">
                <div id="PanelSubtitulo" runat="server" clientidmode="Static" class="panel-heading" style="font-size: 12pt; color: #FFFFFF; text-align: center; font-weight: bold;">
                    Ingreso al Sistema
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="txtNombreUsuario" class="col-sm-4 control-label">Usuario</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="txtNombreUsuario" runat="server" clientidmode="Static" maxlength="50" placeholder="usuario">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtContraseña" class="col-sm-4 control-label">Contraseña</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" id="txtContraseña" runat="server" clientidmode="Static" maxlength="50" placeholder="contraseña">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="DDEmpresa" class="col-sm-4 control-label">Empresa</label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="DDEmpresa" runat="server" CssClass="form-control" ClientIDMode="Static">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 0;">
                            <div class="col-sm-offset-4 col-sm-8">
                                <a href="#" type="button" id="logIn" class="btn btn-default">Iniciar Sesión</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="alert" class="alert alert-warning" role="alert" style="position: fixed; bottom: 5vh; right:5vw; width: 90vw; max-width: 450px; display: none;">
                <button type="button" class="close" data-hide="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Advertencia! </strong><div id="mensaje"></div>
            </div>
        </div>
    </div>
</asp:Content>
