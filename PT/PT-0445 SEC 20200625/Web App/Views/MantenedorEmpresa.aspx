﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Sistema/GPIWeb.Master"
    CodeBehind="MantenedorEmpresa.aspx.vb" Inherits="AplicacionWeb.Configuracion" %>

<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContenidoPPalFiltro" runat="server">
    <link href="../Estilos/vendor/ColorPicker/colpick.css" rel="stylesheet" type="text/css" />
    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }

            .btn-file input[type=file] {
                position: absolute;
                top: 0;
                right: 0;
                min-width: 100%;
                min-height: 100%;
                font-size: 100px;
                text-align: right;
                filter: alpha(opacity=0);
                opacity: 0;
                background: red;
                cursor: inherit;
                display: block;
            }

        input[readonly] {
            background-color: white !important;
            cursor: text !important;
        }
    </style>
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/ColorPicker/colpick.js") %>' type="text/javascript"></script>
    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/mantenedorEmpresa.min.js") %>'></script>

    <div class="navbar navbar-default">
        <div class="col-xs-12">
            <ul class="nav nav-pills">
                <li>
                    <button type="button" id="BtnGuardar" class="btn btn-primary navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Guardar'><i class="fa fa-floppy-o"></i></button>
                </li>
            </ul>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">Apariencia</div>
        <div class="panel-body">
            <div class="row">
                <div class="form-horizontal">
                    <div class="col-xs-12 col-sm-6">
                        <label for="DDEmpresa" class="control-label">Empresa a Modificar</label>
                        <select id="DDEmpresa" class="form-control"></select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <hr />
                </div>
            </div>
            <div class="row">
                <div class="form-horizontal">
                    <div class="col-xs-12 col-sm-6" style="padding-bottom: 5px;">
                        <label for="LblColor" class="control-label">Color</label>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="button" class="picker btn btn-default">Seleccionar Color</button>
                            </span>
                            <asp:TextBox ID="LblColor" runat="server" ClientIDMode="Static" Text="" CssClass="form-control"></asp:TextBox>
                            <span class="input-group-addon"><i id="muestra" style="display: inline-block; width: 20px; height: 20px; vertical-align: text-top;"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6" style="padding-bottom: 5px;">
                        <label class="control-label">Logo</label>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <span class="btn btn-default btn-file">Seleccionar Logo
                                        <input id="FileUpload" type="file" accept="image/*">
                                </span>
                            </span>
                            <input type="text" class="form-control" readonly style="z-index: 0;">
                            <span class="input-group-addon">
                                <img id="logoPreview" src="#" style="height: 20px;" /></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="picsker">
    </div>
</asp:Content>
