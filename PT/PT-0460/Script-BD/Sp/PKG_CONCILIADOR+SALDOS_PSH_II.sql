IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_CONCILIADOR$SALDOS_PSH_II]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_CONCILIADOR$SALDOS_PSH_II]
GO

CREATE PROCEDURE [dbo].[PKG_CONCILIADOR$SALDOS_PSH_II]
( @PFECHA_CIERRE DATETIME
, @PID_CUENTA    NUMERIC = NULL
) AS
BEGIN
	SET NOCOUNT ON

	CREATE TABLE #TBLCONCILIA_RESULTADO (FECHA               DATETIME
                                       , ID_CLIENTE          NUMERIC
                                       , ID_CUENTA           NUMERIC
                                       , ALIAS               VARCHAR(100)
                                       , CODIGO_CUSIP        VARCHAR(100)
                                       , ID_NEMOTECNICO      NUMERIC
                                       , NEMOTECNICO         VARCHAR(100)
                                       , ID_SALDO_ACTIVO_INT NUMERIC
                                       , CUENTA_PSH          VARCHAR(10)
                                       , GPI_CANTIDAD        FLOAT
                                       , GPI_PRECIO_MERCADO  FLOAT
                                       , GPI_VALOR_MERCADO   FLOAT
                                       , CTP_CANTIDAD        FLOAT
                                       , CTP_PRECIO_MERCADO  FLOAT
                                       , CTP_VALOR_MERCADO   FLOAT
                                       , DIF_CANTIDAD        FLOAT
                                       , DIF_PRECIO_MERCADO  FLOAT
                                       , DIF_VALOR_MERCADO   FLOAT
                                       , ESTADO              VARCHAR(3)
                                        )

   DECLARE @LCURSOR$ID_SALDO_ACTIVO_INT   NUMERIC,
           @LCURSOR$FECHA_CIERRE          DATETIME,
           @LCURSOR$ID_CUENTA             NUMERIC,
           @LCURSOR$ALIAS                 VARCHAR(50),
           @LCURSOR$ID_CLIENTE            NUMERIC,
           @LCURSOR$CODIGO_CUSIP          VARCHAR(100),
           @LCURSOR$ID_NEMOTECNICO        NUMERIC,
           @LCURSOR$CANTIDAD              FLOAT,
           @LCURSOR$PRECIO_MERCADO        FLOAT,
           @LCURSOR$VALOR_MERCADO_MON_USD FLOAT,
           @LCONTRAPARTE                  VARCHAR(200)

	INSERT INTO #TBLCONCILIA_RESULTADO (FECHA,
                                        ID_CLIENTE,
                                        ID_CUENTA,
                                        ALIAS,
                                        CODIGO_CUSIP,
                                        ID_NEMOTECNICO,
                                        NEMOTECNICO,
                                        CUENTA_PSH,
                                        GPI_CANTIDAD,
                                        GPI_PRECIO_MERCADO,
                                        GPI_VALOR_MERCADO,
                                        CTP_CANTIDAD,
                                        CTP_PRECIO_MERCADO,
                                        CTP_VALOR_MERCADO,
                                        DIF_CANTIDAD,
                                        DIF_PRECIO_MERCADO,
                                        DIF_VALOR_MERCADO,
                                        ESTADO
								       )
	SELECT FECHA,
		   C.ID_CLIENTE,
		   CSI.ID_CUENTA,
		   CSI.RUT_CLIENTE,
		   CSI.CODIGO_CUSIP,
		   CSI.ID_NEMOTECNICO,
		   CSI.NEMOTECNICO,
		   CSI.CUENTA_PSH,
		   0,
		   0,
		   0,
		   ROUND(CSI.CANTIDAD,4),
		   ROUND(CSI.PRECIO_MERCADO,4),
		   ROUND(CSI.VALOR_MERCADO,2),
		   0,
		   0,
		   0,
		   'CTP'
	  FROM CONC_SALDOS_PSH CSI
	  LEFT OUTER JOIN CUENTAS C ON C.ID_CUENTA = CSI.ID_CUENTA
	 WHERE CSI.FECHA = @PFECHA_CIERRE
	   AND CSI.ID_CUENTA = ISNULL(@PID_CUENTA,CSI.ID_CUENTA)

	DECLARE LCURSOR CURSOR FOR
	SELECT SAI.ID_SALDO_ACTIVO_INT,
           SAI.FECHA_CIERRE,
           C.ID_CLIENTE,
           SAI.ID_CUENTA,
           A.VALOR,
           R.ISIM,
           SAI.ID_NEMOTECNICO,
           SAI.CANTIDAD,
           CONVERT(NUMERIC(28,4),ROUND(CONVERT(NUMERIC(28,8),SAI.PRECIO_MERCADO),4)),
           SAI.VALOR_MERCADO_MON_USD
      FROM SALDOS_ACTIVOS_INT SAI
      LEFT OUTER JOIN REL_NEMOTECNICO_ATRIBUTOS R ON R.ID_NEMOTECNICO = SAI.ID_NEMOTECNICO
      LEFT OUTER JOIN (SELECT ID_ENTIDAD, VALOR FROM VIEW_ALIAS
                        WHERE COD_ORIGEN = 'PERSHING'
                          AND TABLA      = 'CUENTAS' ) A ON ID_ENTIDAD = SAI.ID_CUENTA
         , VIEW_CUENTAS C
     WHERE SAI.ORIGEN            = 'PSH'
       AND SAI.FECHA_CIERRE      = @PFECHA_CIERRE
       AND SAI.ID_CUENTA         = ISNULL(@PID_CUENTA,SAI.ID_CUENTA)
       AND C.ID_CUENTA           = SAI.ID_CUENTA
       AND RTRIM(LTRIM(A.VALOR)) = RTRIM(LTRIM(C.RUT_CLIENTE)) + '/' + RTRIM(LTRIM(SAI.CUENTA_PSH))

    FOR READ ONLY OPEN LCURSOR
    FETCH NEXT FROM LCURSOR INTO @LCURSOR$ID_SALDO_ACTIVO_INT,
                                 @LCURSOR$FECHA_CIERRE,
                                 @LCURSOR$ID_CLIENTE,
                                 @LCURSOR$ID_CUENTA,
                                 @LCURSOR$ALIAS,
                                 @LCURSOR$CODIGO_CUSIP,
                                 @LCURSOR$ID_NEMOTECNICO,
                                 @LCURSOR$CANTIDAD,
                                 @LCURSOR$PRECIO_MERCADO,
                                 @LCURSOR$VALOR_MERCADO_MON_USD

    WHILE NOT(@@FETCH_STATUS = -1)
    BEGIN
		IF (SELECT COUNT(*) FROM #TBLCONCILIA_RESULTADO
             WHERE FECHA = @LCURSOR$FECHA_CIERRE
               AND ID_CUENTA = @LCURSOR$ID_CUENTA
               AND ID_NEMOTECNICO = @LCURSOR$ID_NEMOTECNICO
               AND RTRIM(LTRIM(ALIAS)) + '/' + RTRIM(LTRIM(CUENTA_PSH)) = @LCURSOR$ALIAS) <> 0
        BEGIN
			UPDATE #TBLCONCILIA_RESULTADO
               SET ID_SALDO_ACTIVO_INT      = @LCURSOR$ID_SALDO_ACTIVO_INT
                 , GPI_CANTIDAD             = @LCURSOR$CANTIDAD
                 , GPI_PRECIO_MERCADO       = @LCURSOR$PRECIO_MERCADO
                 , GPI_VALOR_MERCADO        = @LCURSOR$VALOR_MERCADO_MON_USD
                 , DIF_CANTIDAD             = (@LCURSOR$CANTIDAD-CTP_CANTIDAD)
                 , DIF_PRECIO_MERCADO       = (@LCURSOR$PRECIO_MERCADO-CTP_PRECIO_MERCADO)
                 , DIF_VALOR_MERCADO        = (@LCURSOR$VALOR_MERCADO_MON_USD-CTP_VALOR_MERCADO)
                 , ESTADO                   = 'VIG'
             WHERE FECHA                    = @LCURSOR$FECHA_CIERRE
               AND ID_CUENTA                = @LCURSOR$ID_CUENTA
               AND ID_NEMOTECNICO           = @LCURSOR$ID_NEMOTECNICO
               AND ESTADO                   = 'CTP'
               AND RTRIM(LTRIM(ALIAS))
                   + '/' +
                   RTRIM(LTRIM(CUENTA_PSH)) = @LCURSOR$ALIAS
        END
        ELSE
        BEGIN
			INSERT INTO #TBLCONCILIA_RESULTADO (FECHA,
                                                ID_CLIENTE,
                                                ID_CUENTA,
                                                ALIAS,
                                                CODIGO_CUSIP,
                                                ID_NEMOTECNICO,
                                                ID_SALDO_ACTIVO_INT,
                                                GPI_CANTIDAD,
                                                GPI_PRECIO_MERCADO,
                                                GPI_VALOR_MERCADO,
                                                ESTADO)
            SELECT @LCURSOR$FECHA_CIERRE,
                   @LCURSOR$ID_CLIENTE,
                   @LCURSOR$ID_CUENTA,
                   @LCURSOR$ALIAS,
                   @LCURSOR$CODIGO_CUSIP,
                   @LCURSOR$ID_NEMOTECNICO,
                   @LCURSOR$ID_SALDO_ACTIVO_INT,
                   @LCURSOR$CANTIDAD,
                   @LCURSOR$PRECIO_MERCADO,
                   @LCURSOR$VALOR_MERCADO_MON_USD,
                   'GPI'
        END

        FETCH NEXT FROM LCURSOR INTO @LCURSOR$ID_SALDO_ACTIVO_INT,
                                     @LCURSOR$FECHA_CIERRE,
                                     @LCURSOR$ID_CLIENTE,
                                     @LCURSOR$ID_CUENTA,
                                     @LCURSOR$ALIAS,
                                     @LCURSOR$CODIGO_CUSIP,
                                     @LCURSOR$ID_NEMOTECNICO,
                                     @LCURSOR$CANTIDAD,
                                     @LCURSOR$PRECIO_MERCADO,
                                     @LCURSOR$VALOR_MERCADO_MON_USD

    END
    CLOSE LCURSOR
    DEALLOCATE LCURSOR

    UPDATE #TBLCONCILIA_RESULTADO
       SET DIF_CANTIDAD       = (GPI_CANTIDAD-CTP_CANTIDAD)
         , DIF_PRECIO_MERCADO = (GPI_PRECIO_MERCADO-CTP_PRECIO_MERCADO)
         , DIF_VALOR_MERCADO  = (GPI_VALOR_MERCADO-CTP_VALOR_MERCADO)

    UPDATE #TBLCONCILIA_RESULTADO
       SET ESTADO = 'OK'
     WHERE DIF_CANTIDAD = 0
       AND DIF_PRECIO_MERCADO = 0
       AND DIF_VALOR_MERCADO = 0
       AND ESTADO = 'VIG'

    UPDATE #TBLCONCILIA_RESULTADO
       SET ESTADO = 'NOK'
     WHERE (DIF_CANTIDAD <> 0  OR DIF_PRECIO_MERCADO <> 0 OR DIF_VALOR_MERCADO <> 0)
       AND ESTADO = 'VIG'

	SELECT T.FECHA,
           ISNULL(C.ID_EMPRESA,0)      AS 'ID_EMPRESA',
           ISNULL(C.DSC_EMPRESA,'')    AS 'DSC_EMPRESA',
           ISNULL(C.ID_CLIENTE,0)      AS 'ID_CLIENTE',
           ISNULL(C.RUT_CLIENTE,0)     AS 'RUT_CLIENTE',
           ISNULL(C.NOMBRE_CLIENTE,'') AS 'NOMBRE_CLIENTE',
           T.ID_CUENTA,
           ISNULL(C.NUM_CUENTA,'')     AS 'NUM_CUENTA',
           T.ALIAS,
           T.CODIGO_CUSIP,
           T.ID_NEMOTECNICO,
           N.NEMOTECNICO,
           T.ID_SALDO_ACTIVO_INT,
           T.GPI_CANTIDAD,
           T.GPI_PRECIO_MERCADO        AS 'GPI_PRECIO',
           T.GPI_VALOR_MERCADO         AS 'GPI_MONTO',
           T.CTP_CANTIDAD,
           T.CTP_PRECIO_MERCADO        AS 'CTP_PRECIO',
           T.CTP_VALOR_MERCADO         AS 'CTP_MONTO',
           T.DIF_CANTIDAD,
           T.DIF_PRECIO_MERCADO        AS 'DIF_PRECIO',
           T.DIF_VALOR_MERCADO         AS 'DIF_MONTO',
           T.ESTADO
      FROM #TBLCONCILIA_RESULTADO  T
      LEFT OUTER JOIN (SELECT CT.ID_EMPRESA
                            , EM.DSC_EMPRESA
                            , CT.ID_CUENTA
                            , CT.NUM_CUENTA
                            , CT.ID_CLIENTE
                            , CL.RUT_CLIENTE
                            , CASE
							  WHEN (CL.TIPO_ENTIDAD = 'J') THEN CL.RAZON_SOCIAL
                              ELSE (ISNULL(CL.NOMBRES, '') + ' ' + ISNULL(CL.PATERNO, '') + ' ' + ISNULL(CL.MATERNO, ''))
							  END AS 'NOMBRE_CLIENTE'
                         FROM CUENTAS CT
                            , CLIENTES CL
                            , EMPRESAS EM
                        WHERE CL.ID_CLIENTE = CT.ID_CLIENTE
                          AND EM.ID_EMPRESA = CT.ID_EMPRESA
                      ) C ON C.ID_CUENTA = T.ID_CUENTA
      LEFT OUTER JOIN NEMOTECNICOS N ON N.ID_NEMOTECNICO = T.ID_NEMOTECNICO
     ORDER BY T.ESTADO
            , C.ID_EMPRESA
            , C.ID_CUENTA

    DROP TABLE #TBLCONCILIA_RESULTADO
    SET NOCOUNT OFF
END
GO

GRANT EXECUTE ON [PKG_CONCILIADOR$SALDOS_PSH_II] TO DB_EXECUTESP
GO
