USE [CISCB]
GO
IF  EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[sp_CartolaApvFlexibleWeb_temp]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [DBO].[sp_CartolaApvFlexibleWeb_temp]
GO

  
CREATE PROCEDURE [DBO].[sp_CartolaApvFlexibleWeb_temp]  
   @RutCliente  VARCHAR(15)  
 , @FechaDesde  VARCHAR(8) = NULL  
 , @FechaHasta  VARCHAR(8) = NULL  
 , @CodErr      INT    OUTPUT  
 , @MsgErr      VARCHAR(4000) OUTPUT  
AS  
BEGIN  
  
 SET NOCOUNT ON;  
  
 --***********************************************************************************************  
 -- DECLARACION DE VARIABLES  
 --***********************************************************************************************  
 DECLARE @DATE_DESDE    DATETIME  
 DECLARE @ID_CUENTA     INT  
 DECLARE @PID_CAJA_CUENTA   INT  
 DECLARE @PID_MONEDA_SALIDA   INT  
 DECLARE @ID_CLIENTE    INT  
 DECLARE @ID_EMPRESA    INT  
 DECLARE @ID_MONEDA     INT  
 DECLARE @ID_TIPOCUENTA    INT  
 DECLARE @TOTALVALORACTUAL   FLOAT --BIGINT  
 DECLARE @TOTALVALORANTERIOR  FLOAT --BIGINT  
 DECLARE @CAJAACTUAL    NUMERIC(18,4)--BIGINT  
 DECLARE @CAJAANTERIOR    NUMERIC(18,4)--BIGINT  
 DECLARE @COBRARACTUAL    NUMERIC(18,4)--BIGINT  
 DECLARE @COBRARANTERIOR   NUMERIC(18,4)--BIGINT  
 DECLARE @PAGARACTUAL    NUMERIC(18,4)--BIGINT  
 DECLARE @PAGARANTERIOR    NUMERIC(18,4)--BIGINT  
 DECLARE @PATRIMONIOANTERIOR  BIGINT  
 DECLARE @PATRIMONIOACTUAL   BIGINT  
 DECLARE @TOTALPASIVOACTUAL   FLOAT --BIGINT  
 DECLARE @TOTALPASIVOANTERIOR  FLOAT --BIGINT  
 DECLARE @CONSOLIDADO    VARCHAR(10)  
 DECLARE @ID      INT  
 DECLARE @DSC_ARBOL     VARCHAR(100)  
 DECLARE @CONT      INT  
 DECLARE @CANTIDAD     INT  
 DECLARE @PORCENTAJECAJA   NUMERIC(5,2)  
 DECLARE @PORCENTAJERV    NUMERIC(5,2)  
 DECLARE @PORCENTAJERF    NUMERIC(5,2)  
 DECLARE @VECNOM     VARCHAR(MAX)  
 DECLARE @VECMTO     VARCHAR(MAX)  
 DECLARE @CONTGRA     INT  
 DECLARE @CANTIDADCONTGRA   INT  
 DECLARE @DECIMALES     INT  
 DECLARE @SUMACAJA     NUMERIC(18,4)  
 DECLARE @SUMAVNVI     NUMERIC(18,4)  
 DECLARE @SUMAFNFI     NUMERIC(18,4)  
 DECLARE @SUMAOT     NUMERIC(18,4)  
 DECLARE @VECNOM_1     VARCHAR(MAX)  
 DECLARE @VECMTO_1     VARCHAR(MAX)  
 DECLARE @FECHA_OPERATIVA   DATETIME  
 DECLARE @FECHAJJ     DATETIME  
 DECLARE @FECHA_FINAL    DATETIME  
 DECLARE @INTERVALO     INT  
 DECLARE @MINVECNOM_1    NUMERIC(28, 6)  
 DECLARE @MAXVECNOM_1    NUMERIC(28, 6)  
  
 DECLARE @COMISIOHASTA    NUMERIC(18,6)  
 DECLARE @COMISIODESDE    NUMERIC(18,6)  
 DECLARE @CUENTASHASTA    NUMERIC(18,6)  
 DECLARE @CUENTASDESDE    NUMERIC(18,6)  
  
 --***********************************************************************************************  
 -- DECLARACION DE TABLAS  
 --***********************************************************************************************  
 DECLARE @CUENTAS TABLE (  
       ID        INT IDENTITY(1,1),  
       ID_CUENTA      INT NULL,  
       ID_CONTRATO_CUENTA    INT NULL,  
       NUM_CUENTA      INT NULL,  
       ABR_CUENTA      VARCHAR(30),  
       DSC_CUENTA      VARCHAR(100),  
       ID_MONEDA      INT,  
       COD_MONEDA      VARCHAR(10),  
       DSC_MONEDA      VARCHAR(30),  
       FLG_ES_MONEDA_PAGO    VARCHAR(10),  
       OBSERVACION      VARCHAR(100),  
       FLG_BLOQUEADO     VARCHAR(1),  
       OBS_BLOQUEO      VARCHAR(100),  
       FLG_IMP_INSTRUCCIONES   VARCHAR(1),  
       ID_CLIENTE      INT,  
       RUT_CLIENTE      VARCHAR(15),  
       NOMBRE_CLIENTE     VARCHAR(100),  
       ID_TIPO_ESTADO     INT,  
       COD_ESTADO      VARCHAR(1),  
       DSC_ESTADO      VARCHAR(30),  
       COD_TIPO_ADMINISTRACION   VARCHAR(10),  
       DSC_TIPO_ADMINISTRACION   VARCHAR(30),  
       ID_EMPRESA      INT,  
       DSC_EMPRESA      VARCHAR(30),  
       ID_PERFIL_RIESGO    INT,  
       DSC_PERFIL_RIESGO    VARCHAR(30),  
       FECHA_OPERATIVA     DATETIME NULL,  
       FLG_MOV_DESCUBIERTOS   VARCHAR(1),  
       ID_ASESOR      INT,  
       FECHA_CIERRE_CUENTA    DATETIME NULL,  
       DECIMALES_CUENTA    INT,  
       SIMBOLO_MONEDA     VARCHAR(10),  
       FECHA_CONTRATO     DATETIME,  
       ID_TIPOCUENTA     INT,  
       NUMERO_FOLIO     INT,  
       TIPO_AHORRO      INT,  
       RUT_ASESOR      VARCHAR(15),  
       NOMBRE_ASESOR     VARCHAR(100),  
       FLG_CONSIDERA_COM_VC   VARCHAR(1),  
       FECHA_CIERRE_OPERATIVA   DATETIME NULL,  
       DSC_TIPOAHORRO     VARCHAR(250)  
      )  
  
 DECLARE @PATRIMONIO TABLE (  
        ID_SALDO_CAJA    NUMERIC,  
        ID_CAJA_CUENTA    NUMERIC,  
        FECHA_CIERRE    DATETIME,  
        ID_MONEDA_CAJA    NUMERIC,  
        MONTO_MON_CAJA    FLOAT,  
        ID_MONEDA_CTA    NUMERIC,  
        MONTO_MON_CTA    FLOAT,  
        ID_CIERRE     NUMERIC,  
        MONTO_X_COBRAR_MON_CTA  FLOAT,  
        MONTO_X_PAGAR_MON_CTA  FLOAT,  
        MONTO_X_COBRAR_MON_CAJA  FLOAT,  
        MONTO_X_PAGAR_MON_CAJA  FLOAT  
        )  
  
 DECLARE @SERVICIO TABLE (  
        ID      INT    IDENTITY(1,1)  
         , NOMBRE     VARCHAR(100)  
         , VALORACTUAL    NUMERIC(18,4)  
         , PORCENTAJEACTUAL  NUMERIC(5,2)  
         , FECHAHASTA    VARCHAR(8)  
         , VALORANTERIOR   NUMERIC(18,4)  
         , PORCENTAJEANTERIOR  NUMERIC(5,2)  
         , FECHADESDE    VARCHAR(8)  
         , TIPODATO    VARCHAR(20)  
         , BLOQUE     INT   
       )  
  
 DECLARE @ARBOL TABLE (  
      ID_TABLA     INT  IDENTITY(1,1)  
       , ID       INT  
       , DSC_ARBOL     VARCHAR(30)  
       , NIVEL      INT  
       , MONTO_MON_CTA    BIGINT  
       , MONTO_MON_CTA_ANTERIOR  BIGINT  
       , VALOR_HOJA     INT  
       , PADRE      INT  
       , CODIGO      VARCHAR (50)  
       )  
  
 DECLARE @RAMA TABLE (  
      ID_ARBOL   INT,  
      ID_EMPRESA   INT,  
      ID_PADRE   INT,  
      DSC_ARBOL   VARCHAR(30),  
      ORDEN    INT,  
      CODIGO    VARCHAR(10),  
      ID_ACI_TIPO   INT,  
      MONTO_ACTUAL  FLOAT,  
      MONTO_ANTERIOR  FLOAT   
      )  
  
 DECLARE @RENTABILIDADES TABLE (  
         FECHA_CIERRE      DATETIME  
          , RENTABILIDAD_MON_CUENTA    NUMERIC(18,2)  
          , RENTABILIDAD_MENSUAL    NUMERIC(18,2)  
          , HAY_VALOR_CUOTA_MES     VARCHAR(2)  
          , RENTABILIDAD_ANUAL     NUMERIC(18,2)  
          , HAY_VALOR_CUOTA_ANO_CALENDARIO  VARCHAR(2)  
          , RENTABILIDAD_ULT_12_MESES   NUMERIC(18,2)  
          , HAY_VALOR_CUOTA_ULT_12_MESES  VARCHAR(2)  
          , VOLATILIDAD_MENSUAL     NUMERIC(18,2)  
          , VOLATILIDAD_ANUAL     NUMERIC(18,2)  
          , VOLATILIDAD_ULT_12_MESES   NUMERIC(18,2)  
          , RENTABILIDAD_INICIO_CUENTA   NUMERIC(18,2)  
          , RENTABILIDAD_MENSUAL_$$    NUMERIC(18,2)  
          , RENTABILIDAD_ANUAL_$$    NUMERIC(18,2)  
          , RENTABILIDAD_ULT_12_MESES_$$  NUMERIC(18,2)  
          , RENTABILIDAD_INICIO_CUENTA_$$  NUMERIC(18,2)  
          , RENTABILIDAD_MENSUAL_UF    NUMERIC(18,2)  
          , RENTABILIDAD_ANUAL_UF    NUMERIC(18,2)  
          , RENTABILIDAD_ULT_12_MESES_UF  NUMERIC(18,2)  
          , RENTABILIDAD_INICIO_CUENTA_UF  NUMERIC(18,2)  
          , RENTABILIDAD_MENSUAL_DO    NUMERIC(18,2)  
          , RENTABILIDAD_ANUAL_DO    NUMERIC(18,2)  
          , RENTABILIDAD_ULT_12_MESES_DO  NUMERIC(18,2)  
          , RENTABILIDAD_INICIO_CUENTA_DO  NUMERIC(18,2)  
          )  
  
 DECLARE @RENTABILIDADES_CONS TABLE (  
          RENTABILIDAD_MENSUAL_$$   NUMERIC(18,2)  
           , RENTABILIDAD_MENSUAL_UF   NUMERIC(18,2)  
           , RENTABILIDAD_MENSUAL_DO   NUMERIC(18,2)  
           , RENTABILIDAD_ANUAL_$$   NUMERIC(18,2)  
           , RENTABILIDAD_ANUAL_UF   NUMERIC(18,2)  
           , RENTABILIDAD_ANUAL_DO   NUMERIC(18,2)  
           , RENTABILIDAD_ULT_12_MESES_$$ NUMERIC(18,2)  
           , RENTABILIDAD_ULT_12_MESES_UF NUMERIC(18,2)  
           , RENTABILIDAD_ULT_12_MESES_DO NUMERIC(18,2)  
         )  
  
 DECLARE @RENTABILIDADES_OUT TABLE (  
          NOMBRE  VARCHAR(50)  
           , PESO  NUMERIC(5,2)  
           , UF   NUMERIC(5,2)  
           , DOLAR  NUMERIC(5,2)   
           )  
  
 DECLARE @FLUJO TABLE (  
      FECHA_MOVIMIENTO   DATETIME,  
      TIPO_AHORRO     VARCHAR(100),  
      FLG_TIPO_MOVIMIENTO   VARCHAR(100),  
      TIPO_MOVIMIENTO    VARCHAR(30),  
      MONTO      FLOAT,  
      MONTO_UF     FLOAT,  
      MONTO_TOTAL_RESCATES  FLOAT,  
      MONTO_TOTAL_RESCATES_UF  FLOAT,  
      MONTO_TOTAL_APORTES   FLOAT,  
      MONTO_TOTAL_APORTES_UF  FLOAT   
       )  
  
 DECLARE @FLUJOPATRIMONIAL TABLE (  
         FECHA  VARCHAR(8),  
         NOMBRE  VARCHAR(50),  
         APORTES  NUMERIC(18,2),  
         RETIROS  NUMERIC(18,2)   
         )  
  
  
 DECLARE @SALIDA_GRAFICO TABLE (  
         ID_TABLA   INT  IDENTITY(1,1),  
         FECHA_CIERRE  DATETIME,  
         VALOR_CUOTA   FLOAT,  
         RENTABILIDAD  NUMERIC(28,8),  
         VALOR_CUOTA_100  NUMERIC(28,8),  
         RENTAB_ACUM2  NUMERIC(28,8),  
         RENTAB_ACUM   NUMERIC(28,8)  
          )  
  
 DECLARE @PARAMETRO_GRAFICO TABLE (  
          ID_TABLA   INT  IDENTITY(1,1),  
          DIAS    INT ,  
          FECHA_CIERRE  DATETIME,  
          VALOR_CUOTA   NUMERIC(18, 6),  
          RENTABILIDAD  NUMERIC(18, 6),  
          VALOR_CUOTA_100  NUMERIC(18, 6),  
          RENTAB_ACUM   NUMERIC(18, 6)  
          )  
  
 DECLARE @ACTIVOSMONEDA TABLE (  
         MONEDA   VARCHAR(30),  
         PORCENTAJE  FLOAT,  
         MONTO   FLOAT  
         )  
  
 DECLARE @TMP_SALIDA TABLE (  
        MONEDA   VARCHAR(30),  
        PORCENTAJE  FLOAT,  
        MONTO   FLOAT  
         )  
  
 DECLARE @COMCARTERA TABLE (  
        SUMACAJA NUMERIC(5,2),  
        SUMAVNVI NUMERIC(5,2),  
        SUMAFNFI NUMERIC(5,2)  
         )  
  
 DECLARE @PATRIMONIOSALIDA TABLE (  
         ID_PATRIMONIO_CUENTA    INT,  
         ID_CUENTA       INT,  
         FECHA_CIERRE      DATETIME,  
         ID_MONEDA_CUENTA     INT,  
         COMI_DEVENG_MON_CTA     NUMERIC(18,4),  
         SALDO_CAJA_MON_CUENTA    NUMERIC(18,4),  
         SALDO_ACTIVO_MON_CUENTA    NUMERIC(18,4),  
         PATRIMONIO_UF      NUMERIC(18,4),  
         PATRIMONIO_USD                      NUMERIC(18,4),  
         PATRIMONIO_MON_CUENTA               NUMERIC(18,4),  
         PATRIMONIO_$$                       NUMERIC(18,4),  
         PATRIMONIO_MON_CUENTA_MDA_CTA       NUMERIC(18,4),  
         ID_MONEDA_EMPRESA                   INT,  
         SALDO_CAJA_MON_EMPRESA    NUMERIC(18,4),  
         SALDO_ACTIVO_MON_EMPRESA   NUMERIC(18,4),  
         PATRIMONIO_MON_EMPRESA    NUMERIC(18,4),  
         VALOR_CUOTA_MON_CUENTA    NUMERIC(18,4),  
         TOTAL_CUOTAS_MON_CUENTA    NUMERIC(18,4),  
         RENTABILIDAD_MON_CUENTA    NUMERIC(18,4),  
         APORTE_RETIROS_MON_CUENTA   NUMERIC(18,4),  
         ID_CIERRE       INT,  
         MONTO_X_COBRAR_MON_CTA    NUMERIC(18,4),  
         MONTO_X_PAGAR_MON_CTA    NUMERIC(18,4),  
         MONTO_X_COBRAR_MON_EMPRESA   NUMERIC(18,4),  
         MONTO_X_PAGAR_MON_EMPRESA   NUMERIC(18,4),  
         PATRIMONIO_MES_ANTERIOR    NUMERIC(18,4),  
         COMISIONES_DEVENGADAS    NUMERIC(18,4),  
         COMISION       NUMERIC(18,4),  
         SIMULTANEAS       NUMERIC(18,4),  
         FORWARD        FLOAT  
         )  
  
 --***********************************************************************************************  
 -- VARIABLES PASIVOS  
 --***********************************************************************************************  
 DECLARE @FLAGSALIDACOMISION  VARCHAR(5)  
 DECLARE @CONTCOM     INT  
 DECLARE @CANTIDADCOM    INT  
 DECLARE @ID_CUENTACOM    INT  
 DECLARE @PID_MONEDA_SALIDACOM  INT  
  --***********************************************************************************************  
  
BEGIN TRY  
  
 --***********************************************************************************************  
 -- INICIALIZACION DE VARIABLES  
 --***********************************************************************************************  
 SET @CODERR     = 0  
 SET @MSGERR     = ''  
 SET @CAJAACTUAL    = 0  
 SET @CAJAANTERIOR   = 0  
 SET @COBRARACTUAL   = 0  
 SET @COBRARANTERIOR   = 0  
 SET @PAGARACTUAL   = 0  
 SET @PAGARANTERIOR   = 0  
 SET @PATRIMONIOANTERIOR  = 0  
 SET @PATRIMONIOACTUAL  = 0  
 SET @CONSOLIDADO   = 'CLT'  
  
 --INICIALIZACION VARIABLES PASIVOS  
 SET @COMISIOHASTA = 0  
 SET @COMISIODESDE = 0  
 SET @CUENTASHASTA = 0  
 SET @CUENTASDESDE = 0  
  
 SELECT @ID_CLIENTE = CU.ID_CLIENTE  
   FROM CSGPI.DBO.CUENTAS AS CU WITH (NOLOCK)  
     INNER JOIN CSGPI.DBO.CLIENTES AS CL  WITH (NOLOCK) ON CU.ID_CLIENTE = CL.ID_CLIENTE  
     WHERE CL.RUT_CLIENTE = @RUTCLIENTE  
       AND CU.COD_ESTADO = 'H'  
  
 --***********************************************************************************************  
 -- PATRIMONIO  
 --***********************************************************************************************  
  
 IF @ID_CLIENTE IS NOT NULL  
 BEGIN  
  INSERT INTO @CUENTAS EXEC CSGPI.DBO.PKG_CUENTAS$BUSCARCUENTASAPV @ID_CLIENTE, NULL  
  
  SELECT TOP 1  
      @ID_EMPRESA = ID_EMPRESA,  
      @ID_MONEDA = ID_MONEDA,  
      @ID_TIPOCUENTA = ID_TIPOCUENTA,  
      @ID_CLIENTE = ID_CLIENTE  
    FROM @CUENTAS  
 END  
  
 SET @CONT = 1  
 SET @CANTIDAD = ISNULL((SELECT COUNT(ID) FROM @CUENTAS), 0)  
  
 WHILE @CONT <= @CANTIDAD    -- CICLO PARA CUENTAS  
 BEGIN  
  SELECT @ID_CUENTA = ID_CUENTA  
    FROM @CUENTAS  
   WHERE ID = @CONT  
  
  --***********************************************************************************************  
  -- PASIVOS  
  --***********************************************************************************************  
  
  SELECT @FLAGSALIDACOMISION=FLG_CONSIDERA_COM_VC FROM @CUENTAS  
  
  --***********************************************************************************************  
  -- MONTOS @FechaHasta  
  --***********************************************************************************************  
  
  SET @PID_MONEDA_SALIDACOM = 1  
  
  INSERT INTO @PATRIMONIOSALIDA  
  EXEC CSGPI.DBO.PKG_PATRIMONIO_CUENTAS$BUSCAR @ID_CUENTA,@FECHAHASTA,@PID_MONEDA_SALIDACOM  
  
  IF EXISTS(SELECT 1 FROM @PATRIMONIOSALIDA)  
  BEGIN  
   SET @COMISIOHASTA = @COMISIOHASTA + ISNULL((SELECT COMISION FROM @PATRIMONIOSALIDA), 0)  
   SET @CUENTASHASTA = @CUENTASHASTA + ISNULL((SELECT MONTO_X_PAGAR_MON_CTA FROM @PATRIMONIOSALIDA), 0)  
  
   DELETE FROM @PATRIMONIOSALIDA  
  END  
  
  --***********************************************************************************************  
  -- MONTOS @FechaDesde  
  --***********************************************************************************************  
  
  INSERT INTO @PATRIMONIOSALIDA  
  EXEC CSGPI.DBO.PKG_PATRIMONIO_CUENTAS$BUSCAR @ID_CUENTA,@FECHADESDE,@PID_MONEDA_SALIDACOM  
  
  IF EXISTS(SELECT 1 FROM @PATRIMONIOSALIDA)  
  BEGIN  
   SET @COMISIODESDE = @COMISIODESDE + ISNULL((SELECT COMISION FROM @PATRIMONIOSALIDA), 0)  
   SET @CUENTASDESDE = @CUENTASDESDE + ISNULL((SELECT MONTO_X_PAGAR_MON_CTA FROM @PATRIMONIOSALIDA), 0) --ISNULL((SELECT SALDO_CAJA_MON_CUENTA FROM @PATRIMONIO), 0)  
  
   DELETE FROM @PATRIMONIOSALIDA  
  END  
  
  --*****************************************************************************  
  --*****************************************************************************  
  
  --***********************************************************************************************  
  -- Obtiene Datos cuando estan Nulos los parámetros  
  --***********************************************************************************************  
  IF @FECHAHASTA IS NULL OR @FECHAHASTA = ''  
   SET @FECHAHASTA = CONVERT(VARCHAR(8),(CSGPI.DBO.FNT_ENTREGAFECHACIERRECUENTA(@ID_CUENTA)),112)  
  
  IF @FECHADESDE IS NULL OR @FECHADESDE = ''  
  BEGIN  
   SET @DATE_DESDE = CONVERT(DATETIME,@FECHAHASTA,112)  
   SET @DATE_DESDE = CONVERT(DATETIME,'01/' + CAST(MONTH(@DATE_DESDE) AS VARCHAR(2)) + '/' + CAST(YEAR(@DATE_DESDE) AS VARCHAR(4)),103)  
   SET @FECHADESDE = CONVERT(VARCHAR(8),DATEADD(D,-1,(@DATE_DESDE)),112)  
  END  
  
  --***********************************************************************************************  
  -- MONTOS ANTERIORES  
  --***********************************************************************************************  
  
  DECLARE CCAJASRESUMEN CURSOR FOR   -- CURSOR PARA CAJAS DE CUENTAS  
   SELECT ID_CAJA_CUENTA  
     , ID_MONEDA  
     FROM CSGPI.DBO.CAJAS_CUENTA WITH (NOLOCK)  
    WHERE ID_CUENTA IN (@ID_CUENTA)  
  
  OPEN    CCAJASRESUMEN  
  FETCH CCAJASRESUMEN INTO    @PID_CAJA_CUENTA,@PID_MONEDA_SALIDA  
  
  WHILE (@@FETCH_STATUS = 0 )  
  BEGIN  
   --***********************************************************************************************  
   INSERT INTO @PATRIMONIO   
   EXEC CSGPI.DBO.PKG_SALDOS_CAJA$BUSCAR @PID_CAJA_CUENTA, @FECHADESDE, NULL, @ID_CUENTA, 1  
  
   IF EXISTS(SELECT ID_SALDO_CAJA FROM @PATRIMONIO)  
   BEGIN  
    SET @COBRARANTERIOR = @COBRARANTERIOR + ISNULL((SELECT MONTO_X_COBRAR_MON_CTA FROM @PATRIMONIO), 0)  
    SET @CAJAANTERIOR   = @CAJAANTERIOR   + ISNULL((SELECT MONTO_MON_CTA FROM @PATRIMONIO), 0)   
    SET @PAGARANTERIOR  = @PAGARANTERIOR  + ISNULL((SELECT MONTO_X_PAGAR_MON_CTA FROM @PATRIMONIO), 0)  
  
    DELETE FROM @PATRIMONIO  
   END  
  
   --***********************************************************************************************  
   -- MONTOS ACTUALES  
   --***********************************************************************************************  
  
   INSERT INTO @PATRIMONIO   
   EXEC CSGPI.DBO.PKG_SALDOS_CAJA$BUSCAR @PID_CAJA_CUENTA, @FECHAHASTA, NULL, @ID_CUENTA, 1  
  
   IF EXISTS(SELECT ID_SALDO_CAJA FROM @PATRIMONIO)  
   BEGIN  
    SET @COBRARACTUAL = @COBRARACTUAL + ISNULL((SELECT MONTO_X_COBRAR_MON_CTA FROM @PATRIMONIO), 0)  
    SET @CAJAACTUAL = @CAJAACTUAL + ISNULL((SELECT MONTO_MON_CTA FROM @PATRIMONIO), 0) --ISNULL((SELECT SALDO_CAJA_MON_CUENTA FROM @PATRIMONIO), 0)  
    SET @PAGARACTUAL = @PAGARACTUAL + ISNULL((SELECT MONTO_X_PAGAR_MON_CTA FROM @PATRIMONIO), 0)  
  
    DELETE FROM @PATRIMONIO  
   END  
  
   FETCH CCAJASRESUMEN INTO  @PID_CAJA_CUENTA,@PID_MONEDA_SALIDA  
  END  
    
  CLOSE CCAJASRESUMEN  
  DEALLOCATE CCAJASRESUMEN  
 -----------------  
  
  SET @CONT = @CONT + 1  
 END  
  
 IF @COBRARANTERIOR  IS NULL SET @COBRARANTERIOR = 0  
 IF @CAJAANTERIOR  IS NULL SET @CAJAANTERIOR = 0  
 IF @PAGARANTERIOR  IS NULL SET @PAGARANTERIOR = 0  
 IF @PATRIMONIOANTERIOR IS NULL SET @PATRIMONIOANTERIOR = 0  
 IF @COBRARACTUAL  IS NULL SET @COBRARACTUAL = 0  
 IF @CAJAACTUAL   IS NULL SET @CAJAACTUAL = 0  
 IF @PAGARACTUAL   IS NULL SET @PAGARACTUAL = 0  
 IF @PATRIMONIOACTUAL IS NULL SET @PATRIMONIOACTUAL = 0  
  
  -- Inserta Registros  
  INSERT INTO @SERVICIO (  
       NOMBRE,  
       VALORACTUAL,  
       FECHAHASTA,  
       VALORANTERIOR,  
       FECHADESDE,  
       TIPODATO,  
       BLOQUE  
      )  
  VALUES(  
   'Caja'  
    , ROUND(@CAJAACTUAL,0)  
    , @FECHAHASTA  
    , ROUND(@CAJAANTERIOR    ,0)  
    , @FECHADESDE  
    , 'ACTIVOS'  
    , 0   
  )  
  
  INSERT INTO @SERVICIO (  
       NOMBRE,  
       VALORACTUAL,  
       FECHAHASTA,  
       VALORANTERIOR,  
       FECHADESDE,  
       TIPODATO,  
       BLOQUE  
      )  
  VALUES(  
   'Cuentas por Cobrar'  
    , ROUND(@COBRARACTUAL    ,0)  
    , @FECHAHASTA  
    , ROUND(@COBRARANTERIOR    ,0)  
    , @FECHADESDE  
    , 'ACTIVOS'  
    , 0   
  )  
  
  --***********************************************************************************************  
  -- Renta Variable y Renta Fija  
  --***********************************************************************************************  
  
  INSERT INTO @Arbol (  
      ID,  
      DSC_ARBOL,  
      NIVEL,  
      MONTO_MON_CTA,  
      MONTO_MON_CTA_ANTERIOR,  
      VALOR_HOJA,  
      PADRE,  
      CODIGO  
  
  --)  EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_ARBOLES NULL, @ID_EMPRESA, @FechaHasta, @ID_CLIENTE, NULL, @ID_MONEDA, @Consolidado  
  )  EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_ARBOLES_vr2 NULL, @ID_EMPRESA, @FechaHasta, @ID_CLIENTE, NULL, @ID_MONEDA, @Consolidado, 2  
    
  SET @CANTIDAD = ISNULL((SELECT COUNT(ID) FROM @ARBOL), 0)  
  SET @CONT = 1  
  
  WHILE @CONT <= @CANTIDAD  
  BEGIN  
        
    SELECT @ID = ID  
     ,@DSC_ARBOL = DSC_ARBOL  
      FROM @ARBOL  
     WHERE ID_TABLA = @CONT  
  
    IF @DSC_ARBOL = 'Renta Variable Nacional'--'RENTA VARIABLE NACIONAL'  
     INSERT INTO @RAMA EXEC CSGPI.DBO.PKG_SALDOS_ACTIVOS$ABRE_RAMA @ID, NULL, @FECHAHASTA, @ID_EMPRESA, @ID_CLIENTE, NULL, @ID_MONEDA, @CONSOLIDADO  
     --INSERT INTO @RAMA EXEC CSGPI.DBO.PKG_SALDOS_ACTIVOS$ABRE_RAMA_VR2 @ID, NULL, @FECHAHASTA, @ID_EMPRESA, @ID_CLIENTE, NULL, @ID_MONEDA, @CONSOLIDADO  
      
    IF @DSC_ARBOL = 'Renta Fija Nacional'--'RENTA FIJA NACIONAL'  
     INSERT INTO @RAMA EXEC CSGPI.DBO.PKG_SALDOS_ACTIVOS$ABRE_RAMA @ID, NULL, @FECHAHASTA, @ID_EMPRESA, @ID_CLIENTE, NULL, @ID_MONEDA, @CONSOLIDADO  
  --INSERT INTO @RAMA EXEC CSGPI.DBO.PKG_SALDOS_ACTIVOS$ABRE_RAMA_VR2 @ID, NULL, @FECHAHASTA, @ID_EMPRESA, @ID_CLIENTE, NULL, @ID_MONEDA, @CONSOLIDADO  
  
    IF @DSC_ARBOL = 'Renta Fija Internacional'--'RENTA FIJA INTERNACIONAL'  
     INSERT INTO @RAMA EXEC CSGPI.DBO.PKG_SALDOS_ACTIVOS$ABRE_RAMA @ID, NULL, @FECHAHASTA, @ID_EMPRESA, @ID_CLIENTE, NULL, @ID_MONEDA, @CONSOLIDADO  
     --INSERT INTO @RAMA EXEC CSGPI.DBO.PKG_SALDOS_ACTIVOS$ABRE_RAMA_VR2 @ID, NULL, @FECHAHASTA, @ID_EMPRESA, @ID_CLIENTE, NULL, @ID_MONEDA, @CONSOLIDADO  
      
    IF @DSC_ARBOL = 'Renta Variable Internacional'--'RENTA VARIABLE INTERNACIONAL'  
     INSERT INTO @RAMA EXEC CSGPI.DBO.PKG_SALDOS_ACTIVOS$ABRE_RAMA @ID, NULL, @FECHAHASTA, @ID_EMPRESA, @ID_CLIENTE, NULL, @ID_MONEDA, @CONSOLIDADO  
     --INSERT INTO @RAMA EXEC CSGPI.DBO.PKG_SALDOS_ACTIVOS$ABRE_RAMA_VR2 @ID, NULL, @FECHAHASTA, @ID_EMPRESA, @ID_CLIENTE, NULL, @ID_MONEDA, @CONSOLIDADO  
  
    SET @CONT = @CONT + 1  
  END  
  
  
  --RENTA FIJA NACIONAL  
  
  INSERT INTO @SERVICIO (NOMBRE, VALORACTUAL, FECHAHASTA, VALORANTERIOR, FECHADESDE, TIPODATO, BLOQUE)  
  SELECT 'RENTA FIJA', SUM(MONTO_ACTUAL), @FECHAHASTA, SUM(MONTO_ANTERIOR), @FECHADESDE, 'RENTA FIJA', 2  
    FROM @RAMA  
   --WHERE CODIGO = 'RF'  
   WHERE ID_PADRE IN (SELECT ID FROM @ARBOL WHERE CODIGO IN ('RF'))  
  
  INSERT INTO @SERVICIO (NOMBRE, VALORACTUAL, FECHAHASTA, VALORANTERIOR, FECHADESDE, TIPODATO, BLOQUE)  
  SELECT DSC_ARBOL, MONTO_ACTUAL, @FECHAHASTA, MONTO_ANTERIOR, @FECHADESDE, 'RENTA FIJA', 2      
    FROM @RAMA  
   --WHERE CODIGO = 'RF'  
   WHERE ID_PADRE IN (SELECT ID FROM @ARBOL WHERE CODIGO IN ('RF'))  
  
  --RENTA VARIABLE NACIONAL  
  
  INSERT INTO @SERVICIO (NOMBRE, VALORACTUAL, FECHAHASTA, VALORANTERIOR, FECHADESDE, TIPODATO, BLOQUE)  
  SELECT 'RENTA VARIABLE', SUM(MONTO_ACTUAL), @FECHAHASTA, SUM(MONTO_ANTERIOR), @FECHADESDE,'RENTA VARIABLE', 1  
    FROM @RAMA  
  -- WHERE CODIGO IN ('RV','FFMM')--CODIGO = 'RV'  
   WHERE ID_PADRE IN (SELECT ID FROM @ARBOL WHERE CODIGO IN ('RV'))  
  
  INSERT INTO @SERVICIO (NOMBRE, VALORACTUAL, FECHAHASTA, VALORANTERIOR, FECHADESDE, TIPODATO, BLOQUE)  
  SELECT DSC_ARBOL, MONTO_ACTUAL, @FECHAHASTA, MONTO_ANTERIOR, @FECHADESDE, 'RENTA VARIABLE', 1  
    FROM @RAMA  
   --WHERE CODIGO IN ('RV','FFMM')--CODIGO = 'RV'  
   WHERE ID_PADRE IN (SELECT ID FROM @ARBOL WHERE CODIGO IN ('RV'))  
  
  --RENTA FIJA INTERNACIONAL  
  
  INSERT INTO @SERVICIO (NOMBRE, VALORACTUAL, FECHAHASTA, VALORANTERIOR, FECHADESDE, TIPODATO, BLOQUE)  
  SELECT 'RENTA FIJA INT', SUM(MONTO_ACTUAL), @FECHAHASTA, SUM(MONTO_ANTERIOR), @FECHADESDE, 'RENTA FIJA INT', 2  
    FROM @RAMA  
   --WHERE CODIGO = 'RF'  
   WHERE ID_PADRE IN (SELECT ID FROM @ARBOL WHERE CODIGO IN ('RFINT'))  
  
  INSERT INTO @SERVICIO (NOMBRE, VALORACTUAL, FECHAHASTA, VALORANTERIOR, FECHADESDE, TIPODATO, BLOQUE)  
  SELECT DSC_ARBOL, MONTO_ACTUAL, @FECHAHASTA, MONTO_ANTERIOR, @FECHADESDE, 'RENTA FIJA INT', 2      
    FROM @RAMA  
   --WHERE CODIGO = 'RF'  
   WHERE ID_PADRE IN (SELECT ID FROM @ARBOL WHERE CODIGO IN ('RFINT'))  
  
  --RENTA VARIABLE INTERNACIONAL  
  
  INSERT INTO @SERVICIO (NOMBRE, VALORACTUAL, FECHAHASTA, VALORANTERIOR, FECHADESDE, TIPODATO, BLOQUE)  
  SELECT 'RENTA VARIABLE INT', SUM(MONTO_ACTUAL), @FECHAHASTA, SUM(MONTO_ANTERIOR), @FECHADESDE,'RENTA VARIABLE INT', 1  
    FROM @RAMA  
  -- WHERE CODIGO IN ('RV','FFMM')--CODIGO = 'RV'  
   WHERE ID_PADRE IN (SELECT ID FROM @ARBOL WHERE CODIGO IN ('RVINT'))  
  
  INSERT INTO @SERVICIO (NOMBRE, VALORACTUAL, FECHAHASTA, VALORANTERIOR, FECHADESDE, TIPODATO, BLOQUE)  
  SELECT DSC_ARBOL, MONTO_ACTUAL, @FECHAHASTA, MONTO_ANTERIOR, @FECHADESDE, 'RENTA VARIABLE INT', 1  
    FROM @RAMA  
   --WHERE CODIGO IN ('RV','FFMM')--CODIGO = 'RV'  
   WHERE ID_PADRE IN (SELECT ID FROM @ARBOL WHERE CODIGO IN ('RVINT'))     
        
  --***********************************************************************************************  
  -- TOTAL ACTIVOS  
  --***********************************************************************************************  
  
   SELECT @TOTALVALORACTUAL = SUM(VALORACTUAL), @TOTALVALORANTERIOR = SUM(VALORANTERIOR)  
     FROM @SERVICIO  
    WHERE NOMBRE = 'RENTA FIJA'  
       OR NOMBRE = 'RENTA VARIABLE'  
       OR NOMBRE = 'RENTA FIJA INT'  
       OR NOMBRE = 'RENTA VARIABLE INT'  
         
       OR BLOQUE = 0  
  
  INSERT INTO @SERVICIO (  
       NOMBRE,  
       VALORACTUAL,  
       PORCENTAJEACTUAL,  
       FECHAHASTA,  
       VALORANTERIOR,  
       PORCENTAJEANTERIOR,  
       FECHADESDE,  
       TIPODATO,  
       BLOQUE  
      )  
  VALUES(  
   'TOTAL ACTIVOS'  
    , @TotalValorActual  
    , null --100  
    , @FechaHasta  
    , @TotalValorAnterior  
    , null --100  
    , @FechaDesde  
    , 'TOTAL ACTIVOS'  
    , 3 )  
  
  --***********************************************************************************************  
  -- Actualiza Porcentajes  
  --***********************************************************************************************  
  
  IF @TOTALVALORACTUAL <> 0  
    UPDATE @SERVICIO  
       SET PORCENTAJEACTUAL = (CAST(VALORACTUAL AS DECIMAL) * 100) / CAST(@TOTALVALORACTUAL AS DECIMAL)  
     WHERE PORCENTAJEACTUAL IS NULL  
  ELSE  
    UPDATE @SERVICIO  
       SET PORCENTAJEACTUAL = 0  
     WHERE PORCENTAJEACTUAL IS NULL  
  
  IF @TOTALVALORANTERIOR <> 0  
    UPDATE @SERVICIO  
       SET PORCENTAJEANTERIOR = (CAST(VALORANTERIOR AS DECIMAL) * 100) / CAST(@TOTALVALORANTERIOR AS DECIMAL)  
     WHERE PORCENTAJEANTERIOR IS NULL  
  ELSE  
    UPDATE @SERVICIO  
       SET PORCENTAJEANTERIOR = 0  
     WHERE PORCENTAJEANTERIOR IS NULL  
  
  IF (@COMISIOHASTA IS NULL)  
  BEGIN  
 SET @COMISIOHASTA=0  
  END  
  IF (@CUENTASHASTA IS NULL)  
  BEGIN  
 SET @CUENTASHASTA=0  
  END  
  
  IF (@COMISIODESDE IS NULL)  
  BEGIN  
 SET @COMISIODESDE=0  
  END  
  IF (@CUENTASDESDE IS NULL)  
  BEGIN  
 SET @CUENTASDESDE=0  
  END  
  
  IF (LTRIM(RTRIM(@FLAGSALIDACOMISION))= 'S')  
  BEGIN  
 INSERT INTO @SERVICIO (  
       NOMBRE,  
       VALORACTUAL,  
       FECHAHASTA,  
       VALORANTERIOR,  
       FECHADESDE,  
       TIPODATO,  
       BLOQUE  
        )  
 VALUES(  
   'Comisión por Administración'  
    , @COMISIOHASTA--0  
    , @FECHAHASTA  
    , @COMISIODESDE--0  
    , @FECHADESDE  
    , 'PASIVOS'  
    , 4)  
  END  
  
  INSERT INTO @SERVICIO (  
       NOMBRE,  
       VALORACTUAL,  
       FECHAHASTA,  
       VALORANTERIOR,  
       FECHADESDE,  
       TIPODATO,  
       BLOQUE  
      )  
  VALUES(  
   'Cuentas por Pagar'  
    , @CUENTASHASTA --@PAGARACTUAL  
    , @FECHAHASTA  
    , @CUENTASDESDE--@PAGARANTERIOR  
    , @FECHADESDE  
    , 'PASIVOS'  
    , 4 )  
  
   SELECT @TOTALPASIVOACTUAL = SUM(VALORACTUAL), @TOTALPASIVOANTERIOR = SUM(VALORANTERIOR)  
     FROM @SERVICIO  
    WHERE BLOQUE = 4  
  
  INSERT INTO @SERVICIO (  
       NOMBRE,  
       VALORACTUAL,  
       FECHAHASTA,  
       VALORANTERIOR,  
       FECHADESDE,  
       TIPODATO,  
       BLOQUE  
      )  
  VALUES(  
   'TOTAL PASIVOS'  
    , @TOTALPASIVOACTUAL  
    , @FECHAHASTA  
    , @TOTALPASIVOANTERIOR  
    , @FECHADESDE  
    , 'PASIVOS'  
    , 4 )  
  
  --***********************************************************************************************  
  -- TOTAL PATRIMONIO  
  --***********************************************************************************************  
  INSERT INTO @SERVICIO (  
       NOMBRE,  
       VALORACTUAL,  
       FECHAHASTA,  
       VALORANTERIOR,  
       FECHADESDE,  
       TIPODATO,  
       BLOQUE  
      )  
  VALUES(  
   'PATRIMONIO'  
    , (@TOTALVALORACTUAL - @TOTALPASIVOACTUAL)  
    , @FECHAHASTA  
    , (@TOTALVALORANTERIOR - @TOTALPASIVOANTERIOR)  
    , @FECHADESDE  
    , 'PATRIMONIO'      , 5 )  
  
  --***********************************************************************************************  
  -- SALIDA DE DATOS : SERVICIOS  
  --***********************************************************************************************  
  
   SELECT InicioBloque = 'Servicio',  
    TipoDato,  
    Nombre,  
    ValorActual,  
    ISNULL(PORCENTAJEACTUAL,0) AS PorcentajeActual,  
    FechaHasta,  
    ValorAnterior,  
    ISNULL(PORCENTAJEANTERIOR,0) AS PorcentajeAnterior,  
    FechaDesde  
     FROM @SERVICIO  
    ORDER BY BLOQUE, TipoDato DESC, ID  
  
  --***********************************************************************************************  
  -- RENTABILIDADES  
  --***********************************************************************************************  
  
  INSERT INTO @RENTABILIDADES_CONS    
  EXEC CSGPI.DBO.PKG_PATRIMONIO_CUENTAS$RENTABILIDADES_CONSOLIDADA @ID_EMPRESA, @ID_CLIENTE, @FECHAHASTA,@ID_MONEDA  
  
  INSERT INTO @RENTABILIDADES_OUT (  
         NOMBRE,  
         PESO,  
         UF,  
         DOLAR  
          )  
   SELECT NOMBRE = 'Rentabilidad Mensual'  
  , PESO = RENTABILIDAD_MENSUAL_$$  
  , UF = RENTABILIDAD_MENSUAL_UF  
  , DOLAR = RENTABILIDAD_MENSUAL_DO  
     FROM @RENTABILIDADES_CONS  
  
  INSERT INTO @RENTABILIDADES_OUT (  
    NOMBRE,  
    PESO,  
    UF,  
    DOLAR  
   )  
   SELECT NOMBRE = 'Rentabilidad Acumulada Anual'  
  , PESO = RENTABILIDAD_ANUAL_$$  
  , UF = RENTABILIDAD_ANUAL_UF  
  , DOLAR = RENTABILIDAD_ANUAL_DO  
     FROM @RENTABILIDADES_CONS  
  
  INSERT INTO @RENTABILIDADES_OUT (  
         NOMBRE,  
         PESO,  
         UF,  
         DOLAR  
          )  
   SELECT NOMBRE = 'Rentabilidad Últimos 12 Meses'  
  , PESO = RENTABILIDAD_ULT_12_MESES_$$  
  , UF = RENTABILIDAD_ULT_12_MESES_UF  
  , DOLAR = RENTABILIDAD_ULT_12_MESES_DO  
     FROM @RENTABILIDADES_CONS  
  
  --***********************************************************************************************  
  -- SALIDA DE DATOS : RENTABILIDADES  
  --***********************************************************************************************  
  
   SELECT InicioBloque = 'Rentabilidades'  
  , Nombre  
  , Peso  
  , Uf  
  , Dolar  
     FROM @RENTABILIDADES_OUT  
  
  --***********************************************************************************************  
  -- FLUJO PATRIMONIAL  
  --***********************************************************************************************  
  
  INSERT INTO @FLUJO   
  EXEC CSGPI.DBO.PKG_FLUJO_PATRIMONIAL$BUSCARCONSOLIDADO NULL, '20000101', @FECHAHASTA, @ID_MONEDA, @CONSOLIDADO, @ID_CLIENTE, NULL, @ID_TIPOCUENTA  
  
  INSERT INTO @FLUJOPATRIMONIAL (  
         FECHA,  
         NOMBRE,  
         APORTES,  
         RETIROS  
        )  
   SELECT TOP 6 CONVERT(VARCHAR(8),FECHA_MOVIMIENTO,112)  
  , TIPO_AHORRO  
  , APORTE = CASE WHEN  (TIPO_MOVIMIENTO ='APORTE' ) THEN MONTO ELSE 0 END  
  , RETIRO= CASE WHEN  (TIPO_MOVIMIENTO ='RETIRO' ) THEN MONTO ELSE 0   END  
     FROM @FLUJO  
  
  INSERT INTO @FLUJOPATRIMONIAL (  
         NOMBRE,  
         APORTES,  
         RETIROS  
        )  
   SELECT TOP 1  
    NOMBRE = 'TOTAL EN PESOS'  
  , APORTES = MONTO_TOTAL_APORTES  
  , RETIROS = MONTO_TOTAL_RESCATES  
     FROM @FLUJO  
  
  INSERT INTO @FLUJOPATRIMONIAL (  
         NOMBRE,  
         APORTES,  
         RETIROS  
        )  
   SELECT TOP 1  
    NOMBRE = 'TOTAL EN UF'  
  , APORTES = MONTO_TOTAL_APORTES_UF  
  , RETIROS = MONTO_TOTAL_RESCATES_UF  
     FROM @FLUJO  
  
  --***********************************************************************************************  
  -- SALIDA DE DATOS : FLUJO PATRIMONIAL  
  --***********************************************************************************************  
  
   SELECT InicioBloque = 'FlujoPatrimonial'  
  , Fecha  
  , Nombre  
  , Aportes  
  , Retiros  
     FROM @FLUJOPATRIMONIAL  
  
  --***********************************************************************************************  
  -- GENERA GRAFICO  
  --***********************************************************************************************  
  
  SELECT @PORCENTAJECAJA = SUM(PORCENTAJEACTUAL) FROM @SERVICIO WHERE NOMBRE IN ('Caja','Cuentas por Cobrar')  
  SELECT @PORCENTAJERV = PORCENTAJEACTUAL FROM @SERVICIO WHERE NOMBRE = 'RENTA VARIABLE'  
  SELECT @PORCENTAJERF = PORCENTAJEACTUAL FROM @SERVICIO WHERE NOMBRE = 'RENTA FIJA'  
  
  SET @VECNOM = ''  
  SET @VECMTO = ''  
  
  IF @PORCENTAJECAJA > 0  
  BEGIN  
 SET @VECNOM = @VECNOM + 'Caja' + ';'  
 SET @VECMTO = @VECMTO + REPLACE(CAST(@PorcentajeCaja AS  VARCHAR(10)),'.',',') + ';'  
  END  
  
  IF @PORCENTAJERV > 0  
  BEGIN  
 SET @VECNOM = @VECNOM + 'Renta Variable' + ';'  
 SET @VECMTO = @VECMTO + REPLACE(CAST(@PorcentajeRV AS  VARCHAR(10)),'.',',') + ';'  
  END  
  
  IF @PORCENTAJERF > 0  
  BEGIN  
 SET @VECNOM = @VECNOM + 'Renta Fija' + ';'  
 SET @VECMTO = @VECMTO + REPLACE(CAST(@PORCENTAJERF AS  VARCHAR(10)),'.',',') + ';'  
  END  
  
  IF LEN(@VECNOM) > 1  
  BEGIN  
 SET @VECNOM = SUBSTRING(@VECNOM,1,LEN(@VECNOM)-1)  
 SET @VECMTO = SUBSTRING(@VECMTO,1,LEN(@VECMTO)-1)  
  END  
  
  SELECT InicioBloque = 'Grafico', Vecnom = @VECNOM, Vecmto = @VECMTO  
  
  --***********************************************************************************************  
  --***********************************************************************************************  
  -- ACTIVOS POR MONEDAS  
  --***********************************************************************************************  
  
 SET @DECIMALES =(SELECT DICIMALES_MOSTRAR FROM CSGPI.DBO.MONEDAS WITH (NOLOCK) WHERE ID_MONEDA =1)  
  
 INSERT INTO @ACTIVOSMONEDA VALUES ('Pesos',0,0)  
 INSERT INTO @ACTIVOSMONEDA VALUES ('UF',0,0)  
 INSERT INTO @ACTIVOSMONEDA VALUES ('TOTAL',0,0)  
  
 INSERT INTO @TMP_SALIDA   
 EXEC CSGPI.DBO.PKG_CARTOLA_CONS$BUSCAR_ACTIVOS_MONEDA 'CLT',@ID_CLIENTE, @FECHAHASTA,1,@ID_EMPRESA  
  
 UPDATE @ACTIVOSMONEDA  
    SET PORCENTAJE = T2.PORCENTAJE, MONTO = T2.MONTO  
   FROM @ACTIVOSMONEDA T1, @TMP_SALIDA T2  
  WHERE T1.MONEDA = T2.MONEDA  
  
 SELECT  
   CASE 1 
   WHEN 1 THEN 'Monto en $'--'PESOS'  
   WHEN 2 THEN 'Monto en USD'--'DOLAR'  
   END AS signoMoneda,  
   MONEDA as moneda  
    , dbo.fn_CC_IncluirMilesCartola(PORCENTAJE,2) as porcentaje  
    , dbo.fn_CC_IncluirMilesCartola(MONTO,@Decimales) as monto  
    FROM @ActivosMoneda  
  
  --***********************************************************************************************  
  -- COMPOSICION CARTERA  
  --***********************************************************************************************  
  
  SET @SUMACAJA =@PORCENTAJECAJA  
  SET @SUMAVNVI= (@PORCENTAJERV)  
  SET @SUMAFNFI = (@PORCENTAJERF)  
  
  INSERT INTO @COMCARTERA VALUES (@SUMACAJA,@SUMAVNVI,@SUMAFNFI)  
  
  SELECT dbo.fn_CC_IncluirMilesCartola(SumaCaja,2) AS Caja,  
         dbo.fn_CC_IncluirMilesCartola(SumaVNVI,2) as Rv,  
         dbo.fn_CC_IncluirMilesCartola(SumaFNFI,2) as Rf  
    FROM @ComCartera  
  
  --***********************************************************************************************  
  -- GENERA GRAFICO LINEA  
  --***********************************************************************************************  
  
  SET @CONTGRA = 1  
  SET @VECNOM_1 = ''  
  SET @VECMTO_1 = ''  
  SET @FECHAJJ=GETDATE()  
  SET @CANTIDADCONTGRA = ISNULL((SELECT COUNT(ID) FROM @CUENTAS), 0)  
  SET @FECHA_FINAL=  CONVERT(DATETIME,@FECHAHASTA)  
  
  SELECT @ID_CUENTA = ID_CUENTA  
       , @FECHA_OPERATIVA=FECHA_OPERATIVA  
    FROM @CUENTAS   
   ORDER BY FECHA_OPERATIVA DESC  
  
  
  --***********************************************************************************************  
  -- obtiene datos  
  --***********************************************************************************************  
  INSERT INTO @PARAMETRO_GRAFICO  
  EXEC  [CSGPI].[DBO].[PKG_WEB$GRAFICO_RENTABILIDAD] @ID_CUENTA, NULL, NULL, @FECHA_FINAL  
  
  SELECT TOP 1 @INTERVALO = DIAS   FROM @PARAMETRO_GRAFICO  
  
  INSERT INTO @SALIDA_GRAFICO  
  EXEC CSGPI.DBO.PKG_PATRIMONIO_CUENTAS$RENTABILIDAD_PERIODOS_2 NULL, @ID_CLIENTE, NULL, @FECHA_OPERATIVA, @FECHA_FINAL  
  
  SELECT @VECNOM_1 = @VECNOM_1 +   CONVERT(VARCHAR(10),FECHA_CIERRE,3)+ ';'  
       , @VECMTO_1 = @VECMTO_1 + CAST(DBO.FN_CC_INCLUIRMILESCARTOLA(RENTAB_ACUM,2) AS  VARCHAR(20)) +';'  
    FROM @SALIDA_GRAFICO ORDER BY FECHA_CIERRE ASC  
  
  SELECT @MINVECNOM_1 = MIN(RENTAB_ACUM),  @MAXVECNOM_1 = MAX(RENTAB_ACUM)  
    FROM @SALIDA_GRAFICO  
  
  IF @MINVECNOM_1>0  
     SELECT @MINVECNOM_1 =  ROUND(MIN(RENTAB_ACUM),1)-1  FROM @SALIDA_GRAFICO  
  ELSE  
     SELECT @MINVECNOM_1 =  ROUND(MIN(RENTAB_ACUM),0,1)-1  FROM @SALIDA_GRAFICO  
  
  IF @MAXVECNOM_1>0  
     SELECT @MAXVECNOM_1 = ROUND(MAX(RENTAB_ACUM),0)+1  FROM @SALIDA_GRAFICO  
  ELSE  
     SELECT @MAXVECNOM_1 = ROUND(MAX(RENTAB_ACUM),0,1)+1  FROM @SALIDA_GRAFICO  
  
  IF @VECNOM_1 !=''  
  BEGIN  
     SELECT INICIOBLOQUE = 'GraficoLine'  
          , SUBSTRING(@VECNOM_1,1,len(@VECNOM_1)-1) as VECNOM_1  
          , SUBSTRING(@VECMTO_1,1,len(@VECMTO_1)-1) as VECMTO_1  
          , @INTERVALO AS INTERVALO  
          , CAST(@MINVECNOM_1 AS INT) AS MIN_VECMTO  
          , CAST(@MAXVECNOM_1 AS INT) AS MAX_VECMTO  
  END  
  ELSE  
  BEGIN  
 SET @VECNOM_1 = ''  
 SET @VECMTO_1 = ''  
 SET @INTERVALO = 1  
 SET @MINVECNOM_1 = 0  
 SET @MAXVECNOM_1 = 0  
  
 SELECT INICIOBLOQUE = 'GraficoLine'  
         , @VECNOM_1 as Vecnom_1  
         , @VECMTO_1 as Vecmto_1  
         , @INTERVALO AS INTERVALO  
         , CAST(@MINVECNOM_1 AS int) as MIN_VECMTO  
         , CAST(@MAXVECNOM_1 AS int) as MAX_VECMTO  
  END  
  
END TRY  
BEGIN CATCH  
  SET @CodErr = @@ERROR  
  SET @MsgErr = 'Error en el Procedimiento sp_CartolaApvFlexibleWeb_temp:' + ERROR_MESSAGE()  
END CATCH  
END  
GO

GRANT EXECUTE ON [sp_CartolaApvFlexibleWeb_temp] TO DB_EXECUTESP
GO
