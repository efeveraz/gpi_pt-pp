USE [CISCB]
GO
IF  EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[sp_CartolaApvFlexibleTipoAhorroDetWeb_temp]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [DBO].[sp_CartolaApvFlexibleTipoAhorroDetWeb_temp]
GO

CREATE PROCEDURE [DBO].[sp_CartolaApvFlexibleTipoAhorroDetWeb_temp]
   @RutCliente  VARCHAR(15)
 , @FechaDesde  VARCHAR(8)
 , @FechaHasta  VARCHAR(8)
 , @CodErr      INT           OUTPUT
 , @MsgErr      VARCHAR(4000) OUTPUT
AS
BEGIN

 SET NOCOUNT ON;
 --***********************************************************************************************
 -- DECLARACION DE VARIABLES
 --***********************************************************************************************
 DECLARE @UF1                        NUMERIC(10,2)
 DECLARE @UF2                        NUMERIC(10,2)
 DECLARE @DOLAR1                     NUMERIC(10,2)
 DECLARE @DOLAR2                     NUMERIC(10,2)
 DECLARE @ID_CLIENTE                 INT
 DECLARE @ID_CUENTA                  INT
 DECLARE @NUM_CUENTA                 INT
 DECLARE @DSC_TIPOAHORRO             VARCHAR(100)
 DECLARE @DSC_CUENTA                 VARCHAR(100)
 DECLARE @ID_EMPRESA                 INT
 DECLARE @ID_MONEDA                  INT
 DECLARE @ID_TIPOCUENTA              INT
 DECLARE @CONT                       INT
 DECLARE @CANTIDAD                   INT
 DECLARE @CONT2                      INT
 DECLARE @CANTIDAD2                  INT
 DECLARE @CONT3                      INT
 DECLARE @CANTIDAD3                  INT
 DECLARE @ID_ARBOL_CLASE_INST_HOJA   INT
 DECLARE @DSC_ARBOL_CLASE_INST       VARCHAR(100)
 DECLARE @DSC_ARBOL_CLASE_INST_HOJA  VARCHAR(100)
 DECLARE @SALDO_ANTERIOR             FLOAT
 DECLARE @SALDO                      FLOAT
 DECLARE @VECNOM                     VARCHAR(MAX)
 DECLARE @VECMTO                     VARCHAR(MAX)
 DECLARE @ID_CAJA_CUENTA             INT
 DECLARE @ID_ARBOL_SEC_ECONOMICO     INT
 DECLARE @FECHA_DESDE_CAJA           VARCHAR(8)
 DECLARE @DSC_CAJA_CUENTA            VARCHAR(50)
 DECLARE @FECHA_LIQ                  VARCHAR(50)

 --***********************************************************************************************
 -- DECLARACION DE TABLAS
 --***********************************************************************************************
 DECLARE @INDICADORES TABLE (
                             ID_TIPO_CAMBIO  INT
                           , DSC_MONEDA      VARCHAR(50)
                           , VALOR           NUMERIC(18,2)
                           , DSC_TIPO_CAMBIO VARCHAR(100)
                           , ABR_TIPO_CAMBIO VARCHAR(30))

 DECLARE @INDICADORES_SALIDA TABLE (
                                    ID_TABLA     INT  IDENTITY(1,1)
                                  , DESCRIPCION  VARCHAR(30)
                                  , VALOR1       VARCHAR(30)
                                  , VALOR2       VARCHAR(30))

 DECLARE @CUENTAS TABLE (
                         ID_TABLA                 INT IDENTITY(1,1),
                         ID_CUENTA                INT,
                         ID_CONTRATO_CUENTA       INT,
                         NUM_CUENTA               INT,
                         ABR_CUENTA               VARCHAR(30),
                         DSC_CUENTA               VARCHAR(100),
                         ID_MONEDA                INT,
                         COD_MONEDA               VARCHAR(10),
                         DSC_MONEDA               VARCHAR(30),
                         FLG_ES_MONEDA_PAGO       VARCHAR(10),
                         OBSERVACION              VARCHAR(100),
                         FLG_BLOQUEADO            VARCHAR(1),
                         OBS_BLOQUEO              VARCHAR(100),
                         FLG_IMP_INSTRUCCIONES    VARCHAR(1),
                         ID_CLIENTE               INT,
                         RUT_CLIENTE              VARCHAR(15),
                         NOMBRE_CLIENTE           VARCHAR(100),
                         ID_TIPO_ESTADO           INT,
                         COD_ESTADO               VARCHAR(1),
                         DSC_ESTADO               VARCHAR(30),
                         COD_TIPO_ADMINISTRACION  VARCHAR(10),
                         DSC_TIPO_ADMINISTRACION  VARCHAR(30),
                         ID_EMPRESA               INT,
                         DSC_EMPRESA              VARCHAR(30),
                         ID_PERFIL_RIESGO         INT,
                         DSC_PERFIL_RIESGO        VARCHAR(30),
                         FECHA_OPERATIVA          DATETIME,
                         FLG_MOV_DESCUBIERTOS     VARCHAR(1),
                         ID_ASESOR                INT,
                         FECHA_CIERRE_CUENTA      DATETIME,
                         DECIMALES_CUENTA         INT,
                         SIMBOLO_MONEDA           VARCHAR(10),
                         FECHA_CONTRATO           DATETIME,
                         ID_TIPOCUENTA            INT,
                         NUMERO_FOLIO             INT,
                         TIPO_AHORRO              INT,
                         RUT_ASESOR               VARCHAR(15),
                         NOMBRE_ASESOR            VARCHAR(100),
                         FLG_CONSIDERA_COM_VC     VARCHAR(1),
                         FECHA_CIERRE_OPERATIVA   DATETIME,
                         DSC_TIPOAHORRO           VARCHAR(250))

 CREATE TABLE #ARBOL (
                      ID_TABLA                   INT    IDENTITY(1,1),
                      ID_ARBOL_CLASE_INST        INT,
                      ID_ARBOL_CLASE_INST_HOJA   INT,
                      DSC_ARBOL_CLASE_INST       VARCHAR(100),
                      DSC_ARBOL_CLASE_INST_HOJA  VARCHAR(100),
                      MONTO_MON_CTA_INST         BIGINT,
                      MONTO_MON_CTA_HOJA         BIGINT,
                      CODIGO                     VARCHAR(10),
                      NIVEL                      INT,
                      CODIGO_PADRE               VARCHAR(20))

 DECLARE   @SALDOSACTIVOS TABLE (
             ORIGEN                            VARCHAR(50)
           , ID_CUENTA                         NUMERIC
           , FECHA_CIERRE                      DATETIME
           , ID_NEMOTECNICO                    NUMERIC
           , NEMOTECNICO                       VARCHAR(50)
           , EMISOR                            VARCHAR(100)
           , COD_EMISOR                        VARCHAR(10)
           , DSC_NEMOTECNICO                   VARCHAR(120)
           , TASA_EMISION_2                    NUMERIC(18,4)
           , CANTIDAD                          NUMERIC(18,4)
           , PRECIO                            NUMERIC(18,6)
           , TASA_EMISION                      NUMERIC(18,4)
           , FECHA_VENCIMIENTO                 DATETIME
           , PRECIO_COMPRA                     NUMERIC(18,4)
           , TASA                              NUMERIC(18,4)
           , TASA_COMPRA                       NUMERIC(18,4)
           , MONTO_VALOR_COMPRA                NUMERIC(18,4)
           , MONTO_MON_CTA                     NUMERIC(18,4)
           , ID_MONEDA_CTA                     NUMERIC
           , ID_MONEDA_NEMOTECNICO             NUMERIC
           , SIMBOLO_MONEDA                    VARCHAR(3)
           , MONTO_MON_NEMOTECNICO             NUMERIC(18,4)
           , MONTO_MON_ORIGEN                  NUMERIC(18,4)
           , ID_EMPRESA                        NUMERIC
           , ID_ARBOL_CLASE_INST               NUMERIC
           , COD_INSTRUMENTO                   VARCHAR(15)
           , DSC_ARBOL_CLASE_INST              VARCHAR(100)
           , PORCENTAJE_RAMA                   NUMERIC(18,4)
           , PRECIO_PROMEDIO_COMPRA            NUMERIC(18,4)
           , MONTO_PROMEDIO_COMPRA             NUMERIC(18,4)
           , DSC_PADRE_ARBOL_CLASE_INST        VARCHAR(100)
           , RENTABILIDAD                      FLOAT
           , DIAS                              NUMERIC
           , COD_PRODUCTO                      VARCHAR(10)
           , ID_PADRE_ARBOL_CLASE_INST         NUMERIC
           , DURACION                          NUMERIC(18,6)
           , DECIMALES_MOSTRAR                 NUMERIC
           , DSC_CLASIFICADOR_RIESGO           VARCHAR(50)
           , ID_SUBFAMILIA                     NUMERIC
           , COD_SUBFAMILIA                    VARCHAR(50)
           , CODIGO                            VARCHAR(20)
           , MONTO_INVERTIDO                   NUMERIC(18,6)
           , VALOR_MERCADO                     NUMERIC(18,6)
           , UTILIDAD_PERDIDA                  NUMERIC(18,6)
           , NUMERO_CONTRATO                   VARCHAR(50)
           , TIPO_FWD                          VARCHAR(10)
           , MONEDA_EMISION                    VARCHAR(10)
           , MODALIDAD                         VARCHAR(15)
           , UNIDAD_ACTIVO_SUBYACENTE          VARCHAR(10)
           , DECIMALES_ACTIVO_SUBYACENTE       NUMERIC
           , VALOR_NOMINAL                     NUMERIC(18,4)
           , FECHA_INICIO                      DATETIME
           , PRECIO_PACTADO                    NUMERIC(18,6)
           , MONTO_MON_USD                     NUMERIC(18,6)
           , VALOR_PRECIO_PACTADO              NUMERIC(18,4)
        )

 DECLARE @INSTRUMENTOS TABLE (
                              ID_TABLA             INT    IDENTITY(1,1),
                              TIPOINSTRUMENTO      VARCHAR(50),
                              SUBTIPO              VARCHAR(50),
                              NUMCUENTA            INT,
                              DSCCUENTA            VARCHAR(100),
                              NEMOTECNICO          VARCHAR(50),
                              EMISOR               VARCHAR(50),
                              MONEDA               VARCHAR(10),
                              PORCENTAJEINVERSION  NUMERIC(6,2),
                              CANTIDAD             NUMERIC(18,4),
                              PRECIOCOMPRA         NUMERIC(18,4),
                              MONTOCOMPRA          BIGINT,
                              PRECIOACTUAL         NUMERIC(18,4),
                              VALORMERCADO         BIGINT,
                              RENTABILIDAD         NUMERIC(6,2),
                              TASACUPON            NUMERIC(18,4),
                              FECHAVENCIMIENTO     VARCHAR(8),
                              TASACOMPRA           NUMERIC(6,2),
                              TASAMERCADO          NUMERIC(6,2),
                              VALORIZACION         BIGINT,
                              DURATION             NUMERIC (18,6),
                              UTILIDADPERDIDA      NUMERIC(18,4),
                              VALORDOLAR           NUMERIC(18,4),
                              VALORPESO            NUMERIC(18,4))

 DECLARE @RVSECTOR TABLE (
                          ID_TABLA       INT    IDENTITY(1,1),
                          NUMCUENTA      INT,
                          ID_SECTOR      INT,
                          DSC_SECTOR     VARCHAR(50),
                          MONTO_MON_CTA  BIGINT,
                          PORC_SECTOR    NUMERIC(10,6))

 DECLARE @GRAFICOS TABLE (
                          ID_TABLA   INT    IDENTITY(1,1),
                          NUMCUENTA  INT,
                          VECNOM     VARCHAR(MAX),
                          VECMTO     VARCHAR(MAX))

 DECLARE @CAJASCUENTAS TABLE (
                              ID_TABLA         INT    IDENTITY(1,1),
                              ID_CAJA_CUENTA   INT,
                              DSC_CAJA_CUENTA  VARCHAR(30),
                              ID_CUENTA        INT,
                              NUM_CUENTA       VARCHAR(10),
                              ID_MONEDA        INT,
                              COD_MONEDA       VARCHAR(10),
                              DSC_MONEDA       VARCHAR(30),
                              COD_MERCADO      VARCHAR(1),
                              DESC_MERCADO     VARCHAR(30),
                              DECIMALES        INT )

 DECLARE @DESCCUENTAS TABLE (
                             ID_TABLA         INT    IDENTITY(1,1),
                             ID_CAJA_CUENTA   INT,
                             DSC_CAJA_CUENTA  VARCHAR(30),
                             ID_CUENTA        INT,
                             NUM_CUENTA       VARCHAR(10))

 DECLARE @INFORMESCAJAS TABLE (
                               ID_TABLA           INT    IDENTITY(1,1),
                               ID_CUENTA          INT,
                               NUM_CUENTA         INT,
                               DSC_CUENTA         VARCHAR(100),
                               TIPO_CAJA          VARCHAR(50),
                               ID_CIERRE          INT,
                               ID_OPERACION       INT,
                               FECHA_LIQUIDACION  DATETIME,
                               DESCRIPCION        VARCHAR(100),
                               DESCRIPCION_CAJA   VARCHAR(100),
                               MONEDA             VARCHAR(10),
                               MONTO_MOVTO_ABONO  NUMERIC(18,4),
                               MONTO_MOVTO_CARGO  NUMERIC(18,4),
                               SALDO              NUMERIC(18,4))

 DECLARE @INFORMETRANSACCIONES TABLE (
             FECHA_OPERACION        DATETIME
           , FECHA_CORTE            DATETIME
           , TIPO_MOVIMIENTO        VARCHAR(MAX)
           , DSC_TIPO_MOVIMIENTO    VARCHAR(MAX)
           , NUM_CUENTA             VARCHAR(MAX)
           , ID_OPERACION           INT
           , FACTURA                INT
           , NEMOTECNICO            VARCHAR(MAX)
           , CANTIDAD               NUMERIC(18,4)
           , MONEDA                 VARCHAR(MAX)
           , DECIMALES_MOSTRAR      NUMERIC(2,0)
           , PRECIO                 NUMERIC(18,10)
           , COMISION               NUMERIC(18,4)
           , DERECHOS               NUMERIC(18,4)
           , GASTOS                 NUMERIC(18,4)
           , IVA                    NUMERIC(18,4)
           , MONTO                  NUMERIC(18,4)
           , MONTO_TOTAL_OPERACION  NUMERIC(18,4)
           , DSC_TIPO_OPERACION     VARCHAR(MAX)
           , CONTRAPARTE            VARCHAR(100))

 CREATE TABLE #INFORMETRANSACCIONESFINAL(
              FECHA_OPERACION        DATETIME
            , FECHA_CORTE            DATETIME
            , TIPO_MOVIMIENTO        VARCHAR(MAX)
            , DSC_TIPO_MOVIMIENTO    VARCHAR(MAX)
            , NUM_CUENTA             VARCHAR(MAX)
            , ID_OPERACION           INT
            , FACTURA                INT
            , NEMOTECNICO            VARCHAR(MAX)
            , CANTIDAD               NUMERIC(18,4)
            , MONEDA                 VARCHAR(MAX)
            , DECIMALES_MOSTRAR      NUMERIC(2,0)
            , PRECIO                 NUMERIC(18,10)
            , COMISION               NUMERIC(18,4)
            , DERECHOS               NUMERIC(18,4)
            , GASTOS                 NUMERIC(18,4)
            , IVA                    NUMERIC(18,4)
            , MONTO                  NUMERIC(18,4)
            , MONTO_TOTAL_OPERACION  NUMERIC(18,4)
            , DSC_TIPO_OPERACION     VARCHAR(MAX)
            , DSC_CUENTA             VARCHAR(MAX)
            , CONTRAPARTE            VARCHAR(100))

 DECLARE @TMP TABLE (
                     ID_ARBOL_CLASE_INST        NUMERIC,
                     ID_ARBOL_CLASE_INST_HOJA   NUMERIC,
                     DSC_ARBOL_CLASE_INST       VARCHAR(100),
                     DSC_ARBOL_CLASE_INST_HOJA  VARCHAR(100),
                     MONTO_MON_CTA_INST         NUMERIC(18,2),
                     MONTO_MON_CTA_HOJA         NUMERIC(18,2),
                     CODIGO                     VARCHAR(20),
                     NIVEL                      INT,
                     CODIGO_PADRE               VARCHAR(20))

 BEGIN TRY

  SET @CodErr = 0
  SET @MsgErr = ''

  --***********************************************************************************************
  -- INDICADORES
  --***********************************************************************************************

  INSERT INTO @INDICADORES EXEC CSGPI.DBO.PKG_VALOR_TIPO_CAMBIO$BUSCARINDICADORES @FECHAHASTA
  INSERT INTO @INDICADORES_SALIDA
  SELECT 'Valores al', @FECHAHASTA, @FECHADESDE

  SELECT @DOLAR1 = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 1
  SELECT @UF1 = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 2

  INSERT INTO @INDICADORES EXEC CSGPI.dbo.PKG_VALOR_TIPO_CAMBIO$BuscarIndicadores @FechaDesde

  SELECT @DOLAR2 = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 1
  SELECT @UF2 = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 2

  INSERT INTO @INDICADORES_SALIDA
  SELECT 'UF', @UF1, @UF2

  INSERT INTO @INDICADORES_SALIDA
  SELECT 'Dolar OBS.', @DOLAR1, @DOLAR2

  --***********************************************************************************************
  -- CUENTAS APV
  --***********************************************************************************************

  SELECT TOP 1 @ID_CLIENTE = id_cliente
    FROM CSGPI.DBO.VIEW_CUENTAS_VIGENTES
   WHERE RUT_CLIENTE = @RUTCLIENTE

  INSERT INTO @CUENTAS EXEC CSGPI.DBO.PKG_CUENTAS$BUSCARCUENTASAPV @ID_CLIENTE, NULL

--SELECT  '@CUENTAS', * FROM @CUENTAS

  SET @CANTIDAD = ISNULL((SELECT COUNT(ID_TABLA) FROM @CUENTAS), 0)
  SET @CONT = 1

  WHILE @CONT <= @CANTIDAD
  BEGIN

   -- LIMPIA TABLAS
   TRUNCATE TABLE #Arbol

   -- OBTIENE IDENTIFICADORES
   SELECT @ID_CUENTA = ID_CUENTA,
          @NUM_CUENTA = NUM_CUENTA,
          @ID_EMPRESA = ID_EMPRESA,
          @ID_MONEDA = ID_MONEDA,
          @ID_TIPOCUENTA = ID_TIPOCUENTA,
          @DSC_TIPOAHORRO = DSC_TIPOAHORRO
     FROM @CUENTAS
    WHERE ID_TABLA = @CONT

   SET @DSC_CUENTA = 'Cuenta: ' + right('0000' +  CAST(@NUM_CUENTA AS VARCHAR(10)),4) + ' - ' + @DSC_TIPOAHORRO

   INSERT INTO #ARBOL
   --EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA @ID_CUENTA,@ID_EMPRESA,@FechaHasta,NULL,NULL,NULL
   EXEC CSGPI.dbo.PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA_CONSOLIDADO_II @ID_CUENTA,@ID_EMPRESA,@FechaHasta,NULL,NULL,@ID_MONEDA,'CTA'

--select '#ARBOL', * from #ARBOL

   SET @CANTIDAD2 = ISNULL((SELECT COUNT(ID_TABLA) FROM #Arbol), 0)
   SET @CONT2 = 1

   WHILE @CONT2 <= @CANTIDAD2
   BEGIN

    SELECT @ID_ARBOL_CLASE_INST_HOJA = ID_ARBOL_CLASE_INST_HOJA
         , @DSC_ARBOL_CLASE_INST = DSC_ARBOL_CLASE_INST
         , @DSC_ARBOL_CLASE_INST_HOJA = DSC_ARBOL_CLASE_INST_HOJA
      FROM #Arbol
     WHERE ID_TABLA = @CONT2

    DELETE FROM @SALDOSACTIVOS

    INSERT INTO @SALDOSACTIVOS
    EXEC CSGPI.DBO.PKG_SALDOS_ACTIVOS_WEB$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO @ID_CUENTA,@FECHAHASTA,@ID_ARBOL_CLASE_INST_HOJA,@ID_MONEDA,NULL,NULL,NULL

    --***********************************************************************************************
    -- RENTA VARIABLE NACIONAL
    --***********************************************************************************************

    INSERT INTO @INSTRUMENTOS (
           TIPOINSTRUMENTO,
           SUBTIPO,
           NUMCUENTA,
           DSCCUENTA,
           NEMOTECNICO,
           MONEDA,
           PORCENTAJEINVERSION,
           CANTIDAD,
           PRECIOCOMPRA,
           MONTOCOMPRA,
           PRECIOACTUAL,
           VALORMERCADO,
           RENTABILIDAD
            )
    SELECT
      'RENTA VARIABLE',
      DSC_ARBOL_CLASE_INST,
      @NUM_CUENTA,
      @DSC_CUENTA,
      NEMOTECNICO,
      SIMBOLO_MONEDA,
      PORCENTAJE_RAMA,
      CANTIDAD,
      PRECIO_PROMEDIO_COMPRA,
      ROUND(MONTO_PROMEDIO_COMPRA,0),
      PRECIO,
      MONTO_MON_CTA,
      RENTABILIDAD
       FROM @SALDOSACTIVOS
      WHERE DSC_PADRE_ARBOL_CLASE_INST = 'Renta Variable Nacional'

    INSERT INTO @INSTRUMENTOS (
             TIPOINSTRUMENTO,
             SUBTIPO,
             NUMCUENTA,
             DSCCUENTA,
             PORCENTAJEINVERSION,
             MONTOCOMPRA,
             VALORMERCADO
            )
    SELECT
      'RENTA VARIABLE',
      DSC_ARBOL_CLASE_INST,
      @NUM_CUENTA,
      @DSC_CUENTA,
      100,
      SUM(ROUND(MONTO_PROMEDIO_COMPRA,0)),
      SUM(MONTO_MON_CTA)
       FROM @SALDOSACTIVOS
      WHERE DSC_PADRE_ARBOL_CLASE_INST = 'Renta Variable Nacional'
      GROUP BY DSC_ARBOL_CLASE_INST

    --***********************************************************************************************
    -- RENTA VARIABLE POR SECTORES ECONOMICOS
    --***********************************************************************************************

    IF @DSC_ARBOL_CLASE_INST = 'Renta Variable Nacional' AND @DSC_ARBOL_CLASE_INST_HOJA = 'Acciones'
    BEGIN

     INSERT INTO @TMP
     EXEC CSGPI.DBO.PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA @ID_CUENTA, @ID_EMPRESA, @FECHAHASTA

     SELECT @ID_ARBOL_SEC_ECONOMICO = ID_ARBOL_CLASE_INST_HOJA
       FROM @TMP
      WHERE DSC_ARBOL_CLASE_INST = 'Renta Variable Nacional' AND DSC_ARBOL_CLASE_INST_HOJA = 'Acciones'

     INSERT INTO @RVSECTOR (ID_SECTOR, DSC_SECTOR, MONTO_MON_CTA, PORC_SECTOR)
     EXEC CSGPI.DBO.PKG_SALDOS_ACTIVOS$BUSCAR_RV_SECTOR @ID_CUENTA,@FECHAHASTA,'39',@ID_MONEDA

     UPDATE @RVSECTOR
        SET NUMCUENTA = @NUM_CUENTA
      WHERE NUMCUENTA IS NULL

     -- Genera parametros del gr�fico
     SET @VECNOM = ''
     SET @VECMTO = ''
     SET @CANTIDAD3 = ISNULL((SELECT COUNT(ID_TABLA) FROM @RvSector),0)
     SET @CONT3 = ISNULL((SELECT TOP 1 ID_TABLA FROM @RvSector WHERE NumCuenta = @NUM_CUENTA ORDER BY ID_TABLA),0)

     WHILE @CONT3 <= @CANTIDAD3
     BEGIN
      SET @VECNOM = @VECNOM + ISNULL((SELECT DSC_SECTOR FROM @RVSECTOR WHERE ID_TABLA = @CONT3),'') + ' ' +  REPLACE(ISNULL(CAST(CAST(((SELECT PORC_SECTOR FROM @RVSECTOR WHERE ID_TABLA = @CONT3)*100)AS DECIMAL(6,2)) AS VARCHAR(MAX)),''),'.',',')  + '%;'
      SET @VECMTO = @VECMTO + REPLACE(ISNULL(CAST(((SELECT PORC_SECTOR FROM @RVSECTOR WHERE ID_TABLA = @CONT3)*100) AS VARCHAR(MAX)),''),'.',',') + ';'
      SET @CONT3 = @CONT3 + 1
     END

     -- ELIMINA EL �LTIMO ;
     IF LEN(@VECNOM) > 0
     BEGIN
      SET @VECNOM = SUBSTRING(@VECNOM,1,LEN(@VECNOM)-1)
      SET @VECMTO = SUBSTRING(@VECMTO,1,LEN(@VECMTO)-1)
     END

     INSERT INTO @GRAFICOS (NUMCUENTA, VECNOM, VECMTO)
     SELECT @NUM_CUENTA, @VECNOM, @VECMTO

     -- INSERTA TOTAL
     IF EXISTS(SELECT * FROM @RVSECTOR)
     BEGIN
      INSERT INTO @RVSECTOR (NUMCUENTA, DSC_SECTOR, MONTO_MON_CTA)
      SELECT @NUM_CUENTA, 'Total',SUM(MONTO_MON_CTA)
      FROM @RVSECTOR
      WHERE NUMCUENTA = @NUM_CUENTA
     END
    END

    --***********************************************************************************************
    -- RENTA FIJA NACIONAL
    --***********************************************************************************************

    INSERT INTO @INSTRUMENTOS (
             TIPOINSTRUMENTO,
             SUBTIPO,
             NUMCUENTA,
             DSCCUENTA,
             NEMOTECNICO,
             EMISOR,
             MONEDA,
             PORCENTAJEINVERSION,
             CANTIDAD,
             TASACUPON,
             FECHAVENCIMIENTO,
             TASACOMPRA,
             TASAMERCADO,
             VALORIZACION,
             DURATION)
    SELECT
        'RENTA FIJA',
        DSC_ARBOL_CLASE_INST,
        @NUM_CUENTA,
        @DSC_CUENTA,
        NEMOTECNICO,
        COD_EMISOR,
        SIMBOLO_MONEDA,
        CAST(ROUND(PORCENTAJE_RAMA,2) AS NUMERIC(6,2)),
        CANTIDAD,
        TASA_EMISION,
        CONVERT(VARCHAR(8),FECHA_VENCIMIENTO,112),
        CAST(TASA_COMPRA AS NUMERIC(6,2)),
        CAST(TASA AS NUMERIC(6,2)),
        CAST(MONTO_MON_CTA AS BIGINT),
        DURACION
      FROM @SALDOSACTIVOS
     WHERE DSC_PADRE_ARBOL_CLASE_INST = 'Renta Fija Nacional'

    INSERT INTO @INSTRUMENTOS (
             TIPOINSTRUMENTO,
             SUBTIPO,
             NUMCUENTA,
             DSCCUENTA,
             PORCENTAJEINVERSION,
             MONTOCOMPRA,
             VALORMERCADO)
    SELECT
        'RENTA FIJA',
        DSC_ARBOL_CLASE_INST,
        @NUM_CUENTA,
        @DSC_CUENTA,
        100,
        SUM(CAST(MONTO_PROMEDIO_COMPRA AS BIGINT)),
        SUM(CAST(MONTO_MON_CTA AS BIGINT))
      FROM @SALDOSACTIVOS
     WHERE DSC_PADRE_ARBOL_CLASE_INST = 'Renta Fija Nacional'
     GROUP BY DSC_ARBOL_CLASE_INST

    ----***********************************************************************************************
    ---- RENTA FIJA INTERNACIONAL
    ----***********************************************************************************************

    INSERT INTO @INSTRUMENTOS (
           TIPOINSTRUMENTO,
           SUBTIPO,
           NUMCUENTA,
           DSCCUENTA,
           NEMOTECNICO,
           MONEDA,
           CANTIDAD,
           PRECIOCOMPRA,
           MONTOCOMPRA,
           PRECIOACTUAL,
           VALORMERCADO,
           UTILIDADPERDIDA,
                                            VALORDOLAR,
                                            VALORPESO
            )
    SELECT
      'RENTA FIJA INT',
      DSC_ARBOL_CLASE_INST,
      @NUM_CUENTA,
      @DSC_CUENTA,
      NEMOTECNICO,
      SIMBOLO_MONEDA,
      CANTIDAD,
      PRECIO_PROMEDIO_COMPRA,
      MONTO_VALOR_COMPRA,  --ROUND(MONTO_PROMEDIO_COMPRA,0),
      PRECIO,
      MONTO_MON_NEMOTECNICO,  --MONTO_MON_CTA,
      MONTO_MON_CTA - MONTO_PROMEDIO_COMPRA,  --MONTO_MON_CTA - MONTO_PROMEDIO_COMPRA,
      CSGPI.DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(1, MONTO_MON_CTA, ID_MONEDA_CTA, 2, fecha_cierre),
      MONTO_MON_CTA
       FROM @SALDOSACTIVOS
      WHERE DSC_PADRE_ARBOL_CLASE_INST = 'Renta Fija Internacional'

    INSERT INTO @INSTRUMENTOS (
             TIPOINSTRUMENTO,
             SUBTIPO,
             NUMCUENTA,
             DSCCUENTA,
             VALORDOLAR,
                                              VALORPESO
            )
    SELECT
      'RENTA FIJA INT',
      DSC_ARBOL_CLASE_INST,
      @NUM_CUENTA,
      @DSC_CUENTA,
      SUM(CSGPI.DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(1, MONTO_MON_CTA, ID_MONEDA_CTA, 2, FECHA_CIERRE)),
                        SUM(MONTO_MON_CTA)
       FROM @SALDOSACTIVOS
      WHERE DSC_PADRE_ARBOL_CLASE_INST = 'Renta Fija Internacional'
      GROUP BY DSC_ARBOL_CLASE_INST

    ----***********************************************************************************************
    ---- RENTA VARIABLE INTERNACIONAL
    ----***********************************************************************************************

    INSERT INTO @INSTRUMENTOS (
           TIPOINSTRUMENTO,
           SUBTIPO,
           NUMCUENTA,
           DSCCUENTA,
           NEMOTECNICO,
           MONEDA,
           CANTIDAD,
           PRECIOCOMPRA,
           MONTOCOMPRA,
           PRECIOACTUAL,
           VALORMERCADO,
           UTILIDADPERDIDA,
                                            VALORDOLAR,
                                            VALORPESO
            )
    SELECT
      'RENTA VARIABLE INT',
      DSC_ARBOL_CLASE_INST,
      @NUM_CUENTA,
      @DSC_CUENTA,
      NEMOTECNICO,
      SIMBOLO_MONEDA,
      CANTIDAD,
      PRECIO_PROMEDIO_COMPRA,
      MONTO_VALOR_COMPRA,  --ROUND(MONTO_PROMEDIO_COMPRA,0),
      PRECIO,
      MONTO_MON_NEMOTECNICO,  --MONTO_MON_CTA,
      MONTO_MON_NEMOTECNICO - MONTO_VALOR_COMPRA,  --MONTO_MON_CTA - MONTO_PROMEDIO_COMPRA,
      CSGPI.DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(1, MONTO_MON_CTA, ID_MONEDA_CTA, 2, fecha_cierre),
      MONTO_MON_CTA
       FROM @SALDOSACTIVOS
      WHERE DSC_PADRE_ARBOL_CLASE_INST = 'Renta Variable Internacional'

    INSERT INTO @INSTRUMENTOS (
             TIPOINSTRUMENTO,
             SUBTIPO,
             NUMCUENTA,
             DSCCUENTA,
             VALORDOLAR,
             VALORPESO
            )
    SELECT
      'RENTA VARIABLE INT',
      DSC_ARBOL_CLASE_INST,
      @NUM_CUENTA,
      @DSC_CUENTA,
      SUM(CSGPI.DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(1, MONTO_MON_CTA, ID_MONEDA_CTA, 2, FECHA_CIERRE)),
      SUM(MONTO_MON_CTA)
       FROM @SALDOSACTIVOS
      WHERE DSC_PADRE_ARBOL_CLASE_INST = ' l'
      GROUP BY DSC_ARBOL_CLASE_INST

    SET @CONT2 = @CONT2 + 1
   END

   --***********************************************************************************************
   -- INFORMES DE CAJA
   --***********************************************************************************************

   INSERT INTO @CAJASCUENTAS
   EXEC CSGPI.DBO.PKG_CAJAS_CUENTA$BUSCARVIEW NULL, @ID_CUENTA

   SET @CANTIDAD2 = ISNULL((SELECT COUNT(*) FROM @CAJASCUENTAS),0)
   SET @CONT2 = 1
   SET @CONT2 = ISNULL((SELECT MIN(ID_TABLA) FROM @CAJASCUENTAS WHERE ID_CUENTA=@ID_CUENTA),0)

   WHILE @CONT2 <= @CANTIDAD2
   BEGIN

    SET @ID_CAJA_CUENTA = (SELECT ID_CAJA_CUENTA FROM @CAJASCUENTAS WHERE ID_TABLA = @CONT2)

    SET @DSC_CAJA_CUENTA = (SELECT DSC_CAJA_CUENTA  FROM @CAJASCUENTAS WHERE ID_TABLA = @CONT2)

    SELECT @FECHA_DESDE_CAJA = CONVERT(VARCHAR(8), DATEADD(DAY,1,@FECHADESDE), 112)

    EXEC CSGPI.DBO.PKG_SALDOS_CAJA$BUSCAR_SALDOS_CUENTA @PSALDO=@SALDO OUTPUT,@PID_CUENTA=@ID_CUENTA,@PID_CAJA_CUENTA=@ID_CAJA_CUENTA,@PFECHA_CIERRE=@FECHA_DESDE_CAJA--@FECHADESDE

    SET @SALDO = ISNULL(@SALDO, 0)

    INSERT INTO @INFORMESCAJAS (
             FECHA_LIQUIDACION,
             DESCRIPCION,
             SALDO)
    VALUES( CONVERT(DATETIME, @FECHADESDE, 112), 'SALDO ANTERIOR', @SALDO)

    INSERT INTO @InformesCajas (ID_CIERRE, ID_OPERACION, FECHA_LIQUIDACION, DESCRIPCION, DESCRIPCION_CAJA, MONEDA, MONTO_MOVTO_ABONO, MONTO_MOVTO_CARGO)
    EXEC CSGPI.DBO.PKG_MOVIMIENTOS_CAJA_SECURITY @ID_CAJA_CUENTA,@FECHA_DESDE_CAJA,@FECHAHASTA

    SET @FECHA_LIQ = (SELECT TOP 1 FECHA_LIQUIDACION FROM @INFORMESCAJAS ORDER BY ID_TABLA DESC)
    SET @FECHA_LIQ = CONVERT(VARCHAR(8), DATEADD(DAY,1,@FECHA_LIQ), 112)

    EXEC CSGPI.DBO.PKG_SALDOS_CAJA$BUSCAR_SALDOS_CUENTA @PSALDO          = @SALDO OUTPUT,
                 @PID_CUENTA      = @ID_CUENTA,
                 @PID_CAJA_CUENTA = @ID_CAJA_CUENTA,
                 @PFECHA_CIERRE   = @FECHA_LIQ
    SET @SALDO_ANTERIOR =  @SALDO

    UPDATE @INFORMESCAJAS
       SET SALDO = @SALDO_ANTERIOR,
        TIPO_CAJA = @DSC_CAJA_CUENTA
     WHERE SALDO IS NULL

    EXEC CSGPI.DBO.PKG_SALDOS_CAJA$BUSCAR_SALDOS_CUENTA @PSALDO=@SALDO OUTPUT,@PID_CUENTA=@ID_CUENTA,@PID_CAJA_CUENTA=@ID_CAJA_CUENTA,@PFECHA_CIERRE=@FECHAHASTA--@FECHADESDE

    SET @SALDO = ISNULL(@SALDO, 0)

    UPDATE @InformesCajas
       SET ID_CUENTA = @ID_CUENTA
      , NUM_CUENTA = @NUM_CUENTA
      , DSC_CUENTA = @DSC_CUENTA
      , TIPO_CAJA= @DSC_CAJA_CUENTA
     WHERE ID_CUENTA IS NULL

    INSERT INTO @INFORMESCAJAS (
             ID_CUENTA,
             NUM_CUENTA,
             DSC_CUENTA,
             DESCRIPCION,
             MONTO_MOVTO_ABONO,
             MONTO_MOVTO_CARGO,
             SALDO,
             TIPO_CAJA )
    SELECT
        @ID_CUENTA,
        @NUM_CUENTA,
        @DSC_CUENTA,
        'TOTALES',
        SUM(MONTO_MOVTO_ABONO),
        SUM(MONTO_MOVTO_CARGO),
        @SALDO,
        @DSC_CAJA_CUENTA
      FROM @INFORMESCAJAS
     WHERE ID_CUENTA = @ID_CUENTA

    SET @CONT2 = @CONT2 + 1
   END

   SET @CONT = @CONT + 1
  END

  --***********************************************************************************************
  -- IMPRIME RESULTADOS
  --***********************************************************************************************

  SELECT 'Indicadores'       AS INICIOBLOQUE
          , DESCRIPCION         AS Descripcion
          , ISNULL(VALOR1, '')  AS Valor1
          , ISNULL(VALOR2, '')  AS Valor2
       FROM @INDICADORES_SALIDA

  SELECT 'Instrumento'  AS INICIOBLOQUE,
    NumCuenta,
    DscCuenta,
    TipoInstrumento,
    SubTipo,
    Nemotecnico = ISNULL(NEMOTECNICO,''),
    Emisor = ISNULL(EMISOR,''),
    Moneda = ISNULL(MONEDA,''),
    PorcentajeInversion,
    Cantidad,
    PrecioCompra,
    MontoCompra,
    PrecioActual,
    ValorMercado,
    Rentabilidad,
    TasaCupon,
    FechaVencimiento,
    TasaCompra,
    TasaMercado,
    Valorizacion,
    DURATION AS duracion,
    UTILIDADPERDIDA,
                VALORDOLAR,
                VALORPESO
     FROM @INSTRUMENTOS
    ORDER BY NUMCUENTA, TIPOINSTRUMENTO DESC, SUBTIPO, ID_TABLA

  SELECT  'SectoresEconomicos' AS INICIOBLOQUE,
    NUMCUENTA            AS NumCuenta,
    DSC_SECTOR           AS SectoresEconomicos,
    MONTO_MON_CTA        AS Monto
     FROM @RVSECTOR

  SELECT  'Graficos'     AS INICIOBLOQUE,
    NumCuenta,
    VECNOM,
    VECMTO
     FROM @GRAFICOS

  SELECT  'InformesCajas'           AS INICIOBLOQUE,
    NUM_CUENTA            AS NumCuenta,
    DSC_CUENTA            AS DscCuenta,
    TIPO_CAJA            AS TipoCaja,
    ID_OPERACION           AS NumeroOperacion,
    ISNULL(CONVERT(VARCHAR(8), FECHA_LIQUIDACION, 112), '') AS FechaLiquidacion,
    DESCRIPCION            AS Detalle,
    CAST(MONTO_MOVTO_ABONO AS NUMERIC(18,4))      AS Ingreso,
    CAST(MONTO_MOVTO_CARGO AS NUMERIC(18,4))      AS Egreso,
    CAST(SALDO AS NUMERIC(18,4))         AS Saldo
     FROM @INFORMESCAJAS
    ORDER BY ID_TABLA

  --***********************************************************************************************
  -- INFORME DE TRANSACCIONES
  --***********************************************************************************************
  INSERT INTO @InformeTransacciones
  EXEC CSGPI.DBO.PKG_CARTOLA$DETALLEMOVIMIENTOS
               @PCONSOLIDADO  = 'CLT'
             , @PID_ENTIDAD   = @ID_CLIENTE
             , @PFECHA_INICIO = @FECHADESDE
             , @PFECHA_FIN    = @FECHAHASTA
             , @PID_EMPRESA   = @ID_EMPRESA

  INSERT INTO #InformeTransaccionesFinal (
               FECHA_OPERACION
             , FECHA_CORTE
             , TIPO_MOVIMIENTO
             , DSC_TIPO_MOVIMIENTO
             , NUM_CUENTA
             , ID_OPERACION
             , FACTURA
             , NEMOTECNICO
             , CANTIDAD
             , MONEDA
             , DECIMALES_MOSTRAR
             , PRECIO
             , COMISION
             , DERECHOS
             , GASTOS
             , IVA
             , MONTO
             , MONTO_TOTAL_OPERACION
             , DSC_TIPO_OPERACION)
  SELECT
      FECHA_OPERACION
    , FECHA_CORTE
    , TIPO_MOVIMIENTO
    , DSC_TIPO_MOVIMIENTO
    , NUM_CUENTA
    , ID_OPERACION
    , FACTURA
    , NEMOTECNICO
    , CANTIDAD
    , MONEDA
    , DECIMALES_MOSTRAR
    , PRECIO
    , COMISION
    , DERECHOS
    , GASTOS
    , IVA
    , MONTO
    , MONTO_TOTAL_OPERACION
    , DSC_TIPO_OPERACION
    FROM @INFORMETRANSACCIONES

  UPDATE #InformeTransaccionesFinal
     SET DSC_CUENTA = i.DSC_CUENTA
    FROM (SELECT NUM_CUENTA, DSC_CUENTA FROM @InformesCajas) i
   WHERE i.NUM_CUENTA = #InformeTransaccionesFinal.NUM_CUENTA

  SELECT
    'InformeTransacciones' AS INICIOBLOQUE
    , DSC_CUENTA AS DescripcionCaja
    , ISNULL(CONVERT(VARCHAR(8), FECHA_OPERACION, 112), '') AS FechaOperacion
    , ISNULL(CONVERT(VARCHAR(8), FECHA_CORTE, 112), '') AS FechaCorte
    , TIPO_MOVIMIENTO AS TIPOMOVIMIENTO
    , DSC_TIPO_MOVIMIENTO AS DscTipoMovimiento
    , NUM_CUENTA AS NumCuenta
    , ID_OPERACION AS IdOperacion
    , FACTURA AS Factura
    , NEMOTECNICO AS Nemotecnico
    , CANTIDAD AS Cantidad
    , MONEDA AS Moneda
    , DECIMALES_MOSTRAR AS DECIMALESMOSTRAR
    , PRECIO AS Precio
    , COMISION AS Comision
    , DERECHOS AS Derecho
    , GASTOS AS Gastos
    , IVA AS Iva
    , MONTO AS Monto
    , MONTO_TOTAL_OPERACION  AS MONTOTOTALOPERACION
    , DSC_TIPO_OPERACION AS DSCTIPOOPERACION
    , CONTRAPARTE
    FROM #InformeTransaccionesFinal
   ORDER BY NUM_CUENTA, FECHA_OPERACION
  ----***********************************************************************************************

  DROP TABLE #Arbol
  DROP TABLE #InformeTransaccionesFinal

 END TRY
 BEGIN CATCH
  SET @CodErr = @@ERROR
  SET @MsgErr = 'Error en el Procedimiento sp_CartolaApvFlexibleTipoAhorroDetWeb_temp:' + ERROR_MESSAGE()
 END CATCH
END
GO

GRANT EXECUTE ON [sp_CartolaApvFlexibleTipoAhorroDetWeb_temp] TO DB_EXECUTESP
GO
