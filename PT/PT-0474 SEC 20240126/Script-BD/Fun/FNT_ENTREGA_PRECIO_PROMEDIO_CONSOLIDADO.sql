IF EXISTS (SELECT 1 FROM SYSOBJECTS WHERE NAME = 'FNT_ENTREGA_PRECIO_PROMEDIO_CONSOLIDADO')
	DROP FUNCTION FNT_ENTREGA_PRECIO_PROMEDIO_CONSOLIDADO
GO  

CREATE FUNCTION FNT_ENTREGA_PRECIO_PROMEDIO_CONSOLIDADO  
( @PFECHA           DATETIME  
, @PCONSOLIDADO     VARCHAR(3)  
, @PID_ENTIDAD      NUMERIC(10)  
, @PID_NEMOTECNICO  NUMERIC(10))    
 RETURNS NUMERIC(18,04)    
AS    
BEGIN    
     DECLARE @TIPO_MOVIMIENTO CHAR(01),    
             @CANTIDAD        NUMERIC(18,04),    
             @SALDO           NUMERIC(18,04),    
             @SALDO_PESOS     NUMERIC(18,04),    
             @MONTO           NUMERIC(18,04),    
             @PRECIO          NUMERIC(18,04),    
             @PRECIO_PROMEDIO NUMERIC(18,04),    
             @PRIMER_REGISTRO INT,    
             @LID_NEMOTECNICO NUMERIC,    
             @PFECHA_MOV      DATETIME,
             @FECHA_OPERACION DATETIME,
             @ID_MONEDA       NUMERIC(2),  
             @ID_MONEDANEMO   NUMERIC(2),  
             @ID_MONEDA_OPE   NUMERIC(2),
             @VALORCAMBIO     NUMERIC(18,4),
             @LID_MONEDA      NUMERIC(2),  
             @LID_CUENTA      NUMERIC
             
    SELECT @ID_MONEDANEMO = ID_MONEDA FROM NEMOTECNICOS WHERE ID_NEMOTECNICO = @PID_NEMOTECNICO  
    
    DECLARE @TBLCUENTAS TABLE (ID_CUENTA NUMERIC)  
    IF @PCONSOLIDADO = 'CTA'  
       INSERT INTO @TBLCUENTAS SELECT @PID_ENTIDAD  
  
    IF @PCONSOLIDADO = 'CLT'  
       INSERT INTO @TBLCUENTAS 
       SELECT ID_CUENTA 
         FROM CUENTAS 
        WHERE ID_CLIENTE = @PID_ENTIDAD  
          AND COD_ESTADO = 'H'
       
    IF @PCONSOLIDADO = 'GRP'  
       INSERT INTO @TBLCUENTAS 
       SELECT R.ID_CUENTA 
         FROM REL_CUENTAS_GRUPOS_CUENTAS R
            , CUENTAS C
        WHERE R.ID_GRUPO_CUENTA = @PID_ENTIDAD  
          AND C.ID_CUENTA       = R.ID_CUENTA
          AND C.COD_ESTADO      ='H'
          
-- SE DEFINE EL CASO ESPECIAL CMPC    
    SET @PFECHA_MOV = '20110915'     --FECHA DE CAMBIO DE PRECIO    
    SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO     
      FROM NEMOTECNICOS   
     WHERE UPPER(NEMOTECNICO) = UPPER('CMPC')  --DEFINICION NEMOTECNICO    
--DEFINICION DEL CURSOR ESPECIAL EN EL CASO QUE CORRESPONDA    
    
    IF @LID_NEMOTECNICO = @PID_NEMOTECNICO    
       BEGIN     
          DECLARE CURSOR_MOV_ACTIVOS CURSOR FAST_FORWARD FOR   
                SELECT FLG_TIPO_MOVIMIENTO  
                     , CANTIDAD  
                     , CASE WHEN M.FECHA_OPERACION< @PFECHA_MOV AND PRECIO>=10000 THEN PRECIO/10    
                            ELSE PRECIO   
                       END  AS  'PRECIO'  
                     , CASE WHEN M.FECHA_OPERACION< @PFECHA_MOV AND PRECIO>=10000 THEN (CANTIDAD*(PRECIO/10))    
                            ELSE MONTO   
                       END AS 'MONTO'
                     , FECHA_OPERACION
                     , ID_CUENTA    
                     , ID_MONEDA
                  FROM MOV_ACTIVOS M    
                 WHERE M.ID_NEMOTECNICO = @PID_NEMOTECNICO    
                   AND M.ID_CUENTA IN (SELECT ID_CUENTA FROM @TBLCUENTAS)  
                   AND M.FECHA_OPERACION <= @PFECHA    
                   AND M.COD_ESTADO <> DBO.PKG_GLOBAL$CCOD_ESTADO_ANULADO()    
                 ORDER BY M.FECHA_OPERACION, M.FLG_TIPO_MOVIMIENTO, M.ID_OPERACION_DETALLE DESC    
          FOR READ ONLY    
       END    
    ELSE    
       BEGIN     
          DECLARE CURSOR_MOV_ACTIVOS CURSOR FAST_FORWARD FOR
                SELECT FLG_TIPO_MOVIMIENTO  
                     , CANTIDAD  
                     , PRECIO  
                     , MONTO    
                     , FECHA_OPERACION
                     , ID_CUENTA
                     , ID_MONEDA
                  FROM MOV_ACTIVOS M    
                 WHERE M.ID_NEMOTECNICO = @PID_NEMOTECNICO    
                   AND M.ID_CUENTA IN (SELECT ID_CUENTA FROM @TBLCUENTAS)    
                   AND M.FECHA_OPERACION <= @PFECHA    
                   AND M.COD_ESTADO <> DBO.PKG_GLOBAL$CCOD_ESTADO_ANULADO()    
                 ORDER BY M.FECHA_OPERACION, M.ID_OPERACION_DETALLE, M.FLG_TIPO_MOVIMIENTO FOR READ ONLY    
       END     
          
       SET @SALDO = 0    
       SET @PRIMER_REGISTRO = 1    
      OPEN CURSOR_MOV_ACTIVOS FETCH NEXT FROM CURSOR_MOV_ACTIVOS    
         INTO @TIPO_MOVIMIENTO,    
              @CANTIDAD,    
              @PRECIO,    
              @MONTO,
              @FECHA_OPERACION,
              @LID_CUENTA,
              @ID_MONEDA_OPE
  
     WHILE @@FETCH_STATUS = 0 AND @@ERROR = 0    
        BEGIN    
           SELECT @LID_MONEDA = ID_MONEDA FROM CUENTAS WHERE ID_CUENTA = @LID_CUENTA

           IF @LID_MONEDA <> @ID_MONEDANEMO
              BEGIN  
                 IF @ID_MONEDA_OPE <> @ID_MONEDANEMO
                    BEGIN
                       SELECT @VALORCAMBIO = VALOR 
                         FROM VALOR_TIPO_CAMBIO 
                        WHERE ID_TIPO_CAMBIO = (SELECT ID_TIPO_CAMBIO   
                                                  FROM TIPO_CAMBIOS                                                                                      
                                                 WHERE ID_MONEDA_DESTINO = @ID_MONEDANEMO)                                                                                     
                          AND FECHA = @FECHA_OPERACION  
                    END
                 ELSE
                    BEGIN
                       SET @VALORCAMBIO = 1  
                    END
              END  
           ELSE  
              BEGIN  
                 SET @VALORCAMBIO = 1  
              END  
             
           IF @PRIMER_REGISTRO = 1    
              BEGIN    
                 IF @SALDO = 0 AND @TIPO_MOVIMIENTO = 'I'    
                    BEGIN    
                       SET @SALDO = @SALDO + @CANTIDAD    
                       IF @LID_MONEDA = @ID_MONEDANEMO
                          BEGIN
						     SET @SALDO_PESOS = @MONTO    
							 SET @PRECIO_PROMEDIO = @PRECIO    
                          END
                       ELSE
                          BEGIN
                             IF @ID_MONEDA_OPE <> @ID_MONEDANEMO
                                BEGIN
                                   SET @SALDO_PESOS = @MONTO / @VALORCAMBIO  
                                   SET @PRECIO_PROMEDIO = @PRECIO / @VALORCAMBIO  
                                END
                             ELSE
                                BEGIN
                                   SET @SALDO_PESOS = @MONTO    
							       SET @PRECIO_PROMEDIO = @PRECIO    
                                END
                          END
                    END    
              END    
           ELSE    
              BEGIN    
                 IF @TIPO_MOVIMIENTO = 'I'    
                    BEGIN    
                       IF @SALDO > 0    
                          BEGIN    
                             SET @SALDO = @SALDO + @CANTIDAD    
                             IF @LID_MONEDA = @ID_MONEDANEMO
                                BEGIN  
								   SET @SALDO_PESOS = @SALDO_PESOS + @MONTO    
								END
                             ELSE
                                BEGIN  
                                   IF @ID_MONEDA_OPE <> @ID_MONEDANEMO
                                      BEGIN
                                         SET @SALDO_PESOS = @SALDO_PESOS + (@MONTO / @VALORCAMBIO)  
                                      END
                                   ELSE
                                      BEGIN
                                         SET @SALDO_PESOS = @SALDO_PESOS + @MONTO    
                                      END
                                END								
                             SET @PRECIO_PROMEDIO = ROUND(@SALDO_PESOS/@SALDO,4)    
                          END    
                       ELSE    
                          BEGIN    
                             SET @SALDO = @CANTIDAD    
                             IF @LID_MONEDA = @ID_MONEDANEMO
                                BEGIN                             
								   SET @SALDO_PESOS = @MONTO    
								   SET @PRECIO_PROMEDIO = @PRECIO    
                                END
                             ELSE
                                BEGIN
                                   IF @ID_MONEDA_OPE <> @ID_MONEDANEMO
                                      BEGIN
                                         SET @SALDO_PESOS = @MONTO / @VALORCAMBIO  
                                         SET @PRECIO_PROMEDIO = @PRECIO / @VALORCAMBIO  
                                      END
                                   ELSE
                                      BEGIN
                                         SET @SALDO_PESOS = @MONTO
                                         SET @PRECIO_PROMEDIO = @PRECIO
                                      END
                                END
                          END
                    END
                 ELSE    
                    BEGIN    
                       IF @SALDO > 0    
                          BEGIN    
                             SET @SALDO = @SALDO - @CANTIDAD    
                             SET @SALDO_PESOS = @SALDO_PESOS - ROUND((@CANTIDAD*@PRECIO_PROMEDIO),0)    
                             SET @PRECIO_PROMEDIO = @PRECIO_PROMEDIO    
                          END    
                    END    
              END    
           SET @PRIMER_REGISTRO = 2 
           FETCH NEXT FROM CURSOR_MOV_ACTIVOS INTO @TIPO_MOVIMIENTO,    
                                                   @CANTIDAD,    
                                                   @PRECIO,    
                                                   @MONTO,
                                                   @FECHA_OPERACION,
                                                   @LID_CUENTA,
                                                   @ID_MONEDA_OPE
        END    
     CLOSE CURSOR_MOV_ACTIVOS    
     DEALLOCATE CURSOR_MOV_ACTIVOS    
     RETURN @PRECIO_PROMEDIO    
END    
GO

GRANT EXECUTE ON [FNT_ENTREGA_PRECIO_PROMEDIO_CONSOLIDADO] TO DB_EXECUTESP 
GO