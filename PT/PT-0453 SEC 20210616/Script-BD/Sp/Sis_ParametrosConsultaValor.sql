IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[Sis_ParametrosConsultaValor]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
	DROP PROCEDURE Sis_ParametrosConsultaValor
GO

CREATE PROCEDURE Sis_ParametrosConsultaValor
(
	@pIdNegocio NUMERIC(10),
	@pCodigo	VARCHAR(100),
	@pResultado	VARCHAR(2000) output
)
AS
BEGIN TRY

	SET @pResultado	 = 'OK'

	SELECT	@pResultado = VALOR
	FROM	SIS_PARAMETROS WITH (NOLOCK)
	WHERE	CODIGO = @pCodigo
	AND		ID_NEGOCIO = @pIdNegocio
	AND		ESTADO ='VIG'	
		
END TRY
BEGIN CATCH
	SET @pResultado =  ERROR_MESSAGE()	+ ' procedimiento: ' + ERROR_PROCEDURE() + ' l�nea: ' + CONVERT(VARCHAR,ERROR_LINE())
END CATCH
GO

GRANT EXECUTE ON Sis_ParametrosConsultaValor TO DB_EXECUTESP
GO
