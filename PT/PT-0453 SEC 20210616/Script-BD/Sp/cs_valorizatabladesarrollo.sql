IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cs_valorizatabladesarrollo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[cs_valorizatabladesarrollo]
GO
CREATE PROCEDURE DBO.cs_valorizatabladesarrollo  
@familia        char(06)                 ,  
@nemotecnico    char(12)                 ,  
@Feccal         datetime                 ,  
@query_Nemo     char(01)                 ,  
@query_Valo     char(01)                 ,  
@Calcula        Char(01)                 ,  
@nominales      numeric(18,04)           ,  
@tir            numeric(18,06)     output,  
@pvp            numeric(18,06)     output,  
@valor_presente numeric(18,00)     output,  
@valor_UM       float              output,  
@nnumcupact     int            = 0 output,  
@nnumcupprx     int            = 0 output,  
@fecactcupon    datetime       = 0 output,  
@fecprxcupon    datetime       = 0 output,  
@Amortiza_Act   numeric(11,06) = 0 output,  
@Interes_Act    numeric(11,06) = 0 output,  
@Amortiza_Prx   numeric(11,06) = 0 output,  
@Interes_Prx    numeric(11,06) = 0 output,  
@fDuration      float              output,  
@dg_resultado   varchar(255)       output  
--with encryption  
as   
set nocount on  
declare @tir_aux            float  
declare @error              int,  
        @nAst               int,  
        @basecalculo        int,  
        @nCount             int,  
        @vp                 float,  
        @mayor              float,  
        @menor              float,  
        @valor_par          float,  
        @monto_cien         float,  
        @monto_unidad       float,  
        @saldo_cupones      float,  
        @flujo_descontado   float,  
        @tbdesa             char(01),  
        @dFecemi            datetime,  
        @dFecven            datetime,  
        @dFeccal            datetime,  
        @tasaefectiva       numeric(18,06),  
        @tasaefectiva_aux   numeric(18,06),  
        @tasaefectiva_aux_2 numeric(18,06),  
        @idserie            numeric(12,00),  
        @idfamilia          numeric(12,00),  
        @valormon           numeric(12,06),  
        @TasaEmision        numeric(08,06),  
        @Int_Prox_Cup       numeric(11,06),  
        @Amor_Prox_Cup      numeric(11,06),  
        @Saldo_Prox_Cup     numeric(11,06),  
        @dias_reales        int,
        @RF_TIR_DECIMAL     int,
        @RF_VPAR_DECIMAL    int
  
declare @paso table(idserie     int,   
                    nrocupon    numeric(3),  
                    flujo       numeric(11,6),  
                    interes     numeric(11,6),  
                    capital     numeric(11,6),  
                    saldo       numeric(11,6),  
                    Fecha_Cupon datetime)  
  
  
set @tbdesa      = null  
set @dFeccal     = @Feccal  
set @valormon    = 1  
set @error       = 0  
set @dias_reales = 0  
set @tir_aux     = @tir/convert(float,100)  

exec Sis_ParametrosConsultaValor 0, 'RF_TIR_DECIMAL', @RF_TIR_DECIMAL output
exec Sis_ParametrosConsultaValor 0, 'RF_VALORPAR_DECIMAL', @RF_VPAR_DECIMAL output
  
exec @error = cs_entregadatosnemo @familia,             -- 1  
                                  @Nemotecnico,         -- 2  
                                  @dFeccal,             -- 3  
                                  @query_nemo,          -- 4  
                                  @tbdesa       output, -- 5  
                                  @idserie      output, -- 6  
                                  @nNumCupAct   output, -- 7  
                                  @nNumCupPrx   output, -- 8  
                                  @dFecemi      output, -- 9  
                                  @dFecven      output, -- 10  
                                  @tasaefectiva output, -- 11  
                                  @nAst         output, -- 12  
                                  @basecalculo  output, -- 13  
                                  @fecactcupon  output, -- 14  
                                  @fecprxcupon  output, -- 15  
                                  @valormon     output, -- 16  
                                  @TasaEmision  output, -- 17  
                                  @Amortiza_Act output, -- 18  
                                  @Interes_Act  output, -- 19  
                                  @Amortiza_Prx output, -- 20  
                                  @Interes_Prx  output, -- 21  
                                  @dg_resultado output  -- 22  
if @error <> 0   
begin  
  Return @error  
end  
if @Calcula = 'C'  
 Return @error  
  
set @idfamilia = (Select idfamilia from  cs_tb_series WITH (NOLOCK) where idserie = @idserie)  
if @nNumCupAct <> 0  
  set @saldo_cupones = (select saldo from cs_tb_cupones t WITH (NOLOCK) where idserie = @idserie and nrocupon = @nNumCupAct)  
else  
  set @saldo_cupones = 100  
  
if @familia = 'LH'  
begin  
 insert @paso  
 select d.idserie,   
        d.nrocupon,  
        d.flujo,  
        d.interes,  
        d.capital,  
        d.saldo,  
        'Fecha_Cupon' = dateadd(month,(d.nrocupon*s.periodocupon), @dFecemi)  
   from cs_tb_series s WITH (NOLOCK) , cs_tb_cupones d WITH (NOLOCK)  
  where @idserie = s.idserie   
    and d.idserie = s.idserie  

 if datepart(day,@dFeccal) = 31  
  set @dFeccal  = dateadd( day,-1, @dFeccal)  
 if @Calcula = 'M'  
 begin  
    set @flujo_descontado = (select sum((d.flujo/power((convert(float,1) +  
                                    @tir/convert(float,100)),(((convert(float,30)*  
                                    datediff(month,@dFeccal,d.fecha_cupon))-  
                                    datepart(day,@dFeccal))+convert(float,1))/  
                                    convert(float,@basecalculo))))  
                               from @paso d  
                              where d.idserie = @idserie  
                                and d.nrocupon >= @nNumCupPrx)  
    set @fDuration = (select sum((convert(float,d.flujo) * (((convert(float,30)*  
                             datediff(month,@dFeccal,d.fecha_cupon) )-  
                             datepart(day,@dFeccal) ) +convert(float,1))/convert(float,@basecalculo)/power(convert(float,1) + @tir_aux,((((convert(float,30)*  
                             datediff(month,@dFeccal,d.fecha_cupon) )-  
                             datepart(day,@dFeccal) ) +convert(float,1))/convert(float,@basecalculo)))))  
                        from @paso d  
                       where d.idserie = @idserie  
                         and d.nrocupon >= @nNumCupPrx)  
  
 END  

 if @Calcula = 'F'  
 begin  
    select d.nrocupon,  
           Fecha_Cupon,  
           d.capital,  
           d.interes,  
           d.flujo,  
           d.saldo  
      from @paso d  
     where d.idserie = @idserie  
       and d.nrocupon >= @nNumCupPrx  
     order by d.nrocupon  
     Return 0  
 end  
 if @nAst = 0 and substring(@nemotecnico,1,6) <> 'BOT33B'  
 BEGIN  
    set @valor_par = round(@saldo_cupones*power((convert(float,1)+@tasaefectiva/  
                     convert(float,100)),(((datepart(year,@dFeccal)-  
                     datepart(year,@fecactcupon))*convert(float,@basecalculo))+  
                     ((datepart(month,@dFeccal)-datepart(month,@fecactcupon))*  
                     convert(float,30))+(datepart(day,@dFeccal)-  
                     datepart(day,@fecactcupon)))/convert(float,@basecalculo)),10)  
 END  
 else  
 BEGIN  
    set @valor_par = (select sum(Flujo/power((convert(float,1)+@tasaefectiva/  
                             convert(float,100)),(((convert(float,30)*  
                             datediff(month,@dFeccal,Fecha_Cupon))-  
                             datepart(day,@dFeccal))+convert(float,1))/  
                             convert(float,@basecalculo)))  
                        from @paso d   
                       where d.idserie = @idserie   
                         and d.nrocupon >= @nNumCupPrx)  
 END  
end  
else  
begin  
  if @familia in ('BR')  
  begin  
    Declare   
		    @dFecman            datetime,  
		    @cFecman            char(10),  
		    @iMesman            int,  
		    @nM1                int,  
		    @nM2                int,  
		    @nFactor            int,  
		    @nDifmes            int,  
		    @valor_par2         float,  
		    @fvan               float,  
		    @fVan2              float,  
		    @Ipcemi             float,  
		    @Ipccal             float,  
		    @IdIndiceIPC        int  
		  
    Set @TasaEmision = 4  
    Set @basecalculo = 365  
    Set @dFeccal = @Feccal  
    Set @valormon = 1  
    set @nFactor = 0  
    set @IdIndiceIPC = (select idmoneda from dbo.cs_tb_paramfinancieros WITH (NOLOCK)  where moneda = 'IPF')  
    set @iMesman = datepart(DAY,@dFeccal)*-1  
    set @cFecman = convert(char(8),dateadd(day,@iMesman,@dFeccal),112)  
    set @dFecman = convert(datetime,substring(@cFecman,1,4) + substring(@cFecman,5,2)+ '01' )  
    select @Ipcemi = valor from cs_tb_detalleparamfinancieros WITH (NOLOCK) where idmoneda = @IdIndiceIPC and fecha = @dFecemi          
    select @Ipccal = valor from cs_tb_detalleparamfinancieros WITH (NOLOCK) where idmoneda = @IdIndiceIPC and fecha = @dFecman  
    set @nM1 = datepart(month,@dFecemi)  
    set @nM2 = datepart(month,@dFeccal) - 1  

    if @nM1 > 12  
    begin  
       set @nFactor = -1  
       set @nM1 = @nM1 - 12  
    end  
    if @nM2 > @nM1  
       set @nDifmes = @nM2 - @nM1  
    else  
    begin  
       set @nDifmes = (@nM2 + 12) - @nM1  
       set @nFactor = -1  
    end  
 
    set @valor_par  = round(100.0*(@Ipccal/@Ipcemi)*power((1.0+convert(float,(@TasaEmision/100))),datediff(year,@dFecemi,@dFeccal)+@nFactor)*(1.0+(convert(float,(@TasaEmision/100))/12.0)*@nDifmes),8)  
    set @valor_par2 = round(100.0*power(1.0+convert(float,(@TasaEmision/100)),datediff(year,@dFecemi,@dFeccal)+@nFactor)*(1.0+(convert(float,(@TasaEmision/100))/12.0)*@nDifmes),8)  
    set @nM1 = datepart(month,@dFecemi)  
    set @nM2 = datepart(month,@dFecven) - 1  
    set @nFactor = 0  

    if @nM1 > 12  
    begin  
       set @nFactor    = -1  
       set @nM1 = @nM1 - 12  
    end  
    if @nM2>@nM1  
       set @nDifmes = @nM2 - @nM1  
    else  
    begin  
       set @nDifmes = (@nM2 + 12) - @nM1  
       set @nFactor = -1  
    end    

    set @fVan2          = round(100.0*power(1+convert(float,(@TasaEmision/100)),datediff(year,@dFecemi,@dFecven)+@nFactor)*(1.0+(convert(float,(@TasaEmision/100))/12.0)*@nDifmes),8)  
    set @fVan           = @fVan2/(power(convert(float,1)+@tir/convert(float,100),datediff(day,@dFeccal,@dFecven)/convert(float,@basecalculo)))  
    set @vp             = round((@fVan/@valor_par2)*100.0,@RF_VPAR_DECIMAL)  
    set @valor_presente = round((@vp/100.0)*@nominales*(@valor_par/100.0),0)  
    set @nM2            = datepart(month,@dFecven)  
    set @valor_UM       = (@vp/100.0)*@nominales*(@valor_par/100.0)  
    set @pvp            = round(@vp,@RF_VPAR_DECIMAL)  
    Set @fDuration      = ROUND(convert(float,datediff(day, @dFeccal, @dFecven)) / convert(float,@basecalculo),2)  
  
  end  
  Else  
  begin  
      insert @paso  
      select d.idserie,  
             d.nrocupon,  
             d.flujo,  
             d.interes,   
             d.capital,  
             d.saldo,  
             d.fechacupon  
        from cs_tb_series s WITH (NOLOCK) , cs_tb_cupones d  WITH (NOLOCK) 
       where @idserie = s.idserie   
         and d.idserie = s.idserie  
  
     if @Nemotecnico in ('BSECS-2B','BTRA1-3B','BCENC-C')  
     begin  
        if year(@dFeccal + 1) <> year(@dFeccal)  
           set @dias_reales = 1  
     end    
     if @Calcula = 'M'  
     begin  
        set @flujo_descontado = (select sum(d.flujo/power(convert(float,1) + @tir/convert(float,100),  
                                        datediff(day,@dFeccal,d.Fecha_Cupon)/convert(float,@basecalculo)))  
                                   from @paso d  
                                  where d.idserie = @idserie  
                                    and d.nrocupon >= @nNumCupPrx)  
        set @fDuration = (select sum((convert(float,d.flujo) * convert(float,datediff(day,@dFeccal,d.Fecha_Cupon))/convert(float,@basecalculo)/power(convert(float,1) + @tir_aux,(convert(float,datediff(day,@dFeccal,d.Fecha_Cupon))/convert(float,@basecalculo)))))  
                            from @paso d  
                           where d.idserie = @idserie  
                             and d.nrocupon >= @nNumCupPrx)  
  
        set @valor_par = round(@saldo_cupones*power(convert(float,1)+@tasaefectiva/  
                         convert(float,100),datediff(day,@fecactcupon,@dFeccal-@dias_reales)/  
                         convert(float,@basecalculo)),8)  
     end  
     if @Calcula = 'F'  
     begin  
        select d.nrocupon,  
               Fecha_Cupon,  
               d.capital,  
               d.interes,  
               d.flujo,  
               d.saldo  
          from @paso d  
         where d.idserie = @idserie  
           and d.nrocupon >= @nNumCupPrx  
         order by d.nrocupon  
         Return 0  
     end  
  end  
end  
-------------------------------------------------------------------------------  
-- Autor: Mmardones  
-- Fecha: 09/01/2017  
-- para bonos CERO el valor_par queda en cero dando error de divisi�n por cero  
-- El cambio pasa directamente a producci�n  
if @valor_par = 0   
   set @valor_par = 1  
-------------------------------------------------------------------------------  
if not @familia in ('BR')  
begin  
   set @fDuration      = round(@fDuration/@flujo_descontado,8)  
   set @vp             = round((@flujo_descontado/@valor_par)*convert(float,100), @RF_TIR_DECIMAL)  
   set @monto_unidad   = (@vp/convert(float,100))*(@valor_par/convert(float,100))  
   set @pvp            = round(@vp,@RF_VPAR_DECIMAL)  
   set @valor_presente = round(@monto_unidad * @nominales * @valormon,0)  
   set @valor_UM       = @monto_unidad * @nominales  
end  
  
if @query_Valo = 'S'  
begin  
   select @tir            as Tir,  
          @pvp            as pvp,  
          @valor_presente as valor_presente,  
          @valor_UM       as valor_UM,  
          @nnumcupact     as nnumcupact,  
          @nnumcupprx     as nnumcupprx,  
          @fecactcupon    as fecactcupon,  
          @fecprxcupon    as fecprxcupon,  
          @Amortiza_Act   as Amortiza_Act,  
          @Interes_Act    as Interes_Act,  
          @Amortiza_Prx   as Amortiza_Prx,  
          @Interes_Prx    as Interes_Prx,  
          @dg_resultado   as dg_resultado,  
          @valor_par      as valor_par,  
          @VP             AS vp,  
          @monto_unidad   AS monto_UM_unidad,  
          @fDuration      AS Duration  
end  
  
--Agrega duracion en publicadores_precios  
--Agrega Valor TIR en publicadores_precios  
  
if ((SELECT COUNT(1) 
       FROM publicadores_precios WITH (NOLOCK) 
      WHERE fecha = @feccal 
        AND id_nemotecnico = (select id_nemotecnico 
                                from nemotecnicos WITH (NOLOCK) 
                               where nemotecnico=@nemotecnico) AND DURACION IS NULL) > 0  
        And not @fDuration is null)  
Begin  
   update publicadores_precios  
      set duracion = @fDuration  
    where id_nemotecnico = (select id_nemotecnico from nemotecnicos WITH (NOLOCK) where nemotecnico=@nemotecnico)  
      and fecha = @feccal  
End  
set nocount off  
return @error  
GO
GRANT EXECUTE ON [cs_valorizatabladesarrollo] TO DB_EXECUTESP
GO
