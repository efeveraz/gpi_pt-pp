IF  EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[PKG_PROCESOS_IMPORTADOR$CISCB_CB_MOVTO_RFIJA_II]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [DBO].[PKG_PROCESOS_IMPORTADOR$CISCB_CB_MOVTO_RFIJA_II]
GO

CREATE PROCEDURE  [DBO].[PKG_PROCESOS_IMPORTADOR$CISCB_CB_MOVTO_RFIJA_II](
@PFECHA_MOVIMIENTOS CHAR(8)
)
AS
BEGIN

	SET DATEFORMAT YMD
	SET NOCOUNT ON

	DECLARE @LRUT_CLIENTE          CHAR(15),     @LCANTIDAD          DECIMAL,         @LID_CUENTA          INT,
            @LNEMOTECNICO          CHAR(50),     @LNEMOTECNICOFIN    CHAR(50),        @LID_NEMOTECNICO     INT,
            @LCODINSTRUMENTO       CHAR(15),     @LMGS               CHAR(100),       @LNUM_FILA           INT,
            @INTCOUNTER            INT,          @PMGS               VARCHAR(8000),   @LNUMEROORDEN        INT,
            @LCORRELATIVO          INT,          @LACUMULA_CANTIDAD  DECIMAL,         @LREGISTROS          INT,
            @NUMCUENTA             VARCHAR(10),  @COD_ESTADO         VARCHAR(3),      @ID_ASESOR           INT,
            @FLG_BLOQUEADO         VARCHAR(1),   @DSC_BLOQUEO        VARCHAR(120),    @LERRORNOCUENTA      VARCHAR(1),
            @LOUTPUTLOG_SCTAS_FLG  INT,          @LGRABADO           VARCHAR(1),      @LCODRESULTADO       VARCHAR(5) ,
            @LMSGRESULTADO         VARCHAR(800), @ID_ORIGEN          NUMERIC,         @ID_TIPO_CONVERSION  NUMERIC,
            @LID_MOV_ACTIVO        NUMERIC(18),  @LID_SALDO_ACTIVO   INT,             @PFECHA_SALDO_ACTIVO DATETIME,
            @LID_MONEDAPAGO        INT,          @LFECHA             DATETIME,        @LID_MONEDADEPOSITO  INT,
            @LID_EMISOR_ESPECIFICO INT,          @LCOD_INSTRU_SVS    VARCHAR(10)

	SET @INTCOUNTER     = 0
	SET @PFECHA_SALDO_ACTIVO = @PFECHA_MOVIMIENTOS
	SET @PFECHA_SALDO_ACTIVO = @PFECHA_SALDO_ACTIVO -1


	--************************************************************************************************************************************************************************
	---- CREACION DE TABLA DE RECUPERACION DE DATOS
	CREATE TABLE #TABLA1  (RUT_CLIENTE     CHAR(15),      TIPO_OPERACION    CHAR(1),       NUMERO_ORDEN             INT,           CORRELATIVO  INT,
                           NEMOTECNICO     VARCHAR(20),   CANTIDAD          FLOAT ,        PRECIO                   FLOAT,         MONTO_NETO   FLOAT,
                           COMISION        FLOAT ,        DERECHO           FLOAT ,        GASTOS                   FLOAT,         IVA          FLOAT ,
                           MONTO_OPERACION FLOAT ,        FECHA_LIQUIDACION DATETIME,      NOMBRE_CLIENTE           VARCHAR(120),  FACTURA      INT,
                           TIPO_LIQ        VARCHAR(3),    BOLSA             VARCHAR(5),    CUST_NOM                 VARCHAR(1),    GARANTIAS    NUMERIC(18,4),
                           PRESTAMOS       NUMERIC(18,4), SIMULTANEAS       NUMERIC(18,4), NOMBRE_INSTRUMENTO_FECU  VARCHAR(10))
	---- CREACION DE TABLA DE PROCESO
	CREATE TABLE #TABLA2 (NUMERO_FILA      INT,           RUT_CLIENTE          CHAR(15),      TIPO_OPERACION    CHAR(1),       NUMERO_ORDEN    INT,
                          CORRELATIVO      INT,           NEMOTECNICO          CHAR(20),      CANTIDAD          FLOAT,         PRECIO          FLOAT,
                          MONTO_NETO       FLOAT,         COMISION             FLOAT,         DERECHO           FLOAT,         GASTOS          FLOAT,
                          IVA              FLOAT,         MONTO_OPERACION      FLOAT ,        FECHA_LIQUIDACION DATETIME,      NOMBRE_CLIENTE  VARCHAR(120),
                          FACTURA          INT,           TIPO_LIQ             VARCHAR(3),    BOLSA             VARCHAR(5),    CUST_NOM        VARCHAR(1),
                          GARANTIAS        NUMERIC(18,4), PRESTAMOS            NUMERIC(18,4), SIMULTANEAS       NUMERIC(18,4), NOMBRE_INSTRUMENTO_FECU VARCHAR(10),
                          COD_INSTRUMENTO  CHAR(15),      ID_EMISOR_ESPECIFICO INT,           ID_NEMOTECNICO    INT,           ID_CUENTA       INT,
                          NUM_CUENTA       VARCHAR(10),   ID_ASESOR            INT,           FLG_BLOQUEADO     VARCHAR(1),    LERRORNOCUENTA  VARCHAR(1),
                          LLOG_SCTAS_FLG   INT,           DSC_BLOQUEO          VARCHAR(120),  ID_MOV_ACTIVO     INT,           ID_SALDO_ACTIVO INT,
                          GRABADO          VARCHAR(1),    LCODRESULTADO        VARCHAR(5),    LMSGRESULTADO     VARCHAR(800))
	--************************************************************************************************************************************************************************

	-- CARGA DATOS DE PROCEDIMIENTO EN TABLA TEMPORAL
	INSERT #TABLA1
	EXEC [CISCB].[DBO].[AD_QRY_CB_MOVTO_RFIJA] @PFECHA_MOVIMIENTOS = @PFECHA_MOVIMIENTOS

	-- CARGA DATOS DE PROCEDIMIENTO EN TABLA DE PROCESO
	INSERT INTO #TABLA2
	SELECT 1, RUT_CLIENTE     ,  TIPO_OPERACION    ,   NUMERO_ORDEN   ,   CORRELATIVO ,  NEMOTECNICO,   CANTIDAD,
              PRECIO          ,  MONTO_NETO        ,   COMISION       ,   DERECHO     ,  GASTOS     ,    IVA    ,
              MONTO_OPERACION ,  FECHA_LIQUIDACION ,   NOMBRE_CLIENTE ,   FACTURA     ,  TIPO_LIQ   ,    BOLSA  ,
              CUST_NOM        ,  GARANTIAS         ,   PRESTAMOS      ,   SIMULTANEAS ,  NOMBRE_INSTRUMENTO_FECU,
              NULL            ,  NULL              ,   NULL           ,   NULL        ,  NULL                   ,
              NULL            ,  NULL              ,   NULL           ,   NULL        ,  NULL                   ,
              NULL            ,  NULL              ,   NULL           ,   NULL        ,  NULL
	FROM #TABLA1  WITH (NOLOCK)
	ORDER BY RUT_CLIENTE, NUMERO_ORDEN, CORRELATIVO, NEMOTECNICO

	--INCREMENTA EL NUMERO DE FILA EN TABLA DE PROCESO
	UPDATE #TABLA2
	SET @INTCOUNTER = NUMERO_FILA = @INTCOUNTER + 1;

	SELECT @ID_ORIGEN = ID_ORIGEN FROM ORIGENES WITH (NOLOCK) WHERE COD_ORIGEN = 'MAGIC_VALORES'
	SELECT @ID_TIPO_CONVERSION = ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WITH (NOLOCK) WHERE TABLA = 'CUENTAS'

	--****************************************************************************************************************************
	DECLARE CUR_OPERACION CURSOR FOR
	SELECT NUMERO_FILA,
           NEMOTECNICO,
           RUT_CLIENTE,
           NUMERO_ORDEN,
           CANTIDAD,
           CORRELATIVO,
           ISNULL(NOMBRE_INSTRUMENTO_FECU, '')
	FROM #TABLA2  WITH (NOLOCK)
	ORDER BY NUMERO_FILA

	SET @LACUMULA_CANTIDAD = 0
	SET @LREGISTROS = 0

	OPEN CUR_OPERACION
	FETCH CUR_OPERACION INTO @LNUM_FILA,
                             @LNEMOTECNICO,
                             @LRUT_CLIENTE,
                             @LNUMEROORDEN,
                             @LCANTIDAD,
                             @LCORRELATIVO,
                             @LCOD_INSTRU_SVS
	WHILE (@@FETCH_STATUS = 0)
	BEGIN

		SET @LCODRESULTADO  = 'OK'
        SET @LERRORNOCUENTA = ''
		SET @LMSGRESULTADO  = ''
        SET @DSC_BLOQUEO    = ''
        SET @LGRABADO       = 'N'
        SET @LID_MOV_ACTIVO = NULL
		SET @LID_NEMOTECNICO = NULL
		SET @LCODINSTRUMENTO  = ''
		SET @LNEMOTECNICO = RTRIM(LTRIM(@LNEMOTECNICO))

        -- IDENTIFICADOR DE CUENTA **************************
        SELECT @LID_CUENTA = ID_ENTIDAD
        FROM REL_CONVERSIONES WITH (NOLOCK)
        WHERE ID_ORIGEN          = (SELECT ID_ORIGEN FROM ORIGENES WITH (NOLOCK) WHERE COD_ORIGEN = 'MAGIC_VALORES')
          AND ID_TIPO_CONVERSION = (SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WITH (NOLOCK) WHERE TABLA = 'CUENTAS')
          AND VALOR = RTRIM(LTRIM(@LRUT_CLIENTE))

        IF @@ROWCOUNT = 0
        BEGIN
			SET @LERRORNOCUENTA = 'N'
            SET @LOUTPUTLOG_SCTAS_FLG = 1 --TRUE
            SET @ID_ASESOR     = 0
            SET @LCODRESULTADO = 'ERROR'
            SET @LMSGRESULTADO = 'ERROR: RUT CLIENTE: ' + RTRIM(LTRIM(@LRUT_CLIENTE))  + '|NEMOT�CNICO: ' + RTRIM(LTRIM(@LNEMOTECNICO)) + '- MOVIMIENTO NO POSEE CUENTA.'
        END
        ELSE
        BEGIN
			SELECT @COD_ESTADO    = COD_ESTADO,
                   @ID_ASESOR     = ID_ASESOR,
                   @FLG_BLOQUEADO = FLG_BLOQUEADO,
                   @DSC_BLOQUEO   = ISNULL(OBS_BLOQUEO,''),
                   @NUMCUENTA     = NUM_CUENTA
            FROM CUENTAS WITH (NOLOCK) WHERE ID_CUENTA = @LID_CUENTA

            IF @COD_ESTADO = 'H'
            BEGIN
				IF @FLG_BLOQUEADO = 'S'
				BEGIN
					SET @FLG_BLOQUEADO = 'B'
					SET @LERRORNOCUENTA = 'S'
					SET @LCODRESULTADO = 'ERROR'
					SET @LMSGRESULTADO = 'ERROR: CUENTA: ' + @NUMCUENTA  + '|RUT CLIENTE: ' + RTRIM(LTRIM(@LRUT_CLIENTE)) + '|NEMOT�CNICO: ' + RTRIM(LTRIM(@LNEMOTECNICO)) + '-' + @DSC_BLOQUEO
				END
				SET @LOUTPUTLOG_SCTAS_FLG = 0 --FALSE
			END
			ELSE
            BEGIN
				SET @LERRORNOCUENTA = 'S'
				SET @LCODRESULTADO = 'ERROR'
				SET @LMSGRESULTADO = 'ERROR: CUENTA: ' + @NUMCUENTA  + '|RUT CLIENTE: ' + RTRIM(LTRIM(@LRUT_CLIENTE)) + '|NEMOT�CNICO: ' + RTRIM(LTRIM(@LNEMOTECNICO)) + '-CUENTA DESHABILITADA'
			END
		END

        /**** VALIDACION DE NEMO *****/

        SELECT  @LID_MONEDAPAGO = ID_MONEDA
        FROM MONEDAS WITH (NOLOCK)
        WHERE COD_MONEDA = '$$'

        IF ((SELECT CASE WHEN EXISTS(SELECT 1 FROM DBO.VIEW_NEMOTECNICOS WITH (NOLOCK) WHERE NEMOTECNICO = RTRIM(LTRIM(@LNEMOTECNICO))) THEN 1 ELSE 0 END) = 1)
        BEGIN
			SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO,
                   @LCODINSTRUMENTO = COD_INSTRUMENTO
            FROM DBO.VIEW_NEMOTECNICOS WITH (NOLOCK)
            WHERE NEMOTECNICO = RTRIM(LTRIM(@LNEMOTECNICO))
		END
        ELSE IF(SUBSTRING(RTRIM(LTRIM(@LNEMOTECNICO)),1,7) = 'PACTO-C')  --PACTOS *****
		BEGIN
			-- FECHA DE VENCIMIENTO
            SET @LFECHA = CAST(SUBSTRING(@LNEMOTECNICO,9,4) + SUBSTRING(@LNEMOTECNICO,14,2) + SUBSTRING(@LNEMOTECNICO,17,2) AS DATETIME)
            SET @LCODINSTRUMENTO = 'PACTOS_NAC'
            -- MONEDA DEPOSITO
            SET @LID_MONEDADEPOSITO = @LID_MONEDAPAGO
            -- EMISORES ESPECIFICOS
            SELECT TOP 1 @LID_EMISOR_ESPECIFICO = ID_EMISOR_ESPECIFICO
            FROM EMISORES_ESPECIFICO WITH (NOLOCK)
            WHERE COD_SVS_NEMOTECNICO = 'SEC';
            -- NEMOTECNICO
            EXEC [DBO].[PKG_NEMOTECNICOS$GENERACION_NEMOTECNICO_SVS] @PID_EMISOR_ESPECIFICO = @LID_EMISOR_ESPECIFICO ,
                                                                     @PFECHA_VENCIMIENTO    = @LFECHA ,
                                                                     @PID_MONEDA_DEPOSITO   = @LID_MONEDADEPOSITO ,
                                                                     @PID_MONEDA_PAGO       = @LID_MONEDAPAGO ,
                                                                     @PNEMOTECNICO          = @LNEMOTECNICOFIN OUTPUT,
                                                                     @PMGS                  = @LMGS OUTPUT
			IF RTRIM(LTRIM(@LMGS)) <> ''
			BEGIN
				SET @LERRORNOCUENTA = 'S'
                SET @LOUTPUTLOG_SCTAS_FLG = 1 --TRUE
                SET @LCODRESULTADO = 'ERROR'
                SET @LMSGRESULTADO = 'ERROR: PROBLEMAS AL BUSCAR NEMOT�CNICO (PACTOS) ' + RTRIM(LTRIM(@LNEMOTECNICO))
            END
            ELSE
            BEGIN
				SET @LNEMOTECNICOFIN = RTRIM(LTRIM(REPLACE(@LNEMOTECNICOFIN, SUBSTRING(@LNEMOTECNICOFIN,1,2), 'PACTO$  ')))
                SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO
                FROM DBO.VIEW_NEMOTECNICOS WITH (NOLOCK)
				WHERE DBO.VIEW_NEMOTECNICOS.NEMOTECNICO = RTRIM(LTRIM(@LNEMOTECNICOFIN))
			END
		END
        ELSE IF(SUBSTRING(@LNEMOTECNICO,1,2) IN ('DU','DO','D$','DD','DE'))
        BEGIN
			IF NOT ((SUBSTRING(@LNEMOTECNICO,1,2) = 'DE') AND (RTRIM(LTRIM(@LCOD_INSTRU_SVS)) = 'LH'))
            BEGIN
				-- FECHA DE VENCIMIENTO
                SET @LFECHA = CAST('20' + SUBSTRING(@LNEMOTECNICO,11,2) + SUBSTRING(@LNEMOTECNICO,9,2) + SUBSTRING(@LNEMOTECNICO,7,2) AS DATETIME)
                -- CODIGO DE INSTRUMENTO
                SET @LCODINSTRUMENTO = 'DEPOSITO_NAC'
                -- MONEDA DEPOSITO
                -------------------------------------------------------------------------------------------
                -- RESPONSABLE: MMARDONES
                -- FECHA: 10/01/2018
				-- AGREGA LAS TIPOS "DD" Y "DE".
				-- CODIF. MAGIC DESCRIPCI�N                             CODIF. SVS
				-- D$             DEP�SITO EN PESOS, PAGADO EN PESOS    FN
				-- DU             DEP�SITO EN UF, PAGADO EN UF         FU
				-- DO             DEP�SITO EN D�LARES, PAGADO EN PESOS FD
				-- DD             DEPOSITO EN D�LARES, PAGADO EN D�LARES F*
				-- DE             DEPOSITO EN EUROS, PAGADO EN EUROS     FO
				-------------------------------------------------------------------------------------------
                IF SUBSTRING(@LNEMOTECNICO,2,1) = 'D'
                BEGIN
					SET @LID_MONEDADEPOSITO = -1
                END
                ELSE
                BEGIN
                -------------------------------------------------------------------------------------------
					SELECT TOP 1 @LID_MONEDADEPOSITO = ID_MONEDA
                    FROM MONEDAS WITH (NOLOCK)
                    WHERE COD_MONEDA = CASE SUBSTRING(@LNEMOTECNICO,2,1)
									   WHEN '$' THEN '$$'
                                       WHEN 'U' THEN 'UF'
                                       WHEN 'O' THEN 'USD'
                                       WHEN 'D' THEN 'USD'
                                       WHEN 'E' THEN 'EUR'
                                       END
                END
                -- EMISORES ESPECIFICOS
                SELECT TOP 1 @LID_EMISOR_ESPECIFICO = ID_EMISOR_ESPECIFICO
                FROM EMISORES_ESPECIFICO  WITH (NOLOCK)
                WHERE COD_SVS_NEMOTECNICO =SUBSTRING(@LNEMOTECNICO,3,4)
                -- NEMOTECNICO
                EXEC [DBO].[PKG_NEMOTECNICOS$GENERACION_NEMOTECNICO_SVS] @PID_EMISOR_ESPECIFICO = @LID_EMISOR_ESPECIFICO ,
                                                                         @PFECHA_VENCIMIENTO    = @LFECHA ,
                                                                         @PID_MONEDA_DEPOSITO   = @LID_MONEDADEPOSITO ,
                                                                         @PID_MONEDA_PAGO       = @LID_MONEDAPAGO ,
                                                                         @PNEMOTECNICO          = @LNEMOTECNICOFIN OUTPUT,
                                                                         @PMGS                  = @LMGS OUTPUT
                IF RTRIM(LTRIM(@LMGS)) <> ''
                BEGIN
					SET @LERRORNOCUENTA = 'S'
                    SET @LOUTPUTLOG_SCTAS_FLG = 1 --TRUE
                    SET @LCODRESULTADO = 'ERROR'
                    SET @LMSGRESULTADO = 'ERROR: PROBLEMAS AL BUSCAR NEMOT�CNICO (DEPOSITOS) ' + RTRIM(LTRIM(@LNEMOTECNICO))
                END
                ELSE
                BEGIN
                    SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO
                    FROM DBO.VIEW_NEMOTECNICOS WITH (NOLOCK)
                    WHERE DBO.VIEW_NEMOTECNICOS.NEMOTECNICO = RTRIM(LTRIM(@LNEMOTECNICOFIN))
                END
			END
		END
        ELSE IF (SUBSTRING(@LNEMOTECNICO,1,4) IN ('PDBC','PRBC'))
        BEGIN
			-- FECHA DE VENCIMIENTO
            SET @LFECHA = CAST('20' + SUBSTRING(@LNEMOTECNICO,11,2) + SUBSTRING(@LNEMOTECNICO,9,2) + SUBSTRING(@LNEMOTECNICO,7,2) AS DATETIME)
            -- CODIGO DE INSTRUMENTO
            SET @LCODINSTRUMENTO = 'DEPOSITO_NAC'
            -- MONEDA DEPOSITO
            SELECT TOP 1 @LID_MONEDADEPOSITO = ID_MONEDA
            FROM MONEDAS WITH (NOLOCK)
            WHERE COD_MONEDA = CASE SUBSTRING(@LNEMOTECNICO,1,4) WHEN 'PDBC' THEN '$$'
                                                                 WHEN 'PRBC' THEN 'UF' END
            -- EMISORES ESPECIFICOS
            SELECT TOP 1 @LID_EMISOR_ESPECIFICO = ID_EMISOR_ESPECIFICO
            FROM EMISORES_ESPECIFICO WITH (NOLOCK)
            WHERE COD_SVS_NEMOTECNICO = 'CEN'
            -- NEMOTECNICO
            EXEC [DBO].[PKG_NEMOTECNICOS$GENERACION_NEMOTECNICO_SVS] @PID_EMISOR_ESPECIFICO = @LID_EMISOR_ESPECIFICO ,
                                                                     @PFECHA_VENCIMIENTO    = @LFECHA ,
                                                                     @PID_MONEDA_DEPOSITO   = @LID_MONEDADEPOSITO ,
                                                                     @PID_MONEDA_PAGO       = @LID_MONEDAPAGO ,
                                                                     @PNEMOTECNICO          = @LNEMOTECNICOFIN OUTPUT,
                                                                     @PMGS                  = @LMGS OUTPUT
            IF RTRIM(LTRIM(@LMGS)) <> ''
            BEGIN
				SET @LERRORNOCUENTA = 'S'
                SET @LOUTPUTLOG_SCTAS_FLG = 1 --TRUE
                SET @LCODRESULTADO = 'ERROR'
                SET @LMSGRESULTADO = 'ERROR: PROBLEMAS AL BUSCAR NEMOT�CNICO (DEPOSITOS) ' + RTRIM(LTRIM(@LNEMOTECNICO))
            END
            ELSE
            BEGIN
                SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO
                FROM DBO.VIEW_NEMOTECNICOS  WITH (NOLOCK)
                WHERE DBO.VIEW_NEMOTECNICOS.NEMOTECNICO = RTRIM(LTRIM(@LNEMOTECNICOFIN))
            END
		END
        ELSE
        BEGIN
			SET @LERRORNOCUENTA = 'S'
            SET @LOUTPUTLOG_SCTAS_FLG = 1 --TRUE
            SET @LCODRESULTADO = 'ERROR'
            SET @LMSGRESULTADO = 'ERROR: PROBLEMAS AL BUSCAR NEMOT�CNICO ' + RTRIM(LTRIM(@LNEMOTECNICO))
		END

        SET @LID_MOV_ACTIVO = NULL

        /***********************************************************************************************************/
        /***********************************************************************************************************/

        --IF RTRIM(LTRIM(@LCODINSTRUMENTO)) <> 'BONOS_NAC'

        IF EXISTS(SELECT 1 FROM CUENTAS_ACHS WHERE ID_CUENTA = @LID_CUENTA)
        BEGIN
			SELECT @LID_MOV_ACTIVO = ID_MOV_ACTIVO
            FROM  VIEW_SALDOS_ACTIVOS WITH (NOLOCK)
            WHERE FECHA_CIERRE    = @PFECHA_SALDO_ACTIVO
              AND ID_CUENTA       = @LID_CUENTA
              AND ID_NEMOTECNICO  = @LID_NEMOTECNICO
              AND ID_MOV_ACTIVO NOT IN (SELECT ID_MOV_ACTIVO FROM #TABLA2 WITH (NOLOCK) WHERE ID_MOV_ACTIVO IS NOT NULL)

            UPDATE #TABLA2 SET COD_INSTRUMENTO      = @LCODINSTRUMENTO,
                               ID_NEMOTECNICO       = @LID_NEMOTECNICO,
                               ID_CUENTA            = @LID_CUENTA,
                               ID_MOV_ACTIVO        = @LID_MOV_ACTIVO,
            /*----------------------------------------------------------------*/
                               NUM_CUENTA           = @NUMCUENTA,
                               ID_ASESOR            = @ID_ASESOR,
                               FLG_BLOQUEADO        = @FLG_BLOQUEADO,
                               LERRORNOCUENTA       = @LERRORNOCUENTA,
                               LLOG_SCTAS_FLG       = @LOUTPUTLOG_SCTAS_FLG,
                               DSC_BLOQUEO          = @DSC_BLOQUEO,
                               GRABADO              = @LGRABADO,
                               LCODRESULTADO        = @LCODRESULTADO ,
                               LMSGRESULTADO        = @LMSGRESULTADO
            WHERE RUT_CLIENTE  = @LRUT_CLIENTE
              AND NEMOTECNICO  = @LNEMOTECNICO
              AND NUMERO_ORDEN = @LNUMEROORDEN
              AND CORRELATIVO  = @LCORRELATIVO
		END
        ELSE
        BEGIN
			UPDATE #TABLA2 SET COD_INSTRUMENTO = @LCODINSTRUMENTO,
                               ID_NEMOTECNICO  = @LID_NEMOTECNICO,
                               ID_CUENTA       = @LID_CUENTA,
                               NUM_CUENTA      = @NUMCUENTA,
                               ID_ASESOR       = @ID_ASESOR,
							   FLG_BLOQUEADO   = @FLG_BLOQUEADO,
                               LERRORNOCUENTA  = @LERRORNOCUENTA,
                               LLOG_SCTAS_FLG  = @LOUTPUTLOG_SCTAS_FLG,
                               DSC_BLOQUEO     = @DSC_BLOQUEO,
                               GRABADO         = @LGRABADO,
                               LCODRESULTADO   = @LCODRESULTADO,
                               LMSGRESULTADO   = @LMSGRESULTADO
			WHERE NEMOTECNICO = @LNEMOTECNICO
              AND RUT_CLIENTE = @LRUT_CLIENTE
              AND NUMERO_ORDEN = @LNUMEROORDEN
              AND CORRELATIVO  = @LCORRELATIVO
		END
        SET @LACUMULA_CANTIDAD = 0
        SET @LACUMULA_CANTIDAD = @LACUMULA_CANTIDAD + @LCANTIDAD
        SET @LREGISTROS = @LREGISTROS + 1

        --LECTURA DE LA SIGUIENTE FILA DE UN CURSOR
        FETCH CUR_OPERACION INTO @LNUM_FILA,
                                 @LNEMOTECNICO,
                                 @LRUT_CLIENTE,
                                 @LNUMEROORDEN,
                                 @LCANTIDAD,
                                 @LCORRELATIVO,
                                 @LCOD_INSTRU_SVS
	END

	-- CIERRA EL CURSOR
	CLOSE CUR_OPERACION
	DEALLOCATE CUR_OPERACION

	-- TABLA RESULTANTE
	SELECT NUMERO_FILA,           RUT_CLIENTE,         TIPO_OPERACION,           NUMERO_ORDEN,           CORRELATIVO,
           NEMOTECNICO,           CANTIDAD,            PRECIO,                   MONTO_NETO,             COMISION,
           DERECHO,               GASTOS,              IVA,                      MONTO_OPERACION,        FECHA_LIQUIDACION,
           NOMBRE_CLIENTE,        FACTURA,             GARANTIAS,                PRESTAMOS,              SIMULTANEAS,
           BOLSA,                 TIPO_LIQ,            CUST_NOM,                 COD_INSTRUMENTO,        ID_NEMOTECNICO,
           ID_CUENTA ,            NUM_CUENTA,          ID_ASESOR,                FLG_BLOQUEADO,          LERRORNOCUENTA,
           LLOG_SCTAS_FLG,        DSC_BLOQUEO,         ID_MOV_ACTIVO,            GRABADO,                LCODRESULTADO,
           LMSGRESULTADO,         ISNULL(NOMBRE_INSTRUMENTO_FECU,'') 'NOMBRE_INSTRUMENTO_FECU'
	FROM  #TABLA2  WITH (NOLOCK)
	WHERE TIPO_OPERACION IN ('C','V')
	AND   NOT EXISTS (SELECT 1
                      FROM REL_CONVERSIONES
                      WHERE ID_ORIGEN = (SELECT ID_ORIGEN FROM ORIGENES WITH (NOLOCK) WHERE COD_ORIGEN = 'AD_QRY_CB_MOVTO_RFIJA')
                      AND ID_TIPO_CONVERSION = (SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WITH (NOLOCK) WHERE TABLA = 'OPERACIONES_DETALLE')
                      AND VALOR = RTRIM(CONVERT(VARCHAR(50),NUMERO_ORDEN)) + '/' + RTRIM(CONVERT(VARCHAR(50),CORRELATIVO)))
	ORDER BY TIPO_OPERACION, ID_CUENTA,NUMERO_ORDEN, CORRELATIVO

	DROP TABLE #TABLA1
	DROP TABLE #TABLA2
	SET NOCOUNT OFF
END
GO

GRANT EXECUTE ON [PKG_PROCESOS_IMPORTADOR$CISCB_CB_MOVTO_RFIJA_II] TO DB_EXECUTESP
GO