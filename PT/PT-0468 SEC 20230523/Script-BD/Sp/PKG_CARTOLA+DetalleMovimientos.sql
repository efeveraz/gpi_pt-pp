IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_CARTOLA$DetalleMovimientos]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_CARTOLA$DetalleMovimientos]
GO

CREATE PROCEDURE PKG_CARTOLA$DetalleMovimientos
( @PCONSOLIDADO    VARCHAR(3)
, @PID_ENTIDAD     NUMERIC
, @PFECHA_INICIO   DATETIME = NULL
, @PFECHA_FIN      DATETIME = NULL
, @PID_EMPRESA     NUMERIC = NULL
)AS
BEGIN
 SET NOCOUNT ON
 DECLARE @TBLCUENTAS TABLE (ID_CUENTA NUMERIC
                          , ID_MONEDA VARCHAR(3)
                          , NUM_CUENTA VARCHAR(15))

 IF @PCONSOLIDADO = 'CLT'
  BEGIN
      INSERT INTO @TBLCUENTAS
      SELECT ID_CUENTA, ID_MONEDA, NUM_CUENTA
        FROM CUENTAS
       WHERE ID_CLIENTE = @PID_ENTIDAD
         AND COD_ESTADO='H'
         AND ID_EMPRESA = ISNULL(@PID_EMPRESA,ID_EMPRESA)
  END
 ELSE
  IF @PCONSOLIDADO = 'GRP'
   BEGIN
        INSERT INTO @TBLCUENTAS
        SELECT ID_CUENTA, ID_MONEDA, NUM_CUENTA
          FROM CUENTAS
         WHERE ID_CUENTA IN (SELECT ID_CUENTA FROM REL_CUENTAS_GRUPOS_CUENTAS WHERE ID_GRUPO_CUENTA = @PID_ENTIDAD)
           AND COD_ESTADO='H'
           AND ID_EMPRESA = ISNULL(@PID_EMPRESA,ID_EMPRESA)
   END
 ELSE
   BEGIN
        INSERT INTO @TBLCUENTAS
        SELECT ID_CUENTA, ID_MONEDA, NUM_CUENTA
          FROM CUENTAS
         WHERE ID_CUENTA = ISNULL(@PID_ENTIDAD,ID_CUENTA)
           AND COD_ESTADO='H'
           AND ID_EMPRESA = ISNULL(@PID_EMPRESA,ID_EMPRESA)
   END

    SELECT MA.FECHA_OPERACION              AS FECHA_OPERACION
         , OD.FECHA_LIQUIDACION
         , MA.FLG_TIPO_MOVIMIENTO          AS TIPO_MOVIMIENTO
         , CASE OD.COD_TIPO_OPERACION
               WHEN 'VENC_IIF'   THEN 'VENCIMIENTO'
               WHEN 'VENC_RF'    THEN 'VENCIMIENTO'
               WHEN 'VENC_RF_NS' THEN 'VENCIMIENTO'
               WHEN 'SORTEOLET'  THEN 'SORTEOS'
               WHEN 'CUST_DIVFM' THEN 'REINVERSIÓN'
               WHEN 'CUST_NOCAP' THEN CASE MA.FLG_TIPO_MOVIMIENTO
                                         WHEN 'I' THEN 'INGRESO'
                                         ELSE 'EGRESO'
                                      END
               WHEN 'CUST'       THEN CASE MA.FLG_TIPO_MOVIMIENTO
                                         WHEN 'I' THEN 'INGRESO'
                                         ELSE 'EGRESO'
                                      END
               WHEN 'DIV_ULLA'   THEN 'DIVIDENDOS'
               WHEN 'CUST_RETOP' THEN 'OPCIONES'
               WHEN 'CUST_SUSOP' THEN 'OPCIONES'
               ELSE
                  CASE MA.FLG_TIPO_MOVIMIENTO
                     WHEN 'I' THEN
                        CASE I.COD_PRODUCTO
                           WHEN 'FFMM_NAC' THEN 'INVERSION'
                           ELSE 'COMPRA'
                        END
                     ELSE
                        CASE I.COD_PRODUCTO
                           WHEN 'FFMM_NAC' THEN 'RESCATE'
                           ELSE 'VENTA'
                        END
                  END
           END                            AS DSC_TIPO_MOVIMIENTO
         , CU.NUM_CUENTA
         , OD.ID_OPERACION                AS ID_OPERACION
         , OD.FACTURA                     AS FACTURA
         , SUBSTRING(N.NEMOTECNICO,1,20)  AS NEMOTECNICO
         , MA.CANTIDAD                    AS CANTIDAD
         , M.SIMBOLO                      AS MONEDA
         , M2.DICIMALES_MOSTRAR           AS DECIMALES_MOSTRAR
         , MA.PRECIO                      AS PRECIO
         , OD.COMISION                    AS COMISION
         , OD.DERECHOS                    AS DERECHOS
         , OD.GASTOS                      AS GASTOS
         , OD.IVA                         AS IVA
         , MA.MONTO                       AS MONTO
         , OD.MONTO_OPERACION             AS MONTO_TOTAL_OPERACION
         , TIOP.DSC_TIPO_OPERACION        AS DSC_TIPO_OPERACION
         , OD.CONTRAPARTE                 AS CONTRAPARTE
    FROM @TBLCUENTAS CU
    INNER JOIN MOV_ACTIVOS MA         ON MA.ID_CUENTA = CU.ID_CUENTA
    INNER JOIN (SELECT O.ID_CUENTA
                     , O.ID_OPERACION
                     , D.ID_OPERACION_DETALLE
                     , O.FECHA_LIQUIDACION
                     , O.COD_TIPO_OPERACION
                     , T.DSC_TIPO_OPERACION
                     , O.MONTO_OPERACION
                     , ISNULL(R.COMISION,0) COMISION
                     , ISNULL(R.DERECHOS,0) DERECHOS
                     , ISNULL(R.GASTOS,0)   GASTOS
                     , ISNULL(R.IVA,0)      IVA
                     , ISNULL(R.FACTURA,0)  FACTURA
                     , CASE
                           WHEN (SELECT CO.FLG_EMPRESA_SEC FROM CONTRAPARTES CO WHERE CO.ID_CONTRAPARTE = O.ID_CONTRAPARTE) = 'S' THEN 'S'
                           ELSE NULL
                           END 'CONTRAPARTE'
                 FROM OPERACIONES O  , OPERACIONES_DETALLE D
                 LEFT OUTER JOIN REL_OPERACION_DETALLE_ATRIBUTOS R ON R.ID_OPERACION_DETALLE = D.ID_OPERACION_DETALLE , TIPOS_OPERACIONES T
                 WHERE D.ID_OPERACION = O.ID_OPERACION
                   AND T.COD_TIPO_OPERACION = O.COD_TIPO_OPERACION) OD ON OD.ID_OPERACION_DETALLE = MA.ID_OPERACION_DETALLE
    INNER JOIN TIPOS_OPERACIONES TIOP ON OD.COD_TIPO_OPERACION = TIOP.COD_TIPO_OPERACION
    INNER JOIN NEMOTECNICOS N         ON MA.ID_NEMOTECNICO = N.ID_NEMOTECNICO
    INNER JOIN INSTRUMENTOS I         ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
    INNER JOIN EMISORES_ESPECIFICO EE ON N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
    INNER JOIN MONEDAS M              ON M.ID_MONEDA = N.ID_MONEDA
    INNER JOIN MONEDAS M2             ON M2.ID_MONEDA = N.ID_MONEDA_TRANSACCION
    WHERE MA.COD_ESTADO <> 'A'
      AND ((@PFECHA_INICIO IS NULL) OR (MA.FECHA_OPERACION >= @PFECHA_INICIO))
      AND ((@PFECHA_FIN IS NULL) OR (MA.FECHA_OPERACION <= @PFECHA_FIN))
      AND NOT OD.COD_TIPO_OPERACION IN ('VENC_CUPON', 'CUST_PSH')

    UNION ALL

    SELECT MA.FECHA_OPERACION              AS FECHA_OPERACION
         , OD.FECHA_LIQUIDACION
         , MA.FLG_TIPO_MOVIMIENTO          AS TIPO_MOVIMIENTO
         , CASE OD.COD_TIPO_OPERACION
               WHEN 'VENC_IIF'   THEN 'VENCIMIENTO'
               WHEN 'VENC_CUPON' THEN 'VENCIMIENTO'
               WHEN 'VENC_RF'    THEN 'VENCIMIENTO'
               WHEN 'VENC_RF_NS' THEN 'VENCIMIENTO'
               WHEN 'SORTEOLET'  THEN 'SORTEOS'
               WHEN 'CUST_DIVFM' THEN 'REINVERSIÓN'
               WHEN 'CUST_NOCAP' THEN
                  CASE MA.FLG_TIPO_MOVIMIENTO
                     WHEN 'I' THEN 'INGRESO'
                     ELSE 'EGRESO'
                  END
               WHEN 'CUST' THEN
                  CASE MA.FLG_TIPO_MOVIMIENTO
                     WHEN 'I' THEN 'INGRESO'
                     ELSE 'EGRESO'
                  END
               WHEN 'DIV_ULLA'   THEN 'DIVIDENDOS'
               WHEN 'CUST_RETOP' THEN 'OPCIONES'
               WHEN 'CUST_SUSOP' THEN 'OPCIONES'
               ELSE
                  CASE MA.FLG_TIPO_MOVIMIENTO
                     WHEN 'I' THEN
                        CASE I.COD_PRODUCTO
                           WHEN 'FFMM_NAC' THEN 'INVERSION'
                           ELSE 'COMPRA'
                        END
                     ELSE
                        CASE I.COD_PRODUCTO
                           WHEN 'FFMM_NAC' THEN 'RESCATE'
                           ELSE 'VENTA'
                        END
                  END
           END                            AS DSC_TIPO_MOVIMIENTO
         , CU.NUM_CUENTA
         , OD.ID_OPERACION                AS ID_OPERACION
         , OD.FACTURA                     AS FACTURA
         , SUBSTRING(N.NEMOTECNICO,1,20)  AS NEMOTECNICO
         , MA.CANTIDAD                    AS CANTIDAD
         , M.SIMBOLO                      AS MONEDA
         , M2.DICIMALES_MOSTRAR           AS DECIMALES_MOSTRAR
         , MA.PRECIO                      AS PRECIO
         , OD.COMISION                    AS COMISION
         , OD.DERECHOS                    AS DERECHOS
         , OD.GASTOS                      AS GASTOS
         , OD.IVA                         AS IVA
         , MA.MONTO                       AS MONTO
         , OD.MONTO_OPERACION             AS MONTO_TOTAL_OPERACION
         , TIOP.DSC_TIPO_OPERACION        AS DSC_TIPO_OPERACION
         , OD.CONTRAPARTE                 AS CONTRAPARTE
    FROM @TBLCUENTAS CU
    INNER JOIN MOV_ACTIVOS MA         ON MA.ID_CUENTA = CU.ID_CUENTA
    INNER JOIN (SELECT O.ID_CUENTA
                     , O.ID_OPERACION
                     , D.ID_OPERACION_DETALLE
                     , O.FECHA_LIQUIDACION
                     , O.COD_TIPO_OPERACION
                     , T.DSC_TIPO_OPERACION
                     , O.MONTO_OPERACION
                     , O.COD_PRODUCTO
                     , O.COD_ESTADO
                     , ISNULL(R.COMISION,0) COMISION
                     , ISNULL(R.DERECHOS,0) DERECHOS
                     , ISNULL(R.GASTOS,0)   GASTOS
                     , ISNULL(R.IVA,0)      IVA
                     , ISNULL(R.FACTURA,0)  FACTURA
                     , CASE
                           WHEN (SELECT CO.FLG_EMPRESA_SEC FROM CONTRAPARTES CO WHERE CO.ID_CONTRAPARTE = O.ID_CONTRAPARTE) = 'S' THEN 'S'
                           ELSE NULL
                           END 'CONTRAPARTE'
                 FROM OPERACIONES O  , OPERACIONES_DETALLE D
                 LEFT OUTER JOIN REL_OPERACION_DETALLE_ATRIBUTOS R ON R.ID_OPERACION_DETALLE = D.ID_OPERACION_DETALLE , TIPOS_OPERACIONES T
                 WHERE D.ID_OPERACION = O.ID_OPERACION
                   AND T.COD_TIPO_OPERACION = O.COD_TIPO_OPERACION) OD ON OD.ID_OPERACION_DETALLE = MA.ID_OPERACION_DETALLE
    INNER JOIN TIPOS_OPERACIONES TIOP ON OD.COD_TIPO_OPERACION = TIOP.COD_TIPO_OPERACION
    INNER JOIN NEMOTECNICOS N         ON MA.ID_NEMOTECNICO = N.ID_NEMOTECNICO
    INNER JOIN INSTRUMENTOS I         ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
    INNER JOIN EMISORES_ESPECIFICO EE ON N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
    INNER JOIN MONEDAS M              ON M.ID_MONEDA = N.ID_MONEDA
    INNER JOIN MONEDAS M2             ON M2.ID_MONEDA = N.ID_MONEDA_TRANSACCION
    WHERE MA.COD_ESTADO = 'A'
      AND OD.COD_ESTADO = 'C'
      AND OD.COD_PRODUCTO in ('FFMM_INT','RF_INT','RV_INT')
      AND ((@PFECHA_INICIO IS NULL) OR (MA.FECHA_OPERACION >= @PFECHA_INICIO))
      AND ((@PFECHA_FIN IS NULL) OR (MA.FECHA_OPERACION <= @PFECHA_FIN))
      AND NOT OD.COD_TIPO_OPERACION IN ('CUST_PSH')


    UNION ALL

     SELECT MC.FECHA_CIERRE               AS FECHA_OPERACION
          , MC.FECHA_LIQUIDACION
          , MC.FLG_TIPO_MOVIMIENTO        AS TIPO_MOVIMIENTO
          , 'CORTE CUPON'                 AS DSC_TIPO_MOVIMIENTO
          , CU.NUM_CUENTA
          , 0                             AS ID_OPERACION
          , 0                             AS FACTURA
          , SUBSTRING(N.NEMOTECNICO,1,20) AS NEMOTECNICO
          , MC.CANTIDAD                   AS CANTIDAD
          , M.SIMBOLO                     AS MONEDA
          , M2.DICIMALES_MOSTRAR          AS DECIMALES_MOSTRAR
          , MC.PRECIO                     AS PRECIO
          , 0                             AS COMISION
          , 0                             AS DERECHOS
          , 0                             AS GASTOS
          , 0                             AS IVA
          , MC.MONTO                      AS MONTO
          , MC.MONTO                      AS MONTO_TOTAL_OPERACION
          , MC.DSC_MOV_CAJA               AS DSC_TIPO_OPERACION
          , NULL                          AS CONTRAPARTE
     FROM MOV_CAJA MC
     INNER JOIN CAJAS_CUENTA CC        ON CC.ID_CAJA_CUENTA = MC.ID_CAJA_CUENTA
     INNER JOIN @TBLCUENTAS  CU        ON CU.ID_CUENTA = CC.ID_CUENTA
     INNER JOIN NEMOTECNICOS N         ON N.ID_NEMOTECNICO  =MC.ID_NEMOTECNICO
     INNER JOIN EMISORES_ESPECIFICO EE ON N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
     INNER JOIN MONEDAS M              ON M.ID_MONEDA = N.ID_MONEDA
     INNER JOIN MONEDAS M2             ON M2.ID_MONEDA = N.ID_MONEDA_TRANSACCION
     WHERE MC.COD_ESTADO <> 'A'
       AND ((@PFECHA_INICIO IS NULL) OR (MC.FECHA_CIERRE >= @PFECHA_INICIO))
       AND ((@PFECHA_FIN IS NULL) OR (MC.FECHA_CIERRE <= @PFECHA_FIN))
       AND MC.COD_ORIGEN_MOV_CAJA = 'CORTECUPON'
       AND MC.DSC_MOV_CAJA like ('CORTE DE CUPON DEL %')

   UNION ALL

     SELECT D.FECHA                        AS FECHA_OPERACION
          , D.FECHA_CORTE
          , 'E'                            AS TIPO_MOVIMIENTO
          , D.TIPO_VARIACION               AS DSC_TIPO_MOVIMIENTO
          , S.NUM_CUENTA
          , 0                              AS ID_OPERACION
          , 0                              AS FACTURA
          , SUBSTRING(D.NEMOTECNICO,1,20)  AS NEMOTECNICO
          , S.CANTIDAD                     AS CANTIDAD
          , M.SIMBOLO                      AS MONEDA
          , M2.DICIMALES_MOSTRAR           AS DECIMALES_MOSTRAR
          , (D.MONTO/S.CANTIDAD)           AS PRECIO
          , 0                              AS COMISION
          , 0                              AS DERECHOS
          , 0                              AS GASTOS
          , 0                              AS IVA
          , (S.cantidad * D.monto)         AS MONTO
          , (S.cantidad * D.monto)         AS MONTO_TOTAL_OPERACION
          , D.TIPO_VARIACION               AS DSC_TIPO_OPERACION
          , NULL                           AS CONTRAPARTE
     FROM VIEW_DIVIDENDOS D
     INNER JOIN view_saldos_activos S  ON S.FECHA_CIERRE = D.FECHA and S.ID_NEMOTECNICO = D.ID_NEMOTECNICO
     INNER JOIN NEMOTECNICOS N         ON N.ID_NEMOTECNICO = D.ID_NEMOTECNICO
     INNER JOIN EMISORES_ESPECIFICO EE ON N.ID_EMISOR_ESPECIFICO = EE.ID_EMISOR_ESPECIFICO
     INNER JOIN MONEDAS M              ON M.ID_MONEDA = N.ID_MONEDA
     INNER JOIN MONEDAS M2             ON M2.ID_MONEDA = N.ID_MONEDA_TRANSACCION
     WHERE ((@PFECHA_INICIO IS NULL) OR (D.FECHA>= @PFECHA_INICIO))
       AND ((@PFECHA_FIN IS NULL) OR (D.FECHA <= @PFECHA_FIN))
       AND EXISTS (SELECT 1 FROM @TBLCUENTAS C WHERE C.ID_CUENTA = S.ID_CUENTA )
   ORDER BY 1
          , 2
          , 7
          , 8
          , 5

 SET NOCOUNT OFF
END
GO

GRANT EXECUTE ON [PKG_CARTOLA$DetalleMovimientos] TO DB_EXECUTESP
GO