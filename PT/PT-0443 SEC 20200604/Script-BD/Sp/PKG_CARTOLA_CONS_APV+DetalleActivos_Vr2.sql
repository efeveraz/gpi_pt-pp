USE CSGPI
GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_CARTOLA_CONS_APV$DetalleActivos_Vr2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_CARTOLA_CONS_APV$DetalleActivos_Vr2]
GO
CREATE PROCEDURE [dbo].[PKG_CARTOLA_CONS_APV$DetalleActivos_Vr2]
( @PFECHA_CIERRE           DATETIME
, @PID_ARBOL_CLASE         NUMERIC = NULL
, @PCONSOLIDADO            VARCHAR(3)
, @PID_ENTIDAD             NUMERIC
, @PID_MONEDA_SALIDA       NUMERIC = NULL
, @PDSC_ARBOL_CLASE_INST   VARCHAR(100)= NULL
, @PCODIGO                 VARCHAR(20) = NULL
, @PCODIGO_PADRE           VARCHAR(20) = NULL
, @PID_EMPRESA             NUMERIC = NULL
) AS
BEGIN
    SET NOCOUNT ON
    DECLARE @LID_EMPRESA     NUMERIC
          , @LCODIGO_ARBOL   VARCHAR(20)
          , @LID_MONEDA_USD  NUMERIC
          , @LTOTAL_INT      NUMERIC(28,8)
          , @LTOTAL          NUMERIC(28,8)
          , @PID_CUENTA      NUMERIC
          , @PID_NEMOTECNICO NUMERIC
          , @PID_MERCADO_TRANSACCION NUMERIC
          , @PID_MONEDA      NUMERIC
          , @PID_SECTOR      NUMERIC
          , @LORIGEN     VARCHAR(20)
          , @LDECIMALES      NUMERIC
		   , @LCODIGO_PADRE_ARBOL VARCHAR(20)

    DECLARE @LDSC_ARBOL VARCHAR(50)
          , @LDSC_PADRE VARCHAR(50)

	DECLARE @LDSC_ARBOL_CLASE VARCHAR(50)
		  , @LDSC_PADRE_CLASE VARCHAR(50)

     SELECT @LDECIMALES = DICIMALES_MOSTRAR
       FROM MONEDAS
      WHERE ID_MONEDA=@PID_MONEDA_SALIDA


    SELECT @LID_MONEDA_USD = DBO.FNT_DAMEIDMONEDA('USD')

    DECLARE @TBLACI TABLE (ID_ARBOL_CLASE_INST NUMERIC)

    DECLARE @TBLNEMOS TABLE (ID_NEMOTECNICO NUMERIC)

    DECLARE @TBLCUENTAS TABLE (ID_CUENTA NUMERIC, ID_EMPRESA NUMERIC)

    IF @PCONSOLIDADO = 'CLT'
    BEGIN
          INSERT INTO @TBLCUENTAS
          SELECT ID_CUENTA, ID_EMPRESA
            FROM VIEW_CUENTAS_VIGENTES
           WHERE ID_CLIENTE = @PID_ENTIDAD
            AND ID_EMPRESA = ISNULL(@PID_EMPRESA, ID_EMPRESA)
         ORDER BY ID_CUENTA
    END
    ELSE
       IF @PCONSOLIDADO = 'GRP'
        BEGIN
            INSERT INTO @TBLCUENTAS
            SELECT ID_CUENTA   , ID_EMPRESA
              FROM VIEW_CUENTAS_VIGENTES
             WHERE ID_CUENTA IN (SELECT ID_CUENTA FROM REL_CUENTAS_GRUPOS_CUENTAS WHERE ID_GRUPO_CUENTA = @PID_ENTIDAD)
               AND ID_EMPRESA = ISNULL(@PID_EMPRESA, ID_EMPRESA)
         ORDER BY ID_CUENTA
        END
       ELSE
         BEGIN
            INSERT INTO @TBLCUENTAS
            SELECT ID_CUENTA, ID_EMPRESA
              FROM CUENTAS
             WHERE ID_CUENTA=@PID_ENTIDAD
               AND ID_EMPRESA = ISNULL(@PID_EMPRESA, ID_EMPRESA)
         END

    IF ISNULL(@PID_ARBOL_CLASE,0) = 0
     BEGIN
          INSERT INTO @TBLACI
          SELECT ID_ARBOL_CLASE_INST
          FROM (SELECT ID_ARBOL_CLASE_INST, CODIGO
                     , ID_EMPRESA
                     , DSC_ARBOL_CLASE_INST
                     , (SELECT CODIGO
                          FROM ARBOL_CLASE_INSTRUMENTO
                         WHERE ID_ARBOL_CLASE_INST= A.ID_PADRE_ARBOL_CLASE_INST)    CODIGO_PADRE
					 , ID_ACI_TIPO
          FROM ARBOL_CLASE_INSTRUMENTO A  ) T
         WHERE DSC_ARBOL_CLASE_INST = @PDSC_ARBOL_CLASE_INST
           AND CODIGO_PADRE =@PCODIGO_PADRE
           AND ID_EMPRESA IN (SELECT DISTINCT ID_EMPRESA FROM @TBLCUENTAS)
		   AND ID_ACI_TIPO = 2
     END
    ELSE
	begin
		 SELECT @LDSC_ARBOL_CLASE =AT.DSC_ARBOL_CLASE_INST
		       , @LCODIGO_PADRE_ARBOL = (SELECT CODIGO
			                            FROM ARBOL_CLASE_INSTRUMENTO
										WHERE ID_ARBOL_CLASE_INST = AT.ID_PADRE_ARBOL_CLASE_INST)
			   ,@LDSC_PADRE_CLASE =  (SELECT DSC_ARBOL_CLASE_INST
			                            FROM ARBOL_CLASE_INSTRUMENTO
										WHERE ID_ARBOL_CLASE_INST = AT.ID_PADRE_ARBOL_CLASE_INST)
		  FROM ARBOL_CLASE_INSTRUMENTO AT
		 WHERE ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE

		 IF @LDSC_ARBOL_CLASE ='Fondos Mutuos' AND @LCODIGO_PADRE_ARBOL ='RF' OR  @LDSC_ARBOL_CLASE ='Fondos Mutuos Largo Plazo' AND @LCODIGO_PADRE_ARBOL ='RF' OR @LDSC_ARBOL_CLASE ='Fondos Mutuos Corto Plazo' AND @LCODIGO_PADRE_ARBOL ='RF'
		 BEGIN

		  INSERT INTO @TBLACI
		  SELECT ID_ARBOL_CLASE_INST
		   FROM ARBOL_CLASE_INSTRUMENTO
		   WHERE DSC_ARBOL_CLASE_INST IN('Fondos Mutuos','Fondos Mutuos Largo Plazo', 'Fondos Mutuos Corto Plazo')
		   AND ID_PADRE_ARBOL_CLASE_INST IN (SELECT ID_ARBOL_CLASE_INST
											  FROM ARBOL_CLASE_INSTRUMENTO
											  WHERE DSC_ARBOL_CLASE_INST = @LDSC_PADRE_CLASE AND ID_ACI_TIPO =2
											  AND ID_EMPRESA = 1  )

		 END
		 ELSE
		 BEGIN

			INSERT INTO @TBLACI
           SELECT @PID_ARBOL_CLASE
		 END


    end
    INSERT INTO @TBLNEMOS
    SELECT DISTINCT ID_NEMOTECNICO
      FROM REL_ACI_EMP_NEMOTECNICO
     WHERE ID_ARBOL_CLASE_INST IN (SELECT ID_ARBOL_CLASE_INST FROM @TBLACI)


    IF @PCONSOLIDADO = 'CTA'
    BEGIN
        SELECT @LCODIGO_ARBOL = CODIGO
             , @LDSC_ARBOL = DSC_ARBOL_CLASE_INST
             , @LDSC_PADRE = (SELECT DSC_ARBOL_CLASE_INST
                                FROM ARBOL_CLASE_INSTRUMENTO
                               WHERE ID_ARBOL_CLASE_INST= A.ID_PADRE_ARBOL_CLASE_INST)
			, @LCODIGO_PADRE_ARBOL =(SELECT CODIGO
										FROM ARBOL_CLASE_INSTRUMENTO
										WHERE ID_ARBOL_CLASE_INST= A.ID_PADRE_ARBOL_CLASE_INST)
          FROM ARBOL_CLASE_INSTRUMENTO A
         WHERE A.ID_ARBOL_CLASE_INST = @PID_ARBOL_CLASE
    END
 ELSE
    BEGIN
        SELECT @LCODIGO_ARBOL =CODIGO
             , @LDSC_ARBOL = DSC_ARBOL_CLASE_INST
             , @LDSC_PADRE =DSC_PADRE
          FROM (SELECT DISTINCT CODIGO
                     , DSC_ARBOL_CLASE_INST
                     , (SELECT DSC_ARBOL_CLASE_INST
               FROM ARBOL_CLASE_INSTRUMENTO
                         WHERE ID_ARBOL_CLASE_INST= A.ID_PADRE_ARBOL_CLASE_INST)   DSC_PADRE
                     , (SELECT CODIGO
                          FROM ARBOL_CLASE_INSTRUMENTO
                         WHERE ID_ARBOL_CLASE_INST= A.ID_PADRE_ARBOL_CLASE_INST)    CODIGO_PADRE
          FROM ARBOL_CLASE_INSTRUMENTO A  ) T
         WHERE DSC_ARBOL_CLASE_INST = @PDSC_ARBOL_CLASE_INST
           AND CODIGO_PADRE =@PCODIGO_PADRE

        SELECT TOP 1 @PID_ARBOL_CLASE = ID_ARBOL_CLASE_INST
          FROM @TBLACI

    END


    DECLARE @salida TABLE( ID_CUENTA        NUMERIC
        , ID_SALDO_ACTIVO                   NUMERIC
        , FECHA_CIERRE                      DATETIME
        , ID_NEMOTECNICO                    NUMERIC
        , NEMOTECNICO                       VARCHAR(50)
        , EMISOR                            VARCHAR(100)
        , COD_EMISOR                        VARCHAR(10)
        , DSC_NEMOTECNICO                   VARCHAR(120)
        , TASA_EMISION_2                    NUMERIC(18,4)
        , CANTIDAD                          NUMERIC(18,4)
        , GARANTIAS                         NUMERIC(18,4)
        , PRESTAMOS                         NUMERIC(18,4)
        , SIMULTANEAS                       NUMERIC(18,4)
        , PRECIO                            NUMERIC(18,6)
        , TASA_EMISION                      NUMERIC(18,4)
        , FECHA_VENCIMIENTO                 DATETIME
        , PRECIO_COMPRA                     NUMERIC(18,4)
        , TASA                              NUMERIC(18,4)
        , TASA_COMPRA                       NUMERIC(18,4)
        , MONTO_VALOR_COMPRA                NUMERIC(18,4)
        , MONTO_MON_CTA                     NUMERIC(18,4)
        , MONTO_MON_USD                     NUMERIC(18,4)
        , ID_MONEDA_CTA                     NUMERIC
        , ID_MONEDA_NEMOTECNICO             NUMERIC
        , SIMBOLO_MONEDA                    VARCHAR(3)
        , MONTO_MON_NEMOTECNICO             NUMERIC(18,4)
        , MONTO_MON_ORIGEN                  NUMERIC(18,4)
        , ID_EMPRESA                        NUMERIC
        , ID_ARBOL_CLASE_INST               NUMERIC
        , COD_INSTRUMENTO                   VARCHAR(15)
        , DSC_ARBOL_CLASE_INST              VARCHAR(100)
        , PORCENTAJE_RAMA                   NUMERIC(18,4)
        , PRECIO_PROMEDIO_COMPRA            NUMERIC(18,4)
        , DSC_PADRE_ARBOL_CLASE_INST        VARCHAR(100)
        , RENTABILIDAD                      NUMERIC(18,4)
        , DIAS                              NUMERIC
        , DURATION                          NUMERIC
        , COD_PRODUCTO                      VARCHAR(10)
        , ID_PADRE_ARBOL_CLASE_INST         NUMERIC
        , DURACION                          FLOAT
        , DSC_CLASIFICADOR_RIESGO           VARCHAR(50)
        , CODIGO                            VARCHAR(20))

    DECLARE @TBLSALIDA_DET TABLE(ORIGEN                         VARCHAR(20)
							, MERCADO							VARCHAR(50)
                            , SIMBOLO_MONEDA                    VARCHAR(20)
                            , COD_PRODUCTO                      VARCHAR(20)
                            , COD_INSTRUMENTO                   VARCHAR(30)
                            , ID_NEMOTECNICO                    NUMERIC
                            , NEMOTECNICO                       VARCHAR(50)
                            , DSC_NEMOTECNICO                   VARCHAR(120)
                            , ID_MONEDA_NEMOTECNICO             NUMERIC
                            , EMISOR                            VARCHAR(120)
                            , COD_EMISOR                        VARCHAR(20)
                            , FECHA_VENCIMIENTO                 DATETIME
                            , CANTIDAD                          NUMERIC(18,4)
                            , PRECIO                            NUMERIC(18,6)
                            , PRECIO_COMPRA                     NUMERIC(18,6)
                            , PRECIO_PROMEDIO_COMPRA            NUMERIC(18,4)
                            , PRECIO_ACTUAL                     NUMERIC(18,6)
                            , MONTO_INVERTIDO                   NUMERIC(18,6)
                            , MONTO_MON_NEMOTECNICO             NUMERIC(18,4)
                            , MONTO_VALOR_COMPRA                NUMERIC(18,4)
                            , VALOR_MERCADO                     NUMERIC(18,6)
                            , UTILIDAD_PERDIDA                  NUMERIC(18,6)
                            , MONTO_MON_CTA                     NUMERIC(18,4)
                            , MONTO_MON_USD                     NUMERIC(18,4)
                            , PORCENTAJE_RAMA                   NUMERIC(18,2)
                            , TOTAL_HOJA                        NUMERIC(18,4)
                            , MONTO_PROMEDIO_COMPRA             NUMERIC(18,4)
                            , RENTABILIDAD                      NUMERIC(18,4)
                            , DURACION                          FLOAT
                            )


    DECLARE @SALIDA_NOTAS TABLE(SIMBOLO_MONEDA                    VARCHAR(10)
                            , COD_PRODUCTO                      VARCHAR(20)
                            , NEMOTECNICO                       VARCHAR(50)
                            , DSC_NEMOTECNICO                   VARCHAR(120)
                            , CANTIDAD                          NUMERIC(18,4)
                            , PRECIO_COMPRA                     NUMERIC(18,4)
                            , MONTO_INVERTIDO                   NUMERIC(18,6)
                            , PRECIO_ACTUAL                     NUMERIC(18,6)
                            , VALOR_MERCADO                     NUMERIC(18,6)
                      , UTILIDAD_PERDIDA                  NUMERIC(18,6)
                            , MONTO_MON_CTA                     NUMERIC(18,4)
                            , MONTO_MON_USD                     NUMERIC(18,4)
                            )


--------------------------------------------------------------------------------------------


	IF @LCODIGO_ARBOL = 'FWD' --OR @LCODIGO_ARBOL = 'VC'
    BEGIN
		--IF @LCODIGO_ARBOL ='FWD'
		--	BEGIN
          SELECT O.NRO_CONTRATO NUMERO_CONTRATO
              , CASE SD.FLG_TIPO_MOVIMIENTO WHEN 'I' THEN 'COMPRA'
                                                    ELSE 'VENTA'
                   END TIPO_FWD
               ,(CASE WHEN SD.COD_MONEDA_PAGAR = '$$' THEN 'PESOS'
                      ELSE SD.COD_MONEDA_PAGAR
                   END) MONEDA_EMISION
               , UPPER((CASE SD.FLG_TIPO_CUMPLIMIENTO WHEN 'E' THEN 'E. Fisica'
                 WHEN 'C' THEN 'COMPENSACI�N'
                                                        ELSE ''
                 END)) MODALIDAD
               , UPPER((CASE WHEN SD.COD_MONEDA_RECIBIR = '$$' THEN 'PESOS'
                                                                 ELSE SD.COD_MONEDA_RECIBIR
                 END)) UNIDAD_ACTIVO_SUBYACENTE
               , UPPER((CASE WHEN SD.COD_MONEDA_RECIBIR = '$$' THEN 0
                                   ELSE 2
                   END)) DECIMALES_ACTIVO_SUBYACENTE
               , ROUND((CASE WHEN NOT SD.COD_MONEDA_RECIBIR = '$$'
                                    THEN SD.MONTO_MON_RECIBIR
                                    ELSE SD.MONTO_MON_PAGAR
                 END),@LDECIMALES) VALOR_NOMINAL
               , SD.FCH_OPERACION FECHA_INICIO
               , SD.FCH_VENCIMIENTO AS FECHA_VENCIMIENTO
               , ROUND(SD.PARIDAD,3) PRECIO_PACTADO
               , ROUND((SD.PARIDAD * (CASE WHEN NOT SD.COD_MONEDA_RECIBIR = '$$'
                                           THEN SD.MONTO_MON_RECIBIR
                                           ELSE SD.MONTO_MON_PAGAR
                 END)),3)VALOR_PRECIO_PACTADO
               , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SD.ID_CUENTA
                                                        , ROUND(SD.VMM,3)
                                                        , DBO.FNT_DAMEIDMONEDA('$$')
                            , @PID_MONEDA_SALIDA
                                                        , @PFECHA_CIERRE) VALOR_MERCADO
            FROM VIEW_SALDOS_DERIVADOS SD
               , OPERACIONES_DERIVADOS O
           WHERE SD.ID_CUENTA IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
             AND SD.FECHA_CIERRE = @PFECHA_CIERRE
             AND SD.COD_INSTRUMENTO = 'FWD_NAC'
             AND O.ID_MOV_DERIVADO = SD.ID_MOV_DERIVADO
          ORDER BY SD.FCH_OPERACION
   --       END

   --       IF @LCODIGO_ARBOL ='VC'
		 -- BEGIN
		 -- SELECT N.NEMOTECNICO
			--, VC.TIPO_OPERACION
			--, VC.CANTIDAD
			--, VC.FECHA_DE_MVTO
			--, VC.FECHA_VENCIMIENTO
			--, VC.TASA
			--, VC.PRECIO_MEDIO
			--, VC.VALOR
			--, VC.PRECIO_MEDIO_MERCADO
			--, VC.VALOR
			--, VC.PRIMA_A_PLAZO
			--, VC.PRIMA_ACUMULADA
			--, VC.DIAS_EN_CURSO
			--, VC.DIAS_OPERACION
			--, DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(VC.ID_CUENTA
			--											, ROUND(VC.VALOR,3)
			--											, DBO.FNT_DAMEIDMONEDA('$$')
			--											, @PID_MONEDA_SALIDA
			--											, @PFECHA_CIERRE)'RESULTADO'
			--FROM VENTA_CORTA VC, NEMOTECNICOS N
			--WHERE N.ID_NEMOTECNICO = VC.ID_NEMOTECNICO
			--AND VC.FECHA_DE_MVTO = @PFECHA_CIERRE
			--AND VC.ID_CUENTA  IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
			--ORDER BY ID_VTA_CORTA

			--END


     END
    ELSE IF @LCODIGO_ARBOL ='VC'

		  BEGIN
		  SELECT N.NEMOTECNICO
			--, VC.TIPO_OPERACION
            , 'Venta Corta' TIPO_OPERACION
			, VC.CANTIDAD
			, VC.FECHA_DE_MVTO
			, VC.FECHA_VENCIMIENTO
			, VC.TASA
			, VC.PRECIO_MEDIO
			, (VC.CANTIDAD * VC.PRECIO_MEDIO) AS VALOR_INI --VC.VALOR AS VALOR_INI
			, VCD.PRECIO_MEDIO_MERCADO
			, (VC.CANTIDAD * VCD.PRECIO_MEDIO_MERCADO) AS VALOR_FIN --VC.VALOR AS VALOR_FIN
			, VC.PRIMA_A_PLAZO
			, VCD.PRIMA_ACUMULADA
			, VCD.DIAS_EN_CURSO --VC.DIAS_EN_CURSO
			, (VC.DIAS_OPERACION - VCD.DIAS_EN_CURSO) DIAS_OPERACION --VC.DIAS_OPERACION
--			, (ISNULL(VC.PRECIO_MEDIO_MERCADO,0) - ISNULL(VC.PRECIO_MEDIO,0) + ISNULL(VC.PRIMA_A_PLAZO,0)) AS 'RESULTADO'
            , ISNULL((VC.CANTIDAD * VC.PRECIO_MEDIO), 0) -
              ISNULL((VC.CANTIDAD * VCD.PRECIO_MEDIO_MERCADO), 0) -
              ISNULL(VCD.PRIMA_ACUMULADA, 0) AS 'RESULTADO'

			FROM VENTA_CORTA VC, NEMOTECNICOS N, VENTA_CORTA_DEVENGO VCD
			WHERE N.ID_NEMOTECNICO = VC.ID_NEMOTECNICO
			AND @PFECHA_CIERRE BETWEEN VC.FECHA_DE_MVTO AND VC.FECHA_VENCIMIENTO
            AND VC.COD_ESTADO = 'C'
            --AND VC.FECHA_DE_MVTO = @PFECHA_CIERRE
			AND VC.ID_CUENTA  IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
            AND VC.ID_VTA_CORTA = VCD.ID_VTA_CORTA
            AND VC.FOLIO = VCD.FOLIO
            AND VC.FECHA_DE_MVTO = VCD.FECHA_DE_MVTO
            AND VCD.FECHA_DE_DEVENGO = @PFECHA_CIERRE

			ORDER BY N.NEMOTECNICO
    END
    ELSE

--------------------------------------------------------------------------------------------
--   SET @PID_CUENTA = @PID_ENTIDAD
     IF UPPER(@LDSC_PADRE) = 'RENTA FIJA INTERNACIONAL' OR UPPER(@LDSC_PADRE) = 'RENTA VARIABLE INTERNACIONAL'
        or (UPPER(@LDSC_PADRE) ='OTROS ACTIVOS' AND UPPER(@LDSC_ARBOL) = 'FONDOS DE INVERSI�N')
        or (UPPER(@LDSC_PADRE) ='OTROS ACTIVOS' AND UPPER(@LDSC_ARBOL) = 'FONDOS MUTUOS MIXTOS')
      BEGIN

                 INSERT INTO @TBLSALIDA_DET (ORIGEN
									  , MERCADO
                                      , SIMBOLO_MONEDA
                                      , COD_PRODUCTO
                                      , NEMOTECNICO
									  , DSC_NEMOTECNICO
                                      , CANTIDAD
                                      , PRECIO_COMPRA
                                      , PRECIO_PROMEDIO_COMPRA
                                      , MONTO_INVERTIDO
                                      , PRECIO_ACTUAL
                                      , VALOR_MERCADO
                                      , UTILIDAD_PERDIDA
                                      , MONTO_MON_CTA
                                      , MONTO_MON_USD
                                      , MONTO_MON_NEMOTECNICO)
                              SELECT 'VALORES'
									, 'VALORES'
                                     , s.MONEDA
                                     , N.COD_PRODUCTO
                                     , N.NEMOTECNICO
                                     , N.DSC_NEMOTECNICO
                                     , S.CANTIDAD
                                     , S.PRECIO_PROMEDIO_COMPRA
                                     , S.PRECIO_PROMEDIO_COMPRA
                                     , S.MONTO_INVERTIDO
                                     , S.PRECIO_MERCADO
                                     , S.VALOR_MERCADO_MON_ORIGEN
                                     , VALOR_MERCADO_MON_ORIGEN - MONTO_INVERTIDO
                                     , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(S.ID_CUENTA
                                                            , VALOR_MERCADO_MON_USD
                                                            , @LID_MONEDA_USD
                                                            , @PID_MONEDA_SALIDA
                                                            , @PFECHA_CIERRE)   MONTO
                                     , S.VALOR_MERCADO_MON_USD
                                     , S.VALOR_MERCADO_MON_ORIGEN
                                 FROM SALDOS_ACTIVOS_INT  S
                                   , (SELECT N.ID_NEMOTECNICO
                                           , N.NEMOTECNICO
                                           , N.DSC_NEMOTECNICO
                                           , N.COD_INSTRUMENTO
                                           , I.COD_PRODUCTO
                                        FROM NEMOTECNICOS N
                                             INNER JOIN INSTRUMENTOS I ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO) N
                                       WHERE S.ID_CUENTA IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
                                         AND S.FECHA_CIERRE = @PFECHA_CIERRE
                                         AND S.ORIGEN = 'INV'
                                         AND S.ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO FROM @TBLNEMOS)
                                         AND N.ID_NEMOTECNICO = S.ID_NEMOTECNICO



                 INSERT INTO @TBLSALIDA_DET (ORIGEN
									  , MERCADO
                                      , SIMBOLO_MONEDA
                                      , COD_PRODUCTO
                                      , NEMOTECNICO
                                      , DSC_NEMOTECNICO
                                      , CANTIDAD
                                      , PRECIO_COMPRA
                                      , PRECIO_PROMEDIO_COMPRA
                                      , MONTO_INVERTIDO
                                      , PRECIO_ACTUAL
                                      , VALOR_MERCADO
                                      , UTILIDAD_PERDIDA
                                      , MONTO_MON_CTA
                                      , MONTO_MON_USD
                                      , MONTO_MON_NEMOTECNICO)
                                 SELECT 'PERSHING'
									  , 'PERSHING'
                                      , s.MONEDA
                                      , N.COD_PRODUCTO
                                      , N.NEMOTECNICO
                                      , N.DSC_NEMOTECNICO
                                      , S.CANTIDAD
                                      , S.PRECIO_PROMEDIO_COMPRA
                                      , S.PRECIO_PROMEDIO_COMPRA
                                      , S.MONTO_INVERTIDO
                                      , S.PRECIO_MERCADO
                      , S.VALOR_MERCADO_MON_ORIGEN
                                      , VALOR_MERCADO_MON_ORIGEN - MONTO_INVERTIDO
                                      , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(S.ID_CUENTA
                                                                             , VALOR_MERCADO_MON_USD
                                                                             , @LID_MONEDA_USD
                                                                             , @PID_MONEDA_SALIDA
                                                                             , @PFECHA_CIERRE)   MONTO
                                      , S.VALOR_MERCADO_MON_USD
                                      , S.VALOR_MERCADO_MON_ORIGEN
                                   FROM SALDOS_ACTIVOS_INT  S
                                      , (SELECT N.ID_NEMOTECNICO
                                              , N.NEMOTECNICO
                                              , N.DSC_NEMOTECNICO
    , N.COD_INSTRUMENTO
                                              , I.COD_PRODUCTO
                                           FROM NEMOTECNICOS N
                                                INNER JOIN INSTRUMENTOS I ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO) N
                                  WHERE S.ID_CUENTA  IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
                                    AND S.ORIGEN    = 'PSH'
                                    AND S.FECHA_CIERRE = @PFECHA_CIERRE
                                    AND S.ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO FROM @TBLNEMOS)
                       AND N.ID_NEMOTECNICO = S.ID_NEMOTECNICO




             SET @LTOTAL_INT = 0
             SELECT @LTOTAL_INT = SUM(MONTO_MON_CTA)
               FROM @TBLSALIDA_DET
              WHERE ORIGEN = 'VALORES'
             UPDATE @TBLSALIDA_DET
                SET TOTAL_HOJA= ISNULL(@LTOTAL_INT,0)
              WHERE ORIGEN = 'VALORES'

             SET @LTOTAL_INT = 0
             SELECT @LTOTAL_INT = SUM(MONTO_MON_CTA)
               FROM @TBLSALIDA_DET
              WHERE ORIGEN = 'PERSHING'
             UPDATE @TBLSALIDA_DET
                SET TOTAL_HOJA = ISNULL(@LTOTAL_INT,0)
              WHERE ORIGEN = 'PERSHING'

             UPDATE @TBLSALIDA_DET
                SET RENTABILIDAD = ((MONTO_MON_NEMOTECNICO/(PRECIO_PROMEDIO_COMPRA * CANTIDAD))-1)*100
              WHERE PRECIO_PROMEDIO_COMPRA > 0

             UPDATE @TBLSALIDA_DET
                SET PORCENTAJE_RAMA = (MONTO_MON_CTA / TOTAL_HOJA) * 100
              WHERE TOTAL_HOJA > 0

             INSERT INTO @salida
             SELECT SA.ID_CUENTA
                  , SA.ID_SALDO_ACTIVO
                  , SA.FECHA_CIERRE
                  , SA.ID_NEMOTECNICO
                  , N.NEMOTECNICO
                  , ISNULL(EE.DSC_EMISOR_ESPECIFICO,'') AS EMISOR
                  , EE.COD_SVS_NEMOTECNICO AS COD_EMISOR
                  , N.DSC_NEMOTECNICO
                  , ISNULL(TASA_EMISION,0)
                  , SA.CANTIDAD AS CANTIDAD
                  , NULL
                  , NULL
                  , NULL
                  , SA.PRECIO PRECIO
                  , N.TASA_EMISION
                  , N.FECHA_VENCIMIENTO
                  , ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, N.ID_NEMOTECNICO),0) AS PRECIO_COMPRA
                  , TASA
                  , CASE WHEN SA.COD_PRODUCTO = 'RF_NAC' THEN DBO.FNT_ENTREGA_TASA_PROMEDIO (SA.FECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO)
                                                         ELSE SA.TASA_COMPRA
                    END AS TASA_COMPRA
                  , SA.MONTO_VALOR_COMPRA AS MONTO_VALOR_COMPRA
                  , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA, MONTO_MON_CTA, ID_MONEDA_CTA, @PID_MONEDA_SALIDA, @PFECHA_CIERRE)   MONTO
                  , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA, MONTO_MON_CTA, ID_MONEDA_CTA, @LID_MONEDA_USD, @PFECHA_CIERRE)   MONTO_MON_USD
                  , ID_MONEDA_CTA
                  , N.ID_MONEDA
                  , MO.SIMBOLO AS SIMBOLO_MONEDA
                  , MONTO_MON_NEMOTECNICO
                  , MONTO_MON_ORIGEN
                  , 0 --ACI.ID_EMPRESA
                  , 0 --ACI.ID_ARBOL_CLASE_INST
                  , N.COD_INSTRUMENTO
                  , @LDSC_ARBOL --CI.DSC_ARBOL_CLASE_INST
                  , CASE @PCONSOLIDADO WHEN 'CTA' THEN DBO.PKG_GLOBAL$MONTO_MON_CUENTA_HOJA(@PID_ARBOL_CLASE
                       , SA.ID_CUENTA
                                                                                          , @PFECHA_CIERRE
                                                                                          , NULL,NULL,NULL)
                                                  ELSE 0
                    END AS PORCENTAJE_RAMA
                  , ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)
                  , @LDSC_PADRE
                  --, (SELECT ACII.DSC_ARBOL_CLASE_INST
                  --     FROM ARBOL_CLASE_INSTRUMENTO ACII
                  --    WHERE ACII.ID_ARBOL_CLASE_INST = CI.ID_PADRE_ARBOL_CLASE_INST) AS DSC_PADRE_ARBOL_CLASE_INST
                  , 0
                  , 0 AS DIAS
                  , 0 AS DURATION
                  , I.COD_PRODUCTO
                  , 0 --ACI.ID_PADRE_ARBOL_CLASE_INST
                  , (SELECT TOP 1 P.DURACION FROM PUBLICADORES_PRECIOS P
                      WHERE P.ID_NEMOTECNICO = N.ID_NEMOTECNICO
                    AND P.FECHA <= @PFECHA_CIERRE
                    AND P.DURACION != 0
                      ORDER BY P.FECHA DESC)AS DURACION
                  , (SELECT TOP 1 RN.COD_VALOR_CLASIFICACION
                    FROM REL_NEMOTECNICO_VALOR_CLASIFIC RN
                    , CLASIFICADORES_RIESGO C
                   WHERE RN.ID_NEMOTECNICO = ISNULL(@PID_NEMOTECNICO, N.ID_NEMOTECNICO)
                     AND RN.ID_CLASIFICADOR_RIESGO = C.ID_CLASIFICADOR_RIESGO) AS DSC_CLASIFICADOR_RIESGO
                  , @LCODIGO_ARBOL
              FROM --REL_ACI_EMP_NEMOTECNICO ACI
                  --,
                  VIEW_SALDOS_ACTIVOS SA
                  --, ARBOL_CLASE_INSTRUMENTO CI
                  , MONEDAS MO
                  , INSTRUMENTOS I
                  , NEMOTECNICOS N
                    LEFT OUTER JOIN EMISORES_ESPECIFICO EE ON EE.ID_EMISOR_ESPECIFICO = N.ID_EMISOR_ESPECIFICO
             WHERE N.ID_NEMOTECNICO            IN (SELECT ID_NEMOTECNICO FROM @TBLNEMOS) --= ACI.ID_NEMOTECNICO
                   AND SA.ID_NEMOTECNICO       = N.ID_NEMOTECNICO
                   AND SA.ID_CUENTA             IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
                   AND SA.FECHA_CIERRE         = @pfecha_cierre
                   --AND CI.ID_ARBOL_CLASE_INST  = ACI.ID_ARBOL_CLASE_INST
                   --AND ACI.ID_ARBOL_CLASE_INST IN (SELECT ID_ARBOL_CLASE_INST FROM @TBLACI)
                   AND MO.ID_MONEDA            = n.ID_MONEDA
                   AND N.COD_INSTRUMENTO       = I.COD_INSTRUMENTO
                   --AND CI.ID_EMPRESA           = @LID_EMPRESA
                   --AND ACI.ID_EMPRESA          = CI.ID_EMPRESA
                   AND N.ID_NEMOTECNICO        = ISNULL(@pid_Nemotecnico, N.ID_NEMOTECNICO)

          SELECT @LTOTAL = SUM(MONTO_MON_CTA)
            FROM @SALIDA

          UPDATE @SALIDA
             SET PORCENTAJE_RAMA = @LTOTAL

          IF (UPPER(@LDSC_PADRE) ='OTROS ACTIVOS' AND UPPER(@LDSC_ARBOL) = 'FONDOS DE INVERSI�N')   OR
             (UPPER(@LDSC_PADRE) ='OTROS ACTIVOS' AND UPPER(@LDSC_ARBOL) = 'FONDOS MUTUOS MIXTOS')
             SET @LORIGEN='NACIONAL'
          ELSE
             SET @LORIGEN = 'VALORES'

            INSERT INTO @TBLSALIDA_DET (ORIGEN
									  , MERCADO
                                      , SIMBOLO_MONEDA
                                      , COD_PRODUCTO
                                      , COD_INSTRUMENTO
                                      , ID_NEMOTECNICO
                                      , NEMOTECNICO
                                      , DSC_NEMOTECNICO
                                      , ID_MONEDA_NEMOTECNICO
                                      , EMISOR
                                      , CANTIDAD
                                      , PRECIO
                                      , PRECIO_ACTUAL
                                      , PRECIO_COMPRA
                                      , PRECIO_PROMEDIO_COMPRA
                                      , MONTO_INVERTIDO
                                      , VALOR_MERCADO
                                      , MONTO_MON_USD
                                      , MONTO_MON_NEMOTECNICO
                                      , MONTO_MON_CTA
                                      , PORCENTAJE_RAMA
                                      , MONTO_PROMEDIO_COMPRA
                                      , RENTABILIDAD
                                      , UTILIDAD_PERDIDA
    )
            SELECT @LORIGEN
				 ,@LORIGEN
                 , SIMBOLO_MONEDA
                 , COD_PRODUCTO
                 , COD_INSTRUMENTO
                 , ID_NEMOTECNICO
                 , NEMOTECNICO
                 , DSC_NEMOTECNICO
                 , ID_MONEDA_NEMOTECNICO
                 , EMISOR
                 , CANTIDAD
                 , PRECIO
                 , PRECIO
                 , PRECIO_COMPRA
                 , PRECIO_PROMEDIO_COMPRA
                 , MONTO_VALOR_COMPRA
                 , MONTO_MON_NEMOTECNICO
                 , MONTO_MON_USD
                 , MONTO_MON_NEMOTECNICO
                 , MONTO_MON_CTA
  , CASE ISNULL(PORCENTAJE_RAMA,0)
                               WHEN 0 THEN 0
                                ELSE (MONTO_MON_CTA / PORCENTAJE_RAMA) * 100
                   END AS PORCENTAJE_RAMA
                 , MONTO_PROMEDIO_COMPRA
                 , CASE WHEN COD_PRODUCTO = 'RF_NAC' THEN 0
                                               ELSE CASE (MONTO_PROMEDIO_COMPRA) WHEN 0 THEN NULL
                                                    ELSE (((PRECIO * CANTIDAD)/(PRECIO_PROMEDIO_COMPRA * CANTIDAD))-1)*100
                                                    END
                   END AS RENTABILIDAD
                 , MONTO_MON_NEMOTECNICO - (CANTIDAD * (ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, ID_CUENTA, ID_NEMOTECNICO),0)))
             FROM (SELECT distinct S.ID_CUENTA
                        , S.FECHA_CIERRE
                        , S.ID_NEMOTECNICO
                        , S.NEMOTECNICO
                        , S.EMISOR
                        , S.COD_EMISOR
                        , S.DSC_NEMOTECNICO
                        , S.TASA_EMISION_2
                        , sum(S.CANTIDAD) cantidad
                        , S.PRECIO
                        , S.TASA_EMISION
                        , S.FECHA_VENCIMIENTO
                        , S.PRECIO_COMPRA
                        , S.TASA
                        , S.TASA_COMPRA
                        , sum(S.MONTO_VALOR_COMPRA) MONTO_VALOR_COMPRA
                        , sum(ROUND(S.MONTO_MON_CTA,@LDECIMALES)) 'MONTO_MON_CTA'
                        , SUM(MONTO_MON_USD) 'MONTO_MON_USD'
                        , S.ID_MONEDA_CTA
                        , S.ID_MONEDA_NEMOTECNICO
                        , S.SIMBOLO_MONEDA
                        , sum(CASE CODIGO WHEN 'FM_OA' THEN  MONTO_VALOR_COMPRA
                                                       ELSE MONTO_MON_NEMOTECNICO
                              END  ) MONTO_MON_NEMOTECNICO
                        , sum(S.MONTO_MON_ORIGEN) MONTO_MON_ORIGEN
                        --, S.ID_EMPRESA
                        --, S.ID_ARBOL_CLASE_INST
                        , S.COD_INSTRUMENTO
                        , S.DSC_ARBOL_CLASE_INST
                        , S.PORCENTAJE_RAMA
                        , S.PRECIO_PROMEDIO_COMPRA
                        , SUM(S.CANTIDAD * S.PRECIO_PROMEDIO_COMPRA) as MONTO_PROMEDIO_COMPRA
                        , S.DSC_PADRE_ARBOL_CLASE_INST
                        , S.RENTABILIDAD
                        , S.DIAS
                        , S.COD_PRODUCTO
                        --, S.ID_PADRE_ARBOL_CLASE_INST
                        , S.DURACION
                        , M.DICIMALES_MOSTRAR
                        , S.DSC_CLASIFICADOR_RIESGO
                        , SF.ID_SUBFAMILIA AS ID_SUBFAMILIA
                        , SF.COD_SUBFAMILIA AS COD_SUBFAMILIA
                        , S.CODIGO
--                        , CASE WHEN COD_PRODUCTO = 'RF_NAC' THEN 0
--                                                ELSE CASE (SUM(CANTIDAD) * PRECIO_PROMEDIO_COMPRA) WHEN 0 THEN NULL
--                                    ELSE ((sum(MONTO_MON_NEMOTECNICO)/(PRECIO_PROMEDIO_COMPRA * sum(CANTIDAD)))-1)*100
--                                    END
--                          END AS RENTABILIDAD
                   FROM @SALIDA S
                        LEFT JOIN NEMOTECNICOS N ON (S.ID_NEMOTECNICO = N.ID_NEMOTECNICO)
                        LEFT JOIN VIEW_EMISORES_ESPECIFICOS E ON (N.ID_EMISOR_ESPECIFICO = E.ID_EMISOR_ESPECIFICO)
                        LEFT JOIN MONEDAS M ON (M.ID_MONEDA = N.ID_MONEDA)
                        LEFT JOIN SUBFAMILIAS SF ON (N.ID_SUBFAMILIA=SF.ID_SUBFAMILIA)
                  WHERE N.ID_MERCADO_TRANSACCION = ISNULL(@PID_MERCADO_TRANSACCION,N.ID_MERCADO_TRANSACCION)
                    AND N.ID_MONEDA              = ISNULL(@PID_MONEDA,N.ID_MONEDA)
                    AND E.ID_SECTOR              = ISNULL(@PID_SECTOR, E.ID_SECTOR)
                    AND N.ID_NEMOTECNICO         = ISNULL(@pid_Nemotecnico, N.ID_NEMOTECNICO)
             GROUP BY S.ID_CUENTA
                    , S.FECHA_CIERRE
                    , S.ID_NEMOTECNICO
                    , S.NEMOTECNICO
                    , S.EMISOR
                    , S.COD_EMISOR
                    , S.DSC_NEMOTECNICO
                    , S.TASA_EMISION_2
                    , S.PRECIO
                    , S.TASA_EMISION
                    , S.FECHA_VENCIMIENTO
                    , S.PRECIO_COMPRA
                    , S.TASA
                    , S.TASA_COMPRA
                    , S.ID_MONEDA_CTA
                    , S.ID_MONEDA_NEMOTECNICO
                    , S.SIMBOLO_MONEDA
                    --, S.ID_EMPRESA
                    --, S.ID_ARBOL_CLASE_INST
                    , S.COD_INSTRUMENTO
                    , S.DSC_ARBOL_CLASE_INST
                    , S.PORCENTAJE_RAMA
                    , S.PRECIO_PROMEDIO_COMPRA
                    , S.DSC_PADRE_ARBOL_CLASE_INST
                    , S.RENTABILIDAD
                    , S.DIAS
                    , S.COD_PRODUCTO
                    --, S.ID_PADRE_ARBOL_CLASE_INST
                    , S.DURACION
                    , M.DICIMALES_MOSTRAR
                    , S.DSC_CLASIFICADOR_RIESGO
                    , SF.ID_SUBFAMILIA
                    , SF.COD_SUBFAMILIA
                    , S.CODIGO) TEMP
            ORDER BY ID_MONEDA_NEMOTECNICO
                    ,NEMOTECNICO


           SELECT ORIGEN
				, MERCADO
                , SIMBOLO_MONEDA
                , COD_PRODUCTO
                , NEMOTECNICO
                , DSC_NEMOTECNICO
                , ID_MONEDA_NEMOTECNICO
                , EMISOR
                , FECHA_VENCIMIENTO
   , CANTIDAD
                , PRECIO_COMPRA
                , PRECIO_PROMEDIO_COMPRA
                , PRECIO_ACTUAL
                , PRECIO
                , MONTO_INVERTIDO
                , MONTO_MON_NEMOTECNICO
                , VALOR_MERCADO
                , UTILIDAD_PERDIDA
                , MONTO_MON_CTA
                , isnull(MONTO_MON_USD,0) MONTO_MON_USD
                , MONTO_PROMEDIO_COMPRA
                , PORCENTAJE_RAMA
                , TOTAL_HOJA
                , RENTABILIDAD
                , DURACION
           FROM @TBLSALIDA_DET
           ORDER BY ORIGEN
                  , NEMOTECNICO
    END
   ELSE
-----------------------------------------------------------------------------------
    IF (UPPER(@LDSC_PADRE) ='OTROS ACTIVOS' AND UPPER(@LDSC_ARBOL) = 'NOTAS ESTRUCTURADAS')
     BEGIN
            INSERT INTO @SALIDA_NOTAS(SIMBOLO_MONEDA
                                    , COD_PRODUCTO
                                    , NEMOTECNICO
                                    , DSC_NEMOTECNICO
                                    , CANTIDAD
                                    , PRECIO_COMPRA
                                    , MONTO_INVERTIDO
                                    , PRECIO_ACTUAL
                                    , VALOR_MERCADO
                                    , UTILIDAD_PERDIDA
                                    , MONTO_MON_CTA
                                    , MONTO_MON_USD)
                             SELECT 'USD'
                                  , N.COD_PRODUCTO
                                  , N.NEMOTECNICO
                                  , N.DSC_NEMOTECNICO
                                  , S.CANTIDAD
                                  , S.PRECIO_PROMEDIO_COMPRA as PRECIO_COMPRA
                                  , S.MONTO_INVERTIDO
                                  , S.PRECIO_MERCADO
                                  , S.VALOR_MERCADO_MON_ORIGEN
                                  , VALOR_MERCADO_MON_USD - MONTO_INVERTIDO
                                  , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(S.ID_CUENTA
                                                                         , VALOR_MERCADO_MON_USD
                                                                         , @LID_MONEDA_USD
                                                                         , @PID_MONEDA_SALIDA
                                                                         , @PFECHA_CIERRE)   MONTO
                                  , S.VALOR_MERCADO_MON_USD
                             FROM SALDOS_ACTIVOS_INT  S
                                , (SELECT N.ID_NEMOTECNICO
                                        , N.NEMOTECNICO
                                        , N.DSC_NEMOTECNICO
                                        , N.COD_INSTRUMENTO
                                        , I.COD_PRODUCTO
                                     FROM NEMOTECNICOS N
                                          INNER JOIN INSTRUMENTOS I ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO) N
                                    WHERE S.ID_CUENTA  IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
                                      AND S.FECHA_CIERRE = @PFECHA_CIERRE
                                      AND S.ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO FROM @TBLNEMOS)
                                      AND N.ID_NEMOTECNICO = S.ID_NEMOTECNICO

            INSERT INTO @SALIDA_NOTAS(SIMBOLO_MONEDA
                                    , COD_PRODUCTO
                                    , NEMOTECNICO
                                    , DSC_NEMOTECNICO
                                    , CANTIDAD
                                    , PRECIO_COMPRA
                                    , MONTO_INVERTIDO
                                    , PRECIO_ACTUAL
                                    , VALOR_MERCADO
                                    , UTILIDAD_PERDIDA
                                    , MONTO_MON_CTA
                                    , MONTO_MON_USD)
                               SELECT M.SIMBOLO
                                    , N.COD_PRODUCTO
                                    , N.NEMOTECNICO
                                    , N.DSC_NEMOTECNICO
                                    , S.CANTIDAD
                                    , CASE @PCONSOLIDADO WHEN 'CTA' THEN ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE
                                                                                                          , S.ID_CUENTA
                                    , S.ID_NEMOTECNICO),0)
                                                                    ELSE ISNULL(dbo.FNT_ENTREGA_PRECIO_PROMEDIO_CONSOLIDADO(@PFECHA_CIERRE
                                                                                                                          , @PCONSOLIDADO
                                                                                                                          , @PID_ENTIDAD
                                                                                                                          , S.ID_NEMOTECNICO),0)
                                      END
                                    , S.CANTIDAD * (CASE @PCONSOLIDADO WHEN 'CTA'
                                                              THEN (ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE
                                                                                                                  , S.ID_CUENTA
                                                                                                                  , S.ID_NEMOTECNICO),0))
                                                              ELSE (ISNULL(dbo.FNT_ENTREGA_PRECIO_PROMEDIO_CONSOLIDADO(@PFECHA_CIERRE
                                                                                                                     , @PCONSOLIDADO
                                                                                                                     , @PID_ENTIDAD
                                                                                          , S.ID_NEMOTECNICO),0) )
                                                    END)
                                    , S.PRECIO
                                    , S.CANTIDAD * S.PRECIO
                                    , S.MONTO_MON_NEMOTECNICO - (S.CANTIDAD * (CASE @PCONSOLIDADO WHEN 'CTA'
                                                              THEN (ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE
                                                                                                                  , S.ID_CUENTA
                                                                                                                  , S.ID_NEMOTECNICO),0))
                                                              ELSE (ISNULL(dbo.FNT_ENTREGA_PRECIO_PROMEDIO_CONSOLIDADO(@PFECHA_CIERRE
                                                                             , @PCONSOLIDADO
                                                                                                 , @PID_ENTIDAD
                                                                                                                     , S.ID_NEMOTECNICO),0) )
                                                                                   END)  )
                                    , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(S.ID_CUENTA
                                                                           , MONTO_MON_CTA
                                                                           , ID_MONEDA_CTA
                                                                           , @PID_MONEDA_SALIDA
                                                                           , @PFECHA_CIERRE)
                                    , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(S.ID_CUENTA
                                                                           , S.MONTO_MON_NEMOTECNICO
                                                                           , N.ID_MONEDA
                                    , @LID_MONEDA_USD
                                                                           , @PFECHA_CIERRE)
                               FROM (SELECT FECHA_CIERRE
                                          , ID_CUENTA
                                          , ID_MONEDA_CTA
                                          , ID_NEMOTECNICO
                                          , PRECIO
                                          , SUM(CANTIDAD) CANTIDAD
                                          , SUM(MONTO_MON_CTA) MONTO_MON_CTA
                                          , SUM(MONTO_MON_NEMOTECNICO) MONTO_MON_NEMOTECNICO
                                       FROM SALDOS_ACTIVOS
                                      WHERE ID_CUENTA  IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
                                        AND FECHA_CIERRE = @PFECHA_CIERRE
                                     GROUP BY FECHA_CIERRE
                                             ,ID_CUENTA
                                             ,ID_MONEDA_CTA
                                             ,ID_NEMOTECNICO
                                             ,PRECIO
                                    ) S
                                  , (SELECT N.ID_NEMOTECNICO
                                          , N.NEMOTECNICO
                                          , N.DSC_NEMOTECNICO
                                          , N.COD_INSTRUMENTO
                                          , N.ID_MONEDA
                                          , I.COD_PRODUCTO
                                       FROM NEMOTECNICOS N
                                            INNER JOIN INSTRUMENTOS I ON I.COD_INSTRUMENTO = N.COD_INSTRUMENTO) N
                                  , MONEDAS M
                             WHERE S.ID_CUENTA  IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
                               AND S.FECHA_CIERRE = @PFECHA_CIERRE
                               AND S.ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO  FROM @TBLNEMOS)
                               AND N.ID_NEMOTECNICO = S.ID_NEMOTECNICO
                               AND M.ID_MONEDA = N.ID_MONEDA

            SELECT SIMBOLO_MONEDA
                 , COD_PRODUCTO
                 , NEMOTECNICO
                 , DSC_NEMOTECNICO
                 , CANTIDAD
                 , PRECIO_COMPRA
                 , MONTO_INVERTIDO
                 , PRECIO_ACTUAL
                 , VALOR_MERCADO
                 , UTILIDAD_PERDIDA
                 , MONTO_MON_CTA
                 , MONTO_MON_USD
             FROM @SALIDA_NOTAS
             ORDER BY NEMOTECNICO

    END
  -----------------------------------------------------------------------------------
   ELSE
    BEGIN
            INSERT INTO @salida
            SELECT DISTINCT SA.ID_CUENTA
                 , SA.ID_SALDO_ACTIVO
                 , SA.FECHA_CIERRE
                 , SA.ID_NEMOTECNICO
                 , N.NEMOTECNICO
                 , ISNULL(EE.DSC_EMISOR_ESPECIFICO,'') AS EMISOR
                 , EE.COD_SVS_NEMOTECNICO AS COD_EMISOR
                 , N.DSC_NEMOTECNICO
                 , ISNULL(TASA_EMISION,0)
                 , (SA.CANTIDAD) AS CANTIDAD
                 , ISNULL(GPS.GARANTIAS, 0) GARANTIAS
				 , ISNULL(GPS.PRESTAMOS, 0) PRESTAMOS
				 , ISNULL(GPS.SIMULTANEAS, 0) SIMULTANEAS
                 , SA.PRECIO PRECIO
                 , N.TASA_EMISION
                 , N.FECHA_VENCIMIENTO
                 , CASE @PCONSOLIDADO WHEN 'CTA' THEN ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE
                                                                                       , SA.ID_CUENTA
                                                                                       , N.ID_NEMOTECNICO),0)
                                                 ELSE ISNULL(dbo.FNT_ENTREGA_PRECIO_PROMEDIO_CONSOLIDADO(@PFECHA_CIERRE
                                                                                                       , @PCONSOLIDADO
                                                                                                       , @PID_ENTIDAD
                                                                                                       , SA.ID_NEMOTECNICO),0)
                   END AS PRECIO_COMPRA
                 , SA.TASA
                 , CASE @PCONSOLIDADO WHEN 'CTA' THEN (CASE WHEN SA.COD_PRODUCTO = 'RF_NAC'
                                      THEN DBO.FNT_ENTREGA_TASA_PROMEDIO (SA.FECHA_CIERRE
                                                                                                   , SA.ID_CUENTA
                                                                                                   , SA.ID_NEMOTECNICO)
                                                                 ELSE SA.TASA_COMPRA
                                                       END)
                                                 ELSE (CASE WHEN SA.COD_INSTRUMENTO = 'BONOS_NAC'
                                                                 THEN DBO.FNT_ENTREGA_TASA_PROMEDIO_CONSOLIDADO(SA.FECHA_CIERRE
                                                                                                              , @PCONSOLIDADO
                                                                                                              , @PID_ENTIDAD
                                                                   , SA.ID_NEMOTECNICO)
                                                                 ELSE SA.TASA_COMPRA
                                                       END)
                   END AS TASA_COMPRA
                 , SA.MONTO_VALOR_COMPRA AS MONTO_VALOR_COMPRA
                 , DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SA.ID_CUENTA
                                                        , MONTO_MON_CTA
                                                        , ID_MONEDA_CTA
                                                        , @PID_MONEDA_SALIDA
                                                        , @PFECHA_CIERRE)   MONTO
                 , 0 'MONTO_MON_USD'
                 , ID_MONEDA_CTA
                 , N.ID_MONEDA
                 , MO.SIMBOLO AS SIMBOLO_MONEDA
                 , MONTO_MON_NEMOTECNICO
                 , MONTO_MON_ORIGEN
                 , 0 --ACI.ID_EMPRESA
                 , 0 --ACI.ID_ARBOL_CLASE_INST
                 , N.COD_INSTRUMENTO
                 , @LDSC_ARBOL --CI.DSC_ARBOL_CLASE_INST
                 --, 0 AS PORCENTAJE_RAMA
                 , CASE @PCONSOLIDADO WHEN 'CTA' THEN DBO.PKG_GLOBAL$MONTO_MON_CUENTA_HOJA(@PID_ARBOL_CLASE
                                                                                          , SA.ID_CUENTA
                                                                                          , @PFECHA_CIERRE,NULL,NULL,NULL)
                                                 ELSE dbo.Pkg_Global$MONTO_MON_CUENTA_HOJA_Consolidado(@PID_ARBOL_CLASE
                                                                                                      ,@PID_ENTIDAD
                                                                                                      ,SA.FECHA_CIERRE
                                                                                                      ,@PID_ENTIDAD
                                                                                                      ,@PID_ENTIDAD
                                                                                                      ,@PID_MONEDA_SALIDA
                                                                                                      ,@PCONSOLIDADO
                              ,NULL)
                    END AS PORCENTAJE_RAMA
                 --, ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE, SA.ID_CUENTA, SA.ID_NEMOTECNICO),0)
                   , CASE @PCONSOLIDADO WHEN 'CTA' THEN ISNULL(DBO.ENTREGA_PRECIO_PROMEDIO(@PFECHA_CIERRE
                                                                                       , Sa.ID_CUENTA
                                                                                       , Sa.ID_NEMOTECNICO),0)
                                                                    ELSE ISNULL(dbo.FNT_ENTREGA_PRECIO_PROMEDIO_CONSOLIDADO(@PFECHA_CIERRE
                                                                                                                          , @PCONSOLIDADO
                                                                                                                          , @PID_ENTIDAD
                                                                                                                          , Sa.ID_NEMOTECNICO),0)
                                      END
                 --, (SELECT ACII.DSC_ARBOL_CLASE_INST
                 --     FROM ARBOL_CLASE_INSTRUMENTO ACII
                 --    WHERE ACII.ID_ARBOL_CLASE_INST = CI.ID_PADRE_ARBOL_CLASE_INST) AS DSC_PADRE_ARBOL_CLASE_INST
                 , @LDSC_PADRE
                 , 0
                 , 0 AS DIAS
                 , 0 AS DURATION
                 , I.COD_PRODUCTO
                 , 0 --CI.ID_PADRE_ARBOL_CLASE_INST
                 , (SELECT TOP 1 P.DURACION
                      FROM PUBLICADORES_PRECIOS P
                     WHERE P.ID_NEMOTECNICO = N.ID_NEMOTECNICO
                       AND P.FECHA <= @PFECHA_CIERRE
                       AND P.DURACION != 0
                    ORDER BY P.FECHA DESC)AS DURACION
                 , (SELECT TOP 1 RN.COD_VALOR_CLASIFICACION
                      FROM REL_NEMOTECNICO_VALOR_CLASIFIC RN
                         , CLASIFICADORES_RIESGO C
                     WHERE RN.ID_NEMOTECNICO = ISNULL(@PID_NEMOTECNICO, N.ID_NEMOTECNICO)
                       AND RN.ID_CLASIFICADOR_RIESGO = C.ID_CLASIFICADOR_RIESGO) AS DSC_CLASIFICADOR_RIESGO
                 , @LCODIGO_ARBOL --CI.CODIGO
              FROM --REL_ACI_EMP_NEMOTECNICO ACI
                 --,
                   VIEW_SALDOS_ACTIVOS SA
                     LEFT OUTER JOIN GAR_PRE_SIM GPS ON GPS.ID_CUENTA  = SA.ID_CUENTA   AND GPS.ID_NEMOTECNICO =    SA.ID_NEMOTECNICO AND GPS.FECHA_CIERRE = SA.FECHA_CIERRE
                 --, ARBOL_CLASE_INSTRUMENTO CI
                 , MONEDAS MO
                 , INSTRUMENTOS I
                -- , GAR_PRE_SIM GPS
                 , NEMOTECNICOS N
                   LEFT OUTER JOIN EMISORES_ESPECIFICO EE ON EE.ID_EMISOR_ESPECIFICO = N.ID_EMISOR_ESPECIFICO
             WHERE N.ID_NEMOTECNICO       IN (SELECT ID_NEMOTECNICO FROM @TBLNEMOS) --= ACI.ID_NEMOTECNICO
               AND SA.ID_NEMOTECNICO       = N.ID_NEMOTECNICO
               --AND GPS.ID_NEMOTECNICO =    SA.ID_NEMOTECNICO
               AND SA.ID_CUENTA             IN (SELECT ID_CUENTA FROM @TBLCUENTAS)
               --AND GPS.ID_CUENTA  = SA.ID_CUENTA
               AND SA.FECHA_CIERRE     = @pfecha_cierre
               AND MO.ID_MONEDA            = n.ID_MONEDA
               AND N.COD_INSTRUMENTO       = I.COD_INSTRUMENTO
               AND N.ID_NEMOTECNICO        = ISNULL(@pid_Nemotecnico, N.ID_NEMOTECNICO)
               --AND GPS.FECHA_CIERRE = @pfecha_cierre

            SELECT DISTINCT 'NACIONAL' 'ORIGEN'
                 , FECHA_CIERRE
                 , ID_NEMOTECNICO
                 , NEMOTECNICO
                 , EMISOR
                 , COD_EMISOR
                 , DSC_NEMOTECNICO
                 , TASA_EMISION_2
                 , CANTIDAD
                 , GARANTIAS
                 , PRESTAMOS
                 , SIMULTANEAS
                 , PRECIO
                 , TASA_EMISION
                 , FECHA_VENCIMIENTO
                 , PRECIO_COMPRA
                 , TASA
                 , TASA_COMPRA
                 , MONTO_VALOR_COMPRA
                 , MONTO_MON_CTA
                 , ID_MONEDA_CTA
                 , ID_MONEDA_NEMOTECNICO
                 , SIMBOLO_MONEDA
                 , MONTO_MON_NEMOTECNICO
                 , MONTO_MON_ORIGEN
                 , ID_EMPRESA
                 , ID_ARBOL_CLASE_INST
                 , COD_INSTRUMENTO
                 , DSC_ARBOL_CLASE_INST
                 , CASE ISNULL(PORCENTAJE_RAMA,0)
                            WHEN 0 THEN 0
                                   ELSE (MONTO_MON_CTA / PORCENTAJE_RAMA) * 100
                   END AS PORCENTAJE_RAMA
                 , PRECIO_PROMEDIO_COMPRA
                 , MONTO_PROMEDIO_COMPRA
                 , DSC_PADRE_ARBOL_CLASE_INST
                 , CASE (PRECIO_PROMEDIO_COMPRA * CANTIDAD) WHEN 0 THEN 0
                                       ELSE ((MONTO_MON_ORIGEN/(PRECIO_PROMEDIO_COMPRA * CANTIDAD))-1)*100
                              END  AS RENTABILIDAD
                 , DIAS
                 , COD_PRODUCTO
                 , ID_PADRE_ARBOL_CLASE_INST
                 , DURACION
                 , DICIMALES_MOSTRAR
                 , DSC_CLASIFICADOR_RIESGO
                 , ID_SUBFAMILIA AS ID_SUBFAMILIA
                 , COD_SUBFAMILIA AS COD_SUBFAMILIA
                 , CODIGO
           FROM (SELECT S.FECHA_CIERRE
                      , S.ID_NEMOTECNICO
                      , S.NEMOTECNICO
                      , S.EMISOR
                      , S.COD_EMISOR
                      , S.DSC_NEMOTECNICO
                      , S.TASA_EMISION_2
                      , sum(S.CANTIDAD ) cantidad
                      ,S.GARANTIAS
					 ,S.PRESTAMOS
					 ,S.SIMULTANEAS
                      , S.PRECIO
                      , S.TASA_EMISION
                      , S.FECHA_VENCIMIENTO
                      , S.PRECIO_COMPRA
                      , S.TASA
                      , S.TASA_COMPRA
                      , sum(S.MONTO_VALOR_COMPRA) MONTO_VALOR_COMPRA
                      , sum(ROUND(S.MONTO_MON_CTA,@LDECIMALES)) 'MONTO_MON_CTA'
                      , S.ID_MONEDA_CTA
                      , S.ID_MONEDA_NEMOTECNICO
                      , S.SIMBOLO_MONEDA
                      , sum(CASE CODIGO when 'FFMM' THEN  MONTO_VALOR_COMPRA
                                           ELSE MONTO_MON_NEMOTECNICO
                             END  ) MONTO_MON_NEMOTECNICO
                      , sum(S.MONTO_MON_ORIGEN) MONTO_MON_ORIGEN
                      , S.ID_EMPRESA
                      , S.ID_ARBOL_CLASE_INST
                      , S.COD_INSTRUMENTO
                      , S.DSC_ARBOL_CLASE_INST
                      , S.PORCENTAJE_RAMA
                      , S.PRECIO_PROMEDIO_COMPRA
                      , SUM(S.CANTIDAD * S.PRECIO_PROMEDIO_COMPRA) as MONTO_PROMEDIO_COMPRA
                      , S.DSC_PADRE_ARBOL_CLASE_INST
                      , S.RENTABILIDAD
                      , S.DIAS
                      , S.COD_PRODUCTO
                      , S.ID_PADRE_ARBOL_CLASE_INST
                      , S.DURACION
                      , M.DICIMALES_MOSTRAR
                      , S.DSC_CLASIFICADOR_RIESGO
                      , SF.ID_SUBFAMILIA AS ID_SUBFAMILIA
                      , SF.COD_SUBFAMILIA AS COD_SUBFAMILIA
                      , S.CODIGO
                 FROM @SALIDA S
                      LEFT JOIN NEMOTECNICOS N ON (S.ID_NEMOTECNICO = N.ID_NEMOTECNICO)
                      LEFT JOIN VIEW_EMISORES_ESPECIFICOS E ON (N.ID_EMISOR_ESPECIFICO = E.ID_EMISOR_ESPECIFICO)
                      LEFT JOIN MONEDAS M ON (M.ID_MONEDA = N.ID_MONEDA)
                      LEFT JOIN SUBFAMILIAS SF ON (N.ID_SUBFAMILIA=SF.ID_SUBFAMILIA)
                      --INNER JOIN GAR_PRE_SIM GPS ON GPS.ID_NEMOTECNICO  IN (SELECT ID_NEMOTECNICO FROM @TBLNEMOS) AND GPS.ID_CUENTA  IN (SELECT ID_CUENTA FROM @TBLCUENTAS) AND GPS.FECHA_CIERRE = @pfecha_cierre
					  --LEFT OUTER JOIN SIMULTANEAS SI ON SI.ID_NEMOTECNICO IN (SELECT ID_NEMOTECNICO FROM @TBLNEMOS) AND SI.ID_CUENTA  IN (SELECT ID_CUENTA FROM @TBLCUENTAS) AND SI.FCH_SIMULTANEA = @pfecha_cierre
                WHERE N.ID_MERCADO_TRANSACCION = ISNULL(@PID_MERCADO_TRANSACCION,N.ID_MERCADO_TRANSACCION)
                  AND N.ID_MONEDA              = ISNULL(@PID_MONEDA,N.ID_MONEDA)
                  AND E.ID_SECTOR              = ISNULL(@PID_SECTOR, E.ID_SECTOR)
                  AND N.ID_NEMOTECNICO         = ISNULL(@pid_Nemotecnico, N.ID_NEMOTECNICO)
             GROUP BY S.FECHA_CIERRE
                    , S.ID_NEMOTECNICO
                    , S.NEMOTECNICO
                    , S.EMISOR
                    , S.COD_EMISOR
                    , S.DSC_NEMOTECNICO
                  , S.TASA_EMISION_2
                    , S.PRECIO
                    , S.TASA_EMISION
                    , S.FECHA_VENCIMIENTO
                    , S.PRECIO_COMPRA
                    , S.TASA
                    , S.TASA_COMPRA
                    , S.ID_MONEDA_CTA
                    , S.ID_MONEDA_NEMOTECNICO
                    , S.SIMBOLO_MONEDA
                    , S.ID_EMPRESA
                    , S.ID_ARBOL_CLASE_INST
                    , S.COD_INSTRUMENTO
                    , S.DSC_ARBOL_CLASE_INST
                    , S.PORCENTAJE_RAMA
                    , S.PRECIO_PROMEDIO_COMPRA
                    , S.DSC_PADRE_ARBOL_CLASE_INST
                    , S.RENTABILIDAD
                , S.DIAS
                    , S.COD_PRODUCTO
                    , S.ID_PADRE_ARBOL_CLASE_INST
                    , S.DURACION
                    , M.DICIMALES_MOSTRAR
                    , S.DSC_CLASIFICADOR_RIESGO
                    , SF.ID_SUBFAMILIA
                    , SF.COD_SUBFAMILIA
                    , S.CODIGO
                     , S.GARANTIAS
                     ,S.PRESTAMOS
                     ,S.SIMULTANEAS) TEMP
            ORDER BY ID_MONEDA_NEMOTECNICO,NEMOTECNICO
   END
END
GO
GRANT EXECUTE ON [PKG_CARTOLA_CONS_APV$DetalleActivos_Vr2] TO DB_EXECUTESP
GO