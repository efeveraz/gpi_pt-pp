IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_SALDOS_ACTIVOS_INT$TestImportador_INV]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
DROP PROCEDURE [DBO].[PKG_SALDOS_ACTIVOS_INT$TestImportador_INV]
GO
CREATE PROCEDURE [dbo].[PKG_SALDOS_ACTIVOS_INT$TestImportador_INV]
   (
   @PFECHA_CIERRE                 DATETIME
 , @PCUENTA_EXTERNA               VARCHAR(50)
 , @PCODIGO_ISIN                  VARCHAR(30)
 , @PCOD_EMISOR                   VARCHAR(50)
 , @PDSC_EMISOR                   VARCHAR(50)
 , @PTIPO_PRODUCTO                VARCHAR(30)
 , @PTIPO_ACTIVO                  VARCHAR(20)
 , @PDESCRIPCION_VALOR            VARCHAR(50)
 , @PCODIGO_DIVISA_COTIZACION     VARCHAR(20)
 , @PDSC_DIVISA_COTIZACION        VARCHAR(20)
 , @PTOTAL_TITULOS                FLOAT
 , @PTOTAL_NOMINAL                FLOAT
 , @PCOSTE_MEDIO_POSICION         FLOAT
 , @PPRECIO_MERCADO_COTIZACION    FLOAT
 , @PPRECIO                       FLOAT
 , @PFECHA_VENCIMIENTO            DATETIME
 , @PVALOR                        FLOAT
 , @PRATIO_CONVERSION_CONTRA_EURO FLOAT
 , @PCODIGO_MERCADO        VARCHAR(10)
 ) AS
BEGIN
     SET NOCOUNT ON

     DECLARE @LID_CUENTA            NUMERIC
           , @LCODRESULTADO         VARCHAR(10)
           , @LMSGRESULTADO         VARCHAR(800)
           , @LCOD_ORIGEN           VARCHAR(20)
           , @LID_NEMOTECNICO       NUMERIC
           , @LNEMOTECNICO          VARCHAR(50)
           , @LCOD_INSTRUMENTO      VARCHAR(20)
           , @LID_MONEDA_USD        NUMERIC
           , @LID_MONEDA_EUR        NUMERIC
           , @LID_MONEDA_CLP        NUMERIC
           , @LID_EMISOR_ESPECIFICO NUMERIC
           , @LID_EMISOR_GENERAL    NUMERIC
           , @LFECHAVARCHAR         VARCHAR(10)
           , @TIPO_ACTIVO   VARCHAR(5)
           , @LCOD_INST_SVS         VARCHAR(10)
           , @LID_INSTRUMENTO_SVS   NUMERIC
     , @REL_NEMOTECNICO       NUMERIC


     SET @LMSGRESULTADO = ''
     SET @LCODRESULTADO = 'OK'
---------------------------------------------------------------------------------
     SELECT @LID_MONEDA_USD = ID_MONEDA FROM MONEDAS WHERE COD_MONEDA= 'USD'
     SELECT @LID_MONEDA_EUR = ID_MONEDA FROM MONEDAS WHERE COD_MONEDA= 'EUR'
     SELECT @LID_MONEDA_CLP = ID_MONEDA FROM MONEDAS WHERE COD_MONEDA= '$$'
     SET @LFECHAVARCHAR = CONVERT(VARCHAR(8),@PFECHA_CIERRE, 112)

    CREATE TABLE #Tabla1  (FECHA             VARCHAR(10),
                           NOMBRE_MONEDA     VARCHAR(70),
                           CODIGO_MONEDA     VARCHAR(10),
                           CODIGO_MONEDA_SVS VARCHAR(10),
                           PARIDAD           NUMERIC(18,4),
                           TIPO_CAMBIO_USD   NUMERIC(18,4))

     INSERT #Tabla1
     exec [FFMM].[MG_SAFMUTNW].[dbo].[sp_fm_cargaMGSafmutNW$ParidadesMoneda] @LFECHAVARCHAR

     DECLARE @vValorEuro NUMERIC(18,4)

      SELECT @vValorEuro = TIPO_CAMBIO_USD
        FROM #Tabla1
       WHERE CODIGO_MONEDA_SVS = 'EUR'

  --------------------------------------------------------------------------------
     SELECT @LCOD_INSTRUMENTO = CASE @PTIPO_PRODUCTO WHEN 'RV' THEN 'RV_INT_INV'
                                                     WHEN 'RF' THEN 'RF_INT_INV'
                                                     WHEN 'IIC' THEN 'FFMM_INT_INV'
                                END

     SELECT @LID_EMISOR_GENERAL = ID_EMISOR_GENERAL
       FROM EMISORES_GENERALES
      WHERE DSC_EMISOR_GENERAL = CASE @LCOD_INSTRUMENTO WHEN 'RF_INT_INV' THEN 'EMISOR GENERAL RF INT'
                                                        WHEN 'RV_INT_INV' THEN 'EMISOR GENERAL RV INT'
                                                        WHEN 'FFMM_INT_INV' THEN 'EMISOR GENERAL FFMM INT'
                                 END
     ---------------------------------------------------------------------------------
     SELECT @LCOD_ORIGEN = COD_ORIGEN
       FROM ORIGENES
      WHERE DSC_ORIGEN = 'MAGIC VALORES'


     IF ISNULL((SELECT COUNT(1)FROM VIEW_ALIAS WHERE TABLA='CUENTAS' AND COD_ORIGEN=@LCOD_ORIGEN AND VALOR = @PCUENTA_EXTERNA),0) <> 0
      BEGIN
  SELECT @LID_CUENTA = ID_ENTIDAD
  FROM   VIEW_ALIAS
  WHERE TABLA='CUENTAS'
    AND COD_ORIGEN=@LCOD_ORIGEN
    AND VALOR = @PCUENTA_EXTERNA
      END
     ELSE
      BEGIN
          SET @LCODRESULTADO = 'ERROR'
          SET @LMSGRESULTADO = 'ERROR: Cuenta Externa no tiene Alias en GPI'
      END
---------------------------------------------------------------------------------
     IF @LCODRESULTADO = 'OK'
       BEGIN
          SELECT @TIPO_ACTIVO = TIPO_ACTIVO
          FROM   CISCB.DBO.TB_INVERSIS_INSTRUMENTOS
          WHERE  TIPO_PRODUCTO='RV'
             AND CODIGO_ISIN =  @PCODIGO_ISIN

   IF @@ROWCOUNT > 0
             BEGIN
               IF @TIPO_ACTIVO = 'ACE' OR @TIPO_ACTIVO = 'ACI'
                 SET @LCOD_INST_SVS = 'ACE'
               IF @TIPO_ACTIVO = 'CPC' OR @TIPO_ACTIVO = 'DEE' OR @TIPO_ACTIVO = 'DEI'
                 SET @LCOD_INST_SVS = 'OTROC'
             END

           SELECT @LID_INSTRUMENTO_SVS = ID_ENTIDAD
           FROM VIEW_ALIAS
           WHERE COD_ORIGEN = 'INVERSIS'
             AND TABLA      = 'INSTRUMENTOS_SVS'
             AND VALOR      = @LCOD_INST_SVS

           IF ISNULL((SELECT COUNT(1)FROM REL_NEMOTECNICO_ATRIBUTOS WHERE ISIM = @PCODIGO_ISIN),0) <> 0
             BEGIN
                 IF ((SELECT COUNT(1) FROM REL_NEMOTECNICO_ATRIBUTOS WHERE ISIM = @PCODIGO_ISIN) > 1)
                     BEGIN
                         SET @LCODRESULTADO = 'ERROR_OTRO'
                         SET @LMSGRESULTADO = 'ERROR: ISIN (' + @PCODIGO_ISIN + ') se encuentra asociado a más de un nemotécnico, por favor regularizar.'
                     END
                 ELSE
                     BEGIN
                         SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO  , @LNEMOTECNICO    = NEMOTECNICO
                         FROM NEMOTECNICOS
                         WHERE ID_NEMOTECNICO = (SELECT ID_NEMOTECNICO
                                                 FROM REL_NEMOTECNICO_ATRIBUTOS
                                                 WHERE ISIM = @PCODIGO_ISIN)

                         UPDATE REL_NEMOTECNICO_ATRIBUTOS  SET ID_INSTRUMENTO_SVS = @LID_INSTRUMENTO_SVS
                         WHERE ID_NEMOTECNICO = @LID_NEMOTECNICO AND ISIM = @PCODIGO_ISIN

         IF @PTIPO_PRODUCTO = 'RF'
         BEGIN
          SELECT @REL_NEMOTECNICO = count(1)
          FROM rel_aci_emp_nemotecnico
          WHERE id_nemotecnico = @LID_NEMOTECNICO and ID_ACI_TIPO=1
        IF @@ROWCOUNT = 0 EXEC PKG_SALDOS_ACTIVOS_INT$RelacionNemotecnico @LID_NEMOTECNICO,1,0 -- SI EL ID_ACI_TIPO ES IGUAL 1
          ELSE EXEC PKG_SALDOS_ACTIVOS_INT$RelacionNemotecnico @LID_NEMOTECNICO,1,1

          SELECT @REL_NEMOTECNICO = count(1)
          FROM rel_aci_emp_nemotecnico
          WHERE id_nemotecnico = @LID_NEMOTECNICO and ID_ACI_TIPO=2
        IF @@ROWCOUNT = 0 EXEC PKG_SALDOS_ACTIVOS_INT$RelacionNemotecnico @LID_NEMOTECNICO,2,0  -- SI EL ID_ACI_TIPO ES IGUAL 2
          ELSE EXEC PKG_SALDOS_ACTIVOS_INT$RelacionNemotecnico @LID_NEMOTECNICO,2,1
         END -- If Producto = Renta fija
                     END
             END
           ELSE
             BEGIN

                 IF ISNULL((SELECT COUNT(1) FROM NEMOTECNICOS WHERE NEMOTECNICO = @PDESCRIPCION_VALOR),0) <> 0
                     BEGIN
       SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO,@LNEMOTECNICO = NEMOTECNICO
       FROM NEMOTECNICOS
       WHERE NEMOTECNICO = @PDESCRIPCION_VALOR

       INSERT INTO REL_NEMOTECNICO_ATRIBUTOS (ID_NEMOTECNICO, ISIM, ID_INSTRUMENTO_SVS)
       VALUES (@LID_NEMOTECNICO,@PCODIGO_ISIN, @LID_INSTRUMENTO_SVS)
                     END
                   ELSE
                     BEGIN

                      IF NOT EXISTS(SELECT 1 FROM EMISORES_ESPECIFICO WHERE DSC_EMISOR_ESPECIFICO = @PDSC_EMISOR)
                          BEGIN
        INSERT INTO EMISORES_ESPECIFICO (ID_EMISOR_GENERAL,DSC_EMISOR_ESPECIFICO,COD_EMISOR_ESPECIFICO)
        VALUES(@LID_EMISOR_GENERAL,@PDSC_EMISOR,@PCOD_EMISOR)
                          END

    SELECT @LID_EMISOR_ESPECIFICO = ID_EMISOR_ESPECIFICO
    FROM EMISORES_ESPECIFICO
    WHERE DSC_EMISOR_ESPECIFICO = @PDSC_EMISOR

    EXEC PKG_NEMOTECNICOS$Guardar  @PID_NEMOTECNICO               = @LID_NEMOTECNICO OUTPUT
      , @PCOD_INSTRUMENTO              = @LCOD_INSTRUMENTO
      , @PNEMOTECNICO                  = @PDESCRIPCION_VALOR
      , @PID_MERCADO_TRANSACCION       = 22
      , @PID_EMISOR_ESPECIFICO         = @LID_EMISOR_ESPECIFICO
      , @PID_MONEDA                    = @LID_MONEDA_USD
      , @PID_TIPO_ESTADO               = 3
      , @PCOD_ESTADO                   = 'V'
      , @PDSC_NEMOTECNICO              = @PDESCRIPCION_VALOR
      , @PTASA_EMISION                 = NULL
      , @PTIPO_TASA                    = NULL
      , @PPERIODICIDAD                 = NULL
      , @PFECHA_VENCIMIENTO            = @PFECHA_VENCIMIENTO
      , @PCORTE_MINIMO_PAPEL           = 0
      , @PMONTO_EMISION                = 0
      , @PLIQUIDEZ                     = NULL
      , @PBASE                         = NULL
      , @PCOD_PAIS                     = 'OTR'
      , @PID_EMISOR_ESPECIFICO_ORIGEN  = @LID_EMISOR_ESPECIFICO
      , @PID_MONEDA_TRANSACCION        = @LID_MONEDA_USD
      , @PID_SUBFAMILIA                = NULL
      , @PFLG_FUNGIBLE                 = NULL
      , @PFECHA_EMISION                = @PFECHA_CIERRE
      , @PFLG_TIPO_CUOTA_INGRESO       = NULL
      , @PFLG_TIPO_CUOTA_EGRESO        = NULL
      , @PID_USUARIO_INSERT            = 1
      , @PID_USUARIO_UPDATE            = 1
     IF @LID_NEMOTECNICO IS NULL
      BEGIN
      SET @LMSGRESULTADO = 'ERROR: Nemotécnico no pudo ser creado (' + @PDESCRIPCION_VALOR + ').'
      SET @LCODRESULTADO = 'ERROR_OTRO'
      END
          ELSE
     BEGIN
      INSERT INTO REL_NEMOTECNICO_ATRIBUTOS (ID_NEMOTECNICO, ISIM, ID_INSTRUMENTO_SVS)
      VALUES (@LID_NEMOTECNICO,@PCODIGO_ISIN, @LID_INSTRUMENTO_SVS)

        IF @PTIPO_PRODUCTO = 'RF'
        BEGIN
         SELECT (1)
         FROM rel_aci_emp_nemotecnico
         WHERE id_nemotecnico = @LID_NEMOTECNICO and ID_ACI_TIPO=1
       IF @@ROWCOUNT = 0 EXEC PKG_SALDOS_ACTIVOS_INT$RelacionNemotecnico @LID_NEMOTECNICO,1,0 -- SI EL ID_ACI_TIPO ES IGUAL 1
         ELSE EXEC PKG_SALDOS_ACTIVOS_INT$RelacionNemotecnico @LID_NEMOTECNICO,1,1

         SELECT (1)
         FROM rel_aci_emp_nemotecnico
         WHERE id_nemotecnico = @LID_NEMOTECNICO and ID_ACI_TIPO=2
       IF @@ROWCOUNT = 0 EXEC PKG_SALDOS_ACTIVOS_INT$RelacionNemotecnico @LID_NEMOTECNICO,2,0  -- SI EL ID_ACI_TIPO ES IGUAL 2
         ELSE EXEC PKG_SALDOS_ACTIVOS_INT$RelacionNemotecnico @LID_NEMOTECNICO,2,1
       END -- If Producto = Renta fija
                    END
             END
       END
---------------------------------------------------------------------------------
      END
---------------------------------------------------------------------------------
      IF @LCODRESULTADO = 'OK'
        BEGIN
            SELECT EM.DSC_EMPRESA
                 , 'INV'                                 'ORIGEN'
                 , @LID_CUENTA                           'ID_CUENTA'
                 , CT.NUM_CUENTA                         'NUM_CUENTA'
                 , CL.RUT_CLIENTE                        'RUT_CLIENTE'
                 , CASE
                     WHEN (CL.TIPO_ENTIDAD = 'J') THEN CL.RAZON_SOCIAL
                     ELSE (ISNULL(CL.NOMBRES, '') + ' ' + ISNULL(CL.PATERNO, '') + ' ' + ISNULL(CL.MATERNO, ''))
                   END                                   'NOMBRE_CLIENTE'
                 , @PCODIGO_ISIN                         'CODIGO_ISIN'
                 , @LID_NEMOTECNICO                      'ID_NEMOTECNICO'
                 , NE.NEMOTECNICO                        'NEMOTECNICO'
                 , NE.DSC_INSTRUMENTO                    'DSC_INSTRUMENTO'
                 , NE.DSC_PRODUCTO                       'DSC_PRODUCTO'
                 , NE.DSC_EMISOR_ESPECIFICO              'DSC_EMISOR_ESPECIFICO'
                 , @PCODIGO_DIVISA_COTIZACION            'MONEDA'
                 , ISNULL(@PDSC_DIVISA_COTIZACION, '')   'DSC_MONEDA'
                 , CASE @PTIPO_PRODUCTO
                     WHEN 'RV' then @PTOTAL_TITULOS
                     WHEN 'IIC' THEN @PTOTAL_TITULOS
                     ELSE @PTOTAL_NOMINAL
                   END                                   'CANTIDAD'
                 , @PCOSTE_MEDIO_POSICION                'PRECIO_PROMEDIO_COMPRA'
                 , @PPRECIO_MERCADO_COTIZACION           'PRECIO_MERCADO'
                 , @PPRECIO                              'PRECIO_COMPRA'
                 , @PFECHA_VENCIMIENTO                   'FECHA_VENCIMIENTO'
                 , ROUND(
                   CASE @PTIPO_PRODUCTO
                     WHEN 'RV'  then @PCOSTE_MEDIO_POSICION * @PTOTAL_TITULOS
                     WHEN 'IIC' then @PCOSTE_MEDIO_POSICION * @PTOTAL_TITULOS
                     ELSE @PTOTAL_NOMINAL * (@PCOSTE_MEDIO_POSICION/100)
                   END, 4)                               'MONTO_INVERTIDO'
                 , @PVALOR                               'VALOR_MERCADO_MON_ORIGEN'
                 , CASE @PTIPO_PRODUCTO
                      WHEN 'RV' THEN
                         CASE
                            WHEN (@PCODIGO_DIVISA_COTIZACION = 'USD') THEN (@PPRECIO_MERCADO_COTIZACION * @PTOTAL_TITULOS)
                            ELSE (((@PPRECIO_MERCADO_COTIZACION * @PTOTAL_TITULOS) * @PRATIO_CONVERSION_CONTRA_EURO) / @vValorEuro)
                         END
                      WHEN 'IIC' THEN
                         CASE
                            WHEN (@PCODIGO_DIVISA_COTIZACION = 'USD') THEN (@PPRECIO_MERCADO_COTIZACION * @PTOTAL_TITULOS)
                            ELSE (((@PPRECIO_MERCADO_COTIZACION * @PTOTAL_TITULOS) * @PRATIO_CONVERSION_CONTRA_EURO) / @vValorEuro)
                         END
                      ELSE
                         CASE
                            WHEN (@PCODIGO_DIVISA_COTIZACION = 'USD') THEN ((@PPRECIO_MERCADO_COTIZACION * @PTOTAL_NOMINAL)/100)
                            ELSE ((((@PPRECIO_MERCADO_COTIZACION * @PTOTAL_NOMINAL)/100) * @PRATIO_CONVERSION_CONTRA_EURO) / @vValorEuro)
                         END
                   END 'VALOR_MERCADO_MON_USD'
                 , @LCODRESULTADO                        'CODRESULTADO'
                 , @LMSGRESULTADO                        'MSGRESULTADO'
     , @PCODIGO_MERCADO                       'CODMERCADO'
              FROM CUENTAS CT
                 , CLIENTES CL
                 , EMPRESAS EM
                 , (SELECT N.NEMOTECNICO
                         , I.DSC_INTRUMENTO DSC_INSTRUMENTO
                         , P.DSC_PRODUCTO
                         , E.DSC_EMISOR_ESPECIFICO
          FROM NEMOTECNICOS N
                         , INSTRUMENTOS I
                         , PRODUCTOS P
                         , EMISORES_ESPECIFICO E
                     WHERE I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
                       AND P.COD_PRODUCTO = I.COD_PRODUCTO
                       AND N.ID_NEMOTECNICO = @LID_NEMOTECNICO
                       AND E.ID_EMISOR_ESPECIFICO = N.ID_EMISOR_ESPECIFICO) NE
             WHERE CT.ID_CUENTA  = @LID_CUENTA
               AND CL.ID_CLIENTE = CT.ID_CLIENTE
               AND EM.ID_EMPRESA = CT.ID_EMPRESA
       END
      ELSE
       BEGIN
            SELECT ''                                    'DSC_EMPRESA'
                 , 'INV'                                 'ORIGEN'
                 , 0                                     'ID_CUENTA'
                 , ''                                    'NUM_CUENTA'
                 , ''                                    'RUT_CLIENTE'
                 , ''                                    'NOMBRE_CLIENTE'
                 , @PCODIGO_ISIN                         'CODIGO_ISIN'
                 , 0                                     'ID_NEMOTECNICO'
                 , ''                                    'NEMOTECNICO'
                 , ''                                    'DSC_INSTRUMENTO'
                 , ''                                    'DSC_PRODUCTO'
                 , ''                                    'DSC_EMISOR_ESPECIFICO'
                 , @PCODIGO_DIVISA_COTIZACION            'MONEDA'
                 , ISNULL(@PDSC_DIVISA_COTIZACION, '')   'DSC_MONEDA'
                 , CASE @PTIPO_PRODUCTO
                        WHEN 'RV' then @PTOTAL_TITULOS
                        WHEN 'IIC' THEN @PTOTAL_TITULOS
                        ELSE @PTOTAL_NOMINAL
                   END                                   'CANTIDAD'
                 , @PCOSTE_MEDIO_POSICION                'PRECIO_PROMEDIO_COMPRA'
                 , @PPRECIO_MERCADO_COTIZACION           'PRECIO_MERCADO'
                 , @PPRECIO                              'PRECIO_COMPRA'
                 , @PFECHA_VENCIMIENTO                   'FECHA_VENCIMIENTO'
                 , ROUND(CASE @PTIPO_PRODUCTO
                              WHEN 'RV'  then @PCOSTE_MEDIO_POSICION * @PTOTAL_TITULOS
                              WHEN 'IIC' then @PCOSTE_MEDIO_POSICION * @PTOTAL_TITULOS
                              ELSE @PTOTAL_NOMINAL * (@PCOSTE_MEDIO_POSICION/100)
                         END,4)                          'MONTO_INVERTIDO'
                 , @PVALOR                               'VALOR_MERCADO_MON_ORIGEN'
                 , @PVALOR                               'VALOR_MERCADO_MON_USD'
                 , @LCODRESULTADO                        'CODRESULTADO'
                 , @LMSGRESULTADO                        'MSGRESULTADO'
     ,@PCODIGO_MERCADO                       'CODMERCADO'
       END

----------------------------------------------------------------
     DROP TABLE #Tabla1

     SET NOCOUNT ON
END
GO
GRANT EXECUTE ON [PKG_SALDOS_ACTIVOS_INT$TestImportador_INV] TO DB_EXECUTESP
GO