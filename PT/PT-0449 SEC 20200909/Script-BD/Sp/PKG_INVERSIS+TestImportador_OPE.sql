IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_INVERSIS$TestImportador_OPE]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
   DROP PROCEDURE [DBO].[PKG_INVERSIS$TestImportador_OPE]
GO
CREATE PROCEDURE [dbo].[PKG_INVERSIS$TestImportador_OPE]
( @PFECHA_CIERRE                   DATETIME
, @PCUENTA_EXTERNA                 VARCHAR(50)
, @PCODIGO_ISIN                    VARCHAR(30)
, @PCOD_EMISOR                     VARCHAR(50)
, @PDSC_EMISOR                     VARCHAR(50)
, @PTIPO_PRODUCTO                  VARCHAR(30)
, @PTIPO_ACTIVO                    VARCHAR(20)
, @PDESCRIPCION_VALOR              VARCHAR(50)
, @PDIVISA_OPERACION               VARCHAR(20)
, @PDIVISA_CARGO_ABONO             VARCHAR(20)
, @PTOTAL_TITULOS                  FLOAT
, @PTOTAL_NOMINAL                  FLOAT
, @PCOSTE_MEDIO_POSICION           FLOAT
, @PPRECIO_MERCADO_COTIZACION      FLOAT
, @PPRECIO                         FLOAT
, @PFECHA_VENCIMIENTO              DATETIME
, @PVALOR                          FLOAT
, @PRATIO_CONVERSION_CONTRA_EURO   FLOAT
, @PTIPO_COTIZACION                VARCHAR(20)
, @PNUMERO_OPERACION               NUMERIC
, @PEFECTIVO_NETO                  FLOAT
, @PEFECTIVO_NETO_CARGO_ABONO      FLOAT
, @PRATIO_CONVERSION_DIVISA_CUENTA FLOAT
, @PCORRETAJE_DECLARAR_MERCADO     FLOAT
, @PPRODUCTO                       VARCHAR(4)
, @PCLASE                          VARCHAR(4)
, @PTIPO_GPI                       VARCHAR(15)
, @PGASTOS                         FLOAT
)
AS
BEGIN
     SET NOCOUNT ON

     DECLARE @LID_CUENTA             NUMERIC
           , @LNUM_CUENTA            VARCHAR(10)
           , @LCOD_ESTADO_CUENTA     VARCHAR(3)
           , @LCODRESULTADO          VARCHAR(5)
           , @LMSGRESULTADO          VARCHAR(800)
           , @LCOD_ORIGEN            VARCHAR(20)
           , @LID_ORIGEN             NUMERIC
           , @LID_TIPO_CONVERSION    NUMERIC
           , @LID_ENTIDAD            NUMERIC
           , @LID_NEMOTECNICO        NUMERIC
           , @LNEMOTECNICO           VARCHAR(50)
           , @LCOD_INSTRUMENTO       VARCHAR(15)
           , @LID_MONEDA_USD         NUMERIC
           , @LID_MONEDA_EUR         NUMERIC
           , @LID_MONEDA_CLP         NUMERIC
           , @LID_EMISOR_ESPECIFICO  NUMERIC
           , @LID_EMISOR_GENERAL     NUMERIC
           , @LDSC_MONEDA            VARCHAR(10)
           , @LID_MONEDA_OPE         NUMERIC
           , @LDSC_MONEDA_OPE        VARCHAR(10)
           , @LID_MONEDA_CARGO_ABONO NUMERIC
           , @LTIPO_MOVIMIENTO       VARCHAR(20)
           , @LFLG_TIPO_MOVIMIENTO   VARCHAR(1)
           , @LID_CAJA_CUENTA        NUMERIC
           , @LDSC_CAJA_CUENTA       VARCHAR(50)
           , @CANTIDAD               FLOAT
           , @TIPO_ACTIVO   VARCHAR(5)
           , @LCOD_INST_SVS         VARCHAR(10)
           , @LID_INSTRUMENTO_SVS   NUMERIC
           , @CLAS_INST1            NUMERIC
           , @CLAS_INST2            NUMERIC
           , @LID_EMPRESA           NUMERIC



     SET @LMSGRESULTADO = ''
     SET @LCODRESULTADO = 'OK'
---------------------------------------------------------------------------------
     SELECT @LID_MONEDA_USD = ID_MONEDA
       FROM MONEDAS
      WHERE COD_MONEDA='USD'

     SELECT @LID_MONEDA_EUR = ID_MONEDA
       FROM MONEDAS
      WHERE COD_MONEDA='EUR'

     SELECT @LID_MONEDA_CLP = ID_MONEDA
       FROM MONEDAS
      WHERE COD_MONEDA='$$'

---------------------------------------------------------------------------------
     SET @LID_MONEDA_CARGO_ABONO = @LID_MONEDA_USD
     SET @LID_CAJA_CUENTA = 0
     SET @LDSC_CAJA_CUENTA = ''
     SET @LID_MONEDA_OPE = 0
     SET @LDSC_MONEDA_OPE = ''
---------------------------------------------------------------------------------
     SELECT @LCOD_INSTRUMENTO = CASE @PTIPO_PRODUCTO WHEN 'RV' THEN 'RV_INT_INV'
                                                     WHEN 'RF' THEN 'RF_INT_INV'
                                                     WHEN 'IIC' THEN 'FFMM_INT_INV'
                                END
     SELECT @LID_EMISOR_GENERAL = ID_EMISOR_GENERAL
       FROM EMISORES_GENERALES
      WHERE DSC_EMISOR_GENERAL = CASE @LCOD_INSTRUMENTO WHEN 'RF_INT_INV' THEN 'EMISOR GENERAL RF INT'
                                                        WHEN 'RV_INT_INV' THEN 'EMISOR GENERAL RV INT'
                                                        WHEN 'FFMM_INT_INV' THEN 'EMISOR GENERAL FFMM INT'
                                 END
---------------------------------------------------------------------------------
     SET @LTIPO_MOVIMIENTO = ''
     SET @LFLG_TIPO_MOVIMIENTO = ''

     IF @PTIPO_GPI IS NULL
        BEGIN
             SET @LCODRESULTADO = 'ERROR'
             SET @LMSGRESULTADO = 'ERROR: Tipo de Operaci�n <producto, nro y clase> no configurado para : ' + @PCUENTA_EXTERNA
        END
     ELSE
        BEGIN
            IF @PPRODUCTO = 'RV' OR @PPRODUCTO = 'RF'
               BEGIN

               IF @PTIPO_GPI = 'COMPRA'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Compra'
                    SET @LFLG_TIPO_MOVIMIENTO = 'I'
                  END
               ELSE IF @PTIPO_GPI = 'VENTA'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Venta'
                    SET @LFLG_TIPO_MOVIMIENTO = 'E'
                  END
               ELSE IF @PTIPO_GPI = 'VCTO'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Abono'
                    SET @LFLG_TIPO_MOVIMIENTO = 'E'
                  END
               ELSE IF @PTIPO_GPI = 'EGRCUST' OR @PTIPO_GPI = 'EGRCUST_ARP'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Egreso Custodia'
                    SET @LFLG_TIPO_MOVIMIENTO = 'E'
                  END
               ELSE IF @PTIPO_GPI = 'INGCUST' OR @PTIPO_GPI = 'INGCUST_ARP'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Ingreso Custodia'
                    SET @LFLG_TIPO_MOVIMIENTO = 'I'
                  END
               ELSE IF @PTIPO_GPI = 'CARGO'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Cargo'
                    SET @LFLG_TIPO_MOVIMIENTO = 'I'
                  END
               ELSE IF @PTIPO_GPI = 'ABONO'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Abono'
                    SET @LFLG_TIPO_MOVIMIENTO = 'E'
                  END
               ELSE IF @PTIPO_GPI = 'APORTE'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Aporte'
                    SET @LFLG_TIPO_MOVIMIENTO = 'I'
                  END
               ELSE IF @PTIPO_GPI = 'RESCATE'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Rescate'
                    SET @LFLG_TIPO_MOVIMIENTO = 'E'
                  END
               ELSE IF @PTIPO_GPI = 'CUSTODIA'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Ingreso'
                    SET @LFLG_TIPO_MOVIMIENTO = 'I'
                  END
               ELSE
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = ''
                    SET @LFLG_TIPO_MOVIMIENTO = ''
                  END
             END
          ELSE IF @PPRODUCTO = 'IIC' /*FFMM*/
             BEGIN
               IF @PTIPO_GPI = 'COMPRA'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Inversion'
                    SET @LFLG_TIPO_MOVIMIENTO = 'I'
                  END
               ELSE IF @PTIPO_GPI = 'VENTA'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Retiro'
                    SET @LFLG_TIPO_MOVIMIENTO = 'E'
                  END
               ELSE IF @PTIPO_GPI = 'EGRCUST' or @PTIPO_GPI = 'EGRCUST_ARP'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Egreso Custodia'
                    SET @LFLG_TIPO_MOVIMIENTO = 'E'
                  END
               ELSE IF @PTIPO_GPI = 'INGCUST' or @PTIPO_GPI = 'INGCUST_ARP'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Ingreso Custodia'
                    SET @LFLG_TIPO_MOVIMIENTO = 'I'
                  END
               ELSE IF @PTIPO_GPI = 'CARGO'
                 BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Cargo'
                    SET @LFLG_TIPO_MOVIMIENTO = 'I'
                  END
               ELSE IF @PTIPO_GPI = 'ABONO'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Abono'
                    SET @LFLG_TIPO_MOVIMIENTO = 'E'
                  END
               ELSE IF @PTIPO_GPI = 'APORTE'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Aporte'
                    SET @LFLG_TIPO_MOVIMIENTO = 'I'
                  END
               ELSE IF @PTIPO_GPI = 'RESCATE'
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = 'Rescate'
                    SET @LFLG_TIPO_MOVIMIENTO = 'E'
                  END
               ELSE
                  BEGIN
                    SET @LTIPO_MOVIMIENTO = ''
                    SET @LFLG_TIPO_MOVIMIENTO = ''
                  END
             END
          ELSE
            BEGIN
              SET @LTIPO_MOVIMIENTO = ''
              SET @LFLG_TIPO_MOVIMIENTO = ''
            END
        END

     IF @LTIPO_MOVIMIENTO = '' OR @LFLG_TIPO_MOVIMIENTO = ''
        BEGIN
          SET @LCODRESULTADO = 'ERROR'
          SET @LMSGRESULTADO = 'ERROR: Tipo de Operaci�n <producto, nro y clase> no configurado para : ' + @PCUENTA_EXTERNA
        END
---------------------------------------------------------------------------------
     IF @LCODRESULTADO = 'OK'
     BEGIN
        SELECT  @LID_ORIGEN = ID_ORIGEN, @LCOD_ORIGEN = COD_ORIGEN
          FROM ORIGENES
         WHERE COD_ORIGEN = 'MAGIC_VALORES'

        IF ISNULL((SELECT COUNT(1) FROM VIEW_ALIAS WHERE TABLA='CUENTAS' AND COD_ORIGEN=@LCOD_ORIGEN AND VALOR = @PCUENTA_EXTERNA),0) <> 0
           BEGIN
                SELECT @LID_CUENTA = ID_ENTIDAD
                FROM VIEW_ALIAS
                WHERE TABLA='CUENTAS'
                  AND COD_ORIGEN=@LCOD_ORIGEN
                  AND VALOR = @PCUENTA_EXTERNA

                SELECT @LCOD_ESTADO_CUENTA = COD_ESTADO,
                       @LNUM_CUENTA = NUM_CUENTA
                FROM CUENTAS
                WHERE ID_CUENTA = @LID_CUENTA

               IF @LCOD_ESTADO_CUENTA <> 'H'
                  BEGIN
                    SET @LCODRESULTADO = 'ERROR'
                    SET @LMSGRESULTADO = 'ERROR: Cuenta ' + @LNUM_CUENTA + ' no est� vigente en GPI'
                  END
           END
        ELSE
           BEGIN
               SET @LCODRESULTADO = 'ERROR'
               SET @LMSGRESULTADO = 'ERROR: Cuenta Externa no tiene Alias en GPI: ' + @PCUENTA_EXTERNA + ''
           END
     END

---------------------------------------------------------------------------------
     IF @LCODRESULTADO = 'OK'
       BEGIN  /*LA CAJA A CONSIDERAR DEBE SER SIEMPRE CAJA D�LAR NACIONAL*/
         IF (SELECT COUNT(1) FROM CAJAS_CUENTA  WHERE ID_CUENTA = @LID_CUENTA AND ID_MONEDA = @LID_MONEDA_USD AND COD_MERCADO = 'N') = 0
             BEGIN
               SET @LCODRESULTADO = 'ERROR'
               SET @LMSGRESULTADO = 'ERROR: Cuenta GPI: ' + @LNUM_CUENTA +  ' no tiene caja Moneda D�lar Mercado Nacional'
             END
         ELSE
             SELECT @LID_CAJA_CUENTA = ID_CAJA_CUENTA,
                    @LDSC_CAJA_CUENTA = DSC_CAJA_CUENTA
               FROM CAJAS_CUENTA
              WHERE ID_CUENTA = @LID_CUENTA AND ID_MONEDA = @LID_MONEDA_USD AND COD_MERCADO = 'N'
       END
---------------------------------------------------------------------------------
     IF @LCODRESULTADO = 'OK'
      BEGIN
         SET @LCOD_ORIGEN = (SELECT ID_ORIGEN FROM ORIGENES WHERE COD_ORIGEN = 'INVERSIS')
         IF @PTIPO_GPI = 'COMPRA' OR @PTIPO_GPI = 'VENTA' OR @PTIPO_GPI = 'INGCUST' OR @PTIPO_GPI = 'EGRCUST' OR @PTIPO_GPI = 'EGRCUST_ARP' OR @PTIPO_GPI = 'INGCUST_ARP'
            SET @LID_TIPO_CONVERSION = (SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WHERE TABLA = 'OPERACIONES_DETALLE')
         ELSE IF @PTIPO_GPI = 'APORTE' OR @PTIPO_GPI = 'RESCATE'
            SET @LID_TIPO_CONVERSION = (SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WHERE TABLA = 'APORTE_RESCATE_CUENTA')
         ELSE IF @PTIPO_GPI = 'CARGO' OR @PTIPO_GPI = 'ABONO'
            SET @LID_TIPO_CONVERSION = (SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WHERE TABLA = 'CARGOS_ABONOS')

         IF (SELECT COUNT(1) FROM REL_CONVERSIONES WHERE ID_ORIGEN = @LCOD_ORIGEN
                                                     AND ID_TIPO_CONVERSION = @LID_TIPO_CONVERSION
                                                     AND VALOR = CAST(@PNUMERO_OPERACION AS VARCHAR(50))) > 0
           BEGIN
              SET @LCODRESULTADO = 'EXIST'
              SET @LMSGRESULTADO = 'ERROR: Cuenta GPI: ' + @LNUM_CUENTA +  ' ya tiene registrada esta operaci�n ' + CAST(@PNUMERO_OPERACION AS VARCHAR(50))
           END
      END
---------------------------------------------------------------------------------
     IF @LCODRESULTADO = 'OK'
      BEGIN
---------------------------------------------------------------------------------
            SELECT @TIPO_ACTIVO = TIPO_ACTIVO FROM CISCB.DBO.TB_INVERSIS_INSTRUMENTOS
            WHERE TIPO_PRODUCTO='RV' AND CODIGO_ISIN =  @PCODIGO_ISIN

            IF @@ROWCOUNT > 0
              BEGIN
              IF @TIPO_ACTIVO = 'ACE' OR @TIPO_ACTIVO = 'ACI'
                SET @LCOD_INST_SVS = 'ACE'
              IF @TIPO_ACTIVO = 'CPC' OR @TIPO_ACTIVO = 'DEE' OR @TIPO_ACTIVO = 'DEI'
                SET @LCOD_INST_SVS = 'OTROC'
              END

            SELECT @LID_INSTRUMENTO_SVS = ID_ENTIDAD
            FROM VIEW_ALIAS
            WHERE COD_ORIGEN = 'INVERSIS'
            AND TABLA = 'INSTRUMENTOS_SVS'
            AND VALOR = @LCOD_INST_SVS

           IF ISNULL((SELECT COUNT(1)
                      FROM REL_NEMOTECNICO_ATRIBUTOS
                      WHERE ISIM = @PCODIGO_ISIN),0) <> 0
            BEGIN
               IF ((SELECT COUNT(1)
                    FROM REL_NEMOTECNICO_ATRIBUTOS
                    WHERE ISIM = @PCODIGO_ISIN) > 1)
                  BEGIN
                    SET @LCODRESULTADO = 'ERROR_OTRO'
                    SET @LMSGRESULTADO = 'ERROR: ISIN (' + @PCODIGO_ISIN + ') se encuentra asociado a m�s de un nemot�cnico, por favor regularizar.'
                  END
               ELSE

                  SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO
                       , @LNEMOTECNICO    = NEMOTECNICO
                  FROM NEMOTECNICOS
                  WHERE ID_NEMOTECNICO = (SELECT ID_NEMOTECNICO
                                          FROM REL_NEMOTECNICO_ATRIBUTOS
                                          WHERE ISIM = @PCODIGO_ISIN)
                  UPDATE REL_NEMOTECNICO_ATRIBUTOS  SET ID_INSTRUMENTO_SVS = @LID_INSTRUMENTO_SVS
                  WHERE ID_NEMOTECNICO = @LID_NEMOTECNICO AND ISIM = 'ss' --@PCODIGO_ISIN
            END
           ELSE
            BEGIN
                IF @PDSC_EMISOR IS NULL
                    BEGIN
                      SET @LCODRESULTADO = 'ERROR'
                      SET @LMSGRESULTADO = 'ERROR: Emisor no suministrado. Nemot�cnico no puede ser creado '
                    END
                ELSE
                    BEGIN

                     IF NOT EXISTS(SELECT 1 FROM EMISORES_ESPECIFICO WHERE DSC_EMISOR_ESPECIFICO = @PDSC_EMISOR OR COD_EMISOR_ESPECIFICO = @PCOD_EMISOR)
                      BEGIN
                         INSERT INTO EMISORES_ESPECIFICO
                         (ID_EMISOR_GENERAL,DSC_EMISOR_ESPECIFICO,COD_EMISOR_ESPECIFICO)
                         VALUES(@LID_EMISOR_GENERAL,@PDSC_EMISOR,@PCOD_EMISOR)
                      END

                     SELECT @LID_EMISOR_ESPECIFICO = ID_EMISOR_ESPECIFICO
                     FROM EMISORES_ESPECIFICO
                     WHERE DSC_EMISOR_ESPECIFICO = @PDSC_EMISOR
                        OR COD_EMISOR_ESPECIFICO = @PCOD_EMISOR

                     EXEC PKG_NEMOTECNICOS$Guardar  @PID_NEMOTECNICO               = @LID_NEMOTECNICO OUTPUT
                                                  , @PCOD_INSTRUMENTO              = @LCOD_INSTRUMENTO
                                                  , @PNEMOTECNICO                  = @PDESCRIPCION_VALOR
                                                  , @PID_MERCADO_TRANSACCION       = 22
                                                  , @PID_EMISOR_ESPECIFICO         = @LID_EMISOR_ESPECIFICO
                                                  , @PID_MONEDA                    = @LID_MONEDA_USD
                                                  , @PID_TIPO_ESTADO               = 3
                                                  , @PCOD_ESTADO                   = 'V'
                                                  , @PDSC_NEMOTECNICO              = @PDESCRIPCION_VALOR
                                                  , @PTASA_EMISION                 = NULL
                                                  , @PTIPO_TASA                    = NULL
                                                  , @PPERIODICIDAD                 = NULL
                                                  , @PFECHA_VENCIMIENTO            = @PFECHA_VENCIMIENTO
                                                  , @PCORTE_MINIMO_PAPEL           = 0
                                                  , @PMONTO_EMISION                = 0
                                                  , @PLIQUIDEZ                     = NULL
                                                  , @PBASE                         = NULL
                                                  , @PCOD_PAIS                     = 'OTR'
                                                  , @PID_EMISOR_ESPECIFICO_ORIGEN  = @LID_EMISOR_ESPECIFICO
                                                  , @PID_MONEDA_TRANSACCION        = @LID_MONEDA_USD
                                                  , @PID_SUBFAMILIA                = NULL
                                                  , @PFLG_FUNGIBLE                 = NULL
                                                  , @PFECHA_EMISION                = @PFECHA_CIERRE
                                                  , @PFLG_TIPO_CUOTA_INGRESO       = NULL
                                                  , @PFLG_TIPO_CUOTA_EGRESO        = NULL
                                                  , @PID_USUARIO_INSERT            = 1
                                                  , @PID_USUARIO_UPDATE            = 1

                  END
                  IF @LID_NEMOTECNICO IS NULL
                  BEGIN
                       SET @LMSGRESULTADO = 'Nemot�cnico No Creado'
                       SET @LCODRESULTADO = 'ERROR'
                  END
                 ELSE
                  BEGIN
                       INSERT INTO REL_NEMOTECNICO_ATRIBUTOS (ID_NEMOTECNICO, ISIM, ID_INSTRUMENTO_SVS)
                       VALUES (@LID_NEMOTECNICO,@PCODIGO_ISIN, @LID_INSTRUMENTO_SVS)

                       IF  @LCOD_INSTRUMENTO = 'RF_INT_INV'
                           BEGIN
                                EXEC PKG_ASIGNA_RAMA_PSH_INVER  @LID_CUENTA      = @LID_CUENTA
                                                               ,@LID_NEMOTECNICO = @LID_NEMOTECNICO
                           END
                   END
            END
---------------------------------------------------------------------------------
      END
---------------------------------------------------------------------------------
      IF (@PTIPO_PRODUCTO='RV') or (@PTIPO_PRODUCTO='IIC')
         BEGIN
              SET @CANTIDAD = @PTOTAL_TITULOS
         END
      ELSE
         BEGIN
              SET @CANTIDAD =  @PTOTAL_NOMINAL
         END
---------------------------------------------------------------------------------
      IF @LCODRESULTADO = 'OK'
       BEGIN
            SELECT EM.DSC_EMPRESA
                 , 'INV'                                 'ORIGEN'
                 , @LID_CUENTA                           'ID_CUENTA'
                 , CT.NUM_CUENTA
                 , CL.RUT_CLIENTE
                 , CASE WHEN (CL.tipo_entidad = 'J') THEN CL.razon_social
                        ELSE (isnull(CL.nombres, '') + ' ' + isnull(CL.paterno, '') + ' ' + isnull(CL.materno, ''))
                   END                                   'NOMBRE_CLIENTE'
                 , @PCODIGO_ISIN                         'CODIGO_ISIN'
                 , @LID_NEMOTECNICO                      'ID_NEMOTECNICO'
                 , NE.NEMOTECNICO                        'NEMOTECNICO'
                 , NE.DSC_INSTRUMENTO                    'DSC_INSTRUMENTO'
                 , NE.DSC_PRODUCTO                       'DSC_PRODUCTO'
                 , NE.DSC_EMISOR_ESPECIFICO              'DSC_EMISOR_ESPECIFICO'
                 , @LID_MONEDA_OPE                       'ID_MONEDA_OPERACION'
                 , @PDIVISA_OPERACION                    'MONEDA'
                 , @LDSC_MONEDA_OPE                      'DSC_MONEDA_OPERACION'
                 , @LID_MONEDA_CARGO_ABONO               'ID_MONEDA_CARGO_ABONO'
                 , @CANTIDAD                             'CANTIDAD'
                 , @PCOSTE_MEDIO_POSICION                'PRECIO_PROMEDIO_COMPRA'
                 , @PPRECIO_MERCADO_COTIZACION           'PRECIO_MERCADO'
                 , CASE @PTIPO_PRODUCTO
                        WHEN 'RF' THEN @PPRECIO / 100
                        ELSE @PPRECIO
                   END                                   'PRECIO_COMPRA'
                 , @PFECHA_VENCIMIENTO                   'FECHA_VENCIMIENTO'
                 , ROUND(CASE @PTIPO_GPI
                              WHEN 'COMPRA'  THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'VENTA'   THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'APORTE'  THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'RESCATE' THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'CARGO'   THEN (@PEFECTIVO_NETO_CARGO_ABONO  * @PRATIO_CONVERSION_DIVISA_CUENTA) +
                                                  (@PCORRETAJE_DECLARAR_MERCADO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                  WHEN 'ABONO'   THEN (@PEFECTIVO_NETO_CARGO_ABONO  * @PRATIO_CONVERSION_DIVISA_CUENTA) +
                                                  (@PCORRETAJE_DECLARAR_MERCADO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              ELSE (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                         END,4)                          'MONTO_PAGO'
                 , ROUND(CASE @PTIPO_GPI
                              WHEN 'COMPRA'  THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'VENTA'   THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'APORTE'  THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'RESCATE' THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'CARGO'   THEN (@PEFECTIVO_NETO_CARGO_ABONO  * @PRATIO_CONVERSION_DIVISA_CUENTA) +
                                                  (@PCORRETAJE_DECLARAR_MERCADO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'ABONO'   THEN (@PEFECTIVO_NETO_CARGO_ABONO  * @PRATIO_CONVERSION_DIVISA_CUENTA) +
                                                  (@PCORRETAJE_DECLARAR_MERCADO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              ELSE (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                         END,4)                          'MONTO_OPERACION'
                 , @LTIPO_MOVIMIENTO                     'TIPO_MOVIMIENTO'
                 , @LFLG_TIPO_MOVIMIENTO                 'FLG_TIPO_MOVIMIENTO'
                 , @LID_CAJA_CUENTA                      'ID_CAJA_CUENTA'
                 , @LDSC_CAJA_CUENTA                     'DSC_CAJA_CUENTA'
                 , @PGASTOS * @PRATIO_CONVERSION_DIVISA_CUENTA  'GASTOS'
                 , @LCODRESULTADO                        'CODRESULTADO'
                 , @LMSGRESULTADO                        'MSGRESULTADO'
              FROM CUENTAS CT
                 , CLIENTES CL
                 , EMPRESAS EM
                 , (SELECT N.NEMOTECNICO
                         , I.DSC_INTRUMENTO DSC_INSTRUMENTO
                         , P.DSC_PRODUCTO
                         , E.DSC_EMISOR_ESPECIFICO
                      FROM NEMOTECNICOS N
                         , INSTRUMENTOS I
                         , PRODUCTOS P
                         , EMISORES_ESPECIFICO E
                     WHERE I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
                       AND P.COD_PRODUCTO = I.COD_PRODUCTO
                       AND N.ID_NEMOTECNICO = @LID_NEMOTECNICO
                       AND E.ID_EMISOR_ESPECIFICO = N.ID_EMISOR_ESPECIFICO) NE
             WHERE CT.ID_CUENTA  = @LID_CUENTA
               AND CL.ID_CLIENTE = CT.ID_CLIENTE
               AND EM.ID_EMPRESA = CT.ID_EMPRESA
       END
      ELSE
       BEGIN
            SELECT ''                                    'DSC_EMPRESA'
                 , 'INV'                                 'ORIGEN'
                 , 0                                     'ID_CUENTA'
                 , ''                                    'NUM_CUENTA'
                 , ''                                    'RUT_CLIENTE'
                 , ''                                    'NOMBRE_CLIENTE'
                 , @PCODIGO_ISIN                         'CODIGO_ISIN'
                 , 0                                     'ID_NEMOTECNICO'
                 , ''                                    'NEMOTECNICO'
                 , ''                                    'DSC_INSTRUMENTO'
                 , ''                                    'DSC_PRODUCTO'
                 , ''                                    'DSC_EMISOR_ESPECIFICO'
                 , @LID_MONEDA_OPE                       'ID_MONEDA_OPERACION'
                 , @PDIVISA_OPERACION                    'MONEDA'
                 , @LDSC_MONEDA_OPE                      'DSC_MONEDA_OPERACION'
                 , @LID_MONEDA_CARGO_ABONO               'ID_MONEDA_CARGO_ABONO'
                 , @CANTIDAD                             'CANTIDAD'
                 , @PCOSTE_MEDIO_POSICION                'PRECIO_PROMEDIO_COMPRA'
                 , @PPRECIO_MERCADO_COTIZACION           'PRECIO_MERCADO'
                 , CASE @PTIPO_PRODUCTO
                        WHEN 'RF' THEN @PPRECIO / 100
                        ELSE @PPRECIO
                   END                                   'PRECIO_COMPRA'
                 , @PFECHA_VENCIMIENTO                   'FECHA_VENCIMIENTO'
                 , ROUND(CASE @PTIPO_GPI
                              WHEN 'COMPRA'  THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'VENTA'   THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'APORTE'  THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'RESCATE' THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'CARGO'   THEN (@PEFECTIVO_NETO_CARGO_ABONO  * @PRATIO_CONVERSION_DIVISA_CUENTA) +
                                                  (@PCORRETAJE_DECLARAR_MERCADO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'ABONO'   THEN (@PEFECTIVO_NETO_CARGO_ABONO  * @PRATIO_CONVERSION_DIVISA_CUENTA) +
                                                  (@PCORRETAJE_DECLARAR_MERCADO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              ELSE (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)END,4)  'MONTO_PAGO'
                 , ROUND(CASE @PTIPO_GPI
                              WHEN 'COMPRA'  THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'VENTA'   THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'APORTE'  THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'RESCATE' THEN (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'CARGO'   THEN (@PEFECTIVO_NETO_CARGO_ABONO  * @PRATIO_CONVERSION_DIVISA_CUENTA) +
                                                  (@PCORRETAJE_DECLARAR_MERCADO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              WHEN 'ABONO'   THEN (@PEFECTIVO_NETO_CARGO_ABONO  * @PRATIO_CONVERSION_DIVISA_CUENTA) +
                                                  (@PCORRETAJE_DECLARAR_MERCADO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                              ELSE (@PEFECTIVO_NETO * @PRATIO_CONVERSION_DIVISA_CUENTA)
                         END,4)                          'MONTO_OPERACION'
                 , @LTIPO_MOVIMIENTO                     'TIPO_MOVIMIENTO'
                 , @LFLG_TIPO_MOVIMIENTO                 'FLG_TIPO_MOVIMIENTO'
                 , @LID_CAJA_CUENTA                      'ID_CAJA_CUENTA'
                 , @LDSC_CAJA_CUENTA                     'DSC_CAJA_CUENTA'
                 , @PGASTOS                              'GASTOS'
                 , @LCODRESULTADO                        'CODRESULTADO'
                 , @LMSGRESULTADO                        'MSGRESULTADO'
       END
----------------------------------------------------------------
     SET NOCOUNT OFF
END
GO
GRANT EXECUTE ON [PKG_INVERSIS$TestImportador_OPE] TO DB_EXECUTESP
GO