IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_PROCESOS_IMPORTADOR$Busca_ID_MOV_ACTIVO_RFIJA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_PROCESOS_IMPORTADOR$Busca_ID_MOV_ACTIVO_RFIJA]
GO
CREATE PROCEDURE DBO.PKG_PROCESOS_IMPORTADOR$Busca_ID_MOV_ACTIVO_RFIJA(
  @PID_NEMOTECNICO  NUMERIC
, @PID_CUENTA       NUMERIC
, @PFECHA_OPERACION DATETIME)
AS
BEGIN
      SELECT FECHA_OPERACION as FECHA_COMPRA,
             ID_MOV_ACTIVO,
             SALDO as SALDO_CANTIDAD
       FROM (SELECT FECHA_OPERACION,ID_MOV_ACTIVO, ISNULL((CANTIDAD - (SELECT SUM(CANTIDAD) 
                                                                       FROM MOV_ACTIVOS
                                                                       WHERE ID_MOV_ACTIVO_COMPRA = M.ID_MOV_ACTIVO
                                                                         AND COD_ESTADO = 'C'
                                                                         AND FECHA_OPERACION <= @PFECHA_OPERACION)  
                                                             ),M.CANTIDAD) 'SALDO'
                 FROM (SELECT FECHA_OPERACION,ID_MOV_ACTIVO, SUM(CANTIDAD)  CANTIDAD
                         FROM MOV_ACTIVOS 
                        WHERE FLG_TIPO_MOVIMIENTO = 'I'
                          AND COD_ESTADO='C'
                          AND ID_NEMOTECNICO = @PID_NEMOTECNICO
                          AND ID_CUENTA = @PID_CUENTA
                          AND FECHA_OPERACION <= @PFECHA_OPERACION
                       GROUP BY FECHA_OPERACION,ID_MOV_ACTIVO) M
               ) MOV
       WHERE MOV.SALDO > 0 
       ORDER BY FECHA_OPERACION, ID_MOV_ACTIVO
END 
GO
GRANT EXECUTE ON [PKG_PROCESOS_IMPORTADOR$Busca_ID_MOV_ACTIVO_RFIJA] TO DB_EXECUTESP
GO