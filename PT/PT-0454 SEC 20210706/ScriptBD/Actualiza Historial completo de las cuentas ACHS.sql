
Declare  @FECHA          datetime ,        @ID_CUENTA             numeric(10),     @ID_NEMOTECNICO    numeric(10),
	     @ID_OPERACION   numeric(10),      @ID_OPERACION_DETALLE  numeric(8) ,     @ID_MOV_ACTIVO     numeric(10),
	     @CANTIDAD       numeric(18,4),    @SALDO_CANTIDAD        numeric(18,4),   @Fecha_Desde       datetime   ,
         @Fecha_Hasta    datetime     ,    @Correlativo           numeric(18)  

set @Fecha_Desde = '20170309' 
set @Fecha_Hasta = '20170309' 

Create TABLE #Tabla1 ( Correlativo     numeric(18) identity, 
                       FECHA           datetime,
                       ID_CUENTA       numeric(10),
                       ID_NEMOTECNICO  numeric(10),
                       ID_OPERACION    numeric(10),
                       ID_OPERACION_DETALLE numeric(8),
                       ID_MOV_ACTIVO   numeric(10),
                       CANTIDAD        numeric(18,4),
                       SALDO_CANTIDAD  numeric(18,4))



insert into #Tabla1
select distinct o.fecha_operacion, vs.ID_CUENTA, vs.id_nemotecnico, o.id_operacion, od.id_operacion_detalle, vs.id_mov_activo, od.cantidad, vs.cantidad 
from VIEW_SALDOS_ACTIVOS vs
left join mov_activos ma on vs.id_mov_activo = ma.id_mov_activo
left join operaciones_detalle od on ma.id_operacion_detalle = od.id_operacion_detalle
left join operaciones o on od.id_operacion = o.id_operacion
where vs.ID_CUENTA  IN (SELECT CS.ID_CUENTA FROM CUENTAS_SIN_FUNGIR_RF CS)  --= -511
--  and vs.FECHA_CIERRE between  @Fecha_Desde and @Fecha_Hasta 
  and not exists (select 1 from RF_ENLACE_COMPRA r where r.fecha = o.fecha_operacion 
                                                   and r.ID_CUENTA = vs.ID_CUENTA
                                                   and r.id_nemotecnico = vs.id_nemotecnico
                                                   and r.id_operacion = o.id_operacion
                                                   and r.id_operacion_detalle = od.id_operacion_detalle
                                                   and r.id_mov_activo = vs.id_mov_activo)
order by o.fecha_operacion --o.id_operacion
--
--select * from #Tabla1
--drop table #Tabla1
--return

  Declare @id int, @count int             
     Set @id=1            
     select @count=count(1)from #Tabla1              
     while @id<=@count                 
            begin                      

                 select @FECHA                = FECHA,
                        @ID_CUENTA            = ID_CUENTA,
                        @ID_NEMOTECNICO       = ID_NEMOTECNICO,
                        @ID_OPERACION         = ID_OPERACION,
                        @ID_OPERACION_DETALLE = ID_OPERACION_DETALLE,
                        @ID_MOV_ACTIVO        = ID_MOV_ACTIVO,
                        @CANTIDAD             = CANTIDAD,
                        @SALDO_CANTIDAD       = SALDO_CANTIDAD
                 from #Tabla1 where Correlativo =@id     

--                select @FECHA, @ID_CUENTA, @ID_NEMOTECNICO,
--                       @ID_OPERACION, @ID_OPERACION_DETALLE,
--                       @ID_MOV_ACTIVO, @CANTIDAD, @SALDO_CANTIDAD

                 EXEC DBO.PKG_CARGAS_RFN_BON_NAC$REL_ENLACE_VENTA @FECHA    , 
                                                                  @ID_CUENTA, 
                                                                  @ID_NEMOTECNICO,
                                                                  @ID_OPERACION, 
                                                                  @ID_OPERACION_DETALLE,
                                                                  @ID_MOV_ACTIVO, 
                                                                  @CANTIDAD, 
                                                                  @SALDO_CANTIDAD

                select @id=@id+1  
            end    

--select * from #Tabla1

drop table #Tabla1