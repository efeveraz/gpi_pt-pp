----------------------------------------------------------------------------------
IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'COMPRAS')
   BEGIN
       DROP TABLE COMPRAS 
   END 
----------------------------------------------------------------------------------
 IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'OPER_AUX')
   BEGIN
       DROP TABLE OPER_AUX
   END 
-----***** COMPRAS  -------------------------------------------------------------
DECLARE @ID_MOV_ACTIVO            NUMERIC(10),
        @SALDO_CANTIDAD           NUMERIC(18,4),
        @FECHA_COMPRA             DATETIME

DECLARE @ID_COM                   INT,
        @COUNT_COM                INT

-----***** OPERACIONES  ---------------------------------------------------------
DECLARE @OP_ID_OPERACION          NUMERIC(10), 
        @OP_ID_OPERACION_AUX      NUMERIC(10), 
        @OP_ID_OPERACION_DETALLE  NUMERIC(10),   
        @OP_ID_NEMOTECNICO        NUMERIC(10),
        @OP_ID_NEMOTECNICO_AUX    NUMERIC(10),
        @OP_ID_CUENTA             NUMERIC(10),     
        @OP_FECHA_OPERACION       DATETIME,
        @OP_CANTIDAD              NUMERIC(18,4),
        @CANTIDAD_AUX             NUMERIC(18,4)

DECLARE @ID_OPE                   INT,
        @COUNT_OPE                INT

-----***** TABLAS  ---------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'COMPRAS' )
   BEGIN
       CREATE TABLE COMPRAS(NUM_REG_COM         numeric(10) IDENTITY(1,1) NOT NULL,
                            COM_ID_MOV_ACTIVO   NUMERIC(10),
                            COM_SALDO           NUMERIC(18,4),
                            COM_FECHA_OPERACION DATETIME) 
   END 
----------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'OPER_AUX' )
   BEGIN
       CREATE TABLE OPER_AUX(NUM_REG_OPE              numeric(10) IDENTITY(1,1) NOT NULL,
                             OP_ID_OPERACION          NUMERIC(10), 
                             OP_ID_OPERACION_DETALLE  NUMERIC(10),   
                             OP_ID_NEMOTECNICO        NUMERIC(10),
                             OP_ID_CUENTA             NUMERIC(10),     
                             OP_FECHA_OPERACION       DATETIME,
                             OP_CANTIDAD              NUMERIC(18,4)) 
   END 
----------------------------------------------------------------------------------
SET @OP_ID_NEMOTECNICO_AUX = 0
SET @OP_ID_OPERACION_AUX = 0
SET @COUNT_OPE = 0
SET @COUNT_COM = 0
SET @CANTIDAD_AUX = 0

DELETE FROM OPER_AUX

 /******* OPERACIONES *******/
  INSERT INTO OPER_AUX 
  SELECT O.ID_OPERACION, 
         OD.ID_OPERACION_DETALLE,   
         OD.ID_NEMOTECNICO  ,
         O.ID_CUENTA  ,     
         O.FECHA_OPERACION,
         OD.CANTIDAD   
  FROM OPERACIONES O, OPERACIONES_DETALLE OD 
  WHERE O.ID_OPERACION = OD.ID_OPERACION
    AND O.FLG_TIPO_MOVIMIENTO  = 'E'            
    AND O.COD_TIPO_OPERACION   = 'INST'  
    AND O.COD_PRODUCTO         = 'RF_NAC'
    AND O.COD_INSTRUMENTO      = 'BONOS_NAC'
    AND O.ID_CUENTA IN (SELECT CS.ID_CUENTA FROM CUENTAS_SIN_FUNGIR_RF CS)
    AND ISNULL(OD.ID_MOV_ACTIVO_COMPRA,0) = 0
    AND O.COD_ESTADO <> 'A'
    ORDER BY O.FECHA_OPERACION,O.ID_OPERACION,OD.ID_OPERACION_DETALLE
 
  SET @ID_OPE=1
  SELECT @COUNT_OPE=COUNT(1)FROM OPER_AUX 

Continua_Operacion:
 WHILE @ID_OPE<=@COUNT_OPE
      BEGIN
           SELECT @OP_ID_OPERACION         = OP_ID_OPERACION,
                  @OP_ID_OPERACION_DETALLE = OP_ID_OPERACION_DETALLE ,   
                  @OP_ID_NEMOTECNICO       = OP_ID_NEMOTECNICO,
                  @OP_ID_CUENTA            = OP_ID_CUENTA,     
                  @OP_FECHA_OPERACION      = OP_FECHA_OPERACION,
                  @OP_CANTIDAD             = OP_CANTIDAD
           FROM OPER_AUX WHERE NUM_REG_OPE=@ID_OPE

           /** CUANDO CAMBIE DE NEMO U OPERACION SE LLENA TABLA DE COMPRAS CORRESPONDIENTE**/
            IF (@OP_ID_NEMOTECNICO <> @OP_ID_NEMOTECNICO_AUX) --or (@OP_ID_OPERACION <> @OP_ID_OPERACION_AUX)
                 BEGIN 
                      --SELECT @OP_ID_NEMOTECNICO,@OP_ID_NEMOTECNICO_AUX, @OP_ID_OPERACION,@OP_ID_OPERACION_AUX
                      DELETE FROM COMPRAS
                      INSERT INTO COMPRAS

                      SELECT ID_MOV_ACTIVO,
                             SALDO,
                             FECHA_OPERACION AS FECHA_COMPRA
                      FROM (SELECT FECHA_OPERACION,ID_MOV_ACTIVO, ISNULL((CANTIDAD - (SELECT SUM(CANTIDAD) 
                                                                                      FROM MOV_ACTIVOS
                                                                                      WHERE ID_MOV_ACTIVO_COMPRA = M.ID_MOV_ACTIVO
                                                                                        AND COD_ESTADO = 'C'  
                                                                                        AND FECHA_OPERACION <= @OP_FECHA_OPERACION)),M.CANTIDAD) 'SALDO'
                            FROM (SELECT FECHA_OPERACION,ID_MOV_ACTIVO, SUM(CANTIDAD)  CANTIDAD
                                  FROM MOV_ACTIVOS 
                                  WHERE FLG_TIPO_MOVIMIENTO = 'I'
                                    AND COD_ESTADO='C'
                                    AND ID_NEMOTECNICO = @OP_ID_NEMOTECNICO
                                    AND ID_CUENTA = @OP_ID_CUENTA
                                    AND FECHA_OPERACION <= @OP_FECHA_OPERACION
                                  GROUP BY FECHA_OPERACION,ID_MOV_ACTIVO) M
                                  ) MOV
                     WHERE MOV.SALDO > 0 
                     ORDER BY FECHA_OPERACION, ID_MOV_ACTIVO
 
                     /*Iniciarlizar las variables @id y @count*/
                     /**********************/
                     SET @ID_COM=1
                     SELECT @COUNT_COM=COUNT(1)FROM COMPRAS 
                 END 

            IF @COUNT_COM = 0 
               BEGIN 
                    SELECT @COUNT_COM  AS COUNT_COM 
                    SET @ID_OPE = @ID_OPE + 1
                    Goto Continua_Operacion 
               END 

            SET @OP_ID_NEMOTECNICO_AUX = @OP_ID_NEMOTECNICO

Continua_Saldo:
              WHILE (@ID_COM<=@COUNT_COM)  
                    BEGIN
                          SELECT @ID_MOV_ACTIVO  = COM_ID_MOV_ACTIVO ,
                                 @SALDO_CANTIDAD = COM_SALDO,
                                 @FECHA_COMPRA   = COM_FECHA_OPERACION
                          FROM COMPRAS
                          WHERE NUM_REG_COM=@ID_COM

                          IF (@SALDO_CANTIDAD >= @OP_CANTIDAD)  
                              BEGIN 
                                   SET @SALDO_CANTIDAD = (@SALDO_CANTIDAD - @OP_CANTIDAD) 

                                   EXEC DBO.PKG_CARGAS_RFN_BON_NAC$REL_ENLACE_VENTA @FECHA_COMPRA           ,    
                                                                                    @OP_ID_CUENTA           ,      
                                                                                    @OP_ID_NEMOTECNICO      ,    
                                                                                    @OP_ID_OPERACION        ,    
                                                                                    @OP_ID_OPERACION_DETALLE,    
                                                                                    @ID_MOV_ACTIVO          ,    
                                                                                    @OP_CANTIDAD            ,    
                                                                                    @SALDO_CANTIDAD      

                                    UPDATE COMPRAS SET COM_SALDO = @SALDO_CANTIDAD
                                    WHERE COM_ID_MOV_ACTIVO = @ID_MOV_ACTIVO
   
                                    SET @ID_OPE = @ID_OPE + 1
                                    Goto Continua_Operacion
                              END 
                          ELSE    
                              BEGIN   
                                   SET @CANTIDAD_AUX = (@CANTIDAD_AUX + @SALDO_CANTIDAD)
                                   SET @SALDO_CANTIDAD = (@OP_CANTIDAD - @CANTIDAD_AUX) 

                                   EXEC DBO.PKG_CARGAS_RFN_BON_NAC$REL_ENLACE_VENTA @FECHA_COMPRA           ,    
                                                                                    @OP_ID_CUENTA           ,      
                                                                                    @OP_ID_NEMOTECNICO      ,    
                                                                                    @OP_ID_OPERACION        ,    
                                                                                    @OP_ID_OPERACION_DETALLE,    
                                                                                    @ID_MOV_ACTIVO          ,    
                                                                                    @SALDO_CANTIDAD         ,    
                                                                                    0      

                                   If @CANTIDAD_AUX < @OP_CANTIDAD
                                      BEGIN
                                           SET @ID_COM=@ID_COM+1
                                           Goto Continua_Saldo
                                      END
                                  Else
                                      BEGIN
                                           UPDATE COMPRAS SET COM_SALDO = @SALDO_CANTIDAD
                                           WHERE COM_ID_MOV_ACTIVO = @ID_MOV_ACTIVO
                                      END
                              END 
                    END
      END
   
----------------------------------------------------------------------------------
IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'COMPRAS')
   BEGIN
       DROP TABLE COMPRAS
   END 
----------------------------------------------------------------------------------
 IF EXISTS(SELECT 1 FROM SYSOBJECTS WHERE NAME = 'OPER_AUX')
   BEGIN
       DROP TABLE OPER_AUX
   END 
----------------------------------------------------------------------------------





