IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_CARGAS_OPERACIONES$CorteCupon]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_CARGAS_OPERACIONES$CorteCupon]
GO
CREATE PROCEDURE PKG_CARGAS_OPERACIONES$CorteCupon        
( @PID_CUENTA             NUMERIC            
, @PFECHA                 DATETIME            
, @PFECHA_LIQUIDACION     DATETIME            
, @PID_NEMOTECNICO        NUMERIC            
, @PNRO_OPE               VARCHAR(20)        
, @PVALOR                 FLOAT        
, @PCANTIDAD              FLOAT            
, @PPRECIO                FLOAT        
, @PFECHA_VENCIMIENTO     DATETIME        
) AS           
BEGIN        
     SET NOCOUNT ON          
BEGIN TRY        
    DECLARE @LRESULTADO             VARCHAR(1000)        
          , @LMSG_RESULTADO         VARCHAR(1000)        
          , @LCOD_RESULTADO         VARCHAR(5)        
          , @LFCH_PROX_CORTE_CUPON  DATETIME         
          , @LID_CARGO_ABONO        NUMERIC            
          , @LDSC_CARGO_ABONO       VARCHAR(100)            
          , @LFLUJO                 FLOAT            
          , @LNEMOTECNICO           VARCHAR(100)            
          , @LID_MONEDA_CAJA        NUMERIC            
          , @LCOD_MERCADO           CHAR            
          , @LCOD_SUBFAMILIA        VARCHAR(20)        
          , @LDSC_OPERACION         VARCHAR(1000)            
          , @LRETENCION             NUMERIC            
          , @LTOTAL_CUPONES         NUMERIC        
          , @LNRO_CUPON             NUMERIC        
          , @LID_MOV_ACTIVO         NUMERIC        
          , @LID_OPERACION          NUMERIC        
          , @LID_USUARIO            NUMERIC        
          , @LID_ORIGEN             NUMERIC        
          , @LID_TIPO_CONVERSION    NUMERIC        
        
    SELECT @LID_ORIGEN = ID_ORIGEN         
      FROM ORIGENES        
     WHERE COD_ORIGEN='MAGIC_CARGA_OPE'        
        
    SELECT @LID_TIPO_CONVERSION = ID_TIPO_CONVERSION        
      FROM TIPOS_CONVERSION        
     WHERE TABLA='CARGOS_ABONOS'        
---------------------------------------------------------------------------------            
    SET @LID_USUARIO = 1        
---------------------------------------------------------------------------------            
    SET @LFLUJO = @PVALOR        
    SET @LRESULTADO = ''        
    SET @LCOD_RESULTADO = ''        
            
---------------------------------------------------------------------------------            
    SELECT @LNEMOTECNICO    = N.NEMOTECNICO            
         , @LID_MONEDA_CAJA = N.ID_MONEDA_TRANSACCION            
         , @LCOD_MERCADO    = P.COD_MERCADO            
         , @LCOD_SUBFAMILIA = COD_SUBFAMILIA        
      FROM NEMOTECNICOS N            
     INNER JOIN INSTRUMENTOS I  ON I.COD_INSTRUMENTO  = N.COD_INSTRUMENTO            
     INNER JOIN PRODUCTOS P     ON P.COD_PRODUCTO     = I.COD_PRODUCTO            
     INNER JOIN SUBFAMILIAS S   ON S.ID_SUBFAMILIA    = N.ID_SUBFAMILIA        
     WHERE N.ID_NEMOTECNICO = @PID_NEMOTECNICO            
               
----------------------------------------------------------------------------------         
    IF @PFECHA = @PFECHA_VENCIMIENTO         
     BEGIN        
          SET @LRETENCION = 0        
          SELECT @LDSC_OPERACION = DSC_EGRESO        
            FROM TIPOS_OPERACIONES        
           WHERE COD_TIPO_OPERACION = 'VENC_CUPON'            
          
          SET @LDSC_OPERACION = REPLACE(@LDSC_OPERACION, '<<NEMOTECNICOS>>', @LNEMOTECNICO)            
          --SET @LDSC_OPERACION = REPLACE(@LDSC_OPERACION, '<<CANTIDAD>>', CAST(@PCANTIDAD AS NVARCHAR))            
        
          SET @LID_MOV_ACTIVO = NULL        
            
          EXECUTE DBO.PKG_OPERACIONES$MAKE_1_LINE @PID_CUENTA            = @PID_CUENTA           
                                                , @PFLG_TIPO_MOVIMIENTO  = 'E'          
                                                , @PCOD_TIPO_OPERACION   = 'VENC_CUPON'          
                                                , @PDSC_OPERACION        = @LDSC_OPERACION          
                                                , @PFECHA_OPERACION      = @PFECHA        
                                                , @PFECHA_LIQUIDACION    = @PFECHA_LIQUIDACION         
                                                , @PID_NEMOTECNICO       = @PID_NEMOTECNICO          
                                                , @PNEMOTECNICO          = @LNEMOTECNICO          
                                                , @PCANTIDAD             = @PCANTIDAD          
                                                , @PPRECIO               = @PPRECIO        
                                                , @PMONTO_PAGO           = @PVALOR         
                                                , @PID_MOV_ACTIVO_COMPRA = @LID_MOV_ACTIVO          
                                                , @PID_OPERACION         = @LID_OPERACION  OUTPUT          
                                                , @PID_USUARIO           = @LID_USUARIO          
                                                , @PRESULTADO            = @LRESULTADO OUTPUT        
          IF @LRESULTADO IS NULL        
           BEGIN        
             SET @LRESULTADO = 'Operaci�n Vencimiento Generada (N�. ' + CAST(@LID_OPERACION AS NVARCHAR) + ').  '          
           END        
     END        
        
    SET @LRETENCION = DATEDIFF(DAY, @PFECHA, @PFECHA_LIQUIDACION)               
            
    SELECT @LDSC_CARGO_ABONO = DSC_GLOSA_ABONO         
      FROM MOV_CAJA_ORIGEN             
     WHERE COD_ORIGEN_MOV_CAJA = 'CORTECUPON'            
            
    SET @LDSC_CARGO_ABONO = REPLACE(@LDSC_CARGO_ABONO, '<<NEMOTECNICO>>', @LNEMOTECNICO)            
    SET @LDSC_CARGO_ABONO = REPLACE(@LDSC_CARGO_ABONO, '<<CANTIDAD>>', CAST(CONVERT(NUMERIC,@PCANTIDAD) AS NVARCHAR))            
            
    EXECUTE PKG_CARGOS_ABONOS$MAKE @PID_CARGO_ABONO       = @LID_CARGO_ABONO OUTPUT            
                                 , @PCOD_ORIGEN_MOV_CAJA  = 'CORTECUPON'            
                                 , @PID_CAJA_CUENTA       = NULL            
                                 , @PID_CUENTA            = @PID_CUENTA            
                                 , @PID_MONEDA            = @LID_MONEDA_CAJA            
                                 , @PCOD_MERCADO          = @LCOD_MERCADO            
                                 , @PCOD_ESTADO           = 'P'            
                                 , @PDSC_CARGO_ABONO      = @LDSC_CARGO_ABONO            
                                 , @PFECHA_MOVIMIENTO     = @PFECHA            
                                 , @PFLG_TIPO_CARGO       = 'A'            
                                 , @PMONTO                = @LFLUJO             
                                 , @PRETENCION            = @LRETENCION            
                                 , @PFLG_TIPO_ORIGEN      = 'A'            
                                 , @PAUTOMATICO           = 'S'    
                                 , @PCANTIDAD             = @PCANTIDAD    
                                 , @PPRECIO               = @PPRECIO    
                                 , @PID_NEMOTECNICO       = @PID_NEMOTECNICO       
    
            
        
    INSERT INTO REL_CONVERSIONES        
    SELECT @LID_ORIGEN        
         , @LID_TIPO_CONVERSION        
         , @LID_CARGO_ABONO        
         , @PNRO_OPE        
            
    SET @LMSG_RESULTADO = @LRESULTADO + 'Corte de Cup�n Generado (N�. ' + CAST(@LID_CARGO_ABONO AS NVARCHAR) + ')'        
    SET @LCOD_RESULTADO = 'OK'        
        
END TRY        
BEGIN CATCH        
  SET @LCOD_RESULTADO = 'ERROR'        
  SET @LMSG_RESULTADO = '[' + ERROR_PROCEDURE() + ':' + CAST(ERROR_LINE() AS NVARCHAR) + ':' + CAST(ERROR_NUMBER() AS NVARCHAR) + ']' + CHAR(13) +          
                        ' ERROR=' + ERROR_MESSAGE()        
END CATCH        
   SELECT @LCOD_RESULTADO 'CODRESULTADO'        
        , @LMSG_RESULTADO 'MSGRESULTADO'            
        , ISNULL(@LID_OPERACION,0)  'ID_OPERACION'
   SET NOCOUNT OFF          
END   
GO
GRANT EXECUTE ON [PKG_CARGAS_OPERACIONES$CorteCupon] TO DB_EXECUTESP
GO