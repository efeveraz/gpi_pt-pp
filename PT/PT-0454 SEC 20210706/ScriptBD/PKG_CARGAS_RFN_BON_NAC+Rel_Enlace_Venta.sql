IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_CARGAS_RFN_BON_NAC$Rel_Enlace_Venta]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_CARGAS_RFN_BON_NAC$Rel_Enlace_Venta]
GO
CREATE PROCEDURE DBO.PKG_CARGAS_RFN_BON_NAC$Rel_Enlace_Venta(  
@PFECHA                DATETIME,  
@PID_CUENTA            NUMERIC(10),  
@PID_NEMOTECNICO       NUMERIC(10),  
@PID_OPERACION         NUMERIC(10),  
@PID_OPERACION_DETALLE NUMERIC(10),  
@PID_MOV_ACTIVO        NUMERIC(10),  
@PCANTIDAD             NUMERIC(18,4),  
@PSALDO_CANTIDAD       NUMERIC(18,4)  
)  
AS              
BEGIN    
     INSERT INTO RF_ENLACE_COMPRA ( FECHA,  
                                    ID_CUENTA,  
                                    ID_NEMOTECNICO,  
                                    ID_OPERACION, 
                                    ID_OPERACION_DETALLE,  
                                    ID_MOV_ACTIVO,  
                                    CANTIDAD,  
                                    SALDO_CANTIDAD)  
     VALUES                       ( @PFECHA, 
                                    @PID_CUENTA, 
                                    @PID_NEMOTECNICO, 
                                    @PID_OPERACION, 
                                    @PID_OPERACION_DETALLE, 
                                    @PID_MOV_ACTIVO, 
                                    @PCANTIDAD, 
                                    @PSALDO_CANTIDAD)  

     /*** SE ACTUALIZA ID_MOV_ACTIVO_COMPRA ****/  
     UPDATE OPERACIONES_DETALLE  
       SET ID_MOV_ACTIVO_COMPRA = @PID_MOV_ACTIVO  
     WHERE ID_OPERACION_DETALLE = @PID_OPERACION_DETALLE  
       AND ID_OPERACION         = @PID_OPERACION  

     UPDATE MOV_ACTIVOS    
       SET ID_MOV_ACTIVO_COMPRA = @PID_MOV_ACTIVO    
     WHERE ID_OPERACION_DETALLE = @PID_OPERACION_DETALLE    
    /*******************************************/

END 
GO
GRANT EXECUTE ON [PKG_CARGAS_RFN_BON_NAC$Rel_Enlace_Venta] TO DB_EXECUTESP
GO


