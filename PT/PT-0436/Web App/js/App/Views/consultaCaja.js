﻿var jsfechaConsulta;
var jsfechaConsultaHasta;
var jsfechaFormato;
var jsfechaFormatoHasta;
var arrayCajas = [];

function resize() {
    var height;
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#Paginador").outerHeight(true) - 221;
    if (ancho < 768) {
        $("#ListaMovCaja").setGridHeight(165);
        $("#ListaDetalleMov").setGridHeight(165);
        $("#panelfiltro").height(365);
        $("#DDCajaCta").removeAttr("style");
    } else {
        $("#ListaMovCaja").setGridHeight(height * 0.7);
        $("#ListaDetalleMov").setGridHeight(height * 0.3);
        $("#panelfiltro").height($("#panelDerecho").height() - 30);
        $("#DDCajaCta").width(130)
    }
}

function initConsultaCaja() {
    $("#idSubtituloPaginaText").text("Consulta Caja");
    document.title = "Consulta Caja";

    estadoBtnConsultar();

    if (!miInformacionCuenta) {
        $("#dtFechaConsultaHasta").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
        $("#dtFechaConsulta").datepicker('setDate', moment(new Date(localStorage.UltimaFechaCierre)).add(-1, 'months').toDate());
    } else {
        $("#dtFechaConsultaHasta").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
        $("#dtFechaConsulta").datepicker('setDate', moment($("#dtFechaConsultaHasta").datepicker('getDate')).add(-1, 'months').format('L'));
    }

    $("#tabsClienteCuentaGrupo").hide();
    $('.nav-tabs a[href="#tabCuenta"]').tab('show');

    if (miInformacionCuenta) {
        cargaCajaCuentas();
    }
}
function cargarEventHandlersConsultaCaja() {

    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $("#dtRangoFecha").datepicker();

    $("#ListaMovCaja").jqGrid({
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID" },
        colNames: ['ID', 'Fecha Movimiento', 'Fecha Liquidación', 'Origen', 'Glosa', 'Caja', 'Monto Ingreso', 'Monto Egreso', 'ID_CAJA_CUENTA', 'ID_MOV_CAJA', 'ID_OPERACION', 'DECIMALES'],
        colModel: [
            { name: "ID", index: "ID", sortable: false, width: 120, sorttype: "int", editable: false, search: true, hidden: true },
            { name: "FECHA_MOVIMIENTO", index: "FECHA_MOVIMIENTO", sortable: true, width: 130, sorttype: "date", formatter: "date", editable: false, search: true, hidden: false, align: "center", searchoptions: { sopt: ["ge", "le", "eq"], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "FECHA_LIQUIDACION", index: "FECHA_LIQUIDACION", sortable: true, width: 130, sorttype: "date", formatter: "date", editable: false, search: true, hidden: false, align: "center", searchoptions: { sopt: ["ge", "le", "eq"], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "DSC_ORIGEN_MOV_CAJA", index: "DSC_ORIGEN_MOV_CAJA", sortable: true, width: 200, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "DSC_MOV_CAJA", index: "DSC_MOV_CAJA", sortable: true, width: 330, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "DSC_CAJA_CUENTA", index: "DSC_CAJA_CUENTA", sortable: true, width: 110, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "MONTO_ING", index: "MONTO_ING", sortable: true, width: 130, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "MONTO_EGR", index: "MONTO_EGR", sortable: true, width: 130, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "ID_CAJA_CUENTA", index: "ID_CAJA_CUENTA", sortable: true, width: 100, sorttype: "integer", formatter: "integer", editable: false, search: true, hidden: true },
            { name: "ID_MOV_CAJA", index: "ID_MOV_CAJA", sortable: true, width: 100, sorttype: "integer", formatter: "integer", editable: false, search: true, hidden: true },
            { name: "ID_OPERACION", index: "ID_OPERACION", sortable: true, width: 100, sorttype: "integer", formatter: "integer", editable: false, search: true, hidden: true },
            { name: "DECIMALES", index: "DECIMALES", sortable: true, width: 100, sorttype: "integer", formatter: "integer", editable: false, search: true, hidden: true }
        ],
        pager: '#Paginador',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        height: "50%",
        styleUI: 'Bootstrap',
        sortname: "ID",
        sortorder: "asc",
        caption: "Movimientos Caja",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: false,
        groupingView: {
            groupField: ['ID'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        },
        onSelectRow: function (rowId, status, e) {
            $("#ListaDetalleMov").jqGrid('clearGridData');
            $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
            $.ajax({
                url: "ConsultaCaja.aspx/ConsultaDetMovimientosCaja",
                data: "{'idCajaCuenta':" + $("#ListaMovCaja").jqGrid("getCell", rowId, "ID_CAJA_CUENTA") +
                ", 'idMovCaja':" + $("#ListaMovCaja").jqGrid("getCell", rowId, "ID_MOV_CAJA") +
                ", 'idOperacion':" + $("#ListaMovCaja").jqGrid("getCell", rowId, "ID_OPERACION") +
                ", 'decimales':" + $("#ListaMovCaja").jqGrid("getCell", rowId, "DECIMALES") + "}",
                datatype: "json",
                type: "post",
                contentType: "application/json; charset=utf-8",
                complete: function (jsondata, stat) {
                    if (stat === "success") {
                        var mydata = JSON.parse(jsondata.responseText).d;
                        $("#ListaDetalleMov").setGridParam({ data: mydata });
                        $("#ListaDetalleMov").trigger("reloadGrid");
                        if (!mydata.length) {
                            alertaColor(1, "No se encontraron registros.");
                        }
                    }
                    else {
                        alertaColor(3, JSON.parse(jsondata.responseText).Message);
                    }
                    $('#loaderProceso').modal('hide');
                }
            });
        }
    });
    $("#ListaMovCaja").jqGrid().bindKeys().scrollingRows = true;
    $("#ListaMovCaja").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaMovCaja").jqGrid('navGrid', '#Paginador', { add: false, edit: false, del: false, excel: false, search: false });
    $("#ListaMovCaja").jqGrid('navButtonAdd', '#Paginador', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaMovCaja', 'Movimientos Caja', 'Movimientos Caja', '');
        }
    });

    $("#ListaDetalleMov").jqGrid({
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID" },
        colNames: ['ID', 'Fecha Movimiento', 'Fecha Liquidación', 'Instrumento', 'Nemotécnico', 'Cantidad', 'Tasa/Precio', 'Caja', 'Monto Bruto'],
        colModel: [
            { name: "ID", index: "ID", sortable: false, width: 70, sorttype: "int", editable: false, search: true, hidden: true },
            { name: "Fecha_Movimiento", index: "Fecha_Movimiento", sortable: true, width: 130, sorttype: "date", formatter: "date", editable: false, search: true, hidden: false, align: "center", stype: "text" },
            { name: "Fecha_Liquidacion", index: "Fecha_Liquidacion", sortable: true, width: 130, sorttype: "date", formatter: "date", editable: false, search: true, hidden: false, align: "center" },
            { name: "DSC_INTRUMENTO", index: "DSC_INTRUMENTO", sortable: true, width: 200, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "Nemotecnico", index: "Nemotecnico", sortable: true, width: 300, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "Cantidad", index: "Cantidad", sortable: true, width: 130, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "Precio", index: "Precio", sortable: true, width: 130, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 4 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "Dsc_Caja_Cuenta", index: "Dsc_Caja_Cuenta", sortable: true, width: 90, sorttype: "text", editable: false, search: true, hidden: true },
            { name: "Monto_Bruto", index: "Monto_Bruto", sortable: true, width: 130, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } }
        ],
        pager: '#PaginadorDetalle',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        height: 'auto',
        styleUI: 'Bootstrap',
        sortname: "ID",
        sortorder: "asc",
        caption: "Detalle Movimientos Caja",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: false,
        groupingView: {
            groupField: ['ID'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        }
    });
    $("#ListaDetalleMov").jqGrid().bindKeys().scrollingRows = true;

    $("#BtnConsultar").button().click(function () {
        consultaMovimientosCaja();
        resize();
    });

    $("#porCuenta_NroCuenta").change(function () {
        cargaCajaCuentas();
        estadoBtnConsultar();
    });

    $('#DDCajaCta').on('change', () => {
        var dataRow = jQuery.map(arrayCajas, function (obj) {
            if (obj.idCaja == $('#DDCajaCta').val())
                return obj;
        });

        if (dataRow[0].idMoneda === 1) { //1:Peso
            $('#ListaMovCaja').setColProp('MONTO_ING', { formatoptions: { decimalPlaces: 0 } });
            $('#ListaMovCaja').setColProp('MONTO_EGR', { formatoptions: { decimalPlaces: 0 } });
            $('#ListaDetalleMov').setColProp('Monto_Bruto', { formatoptions: { decimalPlaces: 0 } });
        } else {
            $('#ListaMovCaja').setColProp('MONTO_ING', { formatoptions: { decimalPlaces: 2 } });
            $('#ListaMovCaja').setColProp('MONTO_EGR', { formatoptions: { decimalPlaces: 2 } });
            $('#ListaDetalleMov').setColProp('Monto_Bruto', { formatoptions: { decimalPlaces: 2 } });
        }
    });
}

function cargaCajaCuentas() {
    if (!miInformacionCuenta) {
        return;
    }
    $.ajax({
        url: "ConsultaCaja.aspx/ConsultaCajaCuenta",
        data: "{'strNumeroCuenta':'" + miInformacionCuenta.IdCuenta + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var cajaCuenta = JSON.parse(jsondata.responseText).d;
                $("#DDCajaCta").html('');
                arrayCajas = [];
                $.each(cajaCuenta, function (key, val) {
                    $("#DDCajaCta").append('<option value="' + val.ID_CAJA_CUENTA + '">' + capitalizeEachWord(val.DSC_CAJA_CUENTA) + '</option>');
                    arrayCajas.push({ idCaja: val.ID_CAJA_CUENTA, idMoneda: val.ID_MONEDA, dscCaja: val.DSC_CAJA_CUENTA });
                });
                $("#DDCajaCta").change();
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}
function consultaMovimientosCaja() {
    jsfechaFormato = moment.utc($("#dtFechaConsulta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaFormatoHasta = moment.utc($("#dtFechaConsultaHasta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1);
    jsfechaConsultaHasta = jsfechaFormatoHasta.substring(0, jsfechaFormatoHasta.length - 1);
    var jsCaja = $("#DDCajaCta").val();

    //var jsGasto = $("#CBGastos").is(':checked');

    if (!miInformacionCuenta) {
        return;
    }
    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }
    if (jsfechaConsultaHasta === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }

    $("#ListaMovCaja").jqGrid('clearGridData');
    $('#ListaDetalleMov').jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        url: "ConsultaCaja.aspx/ConsultaMovimientosCaja",
        data: "{'fechaConsulta':'" + jsfechaConsulta +
        "', 'fechaConsultaHasta':'" + jsfechaConsultaHasta +
        "', 'idCuenta':" + (!miInformacionCuenta ? "''" : miInformacionCuenta.IdCuenta) +
        ", 'Caja':" + (jsCaja === '-1' ? null : "'" + jsCaja + "'") +
        ", 'MontoIngresoD':" + null +
        ", 'MontoIngresoH':" + null +
        ", 'MontoEgresoD':" + null +
        ", 'MontoEgresoH':" + null + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#ListaMovCaja").setGridParam({ data: mydata });
                $("#ListaMovCaja").trigger("reloadGrid");
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alert(JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function estadoBtnConsultar() {
    if (!miInformacionCuenta) {
        $('#BtnConsultar').tooltip('enable').addClass('disabled');
    } else {
        $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
    }
}

$(document).ready(function () {
    initConsultaCaja();
    cargarEventHandlersConsultaCaja();
    $(window).trigger('resize');
});