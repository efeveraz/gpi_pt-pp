﻿function cargaEmpresas() {
    $.ajax({
        url: "../Servicios/Empresas.asmx/ConsultaNombreEmpresas",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var empresas = JSON.parse(jsondata.responseText).d;
                $("#DDEmpresa").html('');
                $.each(empresas, function (key, val) {
                    $("#DDEmpresa").append('<option value="' + val.ID_EMPRESA + '">' + val.DSC_EMPRESA + '</option>');
                })
                $("#DDEmpresa").prepend('<option value="' + -1 + '">' + 'Todas' + '</option>');
            }
            else {
                alert(JSON.parse(jsondata.responseText).Message);
            }
            $('.loading').hide();
        }
    });
}
function guardar() {
    var data = new FormData();
    var file = $("#FileUpload")[0].files[0];
    var idEmpresa = $("#DDEmpresa").val();
    var color = $("#LblColor").val()

    if (file != null) {
        data.append("file", file);
    }
    data.append("empresa", idEmpresa == null ? -1 : idEmpresa);
    data.append("color", color);

    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: "POST",
        url: "../Servicios/Empresas.asmx/Guardar",
        contentType: false,
        processData: false,
        data: data,
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mensaje = JSON.parse(jsondata.responseText);
                if (mensaje == "err") {
                    alertaColor(3, "Problema al Guardar");
                } else {
                    $("#BodyMaster").css('background-color', 'rgba(' & color & ', 1)');
                    $('#idSubtituloPagina').css('background-color', 'rgba(' & color & ', 0.7)');
                    cargarLogo();
                    alertaColor(0, "Cambio guardado.");
                }
            } else {
                alertaColor(3, JSON.parse(jsondata.statusText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function cargarEventHandlersMantenedorEmpresa() {
    $(document).on('change', '.btn-file :file', function () {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });
    $("#FileUpload").on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#logoPreview').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
            $('#logoPreview').show();
        } else {
            $('#logoPreview').hide();
            $('#logoPreview').attr('src', "")
        }
    });
    $("#BtnGuardar").on('click', function () {
        guardar();
    });
    $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });
}
function initMantenedorEmpresa() {
    $('#logoPreview[src="#"]').hide();
    $('#muestra').css('background-color', 'rgb(' + $('#LblColor').val() + ')');

    $("#idSubtituloPaginaText").text("Empresa");
    document.title = "Empresa";

    $('.picker').colpick({
        color: $('#LblColor').text(),
        onSubmit: function (hsb, hex, rgb, el) {
            $('body').css('background-color', 'rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', 1)');
            $('#idSubtituloPagina').css('background-color', 'rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', 1)');
            $('#muestra').css('background-color', 'rgb(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')');
            $('#LblColor').val(("0" + rgb.r).slice(-3) + ', ' + ("0" + rgb.g).slice(-3) + ', ' + ("0" + rgb.b).slice(-3));
            $(el).colpickHide();
        }
    });
}

$(document).ready(function () {
    initMantenedorEmpresa();
    cargarEventHandlersMantenedorEmpresa();
    cargaEmpresas();
});