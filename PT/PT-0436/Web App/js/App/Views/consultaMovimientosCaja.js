﻿var jsfechaConsulta;
var jsfechaConsultaHasta;
var jsfechaFormato;
var jsfechaFormatoHasta;
var arrayCajas = [];

function resize() {
    var ancho = $(window).width();
    var height;
    height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#Paginador").outerHeight(true) - 92;
    $("#MovimientosCaja").setGridHeight(height);

    if (ancho < 768) {
        $("#DDCajaCta").removeAttr("style");
    } else {
        $("#DDCajaCta").width(130)
    }
}

function initConsultaMovimientosCaja() {

    $("#idSubtituloPaginaText").text("Consulta Movimientos Caja");
    document.title = "Consulta Movimientos Caja";

    $("#tabsClienteCuentaGrupo").hide();
    $('.nav-tabs a[href="#tabCuenta"]').tab('show');

    if (!miInformacionCuenta) {
        $("#dtFechaConsultaHasta").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
        $("#dtFechaConsulta").datepicker('setDate', moment(new Date(localStorage.UltimaFechaCierre)).add(-1, 'months').toDate());
    } else {
        $("#dtFechaConsultaHasta").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
        $("#dtFechaConsulta").datepicker('setDate', moment($("#dtFechaConsultaHasta").datepicker('getDate')).add(-1, 'months').format('L'));
    }

    if (miInformacionCuenta != null) {
        cargaCajaCuentas();
    } else {
        $("#DDCajaCta").prepend('<option>-</option>');
        $('#BtnConsultar').tooltip('enable').addClass('disabled');
    }
}

function cargarEventHandlersConsultaMovCaja() {

    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $("#dtRangoFecha").datepicker();

    $("#MovimientosCaja").jqGrid({
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID" },
        colNames: ['ID', 'Fecha Movimiento', 'Fecha Liquidación', 'Origen Movimiento', 'Movimientos', 'Ingresos', 'Egresos', 'Saldo', 'DSC_CAJA_CUENTA'],
        colModel: [
            { name: "ID", index: "ID", width: 110, sortable: false, sorttype: "int", formatter: "int", editable: false, hidden: true, align: "center" },
            { name: "FECHA_MOVIMIENTO", index: "FECHA_MOVIMIENTO", width: 140, sortable: false, sorttype: "date", formatter: "date", editable: false, hidden: false, align: "center" },
            { name: "FECHA_LIQUIDACION", index: "FECHA_LIQUIDACION", width: 140, sortable: false, sorttype: "date", formatter: "date", editable: false, hidden: false, align: "center" },
            { name: "DSC_ORIGEN_MOV_CAJA", index: "DSC_ORIGEN_MOV_CAJA", width: 220, sortable: false, sorttype: "text", editable: false, hidden: false },
            { name: "DSC_MOV_CAJA", index: "DSC_MOV_CAJA", width: 330, sortable: false, sorttype: "text", editable: false, hidden: false, summaryTpl: '<b>Saldo Caja</b>', summaryType: 'sum' },
            { name: "MONTOINGRESO", index: "MONTOINGRESO", width: 120, sortable: false, sorttype: "number", formatter: "number", editable: false, hidden: false, align: "right", summaryType: 'sum' },
            { name: "MONTOEGRESO", index: "MONTOEGRESO", width: 120, sortable: false, sorttype: "number", formatter: "number", editable: false, hidden: false, align: "right", summaryType: 'sum' },
            { name: "SALDO", index: "SALDO", width: 120, sortable: false, sorttype: "number", formatter: "number", editable: false, hidden: false, align: "right" },
            { name: "DSC_CAJA_CUENTA", index: "DSC_CAJA_CUENTA", sortable: false, width: 30, sorttype: "text", editable: false }
        ],
        pager: '#Paginador',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        sortname: "ID",
        responsive: true,
        styleUI: 'Bootstrap',
        caption: "Detalle Movimientos",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: true,
        rowNum: 9999,
        rowList: [],
        pgbuttons: false,
        pgtext: null,
        viewrecords: false,
        groupingView: {
            groupField: ['DSC_CAJA_CUENTA'],
            groupText: ['<b>{0}</b>'],
            groupColumnShow: [false]
        }
    });
    $("#MovimientosCaja").jqGrid().bindKeys().scrollingRows = true
    $("#MovimientosCaja").jqGrid('navGrid', '#Paginador', { add: false, edit: false, del: false, excel: false, search: false });
    $("#MovimientosCaja").jqGrid('navButtonAdd', '#Paginador', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('MovimientosCaja', 'MovimientosCaja', 'MovimientosCaja', '');
        }
    });

    $("#BtnConsultar").button().click(function () {
        if (miInformacionCuenta == null) {
            return;
        }
        $('#MovimientosCaja').clearGridData(true);
        consultaOperaciones();
    });

    $("#porCuenta_NroCuenta").change(function () {
        cargaCajaCuentas();
    });

    $('#DDCajaCta').on('change', () => {
        var dataRow = jQuery.map(arrayCajas, function (obj) {
            if (obj.idCaja == $('#DDCajaCta').val())
                return obj;
        });

        console.log(dataRow[0].idMoneda)
        if (dataRow[0].idMoneda === 1) { //1:Peso
            $('#MovimientosCaja').setColProp('MONTOINGRESO', { formatoptions: { decimalPlaces: 0 } });
            $('#MovimientosCaja').setColProp('MONTOEGRESO', { formatoptions: { decimalPlaces: 0 } });
            $('#MovimientosCaja').setColProp('SALDO', { formatoptions: { decimalPlaces: 0 } });
        } else {
            $('#MovimientosCaja').setColProp('MONTOINGRESO', { formatoptions: { decimalPlaces: 2 } });
            $('#MovimientosCaja').setColProp('MONTOEGRESO', { formatoptions: { decimalPlaces: 2 } });
            $('#MovimientosCaja').setColProp('SALDO', { formatoptions: { decimalPlaces: 2 } });
        }
    });
}

function cargaCajaCuentas() {
    if (miInformacionCuenta == null) {
        $('#BtnConsultar').tooltip('enable').addClass('disabled');
        return;
    } else {
        $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
    }
    $.ajax({
        url: "ConsultaCaja.aspx/ConsultaCajaCuenta",
        data: "{'strNumeroCuenta':'" + miInformacionCuenta.IdCuenta + "'}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var cajaCuenta = JSON.parse(jsondata.responseText).d;
                $("#DDCajaCta").html('');
                arrayCajas = [];
                $.each(cajaCuenta, function (key, val) {
                    $("#DDCajaCta").append('<option value="' + val.ID_CAJA_CUENTA + '">' + capitalizeEachWord(val.DSC_CAJA_CUENTA) + '</option>');
                    arrayCajas.push({ idCaja: val.ID_CAJA_CUENTA, idMoneda: val.ID_MONEDA, dscCaja: val.DSC_CAJA_CUENTA });
                })
                $("#DDCajaCta").change();
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
};

function consultaOperaciones() {
    jsfechaFormato = moment.utc($("#dtFechaConsulta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaFormatoHasta = moment.utc($("#dtFechaConsultaHasta").datepicker('getDate')).startOf('day').toISOString();

    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1)
    jsfechaConsultaHasta = jsfechaFormatoHasta.substring(0, jsfechaFormatoHasta.length - 1)

    var jsIdCaja = $("#DDCajaCta").val();

    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }

    $("#MovimientosCaja").jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        url: "ConsultaMovimientosCaja.aspx/ConsultaMovimientosCaja",
        data: "{'fechaConsulta':'" + jsfechaConsulta +
        "', 'fechaConsultaHasta':'" + jsfechaConsultaHasta +
        "', 'idCuenta':" + miInformacionCuenta.IdCuenta +
        ", 'idCaja':" + (jsIdCaja == -1 ? "''" : jsIdCaja) + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#MovimientosCaja").setGridParam({ data: mydata });
                $("#MovimientosCaja").trigger("reloadGrid");
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
};

$(document).ready(function () {
    initConsultaMovimientosCaja();
    cargarEventHandlersConsultaMovCaja();
    resize();
});