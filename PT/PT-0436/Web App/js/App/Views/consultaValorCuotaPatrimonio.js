﻿var jsfechaConsulta;
var jsfechaConsultaHasta;
var jsfechaFormato;
var jsfechaFormatoHasta;

function resize() {
    var ancho = $(window).width();
    var height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#Paginador").outerHeight(true) - 146;

    if (ancho < 768) {
        $("#ListaOperaciones").setGridHeight(225);
    } else {
        $("#ListaOperaciones").setGridHeight(height);
    }
}

function initConsultaValorCuotaPatrimonio() {
    $("#idSubtituloPaginaText").text("Consulta de Valor Cuota Patrimonio");
    document.title = "Consulta de Valor Cuota Patrimonio";

    estadoBtnConsultar();

    if (!miInformacionCuenta) {
        $("#dtFechaConsultaHasta").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
        $("#dtFechaConsulta").datepicker('setDate', moment(new Date(localStorage.UltimaFechaCierre)).add(-1, 'months').toDate());
    } else {
        $("#dtFechaConsultaHasta").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
        $("#dtFechaConsulta").datepicker('setDate', moment($("#dtFechaConsultaHasta").datepicker('getDate')).add(-1, 'months').format('L'));
    }

    $("#tabsClienteCuentaGrupo").hide();
    $('.nav-tabs a[href="#tabCuenta"]').tab('show');
}

function eventHandlersConsultaValorCuotaPatrimonio() {
    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $("#dtRangoFecha").datepicker();

    $("#ListaOperaciones").jqGrid({
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID_OPERACION" },
        colNames: ['Fecha', 'Valor Cuota', 'Total Cuotas', 'Patrimonio', 'Movimientos Aporte/Rescate'],
        colModel: [
            { name: "FECHA_CIERRE", index: "FECHA_OPERACION", sortable: true, width: 200, sorttype: "date", formatter: "date", editable: false, search: true, hidden: false, align: "center", searchoptions: { sopt: ['eq'], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "VALOR_CUOTA_MON_CUENTA", index: "VALOR_CUOTA_MON_CUENTA", sortable: true, width: 200, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "TOTAL_CUOTAS_MON_CUENTA", index: "TOTAL_CUOTAS_MON_CUENTA", sortable: true, width: 200, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "PATRIMONIO_MON_CUENTA", index: "PATRIMONIO_MON_CUENTA", sortable: true, width: 200, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "APORTE_RETIROS_MON_CUENTA", index: "APORTE_RETIROS_MON_CUENTA", sortable: true, width: 200, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } }
        ],
        pager: '#Paginador',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        styleUI: 'Bootstrap',
        sortname: "FECHA_CIERRE",
        sortorder: "asc",
        caption: "Valor Cuota Patrimonio",
        ignoreCase: true,
        hidegrid: false,
        grouping: false,
        regional: localeCorto,
        groupingView: {
            groupField: ['FECHA_CIERRE'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        }
    });
    $("#ListaOperaciones").jqGrid().bindKeys().scrollingRows = true
    $("#ListaOperaciones").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#ListaOperaciones").jqGrid('navGrid', '#Paginador', { add: false, edit: false, del: false, excel: false, search: false });
    $("#ListaOperaciones").jqGrid('navButtonAdd', '#Paginador', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('ListaOperaciones', 'Valor Cuota Patrimonio', 'Valor Cuota Patrimonio', '');
        }
    });

    $("#BtnConsultar").button().click(function () {
        consultaValorCuotaPatrimonio();
        resize();
    });

    $("#porCuenta_NroCuenta").on('change', function () {
        estadoBtnConsultar();
    });
}

function consultaValorCuotaPatrimonio() {
    jsfechaFormato = moment.utc($("#dtFechaConsulta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaFormatoHasta = moment.utc($("#dtFechaConsultaHasta").datepicker('getDate')).startOf('day').toISOString();

    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1)
    jsfechaConsultaHasta = jsfechaFormatoHasta.substring(0, jsfechaFormatoHasta.length - 1)

    if (miInformacionCuenta == null) {
        return;
    }
    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }
    if (jsfechaConsultaHasta === "") {
        alert("No ha indicado fecha de consulta hasta");
        return;
    }

    $("#ListaOperaciones").jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        url: "ConsultaValorCuotaPatrimonio.aspx/ConsultaValorCuotaPatrimonio",
        data: "{'fechaConsulta':'" + jsfechaConsulta +
        "', 'fechaConsultaHasta':'" + jsfechaConsultaHasta +
        "', 'idCuenta':" + (miInformacionCuenta == null ? "''" : miInformacionCuenta.IdCuenta) + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#ListaOperaciones").setGridParam({ data: mydata });
                $("#ListaOperaciones").trigger("reloadGrid");
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}
function estadoBtnConsultar() {
    if (miInformacionCuenta == null) {
        $('#BtnConsultar').tooltip('enable').addClass('disabled');
    } else {
        $('#BtnConsultar').tooltip('disable').tooltip('hide').removeClass('disabled');
    }
}

$(document).ready(function () {
    initConsultaValorCuotaPatrimonio();
    eventHandlersConsultaValorCuotaPatrimonio();
    $(window).trigger('resize');
});