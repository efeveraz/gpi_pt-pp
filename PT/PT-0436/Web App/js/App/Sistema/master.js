﻿/*VARIABLES GLOBALES CONTROLES EN CLIENTE*/
var strFmtFecha = new String('DD-MM-YYYY');
var strSeparadorFecha = new String('-');
var intGrillaFilasPagina = 50;
var arrGrillaFilasHoja = [50, 100, 150];

var miInformacionCuenta = localStorage.CuentaSeleccionada === undefined ? (null) : (JSON.parse(localStorage.CuentaSeleccionada));
var miInformacionCliente = localStorage.ClienteSeleccionado === undefined ? (null) : (JSON.parse(localStorage.ClienteSeleccionado));
var miInformacionGrupo = localStorage.GrupoSeleccionado === undefined ? (null) : (JSON.parse(localStorage.GrupoSeleccionado));

var locale;
var localeCorto;
var colorEmp;
var _alert = null;

function GetUrlValue(VarSearch) {
    var SearchString = window.location.search.substring(1);
    var VariableArray = SearchString.split('&');
    for (var i = 0; i < VariableArray.length; i++) {
        var KeyValuePair = VariableArray[i].split('=');
        if (KeyValuePair[0] == VarSearch) {
            return KeyValuePair[1];
        }
    }
}

function getUrlVars() {
    var url = window.location.href, vars = {};
    url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        key = decodeURIComponent(key);
        value = decodeURIComponent(value);
        vars[key] = value;
    });
    return vars;
}

function IsFullScreenCurrently() {
    return (document.fullscreen || document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement);
}

function GoOutFullscreen() {
    if (document.exitFullscreen)
        document.exitFullscreen();
    else if (document.mozCancelFullScreen)
        document.mozCancelFullScreen();
    else if (document.webkitExitFullscreen)
        document.webkitExitFullscreen();
    else if (document.msExitFullscreen)
        document.msExitFullscreen();
}

function GoInFullscreen(element) {
    if (element.requestFullscreen)
        element.requestFullscreen();
    else if (element.mozRequestFullScreen)
        element.mozRequestFullScreen();
    else if (element.webkitRequestFullscreen)
        element.webkitRequestFullscreen();
    else if (element.msRequestFullscreen)
        element.msRequestFullscreen();
}

function trim(myString) {
    return myString.replace(/^\s+/g, '').replace(/\s+$/g, '');
}

function paginaInicio() {
    window.parent.location = "../Sistema/index.aspx";
}

function consume_alert() {
    if (_alert)
        return;
    _alert = window.alert;
    window.alert = function (message) {
        if (message.lenght == 0) {
            return;
        }
        bootbox.addLocale('es_cl', {
            OK: 'Aceptar',
            CANCEL: 'Cancelar',
            CONFIRM: 'Confirmar'
        });
        bootbox.setLocale("es_cl");
        bootbox.alert(message);
    };
}

function isNumberKeyAndSign(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 45 && charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function capitalizeEachWord(str) {
    if (!str) {
        return str;
    }
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function datePick(elem) {
    jQuery(elem).datepicker();
}

function alertaColor(tipo, mensaje) {
    var cl;
    var text;
    var icon;
    $('#alert').removeClass("alert-success");
    $('#alert').removeClass("alert-info");
    $('#alert').removeClass("alert-warning");
    $('#alert').removeClass("alert-danger");

    switch (tipo) {
        case 0:
            cl = "alert-success";
            text = "Éxito! ";
            icon = "fa fa-check-circle";
            break;
        case 1:
            cl = "alert-info";
            text = "Atención! ";
            icon = "fa fa-info-circle";
            break;
        case 2:
            cl = "alert-warning";
            text = "Alerta! ";
            icon = "fa fa-exclamation-circle";
            break;
        case 3:
            cl = "alert-danger";
            text = "Error! ";
            icon = "fa fa-exclamation-triangle";
            break;
        default:
            cl = "alert-danger";
            text = "Error! ";
            icon = "fa fa-exclamation-triangle";
    }
    $('#alert').addClass(cl);
    $('#tipoAlerta').text(text);
    $('#mensaje').text(mensaje);
    $('#iconAlerta').removeClass();
    $('#iconAlerta').addClass(icon);
    $('#alert').fadeIn('slow');
    $('#alert').delay(3000).fadeOut('slow');
}
function cargarLogo() {
    $('#idLogoEmpresa').attr('src', "data:image/png;base64," + localStorage.LogoEmpresa);
}
function cargaMenu() {
    $('#navbar').append(JSON.parse(localStorage.Menu).d);
    $("li.active").parent().siblings().parent("li").addClass("active").parent().siblings().parent("li").addClass("active");

    $('#navbar .menuPadre').on('click', function () {
        $('#navbar > li > ul').not(this.hash).collapse('hide');
        $('#navbar > li > ul > li > ul').not(this.hash).collapse('hide');
    });
    $('#navbar > li > ul').on('click', 'a', function () {
        $('#navbar > li > ul > li > ul').not(this.hash).collapse('hide');
    });
    $('li', '#navbar')
        .filter(function () {
            return !!$(this).find('a[href="..' + location.pathname + '"]').length;
        }).addClass("active");
}
function cargaPrecio() {
    var fechaCierre = new Date();
    var fechaCambio = new Date();
    var cadenaHtml;

    $.ajax({
        url: "../Servicios/Menu.asmx/consultarValores",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var resValores = JSON.parse(jsondata.responseText).d;

                fechaCambio = moment.utc(resValores.Fecha).startOf('day').format('L');
                $('#fechaValores').html(fechaCambio);

                $('#codUSD').html('USD');
                $('#dscUSD').html(resValores.objUSD[resValores.objUSD.length - 1].DSC_TIPO_CAMBIO);
                $('#valorUSD').html(numeral(resValores.objUSD[resValores.objUSD.length - 1].VALOR).format('0,000.00'));

                $('#codUF').html('UF');
                $('#dscUF').html(resValores.objUF[resValores.objUF.length - 1].DSC_TIPO_CAMBIO);
                $('#valorUF').html(numeral(resValores.objUF[resValores.objUF.length - 1].VALOR).format('0,000.00'));

                $('#codEUR').html('EUR');
                $('#dscEUR').html(resValores.objEUR[resValores.objEUR.length - 1].DSC_TIPO_CAMBIO);
                $('#valorEUR').html(numeral(resValores.objEUR[resValores.objEUR.length - 1].VALOR).format('0,000.00'));

                $('#codIVP').html('IVP');
                $('#dscIVP').html(resValores.objIVP[resValores.objIVP.length - 1].DSC_TIPO_CAMBIO);
                $('#valorIVP').html(numeral(resValores.objIVP[resValores.objIVP.length - 1].VALOR).format('0,000.00'));

                var i;
                for (i = 0; i < resValores.objUSD.length; i++) {
                    resValores.objUSD[i].y = resValores.objUSD[i]['VALOR'];
                    resValores.objUSD[i].x = Number(resValores.objUSD[i]['FECHA'].substring(6, resValores.objUSD[i]['FECHA'].length - 2));
                    delete resValores.objUSD[i].VALOR;
                    delete resValores.objUSD[i].FECHA;
                }
                for (i = 0; i < resValores.objUF.length; i++) {
                    resValores.objUF[i].y = resValores.objUF[i]['VALOR'];
                    resValores.objUF[i].x = Number(resValores.objUF[i]['FECHA'].substring(6, resValores.objUF[i]['FECHA'].length - 2));
                    delete resValores.objUF[i].VALOR;
                    delete resValores.objUF[i].FECHA;
                }
                for (i = 0; i < resValores.objEUR.length; i++) {
                    resValores.objEUR[i].y = resValores.objEUR[i]['VALOR'];
                    resValores.objEUR[i].x = Number(resValores.objEUR[i]['FECHA'].substring(6, resValores.objEUR[i]['FECHA'].length - 2));
                    delete resValores.objEUR[i].VALOR;
                    delete resValores.objEUR[i].FECHA;
                }
                for (i = 0; i < resValores.objIVP.length; i++) {
                    resValores.objIVP[i].y = resValores.objIVP[i]['VALOR'];
                    resValores.objIVP[i].x = Number(resValores.objIVP[i]['FECHA'].substring(6, resValores.objIVP[i]['FECHA'].length - 2));
                    delete resValores.objIVP[i].VALOR;
                    delete resValores.objIVP[i].FECHA;
                }

                grafico(resValores.objUSD, '#chartUSD');
                grafico(resValores.objUF, '#chartUF');
                grafico(resValores.objEUR, '#chartEUR');
                grafico(resValores.objIVP, '#chartIVP');
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('.loading').hide();
        }
    });
}
function grafico(data, idGrafico) {
    var graphDolar = new Rickshaw.Graph({
        element: document.querySelector(idGrafico),
        //width: 242, //259
        height: 100,
        series: [{
            color: 'steelblue',
            data: data
        }],
        min: 'auto'
    });

    graphDolar.render();
}
function isNavigationStatic() {
    if ($('#formulario').is('.nav-static')) {
        return true;
    } else {
        return false;
    }
}
function collapseNavigation() {
    if (isNavigationStatic() && ($('body').width() > 991)) {
        return;
    }
    $('#formulario').addClass('nav-collapsed');
    $('#navbar > li > a').addClass('collapsed');
    $('#navbar > li > ul').collapse('hide');
    $('#idLogoEmpresa').fadeOut(300);
}
function expandNavigation() {
    if (isNavigationStatic() && ($('body').width() > 991)) {
        return;
    }
    $('#formulario').removeClass('nav-collapsed');
    $('#idLogoEmpresa').fadeIn(300);
}
function toggleNavigationState() {
    if (isNavigationStatic()) {
        $('#formulario').removeClass('nav-static');
        $('#idLogoEmpresa').fadeOut(300);
        collapseNavigation();
    } else {
        $('#formulario').addClass('nav-static');
        $('#idLogoEmpresa').fadeIn(300);
    }
    $(window).trigger('resize');
}

function cargarEventHandlersMaster() {
    $(window).bind('resize', function () {
        $('#sidebar').height($(window).height() - 18);

        var widthSec = $('#idSecUF').width() + 35;
        $('#chartUF svg').width(widthSec);
        $('#chartUSD svg').width(widthSec);
        $('#chartEUR svg').width(widthSec);
        $('#chartIVP svg').width(widthSec);

        $('#chartUF').width(widthSec);
        $('#chartUSD').width(widthSec);
        $('#chartEUR').width(widthSec);
        $('#chartIVP').width(widthSec);
    }).trigger('resize');

    $("#alert [data-hide]").on("click", function () {
        $(this).closest("." + $(this).attr("data-hide")).hide();
    });

    $("#nav-state-toggle").on("click", function () {
        toggleNavigationState();
    });

    $("#navbar").on("click", function () {
        expandNavigation();
    });

    $("#sidebar").on("mouseenter", function () {
        if ($('body').width() > 991) {
            expandNavigation();
        }
    });
    $("#sidebar").on("mouseleave", function () {
        if ($('body').width() > 991) {
            collapseNavigation();
        }
    });

    $(document).on('click', '[data-toggle=chat-sidebar]', function () {
        $('#formulario').toggleClass('chat-sidebar-opened');
    });
    $(document).on('click', '[data-toggle=window-full]', function () {
        if (IsFullScreenCurrently()) {
            GoOutFullscreen();
            $('#window-max').attr('data-original-title', 'Maximizar');
            $('#iconWindowMax').toggleClass("fa-window-maximize");
            $('#iconWindowMax').toggleClass("fa-window-restore");
        } else {
            GoInFullscreen(document.getElementById('BodyMaster'));
            $('#window-max').attr('data-original-title', 'Minimizar');
            $('#iconWindowMax').toggleClass("fa-window-maximize");
            $('#iconWindowMax').toggleClass("fa-window-restore");
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

    $("#nav-collapse-toggle").on("click", function () {
        if ($('#formulario').is('.nav-collapsed')) {
            expandNavigation();
        } else {
            collapseNavigation();
        }
        $(window).trigger('resize');
    });
}

function initMaster(callback) {
    $('body').append(localStorage.CssDinamico);
    consume_alert();

    var a = {
        delimiters: {
            thousands: ".",
            decimal: ","
        },
        abbreviations: {
            thousand: "k",
            million: "mm",
            billion: "b",
            trillion: "t"
        },
        ordinal: function (a) {
            var b = a % 10;
            return 1 === b || 3 === b ? "er" : 2 === b ? "do" : 7 === b || 0 === b ? "mo" : 8 === b ? "vo" : 9 === b ? "no" : "to";
        },
        currency: {
            symbol: "$"
        }
    };
    numeral.language('cl', a);
    numeral.language('cl');

    locale = window.navigator.userLanguage || window.navigator.language;
    moment.locale(locale);
    localeCorto = locale.split('-', 1);

    $.fn.datepicker.defaults.language = locale;
    $.fn.datepicker.defaults.autoclose = 'true';

    cargarUserData();
    callback();
}
function cargarDatosMaster() {
    cargarLogo();
    cargaMenu();
    cargaPrecio();
}
function CerrarSesion() {
    window.location.href = "../Login/Login.aspx";
}
function cargarUserData() {
    $('#userFirstName').html(capitalizeEachWord(localStorage.UserName));
    $('#userFullName').html(capitalizeEachWord(localStorage.UserName));
    $('#userNomEmpresa').html(capitalizeEachWord(localStorage.DscEmpresa));
}

$(document).ready(function () {
    initMaster(function () {
        cargarDatosMaster();
        cargarEventHandlersMaster();
    });
});