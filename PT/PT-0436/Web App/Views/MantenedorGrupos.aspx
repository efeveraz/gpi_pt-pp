﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Sistema/GPIWeb.Master"
    CodeBehind="MantenedorGrupos.aspx.vb" Inherits="AplicacionWeb.MantenedorGrupos" %>

<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContenidoPPalFiltro" runat="server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/mantenedorGrupos.min.js") %>'></script>
    <link href='<%= VersionLinkHelper.GetVersion("../js/vendor/DataTables/datatables.min.css") %>' rel="stylesheet" />
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/DataTables/datatables.min.js") %>'></script>

    <div class="row">
        <div id="panelGrupos" class="col-xs-12">
            <div class="navbar navbar-default">
                <div class="col-xs-12">
                    <ul class='nav nav-pills'>
                        <li>
                            <button type="button" id="btnHabilitaAgregar" class="btn btn-default navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Nuevo'><i class='fa fa-plus'></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnHabilitaEditar" class='btn btn-default navbar-btn fade hidden' data-toggle='tooltip' data-placement='bottom' data-original-title='Editar'><i class='fa fa-pencil'></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnEliminar" class="btn btn-danger navbar-btn fade hidden" data-toggle='tooltip' data-placement='bottom' data-original-title='Eliminar'><i class='fa fa-trash'></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnPermisosCuenta" class='btn btn-default navbar-btn fade hidden' data-toggle='tooltip' data-placement='bottom' data-original-title='Permisos Cuenta'>Cuentas</button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default mb-0">
                <div class="panel-body">
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='table-responsive'>
                                <table class='table table-striped table-hover table-bordered table-condensed' id='tablaGrupos'>
                                    <thead style="display: table-header-group">
                                        <tr>
                                            <th></th>
                                            <th>Grupo</th>
                                            <th>Asesor</th>
                                            <th>Dirección</th>
                                            <th>Email</th>
                                            <th>Teléfono</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="panelAgregar" class="col-sm-12 hidden">
            <div class="navbar navbar-default">
                <div class="col-xs-12">
                    <ul class='nav nav-pills'>
                        <li>
                            <button type="button" id="btnGuardarGrupo" class="btn btn-primary navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Guardar'><i class="fa fa-floppy-o"></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnCancelar" class="btn btn-danger navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Cancelar'><i class="fa fa-times"></i></button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default">
                <div id="tituloAgregar" class="panel-heading">Agregar Nuevo Grupo de Cuentas</div>
                <div id="tituloEditar" class="panel-heading">Editar Grupo de Cuentas</div>
                <div id="sPanelAgregar" class="panel-body">
                    <div class="row">
                        <div class="form-horizontal">
                            <div id="divTxtAgregarGrupo" class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <label class="control-label" for="txtAgregarGrupo">Grupo</label>
                                <input id="txtAgregarGrupo" type="text" maxlength="60" class="form-control input-sm" />
                                <span id="lblErrNomGrupo" class="label label-danger hidden"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-horizontal">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <label class="control-label" for="sAgregarAsesor">Asesor</label>
                                <select id="DDAsesorAgregar" class="form-control input-sm">
                                </select>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <label class="control-label">Dirección</label>
                                <input id="txtAgregarDireccion" maxlength="100" type="text" class="form-control input-sm" />
                            </div>
                            <div id="divTxtAgregarEmail" class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <label class="control-label">Email</label>
                                <input id="txtAgregarEmail" maxlength="50" type="email" class="form-control input-sm" />
                                <span id="lblErrEmail" class="label label-danger hidden"></span>
                            </div>
                            <div id="divTxtAgregarTelefono" class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                <label class="control-label">Teléfono</label>
                                <input id="txtAgregarTelefono" type="tel" oninput="if(value.length>15)value=value.slice(0,15)" class="form-control input-sm" />
                                <span id="lblErrTelefono" class="label label-danger hidden"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="panelCuentas" class="col-sm-12 hidden">
            <div class="navbar navbar-default">
                <div class="col-xs-12">
                    <ul class='nav nav-pills'>
                        <li>
                            <button type="button" id="btnGuardarAsociar" class="btn btn-primary navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Guardar'><i class="fa fa-floppy-o"></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnCancelarAsociar" class="btn btn-danger navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Cancelar'><i class="fa fa-times"></i></button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default mb-0">
                <div class="panel-heading">
                    Permisos cuenta Grupo:
                    <label id="lbGrupoCuenta"></label>
                </div>
                <div id="idPnlCtas" class="panel-body" style="overflow-y: auto;">
                    <div id="panelAsociar" class="col-xs-12 col-md-5 col-sm-5">
                        <div class="panel panel-default mb-0">
                            <div class="panel-heading">Cuentas Asociadas</div>
                            <div id="lisPnlAsociar" class="panel-heading">
                                <div class="input-group">
                                    <input id="txtBuscarAsociados" type="text" class="form-control" placeholder="Buscar...">
                                    <span class="input-group-btn">
                                        <button id="btnLimpiarAso" class="btn btn-default" type="button">
                                            <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
                                        </button>
                                    </span>
                                </div>
                                <div id="lisGrupoAsociar" style="overflow: auto;">
                                    <ul id="ulLisCtaAsociada" class="list-group checked-list-box">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="panelBtnAsociarV" class="hidden-xs col-md-2 col-sm-2 text-center">
                        <div class="panel-body">
                            <div class="form-group-sm">
                                <button type="button" id="btnAsociarV" class="btn btn-default">
                                    <span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>
                                </button>
                            </div>
                            <div class="form-group-sm">
                                <button type="button" id="btnDesAsociarV" class="btn btn-default">
                                    <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
                                </button>
                            </div>
                            <div class="form-group-sm">
                                <button type="button" id="btnAsociarTodosV" class="btn btn-default">
                                    <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
                                </button>
                            </div>
                            <div class="form-group-sm">
                                <button type="button" id="btnDesAsociarTodosV" class="btn btn-default">
                                    <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="panelBtnAsociarH" class="col-xs-12 hidden-lg hidden-md hidden-sm text-center mb-0">
                        <div class="panel-body">
                            <div class="form-group-sm">
                                <button type="button" id="btnAsociarH" class="btn btn-default">
                                    <span class="glyphicon glyphicon-triangle-left fa-rotate-90" aria-hidden="true"></span>
                                </button>
                                <button type="button" id="btnDesAsociarH" class="btn btn-default">
                                    <span class="glyphicon glyphicon-triangle-right fa-rotate-90" aria-hidden="true"></span>
                                </button>
                                <button type="button" id="btnAsociarTodosH" class="btn btn-default">
                                    <span class="glyphicon glyphicon-backward fa-rotate-90" aria-hidden="true"></span>
                                </button>
                                <button type="button" id="btnDesAsociarTodosH" class="btn btn-default">
                                    <span class="glyphicon glyphicon-forward fa-rotate-90" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="panelDisponibles" class="col-xs-12 col-md-5 col-sm-5">
                        <div class="panel panel-default mb-0">
                            <div class="panel-heading">Cuentas Disponibles</div>
                            <div id="lisPnlDisponible" class="panel-heading">
                                <div class="input-group">
                                    <input id="txtBuscarDisponible" type="text" class="form-control" placeholder="Buscar...">
                                    <span class="input-group-btn">
                                        <button id="btnLimpiarDes" class="btn btn-default" type="button">
                                            <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
                                        </button>
                                    </span>
                                </div>
                                <div id="lisGrupoDisponible" style="overflow: auto;">
                                    <ul id="ulLisCtaDisponible" class="list-group checked-list-box">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
