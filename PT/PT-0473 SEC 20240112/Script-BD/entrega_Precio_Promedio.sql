IF  EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[ENTREGA_PRECIO_PROMEDIO]')  AND TYPE IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [DBO].[ENTREGA_PRECIO_PROMEDIO]
GO

CREATE FUNCTION ENTREGA_PRECIO_PROMEDIO
( @FECHA DATETIME
, @IDCUENTA NUMERIC(10)
, @IDNEMO NUMERIC(10))
 RETURNS NUMERIC(18,04)
AS
BEGIN
     DECLARE @TIPO_MOVIMIENTO CHAR(01),
             @CANTIDAD        NUMERIC(18,04),
             @SALDO           NUMERIC(18,04),
             @SALDO_PESOS     NUMERIC(18,04),
             @MONTO           NUMERIC(18,04),
             @PRECIO          NUMERIC(18,04),
             @PRECIO_PROMEDIO NUMERIC(18,04),
             @PRIMER_REGISTRO INT,
             @FECHA_OPERACION DATETIME,
             @ID_MONEDA       NUMERIC(2),
             @ID_MONEDANEMO   NUMERIC(2),
             @ID_MONEDA_OPE   NUMERIC(2),
             @VALORCAMBIO     NUMERIC(18,4)

    SELECT @ID_MONEDA = ID_MONEDA FROM CUENTAS WHERE ID_CUENTA = @IDCUENTA
    SELECT @ID_MONEDANEMO = ID_MONEDA FROM NEMOTECNICOS WHERE ID_NEMOTECNICO = @IDNEMO

    DECLARE CURSOR_MOV_ACTIVOS CURSOR FAST_FORWARD FOR
          SELECT FLG_TIPO_MOVIMIENTO,
                 CANTIDAD,
                 PRECIO,
                 MONTO,
                 FECHA_OPERACION,
                 ID_MONEDA
            FROM MOV_ACTIVOS M WITH (NOLOCK)
           WHERE M.ID_NEMOTECNICO = @IDNEMO
             AND M.ID_CUENTA = @IDCUENTA
             AND M.FECHA_OPERACION <= @FECHA
             AND M.COD_ESTADO <> DBO.PKG_GLOBAL$CCOD_ESTADO_ANULADO()
           ORDER BY  M.FECHA_OPERACION, M.ID_OPERACION_DETALLE, M.FLG_TIPO_MOVIMIENTO FOR READ ONLY

      SET @SALDO = 0
      SET @PRIMER_REGISTRO = 1
     OPEN CURSOR_MOV_ACTIVOS FETCH NEXT FROM CURSOR_MOV_ACTIVOS
        INTO @TIPO_MOVIMIENTO,
             @CANTIDAD,
             @PRECIO,
             @MONTO,
             @FECHA_OPERACION,
             @ID_MONEDA_OPE
     WHILE @@FETCH_STATUS = 0 AND @@ERROR = 0
        BEGIN
           IF @ID_MONEDA <> @ID_MONEDANEMO
              BEGIN
                 IF @ID_MONEDA_OPE <> @ID_MONEDANEMO
                    BEGIN
                       SELECT @VALORCAMBIO = VALOR
                         FROM VALOR_TIPO_CAMBIO
                        WHERE ID_TIPO_CAMBIO = (SELECT ID_TIPO_CAMBIO
                                                  FROM TIPO_CAMBIOS
                                                 WHERE ID_MONEDA_DESTINO = @ID_MONEDANEMO)
                          AND FECHA = @FECHA_OPERACION
                    END
                 ELSE
                 BEGIN
                    SET @VALORCAMBIO = 1
                 END
              END
           ELSE
              BEGIN
                 SET @VALORCAMBIO = 1
              END

           IF @PRIMER_REGISTRO = 1
              BEGIN
                 IF @SALDO = 0 AND @TIPO_MOVIMIENTO = 'I'
                    BEGIN
                       SET @SALDO = @SALDO + @CANTIDAD
                       IF @ID_MONEDA = @ID_MONEDANEMO
                          BEGIN
                             SET @SALDO_PESOS = @MONTO
                             SET @PRECIO_PROMEDIO = @PRECIO
                          END
                       ELSE
                          BEGIN
                             IF @ID_MONEDA_OPE <> @ID_MONEDANEMO
                                BEGIN
                                   SET @SALDO_PESOS = @MONTO / @VALORCAMBIO
                                   SET @PRECIO_PROMEDIO = @PRECIO / @VALORCAMBIO
                                END
                             ELSE
                                BEGIN
                                   SET @SALDO_PESOS = @MONTO
                                   SET @PRECIO_PROMEDIO = @PRECIO
                                END
                          END
                    END
              END
           ELSE
              BEGIN
                 IF @TIPO_MOVIMIENTO = 'I'
                    BEGIN
                       IF @SALDO > 0
                          BEGIN
                             SET @SALDO = @SALDO + @CANTIDAD
                             IF @ID_MONEDA = @ID_MONEDANEMO
                                BEGIN
								   SET @SALDO_PESOS = @SALDO_PESOS + @MONTO
                                END
                             ELSE
                                BEGIN
                                   IF @ID_MONEDA_OPE <> @ID_MONEDANEMO
                                      BEGIN
                                         SET @SALDO_PESOS = @SALDO_PESOS + (@MONTO / @VALORCAMBIO)
                                      END
                                   ELSE
                                      BEGIN
                                         SET @SALDO_PESOS = @SALDO_PESOS + @MONTO
                                      END
                                END
                             SET @PRECIO_PROMEDIO = ROUND(@SALDO_PESOS/@SALDO,4)
                          END
                       ELSE
                          BEGIN
                             SET @SALDO = @CANTIDAD
                             IF @ID_MONEDA = @ID_MONEDANEMO
                                BEGIN
                                   SET @SALDO_PESOS = @MONTO
                                   SET @PRECIO_PROMEDIO = @PRECIO
                                END
                             ELSE
                                BEGIN
                                   IF @ID_MONEDA_OPE <> @ID_MONEDANEMO
                                      BEGIN
                                         SET @SALDO_PESOS = @MONTO / @VALORCAMBIO
                                         SET @PRECIO_PROMEDIO = @PRECIO / @VALORCAMBIO
                                      END
                                   ELSE
                                      BEGIN
                                         SET @SALDO_PESOS = @MONTO
                                         SET @PRECIO_PROMEDIO = @PRECIO
                                      END
                                END
                          END
                    END
                 ELSE
                    BEGIN
                       IF @SALDO > 0
                          BEGIN
                             SET @SALDO = @SALDO - @CANTIDAD
                             SET @SALDO_PESOS = @SALDO_PESOS - ROUND((@CANTIDAD*@PRECIO_PROMEDIO),0)
                             SET @PRECIO_PROMEDIO = @PRECIO_PROMEDIO
                          END
                    END
              END
           SET @PRIMER_REGISTRO = 2
           FETCH NEXT FROM CURSOR_MOV_ACTIVOS INTO @TIPO_MOVIMIENTO,
                                                   @CANTIDAD,
                                                   @PRECIO,
                                                   @MONTO,
                                                   @FECHA_OPERACION,
                                                   @ID_MONEDA_OPE
        END
     CLOSE CURSOR_MOV_ACTIVOS
     DEALLOCATE CURSOR_MOV_ACTIVOS
     RETURN @PRECIO_PROMEDIO
END
GO

GRANT EXECUTE ON [ENTREGA_PRECIO_PROMEDIO] TO DB_EXECUTESP
GO



