USE [CSGPI]
GO
IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_CLIENTES$BuscarClientesAPV_DJ1899]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
DROP PROCEDURE [DBO].[PKG_CLIENTES$BuscarClientesAPV_DJ1899]
GO

CREATE PROCEDURE [dbo].[PKG_CLIENTES$BuscarClientesAPV_DJ1899]
@Pid_Cliente float(53)   =  NULL,
@PRut_Cliente varchar(10)=  NULL
AS
BEGIN

            SELECT * ,
                   CASE WHEN (tipo_entidad = 'J') THEN  razon_social
		           ELSE
		              (isnull (nombres, '') + ' ' + isnull (paterno, '') + ' ' + isnull (materno, ''))
		           END AS nombre_cliente
              FROM CLIENTES
             WHERE (ID_CLIENTE in (select id_cliente from view_cuentas_vigentes where id_tipocuenta = 2))
			   --AND COD_ESTADO = 'V'
               AND (RUT_CLIENTE = ISNULL(@PRut_Cliente, RUT_CLIENTE))
            --ORDER BY RUT_CLIENTE;

			UNION ALL

			SELECT  convert(int,SUBSTRING(LTRIM(RTRIM(( REPLACE(REPLACE(AKA4CPP.A4T3CZ,'.',''),'-','')))),1,LEN(LTRIM(RTRIM((REPLACE(REPLACE(AKA4CPP.A4T3CZ,'.',''),'-','')))))-1)) ID_CLIENTE,
				    '0'     									   ID_TIPO_ESTADO,
				    'V'										       COD_ESTADO,
				    NULL									       COD_PAIS_CONYUGE,
					NULL										   COD_PAIS,
					LTRIM(RTRIM((REPLACE(AKA4CPP.A4T3CZ,'.','')))) RUT_CLIENTE,
					NULL										   TIPO_ENTIDAD,
					NULL										   PATERNO,
					NULL										   MATERNO,
					NULL										   NOMBRES,
					NULL										   PROFESION,
					NULL										   SEXO,
					NULL										   FECHA_NACIMIENTO,
					NULL										   ESTADO_CIVIL,
					NULL										   CELULAR,
					NULL										   OBSERVACIONES,
					NULL										   EMAIL,
					NULL										   EMPLEADOR,
					NULL										   CARGO,
					NULL										   RUT_CONYUGE,
					NULL										   PATERNO_CONYUGE,
					NULL										   MATERNO_CONYUGE,
					NULL										   NOMBRES_CONYUGE,
					NULL										   COPIA_ESCRITURA,
					NULL										   RAZON_SOCIAL,
					NULL										   ID_TIPO_SOCIEDAD,
					NULL										   GIRO,
					NULL										   FECHA_FORMACION,
					NULL										   LUGAR,
					NULL										   NOTARIA,
					NULL										   REGISTRO,
					NULL										   NUMERO,
					NULL										   ANO,
					NULL										   ANTECEDENTES,
					NULL										   SEGMENTO_CLIENTE,
					NULL										   FLG_MAYOR_EDAD,
					NULL										   FLG_CLIENTE_BANCARIO,
					NULL										   SITIOWEB,
					NULL										   ORIGEN_REFERIDO,
					NULL										   CONTACTO_REFERIDO,
					NULL										   COD_SUCURSAL,
					NULL										   COD_EJECUTIVO,
					NULL										   ID_TIPO_REFERIDO,
					NULL										   FLG_CONTRATO_BPI,
					NULL										   ID_CONTRATO_NUPCIAL,
					NULL										   ID_USUARIO_INSERT,
					NULL										   FCH_INSERT,
					NULL										   ID_USUARIO_UPDATE,
					NULL										   FCH_UPDATE,
					NULL										   FOLIO_1862_APORTE_RESCATE_CAPI,
					NULL										   FOLIO_1862_REG_INDIVIDUAL,
					NULL										   FECHA_CREACION,
					NULL										   TIPO_TRABAJADOR,
					NULL										   ID_BANCO,
					NULL										   NUMERO_CTA_CTE,
					NULL										   FECHA_PENSIONADO,
					'A'											   ACTIVO_PENSIONADO,
					AKA4CPP.A4HNXT								   NOMBRE_CLIENTE
			   FROM FFMM.INVERSIONES.DBO.TB_ULLA_AS400_AKA4CPP AKA4CPP
			  ORDER BY 1 --RUT_CLIENTE
END
GO

GRANT EXECUTE ON [PKG_CLIENTES$BuscarClientesAPV_DJ1899] TO DB_EXECUTESP
GO