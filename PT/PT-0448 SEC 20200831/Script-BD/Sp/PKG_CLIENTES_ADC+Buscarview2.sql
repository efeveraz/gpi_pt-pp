IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_CLIENTES_ADC$Buscarview2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [DBO].[PKG_CLIENTES_ADC$Buscarview2]
GO

CREATE PROCEDURE [dbo].[PKG_CLIENTES_ADC$Buscarview2]
@PFecha_cierre        DATETIME   = NULL,
@Pid_Empresa          NUMERIC    = NULL,
@Pid_Asesor           NUMERIC    = NULL,
@Pid_Cliente          NUMERIC    = NULL,
@PCod_Estado_Cliente  VARCHAR(1) = NULL
AS
BEGIN
   SET NOCOUNT ON
     DECLARE @LID_ORIGEN NUMERIC, @LID_TIPO_CONVERSION NUMERIC

   SET @LID_ORIGEN = (SELECT ID_ORIGEN FROM ORIGENES WHERE COD_ORIGEN = 'MAGIC_VALORES')
   SET @LID_TIPO_CONVERSION = (SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WHERE TABLA = 'CUENTAS')

   CREATE TABLE #TBLCOMISION(ORIGEN          VARCHAR(3)
                           ,ID_CUENTA NUMERIC
                           ,COD_INSTRUMENTO VARCHAR(30)
                           ,COD_PRODUCTO    VARCHAR(50)
                           ,COMISION        FLOAT)
   INSERT INTO #TBLCOMISION
   SELECT 'ADC'
        , CT.ID_CUENTA
        , CC.COD_INSTRUMENTO
        , IT.COD_PRODUCTO
        , (CAST(CC.COMISION_HONORARIOS AS FLOAT) * 100) 
     FROM VIEW_CUENTAS CT 
        , COMISIONES_CUENTAS CC 
          INNER JOIN INSTRUMENTOS IT ON IT.COD_INSTRUMENTO = CC.COD_INSTRUMENTO 
    WHERE CT.ID_EMPRESA = ISNULL(@PID_EMPRESA, CT.ID_EMPRESA) 
      AND CT.ID_ASESOR = ISNULL(@PID_ASESOR, CT.ID_ASESOR)
	  AND CT.ID_CLIENTE = ISNULL(@PID_CLIENTE, CT.ID_CLIENTE)
      AND CT.COD_ESTADO_CLIENTE = ISNULL(@PCOD_ESTADO_CLIENTE, CT.COD_ESTADO_CLIENTE)
      AND CC.ID_CUENTA = CT.ID_CUENTA
      AND ((@PFECHA_CIERRE BETWEEN CC.FECHA_INI AND CC.FECHA_TER) OR (CC.FECHA_INI < @PFECHA_CIERRE AND CC.FECHA_TER IS NULL))
   UNION
    SELECT TOP 1 'TRX'
        , CT.ID_CUENTA
        , VI.COD_INSTRUMENTO
        , IT.COD_PRODUCTO
        , CAST(VI.COMISION AS FLOAT) * 100 
	  FROM VIEW_CUENTAS CT 
        , VIEW_COM_INSTR_CUENTA_TODAS VI
           INNER JOIN INSTRUMENTOS IT ON IT.COD_INSTRUMENTO = VI.COD_INSTRUMENTO 
     WHERE CT.ID_EMPRESA = ISNULL(@PID_EMPRESA, CT.ID_EMPRESA) 
      AND CT.ID_ASESOR = ISNULL(@PID_ASESOR, CT.ID_ASESOR)
	  AND CT.ID_CLIENTE = ISNULL(@PID_CLIENTE, CT.ID_CLIENTE)
      AND CT.COD_ESTADO_CLIENTE = ISNULL(@PCOD_ESTADO_CLIENTE, CT.COD_ESTADO_CLIENTE)
      AND VI.ID_CUENTA = CT.ID_CUENTA
      AND ((@PFECHA_CIERRE BETWEEN FECHA_DESDE AND FECHA_HASTA) OR (FECHA_DESDE < @PFECHA_CIERRE AND FECHA_HASTA IS NULL))   		       
   
   SELECT VC.NUM_CUENTA
        , VC.NOMBRE_CLIENTE
        , CASE WHEN ((SELECT CHARINDEX('/', VALOR, 1) 
                        FROM REL_CONVERSIONES 
                       WHERE ID_ORIGEN = @LID_ORIGEN AND 
                             ID_TIPO_CONVERSION = @LID_TIPO_CONVERSION AND 
                             ID_ENTIDAD = VC.ID_CUENTA) > 0) THEN
		                           (VC.RUT_CLIENTE +
		                              CASE WHEN ISNULL(SUBSTRING((SELECT (', ' + SUBSTRING(VALOR, CHARINDEX('/', VALOR, 1) + 1, LEN(VALOR)))  
								                                    FROM REL_CONVERSIONES  
								                                   WHERE ID_ORIGEN = @LID_ORIGEN
								                                     AND ID_TIPO_CONVERSION = @LID_TIPO_CONVERSION
								                                     AND ID_ENTIDAD = VC.ID_CUENTA
								                               FOR XML PATH ('')),3,100 ),'') = '' THEN '' 
			                               ELSE 
				                                 '/' + SUBSTRING((SELECT (', ' + SUBSTRING(VALOR, CHARINDEX('/', VALOR, 1) + 1, LEN(VALOR)))  
								                                    FROM REL_CONVERSIONES  
								                                   WHERE ID_ORIGEN = @LID_ORIGEN
								                                     AND ID_TIPO_CONVERSION = @LID_TIPO_CONVERSION
								                                     AND ID_ENTIDAD = VC.ID_CUENTA
								                            FOR XML PATH ('')),3,100 ) 
				                      END )
                   ELSE VC.RUT_CLIENTE
          END AS RUT_CLIENTE
        , VC.DSC_ESTADO
        , VC.DSC_EMPRESA
        , VC.DSC_ASESOR
        , VC.DSC_TIPO_ADMINISTRACION
        , DSC_PERFIL_RIESGO
        , CAST(VC.PORCEN_RV AS INT) PORCEN_RV
        , CAST(VC.PORCEN_RF AS INT) PORCEN_RF
        , VC.FECHA_CONTRATO
        , VC.FECHA_OPERATIVA
        , (SELECT CT.FECHA_CIERRE_CUENTA 
		     FROM CUENTAS AS CT 
		    WHERE CT.ID_CUENTA = VC.ID_CUENTA) AS FECHA_CIERRE_CUENTA
		, MO.DSC_MONEDA
		, PM.SALDO_ACTIVO_MON_CUENTA
		, PM.PATRIMONIO_MON_CUENTA
		, ISNULL(( SELECT COMISION
		             FROM #TBLCOMISION CC
		            WHERE CC.ORIGEN = 'ADC'
		              AND CC.ID_CUENTA = VC.ID_CUENTA
		              AND CC.COD_INSTRUMENTO = 'ACCIONES_NAC'),0) 'RVADC_NAC'
		, ISNULL(( SELECT COMISION
		             FROM #TBLCOMISION CC
		            WHERE CC.ORIGEN = 'ADC'
		              AND CC.ID_CUENTA = VC.ID_CUENTA
		              AND CC.COD_INSTRUMENTO = 'ACCIONES_INT'),0) 'RVADC_INT'
		, ISNULL(( SELECT COMISION
		             FROM #TBLCOMISION CC
		            WHERE CC.ORIGEN = 'ADC'
		              AND CC.ID_CUENTA = VC.ID_CUENTA
		              AND CC.COD_INSTRUMENTO = 'BONOS_NAC'),0) 'RFADC_NAC'
		, ISNULL(( SELECT COMISION
		             FROM #TBLCOMISION CC
		            WHERE CC.ORIGEN = 'ADC'
		              AND CC.ID_CUENTA = VC.ID_CUENTA
		              AND CC.COD_INSTRUMENTO = 'BONOS_INT'),0) 'RFADC_INT'
		, ISNULL(( SELECT ISNULL(SUM(COMISION),0)
		             FROM #TBLCOMISION CC
		            WHERE CC.ORIGEN = 'ADC'
		              AND CC.ID_CUENTA = VC.ID_CUENTA
		              AND CC.COD_PRODUCTO = 'RF_NAC'
		              AND CC.COD_INSTRUMENTO NOT IN ('BONOS_NAC')),0) 'IIFADC_NAC'
		, ISNULL(( SELECT ISNULL(SUM(COMISION),0)
		             FROM #TBLCOMISION CC
		            WHERE CC.ORIGEN = 'ADC'
		              AND CC.ID_CUENTA = VC.ID_CUENTA
		              AND CC.COD_PRODUCTO = 'RF_INT'
		              AND CC.COD_INSTRUMENTO NOT IN ('BONOS_INT')),0) 'IIFADC_INT'
		, ISNULL(( SELECT COMISION
		             FROM #TBLCOMISION CC
		            WHERE CC.ORIGEN = 'TRX'
		              AND CC.ID_CUENTA = VC.ID_CUENTA
		              AND CC.COD_PRODUCTO ='RV_NAC'),0) AS 'RV_NAC'  
		, ISNULL(( SELECT COMISION
		             FROM #TBLCOMISION CC
		            WHERE CC.ORIGEN = 'TRX'
		              AND CC.ID_CUENTA = VC.ID_CUENTA
		              AND CC.COD_PRODUCTO ='RV_INT'),0) AS 'RV_INT'  
		, ISNULL(( SELECT COMISION
		             FROM #TBLCOMISION CC
		            WHERE CC.ORIGEN = 'TRX'
		              AND CC.ID_CUENTA = VC.ID_CUENTA
		              AND CC.COD_PRODUCTO ='RF_NAC' 
	                  AND CC.COD_INSTRUMENTO  IN ('BONOS_NAC')),0) AS 'RF_NAC'
		, ISNULL(( SELECT COMISION
		             FROM #TBLCOMISION CC
		            WHERE CC.ORIGEN = 'TRX'
		              AND CC.ID_CUENTA = VC.ID_CUENTA
		              AND CC.COD_PRODUCTO ='RF_INT' 
		              AND CC.COD_INSTRUMENTO  IN ('BONOS_INT')),0) AS 'RF_INT'
		, ISNULL(( SELECT ISNULL(SUM(COMISION),0)
		             FROM #TBLCOMISION CC
		            WHERE CC.ORIGEN = 'TRX'
		              AND CC.ID_CUENTA = VC.ID_CUENTA
		              AND CC.COD_PRODUCTO ='RF_INT' 
	                  AND CC.COD_INSTRUMENTO NOT  IN ('BONOS_INT')),0) AS 'IIF_INT'
		, ISNULL(( SELECT ISNULL(SUM(COMISION),0)
		             FROM #TBLCOMISION CC
		            WHERE CC.ORIGEN = 'TRX'
		              AND CC.ID_CUENTA = VC.ID_CUENTA
		              AND CC.COD_PRODUCTO ='RF_NAC' 
	                  AND CC.COD_INSTRUMENTO NOT  IN ('BONOS_NAC')),0) AS 'IIF_NAC'
/*
	     , CASE WHEN (VC.COD_ESTADO_CLIENTE = 'V') THEN 'VIGENTES'
                WHEN (VC.COD_ESTADO_CLIENTE = 'A') THEN 'ANULADOS'
           END AS 'DSC_ESTADO_CLIENTE'
*/
	     , CASE WHEN @PCOD_ESTADO_CLIENTE IS NULL THEN
                     CASE WHEN (VC.COD_ESTADO_CLIENTE = 'V') THEN 'VIGENTES'
                          WHEN (VC.COD_ESTADO_CLIENTE = 'A') THEN 
                               CASE WHEN ((@PFECHA_CIERRE BETWEEN ISNULL(CL.FECHA_CREACION, CL.FCH_INSERT) AND CL.FECHA_TERMINO)) THEN 'VIGENTES'
                                    ELSE 'ANULADOS'
                               END
                     END
                ELSE
	                 CASE WHEN (VC.COD_ESTADO_CLIENTE = 'V') THEN 'VIGENTES'
                          WHEN (VC.COD_ESTADO_CLIENTE = 'A') THEN 'ANULADOS'
                     END     
           END AS 'DSC_ESTADO_CLIENTE'
         , CL.EMAIL AS EMAIL_CLIENTE
     FROM  VIEW_CUENTAS AS VC
		   INNER JOIN PATRIMONIO_CUENTAS PM ON VC.ID_CUENTA = PM.ID_CUENTA 
				                           AND PM.FECHA_CIERRE = ISNULL(@PFECHA_CIERRE, FECHA_CIERRE) 
		   INNER JOIN MONEDAS AS MO ON PM.ID_MONEDA_CUENTA = MO.ID_MONEDA
           INNER JOIN CLIENTES CL ON VC.ID_CLIENTE = CL.ID_CLIENTE
	WHERE VC.ID_EMPRESA = ISNULL(@PID_EMPRESA, VC.ID_EMPRESA) 
      AND VC.ID_ASESOR = ISNULL(@PID_ASESOR, VC.ID_ASESOR)
	  AND VC.ID_CLIENTE = ISNULL(@PID_CLIENTE, VC.ID_CLIENTE)
      AND VC.COD_ESTADO_CLIENTE = ISNULL(@PCOD_ESTADO_CLIENTE, VC.COD_ESTADO_CLIENTE)
	ORDER BY VC.ID_EMPRESA, VC.ID_ASESOR, VC.ID_CLIENTE
	

    DROP TABLE #TBLCOMISION	
    SET NOCOUNT OFF
END
GO

GRANT EXECUTE ON [PKG_CLIENTES_ADC$Buscarview2] TO DB_EXECUTESP
GO
