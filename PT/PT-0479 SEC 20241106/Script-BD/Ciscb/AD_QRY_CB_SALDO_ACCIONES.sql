USE [CisCB]
GO
/****** Object:  StoredProcedure [dbo].[AD_QRY_CB_SALDO_ACCIONES]    Script Date: 05/11/2024 12:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--/******************************************************************
--Procedimiento         : AD_QRY_CB_SALDO_ACCIONES
--Objetivo Descripcion  : SP creado para Obtener Saldo de acciones de Clientes GPI
--Sistema               :
--Base de Datos         : CisCB
--Tablas Usadas         :
--Paramentros Entrada   : Fecha_Saldo (YYYYMMDD),Rut_Cliente
--Paramentros Salida    :
--Cadena Ejecucion      :
--								USE [CISCB]
--								GO

--								DECLARE @return_value int

--								EXEC @return_value = [dbo].[AD_QRY_CB_SALDO_ACCIONES]
--								  @pFecha_saldo = N'20160924',
--								  @pRutCliente = N'76038702-9/2'

--								SELECT 'Return Value' = @return_value

--								GO
--Autor                 :
--Modificacion		  :	Fernando Tapia
--Fecha Creacion        :
--Fecha Modificacion    : 08/10/2018

--Autor                 : Waldo Mu�oz
--Modificacion		  :	Se realiza Modificacion en Logica de simultaneas, incluyendo nuevo nivel de agrupación
--Fecha Creacion        :
--Fecha Modificacion    : 12/05/2021

--    Autor                 : Waldo Mu�oz
--Modificacion		  :	Se realiza Modificacion CREATE
--Fecha Creacion        :
--Fecha Modificacion    : 12/05/2021

--  Autor                 : Cristian Alvarez
--Modificacion		  :	Se realiza Modificacion
--Fecha Creacion        :
--Fecha Modificacion    : 09/07/2021

--  Autor                 : Cristian Alvarez
--Modificacion		  :	Se realiza Optimizacion de SP
--Fecha Creacion        :
--Fecha Modificacion    : 30/07/2021
--****************************************************************/


ALTER PROCEDURE [dbo].[AD_QRY_CB_SALDO_ACCIONES]
    @pFecha_saldo CHAR(8),
	@pRutCliente VARCHAR(15)  = NULL
AS
BEGIN
--declare
--@pFecha_saldo CHAR(8) = '20240727',
--@pRutCliente VARCHAR(15) = null
 set dateformat ymd
 set nocount on

 declare @vFecSal int, @msgerr char(80)

 set @msgerr = ''
 if isdate(@pFecha_saldo) = 0
  set @msgerr = 'Fecha Invalida (' + rtrim(@pFecha_saldo)

 if @msgerr=''
 begin
  if rtrim(@pFecha_saldo) = ''
   set @msgerr = 'Debe ingresar una Fecha (YYYYMMDD)'
  else
   set @vFecSal = (CAST(convert(datetime, @pFecha_saldo) AS INT)+693596)
 end

 create table #tmp_acciones(
							  t_Rut_cliente    char(15),
							  t_Nemotecnico    varchar(20),
							  t_Saldo          decimal(16,4),
							  t_Valor_mercado  Decimal(16),
							  t_precio         Decimal(16,4),
							  t_Nombre_cliente varchar(120) NULL,
							  t_simultaneas	   decimal(16,4),
							  t_garantias	   decimal(16,4),
							  t_prestamos	   decimal(16,4)
							 )

 if @msgerr = ''
 BEGIN
-- Crear tablas temporales para subconsultas
SELECT
    Gar_idpersona,
    Gar_instrumento,
    SUM(Gar_cantidad) AS Cantidad_garantia
INTO #tmp_garantia
FROM GARANTIA WITH (NOLOCK)
WHERE
    Gar_fecha_ingreso <= @vFecSal AND
    (Gar_fecha_levantamiento > @vFecSal OR Gar_fecha_levantamiento = 0 AND Gar_estado = 'V') AND
    Gar_tipo NOT IN ('SIMULTANEADAS', 'PRESTAMO', 'VENTA CORTA RF', 'VENTA CORTA RV') AND
    Gar_instrumento NOT LIKE 'CFM%' AND
    Gar_folio > 0
GROUP BY Gar_idpersona, Gar_instrumento;

SELECT
    Gar_idpersona,
    Gar_instrumento,
    SUM(Gar_cantidad) AS Cantidad_prestamo
INTO #tmp_prestamo
FROM GARANTIA WITH (NOLOCK)
WHERE
    Gar_fecha_ingreso <= @vFecSal AND
    Gar_fecha_vencimiento >= @vFecSal AND
    Gar_fecha_levantamiento >=
    (CASE WHEN Gar_fecha_levantamiento > 0 THEN @vFecSal ELSE Gar_fecha_levantamiento END) AND
    UPPER(Gar_estado) <> 'A' AND
    Gar_tipo = 'PRESTAMO' AND
    Gar_instrumento NOT LIKE 'CFM%' AND
    Gar_folio > 0
GROUP BY Gar_idpersona, Gar_instrumento;

SELECT
    tb_sim.id_cliente,
    tb_sim.instrumento AS Instrumento,
    SUM(ISNULL(tb_sim.nominales, 0)) AS Cantidad_simultanea
INTO #tmp_simultanea
FROM (
    SELECT
        bac.instrumento,
        bac.id_cliente,
        bac.nominales,
        bac.concepto,
        bac.id_concepto,
        bac.id_linea_concepto
    FROM dbo.BACKOFF bac WITH (NOLOCK)
    LEFT JOIN dbo.CIEBOL cie WITH (NOLOCK)
        ON bac.Fecha_palo = cie.Fecha_del_cierre AND
           bac.Hora_palo = cie.Hora_del_cierre AND
           bac.codigo_bolsa = cie.Bolsa_del_cierre AND
           bac.id_cierre_bolsa = cie.Id_bolsa_del_cierre AND
           bac.tipo_liquidacion = cie.Tipo_de_liquidacion
    INNER JOIN dbo.VIEW_CLIENTE_GPI_SEC v
        ON v.cuenta = bac.Id_cliente AND v.origen_magic = 'MAGIC_VALORES'
    WHERE bac.fecha_de_mvto <= @vFecSal AND
          bac.origen = 'OP' AND
          bac.concepto IN ('IC', 'IV', 'CD', 'VD') AND
          bac.id_concepto > 0 AND
          bac.id_linea_concepto > 0 AND
          bac.fecha_liquidacion > @vFecSal AND
          bac.tipo_liquidacion = 'TP' AND
          bac.modulo_de_entrada = 'RV' AND
          bac.numero_orden_simultanea > 0
) tb_sim
GROUP BY tb_sim.instrumento, tb_sim.id_cliente;

INSERT INTO #tmp_acciones
SELECT
    sc.Id_cliente AS 'RUT CLIENTE',
    sc.Instrumento AS 'NEMOTECNICO',
    sc.Cantidad AS 'SALDO',
      'VALOR MERCADO'= Case
	                       When ((SELECT isnull(M.Macc_accion_tipo, '') FROM MAEACC M WHERE M.Macc_accion = sc.Instrumento) = 'O') then
                               convert(numeric(16), sc.Cantidad *
                                       isnull((select top 1 pm.Valor_mercado
                                                 from premer as pm with(nolock)
                                                where pm.Instrumento=sc.Instrumento and
                                                      pm.Fecha_de_tasas <= @vFecSal
                                                order by pm.Fecha_de_tasas desc),0))
                           Else
					           convert(numeric(16), sc.Cantidad *
                                       isnull((select top 1 pm.Valor_mercado
                                                 from premer as pm with(nolock)
                                                where pm.Instrumento=sc.Instrumento and
                                                      pm.Fecha_de_tasas <= @vFecSal
                                                order by pm.Fecha_de_tasas desc),1))
                       End,
             'PRECIO'= Case
	                       When ((SELECT isnull(M.Macc_accion_tipo, '') FROM MAEACC M WHERE M.Macc_accion = sc.Instrumento) = 'O') then
                               convert(numeric(18,4), isnull((select top 1 pm.Valor_mercado
                                                                from premer as pm with(nolock)
                                                               where pm.Instrumento=sc.Instrumento and
                                                                     pm.Fecha_de_tasas <= @vFecSal
                                                               order by pm.Fecha_de_tasas desc),0))
                           Else
                               convert(numeric(18,4), isnull((select top 1 pm.Valor_mercado
                                                                from premer as pm with(nolock)
                                                               where pm.Instrumento=sc.Instrumento and
                                                                     pm.Fecha_de_tasas <= @vFecSal
                                                               order by pm.Fecha_de_tasas desc),1))
                       End,
    NULL AS 'Nombre_cliente',
    isnull(s.Cantidad_simultanea,0) AS 'simultaneas',
    isnull(g.Cantidad_garantia	,0) AS 'garantias',
    isnull(p.Cantidad_prestamo	,0) AS 'prestamos'
FROM ciscb.dbo.salcuscli sc WITH (NOLOCK)
INNER JOIN dbo.VIEW_CLIENTE_GPI_SEC v
    ON v.cuenta = sc.Id_cliente AND v.origen_magic = 'MAGIC_VALORES'
INNER JOIN dbo.persocb ps WITH (NOLOCK)
    ON ps.Id_cliente = sc.Id_cliente AND ps.Administracion_de_cartera = 'S'
INNER JOIN Instru i WITH (NOLOCK)
    ON i.Id_instrumento = sc.Instrumento AND i.Tipo_instrumento = 'A'
LEFT JOIN #tmp_garantia g
    ON sc.Id_cliente = g.Gar_idpersona AND sc.Instrumento = g.Gar_instrumento
LEFT JOIN #tmp_prestamo p
    ON sc.Id_cliente = p.Gar_idpersona AND sc.Instrumento = p.Gar_instrumento
LEFT JOIN #tmp_simultanea s
    ON sc.Id_cliente = s.id_cliente AND sc.Instrumento = s.Instrumento
WHERE sc.Fecha_saldo = @vFecSal
AND sc.Id_cliente = ISNULL(@pRutCliente, sc.Id_cliente);

END

-- Crear la tabla temporal
CREATE TABLE #tmp_Simultaneas (
    instrumento VARCHAR(255),
    id_cliente VARCHAR(255),
    sim_prepago DECIMAL(18, 2)
);

-- Insertar datos en la tabla temporal
INSERT INTO #tmp_Simultaneas (instrumento, id_cliente, sim_prepago)
SELECT
    bac.instrumento,
    bac.id_cliente,
    SUM(ppg.nominales) AS sim_prepago
FROM dbo.BACKOFF bac
LEFT JOIN dbo.CIEBOL cie
    ON bac.Fecha_palo = cie.Fecha_del_cierre
    AND bac.Hora_palo = cie.Hora_del_cierre
    AND bac.codigo_bolsa = cie.Bolsa_del_cierre
    AND bac.id_cierre_bolsa = cie.Id_bolsa_del_cierre
    AND bac.tipo_liquidacion = cie.Tipo_de_liquidacion
INNER JOIN dbo.VIEW_CLIENTE_GPI_SEC v
    ON v.cuenta = bac.Id_cliente AND v.origen_magic = 'MAGIC_VALORES'
LEFT JOIN dbo.PREPAGO_SIMULTANEA ppg
    ON ppg.concepto = bac.concepto
    AND ppg.id_concepto = bac.id_concepto
    AND ppg.id_linea_concepto = bac.Id_Linea_Concepto
    AND ppg.fecha_liquidacion <= @vFecSal
WHERE bac.fecha_de_mvto <= @vFecSal
    AND bac.origen = 'OP'
    AND bac.concepto IN ('IC', 'IV', 'CD', 'VD')
    AND bac.id_concepto > 0
    AND bac.id_linea_concepto > 0
    AND bac.fecha_liquidacion > @vFecSal
    AND bac.tipo_liquidacion = 'TP'
    AND bac.modulo_de_entrada = 'RV'
    AND bac.numero_orden_simultanea > 0
GROUP BY bac.instrumento, bac.id_cliente;

UPDATE #tmp_acciones
SET t_simultaneas = t_simultaneas - temp.sim_prepago
FROM #tmp_acciones
INNER JOIN #tmp_Simultaneas temp
    ON t_Rut_cliente = temp.id_cliente
    AND t_Nemotecnico = temp.instrumento;


--Retorno de datos

 select 'RUT_CLIENTE'			= t_Rut_cliente,
        'NEMOTECNICO'			= t_Nemotecnico,
        'SALDO'					= t_Saldo,
        'VALOR_MERCADO'			= t_Valor_mercado,
        'PRECIO'				= t_precio,
        'NOMBRE_CLIENTE'		= DBO.fn_va_NombreCliente(t_Rut_cliente),
        'CANTIDAD_GARANTIAS'	= t_garantias,
        'CANTIDAD_PRESTAMOS'	= t_prestamos,
        'CANTIDAD_SIMULTANEAS'	= t_simultaneas
 from	#tmp_acciones
 order by t_Rut_cliente, t_Nemotecnico

DROP TABLE #tmp_acciones
DROP TABLE #tmp_simultanea
DROP TABLE #tmp_prestamo
DROP TABLE #tmp_garantia
DROP TABLE #tmp_Simultaneas

End
