ALTER PROCEDURE [dbo].[HAF_PROCESOCIERRE_PATRIMONIOSCUENTA_CALCULAR]        
( @PID_CUENTA   NUMERIC        
, @PFECHA       DATETIME        
, @PID_USUARIO  NUMERIC        
, @PRESULTADO   VARCHAR(MAX) OUTPUT        
)     
AS        
SET NOCOUNT ON        
BEGIN TRY    
    
  DECLARE @LPROCESO VARCHAR(100)         
  SET @LPROCESO = 'HAF_PROCESOCIERRE_PATRIMONIOSCUENTA_CALCULAR'        
        
  INSERT INTO #TMP_LOG(ID_USUARIO, PROCESO, MENSAJE)        
  VALUES (@PID_USUARIO, @LPROCESO, REPLICATE(CHAR(9), 2) + 'CALCULANDO PATRIMONIOS...')        
        
  DELETE FROM PATRIMONIO_CUENTAS         
   WHERE ID_CUENTA = @PID_CUENTA        
     AND FECHA_CIERRE = @PFECHA        
        
  DECLARE @LDECIMALES_CTA NUMERIC         
  SET @LDECIMALES_CTA  = ISNULL((SELECT M.DICIMALES_MOSTRAR        
                                   FROM CUENTAS CTA WITH (NOLOCK)         
                                  INNER JOIN MONEDAS M WITH (NOLOCK) ON M.ID_MONEDA = CTA.ID_MONEDA        
                                  WHERE CTA.ID_CUENTA = @PID_CUENTA        
                                ), 0)        
        
  DECLARE @LID_MONEDA_USD NUMERIC        
  SELECT @LID_MONEDA_USD = DBO.FNT_DAMEIDMONEDA('USD')        
            
  DECLARE @LSALDO_CAJA_MON_CUENTA   FLOAT         
        , @LMONTO_X_COBRAR_MON_CTA  FLOAT         
        , @LMONTO_X_PAGAR_MON_CTA   FLOAT         
        , @LVALOR_CUOTA_MON_CUENTA  FLOAT         
        , @LTOTAL_CUOTAS_MON_CUENTA FLOAT         
        , @LPATRIMONIO_MON_CUENTA   FLOAT        
      
  DECLARE @LAPORTE_MON_CTA    FLOAT         
        , @LRESCATE_MON_CTA   FLOAT         
  DECLARE @LSALDO_ACTIVO_MON_CUENTA FLOAT         
        
  DECLARE @LMONTO_COMISION_DEVENGADA FLOAT        
  SET @LMONTO_COMISION_DEVENGADA = 0        
        
  SELECT  @LSALDO_CAJA_MON_CUENTA   = 0        
        , @LMONTO_X_COBRAR_MON_CTA  = 0        
        , @LMONTO_X_PAGAR_MON_CTA   = 0        
        , @LVALOR_CUOTA_MON_CUENTA  = 0        
        , @LTOTAL_CUOTAS_MON_CUENTA = 0        
        , @LAPORTE_MON_CTA          = 0        
        , @LRESCATE_MON_CTA         = 0        
        , @LSALDO_ACTIVO_MON_CUENTA = 0        
        , @LPATRIMONIO_MON_CUENTA   = 0        
         

  SELECT  @LSALDO_CAJA_MON_CUENTA  = ROUND(ISNULL(SUM(SC.MONTO_MON_CTA), 0), @LDECIMALES_CTA)       
        , @LMONTO_X_COBRAR_MON_CTA = ROUND(ISNULL(SUM(SC.MONTO_X_COBRAR_MON_CTA), 0),@LDECIMALES_CTA)        
        , @LMONTO_X_PAGAR_MON_CTA  = ROUND(ISNULL(SUM(SC.MONTO_X_PAGAR_MON_CTA), 0), @LDECIMALES_CTA)        
     FROM View_saldos_caja SC WITH (NOLOCK)          
	WHERE SC.id_cuenta    = @PID_CUENTA         
      AND SC.FECHA_CIERRE = @PFECHA        
        
  SET @LMONTO_COMISION_DEVENGADA = 0    
  IF (SELECT FLG_CONSIDERA_COM_VC FROM CUENTAS WITH (NOLOCK) WHERE ID_CUENTA = @PID_CUENTA) = 'S'    
   BEGIN        
       SET @LMONTO_COMISION_DEVENGADA = dbo.FNT_COMISIONES_HONO_ASE_CUENTA$COMISIONES_DEVENGADAS(@PID_CUENTA, @PFECHA) +    
                                        dbo.FNT_COMI_FIJA_HONO_ASE_CUENTA$COMISIONES_DEVENGADAS(@PID_CUENTA,@PFECHA)    
   END   														

  IF    (SELECT COUNT(1) FROM SALDOS_ACTIVOS SA WITH (NOLOCK) WHERE SA.ID_CUENTA = @PID_CUENTA AND SA.FECHA_CIERRE = @PFECHA) = 0    
    AND (SELECT COUNT(1) FROM View_saldos_caja SC WITH (NOLOCK) WHERE SC.id_cuenta = @PID_CUENTA AND SC.FECHA_CIERRE = @PFECHA AND SC.MONTO_MON_CAJA <> 0) = 0    
-----------    
    AND (SELECT COUNT(1) FROM View_saldos_caja SC WITH (NOLOCK) WHERE SC.id_cuenta = @PID_CUENTA AND SC.FECHA_CIERRE = @PFECHA AND SC.MONTO_X_COBRAR_MON_CTA <> 0) = 0    
    AND (SELECT COUNT(1) FROM View_saldos_caja SC WITH (NOLOCK) WHERE SC.id_cuenta = @PID_CUENTA AND SC.FECHA_CIERRE = @PFECHA AND SC.MONTO_X_PAGAR_MON_CTA <> 0) = 0            
    AND (SELECT COUNT(1) FROM VIEW_MOV_CAJA_NO_RENTABILIDAD VMC WITH (NOLOCK) WHERE VMC.ID_CUENTA = @PID_CUENTA AND VMC.FECHA_MOVIMIENTO = @PFECHA AND VMC.COD_ESTADO <> 'A') = 0    
    AND (SELECT COUNT(1) FROM VIEW_MOV_ACTIVOS_NO_RENTAB VMC WITH (NOLOCK) WHERE VMC.ID_CUENTA = @PID_CUENTA AND VMC.FECHA_MOVIMIENTO = @PFECHA) = 0        
-----------    
    AND (SELECT COUNT(1) FROM SALDOS_ACTIVOS_INT SA WITH (NOLOCK) WHERE SA.ID_CUENTA = @PID_CUENTA AND SA.FECHA_CIERRE = @PFECHA) = 0    
    AND (SELECT COUNT(1) FROM VIEW_SALDOS_DERIVADOS SD WITH (NOLOCK) WHERE SD.ID_CUENTA = @PID_CUENTA AND SD.FECHA_CIERRE = @PFECHA AND SD.COD_INSTRUMENTO = 'FWD_NAC') = 0    
  BEGIN        
       SELECT @LSALDO_ACTIVO_MON_CUENTA = PC.SALDO_ACTIVO_MON_CUENTA        
            , @LVALOR_CUOTA_MON_CUENTA  = PC.VALOR_CUOTA_MON_CUENTA        
            , @LTOTAL_CUOTAS_MON_CUENTA  = PC.TOTAL_CUOTAS_MON_CUENTA        
         FROM PATRIMONIO_CUENTAS PC WITH (NOLOCK)        
        WHERE PC.id_cuenta = @PID_CUENTA         
          AND PC.FECHA_CIERRE = DATEADD(DAY, -1, @PFECHA)    

       SET @LPATRIMONIO_MON_CUENTA = (@LSALDO_ACTIVO_MON_CUENTA + @LSALDO_CAJA_MON_CUENTA +  @LMONTO_X_COBRAR_MON_CTA - @LMONTO_X_PAGAR_MON_CTA - @LMONTO_COMISION_DEVENGADA)                  

  END        
  ELSE          
  BEGIN        
        
       SET @LSALDO_ACTIVO_MON_CUENTA = ROUND(ISNULL((SELECT SUM(SA.MONTO_MON_CTA)         
                                                       FROM SALDOS_ACTIVOS SA WITH (NOLOCK)         
                                                      WHERE SA.ID_CUENTA = @PID_CUENTA     
                                                        AND SA.FECHA_CIERRE = @PFECHA), 0), @LDECIMALES_CTA)        
        
       --------------------------------------------------------------------------------------------        
       -- SE AGREGA LA SUMATORIA DE LOS SALDOS DE PERSHING/INVERSIS PARA EL CALCULO DE PATRIMONIO.        
       --------------------------------------------------------------------------------------------        
       DECLARE @LSALDOS_INTERNACIONAL FLOAT        
       SELECT @LSALDOS_INTERNACIONAL = ISNULL(SUM(dbo.PKG_TIPO_CAMBIO$FNT_CAMBIO_PARIDAD (S.ID_CUENTA        
                                                                                        , S.VALOR_MERCADO_MON_USD        
                                                                                        , @LID_MONEDA_USD        
                                                                                        , C.ID_MONEDA        
                                                                                        , S.FECHA_CIERRE        
                                                                                        , 'N')),0)        
         FROM SALDOS_ACTIVOS_INT S        
            , CUENTAS C WITH (NOLOCK)        
        WHERE S.ID_CUENTA    = @PID_CUENTA        
          AND S.FECHA_CIERRE = @PFECHA        
          AND C.ID_CUENTA    = S.ID_CUENTA        
        
       SET @LSALDO_ACTIVO_MON_CUENTA = @LSALDO_ACTIVO_MON_CUENTA + @LSALDOS_INTERNACIONAL            
       --------------------------------------------------------------------------------------------        
       -- SE AGREGA LA SUMATORIA DE FORWARDS        
       --------------------------------------------------------------------------------------------        
       DECLARE @LSALDOS_FORWARDS FLOAT        
       SELECT @LSALDOS_FORWARDS  = ISNULL(DBO.FNT_PKG_TIPO_CAMBIO$CAMBIO_PARIDAD(SD.ID_CUENTA        
                                                                               , SUM(SD.VMM)        
                                                                               , DBO.FNT_DAMEIDMONEDA('$$')        
                                                                               , C.ID_MONEDA      
                                                                               , @PFECHA) ,0)      
         FROM VIEW_SALDOS_DERIVADOS SD    
            , CUENTAS C  WITH (NOLOCK)            
        WHERE SD.ID_CUENTA       = @PID_CUENTA            
          AND SD.FECHA_CIERRE    = @PFECHA              
          AND SD.COD_INSTRUMENTO = 'FWD_NAC'            
          AND C.ID_CUENTA        = SD.ID_CUENTA    
        GROUP BY SD.ID_CUENTA,C.ID_MONEDA    
    
       SET @LSALDO_ACTIVO_MON_CUENTA = @LSALDO_ACTIVO_MON_CUENTA + ISNULL(@LSALDOS_FORWARDS,0)        

       --------------------------------------------------------------------------------------------          
       -- SE DESCUENTA SALDO SIMULTANEA  
       --------------------------------------------------------------------------------------------          
       DECLARE @LSALDOS_SIMULTANEA FLOAT          
        SELECT @LSALDOS_SIMULTANEA  = SUM(SA.TOTAL)  
          FROM SIMULTANEAS S ,   
               SIMULTANEAS_SALDO SA,   
               CUENTAS C  
        WHERE S.ID_CUENTA      = @PID_CUENTA              
          AND S.FCH_SIMULTANEA = @PFECHA  
          AND S.ID_CUENTA      = SA.ID_CUENTA               
          AND S.ID_SIMULTANEA  = SA.ID_SIMULTANEA  
          AND S.ID_CUENTA      = C.ID_CUENTA               
      
       SET @LSALDO_ACTIVO_MON_CUENTA = @LSALDO_ACTIVO_MON_CUENTA - ISNULL(@LSALDOS_SIMULTANEA,0)          
  
  
       --------------------------------------------------------------------------------------------          
       -- SE AGREGA MTM VENTA CORTA  
       -- El mark to market = (Precio Factura Venta Mercado - Precio Mercado) + Prima  
       --------------------------------------------------------------------------------------------          
       DECLARE @LVENTA_CORTA FLOAT      
 
        SELECT @LVENTA_CORTA = SUM(
                                   ISNULL((V.CANTIDAD * V.PRECIO_MEDIO), 0) -
                                   ISNULL((V.CANTIDAD * VCD.PRECIO_MEDIO_MERCADO), 0) - 
                                   ISNULL(VCD.PRIMA_ACUMULADA, 0)
                                   ) 
          FROM VENTA_CORTA V,  
               CUENTAS C,
               VENTA_CORTA_DEVENGO VCD
        WHERE V.ID_CUENTA      = @PID_CUENTA              
          AND V.ID_CUENTA      = C.ID_CUENTA               
          AND @PFECHA BETWEEN V.FECHA_DE_MVTO AND V.FECHA_VENCIMIENTO
          AND V.COD_ESTADO = 'C'
          AND V.ID_VTA_CORTA = VCD.ID_VTA_CORTA 
          AND V.FOLIO = VCD.FOLIO 
          AND V.FECHA_DE_MVTO = VCD.FECHA_DE_MVTO 
          AND VCD.FECHA_DE_DEVENGO = @PFECHA


       SET @LSALDO_ACTIVO_MON_CUENTA = @LSALDO_ACTIVO_MON_CUENTA + ISNULL(@LVENTA_CORTA,0)          
  
       --------------------------------------------------------------------------------------------          
       -- SE AGREGA LA SUMATORIA DE LOS PATRIMONIOS EXTRAS, ESTOS SE CONCIDERARAN EN EL CALCULO DE         
       -- PATRIMONIO.        
       -- HAF: 16/05/2009        
       --------------------------------------------------------------------------------------------        
       DECLARE @LPATRIM_CTAS_EXTRAS$MONTO_ACTIVO FLOAT        
             , @LPATRIM_CTAS_EXTRAS$MONTO_PASIVO FLOAT        
                    
       SELECT @LPATRIM_CTAS_EXTRAS$MONTO_ACTIVO = ISNULL(SUM(MONTO_ACTIVO), 0)        
            , @LPATRIM_CTAS_EXTRAS$MONTO_PASIVO = ISNULL(SUM(MONTO_PASIVO), 0)        
         FROM PATRIM_CTAS_EXTRAS WITH (NOLOCK)        
        WHERE ID_CUENTA      = @PID_CUENTA        
          AND FCH_PATRIMONIO = @PFECHA        
        
       --ENTONCES        
       SET @LSALDO_ACTIVO_MON_CUENTA = @LSALDO_ACTIVO_MON_CUENTA + (@LPATRIM_CTAS_EXTRAS$MONTO_ACTIVO - @LPATRIM_CTAS_EXTRAS$MONTO_PASIVO)    
       --------------------------------------------------------------------------------------------        
             
       SET @LPATRIMONIO_MON_CUENTA = (@LSALDO_ACTIVO_MON_CUENTA + @LSALDO_CAJA_MON_CUENTA +  @LMONTO_X_COBRAR_MON_CTA - @LMONTO_X_PAGAR_MON_CTA - @LMONTO_COMISION_DEVENGADA)    
    
  END    
    
  EXECUTE HAF_PROCESOCIERRE_PATRIMONIOSCUENTA_BUSCARNORENT @PID_CUENTA  = @PID_CUENTA    
                                                         , @PFECHA      = @PFECHA    
                                                         , @PAPORTE     = @LAPORTE_MON_CTA OUTPUT    
                                                         , @PRESCATE    = @LRESCATE_MON_CTA OUTPUT    
                                                         , @PID_USUARIO = @PID_USUARIO    
                                                         , @PRESULTADO  = @PRESULTADO OUTPUT    
  IF @PRESULTADO IS NOT NULL    
  BEGIN    
     RAISERROR(@PRESULTADO, 18, 1)    
  END    
       
  INSERT INTO PATRIMONIO_CUENTAS    
  (ID_CUENTA    
  ,FECHA_CIERRE    
  ,ID_MONEDA_CUENTA    
  ,SALDO_CAJA_MON_CUENTA    
  ,SALDO_ACTIVO_MON_CUENTA    
  ,PATRIMONIO_MON_CUENTA    
  ,ID_MONEDA_EMPRESA    
  ,SALDO_CAJA_MON_EMPRESA    
  ,SALDO_ACTIVO_MON_EMPRESA    
  ,PATRIMONIO_MON_EMPRESA    
  ,VALOR_CUOTA_MON_CUENTA    
  ,TOTAL_CUOTAS_MON_CUENTA    
  ,RENTABILIDAD_MON_CUENTA    
  ,APORTE_RETIROS_MON_CUENTA    
  ,ID_CIERRE    
  ,MONTO_X_COBRAR_MON_CTA    
  ,MONTO_X_PAGAR_MON_CTA    
  ,MONTO_X_COBRAR_MON_EMPRESA    
  ,MONTO_X_PAGAR_MON_EMPRESA    
  ,COMI_DEVENG_MON_CTA)    
  SELECT  @PID_CUENTA                            --<ID_CUENTA, numeric(10,0),>    
        , @PFECHA                                --<FECHA_CIERRE, datetime,>    
        , CTA.ID_MONEDA                          --<ID_MONEDA_CUENTA, numeric(4,0),>    
        , @LSALDO_CAJA_MON_CUENTA                --<SALDO_CAJA_MON_CUENTA, numeric(18,4),>    
        , @LSALDO_ACTIVO_MON_CUENTA              --<SALDO_ACTIVO_MON_CUENTA, numeric(18,4),>    
        , @LPATRIMONIO_MON_CUENTA                --<PATRIMONIO_MON_CUENTA, numeric(18,4),>    
        , E.ID_MONEDA                            --<ID_MONEDA_EMPRESA, numeric(4,0),>    
        , 0                                      --<SALDO_CAJA_MON_EMPRESA, numeric(18,4),>    
        , 0                                      --<SALDO_ACTIVO_MON_EMPRESA, numeric(18,4),>    
        , 0                                      --<PATRIMONIO_MON_EMPRESA, numeric(18,4),>    
        , @LVALOR_CUOTA_MON_CUENTA               --<VALOR_CUOTA _MON_CUENTA, numeric(18,4),>    
        , @LTOTAL_CUOTAS_MON_CUENTA              --<TOTAL_CUOTAS_MON_CUENTA, numeric(18,4),>    
        , 0                                      --<RENTABILIDAD_MON_CUENTA, numeric(18,4),>    
        , (@LAPORTE_MON_CTA - @LRESCATE_MON_CTA) --<APORTE_RETIROS_MON_CUENTA, numeric(18,4),>    
        , CC.ID_CIERRE                           --<ID_CIERRE, numeric(10,0),>    
        , @LMONTO_X_COBRAR_MON_CTA               --<MONTO_X_COBRAR_MON_CTA, numeric(18,4),>    
        , @LMONTO_X_PAGAR_MON_CTA                --<MONTO_X_PAGAR_MON_CTA, numeric(18,4),>    
        , 0                                      --<MONTO_X_COBRAR_MON_EMPRESA, numeric(18,4),>    
        , 0                                      --<MONTO_X_PAGAR_MON_EMPRESA, numeric(18,4),>    
        , @LMONTO_COMISION_DEVENGADA             --<COMI_DEVENG_MON_CTA, numeric(18,4),>    
     FROM CUENTAS CTA WITH (NOLOCK)    
    INNER JOIN CIERRES_CUENTAS CC WITH (NOLOCK) ON CC.ID_CUENTA = CTA.ID_CUENTA --CTA.ID_EMPRESA    
                                 AND CC.FECHA_CIERRE = @PFECHA    
    INNER JOIN EMPRESAS E         WITH (NOLOCK) ON E.ID_EMPRESA = CTA.ID_EMPRESA    
    WHERE CTA.ID_CUENTA = @PID_CUENTA    
        
  INSERT INTO #TMP_LOG(ID_USUARIO, PROCESO, MENSAJE)    
  VALUES (@PID_USUARIO, @LPROCESO, REPLICATE(CHAR(9), 2) + 'PATRIMONIOS CALCULADOS.')    
        
  SET @PRESULTADO = NULL    
END TRY    
BEGIN CATCH    
  SET @PRESULTADO =  '[' + ERROR_PROCEDURE() + ':' + CAST(ERROR_LINE() AS NVARCHAR) + ':' + CAST(ERROR_NUMBER() AS NVARCHAR) + ']' + CHAR(13) +    
                     ' ERROR=' + ERROR_MESSAGE()    
        
  INSERT INTO #TMP_LOG(ID_USUARIO, PROCESO, MENSAJE)    
  VALUES (@PID_USUARIO, @LPROCESO, @PRESULTADO)    
END CATCH    
SET NOCOUNT OFF
GO