IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_PERSHING$BuscaTransacciones_PSH]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
DROP PROCEDURE [DBO].[PKG_PERSHING$BuscaTransacciones_PSH]
GO

CREATE PROCEDURE [dbo].[PKG_PERSHING$BuscaTransacciones_PSH]
@pCuenta          VARCHAR (15) = NULL,
@pFecIni          VARCHAR(8),
@pFecFin          VARCHAR(8),
@pCodErr          INT           OUTPUT,
@pMsgErr          VARCHAR(4000) OUTPUT
AS
BEGIN
	SET NOCOUNT ON

    SET @pcoderr = 0
	SET @pmsgerr = ''

    CREATE TABLE  #TBLTRANSACCIONES_PSH (FECHA                DATETIME
                                       , CUENTA               VARCHAR(9)
                                       , CUSIP                VARCHAR(9)
                                       , FAMILIA_PRODUCTO     VARCHAR(20)
                                       , NOMBRE               VARCHAR(200)
                                       , NOMBRE_CORTO         VARCHAR(200)
                                       , MONEDA_TRADE         VARCHAR(3)
                                       , CANTIDAD             DECIMAL(18,5)
                                       , PRECIO_MONEDA_TRADE  DECIMAL(18,5)
                                       , COMPRA_VENTA         VARCHAR(1)
                                       , FECHA_SETTLEMENT     DATETIME
                                       , FECHA_PROCESO        DATETIME
                                       , PRINCIPAL            DECIMAL(18,3)
                                       , MONTO_NETO           DECIMAL(18,3)
                                       , COMISION             DECIMAL(18,2)
                                       , VALOR_REL_CONV       VARCHAR(30)
                                       , RUT_CLIENTE          VARCHAR(15)
                                       , ISIN                 VARCHAR(12)
                                       , CANCELADO            VARCHAR(1)
                                       , CORREGIDO            VARCHAR(2)
                                       , TIPO                 VARCHAR(2))

  BEGIN TRY

    INSERT INTO #TBLTRANSACCIONES_PSH
    SELECT T.FECHA                                                                                                      ,
           T.CUENTA                                                                                                     ,
           T.CUSIP                                                                                                      ,
           I.FAMILIA_PRODUCTO                                                                                           ,
           I.NOMBRE                                                                                                     ,
           NOMBRE_CORTO=I.NOMBRE_CORTO                                                                                  ,
           T.MONEDA_TRADE                                                                                               ,
           T.CANTIDAD                                                                                                   ,
           T.PRECIO_MONEDA_TRADE                                                                                        ,
           T.COMPRA_VENTA                                                                                               ,
           T.FECHA_SETTLEMENT                                                                                           ,
           T.FECHA_PROCESO                                                                                              ,
           T.PRINCIPAL                                                                                                  ,
           T.MONTO_NETO                                                                                                 ,
           T.COMISION                                                                                                   ,
           CONVERT(VARCHAR(8),T.FECHA,112) + '-' + T.CUENTA + '-' + CONVERT(VARCHAR(8),T.SECUENCIAL) AS VALOR_REL_CONV  ,
           C.RUT_CLIENTE                                                                                                ,
           I.ISIN                                                                                                       ,
           T.CANCELADO                                                                                                  ,
           T.CORREGIDO                                                                                                  ,
           'OP'
      FROM [LNKS-FM-P013].PERSHING.PERSHING.CLIENTES_PERSHING C
      LEFT JOIN [LNKS-FM-P013].PERSHING.PERSHING.PSH_TRANSACCIONES T ON (T.CUENTA = C.CUENTA_CLIENTE 
                                                                     AND T.FECHA BETWEEN @pfecini AND @pfecfin)
      LEFT OUTER JOIN (SELECT CUSIP, MAX(FECHA_ULTIMA_MODIFICACION) FECHA_ULTIMA_MODIFICACION
                         FROM [LNKS-FM-P013].PERSHING.PERSHING.PSH_INSTRUMENTOS
                        WHERE FECHA_ULTIMA_MODIFICACION <= @pfecfin
                        GROUP BY CUSIP ) MAX_I ON MAX_I.CUSIP = T.CUSIP
      LEFT OUTER JOIN (SELECT FAMILIA_PRODUCTO, NOMBRE, NOMBRE_CORTO, CUSIP, FECHA_ULTIMA_MODIFICACION, ISIN
                         FROM [LNKS-FM-P013].PERSHING.PERSHING.PSH_INSTRUMENTOS )  I ON I.CUSIP = T.CUSIP
                                                                                    AND I.FECHA_ULTIMA_MODIFICACION = MAX_I.FECHA_ULTIMA_MODIFICACION
     WHERE T.CUENTA = ISNULL(@pcuenta,T.CUENTA)
       AND T.FECHA  BETWEEN @pfecini AND @pfecfin
       AND C.ADC    = 'S'
     ORDER BY T.FECHA DESC

    INSERT INTO #TBLTRANSACCIONES_PSH
    SELECT T.FECHA                                                                                                      ,
           T.CUENTA                                                                                                     ,
           T.CUSIP                                                                                                      ,
           I.FAMILIA_PRODUCTO                                                                                           ,
           I.NOMBRE                                                                                                     ,
           NOMBRE_CORTO = I.NOMBRE_CORTO                                                                                ,
           T.MONEDA_TRADE                                                                                               ,
           T.CANTIDAD                                                                                                   ,
           CASE
           WHEN T.CANTIDAD <  0 THEN
              CASE
              WHEN T.VALOR_MERCADO < 0 THEN (T.VALOR_MERCADO / T.CANTIDAD)
              WHEN T.VALOR_MERCADO > 0 THEN ((T.VALOR_MERCADO * -1) / T.CANTIDAD)
              END
           WHEN T.CANTIDAD > 0 THEN
              CASE
              WHEN T.VALOR_MERCADO <  0 THEN ((T.VALOR_MERCADO * -1) / T.CANTIDAD)
              WHEN T.VALOR_MERCADO > 0 THEN (T.VALOR_MERCADO / T.CANTIDAD)
              END
           END 'PRECIO_MONEDA_TRADE'                                                                                    ,
           CASE
           WHEN RTRIM(LTRIM(T.COMPRA_VENTA)) = '' THEN
              CASE
              WHEN T.CANTIDAD <  0 THEN 'V'
              WHEN T.CANTIDAD >= 0 THEN 'C'
              END
           ELSE T.COMPRA_VENTA
           END 'COMPRA_VENTA'                                                                                           ,
           T.FECHA_SETTLEMENT                                                                                           ,
           T.FECHA_PROCESO                                                                                              ,
           CASE
           WHEN RTRIM(LTRIM(T.COMPRA_VENTA)) = '' THEN
              CASE
              WHEN T.CANTIDAD <  0 THEN
                 CASE
                 WHEN T.VALOR_MERCADO < 0 THEN T.VALOR_MERCADO
                 WHEN T.VALOR_MERCADO >= 0 THEN (T.VALOR_MERCADO * -1)
                 END
              WHEN T.CANTIDAD >= 0 THEN
                 CASE
                 WHEN T.VALOR_MERCADO <  0 THEN (T.VALOR_MERCADO * -1)
                 WHEN T.VALOR_MERCADO >= 0 THEN T.VALOR_MERCADO
                 END
              END
           ELSE
              CASE
              WHEN T.CANTIDAD < 0 THEN
                 CASE
                 WHEN T.VALOR_MERCADO < 0 THEN T.VALOR_MERCADO
                 WHEN T.VALOR_MERCADO >= 0 THEN (T.VALOR_MERCADO * -1)
                 END
              WHEN T.CANTIDAD >= 0 THEN
                 CASE
                 WHEN T.VALOR_MERCADO <  0 THEN (T.VALOR_MERCADO * -1)
                 WHEN T.VALOR_MERCADO >= 0 THEN T.VALOR_MERCADO
                 END
              END
           END 'PRINCIPAL'                                                                                              ,
           CASE
           WHEN RTRIM(LTRIM(T.COMPRA_VENTA)) = '' THEN
              CASE
              WHEN T.CANTIDAD <  0 THEN
                 CASE
                 WHEN T.VALOR_MERCADO < 0 THEN T.VALOR_MERCADO
                 WHEN T.VALOR_MERCADO >= 0 THEN (T.VALOR_MERCADO * -1)
                 END
              WHEN T.CANTIDAD >= 0 THEN
                 CASE
                 WHEN T.VALOR_MERCADO <  0 THEN (T.VALOR_MERCADO * -1)
                 WHEN T.VALOR_MERCADO >= 0 THEN T.VALOR_MERCADO
                 END
              END
           ELSE
              CASE
              WHEN T.CANTIDAD <  0 THEN
                 CASE
                 WHEN T.VALOR_MERCADO < 0 THEN T.VALOR_MERCADO
                 WHEN T.VALOR_MERCADO >= 0 THEN (T.VALOR_MERCADO * -1)
                 END
              WHEN T.CANTIDAD >= 0 THEN
                 CASE
                 WHEN T.VALOR_MERCADO <  0 THEN (T.VALOR_MERCADO * -1)
                 WHEN T.VALOR_MERCADO >= 0 THEN T.VALOR_MERCADO
                 END
              END
           END 'MONTO_NETO'                                                                                             ,
           T.COMISION                                                                                                   ,
           CONVERT(VARCHAR(8),T.FECHA,112) + '-' + T.CUENTA + '-' + CONVERT(VARCHAR(8),T.SECUENCIAL) AS VALOR_REL_CONV  ,
           C.RUT_CLIENTE                                                                                                ,
           I.ISIN                                                                                                       ,
           T.CANCELADO                                                                                                  ,
           T.CORREGIDO                                                                                                  ,
           'AR'
      FROM [LNKS-FM-P013].PERSHING.PERSHING.CLIENTES_PERSHING C
      LEFT JOIN [LNKS-FM-P013].PERSHING.PERSHING.PSH_MOVIMIENTOS T ON (T.CUENTA=C.CUENTA_CLIENTE AND T.FECHA BETWEEN @pfecini AND @pfecfin)
      LEFT OUTER JOIN (SELECT CUSIP, MAX(FECHA_ULTIMA_MODIFICACION) FECHA_ULTIMA_MODIFICACION
                         FROM [LNKS-FM-P013].PERSHING.PERSHING.PSH_INSTRUMENTOS
                        WHERE FECHA_ULTIMA_MODIFICACION <= @pfecfin
                        GROUP BY CUSIP ) MAX_I ON MAX_I.CUSIP = T.CUSIP
      LEFT OUTER JOIN (SELECT FAMILIA_PRODUCTO, NOMBRE, NOMBRE_CORTO, CUSIP, FECHA_ULTIMA_MODIFICACION, ISIN
                         FROM [LNKS-FM-P013].PERSHING.PERSHING.PSH_INSTRUMENTOS)  I ON I.CUSIP = T.CUSIP
                                                                                   AND I.FECHA_ULTIMA_MODIFICACION = MAX_I.FECHA_ULTIMA_MODIFICACION
     WHERE (T.FECHA BETWEEN @pfecini AND @pfecfin)
       AND T.CUENTA = ISNULL(@pcuenta,T.CUENTA)
       AND EXISTS (SELECT 1 FROM [LNKS-FM-P013].PERSHING.PERSHING.PSH_APORTES_RESCATES_CODES A
                    WHERE T.CODIGO_TIPO = A.CODIGOTIPO)
       AND NOT T.CUSIP LIKE '%99999%'
       AND NOT EXISTS (SELECT 1 FROM [LNKS-FM-P013].PERSHING.PERSHING.PSH_TRANSACCIONES S
                        WHERE T.CUENTA = S.CUENTA
                          AND T.FECHA = S.FECHA
                          AND T.SECUENCIAL = S.SECUENCIAL)

    SELECT FECHA
         , CUENTA
         , CUSIP
         , FAMILIA_PRODUCTO
         , NOMBRE
         , NOMBRE_CORTO
         , MONEDA_TRADE
         , CANTIDAD
         , PRECIO_MONEDA_TRADE
         , COMPRA_VENTA
         , FECHA_SETTLEMENT
         , FECHA_PROCESO
         , PRINCIPAL
         , MONTO_NETO
         , COMISION
         , VALOR_REL_CONV
         , RUT_CLIENTE
         , ISIN
         , CANCELADO
         , CORREGIDO
         , TIPO
      FROM #TBLTRANSACCIONES_PSH
     ORDER BY FECHA, CUSIP

  END TRY
  BEGIN CATCH
     SET @pCodErr = @@ERROR   
     SET @pMsgErr = 'Error en el procedimiento PKG_PERSHING$BuscaTransacciones_PSH: ' + ERROR_MESSAGE()
  END CATCH
  
  DROP TABLE #TBLTRANSACCIONES_PSH  

  SET NOCOUNT OFF
END
GO

GRANT EXECUTE ON [PKG_PERSHING$BuscaTransacciones_PSH] TO DB_EXECUTESP
GO