IF EXISTS (SELECT 1 FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[DBO].[PKG_SALDOS_ACTIVOS_INT$TestImportador_PSH_OPE]') AND OBJECTPROPERTY(ID, N'ISPROCEDURE') = 1)
DROP PROCEDURE [DBO].[PKG_SALDOS_ACTIVOS_INT$TestImportador_PSH_OPE]
GO

CREATE PROCEDURE [dbo].[PKG_SALDOS_ACTIVOS_INT$TestImportador_PSH_OPE]
( @PFECHA_CIERRE          DATETIME
, @PCUENTA                VARCHAR(50)
, @PCUSIP                 VARCHAR(30)
, @PFAMILIA_PRODUCTO      VARCHAR(50)
, @PNOMBRE                VARCHAR(100)
, @PNOMBRE_CORTO          VARCHAR(50)
, @PFECHA_TRANSAC         DATETIME
, @PMONEDA_TRANSAC        VARCHAR(10)
, @PCOMPRAVENTA           VARCHAR(1)
, @PCANTIDAD              FLOAT
, @PPRECIO                FLOAT
, @PFECHAOPERACION        DATETIME
, @PFECHALIQUIDACION      DATETIME
, @PMONTO_PAGO            FLOAT
, @PPORC_COMISION         FLOAT
, @PMONTO_OPERACION       FLOAT
, @PVALOR_REL_CONV        VARCHAR(100)
, @PRUT_CLIENTE           VARCHAR(15)
, @PISIN                  VARCHAR(20) = NULL
, @PCANCELADO             VARCHAR(1)  = NULL
, @PCORREGIDO             VARCHAR(1)  = NULL
)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @LRUT_CLIENTE             VARCHAR(12)  ,
            @LID_CUENTA               NUMERIC      ,
            @LNUM_CUENTA              VARCHAR(10)  ,
            @LCODRESULTADO            VARCHAR(5)   ,
            @LMSGRESULTADO            VARCHAR(800) ,
            @LCOD_ORIGEN_CUENTA       VARCHAR(800) ,
            @LCOD_ORIGEN_OPERACIONES  VARCHAR(800) ,
            @LID_NEMOTECNICO          NUMERIC      ,
            @LNEMOTECNICO             VARCHAR(50)  ,
            @LCOD_INSTRUMENTO         VARCHAR(15)  ,
            @LID_EMISOR_ESPECIFICO    NUMERIC      ,
            @LID_MONEDA               NUMERIC      ,
            @LDES_MONEDA              VARCHAR(10)  ,
            @LID_TIPO_CONVERSION      INT          ,
            @LID_OPE_DETALLE          NUMERIC      ,
            @LID_OPE                  NUMERIC      ,
            @LID_CAJA_CUENTA          NUMERIC      ,
            @LCOD_INST_SVS            VARCHAR(10)  ,
            @LID_INSTRUMENTO_SVS      NUMERIC

     SET @LMSGRESULTADO = ''
     SET @LCODRESULTADO = 'OK'
     ---------------------------------------------------------------------------------
  BEGIN TRY
     SET @PRUT_CLIENTE = LTRIM(RTRIM(@PRUT_CLIENTE))

     SELECT @LCOD_ORIGEN_CUENTA = ID_ORIGEN
       FROM ORIGENES
      WHERE COD_ORIGEN = 'PERSHING'

     SELECT @LCOD_ORIGEN_OPERACIONES = ID_ORIGEN
       FROM ORIGENES
      WHERE COD_ORIGEN = 'PERSHING'

     SELECT @LID_TIPO_CONVERSION  = ID_TIPO_CONVERSION
       FROM TIPOS_CONVERSION
      WHERE TABLA = 'OPERACIONES_DETALLE'
     ---------------------------------------------------------------------------------

     IF ISNULL(@PCANCELADO,'') = ''
        IF ISNULL((SELECT COUNT(1) FROM REL_CONVERSIONES WHERE ID_ORIGEN          = @LCOD_ORIGEN_OPERACIONES
                                                           AND ID_TIPO_CONVERSION = @LID_TIPO_CONVERSION
                                                           AND VALOR              = @pVALOR_REL_CONV),0) <> 0
        BEGIN
            SELECT @LID_OPE_DETALLE = ID_ENTIDAD
              FROM REL_CONVERSIONES
             WHERE ID_ORIGEN          = @LCOD_ORIGEN_OPERACIONES
               AND ID_TIPO_CONVERSION = @LID_TIPO_CONVERSION
               AND VALOR              = @pVALOR_REL_CONV

            SELECT @LID_OPE = ID_OPERACION
              FROM OPERACIONES_DETALLE
             WHERE ID_OPERACION_DETALLE = @LID_OPE_DETALLE

               SET @LCODRESULTADO = 'ERROR'
               SET @LMSGRESULTADO = 'ERROR: Registro ya fue creado, N� Operacion : ' + convert(varchar(20),@LID_OPE)
		END

     ---------------------------------------------------------------------------------
		IF ISNULL((SELECT COUNT(1) FROM VIEW_ALIAS WHERE TABLA     = 'MONEDAS'
                                                     AND ID_ORIGEN = @LCOD_ORIGEN_OPERACIONES
													 AND VALOR     = @PMONEDA_TRANSAC),0) <> 0
		BEGIN
			SELECT @LID_MONEDA  = ID_ENTIDAD, 
			       @LDES_MONEDA = VALOR
              FROM VIEW_ALIAS
             WHERE TABLA     = 'MONEDAS'
               AND ID_ORIGEN = @LCOD_ORIGEN_OPERACIONES
               AND VALOR     = @PMONEDA_TRANSAC
		END
		ELSE
		BEGIN
			SET @LCODRESULTADO = 'ERROR'
            SET @LMSGRESULTADO = 'ERROR: Moneda Externa no tiene Alias en GPI ' + @PMONEDA_TRANSAC
		END
    ---------------------------------------------------------------------------------
		IF ISNULL(@PFAMILIA_PRODUCTO,'') <> ''
		BEGIN
			SELECT @LCOD_INSTRUMENTO = CASE @PFAMILIA_PRODUCTO WHEN '(FIX INC)' THEN 'RF_INT_PSH'
                                                               WHEN '(EQUITY)'  THEN 'RV_INT_PSH'
                                                               WHEN '(OPTION)'  THEN 'RV_INT_PSH'
                                                               WHEN '(MUT FND)' THEN 'FFMM_INT_PSH' END
			IF ISNULL(@LCOD_INSTRUMENTO,'') = ''
			BEGIN
				SET @LCODRESULTADO = 'ERROR'
                SET @LMSGRESULTADO = 'ERROR: Familia de Productos no Registrada'
            END
		END
		ELSE
        BEGIN
			SET @LCODRESULTADO = 'ERROR'
			SET @LMSGRESULTADO = 'ERROR: Cuenta no asignada a Familia de Productos'
        END
   ---------------------------------------------------------------------------------
		IF @PRUT_CLIENTE IS NULL
			SET @PRUT_CLIENTE = ''

		IF ISNULL((SELECT COUNT(1) FROM VIEW_ALIAS WHERE TABLA     = 'CUENTAS'
                                                     AND ID_ORIGEN = @LCOD_ORIGEN_CUENTA
                                                     AND VALOR     = @PRUT_CLIENTE + '/' + @PCUENTA),0) <> 0
        BEGIN
			SELECT @LID_CUENTA = ID_ENTIDAD FROM VIEW_ALIAS WHERE TABLA     = 'CUENTAS'
			                                                  AND ID_ORIGEN = @LCOD_ORIGEN_CUENTA
                                                              AND VALOR     = @PRUT_CLIENTE + '/' + @PCUENTA
            
            SELECT @LNUM_CUENTA = NUM_CUENTA FROM CUENTAS WHERE ID_CUENTA = @LID_CUENTA
        END
		ELSE
        BEGIN
			SET @LCODRESULTADO = 'ERROR'
            SET @LMSGRESULTADO = 'ERROR: Cuenta Externa no tiene Alias en GPI ' + @PRUT_CLIENTE
        END
    ---------------------------------------------------------------------------------
		IF @LCODRESULTADO = 'OK'
		BEGIN
			IF ISNULL((SELECT COUNT(1) FROM CAJAS_CUENTA WHERE ID_CUENTA   = @LID_CUENTA
                                                           AND ID_MONEDA   = @LID_MONEDA
                                                           AND COD_MERCADO = 'I'),0) <> 0
            BEGIN
				SELECT @LID_CAJA_CUENTA = ID_CAJA_CUENTA
                  FROM CAJAS_CUENTA
                 WHERE ID_CUENTA   = @LID_CUENTA
                   AND ID_MONEDA   = @LID_MONEDA
                   AND COD_MERCADO = 'I'
            END
            ELSE
            BEGIN
				SET @LCODRESULTADO = 'ERROR'
                SET @LMSGRESULTADO = 'ERROR: Cuenta ' + @LNUM_CUENTA + ' no tiene Caja para moneda correspondiente'
            END
		END
    ---------------------------------------------------------------------------------
		IF @LCODRESULTADO = 'OK'
		BEGIN
			SELECT @LCOD_INST_SVS = CASE @LCOD_INSTRUMENTO WHEN 'RV_INT_PSH'   THEN 'ACE'
                                                           WHEN 'RF_INT_PSH'   THEN 'BEE'
                                                           WHEN 'FFMM_INT_PSH' THEN 'CFME'  END
            SELECT @LID_INSTRUMENTO_SVS = ID_ENTIDAD
              FROM VIEW_ALIAS
             WHERE COD_ORIGEN = 'PERSHING'
               AND TABLA      = 'INSTRUMENTOS_SVS'
               AND VALOR      = @LCOD_INST_SVS

			IF ISNULL((SELECT COUNT(1) FROM REL_NEMOTECNICO_ATRIBUTOS WHERE CUSIP = @PCUSIP),0) <> 0
            BEGIN
				IF ((SELECT COUNT(1) FROM REL_NEMOTECNICO_ATRIBUTOS WHERE CUSIP = @PCUSIP) > 1)
                BEGIN
					SET @LCODRESULTADO = 'ERROR_OTRO'
                    SET @LMSGRESULTADO = 'ERROR: CUSIP (' + @PCUSIP + ') se encuentra asociado a m�s de un nemot�cnico, por favor regularizar.'
                END
                ELSE
                BEGIN
					SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO                      ,
                           @LNEMOTECNICO    = NEMOTECNICO
                      FROM NEMOTECNICOS
                     WHERE ID_NEMOTECNICO = (SELECT ID_NEMOTECNICO
                                               FROM REL_NEMOTECNICO_ATRIBUTOS
                                              WHERE CUSIP = @PCUSIP)

                    UPDATE REL_NEMOTECNICO_ATRIBUTOS
                       SET ID_INSTRUMENTO_SVS = @LID_INSTRUMENTO_SVS,
                           ISIM               = @PISIN
                     WHERE ID_NEMOTECNICO = @LID_NEMOTECNICO
                       AND CUSIP          = @PCUSIP
                END
            END
            ELSE
            BEGIN
				IF ISNULL((SELECT COUNT(1) FROM REL_NEMOTECNICO_ATRIBUTOS WHERE ISIM = @PISIN),0) <> 0
                BEGIN
					IF ((SELECT COUNT(1) FROM REL_NEMOTECNICO_ATRIBUTOS WHERE ISIM = @PISIN) > 1)
                    BEGIN
						SET @LCODRESULTADO = 'ERROR_OTRO'
						SET @LMSGRESULTADO = 'ERROR: ISIN (' + @PISIN + ') se encuentra asociado a m�s de un nemot�cnico, por favor regularizar.'
                    END
                    ELSE
                    BEGIN
						SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO,
                               @LNEMOTECNICO    = NEMOTECNICO
                          FROM NEMOTECNICOS
                         WHERE ID_NEMOTECNICO = (SELECT ID_NEMOTECNICO
                                                   FROM REL_NEMOTECNICO_ATRIBUTOS
                                                   WHERE ISIM = @PISIN)

						UPDATE REL_NEMOTECNICO_ATRIBUTOS
						   SET ID_INSTRUMENTO_SVS = @LID_INSTRUMENTO_SVS,
                               CUSIP              = @PCUSIP
                         WHERE ID_NEMOTECNICO = @LID_NEMOTECNICO AND ISIM = @PISIN
                    END
                END
                ELSE
                BEGIN
					IF ISNULL((SELECT COUNT(1) FROM NEMOTECNICOS WHERE NEMOTECNICO = @PNOMBRE_CORTO),0) <> 0
					BEGIN
						SELECT @LID_NEMOTECNICO = ID_NEMOTECNICO,
                               @LNEMOTECNICO    = NEMOTECNICO
                          FROM NEMOTECNICOS
                         WHERE NEMOTECNICO = @PNOMBRE_CORTO

						IF ((SELECT COUNT(*) FROM REL_NEMOTECNICO_ATRIBUTOS WHERE ID_NEMOTECNICO = @LID_NEMOTECNICO) = 0)
                        BEGIN
							INSERT INTO REL_NEMOTECNICO_ATRIBUTOS (ID_NEMOTECNICO, ISIM, CUSIP)
                            VALUES (@LID_NEMOTECNICO, @PISIN, @PCUSIP)
                        END
                        ELSE
                        BEGIN
							UPDATE REL_NEMOTECNICO_ATRIBUTOS
							   SET CUSIP = @PCUSIP,
                                   ISIM  = @PISIN
                             WHERE ID_NEMOTECNICO = @LID_NEMOTECNICO
                        END
                    END
                    ELSE
                    BEGIN
						SELECT @LID_EMISOR_ESPECIFICO = ID_EMISOR_ESPECIFICO
                          FROM EMISORES_ESPECIFICO
                         WHERE COD_EMISOR_ESPECIFICO = CASE @LCOD_INSTRUMENTO WHEN 'RV_INT_PSH'   THEN 'ERVINT'
                                                                              WHEN 'RF_INT_PSH'   THEN 'ERFINT'
                                                                              WHEN 'FFMM_INT_PSH' THEN 'EFMINT' END

						EXEC PKG_NEMOTECNICOS$Guardar  @PID_NEMOTECNICO               = @LID_NEMOTECNICO OUTPUT
                                                     , @PCOD_INSTRUMENT               = @LCOD_INSTRUMENTO
                                                     , @PNEMOTECNICO                  = @PNOMBRE_CORTO
                                                     , @PID_MERCADO_TRANSACCION       = 22
                                                     , @PID_EMISOR_ESPECIFICO         = @LID_EMISOR_ESPECIFICO
                                                     , @PID_MONEDA                    = @LID_MONEDA
                                                     , @PID_TIPO_ESTADO               = 3
                                                     , @PCOD_ESTADO                   = 'V'
                                                     , @PDSC_NEMOTECNICO              = @PNOMBRE
                                                     , @PTASA_EMISION                 = NULL
                                                     , @PTIPO_TASA                    = NULL
                                                     , @PPERIODICIDAD                 = NULL
                                                     , @Pfecha_Vencimiento            = @PFECHA_TRANSAC
                                                     , @PCORTE_MINIMO_PAPEL           = 0
                                                     , @PMONTO_EMISION                = 0
                                                     , @PLIQUIDEZ                     = NULL
                                                     , @PBASE                         = NULL
                                                     , @PCOD_PAIS                     = 'OTR'
                                                     , @PID_EMISOR_ESPECIFICO_ORIGEN  = @LID_EMISOR_ESPECIFICO
                                                     , @PID_MONEDA_TRANSACCION        = @LID_MONEDA
                                                     , @PID_SUBFAMILIA                = NULL
                                                     , @PFLG_FUNGIBLE                 = NULL
                                                     , @PFECHA_EMISION                = @PFECHA_CIERRE
                                                     , @PFLG_TIPO_CUOTA_INGRESO       = NULL
                                                     , @PFLG_TIPO_CUOTA_EGRESO        = NULL
                                                     , @PID_USUARIO_INSERT            = 1
                                                     , @PID_USUARIO_UPDATE            = 1

                        IF @LID_NEMOTECNICO IS NULL
                        BEGIN
							SET @LMSGRESULTADO = 'ERROR: Nemot�cnico no pudo ser creado (' + @PNOMBRE_CORTO + ').'
                            SET @LCODRESULTADO = 'ERROR_OTRO'
                        END
                        ELSE
                        BEGIN
							INSERT INTO REL_NEMOTECNICO_ATRIBUTOS  (ID_NEMOTECNICO, ISIM, CUSIP, ID_INSTRUMENTO_SVS)
                            VALUES (@LID_NEMOTECNICO, @PISIN, @PCUSIP, @LID_INSTRUMENTO_SVS)

                            IF @LCOD_INSTRUMENTO = 'RF_INT_PSH'
                            BEGIN
								EXEC PKG_ASIGNA_RAMA_PSH_INVER  @LID_CUENTA      = @LID_CUENTA
                                                              , @LID_NEMOTECNICO = @LID_NEMOTECNICO
                            END
                        END
                    END
                END
            END
		END
        ---------------------------------------------------------------------------------
        IF @LCODRESULTADO = 'OK'
        BEGIN
			SELECT EM.DSC_EMPRESA                                                ,
                   @LID_CUENTA                                'ID_CUENTA'        ,
                   @LID_CAJA_CUENTA                           'ID_CAJA_CUENTA'   ,
                   CT.NUM_CUENTA                                                 ,
                   CL.RUT_CLIENTE                                                ,
                   @PCUSIP                                    'CUSIP'            ,
                   @LID_NEMOTECNICO                           'ID_NEMOTECNICO'   ,
                   (SELECT NEMOTECNICO
                      FROM NEMOTECNICOS
                     WHERE ID_NEMOTECNICO = @LID_NEMOTECNICO) 'NEMOTECNICO'      ,
                   NE.DSC_INSTRUMENTO                         'DSC_INSTRUMENTO'  ,
                   NE.DSC_PRODUCTO                            'DSC_PRODUCTO'     ,
                   @LID_MONEDA                                'ID_MONEDA'        ,
                   @LDES_MONEDA                               'MONEDA'           ,
                   CASE @PCompraVenta
				      WHEN 'V'  THEN 'Venta'
                      WHEN 'C'  THEN 'Compra'
                   END                                        'COMPRAVENTA'      ,
                   CASE
                      WHEN @pCantidad < 0 THEN
                         @pCantidad * -1
                      ELSE @pCantidad
                   END                                        'CANTIDAD'         ,
                   @pPrecio                                   'PRECIO'           ,
                   @pFechaOperacion                           'FECHAOPERACION'   ,
                   @pFechaLiquidacion                         'FECHALIQUIDACION' ,
                   CASE
                      WHEN @pMONTO_PAGO < 0 THEN
                         @pMONTO_PAGO * -1
                      ELSE @pMONTO_PAGO
                   END                                        'MONTO_PAGO'       ,
                   @pPORC_COMISION                            'COMISION'         ,
                   CASE
                      WHEN @pMONTO_OPERACION < 0 THEN
                         @pMONTO_OPERACION * -1
                      ELSE @pMONTO_OPERACION
                   END                                        'MONTO_OPERACION'  ,
                   @LCODRESULTADO                             'CODRESULTADO'     ,
                   @pVALOR_REL_CONV                           'VALOR_REL_CONV'   ,
                   @LMSGRESULTADO                             'MSGRESULTADO'     ,
                   @PCANCELADO                                'CANCELADO'        ,
                   @PCORREGIDO                                'CORREGIDO'
              FROM CUENTAS CT
                 , CLIENTES CL
                 , EMPRESAS EM              ,
                   (SELECT N.NEMOTECNICO
                         , I.DSC_INTRUMENTO DSC_INSTRUMENTO
                         , P.DSC_PRODUCTO
                      FROM NEMOTECNICOS N
                         , INSTRUMENTOS I
                         , PRODUCTOS P
                     WHERE I.COD_INSTRUMENTO = N.COD_INSTRUMENTO
                       AND P.COD_PRODUCTO    = I.COD_PRODUCTO
                       AND N.ID_NEMOTECNICO  = @LID_NEMOTECNICO) NE
             WHERE CT.ID_CUENTA  = @LID_CUENTA
               AND CL.ID_CLIENTE = CT.ID_CLIENTE
               AND EM.ID_EMPRESA = CT.ID_EMPRESA
		END
		ELSE
		BEGIN
			SELECT ''                                 'DSC_EMPRESA'     ,
                   0                                  'ID_CUENTA'       ,
                   0                                  'ID_CAJA_CUENTA'  ,
                   ''                                 'NUM_CUENTA'      ,
                   ''                                 'RUT_CLIENTE'     ,
                   @PCUSIP                            'CUSIP'           ,
                   0                                  'ID_NEMOTECNICO'  ,
                   ''                                 'NEMOTECNICO'     ,
                   ''                                 'DSC_INSTRUMENTO' ,
                   ''                                 'DSC_PRODUCTO'    ,
                   0                                  'ID_MONEDA'       ,
                   @PMONEDA_TRANSAC                   'MONEDA'          ,
                   CASE @PCompraVenta
                      WHEN 'V'  THEN 'Venta'
                      WHEN 'C'  THEN 'Compra'
                   END                                'COMPRAVENTA'     ,
                   CASE
                      WHEN @pCantidad < 0 THEN
					     @pCantidad * -1
                      ELSE @pCantidad
                   END                                'CANTIDAD'        ,
                   @pPrecio                           'PRECIO'          ,
                   @pFechaOperacion                   'FECHAOPERACION'  ,
                   @pFechaLiquidacion                 'FECHALIQUIDACION',
                   CASE
                      WHEN @pMONTO_PAGO < 0 THEN
                         @pMONTO_PAGO * -1
                      ELSE @pMONTO_PAGO    END        'MONTO_PAGO'      ,
                         @pPORC_COMISION              'COMISION'        ,
                   CASE
                      WHEN @pMONTO_OPERACION < 0 THEN
                         @pMONTO_OPERACION * -1
                      ELSE @pMONTO_OPERACION END      'MONTO_PAGO'      ,
                   @pVALOR_REL_CONV                   'VALOR_REL_CONV'  ,
                   @LCODRESULTADO                     'CODRESULTADO'    ,
                   @LMSGRESULTADO                     'MSGRESULTADO'    ,
                   @PCANCELADO                        'CANCELADO'       ,
                   @PCORREGIDO                        'CORREGIDO'
		END
----------------------------------------------------------------
  END TRY
  BEGIN CATCH
     SET @LCODRESULTADO = @@ERROR   
     SET @LMSGRESULTADO = 'Error en el procedimiento PKG_SALDOS_ACTIVOS_INT$TestImportador_PSH_OPE: ' + ERROR_MESSAGE()
  END CATCH

  SET NOCOUNT OFF

END
GO

GRANT EXECUTE ON [PKG_SALDOS_ACTIVOS_INT$TestImportador_PSH_OPE] TO DB_EXECUTESP
GO
