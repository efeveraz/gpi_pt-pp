﻿// jqgridExcelAsp.js
var JqGridAExcelASP = {
    jqGridAExcel: function (strJqgridNombre, strNombreExcel, strTitulo, strJqGrid_TODO) {
        var strSalida = "OK";
        var varJqgridColModel = $("#" + strJqgridNombre + "").getGridParam('colModel')
        var varJqgridColNames = $("#" + strJqgridNombre + "").getGridParam('colNames')
        var varDataJqgrid = $("#" + strJqgridNombre + "").jqGrid('getGridParam', 'data');
        varJqgridColModel.forEach(function (columna) {
            varDataJqgrid.forEach(function (item) {
                if (columna.hidden && strJqGrid_TODO != 'DATACOMPLETA') {
                    delete item[columna.name];
                }
                delete item["__type"];
            });
        });

        miSessionAjax.SetSessionVarObj('DataExcel', JSON.stringify(varDataJqgrid), function (data, err) {
            miSessionAjax.SetSessionVarObj('jqgridColModel', JSON.stringify(varJqgridColModel), function (data, err) {
                miSessionAjax.SetSessionVarObj('jqgridColMames', JSON.stringify(varJqgridColNames), function (data, err) {
                    miSessionAjax.SetSessionVar('ExcelNomArchivo', strNombreExcel);
                    miSessionAjax.SetSessionVar('ExcelTitulo', strTitulo);

                    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        url: "../Servicios/ExportarGenericoExcel.asmx/exportarExcel",
                        data: "{}",
                        datatype: "json",
                        type: "post",
                        contentType: "application/json; charset=utf-8",
                        complete: function (jsondata, stat) {
                            if (stat == "success") {
                                var mydata = JSON.parse(jsondata.responseText).d;
                                $('#loaderProceso').modal('hide');

                                var link = document.createElement("a");
                                link.href = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + mydata;

                                link.download = strNombreExcel;
                                link.target = "blank";

                                document.body.appendChild(link);
                                link.click();
                                document.body.removeChild(link);
                            }
                        }
                    });

                });
            });
        });
    },

    jqGridMantenedorComisionAdm: function (strArrayCuentas, strFecha, strFechaHasta, strNombreReporte, strNomFunction) {
        miSessionAjax.SetSessionVarObj('ArrayCuentas', JSON.stringify(strArrayCuentas), function (data, err) {
            miSessionAjax.SetSessionVar('FechaDesde', strFecha);
            miSessionAjax.SetSessionVar('FechaHasta', strFechaHasta);
            $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
            $.ajax({
                url: "../Servicios/ExportarExcelComisionAdm.asmx/" + strNomFunction,
                data: "{}",
                datatype: "json",
                type: "post",
                contentType: "application/json; charset=utf-8",
                complete: function (jsondata, stat) {
                    if (stat == "success") {
                        var mydata = JSON.parse(jsondata.responseText).d;
                        //window.open("data:application/pdf;base64," + mydata);

                        var link = document.createElement("a");
                        link.href = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + mydata;

                        //Set properties as you wise
                        link.download = strNombreReporte; //"ReporteComisionAdministracion";
                        link.target = "blank";

                        //this part will append the anchor tag and remove it after automatic click
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                        $('#loaderProceso').modal('hide');
                    }
                    //else {alertaColor(3, JSON.parse(jsondata.responseText).Message);}
                }
            });

        });
    },

    jqGridAExcelPatrimonioActivos: function (grillaDetallado, grillaResumen, strNombreExcel, tituloDetalle, tituloResumen) {
        var strSalida = "OK";
        var colModelDetallado = $("#" + grillaDetallado + "").getGridParam('colModel');
        var colNamesDetallado = $("#" + grillaDetallado + "").getGridParam('colNames');
        var colGroupsDetallado = $("#" + grillaDetallado + "").getGridParam('groupHeader')[0].groupHeaders;
        var dataDetallado = $("#" + grillaDetallado + "").jqGrid('getGridParam', 'data');

        var colModelResumen = $("#" + grillaResumen + "").getGridParam('colModel')
        var colNamesResumen = $("#" + grillaResumen + "").getGridParam('colNames')
        var colGroupsResumen = $("#" + grillaResumen + "").getGridParam('groupHeader')[0].groupHeaders;
        var dataResumen = $("#" + grillaResumen + "").jqGrid('getGridParam', 'data');

        colModelDetallado.forEach(function (columna) {
            dataDetallado.forEach(function (item) {
                if (columna.hidden) {
                    delete item[columna.name];
                }
                delete item["__type"];
            });
        });
        colModelResumen.forEach(function (columna) {
            dataResumen.forEach(function (item) {
                if (columna.hidden) {
                    delete item[columna.name];
                }
                delete item["__type"];
            });
        });

        miSessionAjax.SetSessionVarObj('dataResumen', JSON.stringify(dataResumen), function (data, err) {
            miSessionAjax.SetSessionVarObj('colModelResumen', JSON.stringify(colModelResumen), function (data, err) {
                miSessionAjax.SetSessionVarObj('colNamesResumen', JSON.stringify(colNamesResumen), function (data, err) {
                    miSessionAjax.SetSessionVarObj('dataDetallado', JSON.stringify(dataDetallado), function (data, err) {
                        miSessionAjax.SetSessionVarObj('colModelDetallado', JSON.stringify(colModelDetallado), function (data, err) {
                            miSessionAjax.SetSessionVarObj('colNamesDetallado', JSON.stringify(colNamesDetallado), function (data, err) {
                                miSessionAjax.SetSessionVarObj('colGroupsDetallado', JSON.stringify(colGroupsDetallado), function (data, err) {
                                    miSessionAjax.SetSessionVarObj('colGroupsResumen', JSON.stringify(colGroupsResumen), function (data, err) {
                                        miSessionAjax.SetSessionVar('ExcelNomArchivo', strNombreExcel);
                                        miSessionAjax.SetSessionVar('tituloDetalle', tituloDetalle);
                                        miSessionAjax.SetSessionVar('tituloResumen', tituloResumen);
                                        $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
                                        $.ajax({
                                            url: "../Servicios/ExportarExcelConPatAct.asmx/ExpData",
                                            data: "{}",
                                            datatype: "json",
                                            type: "post",
                                            contentType: "application/json; charset=utf-8",
                                            complete: function (jsondata, stat) {
                                                if (stat == "success") {
                                                    var mydata = JSON.parse(jsondata.responseText).d;
                                                    //window.open("data:application/pdf;base64," + mydata);

                                                    var link = document.createElement("a");
                                                    link.href = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + mydata;

                                                    //Set properties as you wise
                                                    link.download = strNombreExcel; //"ReporteComisionAdministracion";
                                                    link.target = "blank";

                                                    //this part will append the anchor tag and remove it after automatic click
                                                    document.body.appendChild(link);
                                                    link.click();
                                                    document.body.removeChild(link);
                                                    $('#loaderProceso').modal('hide');
                                                }
                                                //else {alertaColor(3, JSON.parse(jsondata.responseText).Message);}
                                            }
                                        });

                                    });
                                });
                            });
                        });
                    });
                });
            });
        });

    }
};

var DatatableAExcelASP = {

    DatatableAExcel: function (datatableData, datatableColModel, datatableColumn, datatableNomExcel, datatableTitulo) {

        var strSalida = "OK";

        datatableData.each(function (value, index) {
            delete value["__type"];
        });

        miSessionAjax.SetSessionVarObj('DataExcel', JSON.stringify(datatableData.toArray()), function (data, err) {
            miSessionAjax.SetSessionVarObj('jqgridColModel', JSON.stringify(datatableColModel), function (data, err) {
                miSessionAjax.SetSessionVarObj('jqgridColMames', JSON.stringify(datatableColumn), function (data, err) {
                    miSessionAjax.SetSessionVar('ExcelNomArchivo', datatableNomExcel);
                    miSessionAjax.SetSessionVar('ExcelTitulo', datatableTitulo);

                    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
                    $.ajax({
                        url: "../Servicios/ExportarGenericoExcel.asmx/exportarExcel",
                        data: "{}",
                        datatype: "json",
                        type: "post",
                        contentType: "application/json; charset=utf-8",
                        complete: function (jsondata, stat) {
                            if (stat == "success") {
                                var mydata = JSON.parse(jsondata.responseText).d;
                                $('#loaderProceso').modal('hide');

                                var link = document.createElement("a");
                                link.href = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," + mydata;

                                link.download = datatableNomExcel;
                                link.target = "blank";

                                document.body.appendChild(link);
                                link.click();
                                document.body.removeChild(link);
                            }
                        }
                    });
                });
            });
        });
    }

};