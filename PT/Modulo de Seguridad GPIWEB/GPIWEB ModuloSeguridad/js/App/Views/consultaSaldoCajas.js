﻿var jsfechaConsulta;

function resize() {
    var ancho = $(window).width();
    var height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - $("#paginador").outerHeight(true) - 143;

    if (ancho < 768) {
        $("#SaldoCajas").setGridWidth($("#panelDerecho").width() - 2);
        $("#SaldoCajas").setGridHeight(200);
    } else {
        $("#SaldoCajas").setGridWidth($("#panelDerecho").width() - 2);
        $("#SaldoCajas").jqGrid('setGridHeight', height);
    }
}

function initConsultaSaldoCajas() {
    $("#idSubtituloPaginaText").text("Consulta Saldo Cajas");
    document.title = "Consulta Saldo Cajas";

    if (!miInformacionCuenta) {
        $(".input-group.date").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
    } else {
        $(".input-group.date").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
    }

    $("#tabsClienteCuentaGrupo").hide();
    $('.nav-tabs a[href="#tabCuenta"]').tab('show');
}
function cargarEventHandlersConsultaSaldoCajas() {
    $(window).bind('resize', function () {
        resize()
    }).trigger('resize');

    $(".input-group.date").datepicker();

    $("#SaldoCajas").jqGrid({
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        scrollOffset: 0,
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID_CAJA_CUENTA" },
        colNames: ['ID_CAJA_CUENTA', 'Cuenta', 'Empresa', 'Rut', 'Nombre Cliente', 'Nombre Asesor', 'Moneda Caja', 'Nombre Caja', 'Saldo Disponible', 'Saldo Contable'],
        colModel: [
            { name: "ID_CAJA_CUENTA", index: "ID_CAJA_CUENTA", sortable: true, width: 100, sorttype: "int", formatter: "int", editable: false, search: true, hidden: true, align: "left" },
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 100, sorttype: "text", formatter: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "DSC_EMPRESA", index: "DSC_EMPRESA", sortable: true, width: 200, sorttype: "text", formatter: "text", editable: false, search: true, hidden: true, align: "left" },
            { name: "RUT_CLIENTE", index: "RUT_CLIENTE", sortable: true, width: 100, sorttype: "text", formatter: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "NOMBRE_CLIENTE", index: "NOMBRE_CLIENTE", sortable: true, width: 300, sorttype: "text", formatter: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "NOMBRE", index: "NOMBRE", sortable: true, width: 200, sorttype: "text", formatter: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "DSC_MONEDA", index: "DSC_MONEDA", sortable: true, width: 100, sorttype: "text", formatter: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "DSC_CAJA_CUENTA", index: "DSC_CAJA_CUENTA", sortable: true, width: 100, sorttype: "text", formatter: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "MONTO_MON_CAJA", index: "MONTO_MON_CAJA", sortable: true, width: 130, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } },
            { name: "SALDO_PENDIENTE", index: "SALDO_PENDIENTE", sortable: true, width: 120, sorttype: "number", formatter: "number", editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] } }
        ],
        pager: '#paginador',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        styleUI: 'Bootstrap',
        sortname: "NUM_CUENTA",
        sortorder: "asc",
        caption: "Saldo Cajas",
        ignoreCase: true,
        hidegrid: false,
        grouping: false,
        regional: localeCorto,
        groupingView: {
            groupField: ['FECHA_CIERRE'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        },
        gridComplete: function () {
            var ids = $("#SaldoCajas").jqGrid("getDataIDs"), l = ids.length, i, rowid, status;
            var rowid = "";
        }
    });

    $("#SaldoCajas").jqGrid().bindKeys().scrollingRows = true
    $("#SaldoCajas").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#SaldoCajas").jqGrid('navGrid', '#paginador', { add: false, edit: false, del: false, excel: false, search: false });

    $("#SaldoCajas").jqGrid('navButtonAdd', '#paginador', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('SaldoCajas', 'Saldo Cajas', 'Saldo Cajas', '');
        }
    });
    $('#paginador_left').width('auto');

    $("#BtnConsultar").button().click(function () {
        consultaSaldoCajas();
    });
}
function consultaSaldoCajas() {
    var jsfechaFormato = moment.utc($(".input-group.date").datepicker('getDate')).startOf('day').toISOString();
    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1);

    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }

    $("#SaldoCajas").jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        url: "ConsultaSaldoCajas.aspx/ConsultaSaldoCajas",
        data: "{'fechaConsulta':'" + jsfechaConsulta +
        "', 'idCuenta':" + (miInformacionCuenta == null ? "''" : miInformacionCuenta.IdCuenta) + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#SaldoCajas").setGridParam({ data: mydata });
                $("#SaldoCajas").trigger("reloadGrid");
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
}

$(document).ready(function () {
    initConsultaSaldoCajas();
    cargarEventHandlersConsultaSaldoCajas();
    $(window).trigger('resize');
});