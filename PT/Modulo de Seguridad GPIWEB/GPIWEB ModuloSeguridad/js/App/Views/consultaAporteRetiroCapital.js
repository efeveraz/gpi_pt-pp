﻿var jsfechaConsulta;
var jsfechaConsultaHasta;
var jsfechaFormato;
var jsfechaFormatoHasta;

function resize() {
    var height;
    var ancho = $(window).width();
    height = $("#content").outerHeight(true) - $("#filtroClienteCuentaGrupo").outerHeight(true) - $("#encSubPantalla").outerHeight(true) - 300;
    if (ancho < 768) {
        $("#PatrimonioYActivosDetallado").setGridHeight(170);
        $("#PatrimonioYActivosResumen").setGridHeight(170);
    } else {
        $("#PatrimonioYActivosDetallado").setGridHeight(height);
        $("#PatrimonioYActivosResumen").setGridHeight(height);
    }
}


function initConsultaAporteRetiroCapital() {
    $("#idSubtituloPaginaText").text("Consulta de Aporte Retiro Capital");
    document.title = "Consulta de Aporte Retiro Capital";

    cargarMonedas();

    if (!miInformacionCuenta) {
        $("#dtFechaConsultaHasta").datepicker('setDate', new Date(localStorage.UltimaFechaCierre));
        $("#dtFechaConsulta").datepicker('setDate', moment(new Date(localStorage.UltimaFechaCierre)).add(-1, 'months').toDate());
    } else {
        $("#dtFechaConsultaHasta").datepicker('setDate', moment(miInformacionCuenta.UltimaFechaConsulta).format('L'));
        $("#dtFechaConsulta").datepicker('setDate', moment($("#dtFechaConsultaHasta").datepicker('getDate')).add(-1, 'months').format('L'));
    }
}


function cargarEventHandlersCARetiroCapital() {
    $("#dtRangoFecha").datepicker();

    $(window).bind('resize', function () {
        resize();
    }).trigger('resize');

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $(window).trigger('resize');
    });

    $("#tabsClienteCuentaGrupo").hide();
    $('.nav-tabs a[href="#tabCuenta"]').tab('show');

    $("#PatrimonioYActivosDetallado").jqGrid({
        shrinkToFit: false,
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "ID_CUENTA" },
        colNames: ['Apo Ini', 'Fecha Movimiento', 'Cuenta', 'Rut', 'Cliente', 'Asesor', 'Aporte', 'Retiro', 'Observación'],
        colModel: [
            { name: "FLG_PRIMERAPORTE", index: "FLG_PRIMERAPORTE", sortable: true, width: 60, sorttype: "int", formatter: "checkbox", editable: false, search: true, hidden: false, align: "center" },
            { name: "FECHA_MOVIMIENTO", index: "FECHA_MOVIMIENTO", sortable: true, width: 130, sorttype: "date", formatter: "date", editable: false, search: true, hidden: false, align: "center", searchoptions: { sopt: ['eq'], dataInit: datePick, attr: { title: 'Select Date', type: "date" } } },
            { name: "NUM_CUENTA", index: "NUM_CUENTA", sortable: true, width: 70, sorttype: "int", formatter: "int", editable: false, search: true, hidden: false, align: "right" },
            { name: "RUT_CLIENTE", index: "RUT_CLIENTE", sortable: true, width: 80, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "NOMBRE_CLIENTE", index: "NOMBRE_CLIENTE", sortable: true, width: 190, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "NOMBRE_ASESOR", index: "NOMBRE_CLIENTE", sortable: true, width: 140, sorttype: "text", editable: false, search: true, hidden: false },
            { name: "MONTO_APORTE", index: "MONTO_APORTE", sortable: true, width: 110, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 0 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] }, summaryType: 'sum' },
            { name: "MONTO_RETIRO", index: "MONTO_RETIRO", sortable: true, width: 110, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 0 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] }, summaryType: 'sum' },
            { name: "OBSERVACION", index: "OBSERVACION", sortable: true, width: 150, sorttype: "text", editable: false, search: true, hidden: false }
        ],
        pager: '#Paginador',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        height: "50%",
        styleUI: 'Bootstrap',
        sortname: "NUM_CUENTA",
        sortorder: "asc",
        caption: "Detalle",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: false,
        footerrow: true,
        loadComplete: function () {
            var $self = $(this),
                sum = $self.jqGrid("getCol", "MONTO_APORTE", false, "sum");
            $self.jqGrid("footerData", "set", { NOMBRE_ASESOR: "Total:", MONTO_APORTE: sum });

            var $self2 = $(this),
                sum2 = $self2.jqGrid("getCol", "MONTO_RETIRO", false, "sum");
            $self2.jqGrid("footerData", "set", { MONTO_RETIRO: sum2 });
        },
        groupingView: {
            groupField: ['NUM_CUENTA'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupColumnShow: [false],
            groupSummary: [true],
            showSummaryOnHide: true
        }
    });
    $("#PatrimonioYActivosDetallado").jqGrid().bindKeys().scrollingRows = true
    $("#PatrimonioYActivosDetallado").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#PatrimonioYActivosDetallado").jqGrid('navGrid', '#Paginador', { add: false, edit: false, del: false, excel: false, search: false });
    $("#PatrimonioYActivosDetallado").jqGrid('navButtonAdd', '#Paginador', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('PatrimonioYActivosDetallado', 'Patrimonio y Activos Detallado', 'Patrimonio y Activos Detallado', '');
        }
    });

    $("#PatrimonioYActivosResumen").jqGrid({
        loadonce: true,
        datatype: 'local',
        jsonReader: { repeatitems: false, root: "d.rows", page: "d.page", total: "d.total", records: "d.records", id: "NOMBRE_ASESOR" },
        colNames: ['Asesor', 'Aporte', 'Retiro', 'Neto'],
        colModel: [
            { name: "NOMBRE_ASESOR", index: "NOMBRE_ASESOR", sortable: true, width: 50, sorttype: "text", formatter: "text", editable: false, search: true, hidden: false, align: "left" },
            { name: "MONTO_APORTE", index: "MONTO_APORTE", sortable: true, width: 50, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 0 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] }, summaryType: 'sum' },
            { name: "MONTO_RETIRO", index: "MONTO_RETIRO", sortable: true, width: 110, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 0 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] }, summaryType: 'sum' },
            { name: "MONTO_NETO", index: "MONTO_NETO", sortable: true, width: 110, sorttype: "number", formatter: "number", formatoptions: { decimalPlaces: 0 }, editable: false, search: true, hidden: false, align: "right", searchoptions: { sopt: ['eq', 'ne', 'le', 'lt', 'gt', 'ge'] }, summaryType: 'sum' }
        ],
        pager: '#PaginadorResumen',
        loadtext: 'Cargando datos...',
        recordtext: "{0} - {1} de {2} elementos",
        emptyrecords: 'No hay resultados',
        pgtext: 'Pág: {0} de {1}',
        rowNum: intGrillaFilasPagina,
        rowList: arrGrillaFilasHoja,
        viewrecords: true,
        multiselect: false,
        autowidth: true,
        responsive: true,
        height: 'auto',
        styleUI: 'Bootstrap',
        sortname: "NOMBRE_ASESOR",
        sortorder: "asc",
        caption: "Resumen",
        ignoreCase: true,
        hidegrid: false,
        regional: localeCorto,
        grouping: false,
        footerrow: true,
        groupingView: {
            groupField: ['NOMBRE_ASESOR'],
            groupText: ['<b>{0} - {1} Item(s)</b>'],
            groupSummary: [true]
        },
        loadComplete: function () {
            var $self1 = $(this),
                sum1 = $self1.jqGrid("getCol", "MONTO_APORTE", false, "sum");
            $self1.jqGrid("footerData", "set", { NOMBRE_ASESOR: "Total:", MONTO_APORTE: sum1 });

            var $self2 = $(this),
                sum2 = $self2.jqGrid("getCol", "MONTO_RETIRO", false, "sum");
            $self2.jqGrid("footerData", "set", { MONTO_RETIRO: sum2 });

            var $self3 = $(this),
                sum3 = $self3.jqGrid("getCol", "MONTO_NETO", false, "sum");
            $self3.jqGrid("footerData", "set", { MONTO_NETO: sum3 });
        },
        gridComplete: function () {
            var ids = $("#PatrimonioYActivosResumen").jqGrid("getDataIDs"), l = ids.length, i, rowid, status;
            var rowid = "";
        }
    });
    $("#PatrimonioYActivosResumen").jqGrid().bindKeys().scrollingRows = true
    $("#PatrimonioYActivosResumen").jqGrid('filterToolbar', { searchOperators: true, stringResult: true, searchOnEnter: false, defaultSearch: "cn", ignoreCase: true });
    $("#PatrimonioYActivosResumen").jqGrid('navGrid', '#PaginadorResumen', { add: false, edit: false, del: false, excel: false, search: false });
    $("#PatrimonioYActivosResumen").jqGrid('navButtonAdd', '#PaginadorResumen', {
        caption: "Excel", buttonicon: "ui-icon-plusthick", title: "Exporta la informacion visible en pantalla a un archivo Excel",
        onClickButton: function () {
            JqGridAExcelASP.jqGridAExcel('PatrimonioYActivosResumen', 'Patrimonio y Activos Resumen', 'Patrimonio y Activos Resumen', '');
        }
    });

    $("#BtnConsultar").button().click(function () {
        $('#PatrimonioYActivosDetallado').jqGrid('clearGridData');
        consultaOperaciones();
        resize();
    });

    $("#DDMoneda :selected").text();
    $('#DDMoneda').val();

    $('#DDMoneda').on('change', () => {
        if ($("#DDMoneda").val() === '1') {
            $('#PatrimonioYActivosDetallado').setColProp('MONTO_APORTE', { formatoptions: { decimalPlaces: 0 } });
            $('#PatrimonioYActivosDetallado').setColProp('MONTO_RETIRO', { formatoptions: { decimalPlaces: 0 } });
            $('#PatrimonioYActivosResumen').setColProp('MONTO_APORTE', { formatoptions: { decimalPlaces: 0 } });
            $('#PatrimonioYActivosResumen').setColProp('MONTO_RETIRO', { formatoptions: { decimalPlaces: 0 } });
            $('#PatrimonioYActivosResumen').setColProp('MONTO_NETO', { formatoptions: { decimalPlaces: 0 } });
        } else {
            $('#PatrimonioYActivosDetallado').setColProp('MONTO_APORTE', { formatoptions: { decimalPlaces: 2 } });
            $('#PatrimonioYActivosDetallado').setColProp('MONTO_RETIRO', { formatoptions: { decimalPlaces: 2 } });
            $('#PatrimonioYActivosResumen').setColProp('MONTO_APORTE', { formatoptions: { decimalPlaces: 2 } });
            $('#PatrimonioYActivosResumen').setColProp('MONTO_RETIRO', { formatoptions: { decimalPlaces: 2 } });
            $('#PatrimonioYActivosResumen').setColProp('MONTO_NETO', { formatoptions: { decimalPlaces: 2 } });
        }
    });
}

function cargarMonedas() {
    $.ajax({
        url: "../Servicios/Globales.asmx/consultaMonedas",
        data: "{}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat === "success") {
                var res = JSON.parse(jsondata.responseText).d;
                $("#DDMoneda").empty();
                $.each(res, function (key, val) {
                    $("#DDMoneda").append('<option value="' + val.ID_MONEDA + '">' + capitalizeEachWord(val.DSC_MONEDA) + '</option>');
                });
            } else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
        }
    });
}

function consultaOperaciones() {
    var jsidMoneda = $("#DDMoneda").val();
    jsfechaFormato = moment.utc($("#dtFechaConsulta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaFormatoHasta = moment.utc($("#dtFechaConsultaHasta").datepicker('getDate')).startOf('day').toISOString();
    jsfechaConsulta = jsfechaFormato.substring(0, jsfechaFormato.length - 1)
    jsfechaConsultaHasta = jsfechaFormatoHasta.substring(0, jsfechaFormatoHasta.length - 1)

    if (jsfechaConsulta === "") {
        alert("No ha indicado fecha de consulta desde");
        return;
    }
    if (jsfechaConsultaHasta === "") {
        alert("No ha indicado fecha de consulta hasta");
        return;
    }

    $("#PatrimonioYActivosDetallado").jqGrid('clearGridData');
    $('#loaderProceso').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        url: "ConsultaAporteRetiroCapital.aspx/ConsultaAporteRetiroCapitalDetalle",
        data: "{'fechaConsulta':'" + jsfechaConsulta +
        "', 'fechaConsultaHasta':'" + jsfechaConsultaHasta +
        "', 'idCuenta':" + (miInformacionCuenta == null ? "''" : miInformacionCuenta.IdCuenta) +
        ", 'idMoneda':" + jsidMoneda + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#PatrimonioYActivosDetallado").setGridParam({ data: mydata });
                $("#PatrimonioYActivosDetallado").trigger("reloadGrid");
                if (!mydata.length) {
                    alertaColor(1, "No se encontraron registros.");
                }
            }
            else {
                alertaColor(3, JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
    $("#PatrimonioYActivosResumen").jqGrid('clearGridData');
    $.ajax({
        url: "ConsultaAporteRetiroCapital.aspx/ConsultaAporteRetiroCapitalResumen",
        data: "{'fechaConsulta':'" + jsfechaConsulta +
        "', 'fechaConsultaHasta':'" + jsfechaConsultaHasta +
        "', 'idCuenta':" + (miInformacionCuenta == null ? "''" : miInformacionCuenta.IdCuenta) +
        ", 'idMoneda':" + jsidMoneda + "}",
        datatype: "json",
        type: "post",
        contentType: "application/json; charset=utf-8",
        complete: function (jsondata, stat) {
            if (stat == "success") {
                var mydata = JSON.parse(jsondata.responseText).d;
                $("#PatrimonioYActivosResumen").setGridParam({ data: mydata });
                $("#PatrimonioYActivosResumen").trigger("reloadGrid");
            }
            else {
                alert(JSON.parse(jsondata.responseText).Message);
            }
            $('#loaderProceso').modal('hide');
        }
    });
};


$(document).ready(function () {
    initConsultaAporteRetiroCapital();
    cargarEventHandlersCARetiroCapital();
    resize();
});