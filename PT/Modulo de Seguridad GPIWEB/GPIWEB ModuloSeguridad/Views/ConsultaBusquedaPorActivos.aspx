﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb.master" AutoEventWireup="false" 
    CodeBehind="ConsultaBusquedaPorActivos.aspx.vb" Inherits="AplicacionWeb.ConsultaBusquedaPorActivos" %>

<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPalFiltro" runat="Server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/consultaBusquedaPorActivos.min.js") %>'></script>
    <link href='<%= VersionLinkHelper.GetVersion("../Estilos/vendor/select2/select2.min.css") %>' rel="stylesheet" />
    <link href='<%= VersionLinkHelper.GetVersion("../Estilos/vendor/select2/select2-bootstrap.min.css") %>' rel="stylesheet" />
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/select2/select2.full.min.js") %>'></script>
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/select2/i18n/es.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion row top-buffer">
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <select id="DDInstrumento" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Instrumento">
                    </select>
                </div>
                <div class="form-group select2-width">
                    <select id="DDNemotecnico" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Nemotécnico" style="width: 100%;"> <!--style="min-width: 156px; max-width: 240px;"-->
                        <option value="-1">Seleccione Nemotécnicos</option>
                    </select>
                </div>
                <div class="form-group">
                    <div class="input-group date">
                        <input type="text" id="dtFechaConsulta" class="form-control" style="text-align: center;">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" id="BtnConsultar" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Seleccione Nemotécnico">Consultar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row top-buffer">
        <div class="col-sm-12">
            <div id="divGrilla" style="height: auto" class="DivCentradosGrillaCompleta">
                <table id="ListaBusAct">
                    <tr>
                        <td></td>
                    </tr>
                </table>
                <div id="Paginador">
                </div>
            </div>
        </div>
    </div>
</asp:Content>