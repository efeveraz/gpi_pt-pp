﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Sistema/GPIWeb.Master"
    CodeBehind="MantenedorRoles.aspx.vb" Inherits="AplicacionWeb.MantenedorRoles" %>

<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContenidoPPalFiltro" runat="server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/mantenedorRoles.min.js") %>'></script>
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/bootstrap-treeview.js") %>'></script>
    <link href='<%= VersionLinkHelper.GetVersion("../js/vendor/DataTables/datatables.min.css") %>' rel="stylesheet" />
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/DataTables/datatables.min.js") %>'></script>

    <div class="row">
        <div class="col-xs-12" id="panelListaRoles">
            <div class="navbar navbar-default">
                <div class="col-xs-12">
                    <ul class='nav nav-pills'>
                        <li>
                            <button type="button" id="btnNuevoRol" class="btn btn-default navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Nuevo'><i class='fa fa-plus'></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnEditarRol" class='btn btn-default navbar-btn fade hidden' data-toggle='tooltip' data-placement='bottom' data-original-title='Editar'><i class='fa fa-pencil'></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnEliminar" class='btn btn-danger navbar-btn fade hidden' data-toggle='tooltip' data-placement='bottom' data-original-title='Eliminar'><i class='fa fa-trash'></i></button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default mb-0">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12" id="tablaRoles">
                            <div class='table-responsive'>
                                <table class='table table-striped table-hover table-bordered table-condensed display nowrap' id='ListaRoles' width="100%">
                                    <thead style="display: table-header-group">
                                        <tr>
                                            <th></th>
                                            <th>Nombre Rol</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="panelRol" class="col-sm-12 hidden">
            <div class="navbar navbar-default">
                <div class="col-xs-12">
                    <ul class='nav nav-pills'>
                        <li>
                            <button type="button" id="btnGuardarNuevoRol" class="btn btn-primary navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Guardar'><i class="fa fa-floppy-o"></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnCancelar" class="btn btn-danger navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Cancelar'><i class="fa fa-times"></i></button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="panel panel-default mb-0">
                        <div id="tituloAgregar" class="panel-heading ">Agregar Nuevo Rol</div>
                        <div id="tituloEditar" class="panel-heading ">Editar Rol</div>
                        <div class="panel-body">
                            <div id="divTxtNombreRol" class="form-group-sm">
                                <label class="control-label" for="txtNombreRol">Nombre Rol</label>
                                <input type="text" id="txtNombreRol" maxlength="50" class="form-control input-sm" required>
                                <span id="lblErrNomRol" class="label label-danger hidden"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="panel panel-default mb-0">
                        <div id="tituloPermiso" class="panel-heading ">Permisos</div>
                        <div id="panelArbol" class="panel-body" style="overflow-y: auto;">
                            <div id="arbol">
                                <div id="treeview-checkable" class=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
