﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb_filtroCuenta.master" AutoEventWireup="false"
    CodeBehind="MantenedorComisionesTransaccionales.aspx.vb" Inherits="AplicacionWeb.MantenedorComisionesTransaccionales" %>

<%@ MasterType VirtualPath="~/Sistema/GPIWeb_filtroCuenta.master" %>
<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPal" runat="Server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/mantenedorComisionesTransaccionales.min.js") %>'></script>

    <link href='<%= VersionLinkHelper.GetVersion("../js/vendor/DataTables/datatables.min.css") %>' rel="stylesheet" />
    <script src='<%= VersionLinkHelper.GetVersion("../js/vendor/DataTables/datatables.min.js") %>'></script>

    <div class="row">
        <div id="panelPrincipal" class="col-xs-12 top-buffer">
            <div class="navbar navbar-default">
                <div class="col-xs-12">
                    <ul class='nav nav-pills'>
                        <li>
                            <button type="button" id="btnConsultar" class="btn btn-primary navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Seleccione Cuenta o Todas'>Consultar</button>
                        </li>
                        <li>
                            <div class="checkbox" style="margin-top: 15px;">
                                &nbsp;
                                <input id="chkTodos" type="checkbox">
                                <label for="chkTodos" style="padding-left: 5px;">Todas</label>
                                &nbsp;&nbsp;
                            </div>
                        </li>
                        <li>
                            <button type="button" id="btnHabilitaAgregar" class="btn btn-default navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Nuevo'><i class='fa fa-plus'></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnHabilitaEditar" class='btn btn-default navbar-btn fade hidden' data-toggle='tooltip' data-placement='bottom' data-original-title='Editar'><i class='fa fa-pencil'></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnEliminar" class="btn btn-danger navbar-btn fade hidden" data-toggle='tooltip' data-placement='bottom' data-original-title='Eliminar'><i class='fa fa-trash'></i></button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default" style="margin-bottom: 0px;">
                <div class="panel-body">
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='table-responsive'>
                                <table class='table table-striped table-hover table-bordered table-condensed' id='tablaMantenedor' width="100%">
                                    <thead style="display: table-header-group">
                                        <tr>
                                            <th></th>
                                            <th>Cuenta</th>
                                            <th>Nombre Cuenta</th>
                                            <th>Instrumento</th>
                                            <th>Fecha Inicio</th>
                                            <th>Fecha Término</th>
                                            <th>Comisión %</th>
                                            <th>Gastos</th>
                                            <th>Derechos Bolsa %</th>
                                            <th>IVA</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="panelAgregar" class="col-sm-12 top-buffer hidden">
            <div class="navbar navbar-default">
                <div class="col-xs-12">
                    <ul class='nav nav-pills'>
                        <li>
                            <button type="button" id="btnGuardar" class="btn btn-primary navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Guardar'><i class="fa fa-floppy-o"></i></button>
                        </li>
                        <li>
                            <button type="button" id="btnCancelar" class="btn btn-danger navbar-btn" data-toggle='tooltip' data-placement='bottom' data-original-title='Cancelar'><i class="fa fa-times"></i></button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default">
                <div id="tituloAgregaEdita" class="panel-heading"></div>
                <div id="sPanelAgregar" class="panel-body">
                    <div class="row">
                        <div class="form-horizontal">
                            <div id="divDDInstrumento" class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="padding-top: 5px;">
                                <label class="control-label">Código Instrumento</label>
                                <select id="DDInstrumento" class="form-control input-sm">
                                </select>
                                <span id="lblErrInstrumento" class="label label-danger hidden"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-horizontal">
                            <div id="divTxtComision" class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="padding-top: 5px; padding-bottom: 20px;">
                                <label class="control-label">Comisión</label>
                                <div class="input-group">
                                    <input id="txtComision" type="number" min="0" max="100" step="any" placeholder="0.0000" class="form-control input-sm text-right" />
                                    <span class="input-group-addon">%</span>
                                </div>
                                <span id="lblErrComision" class="label label-danger hidden">El Porcentaje debe estar entre 0 y 100%</span>
                            </div>
                            <div id="divTxtGastos" class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="padding-top: 5px; padding-bottom: 20px;">
                                <label class="control-label">Gastos</label>
                                <input id="txtGastos" maxlength="6" type="number" step="any" placeholder="0.0000" class="form-control input-sm text-right" />
                            </div>
                            <div id="divTxtDerBolsa" class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="padding-top: 5px; padding-bottom: 20px;">
                                <label class="control-label">Derechos Bolsa</label>
                                <div class="input-group">
                                    <input id="txtDerechosBolsa" type="number" min="0" max="100" step="any" placeholder="0.0000" class="form-control input-sm text-right" />
                                    <span class="input-group-addon">%</span>
                                </div>
                                <span id="lblErrDerBolsa" class="label label-danger hidden">El Porcentaje debe estar entre 0 y 100%</span>
                            </div>
                            <div id="divChkIva" class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="padding-top: 9px; padding-bottom: 25px;">
                                <label class="control-label"></label>
                                <div class="checkbox">
                                    <input id="chkIva" type="checkbox">
                                    <label for="chkIva">Valores Afectos a IVA</label>
                                </div>
                            </div>
                            <div id="divTxtFechas" class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="padding-top: 5px;">
                                <label class="control-label">Periodo vigencia</label>
                                <div id="dtRangoFecha" class="input-daterange input-group">
                                    <input id="dtFechaDesde" type="text" class="form-control input-sm" />
                                    <span class="input-group-addon">a</span>
                                    <div id="divFechaTermino" class="input-group-btn">
                                        <button id="btnFechaTermino" type="button" class="btn btn-default btn-sm">Fecha Término</button>
                                    </div>
                                    <input id="dtFechaHasta" type="text" class="form-control input-sm hidden" />
                                    <div id="divFechaTerminoX" class="input-group-btn hidden">
                                        <button id="btnFechaTerminoX" type="button" class="btn btn-default btn-sm" data-toggle='tooltip' data-placement='bottom' data-original-title='Sin Fecha'><i class="fa fa-angle-left"></i></button>
                                    </div>
                                </div>
                                <span id="lblErrFecha" class="label label-danger hidden"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12" style="margin-top: 12px;">
                            <ul id="ulAlertas" class="text-danger bg-danger"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
