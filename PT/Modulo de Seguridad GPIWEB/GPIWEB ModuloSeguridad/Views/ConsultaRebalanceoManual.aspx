﻿<%@ Page Language="vb" MasterPageFile="~/Sistema/GPIWeb.master" AutoEventWireup="false"
    CodeBehind="ConsultaRebalanceoManual.aspx.vb" Inherits="AplicacionWeb.ConsultaRebalanceoManual" %>

<%@ Import Namespace="AplicacionWeb" %>

<asp:Content ID="inicialppal" ContentPlaceHolderID="ContenidoPPalFiltro" runat="Server">

    <script src='<%= VersionLinkHelper.GetVersion("../js/App/Views/consultaRebalanceoManual.min.js") %>'></script>

    <div id="encSubPantalla">
        <div class="divConsultarSeccion row top-buffer">
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <select id="DDInstrumento" class="form-control" data-toggle="tooltip" data-placement="bottom" title="Instrumentos">
                    </select>
                </div>
                <div class="form-group">
                    <div class="input-group date">
                        <input type="text" id="dtFechaConsulta" class="form-control" style="text-align: center;">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" id="BtnConsultar" class="btn btn-primary">Consultar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row top-buffer">
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tabs-1" data-toggle="tab">Renta Variable</a></li>
                <li><a href="#tabs-2" data-toggle="tab">Renta Fija</a></li>
                <li><a href="#tabs-3" data-toggle="tab">Fondos Mutuos</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tabs-1">
                    <div id="DivRV" style="height: auto" class="DivCentradosGrillaCompleta">
                        <table id="ListaOperacionesRV">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                        <div id="PaginadorRV">
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tabs-2">
                    <div id="DivRF" style="height: auto;" class="DivCentradosGrillaCompleta">
                        <table id="ListaOperacionesRF">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                        <div id="PaginadorRF">
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tabs-3">
                    <div id="DivFFMM" style="height: auto;" class="DivCentradosGrillaCompleta">
                        <table id="ListaOperacionesFFMM">
                            <tr>
                                <td></td>
                            </tr>
                        </table>
                        <div id="PaginadorFFMM">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
