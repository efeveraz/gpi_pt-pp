IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_OPERACIONES$Buscar_Operaciones_Cliente]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_OPERACIONES$Buscar_Operaciones_Cliente]
GO
CREATE PROCEDURE [dbo].[PKG_OPERACIONES$Buscar_Operaciones_Cliente]   
( @Pfecha_ini DATETIME = NULL  
, @Pfecha_ter DATETIME = NULL  
, @Pid_Empresa NUMERIC = NULL  
, @Pid_Operacion NUMERIC = NULL  
, @Pcod_estado VARCHAR(3) = NULL  
, @Pid_Cuenta FLOAT(53) = NULL  
, @Pid_Asesor FLOAT(53) = NULL  
, @Pcod_Instrumento VARCHAR(15) = NULL  
, @Pcod_Producto VARCHAR(10) = NULL  
, @Pcod_tipo_operacion VARCHAR(10) = NULL  
, @Pflg_tipo_movimiento VARCHAR(1) = NULL  
, @pid_cliente NUMERIC = NULL  
, @pid_usuario NUMERIC = NULL  
, @pId_Nemotecnico NUMERIC = NULL)  
AS  
BEGIN  
    SELECT DISTINCT CTA.*  
    FROM VIEW_OPERACIONES CTA  
        , VIEW_CUENTAS_VIGENTES VCV --Vc  
    WHERE ((@Pid_empresa IS NULL) OR (CTA.ID_EMPRESA = @Pid_empresa))  
        AND ((@Pid_Operacion IS NULL) OR (CTA.ID_OPERACION = @Pid_Operacion))  
        AND CTA.ID_CUENTA IN (SELECT C.ID_CUENTA FROM CUENTAS C WHERE C.ID_CLIENTE = ISNULL(@pid_cliente, C.ID_CLIENTE))  
        AND ((@Pid_Cuenta IS NULL) OR (CTA.ID_CUENTA = @Pid_Cuenta))  
        AND ((@Pid_Asesor IS NULL) OR (CTA.ID_ASESOR = @Pid_Asesor))  
        AND (cta.FECHA_OPERACION BETWEEN isnull(@Pfecha_ini, cta.FECHA_OPERACION) AND ISNULL(@Pfecha_ter, CTA.FECHA_OPERACION))  
        AND ((@Pcod_estado IS NULL) OR (CTA.COD_ESTADO = @Pcod_estado))   
        AND ((@Pcod_Instrumento IS NULL) OR (CTA.COD_INSTRUMENTO = @Pcod_Instrumento))  
        AND ((@Pcod_Producto IS NULL) OR (CTA.COD_PRODUCTO = @Pcod_Producto))  
        AND ((@Pcod_tipo_operacion IS NULL) OR (CTA.COD_TIPO_OPERACION = @Pcod_tipo_operacion))  
        AND ((@Pflg_tipo_movimiento IS NULL) OR (CTA.FLG_TIPO_MOVIMIENTO = @Pflg_tipo_movimiento))  
        AND (CTA.ID_CUENTA = VCV.ID_CUENTA) --Vc  
        AND EXISTS(SELECT 1 FROM VIEW_OPERACION_DETALLE VD  
            WHERE CTA.ID_OPERACION = VD.ID_OPERACION  
                AND ((@pid_nemotecnico IS NULL) OR (VD.ID_NEMOTECNICO = @pid_nemotecnico)))        
Union all 

    Select V.FECHA_DE_MVTO     AS FECHA_OPERACION,
           V.ID_VTA_CORTA      AS ID_OPERACION,
           V.ID_CUENTA         AS ID_CUENTA,
           C.NUM_CUENTA        AS NUM_CUENTA,
           N.COD_INSTRUMENTO   AS COD_INSTRUMENTO,
           N.COD_PRODUCTO      AS COD_PRODUCTO,
           V.VALOR             AS MONTO_OPERACION,
           1                   AS ID_MONEDA_OPERACION,
           'PESOS'             AS DSC_MONEDA,
           NULL                AS ID_CONTRAPARTE,
           NULL                AS DSC_CONTRAPARTE,
           'Venta Corta'       as DSC_INTRUMENTO,
           NULL                AS FORMULARIO,
           C.ID_ASESOR         AS ID_ASESOR,
           C.NOMBRE_ASESOR     AS NOMBRE_ASESOR,
           V.tipo_operacion    AS COD_TIPO_OPERACION,
           'Venta Corta'       AS DSC_TIPO_OPERACION,
           'I'                 AS FLG_TIPO_MOVIMIENTO ,
           C.ID_EMPRESA        AS ID_EMPRESA,
           V.COD_ESTADO        AS COD_ESTADO ,
           E.DSC_ESTADO        AS DSC_ESTADO ,
           'Venta Corta'       AS DSC_PRODUCTO,
           NULL                AS FECHA_LIQUIDACION ,
           NULL                AS ID_CARGO_ABONO ,
           0                   AS COMISION ,
           0                   AS derechos ,
           0                   AS IVA ,
           0                   AS porc_comision ,
           0                   AS GASTOS
    FROM dbo.VENTA_CORTA V , VIEW_CUENTAS_VIGENTES C, VIEW_NEMOTECNICOS N, ESTADOS E
    WHERE ((@Pid_empresa IS NULL) OR (C.ID_EMPRESA = @Pid_empresa))  
      AND ((@Pid_Operacion IS NULL) OR (V.ID_VTA_CORTA  = @Pid_Operacion))  
      AND ((@Pid_Cuenta IS NULL) OR (V.ID_CUENTA = @Pid_Cuenta))  
      AND ((@Pid_Asesor IS NULL) OR (C.ID_ASESOR = @Pid_Asesor))  
      AND (V.FECHA_DE_MVTO BETWEEN isnull(@Pfecha_ini, V.FECHA_DE_MVTO) AND ISNULL(@Pfecha_ter, V.FECHA_DE_MVTO))  
      AND ((@Pcod_estado IS NULL) OR (V.COD_ESTADO = 'C')) --'C' = @Pcod_estado))   
      AND ((@Pcod_Instrumento IS NULL) OR (N.COD_INSTRUMENTO = @Pcod_Instrumento))  
      AND (V.ID_CUENTA = C.ID_CUENTA)
      AND (V.ID_NEMOTECNICO = N.ID_NEMOTECNICO)
      AND (V.ID_TIPO_ESTADO = E.ID_TIPO_ESTADO)
      AND (V.COD_ESTADO = E.COD_ESTADO)
    
END  
GO
GRANT EXECUTE ON [PKG_OPERACIONES$Buscar_Operaciones_Cliente] TO DB_EXECUTESP
GO