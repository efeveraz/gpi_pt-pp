IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_PROCESOS_IMPORTADOR$CisCB_VENTA_CORTA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_PROCESOS_IMPORTADOR$CisCB_VENTA_CORTA]
GO  
CREATE PROCEDURE  [dbo].[PKG_PROCESOS_IMPORTADOR$CisCB_VENTA_CORTA] (    
@pFecha_movimientos char(8) ,    
@pRut_Cliente       varchar(15)  = null    
)        
AS        
        
BEGIN        

   SET DATEFORMAT YMD        
   SET NOCOUNT ON        
        
   DECLARE @lCantidad             decimal     ,    @lNum_Fila             int         ,    @linstrumento    CHAR(12)   ,     
           @lNemotecnicoFin       char(50)    ,    @lid_Nemotecnico       Numeric(10) ,    @lfolio          INT        ,    
           @lRut_Cliente          char(15)    ,    @lRegistros            int         ,    @lcuenta_gpi     varchar(10),    
           @LCODRESULTADO         varchar(5)  ,    @lNemotecnico          char(50)    ,    @lErrorNoCuenta  varchar(1) ,    
           @LMSGRESULTADO         varchar(800),    @DSC_BLOQUEO           varchar(120),    @lid_cuenta      int        ,    
           @LCODINSTRUMENTO       char(15)    ,    @intCounter            int         ,    @ID_ORIGEN       numeric    ,      
           @ID_TIPO_CONVERSION    numeric     ,    @lOutputLOG_SCTAS_flg  int         ,    @ID_ASESOR       int        ,        
           @COD_ESTADO            varchar(3)  ,    @FLG_BLOQUEADO         varchar(1)  ,    @lValor_Cli      varchar(50),    
           @lRutCuenta            varchar(20) ,    @lCuentaGPI            varchar(15) ,    @lNumeroOperacion numeric(10)
   SET @intCounter     = 0        
       
--************************************************************************************************************************************************************************        
    
-- CREACION DE TABLA DE RECUPERACION DE DATOS        
CREATE TABLE #Tabla1 ( Nombre_bolsa         CHAR(40)      ,  codigo_bolsa     CHAR(1)      ,   id_cliente            CHAR(15)     ,                          
                       nombre_cliente       CHAR(60)      ,  fecha_de_mvto    INT          ,   folio                 INT          ,  
                       tipo_operacion       CHAR(3)       ,  Nombre_agente    VARCHAR(122) ,   canal                 SMALLINT     ,  
                       agencia              SMALLINT      ,  agente           SMALLINT     ,   instrumento           CHAR(12)     ,  
                       cantidad             DECIMAL(16,4) ,  precio_medio     DECIMAL(16,4),   valor                 DECIMAL(16,4),  
                       tasa                 DECIMAL(16,4) ,  prima_a_plazo    INT          ,   prima_acumulada       INT          ,                          
                       dias_operacion       INT           ,  dias_en_curso    INT          ,   interes_devengado_mes INT          ,  
                       fecha_vencimiento    INT           ,  correlativo      INT          ,   base_tasa             CHAR(1)      ,  
                       precio_medio_mercado DECIMAL(16,4) ,  tipo_transaccion CHAR(3)      ,   tipo_origen           CHAR(1)      ,  
                       tipo_persona         SMALLINT      ,  tipo_liquidacion CHAR(2)      ,   cuenta_gpi            varchar(10)  ,  
                       Mandato              VARCHAR(1)    )      
    
-- CREACION DE TABLA DE PROCESO        
CREATE TABLE #Tabla2  ( NUMERO_FILA     int           ,  Nombre_bolsa     CHAR(40)    ,  codigo_bolsa  CHAR(1)  ,   id_cliente         CHAR(15) ,  
                        nombre_cliente  CHAR(60)      ,  fecha_de_mvto    INT         ,  folio         INT      ,     
                        tipo_operacion  CHAR(3)       ,  Nombre_agente    VARCHAR(122),  canal         SMALLINT ,    
                        agencia         SMALLINT      ,  agente           SMALLINT    ,  instrumento   CHAR(12) ,   
                        cantidad        DECIMAL(16,4) ,  precio_medio     DECIMAL(16,4), valor         DECIMAL(16,4),    
                        tasa            DECIMAL(16,4) ,  prima_a_plazo    INT          , prima_acumulada    INT     ,                          
                        dias_operacion       INT      ,  dias_en_curso    INT          , interes_devengado_mes INT  ,     
                        fecha_vencimiento    INT      ,  correlativo      INT          ,   base_tasa    CHAR(1)     ,  
                        precio_medio_mercado DECIMAL(16,4) ,  tipo_transaccion CHAR(3) ,   tipo_origen  CHAR(1)     ,    
                        tipo_persona         SMALLINT      ,  tipo_liquidacion CHAR(2) ,   cuenta_gpi  varchar(10)  ,  
                        Mandato              VARCHAR(1)    ,  Cod_Producto     Varchar(15)  , id_cuenta     NUMERIC(10)  ,       
                       ID_Nemotecnico       NUMERIC(10)   ,  LCODRESULTADO    VARCHAR(5)   ,   LMSGRESULTADO         varchar(800) )      
--************************************************************************************************************************************************************************        
CREATE TABLE #Clientes_AUX (ValorCli varchar(50))    
CREATE TABLE #Clientes (ValorCli varchar(50))    
--************************************************************************************************************************************************************************              
    
   SELECT @ID_ORIGEN = ID_ORIGEN FROM ORIGENES WITH (NOLOCK) WHERE COD_ORIGEN = 'MAGIC_VALORES'        
   SELECT @ID_TIPO_CONVERSION = ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WITH (NOLOCK) WHERE TABLA = 'CUENTAS'        
    
   IF @pRut_Cliente IS NULL     
      INSERT #Clientes_AUX    
      SELECT distinct (VALOR)      
      FROM rel_conversiones           
       WHERE ID_ORIGEN  = @ID_ORIGEN          
         AND ID_TIPO_CONVERSION  = @ID_TIPO_CONVERSION                
   ELSE    
      INSERT #Clientes_AUX
      SELECT distinct(VALOR)      
      FROM rel_conversiones           
      WHERE ID_ORIGEN  = @ID_ORIGEN          
       AND ID_TIPO_CONVERSION  = @ID_TIPO_CONVERSION                
       AND VALOR like  @pRut_Cliente + '%'           
    

	  /* SE QUITA EL VALOR QUE ESTA DESPUES DE "/" DEJANDO SOLO EL RUT EN EL CASO QUE SEA NESESARIO QUITARLO */
      Declare @id int,     @count int,   @lBuscar int
      Set @id=1        
      select @count=count(1)from #Clientes_AUX            
      while @id<=@count         
           begin                 
               select @lValor_Cli =  ValorCli                
               from (select  ValorCli,RANK()OVER (ORDER BY ValorCli ASC)AS RANK from #Clientes_AUX) as ji where rank=@id
               
               set @lBuscar = 0
               set @lBuscar = CHARINDEX ( '/' , @lValor_Cli , 1 ) 
               if  @lBuscar > 0 
			   Set @lValor_Cli = substring(@lValor_Cli,1 ,charindex('/',@lValor_Cli)-1)                   
               
               if not exists (select 1 from #Clientes where ValorCli = @lValor_Cli) 
                  INSERT #Clientes
                  SELECT  @lValor_Cli     
              Set @id=@id+1       
            end         
      /******************************************************************************************/

      select @count=count(1)from #Clientes
	  IF @count = 1 
	     Begin
		      declare @RutCli varchar(15)  
		      select @RutCli  = ValorCli from #Clientes 
		      INSERT #Tabla1              
              EXEC [CisCB].[dbo].[AD_QRY_CB_VENTAS_CORTAS_VIGENTES] @pRutCliente =     @RutCli ,            
                                                                    @pFechaConsulta =  @pFecha_movimientos ,            
                                                                    @pTipoOperacion = 'AM' ,    
                                                                    @pCodErr = 0 ,           
                                                                    @pMsgErr  =''  
		 End
      Else
	     Begin
		      INSERT #Tabla1              
              EXEC [CisCB].[dbo].[AD_QRY_CB_VENTAS_CORTAS_VIGENTES] @pFechaConsulta =  @pFecha_movimientos ,            
                                                                    @pTipoOperacion = 'AM' ,    
                                                                    @pCodErr = 0 ,           
                                                                    @pMsgErr  =''  
		 End 

   -- CARGA DATOS DE PROCEDIMIENTO EN TABLA DE PROCESO        
   INSERT INTO #Tabla2        
   SELECT 1, Nombre_bolsa          ,  codigo_bolsa      ,   id_cliente     ,  nombre_cliente   , fecha_de_mvto         ,  folio             ,   tipo_operacion ,  Nombre_agente    ,                
             canal                 ,  agencia           ,   agente         ,  instrumento      , cantidad              ,  precio_medio      ,   valor          ,  tasa             ,                
             prima_a_plazo         ,  prima_acumulada   ,   dias_operacion ,  dias_en_curso    , interes_devengado_mes ,  fecha_vencimiento ,   correlativo    ,  base_tasa        ,         
             precio_medio_mercado  ,  tipo_transaccion  ,   tipo_origen    ,  tipo_persona     , tipo_liquidacion      ,  cuenta_gpi        ,   Mandato        ,  NULL             ,    
             NULL                  ,  NULL              ,   NULL           , null     
   FROM #Tabla1  WITH (NOLOCK)  , #Clientes_AUX WITH (NOLOCK)
   where id_cliente = ValorCli    
    
   --INCREMENTA EL NUMERO DE FILA EN TABLA DE PROCESO        
   UPDATE #Tabla2        
   SET @intCounter = NUMERO_FILA = @intCounter + 1;        
        
   --****************************************************************************************************************************        
   DECLARE CUR_OPERACION CURSOR FOR        
   SELECT NUMERO_FILA,        
          instrumento,        
          id_cliente ,    
          cuenta_gpi  ,    
          folio      
   FROM #Tabla2  WITH (NOLOCK)       
   order by NUMERO_FILA         
        
   OPEN CUR_OPERACION        
   FETCH CUR_OPERACION INTO @lNum_Fila,        
                            @linstrumento,        
                            @lRutCuenta,    
                            @lCuentaGPI,    
                            @lfolio         
   WHILE (@@FETCH_STATUS = 0)        
   BEGIN        
        
         SET @LCODRESULTADO  = 'OK'        
         SET @LMSGRESULTADO  = ''        
         SET @lid_Nemotecnico = NULL        
         SET @lNemotecnico = rtrim(ltrim(@lNemotecnico))        
        
         -- IDENTIFICADOR DE CUENTA **************************        
         SELECT @lid_cuenta = id_entidad        
         FROM REL_CONVERSIONES WITH (NOLOCK)        
         WHERE ID_ORIGEN          = @ID_ORIGEN      
           AND ID_TIPO_CONVERSION = @ID_TIPO_CONVERSION     
           AND VALOR = rtrim(ltrim(@lRutCuenta))        
    
         If @@RowCount = 0        
            BEGIN        
                SET @LCODRESULTADO = 'ERROR'        
                SET @LMSGRESULTADO = 'ERROR: Rut Cliente: ' + rtrim(ltrim(@lRut_Cliente))  + ' |Nemotécnico: ' + rtrim(ltrim(@linstrumento)) + '- Movimiento no posee cuenta.'        
            END        
         ELSE        
            BEGIN        
                 SELECT @COD_ESTADO    = COD_ESTADO,        
                        @FLG_BLOQUEADO = FLG_BLOQUEADO,        
                        @DSC_BLOQUEO   = ISNULL(OBS_BLOQUEO,'')        
                 FROM CUENTAS WITH (NOLOCK) WHERE ID_CUENTA = @lid_cuenta        
        
                 IF @COD_ESTADO = 'H'        
                    BEGIN        
                        IF @FLG_BLOQUEADO = 'S'        
                           BEGIN        
                                SET @LCODRESULTADO = 'ERROR'        
                                SET @LMSGRESULTADO = 'ERROR: Cuenta: ' + @lCuentaGPI  + '- Cuenta Bloqueada:' + @DSC_BLOQUEO        
                           END         
                    END     
                 ELSE        
                        BEGIN        
                               SET @LCODRESULTADO = 'ERROR'        
                               SET @LMSGRESULTADO = 'ERROR: Cuenta: ' + @lCuentaGPI  + '|Rut Cliente: ' + rtrim(ltrim(@lRut_Cliente)) + '-Cuenta Deshabilitada'        
                           END        
            END        
        
         /**** VALIDACION DE NEMO *****/        
    
         IF ((SELECT CASE WHEN EXISTS(SELECT 1 FROM dbo.VIEW_NEMOTECNICOS WITH (NOLOCK) WHERE NEMOTECNICO = rtrim(ltrim(@linstrumento))) THEN 1 ELSE 0 END) = 1)        
             BEGIN        
                  SELECT @lid_Nemotecnico = ID_NEMOTECNICO,        
                         @lCodInstrumento = COD_INSTRUMENTO        
                  FROM dbo.VIEW_NEMOTECNICOS WITH (NOLOCK)        
                  WHERE NEMOTECNICO = rtrim(ltrim(@linstrumento))        
             END        
         ELSE        
             BEGIN        
                 SET @LCODRESULTADO = 'ERROR'        
                 SET @LMSGRESULTADO = 'ERROR: Problemas al buscar Nemotécnico ' + rtrim(ltrim(@lNemotecnico))        
            END        
        
         /***********************************************************************************************************/        
         /***********************************************************************************************************/        
    
               UPDATE #Tabla2 SET ID_CUENTA       = @lid_cuenta,        
                                  Cod_Producto    = @lCodInstrumento,    
                                  ID_Nemotecnico  = @lid_Nemotecnico,     
                                  LCODRESULTADO   = @LCODRESULTADO,        
                                  LMSGRESULTADO   = @LMSGRESULTADO        
               WHERE Instrumento = @linstrumento        
               AND id_cliente = @lRutCuenta        
               AND NUMERO_FILA  = @lNum_Fila      
               AND folio = @lfolio    
       
        --Lectura de la siguiente fila de un cursor        
         FETCH CUR_OPERACION INTO @lNum_Fila,        
                                  @linstrumento,        
                                  @lRutCuenta,    
                                  @lCuentaGPI,    
                                  @lfolio         
   END        
        
   -- Cierra el cursor        
   CLOSE CUR_OPERACION        
   DEALLOCATE CUR_OPERACION        
        
   -- Tabla Resultante      
    
    
   SELECT   T.Nombre_bolsa     ,  T.codigo_bolsa      ,  T.id_cliente      ,T.nombre_cliente,  CONVERT(DATETIME,T.fecha_de_mvto -693596,112) as fecha_de_mvto   ,               
            T.folio            ,  T.tipo_operacion    ,  T.Nombre_agente   ,  T.canal       ,  T.agencia        ,          T.agente                ,  
            T.instrumento      ,  T.cantidad          ,  T.precio_medio    ,  T.valor       ,  T.tasa           ,          T.prima_a_plazo         ,   
            T.prima_acumulada  ,  T.dias_operacion    ,  T.dias_en_curso   ,  T.interes_devengado_mes         ,  CONVERT(DATETIME,T.fecha_vencimiento -693596,112) as fecha_vencimiento ,
            T.correlativo      ,  T.base_tasa         ,  T.precio_medio_mercado  ,  T.tipo_transaccion        ,  T.tipo_origen                   ,  
            T.tipo_persona     ,  T.tipo_liquidacion  ,  T.cuenta_gpi            ,  T.Mandato                 ,  T.Cod_Producto                  ,  
            T.id_cuenta        ,  T.ID_Nemotecnico    ,  T.LCODRESULTADO         ,  T.LMSGRESULTADO           , E.DSC_EMPRESA      , 
           CASE WHEN CL.TIPO_ENTIDAD = 'J' THEN CL.RAZON_SOCIAL 
                WHEN CL.TIPO_ENTIDAD = 'N' THEN CL.NOMBRES + ' ' + CL.PATERNO + ' ' + CL.MATERNO end as NombreCliente,
            RTRIM(LTRIM(CL.RUT_CLIENTE)) 'RUT_CLIENTE'
   FROM  #Tabla2 T WITH (NOLOCK)    
   left JOIN CUENTAS C   ON C.id_cuenta  = T.id_cuenta
   left JOIN EMPRESAS E  ON E.id_empresa  = C.id_empresa
   left JOIN CLIENTES CL ON CL.id_cliente = C.id_cliente   
   Where NOT EXISTS (
                     SELECT 1  FROM VENTA_CORTA_DEVENGO
                     WHERE id_vta_corta     = (SELECT ID_ENTIDAD FROM REL_CONVERSIONES        
                                                WHERE ID_ORIGEN = (SELECT ID_ORIGEN FROM ORIGENES WITH (NOLOCK) WHERE COD_ORIGEN = 'AD_QRY_CB_VENTAS_CORTAS_VIGENTES')        
                                                  AND ID_TIPO_CONVERSION = (SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WITH (NOLOCK) WHERE TABLA = 'VENTA_CORTA')        
                                                  AND VALOR = T.folio)
                       AND folio            = T.folio
                       AND fecha_de_mvto    = CONVERT(DATETIME,T.fecha_de_mvto -693596,112)
                       AND fecha_de_devengo = @pFecha_movimientos
                     )
   order by NUMERO_FILA , ID_CUENTA    
    
   DROP TABLE #Tabla1        
   DROP TABLE #Tabla2        
   DROP TABLE #Clientes_AUX 
   DROP TABLE #Clientes
   SET NOCOUNT OFF        
END        
GO
GRANT EXECUTE ON [PKG_PROCESOS_IMPORTADOR$CisCB_VENTA_CORTA] TO DB_EXECUTESP
GO  