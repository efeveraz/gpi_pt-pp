IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PKG_CARGAS$INGRESA_VENTA_CORTA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_CARGAS$INGRESA_VENTA_CORTA]
GO
CREATE procedure PKG_CARGAS$INGRESA_VENTA_CORTA (
@pNombre_bolsa          CHAR(40)     ,
@pcodigo_bolsa          CHAR(1)      ,
@pid_cliente            CHAR(15)     ,
@pfecha_de_mvto         datetime     ,
@pfolio                 INT          ,
@ptipo_operacion        CHAR(3)      , 
@pcantidad              DECIMAL(16,4),
@pprecio_medio          DECIMAL(16,4),
@pvalor                 DECIMAL(16,4),
@ptasa                  DECIMAL(16,4),
@pprima_a_plazo         INT          ,
@pprima_acumulada       INT          ,
@pdias_operacion        INT          ,
@pdias_en_curso         INT          ,
@pinteres_devengado_mes INT          ,
@pfecha_vencimiento     datetime     ,
@pbase_tasa             CHAR(1)      , 
@pprecio_medio_mercado  DECIMAL(16,4),
@pCod_Producto          VARCHAR(15)  ,
@pid_cuenta             NUMERIC(10)  ,
@pID_Nemotecnico        NUMERIC(10)  ,
@pFecha                 datetime
)
AS
BEGIN   

   Declare @Correlativo_Vta    numeric(18,0),   
           @id_moneda          int ,
           @ID_ORIGEN          numeric(10)  ,   
           @ID_TIPO_CONVERSION numeric(10)

   If NOT Exists (select 1 from VENTA_CORTA where folio = @pfolio and id_cuenta = @pid_cuenta and cod_estado = 'C')
   Begin
     INSERT INTO dbo.VENTA_CORTA ( folio                  ,
                                   Nombre_bolsa           ,
                                   codigo_bolsa           ,   
                                   id_cliente             ,    
                                   fecha_de_mvto          ,    
                                   tipo_operacion         ,   
                                   cantidad               ,    
                                   precio_medio           ,   
                                   valor                  ,
                                   tasa                   ,  
                                   prima_a_plazo          ,   
                                   prima_acumulada        , 
                                   dias_operacion         ,  
                                   dias_en_curso          ,   
                                   interes_devengado_mes  ,   
                                   fecha_vencimiento      ,
                                   base_tasa              ,
                                   precio_medio_mercado   ,
                                   Cod_Producto           ,
                                   id_cuenta              ,
                                   ID_Nemotecnico)

                values         (   @pfolio                ,
                                   @pNombre_bolsa         ,
                                   @pcodigo_bolsa         ,  
                                   @pid_cliente           ,
                                   @pfecha_de_mvto        ,
                                   @ptipo_operacion       ,
                                   @pcantidad             ,
                                   @pprecio_medio         ,
                                   @pvalor                ,
                                   @ptasa                 ,
                                   @pprima_a_plazo        ,       
                                   @pprima_acumulada      ,      
                                   @pdias_operacion       ,     
                                   @pdias_en_curso        ,    
                                   @pinteres_devengado_mes, 
                                   @pfecha_vencimiento    ,
                                   @pbase_tasa            ,
                                   @pprecio_medio_mercado ,
                                   @pCod_Producto         ,
                                   @pid_cuenta            ,
                                   @pID_Nemotecnico       )

      SET @Correlativo_Vta = @@IDENTITY 

      SELECT @ID_ORIGEN = ID_ORIGEN FROM ORIGENES WITH (NOLOCK) WHERE COD_ORIGEN = 'AD_QRY_CB_VENTAS_CORTAS_VIGENTES'
      SELECT @ID_TIPO_CONVERSION = ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WITH (NOLOCK) WHERE TABLA = 'VENTA_CORTA'

      INSERT INTO REL_CONVERSIONES (ID_ORIGEN, ID_TIPO_CONVERSION, ID_ENTIDAD, VALOR )
      VALUES (@ID_ORIGEN, @ID_TIPO_CONVERSION, @Correlativo_Vta, @pfolio)
   end
   ELSE
   BEGIN
      SET @Correlativo_Vta = (SELECT ID_ENTIDAD FROM REL_CONVERSIONES        
							   WHERE ID_ORIGEN = (SELECT ID_ORIGEN FROM ORIGENES WITH (NOLOCK) WHERE COD_ORIGEN = 'AD_QRY_CB_VENTAS_CORTAS_VIGENTES')        
							     AND ID_TIPO_CONVERSION = (SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WITH (NOLOCK) WHERE TABLA = 'VENTA_CORTA')        
								 AND VALOR = @pfolio)   
   END
   
   if NOT EXISTS (SELECT 1  FROM VENTA_CORTA_DEVENGO
                   WHERE id_vta_corta     = (SELECT ID_ENTIDAD FROM REL_CONVERSIONES        
                                              WHERE ID_ORIGEN = (SELECT ID_ORIGEN FROM ORIGENES WITH (NOLOCK) WHERE COD_ORIGEN = 'AD_QRY_CB_VENTAS_CORTAS_VIGENTES')        
                                                AND ID_TIPO_CONVERSION = (SELECT ID_TIPO_CONVERSION FROM TIPOS_CONVERSION WITH (NOLOCK) WHERE TABLA = 'VENTA_CORTA')        
                                                AND VALOR = @pfolio)
                     AND folio            = @pfolio
                     AND fecha_de_mvto    = @pfecha_de_mvto
                     AND fecha_de_devengo = @pFecha)
   Begin
      INSERT INTO VENTA_CORTA_DEVENGO (id_vta_corta, folio, fecha_de_mvto, fecha_de_devengo, prima_a_plazo, prima_acumulada, dias_operacion, dias_en_curso, interes_devengado_mes)
      VALUES (@Correlativo_Vta, @pfolio, @pfecha_de_mvto, @pFecha, @pprima_a_plazo, @pprima_acumulada, @pdias_operacion, @pdias_en_curso, @pinteres_devengado_mes)
   end                        
   
end 
GO
GRANT EXECUTE ON [PKG_CARGAS$INGRESA_VENTA_CORTA] TO DB_EXECUTESP
GO


