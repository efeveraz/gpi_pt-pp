VERSION 5.00
Object = "{1C0489F8-9EFD-423D-887A-315387F18C8F}#1.0#0"; "vsflex8l.ocx"
Object = "{FACAC329-31C6-41BF-B37A-3E65D9715ED5}#5.0#0"; "hControl2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "MSMAPI32.OCX"
Begin VB.Form Frm_CargaVenCorta 
   BorderStyle     =   0  'None
   Caption         =   "Importador Venta Corta"
   ClientHeight    =   8850
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   14730
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8850
   ScaleWidth      =   14730
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frm_Cuentas 
      Caption         =   "Operaciones sin Conflictos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   4455
      Left            =   0
      TabIndex        =   7
      Top             =   1200
      Width           =   14685
      Begin MSComctlLib.Toolbar Toolbar_Operaciones 
         Height          =   330
         Left            =   60
         TabIndex        =   8
         Top             =   255
         Width           =   10995
         _ExtentX        =   19394
         _ExtentY        =   582
         ButtonWidth     =   2487
         ButtonHeight    =   582
         AllowCustomize  =   0   'False
         Wrappable       =   0   'False
         Appearance      =   1
         Style           =   1
         TextAlignment   =   1
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   5
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Grabar"
               Key             =   "SAVE"
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Importar"
               Key             =   "IMPORT"
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Sel. Todo"
               Key             =   "SEL_ALL"
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Anula Sel."
               Key             =   "SEL_NOTHING"
            EndProperty
            BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Limpiar"
               Key             =   "REFRESH"
            EndProperty
         EndProperty
      End
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Vta 
         CausesValidation=   0   'False
         Height          =   3675
         Left            =   60
         TabIndex        =   9
         Top             =   660
         Width           =   14475
         _cx             =   25532
         _cy             =   6482
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   2
         SelectionMode   =   1
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   35
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_CargaVenCorta.frx":0000
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   0   'False
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   2
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   1
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   0
         AutoSizeMouse   =   0   'False
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin VB.Frame Frnm_Grilla_Sucesos 
      Caption         =   "Operaciones con Conflictos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   2925
      Left            =   0
      TabIndex        =   6
      Top             =   5670
      Width           =   14685
      Begin VSFlex8LCtl.VSFlexGrid Grilla_Vta_Sin_Cuenta 
         CausesValidation=   0   'False
         Height          =   2595
         Left            =   60
         TabIndex        =   12
         Top             =   240
         Width           =   14475
         _cx             =   25532
         _cy             =   4577
         Appearance      =   2
         BorderStyle     =   1
         Enabled         =   -1  'True
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MousePointer    =   0
         BackColor       =   -2147483643
         ForeColor       =   -2147483640
         BackColorFixed  =   -2147483633
         ForeColorFixed  =   -2147483630
         BackColorSel    =   65535
         ForeColorSel    =   0
         BackColorBkg    =   -2147483643
         BackColorAlternate=   -2147483643
         GridColor       =   -2147483633
         GridColorFixed  =   -2147483632
         TreeColor       =   -2147483632
         FloodColor      =   192
         SheetBorder     =   -2147483642
         FocusRect       =   2
         HighLight       =   1
         AllowSelection  =   0   'False
         AllowBigSelection=   0   'False
         AllowUserResizing=   2
         SelectionMode   =   1
         GridLines       =   10
         GridLinesFixed  =   2
         GridLineWidth   =   1
         Rows            =   2
         Cols            =   36
         FixedRows       =   1
         FixedCols       =   0
         RowHeightMin    =   0
         RowHeightMax    =   0
         ColWidthMin     =   0
         ColWidthMax     =   0
         ExtendLastCol   =   -1  'True
         FormatString    =   $"Frm_CargaVenCorta.frx":0628
         ScrollTrack     =   -1  'True
         ScrollBars      =   3
         ScrollTips      =   -1  'True
         MergeCells      =   0
         MergeCompare    =   0
         AutoResize      =   0   'False
         AutoSizeMode    =   0
         AutoSearch      =   2
         AutoSearchDelay =   2
         MultiTotals     =   -1  'True
         SubtotalPosition=   1
         OutlineBar      =   0
         OutlineCol      =   0
         Ellipsis        =   0
         ExplorerBar     =   2
         PicturesOver    =   0   'False
         FillStyle       =   0
         RightToLeft     =   0   'False
         PictureType     =   0
         TabBehavior     =   0
         OwnerDraw       =   0
         Editable        =   1
         ShowComboButton =   1
         WordWrap        =   0   'False
         TextStyle       =   0
         TextStyleFixed  =   0
         OleDragMode     =   0
         OleDropMode     =   0
         ComboSearch     =   0
         AutoSizeMouse   =   0   'False
         FrozenRows      =   0
         FrozenCols      =   0
         AllowUserFreezing=   0
         BackColorFrozen =   0
         ForeColorFrozen =   0
         WallPaperAlignment=   9
         AccessibleName  =   ""
         AccessibleDescription=   ""
         AccessibleValue =   ""
         AccessibleRole  =   24
      End
   End
   Begin VB.Frame Frm_Fecha_Proceso 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   765
      Left            =   0
      TabIndex        =   0
      Top             =   360
      Width           =   14715
      Begin VB.CommandButton cmb_buscar_Rut 
         Caption         =   "?"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   11010
         Picture         =   "Frm_CargaVenCorta.frx":0C68
         TabIndex        =   1
         Top             =   270
         Width           =   375
      End
      Begin MSMAPI.MAPISession MAPISession1 
         Left            =   13290
         Top             =   870
         _ExtentX        =   1005
         _ExtentY        =   1005
         _Version        =   393216
         DownloadMail    =   -1  'True
         LogonUI         =   -1  'True
         NewSession      =   0   'False
      End
      Begin MSComCtl2.DTPicker DTP_Fecha_Proceso 
         Height          =   315
         Left            =   1470
         TabIndex        =   2
         Top             =   270
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   21823489
         CurrentDate     =   37732
      End
      Begin MSMAPI.MAPIMessages MAPIMessages1 
         Left            =   14010
         Top             =   900
         _ExtentX        =   1005
         _ExtentY        =   1005
         _Version        =   393216
         AddressEditFieldCount=   1
         AddressModifiable=   0   'False
         AddressResolveUI=   0   'False
         FetchSorted     =   0   'False
         FetchUnreadOnly =   0   'False
      End
      Begin hControl2.hTextLabel TxtRazonSocial 
         Height          =   315
         Left            =   6105
         TabIndex        =   3
         Top             =   270
         Width           =   4860
         _ExtentX        =   8573
         _ExtentY        =   556
         LabelWidth      =   15
         Caption         =   "Nombre"
         Text            =   ""
         MaxLength       =   99
      End
      Begin hControl2.hTextLabel Txt_Rut_Cliente 
         Height          =   315
         Left            =   3240
         TabIndex        =   4
         Top             =   270
         Width           =   2955
         _ExtentX        =   5212
         _ExtentY        =   556
         LabelWidth      =   1440
         TextMinWidth    =   1300
         Caption         =   "Rut Cliente"
         Text            =   ""
         MaxLength       =   10
      End
      Begin VB.Label Lbl_Fecha_Proceso 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Fecha Proceso"
         Height          =   345
         Left            =   210
         TabIndex        =   5
         Top             =   270
         Width           =   1215
      End
   End
   Begin MSComctlLib.Toolbar Toolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   14730
      _ExtentX        =   25982
      _ExtentY        =   635
      ButtonWidth     =   1640
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Appearance      =   1
      Style           =   1
      TextAlignment   =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "STATUSBAR"
            Style           =   4
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Salir"
            Key             =   "EXIT"
            Description     =   "Cierra la ventana sin almacenar los cambios"
            Object.ToolTipText     =   "Cierra la ventana sin almacenar los cambios"
         EndProperty
      EndProperty
      BorderStyle     =   1
      Begin MSComctlLib.ProgressBar BarraProceso 
         Height          =   255
         Left            =   11040
         TabIndex        =   11
         Top             =   30
         Width           =   780
         _ExtentX        =   1376
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
         Scrolling       =   1
      End
   End
End
Attribute VB_Name = "Frm_CargaVenCorta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

 Public fKey As String
 Public fClass As cls_CargaVenCorta
 Public fForm_Container    As Object
 Const chrArchivoAdjunto = "ARCHIVO"

 Public fArchivo         As String

 ' variables para logs
 Public lMsgGrabaOk As String
 Public lGrabados, lNoGrabados As Long
 
 Public fModoAuto        As Boolean

 Public fPathLog         As String
 Public fArchivo_Log     As Byte
 Public fGeneraLog      As Boolean

 Public fFechaProceso    As Date
 Public fRutCliente      As String
 
 Dim lNombreArchivo As String
 Dim lOutputLOG     As String
 Dim lOutputLOG_ERR As String
 Dim lCount As Integer
 Dim blnFileOpen As Boolean

Dim lcAlias As Object

Dim lid_Empresa As Double


Private Sub cmb_buscar_Rut_Click()
Dim lId_Cuenta            As String
Dim lLcCuenta             As Object
    
    Set lLcCuenta = Fnt_CreateObject(cDLL_BuscadorCuentas)
    
    lId_Cuenta = NVL(lLcCuenta.Buscar_Cliente(pFiltro_RutCliente:=Txt_Rut_Cliente.Text, pBusca_Cuenta:=True), 0)
    
    If lId_Cuenta <> "" Then
        If lId_Cuenta <> "0" Then
            Call Sub_Mostrar_Cuenta(lId_Cuenta)
        End If
    End If
End Sub

Private Sub Form_Load()
Dim lreg  As hCollection.hFields

 Sub_CargaForm
 lGrabados = 0
 lNoGrabados = 0
 If Not fModoAuto Then

    Dim lFormSetting As String
    lFormSetting = GetSetting("CsGpi", "Pantallas", Me.Name, Me.Top & "/" & Me.Left)
    
     If lFormSetting <> "" Then
         Me.Top = Mid(lFormSetting, 1, InStr(lFormSetting, "/") - 1)
         Me.Left = Mid(lFormSetting, InStr(lFormSetting, "/") + 1)
     End If

     With Toolbar
     Set .ImageList = MDI_Principal.ImageListGlobal16
         .Buttons("EXIT").Image = cBoton_Salir
     End With
     
     With Toolbar_Operaciones
      Set .ImageList = MDI_Principal.ImageListGlobal16
         .Buttons("SAVE").Image = cBoton_Grabar
         .Buttons("IMPORT").Image = cBoton_Aceptar
         .Buttons("REFRESH").Image = cBoton_Original
         .Buttons("SEL_ALL").Image = "boton_seleccionar_todos"
         .Buttons("SEL_NOTHING").Image = "boton_seleccionar_ninguno"
         .Buttons("SAVE").Enabled = False
         .Buttons("REFRESH").Enabled = True
         .Buttons("SEL_ALL").Enabled = False
         .Buttons("SEL_NOTHING").Enabled = False
         .Appearance = ccFlat
     End With
     Call Sub_Limpia_Objetos
End If
End Sub
'
Private Sub Sub_CargaForm()

Dim lConf_Archivo   As String
  
  Set Proce.gDB = fClass.gDB
  
  lConf_Archivo = App.Path & "\CSG_CARGAS.ini"
  fPathLog = Trim(Get_Ini("CSG_CARGAVTACORT", lConf_Archivo, "PATH_LOG", ".\"))
  
  If Mid(fPathLog, Len(fPathLog), 1) <> "\" Then
      fPathLog = fPathLog & "\"
  End If
  
  If Not fModoAuto Then
     DTP_Fecha_Proceso.Value = Fnt_FechaServidor
     lNombreArchivo = fPathLog & "CSG_CARGA_VTA_CORT_MAN_" & Format(Fnt_SYSDATE, "yyyymmdd_hhmmss") & ".txt"
  Else
     DTP_Fecha_Proceso.Value = fFechaProceso
     Txt_Rut_Cliente.Text = fRutCliente
     lNombreArchivo = fPathLog & "CSG_CARGA_VTA_CORT_AUT_" & Format(Fnt_SYSDATE, "yyyymmdd_hhmmss") & ".txt"
  End If
  
  fGeneraLog = False
  If Trim(Get_Ini("GENERA_LOG", lConf_Archivo, "AUTOMATICO", "N")) = "S" Then
     fGeneraLog = True
  End If

  lOutputLOG_ERR = ""
  On Error Resume Next
      Kill lNombreArchivo
  On Error GoTo ErrProcedure

  'fArchivo_Log = FreeFile
  'Open lNombreArchivo For Output As #fArchivo_Log

ErrProcedure:
End Sub

Private Sub Form_Resize()
  Call Sub_ConfiguraToolBar(Toolbar, BarraProceso, Me)
End Sub

Private Sub Form_Unload(Cancel As Integer)
   'Call PubPrdGrabaPosicion(Me)
    Call SaveSetting("CsGpi", "Pantallas", Me.Name, Me.Top & "/" & Me.Left)
    Unload MDI_Principal
End Sub

Public Sub Toolbar_ButtonClick(ByVal Button As MSComctlLib.Button)

  If Not fModoAuto Then
     Me.SetFocus
     DoEvents
  End If

  Select Case Button.Key
    Case "EXIT"
         If fModoAuto Then
             Unload Me
         Else
           Call Me.fForm_Container.Sub_Unload
           Set Me.fForm_Container = Nothing
         End If
        If blnFileOpen Then
            Call Archivo_Log("", True)
            blnFileOpen = False
        End If
  End Select
End Sub

Private Sub Toolbar_Operaciones_ButtonClick(ByVal Button As MSComctlLib.Button)
  If Not fModoAuto Then
     Me.SetFocus
     DoEvents
  End If

  Select Case Button.Key
         Case "IMPORT"
               Call Sub_Importa
               BarraProceso.Value = 0
               
               If Grilla_Vta.Rows > 1 Or Grilla_Vta_Sin_Cuenta.Rows > 1 Then
                  With Toolbar_Operaciones
                      .Buttons("SAVE").Enabled = True
                      .Buttons("IMPORT").Enabled = False
                      .Buttons("SEL_ALL").Enabled = True
                      .Buttons("SEL_NOTHING").Enabled = True
                      .Buttons("REFRESH").Enabled = True
                  End With
               ElseIf Grilla_Vta.Rows = 1 And Grilla_Vta_Sin_Cuenta.Rows = 1 Then
                      MsgBox " No existen datos con criterio de busqueda, o cliente no tiene cuenta en GPI. ", vbCritical, "Venta Corta"
               End If
               
          
         Case "SEL_ALL"
               Call Sub_CambiaCheck(Grilla_Vta, True)
               
         Case "SEL_NOTHING"
               Call Sub_CambiaCheck(Grilla_Vta, False)
      
         Case "REFRESH"
               
               With Toolbar_Operaciones
                   .Buttons("SAVE").Enabled = False
                   .Buttons("IMPORT").Enabled = True
                   .Buttons("SEL_ALL").Enabled = False
                   .Buttons("SEL_NOTHING").Enabled = False
                   .Buttons("REFRESH").Enabled = True
               End With
               Call Sub_Limpia_Objetos

         Case "SAVE"
               If Sub_Graba_Operaciones_Vta_corta Then
                  With Toolbar_Operaciones
                      .Buttons("SAVE").Enabled = False
                      .Buttons("IMPORT").Enabled = True
                      .Buttons("SEL_ALL").Enabled = False
                      .Buttons("SEL_NOTHING").Enabled = False
                      .Buttons("REFRESH").Enabled = True
                  End With
               End If

  End Select
End Sub

Private Function Sub_Graba_Operaciones_Vta_corta() As Boolean
Dim lFila As Long
Dim lFilasChk As Integer

Sub_Graba_Operaciones_Vta_corta = True

lFilasChk = 0
  For lFila = 1 To Grilla_Vta.Rows - 1
      If Grilla_Vta.Cell(flexcpChecked, lFila, Grilla_Vta.ColIndex("chk")) = flexChecked Then
        lFilasChk = 1
        If Not Fnt_Ope_Vta_corta(pGrilla:=Grilla_Vta, _
                                       pFila:=lFila) Then
        End If
      End If
  Next
  
  If Not fModoAuto Then
     If lFilasChk = 0 Then
        MsgBox "Debe seleccionar una Operación.", vbExclamation, Me.Caption
        Sub_Graba_Operaciones_Vta_corta = False
     Else
        MsgBox "Grabación de Venta Corta finalizada.", vbInformation, Me.Caption
     End If
  End If
    If fGeneraLog Then
        Archivo_Log "INICIO GRABACIÓN GPI:"
        Archivo_Log "RESUMEN"
        Archivo_Log vbTab & "Total Registros Grabados      :" & lGrabados
        Archivo_Log vbTab & "Total Registros con Problemas :" & lNoGrabados
        Archivo_Log vbTab & "Total Registros               :" & lGrabados + lNoGrabados
        Archivo_Log "============================================================================================================="
        lGrabados = 0
        lNoGrabados = 0
        If fModoAuto Then
            Call Archivo_Log("", True)
            blnFileOpen = False
        End If
    End If
    Call Grilla_Vta.Select(0, 0)
    Call Grilla_Vta_Sin_Cuenta.Select(0, 0)
  
End Function

Private Function Fnt_Ope_Vta_corta(pGrilla As VSFlexGrid, pFila As Long) As Boolean
Dim lFila As Long
Dim lcAcciones As Object
Dim lRollback As Boolean
'---------------------------------------
Dim lreg As hFields
Dim lRegistro As String
 
Dim lNombre_bolsa          As String
Dim lcodigo_bolsa          As String
Dim lId_Cliente            As String
Dim lfecha_de_mvto         As String
Dim lfolio                 As Integer
Dim lTipo_Operacion        As String
Dim lCantidad              As Double
Dim lprecio_medio          As Double
Dim lValor                 As Double
Dim lTasa                  As Double
Dim lprima_a_plazo         As Double
Dim lprima_acumulada       As Double
Dim ldias_operacion        As Integer
Dim ldias_en_curso         As Integer
Dim linteres_devengado_mes As Double
Dim lFecha_Vencimiento     As String
Dim lbase_tasa             As String
Dim lprecio_medio_mercado  As Double
Dim ltipo_transaccion      As String
Dim ltipo_origen           As String
Dim lTipo_Liquidacion      As String
Dim lCod_Producto          As String
Dim lId_Cuenta             As Long
Dim lId_Nemotecnico        As Long
Dim lMsg_Error             As String
      
      
lNombre_bolsa = GetCell(pGrilla, pFila, "Nombre_bolsa")
lcodigo_bolsa = GetCell(pGrilla, pFila, "codigo_bolsa")
lId_Cliente = GetCell(pGrilla, pFila, "id_cliente")
lfecha_de_mvto = GetCell(pGrilla, pFila, "fecha_de_mvto")
lfolio = GetCell(pGrilla, pFila, "folio")
lTipo_Operacion = GetCell(pGrilla, pFila, "tipo_operacion")
lCantidad = GetCell(pGrilla, pFila, "cantidad")
lprecio_medio = GetCell(pGrilla, pFila, "precio_medio")
lValor = GetCell(pGrilla, pFila, "valor")
lTasa = GetCell(pGrilla, pFila, "tasa")
lprima_a_plazo = GetCell(pGrilla, pFila, "prima_a_plazo")
lprima_acumulada = GetCell(pGrilla, pFila, "prima_acumulada")
ldias_operacion = GetCell(pGrilla, pFila, "dias_operacion")
ldias_en_curso = GetCell(pGrilla, pFila, "dias_en_curso")
linteres_devengado_mes = GetCell(pGrilla, pFila, "interes_devengado_mes")
lFecha_Vencimiento = GetCell(pGrilla, pFila, "fecha_vencimiento")
lbase_tasa = GetCell(pGrilla, pFila, "base_tasa")
lprecio_medio_mercado = GetCell(pGrilla, pFila, "precio_medio_mercado")
ltipo_transaccion = GetCell(pGrilla, pFila, "tipo_transaccion")
ltipo_origen = GetCell(pGrilla, pFila, "tipo_origen")
lTipo_Liquidacion = GetCell(pGrilla, pFila, "tipo_liquidacion")
lCod_Producto = GetCell(pGrilla, pFila, "Cod_Producto")
lId_Cuenta = GetCell(pGrilla, pFila, "id_cuenta")
lId_Nemotecnico = GetCell(pGrilla, pFila, "ID_Nemotecnico")
      
  lRollback = False
  gDB.IniciarTransaccion
  
  lMsg_Error = ""
  gDB.Parametros.Clear
  gDB.Procedimiento = "PKG_CARGAS$INGRESA_VENTA_CORTA"
  
  gDB.Parametros.Add "pNombre_bolsa", ePT_Caracter, lNombre_bolsa, ePD_Entrada
  gDB.Parametros.Add "pcodigo_bolsa", ePT_Caracter, lcodigo_bolsa, ePD_Entrada
  gDB.Parametros.Add "pid_cliente", ePT_Caracter, lId_Cliente, ePD_Entrada
  gDB.Parametros.Add "pfecha_de_mvto", ePT_Fecha, lfecha_de_mvto, ePD_Entrada
  gDB.Parametros.Add "pfolio", ePT_Numero, lfolio, ePD_Entrada
  gDB.Parametros.Add "ptipo_operacion", ePT_Caracter, lTipo_Operacion, ePD_Entrada
  gDB.Parametros.Add "pcantidad", ePT_Numero, lCantidad, ePD_Entrada
  gDB.Parametros.Add "pprecio_medio", ePT_Numero, lprecio_medio, ePD_Entrada
  gDB.Parametros.Add "pvalor", ePT_Numero, lValor, ePD_Entrada
  gDB.Parametros.Add "ptasa", ePT_Numero, lTasa, ePD_Entrada
  gDB.Parametros.Add "pprima_a_plazo", ePT_Numero, lprima_a_plazo, ePD_Entrada
  gDB.Parametros.Add "pprima_acumulada", ePT_Numero, lprima_acumulada, ePD_Entrada
  gDB.Parametros.Add "pdias_operacion", ePT_Numero, ldias_operacion, ePD_Entrada
  gDB.Parametros.Add "pdias_en_curso", ePT_Numero, ldias_en_curso, ePD_Entrada
  gDB.Parametros.Add "pinteres_devengado_mes", ePT_Numero, linteres_devengado_mes, ePD_Entrada
  gDB.Parametros.Add "pfecha_vencimiento", ePT_Fecha, lFecha_Vencimiento, ePD_Entrada
  gDB.Parametros.Add "pbase_tasa", ePT_Caracter, lbase_tasa, ePD_Entrada
  gDB.Parametros.Add "pprecio_medio_mercado", ePT_Numero, lprecio_medio_mercado, ePD_Entrada
  gDB.Parametros.Add "pCod_Producto", ePT_Caracter, lCod_Producto, ePD_Entrada
  gDB.Parametros.Add "pid_cuenta", ePT_Numero, lId_Cuenta, ePD_Entrada
  gDB.Parametros.Add "pID_Nemotecnico", ePT_Numero, lId_Nemotecnico, ePD_Entrada
  gDB.Parametros.Add "pFecha", ePT_Caracter, Format(DTP_Fecha_Proceso.Value, "YYYYMMDD"), ePD_Entrada
  
  If Not gDB.EjecutaSP Then
     lMsg_Error = gDB.ErrMsg
     GoTo ErrProcedure
  End If
            
  lMsg_Error = " - Operación Ingresada correctamente."
  GoTo ExitProcedure
  
ErrProcedure:
  lRollback = True
  
ExitProcedure:
  
 If lRollback Then
    lNoGrabados = lNoGrabados + 1
    pGrilla.Cell(flexcpForeColor, pFila, 1, pFila, pGrilla.Cols - 1) = vbRed
    Call SetCell(pGrilla, pFila, "dsc_error", lMsg_Error, pAutoSize:=False)
    gDB.RollbackTransaccion
  Else
    lGrabados = lGrabados + 1
    lCount = lCount + 1
    pGrilla.Cell(flexcpForeColor, pFila, 1, pFila, pGrilla.Cols - 1) = vbBlue
    gDB.CommitTransaccion
  End If
  Fnt_Ope_Vta_corta = Not lRollback
  
End Function

Private Sub Sub_Limpia_Objetos()
  Grilla_Vta.Rows = 1
  Grilla_Vta_Sin_Cuenta.Rows = 1
  Txt_Rut_Cliente.Text = ""
  TxtRazonSocial.Text = ""
  lGrabados = 0
  lNoGrabados = 0
  BarraProceso.Value = 0
End Sub

Private Sub Sub_Importa()
Dim lcNemotecnicos  As Object
Dim lcCuenta        As Object
Dim lCursor         As hRecord
Dim lCampo          As hFields
'---------------------------------------
Dim lGrilla As VSFlexGrid
'---------------------------------------
Dim lId_Cuenta As Variant
Dim lFolioContraparte
Dim lId_Nemotecnico
Dim lNum_Cuenta As String
Dim lCod_Instrumento As String
Dim lNombre_Cliente As String
Dim lMsg_Error    As String
Dim lLinea        As Long
Dim lTipoInterfaz As String
Dim lNemo As String
Dim lErrorNoCuenta As String

 
 lCount = 0
 Dim lOutputLOG_CCTAS As String: Dim lOutputLOG_CCTAS_cont As Integer: lOutputLOG_CCTAS = "": lOutputLOG_CCTAS_cont = 0
 Dim lOutputLOG_SCTAS As String: Dim lOutputLOG_SCTAS_cont As Integer: lOutputLOG_SCTAS = "": lOutputLOG_SCTAS_cont = 0
 Dim lOutputLOG_SCTAS_flg As Boolean
 Dim lMsgRegError As String
'variables para formatear salida del log
 Dim lLogCabecera As String
 Dim lLogErrores As String
 Dim lProceValCar As String
 Dim lNroRegistro As String
 Dim lNomGrilla As String
 Dim lConSC As Long
 
 
On Error GoTo ErrProcedure
  lConSC = 0
  Me.Enabled = False
  Call Sub_Bloquea_Puntero(Me)
  
  Grilla_Vta.Rows = 1
  Grilla_Vta_Sin_Cuenta.Rows = 1
  
  If fGeneraLog Then
    Call AbrirLog
  End If
  
  If fModoAuto Then
     lTipoInterfaz = "Automática"
  Else
     lTipoInterfaz = "Manual"
  End If
  
  If fGeneraLog Then
    lOutputLOG = "Validación y Carga " & lTipoInterfaz & ": Importación Venta Corta" & vbCrLf
    lOutputLOG = lOutputLOG & "=============================================================================================================" & vbCrLf
    lOutputLOG = lOutputLOG & vbCrLf
    lOutputLOG = lOutputLOG & "Fecha y hora inicio: " & Format(Fnt_SYSDATE, cFormatSysdate) & vbCrLf
    lOutputLOG = lOutputLOG & "Fecha de Proceso: " & DTP_Fecha_Proceso.Value & vbCrLf
    lLogCabecera = lOutputLOG
    
    lProceValCar = lProceValCar & "-------------------------------------------------------------------------------------------------------------" & vbCrLf
    lProceValCar = lProceValCar & "INICIO VALIDACION Y CARGA:" & vbCrLf
    
  End If

  gDB.Parametros.Clear
  gDB.Procedimiento = "PKG_PROCESOS_IMPORTADOR$CisCB_VENTA_CORTA"
  gDB.Parametros.Add "pFecha_movimientos", ePT_Caracter, Format(DTP_Fecha_Proceso.Value, "YYYYMMDD"), ePD_Entrada
  
  If Trim(Txt_Rut_Cliente.Text) <> "" And Trim(TxtRazonSocial.Text) <> "" Then
     gDB.Parametros.Add "pRut_Cliente", ePT_Caracter, Trim(Txt_Rut_Cliente.Text), ePD_Entrada
  End If
  
  gDB.Parametros.Add "pcursor", ePT_Cursor, "", ePD_Ambos
  If Not gDB.EjecutaSP Then
     If Not fModoAuto Then
        MsgBox gDB.ErrMsg, vbCritical, Me.Caption
     End If
     lOutputLOG_ERR = lOutputLOG_ERR & vbCrLf & gDB.ErrMsg
     lLogErrores = lLogErrores & lOutputLOG_ERR & vbCrLf
     GoTo ExitProcedure
  End If
  
  Set lCursor = gDB.Parametros("pcursor").VALOR
    
  If lCursor.Count <= 0 Then
    GoTo ExitProcedure
  End If
  
  BarraProceso.Max = lCursor.Count
  BarraProceso.Value = BarraProceso.Min
  For Each lCampo In lCursor
      BarraProceso.Value = lCampo.Index
   
      If (lCampo("LCODRESULTADO").Value = "OK") Then
        Set lGrilla = Grilla_Vta
        lOutputLOG_CCTAS_cont = lOutputLOG_CCTAS_cont + 1
        lNomGrilla = "CONC"
      Else
        Set lGrilla = Grilla_Vta_Sin_Cuenta
        lNomGrilla = "SINC"
        lOutputLOG_SCTAS_cont = lOutputLOG_SCTAS_cont + 1
      End If

      lLinea = lGrilla.Rows
      Call lGrilla.AddItem("")
      If (fModoAuto) Then
         lGrilla.Cell(flexcpChecked, lLinea, lGrilla.ColIndex("chk")) = flexChecked
      End If
    
    
      Call SetCell(lGrilla, lLinea, "rut", Trim(lCampo("RUT_CLIENTE").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "Nombre_bolsa", Trim(lCampo("Nombre_bolsa").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "codigo_bolsa", Trim(lCampo("codigo_bolsa").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "id_cliente", Trim(lCampo("id_cliente").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "nombre_cliente", Trim(lCampo("nombre_cliente").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "fecha_de_mvto", Trim(lCampo("fecha_de_mvto").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "folio", Trim(lCampo("folio").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "tipo_operacion", Trim(lCampo("tipo_operacion").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "instrumento", Trim(lCampo("instrumento").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "cantidad", Trim(lCampo("cantidad").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "precio_medio", Trim(lCampo("precio_medio").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "valor", Trim(lCampo("valor").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "tasa", Trim(lCampo("tasa").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "prima_a_plazo", Trim(lCampo("prima_a_plazo").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "prima_acumulada", Trim(lCampo("prima_acumulada").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "dias_operacion", Trim(lCampo("dias_operacion").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "dias_en_curso", Trim(lCampo("dias_en_curso").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "interes_devengado_mes", Trim(lCampo("interes_devengado_mes").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "fecha_vencimiento", Trim(lCampo("fecha_vencimiento").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "correlativo", Trim(lCampo("correlativo").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "base_tasa", Trim(lCampo("base_tasa").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "precio_medio_mercado", Trim(lCampo("precio_medio_mercado").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "tipo_transaccion", Trim(lCampo("tipo_transaccion").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "tipo_origen", Trim(lCampo("tipo_origen").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "tipo_persona", Trim(lCampo("tipo_persona").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "tipo_liquidacion", Trim(lCampo("tipo_liquidacion").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "cuenta_gpi", Trim(lCampo("cuenta_gpi").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "Mandato", Trim(lCampo("Mandato").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "Cod_Producto", Trim(lCampo("Cod_Producto").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "id_cuenta", Trim(lCampo("id_cuenta").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "ID_Nemotecnico", Trim(lCampo("ID_Nemotecnico").Value), pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "nombre_cliente", lCampo("NombreCliente").Value, pAutoSize:=False)
      Call SetCell(lGrilla, lLinea, "Empresa", lCampo("DSC_EMPRESA").Value, pAutoSize:=False)
      
      
      If lNomGrilla = "SINC" Then
         Call SetCell(lGrilla, lLinea, "lMsgResultado", lCampo("lMsgResultado").Value, pAutoSize:=False)
      End If

ProximoMovimiento:
    'este label sirve para saltarce el ingreso de un de operacion con un tipo de movimiento que no corresponde,
    'como los ingresos de custodias, la otra forma es hacer un if gigante que no quiero hacer :-P
    'HAF: 20071108
  Next
  If Not fModoAuto Then
    Call Sub_AjustaColumnas_Grilla(Grilla_Vta)
    Call Sub_AjustaColumnas_Grilla(Grilla_Vta_Sin_Cuenta)
  End If
ErrProcedure:

  If Not Err.Number = 0 Then
    Call Fnt_MsgError(eLog_Subtipo.eLS_ErrSystem _
                     , "Problemas en la importación de Venta Corta." _
                     , Err.Description)
    lOutputLOG_ERR = lOutputLOG_ERR & vbCrLf & "Problemas en la importación de Venta Corta*"
    lLogErrores = lLogErrores & lMsgRegError & "Problemas en la importación de Venta Corta*" & vbCrLf
    GoTo ExitProcedure
    Resume
  End If

ExitProcedure:
  If fGeneraLog Then
    lOutputLOG = lOutputLOG & "------------------------------------------------------------------" & vbCrLf
    lOutputLOG = lOutputLOG & "OPERACIONES SIN CONFLICTOS: " & vbCrLf
    lOutputLOG = lOutputLOG & lOutputLOG_CCTAS
    lOutputLOG = lOutputLOG & "Total registros importados (Operaciones Sin Conflictos): " & Format(lOutputLOG_CCTAS_cont, "#,##0") & vbCrLf & vbCrLf
    lOutputLOG = lOutputLOG & "------------------------------------------------------------------" & vbCrLf
    lOutputLOG = lOutputLOG & "OPERACIONES CON CONFLICTOS" & vbCrLf
    lOutputLOG = lOutputLOG & lOutputLOG_SCTAS
    lOutputLOG = lOutputLOG & "Total registros importados (Operaciones Sin Conflictos): " & Format(lOutputLOG_SCTAS_cont, "#,##0") & vbCrLf & vbCrLf
    lOutputLOG = lOutputLOG & "------------------------------------------------------------------" & vbCrLf
    lOutputLOG = lOutputLOG & "Importación de Venta Corta" & vbCrLf
    lOutputLOG = lOutputLOG & "Total registros importados: " & Format(lOutputLOG_CCTAS_cont + lOutputLOG_SCTAS_cont, "#,##0") & vbCrLf
    If Trim(lOutputLOG_ERR) <> "" Then
       lOutputLOG = lOutputLOG & "------------------------------------------------------------------" & vbCrLf
       lOutputLOG = lOutputLOG & "------------------------------------------------------------------" & vbCrLf
    End If
      'Archivo_Log lOutputLOG
    lLogErrores = lLogErrores & IIf(lLogErrores = "", lLogErrores & "FIN VALIDACIÓN Y CARGA", vbCrLf & "FIN VALIDACIÓN Y CARGA") & vbCrLf
    Archivo_Log lLogCabecera
    Archivo_Log lProceValCar
    Archivo_Log lLogErrores
    Archivo_Log "RESUMEN" & vbCrLf
    Archivo_Log vbTab & "Total Registros válidos       : " & lOutputLOG_CCTAS_cont
    Archivo_Log vbTab & "Total Registros con Problemas : " & lOutputLOG_SCTAS_cont
    Archivo_Log vbTab & "Total Registros               : " & lOutputLOG_CCTAS_cont + lOutputLOG_SCTAS_cont & vbCrLf
    lOutputLOG = ""
    lOutputLOG_ERR = ""
    
      If Grilla_Vta.Rows = 1 And Grilla_Vta_Sin_Cuenta.Rows = 1 Then
          Archivo_Log "============================================================================================================="
      End If
      
      If lConSC > 0 Then
          If Not fModoAuto Then
             If Grilla_Vta.Rows = 1 Then
                Archivo_Log "============================================================================================================="
             Else
                Archivo_Log "-------------------------------------------------------------------------------------------------------------"
             End If
          Else
             Archivo_Log "-------------------------------------------------------------------------------------------------------------"
          End If
      Else
          Archivo_Log "-------------------------------------------------------------------------------------------------------------"
      End If
  End If
    
  Set lcCuenta = Nothing
  Set lcNemotecnicos = Nothing
  Set lcAlias = Nothing
  gDB.Parametros.Clear
  
  Me.Enabled = True
  Call Sub_Desbloquea_Puntero(Me)

End Sub

Public Sub Archivo_Log(pErrMsg As String, Optional pExit As Boolean = False)
    If Not blnFileOpen Then Exit Sub
    Print #fArchivo_Log, pErrMsg
    If pExit Then
        Close #fArchivo_Log
    End If
End Sub
Private Sub AbrirLog()

If Not blnFileOpen Then
 
 fArchivo_Log = FreeFile
 Open lNombreArchivo For Output As #fArchivo_Log
 blnFileOpen = True
 
End If

End Sub

Private Sub Txt_Rut_Cliente_KeyPress(KeyAscii As Integer)
  
  TxtRazonSocial.Text = ""
  
  If KeyAscii = vbKeyReturn Then
     Call cmb_buscar_Rut_Click
  End If
End Sub

Public Sub Sub_Mostrar_Cuenta(pCuenta, Optional pId As Boolean = True, Optional pDesdeTxtCuentas As Boolean = False)
Dim lCursor As hRecord
Dim lError  As String
Dim lexiste As Boolean

  Call Sub_Entrega_DatosCuenta(pCuenta, pId, lCursor, lError, lexiste)
  If lexiste Then
     Txt_Rut_Cliente.Text = lCursor(1)("rut_cliente").Value
     TxtRazonSocial.Text = lCursor(1)("nombre_cliente").Value
  End If
End Sub

Public Sub Sub_Proceso_Lectura_Automatica()
  Call Sub_Bloquea_Puntero(Me)
  Call Sub_Importa
  Call Sub_Graba_Operaciones_Vta_corta
  Call Sub_Desbloquea_Puntero(Me)
End Sub

Public Function Mostrar() As Boolean
    Mostrar = False
On Error Resume Next
  If Not fModoAuto Then
      Call Form_Resize
      Me.Show
  End If
On Error GoTo 0
    Mostrar = True
End Function
