IF  EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[PKG_APORTE_RESCATE_CTA$BuscarAportesRetirosCapital]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[PKG_APORTE_RESCATE_CTA$BuscarAportesRetirosCapital]
GO

CREATE PROCEDURE [dbo].[PKG_APORTE_RESCATE_CTA$BuscarAportesRetirosCapital]
( @Pid_Cuenta        FLOAT(53) = null
, @Pid_asesor        FLOAT(53) = null
, @pid_empresa       NUMERIC
, @pfechaIni         DATETIME = null
, @pfechaFin         DATETIME = null
, @pId_Moneda_Salida NUMERIC = NULL
, @pTipoConsulta     CHAR(1)
, @pSoloLectura		 CHAR(2))
AS
SET NOCOUNT ON;
DECLARE @lId_Moneda_Salida NUMERIC
SELECT @lId_Moneda_Salida = dbo.FNT_DAMEIDMONEDA('$$')
SET @pid_moneda_salida = isnull(@pId_Moneda_Salida,@lId_Moneda_Salida)
DECLARE @LTIPO AS VARCHAR(20)

DECLARE @salida TABLE(FECHA_MOVIMIENTO    DATETIME
                    , RUT_CLIENTE         VARCHAR(10)
                    , NOMBRE_CLIENTE      VARCHAR(152)
                    , NUM_CUENTA          FLOAT(53)
                    , DSC_CUENTA          VARCHAR(20)
                    , ID_CUENTA           FLOAT(53)
                    , ID_ASESOR           NUMERIC(4)
                    , NOMBRE_ASESOR       VARCHAR(50)
                    , FLG_TIPO_MOVIMIENTO VARCHAR(1)
                    , TIPO_MOVIMIENTO     VARCHAR(100)
                    , ID_MONEDA           NUMERIC(10)
                    , MONTO               FLOAT(53)
                    , OBSERVACION         VARCHAR(150))

DECLARE @salida2 TABLE(FECHA_MOVIMIENTO     DATETIME
                    , MES                   NUMERIC
                    , RUT_CLIENTE           VARCHAR(10)
                    , NOMBRE_CLIENTE        VARCHAR(152)
                    , NUM_CUENTA            FLOAT(53)
                    , DSC_CUENTA            VARCHAR(20)
                    , ID_CUENTA             FLOAT(53)
                    , ID_ASESOR             NUMERIC(4)
                    , NOMBRE_ASESOR         VARCHAR(50)
                    , FLG_TIPO_MOVIMIENTO   VARCHAR(1)
                    , TIPO_MOVIMIENTO       VARCHAR(100)
                    , ID_MONEDA             NUMERIC(10)
                    , MONTO_APORTE          FLOAT(53)
                    , MONTO_RETIRO          FLOAT(53)
                    , MONTO_CONSIGNO        FLOAT(53)
                    , OBSERVACION           VARCHAR(150))

SELECT @LTIPO = T.DESCRIPCION_CORTA
FROM TIPO_CUENTAS T
    , CUENTAS C
WHERE T.ID_TIPOCUENTA = C.ID_TIPOCUENTA

IF @LTIPO <> 'APV'
    SET @LTIPO='ADC'

INSERT @salida
    SELECT APORES.FECHA as FECHA_MOVIMIENTO
        , CUE.RUT_CLIENTE
        , CUE.NOMBRE_CLIENTE
        , CUE.NUM_CUENTA
        , CUE.DSC_CUENTA
        , CUE.ID_CUENTA
        , CUE.ID_ASESOR
        , CUE.NOMBRE_ASESOR
        , APORES.TIPO_MOVIMIENTO AS FLG_TIPO_MOVIMIENTO
        , DBO.Pkg_Global$Dame_Glosa_Tipo_Documento( APORES.TIPO_MOVIMIENTO, 'S', @LTIPO) AS TIPO_MOVIMIENTO
        , CJ.ID_MONEDA
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CUE.id_Cuenta, DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CUE.id_Cuenta, APORESDET.MONTO, CJ.ID_MONEDA, dbo.FNT_DAMEIDMONEDA('$$'), ARC.FECHA_MOVIMIENTO), dbo.FNT_DAMEIDMONEDA('$$'), @pId_Moneda_Salida, APORES.FECHA) AS MONTO
        , MCJ.DSC_MOV_CAJA AS OBSERVACION
    FROM APORTES_RETIROS_1862 APORES
        , APORTE_RESCATE_CUENTA ARC
        , CAJAS_CUENTA CJ
        , VIEW_CUENTAS_VIGENTES CUE
        , DETALLE_APORTES_RETIROS_1862 APORESDET
        , VIEW_MOV_CAJA MCJ
    WHERE (CUE.id_cuenta = isnull(@Pid_Cuenta, CUE.id_cuenta))
        AND CUE.id_empresa = @pId_Empresa
        AND (CUE.id_asesor = isnull(@Pid_Asesor, CUE.id_asesor))
        --AND CUE.ID_TIPOCUENTA = 1
        AND (APORES.FECHA >= @pFechaIni and APORES.FECHA <= DBO.FNT_EntregaFechaCierreCuenta_ARC(CUE.id_cuenta, @pFechaFin, @pSoloLectura))
        AND ARC.id_cuenta = CUE.ID_CUENTA
        AND (ARC.ID_APO_RES_CUENTA = APORES.ID_APO_RES_CUENTA)
        AND APORES.ID_APO_RES_CUENTA IS NOT NULL
        AND ARC.ID_CAJA_CUENTA = CJ.ID_CAJA_CUENTA
        AND APORESDET.ID_COMPROBANTE = APORES.ID_COMPROBANTE
        AND APORES.COD_ESTADO <> 'A'
        AND MCJ.id_mov_caja = ARC.id_mov_caja
        AND MCJ.COD_ORIGEN_MOV_CAJA NOT IN ('CARABO_INT','APORES_INT')

INSERT @salida
    SELECT APORES.FECHA AS FECHA_MOVIMIENTO
        , CUE.RUT_CLIENTE
        , CUE.NOMBRE_CLIENTE
        , CUE.NUM_CUENTA
        , CUE.DSC_CUENTA
        , CUE.ID_CUENTA
        , CUE.ID_ASESOR
        , CUE.NOMBRE_ASESOR
        , APORES.TIPO_MOVIMIENTO AS FLG_TIPO_MOVIMIENTO
        , DBO.Pkg_Global$Dame_Glosa_Tipo_Documento( APORES.TIPO_MOVIMIENTO, 'N', @LTIPO) as TIPO_MOVIMIENTO
        , OPE.ID_MONEDA_OPERACION AS ID_MONEDA
        , DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CUE.id_Cuenta, DBO.FNT_PKG_TIPO_CAMBIO$Cambio_Paridad(CUE.id_Cuenta, APORESDET.MONTO, OPE.ID_MONEDA_OPERACION, dbo.FNT_DAMEIDMONEDA('$$'), APORES.FECHA), dbo.FNT_DAMEIDMONEDA('$$'), @pId_Moneda_Salida, APORES.FECHA) AS MONTO
        , CASE  WHEN (APORES.TIPO_MOVIMIENTO = 'I') THEN 'INGRESO CUSTODIA ' + aporesdet.nemotecnico
            WHEN (APORES.TIPO_MOVIMIENTO = 'E') THEN 'RETIRO CUSTODIA ' + aporesdet.nemotecnico
            ELSE ''
        END AS OBSERVACION
    FROM APORTES_RETIROS_1862 APORES
        , OPERACIONES OPE
        , VIEW_CUENTAS_VIGENTES CUE
        , DETALLE_APORTES_RETIROS_1862 APORESDET
    WHERE (CUE.id_cuenta = isnull(@Pid_Cuenta, CUE.id_cuenta))
        AND CUE.id_empresa = @pId_Empresa
        AND (CUE.id_asesor = isnull(@Pid_Asesor, CUE.id_asesor))
        --AND CUE.ID_TIPOCUENTA = 1
        AND OPE.id_cuenta = CUE.ID_CUENTA
        AND APORES.ID_OPERACION = OPE.ID_OPERACION
        AND (APORES.FECHA between @pFechaIni and DBO.FNT_EntregaFechaCierreCuenta_ARC(CUE.id_cuenta, @pFechaFin, @pSoloLectura) )
        AND APORES.ID_OPERACION IS NOT NULL
        and APORESDET.ID_COMPROBANTE = APORES.ID_COMPROBANTE
        AND APORES.COD_ESTADO <> 'A'

INSERT INTO @salida2
    SELECT FECHA_MOVIMIENTO
        , MONTH(FECHA_MOVIMIENTO) AS MES
        , RUT_CLIENTE
        , NOMBRE_CLIENTE
        , NUM_CUENTA
        , DSC_CUENTA
        , ID_CUENTA
        , ID_ASESOR
        , NOMBRE_ASESOR
        , FLG_TIPO_MOVIMIENTO
        , TIPO_MOVIMIENTO
        , ID_MONEDA
        , CASE  WHEN (TIPO_MOVIMIENTO = 'APORTE') THEN MONTO
             ELSE 0
        END AS MONTO_APORTE
        , CASE  WHEN (TIPO_MOVIMIENTO = 'RETIRO') THEN MONTO
            ELSE 0
        END AS MONTO_RETIRO
        , CASE  WHEN (TIPO_MOVIMIENTO = 'APORTE') THEN (CAST(MONTO as float) * 1)
            WHEN (TIPO_MOVIMIENTO = 'RETIRO') THEN (CAST(MONTO as float) * -1)
            ELSE 0
        END AS MONTO_CONSIGNO
        , OBSERVACION
    FROM @salida

IF @pTipoConsulta = 'D'  --Detalle
    SELECT     FECHA_MOVIMIENTO, MES, RUT_CLIENTE, NOMBRE_CLIENTE, NUM_CUENTA, DSC_CUENTA, ID_CUENTA, ID_ASESOR, NOMBRE_ASESOR
			 , FLG_TIPO_MOVIMIENTO, TIPO_MOVIMIENTO, ID_MONEDA, MONTO_APORTE, MONTO_RETIRO, MONTO_CONSIGNO, OBSERVACION
			 , CASE
			   WHEN FLG_TIPO_MOVIMIENTO IN ('A', 'I') THEN dbo.Fnt_PrimerAporte(FECHA_MOVIMIENTO, ID_CUENTA)
			   ELSE 0
			   END 'FLG_PRIMERAPORTE'
    FROM @salida2
    ORDER BY FECHA_MOVIMIENTO
ELSE
    IF @pTipoConsulta = 'M' -- Resumen Mensual
        SELECT MES
            , NOMBRE_ASESOR
            , SUM(MONTO_APORTE) AS MONTO_APORTE
            , SUM(MONTO_RETIRO) AS MONTO_RETIRO
            , SUM(MONTO_CONSIGNO) AS MONTO_NETO
        FROM @salida2
        GROUP BY MES, NOMBRE_ASESOR
        ORDER BY MES, NOMBRE_ASESOR
    ELSE
        SELECT NOMBRE_ASESOR
            , SUM(MONTO_APORTE) AS MONTO_APORTE
            , SUM(MONTO_RETIRO) AS MONTO_RETIRO
            , SUM(MONTO_CONSIGNO) AS MONTO_NETO
        FROM @salida2
        GROUP BY NOMBRE_ASESOR
        ORDER BY NOMBRE_ASESOR

SET NOCOUNT OFF;
GO
GRANT EXECUTE ON [PKG_APORTE_RESCATE_CTA$BuscarAportesRetirosCapital] TO DB_EXECUTESP
GO
