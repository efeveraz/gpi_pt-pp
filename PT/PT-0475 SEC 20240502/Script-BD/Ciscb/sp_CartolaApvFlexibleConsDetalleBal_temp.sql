IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_CartolaApvFlexibleConsDetalleBal_temp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_CartolaApvFlexibleConsDetalleBal_temp]
GO

CREATE PROCEDURE [DBO].[sp_CartolaApvFlexibleConsDetalleBal_temp]
 @RUTCLIENTE      VARCHAR(15)
,@FECHADESDE      VARCHAR(8)
,@FECHAHASTA      VARCHAR(8)
,@CUENTAENTRADA   INT
,@CODERR          INT            OUTPUT
,@MSGERR          VARCHAR(4000)  OUTPUT
AS
BEGIN

   SET NOCOUNT ON;
/*

DECLARE
@RUTCLIENTE      VARCHAR(15)
,@FECHADESDE      VARCHAR(8)
,@FECHAHASTA      VARCHAR(8)
,@CUENTAENTRADA   INT
,@CODERR          INT
,@MSGERR          VARCHAR(4000)

SET @RUTCLIENTE     = '76258327-5'
SET @FECHADESDE      ='20170531'
SET @FECHAHASTA     ='20170611'
SET @CUENTAENTRADA   =1929
SET @CODERR          =0
SET @MSGERR         =''
 */
   --***********************************************************************************************
   -- DECLARACION DE VARIABLES
   DECLARE @UF1                            NUMERIC(10,2)
   DECLARE @UF2                            NUMERIC(10,2)
   DECLARE @DOLAR1                         NUMERIC(10,2)
   DECLARE @DOLAR2                         NUMERIC(10,2)
   DECLARE @CONSOLIDADO                    VARCHAR(10)
   DECLARE @ID_CLIENTE                     INT
   DECLARE @ID_EMPRESA                     INT
   DECLARE @ID_MONEDA                      INT
   DECLARE @ID_TIPOCUENTA                  INT
   DECLARE @CONT                           INT
   DECLARE @CANTIDAD                       INT
   DECLARE @ID_ARBOL_CLASE_INST_HOJA       INT
   DECLARE @DSC_ARBOL_CLASE_INST           VARCHAR(100)
   DECLARE @DSC_ARBOL_CLASE_INST_HOJA      VARCHAR(100)
   DECLARE @SALDO_ANTERIOR                 FLOAT
   DECLARE @SALDO                          FLOAT
   DECLARE @VECNOM                         VARCHAR(MAX)
   DECLARE @VECMTO                         VARCHAR(MAX)
   DECLARE @ID_CUENTA                      INT
   DECLARE @PID_CAJA_CUENTA                INT
   DECLARE @PID_MONEDA_SALIDA              INT
   DECLARE @ID_CAJA_CUENTA                 INT
   DECLARE @CANTIDAD2                      INT
   DECLARE @CONT2                          INT
   DECLARE @SALDO_PRIMER                   FLOAT
   DECLARE @ID_ARBOL_CLASE_INST_HOJA_ELM   INT
   DECLARE @CODIGO                         VARCHAR(50)
   DECLARE @DECIMALES                      INT
   DECLARE @DESC_CAJA_CUENTA               VARCHAR(100)
   DECLARE @ID_ARBOL_SEC_ECONOMICO         INT
   DECLARE @FECHA_DESDE_CAJA               VARCHAR(8)

   --***********************************************************************************************
   -- DECLARACION DE TABLAS

   DECLARE @INDICADORES TABLE (ID_TIPO_CAMBIO    INT
                              ,DSC_MONEDA        VARCHAR(50)
                              ,VALOR             NUMERIC(18,2)
                              ,DSC_TIPO_CAMBIO   VARCHAR(100)
                              ,ABR_TIPO_CAMBIO   VARCHAR(30) )

   DECLARE @INDICADORES_SALIDA TABLE (ID_TABLA       INT      IDENTITY(1,1)
                                     ,DESCRIPCION    VARCHAR(30)
                                     ,VALOR1         VARCHAR(30)
                                     ,VALOR2         VARCHAR(30) )

 ---*********************
   DECLARE @CUENTAS TABLE (
                           ID                            INT IDENTITY(1,1),
                           ID_CUENTA                     INT,
                           ID_CLIENTE                    INT,
                           ID_TIPO_ESTADO                INT,
                           COD_ESTADO                    VARCHAR(1),
                           COD_TIPO_ADMINISTRACION       VARCHAR(10),
                           ID_CONTRATO_CUENTA            INT,
                           ID_EMPRESA                    INT,
                           ID_ASESOR                     INT,
                           ID_MONEDA                     INT,
                           ID_PERFIL_RIESGO              INT,
                           NUM_CUENTA                    INT,
                           ABR_CUENTA                    VARCHAR(30),
                           DSC_CUENTA                    VARCHAR(100),
                           OBSERVACION                   VARCHAR(100),
                           FLG_BLOQUEADO                 VARCHAR(1),
                           OBS_BLOQUEO                   VARCHAR(100),
                           FLG_IMP_INSTRUCCIONES         VARCHAR(1),
                           FECHA_OPERATIVA               DATETIME,
                           FLG_MOV_DESCUBIERTOS          VARCHAR(1),
                           ID_USUARIO_INSERT             INT,
                           FCH_INSERT                    DATETIME,
                           ID_USUARIO_UPDATE             INT,
                           FCH_UPDATE                    DATETIME,
                           PORCEN_RF                     NUMERIC(18,4),
                           PORCEN_RV                     NUMERIC(18,4),
                           FECHA_CIERRE_CUENTA           DATETIME,
                           FECHA_CONTRATO                DATETIME,
                           ID_TIPOCUENTA                 INT,
                           NUMERO_FOLIO                  INT,
                           TIPO_AHORRO                   INT,
                           FLG_CONSIDERA_COM_VC          VARCHAR(1),
                           FLG_COMISION_AFECTA_IMPUESTO  VARCHAR(1),
                           CODIGO_EXTERNO_CUENTA         INT )


   --*************************


   DECLARE @ARBOL   TABLE (ID_TABLA                  INT IDENTITY(1,1),
                           ID_ARBOL_CLASE_INST       INT,
                           ID_ARBOL_CLASE_INST_HOJA  INT,
                           DSC_ARBOL_CLASE_INST      VARCHAR(100),
                           DSC_ARBOL_CLASE_INST_HOJA VARCHAR(100),
                           MONTO_MON_CTA_INST        BIGINT,
                           MONTO_MON_CTA_HOJA        BIGINT,
                           CODIGO                    VARCHAR(10),
                           NIVEL                     INT ,
                           CODIGO_PADRE              VARCHAR(50) )

   DECLARE   @SALDOSACTIVOS TABLE(ORIGEN                            VARCHAR(50)
                                , ID_CUENTA                         NUMERIC
                                , FECHA_CIERRE                      DATETIME
                                , ID_NEMOTECNICO                    NUMERIC
                                , NEMOTECNICO                       VARCHAR(50)
                                , EMISOR                            VARCHAR(100)
                                , COD_EMISOR                        VARCHAR(10)
                                , DSC_NEMOTECNICO                   VARCHAR(120)
                                , TASA_EMISION_2                    NUMERIC(18,4)
                                , CANTIDAD                          NUMERIC(18,4)
                                , PRECIO                            NUMERIC(18,6)
                                , TASA_EMISION                      NUMERIC(18,4)
                                , FECHA_VENCIMIENTO                 DATETIME
                                , PRECIO_COMPRA                     NUMERIC(18,4)
                                , TASA                              NUMERIC(18,4)
                                , TASA_COMPRA                       NUMERIC(18,4)
                                , MONTO_VALOR_COMPRA                NUMERIC(18,4)
                                , MONTO_MON_CTA                     NUMERIC(18,4)
                                , ID_MONEDA_CTA                     NUMERIC
                                , ID_MONEDA_NEMOTECNICO             NUMERIC
                                , SIMBOLO_MONEDA                    VARCHAR(3)
                                , MONTO_MON_NEMOTECNICO             NUMERIC(18,4)
                                , MONTO_MON_ORIGEN                  NUMERIC(18,4)
                                , ID_EMPRESA                        NUMERIC
                                , ID_ARBOL_CLASE_INST               NUMERIC
                                , COD_INSTRUMENTO                   VARCHAR(15)
                                , DSC_ARBOL_CLASE_INST              VARCHAR(100)
                                , PORCENTAJE_RAMA                   NUMERIC(18,4)
                                , PRECIO_PROMEDIO_COMPRA            NUMERIC(18,4)
                                , MONTO_PROMEDIO_COMPRA             NUMERIC(18,4)
                                , DSC_PADRE_ARBOL_CLASE_INST        VARCHAR(100)
                                , RENTABILIDAD                      FLOAT
                                , DIAS                              NUMERIC
                                , COD_PRODUCTO                      VARCHAR(10)
                                , ID_PADRE_ARBOL_CLASE_INST         NUMERIC
                                , DURATION                          FLOAT
                                , DECIMALES_MOSTRAR                 NUMERIC
                                , DSC_CLASIFICADOR_RIESGO           VARCHAR(50)
                                , ID_SUBFAMILIA                     NUMERIC
                                , COD_SUBFAMILIA                    VARCHAR(50)
                                , CODIGO                            VARCHAR(20)
                                , MONTO_INVERTIDO                   NUMERIC(18,6)
                                , VALOR_MERCADO                     NUMERIC(18,6)
                                , UTILIDAD_PERDIDA                  NUMERIC(18,6)
                                , NUMERO_CONTRATO                   VARCHAR(50)
                                , TIPO_FWD                          VARCHAR(10)
                                , MONEDA_EMISION                    VARCHAR(10)
                                , MODALIDAD                         VARCHAR(15)
                                , UNIDAD_ACTIVO_SUBYACENTE          VARCHAR(10)
                                , DECIMALES_ACTIVO_SUBYACENTE       NUMERIC
                                , VALOR_NOMINAL                     NUMERIC(18,4)
                                , FECHA_INICIO                      DATETIME
                                , PRECIO_PACTADO                    NUMERIC(18,6)
                                , VALOR_PRECIO_PACTADO              NUMERIC(18,4)
                                , MONTO_MON_USD                     NUMERIC(18,6) )

   --*** RENTA VARIABLE NACIONAL
   DECLARE @RENTAVARIABLE TABLE (ID_TABLA               INT            IDENTITY(1,1),
                                 ORIGEN                 VARCHAR(50),
                                 TIPOINSTRUMENTO        VARCHAR(50),
                                 NEMOTECNICO            VARCHAR(50),
                                 MONEDA                 VARCHAR(10),
                                 PORCENTAJEINVERSION    NUMERIC(6,2),
                                 CANTIDAD               NUMERIC(18,4), --INT,
                                 PRECIOCOMPRA           NUMERIC(18,4),
                                 MONTOCOMPRA            NUMERIC(18,4),-- BIGINT
                                 PRECIOACTUAL           NUMERIC(18,4),
                                 VALORMERCADO           NUMERIC(18,6),  -- BIGINT,
                                 MONTO_MON_ORIGEN       NUMERIC(18,6),   --BIGINT,
                                 RENTABILIDAD           NUMERIC(18,4),
                                 COD_PRODUCTO           VARCHAR(10),
                                 DURATION               FLOAT,
                                 CODIGO                 VARCHAR(20) )


   DECLARE @RVOTROSACTIVOSFINV TABLE (ID_TABLA               INT            IDENTITY(1,1),
                                      ORIGEN                 VARCHAR(50),
                                      TIPOINSTRUMENTO        VARCHAR(50),
                                      NEMOTECNICO            VARCHAR(50),
                                      MONEDA                 VARCHAR(10),
                                      PORCENTAJEINVERSION    NUMERIC(6,2),
                                      CANTIDAD               NUMERIC(18,4),
                                      PRECIOCOMPRA           NUMERIC(18,4),
                                      MONTOCOMPRA            NUMERIC(18,4),
                                      PRECIOACTUAL           NUMERIC(18,4),
                                      VALORMERCADO           NUMERIC(18,6),
                                      MONTO_MON_ORIGEN       NUMERIC(18,6),
                                      RENTABILIDAD           NUMERIC(18,4),
                                      COD_PRODUCTO           VARCHAR(10),
                                      DURATION               FLOAT,
                                      CODIGO                 VARCHAR(20) )

  --******RENTA VARIABLE INTERNACIONAL ACCIONES, FONDOS MUTUOS, ETFS
   DECLARE @RENTAVARIABLEINTERNACIONAL TABLE (ID_TABLA               INT            IDENTITY(1,1),
                                              ORIGEN                 VARCHAR(50),
                                              TIPOINSTRUMENTO        VARCHAR(100),
                                              NEMOTECNICO            VARCHAR(350),
                                              MONEDA                 VARCHAR(10),
                                              PORCENTAJEINVERSION    NUMERIC(6,2),
                                              CANTIDAD               NUMERIC(18,4),
                                              PRECIOCOMPRA           NUMERIC(18,4),
                                              MONTOCOMPRA            NUMERIC(18,4),
                                              PRECIOACTUAL           NUMERIC(18,4),
                                              VALORMERCADO           NUMERIC(18,4),
                                              MONTO_MON_ORIGEN       NUMERIC(18,4),
                                              RENTABILIDAD           NUMERIC(18,4),
                                              COD_PRODUCTO           VARCHAR(10),
                                              DURATION               FLOAT,
                                              CODIGO                 VARCHAR(20),
                                              MONTOINVERTIDO         NUMERIC(18,4),
                                              UTILIDADPERDIDA        NUMERIC(18,4),
                                              VALORIZACIONUSD        NUMERIC(18,4),
                                              VALORIZACIONPESO       NUMERIC(18,4) )


  --***RENATA FIJA NACIONAL FONDOS LARGO Y CORTO ,ESTRUCTURADO,INVERSIONES, VENTA CORTA
   DECLARE @RENTAFIJAFONDOS TABLE (ID_TABLA               INT            IDENTITY(1,1),
                                   ORIGEN                 VARCHAR(50),
                                   TIPOINSTRUMENTO        VARCHAR(50),
                                   NEMOTECNICO            VARCHAR(350),
                                   MONEDA                 VARCHAR(10),
                                   PORCENTAJEINVERSION    NUMERIC(6,2),
                                   CANTIDAD               NUMERIC(18,4),
                                   PRECIOCOMPRA           NUMERIC(18,4),
                                   MONTOCOMPRA            BIGINT,
                                   PRECIOACTUAL           NUMERIC(18,4),
                                   VALORMERCADO           NUMERIC(18,4),
                                   MONTO_MON_ORIGEN       NUMERIC(18,4),
                                   MONTO_MON_NEMOTECNICO  NUMERIC(18,4),
                                   RENTABILIDAD           NUMERIC(18,4),
                                   COD_PRODUCTO           VARCHAR(10),
                                   DURATION               FLOAT,
                                   CODIGO                 VARCHAR(20) )



   --***RENATA FIJA INTERNACIONAL BONOS ,FONDOS MUTUOS,ETFS
   DECLARE @RENTAFIJAFONDOSINTER TABLE (ID_TABLA               INT            IDENTITY(1,1),
                                        ORIGEN                 VARCHAR(50),
                                        TIPOINSTRUMENTO        VARCHAR(50),
                                        NEMOTECNICO            VARCHAR(350),
                                        MONEDA                 VARCHAR(10),
                                        PORCENTAJEINVERSION    NUMERIC(6,2),
                                        CANTIDAD               NUMERIC(18,4),
                                        PRECIOCOMPRA           NUMERIC(18,4),
                                        MONTOCOMPRA            NUMERIC(18,4),
                                        PRECIOACTUAL           NUMERIC(18,4),
                                        VALORMERCADO           NUMERIC(18,4),
                                        MONTO_MON_ORIGEN       NUMERIC(18,4),
                                        MONTO_MON_NEMOTECNICO  NUMERIC(18,4),
                                        RENTABILIDAD           NUMERIC(18,4),
                                        COD_PRODUCTO           VARCHAR(10),
                                        DURATION               FLOAT,
                                        CODIGO                 VARCHAR(20),
                                        MONTOINVERTIDO         NUMERIC(18,4),
                                        UTILIDADPERDIDA        NUMERIC(18,4),
                                        VALORIZACIONUSD        NUMERIC(18,4),
                                        VALORIZACIONPESO       NUMERIC(18,4) )

 --FONDOS MIXTOS
   DECLARE @RENTAFONDOSMIXTOS TABLE (ID_TABLA               INT            IDENTITY(1,1),
                                     ORIGEN                 VARCHAR(50),
                                     TIPOINSTRUMENTO        VARCHAR(50),
                                     NEMOTECNICO            VARCHAR(350),
                                     MONEDA                 VARCHAR(10),
                                     PORCENTAJEINVERSION    NUMERIC(6,2),
                                     CANTIDAD               NUMERIC(18,4), --INT,
                                     PRECIOCOMPRA           NUMERIC(18,4),
                                     MONTOCOMPRA            BIGINT,
                                     PRECIOACTUAL           NUMERIC(18,4),
                                     VALORMERCADO           BIGINT,
                                     MONTO_MON_ORIGEN       NUMERIC(18,4),
                                     RENTABILIDAD           NUMERIC(18,4),
                                     COD_PRODUCTO           VARCHAR(10),
                                     DURATION               FLOAT,
                                     CODIGO                 VARCHAR(20) )

--RENTA FIJA NACIONAL : BONOS ESTATALES, EMPRESAS, LETRAS HIPOTECARIO,DEPOSITOS A PLAZO, PACTOS
   DECLARE @RENTAFIJA TABLE (ID_TABLA               INT            IDENTITY(1,1),
                             ORIGEN                 VARCHAR(50),
                             TIPOINSTRUMENTO        VARCHAR(50),
                             NEMOTECNICO            VARCHAR(350),
                             EMISOR                 VARCHAR(50),
                             MONEDA                 VARCHAR(10),
                             PORCENTAJEINVERSION    NUMERIC(6,2),
                             NOMINALES              NUMERIC(18,4),
                             TASACUPON              VARCHAR(10),
                             FECHAVENCIMIENTO       VARCHAR(8),
                             TASACOMPRA             NUMERIC(18,4),
                             TASAMERCADO            NUMERIC(6,2),
                             VALORIZACION           BIGINT,
                             COD_PRODUCTO           VARCHAR(10),
                             DURATION               FLOAT,
                             CODIGO                 VARCHAR(20) )

--RENTA FIJA INTERNACIONAL : BONOS
   DECLARE @RENTAFIJAINTERNACIONAL TABLE (ID_TABLA               INT            IDENTITY(1,1),
                                          ORIGEN                 VARCHAR(50),
                                          TIPOINSTRUMENTO        VARCHAR(50),
                                          NEMOTECNICO            VARCHAR(350),
                                          EMISOR                 VARCHAR(50),
                                          MONEDA                 VARCHAR(10),
                                          PORCENTAJEINVERSION    NUMERIC(6,2),
                                          NOMINALES              NUMERIC(18,4),
                                          TASACUPON              VARCHAR(10),
                                          FECHAVENCIMIENTO       VARCHAR(8),
                                          TASACOMPRA             NUMERIC(18,4),
                                          TASAMERCADO            NUMERIC(6,2),
                                          VALORIZACION           NUMERIC(18,4),
                                          COD_PRODUCTO           VARCHAR(10),
                                          DURATION               FLOAT,
                                          CODIGO                 VARCHAR(20) )


   DECLARE @CAJASCUENTAS TABLE (ID_TABLA            INT            IDENTITY(1,1),
                                ID_CAJA_CUENTA      INT,
                                DSC_CAJA_CUENTA     VARCHAR(30),
                                ID_CUENTA           INT,
                                NUM_CUENTA          VARCHAR(10),
                                ID_MONEDA           INT,
                                COD_MONEDA          VARCHAR(10),
                                DSC_MONEDA          VARCHAR(30),
                                COD_MERCADO         VARCHAR(1),
                                DESC_MERCADO        VARCHAR(30),
                                DECIMALES           INT )

   DECLARE @SALDOINICIAL TABLE (ID_MONEDA           INT,
                                SALDOCAJA           NUMERIC(18,4) )

   DECLARE @INFORMESCAJAS   TABLE (ID_TABLA           INT            IDENTITY(1,1),
                                   ID_CUENTA          INT,
                                   ID_CIERRE          INT,
                                   ID_OPERACION       INT,
                                   FECHA_LIQUIDACION  DATETIME,
                                   DESCRIPCION        VARCHAR(100),
                                   DESCRIPCION_CAJA   VARCHAR(100),
                                   MONEDA             VARCHAR(10),
                                   MONTO_MOVTO_ABONO  NUMERIC(18,6),
                                   MONTO_MOVTO_CARGO  NUMERIC(18,6),
                                   SALDO              NUMERIC(18,6),
                                   ID_MONEDA          INT ,
                                   DSC_CAJA_CUENTA    VARCHAR(100),
                                   ID_CAJA_CUENTA     INT )

   DECLARE @RVSECTOR TABLE (ID_TABLA        INT            IDENTITY(1,1),
                            ID_SECTOR       INT,
                            DSC_SECTOR      VARCHAR(50),
                            MONTO_MON_CTA   BIGINT,
                            PORC_SECTOR     NUMERIC(10,6) )


--******GLOSA MONEDA
   DECLARE @GLOSAMONEDA TABLE (ID_TABLA        INT            IDENTITY(1,1),
                               DSC_MONEDA      VARCHAR(50) )

  DECLARE @TMP TABLE (ID_ARBOL_CLASE_INST       NUMERIC,
                      ID_ARBOL_CLASE_INST_HOJA  NUMERIC,
                      DSC_ARBOL_CLASE_INST      VARCHAR(100),
                      DSC_ARBOL_CLASE_INST_HOJA VARCHAR(100),
                      MONTO_MON_CTA_INST        NUMERIC(18,2),
                      MONTO_MON_CTA_HOJA        NUMERIC(18,2),
                      CODIGO                    VARCHAR(20),
                      NIVEL                     INT ,
                      CODIGO_PADRE              VARCHAR(20) )

   DECLARE @FORWARDS TABLE (ID_TABLA               INT            IDENTITY(1,1),
                            ORIGEN                 VARCHAR(50),
                            TIPOINSTRUMENTO        VARCHAR(50),
                            NUMEROCONTRATO         NUMERIC(18,4),
                            MONEDAEMISION          VARCHAR(50),
                            MODALIDAD              VARCHAR(100),
                            UNIDADACTIVO           VARCHAR(50),
                            CANTIDADNOMINAL        NUMERIC(18,4),
                            FECHAINICIAL           DATETIME,
                            FECHAFINAL             DATETIME,
                            PRECIOPACTADO          NUMERIC(18,4),
                            RESULTADO              NUMERIC(18,4) )

   DECLARE @TRANSACCION TABLE(FECHAOPERACION       DATETIME
                            , FECHALIQUIDACION     DATETIME
                            , TIPOMOVIMIENTO       VARCHAR(1)
                            , DESCTIPOMOVIMIENTO   VARCHAR(50)
                            , NUMCUENTA            VARCHAR(5)
                            , IDOPERACION          INT
                            , FACTURA              INT
                            , NEMOTECNICO          VARCHAR(350)
                            , CANTIDAD             DECIMAL(16,4)
                            , MONEDA               VARCHAR(3)
                            , DECIMALESMOSTRAR     INT
                            , PRECIO               DECIMAL(16,4)
                            , COMISION             DECIMAL(16,4)
                            , DERECHOS             DECIMAL(16,4)
                            , GASTOS               DECIMAL(16,4)
                            , IVA                  DECIMAL(16,4)
                            , MONTO                DECIMAL(16,4)
                            , MONTOTOTALOPERACION  DECIMAL(16,4)
                            , DESCTIPOOPERACION    VARCHAR(350)
                            , CONTRAPARTE          VARCHAR(10) )

   --***********************************************************************************************

   BEGIN TRY

      SET @CODERR = 0
      SET @MSGERR = ''
      SET @CONSOLIDADO = 'CLT'

      --***********************************************************************************************
      -- INDICADORES
      --***********************************************************************************************

      INSERT INTO @INDICADORES EXEC CSGPI.DBO.PKG_VALOR_TIPO_CAMBIO$BUSCARINDICADORES @FECHAHASTA

      INSERT INTO @INDICADORES_SALIDA
      SELECT 'VALORES AL'
            ,@FECHAHASTA
            ,@FECHADESDE

      SELECT @DOLAR1 = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 1
      SELECT @UF1    = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 2

      INSERT INTO @INDICADORES EXEC CSGPI.DBO.PKG_VALOR_TIPO_CAMBIO$BUSCARINDICADORES @FECHADESDE

      SELECT @DOLAR2 = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 1
      SELECT @UF2    = VALOR FROM @INDICADORES WHERE ID_TIPO_CAMBIO = 2

      INSERT INTO @INDICADORES_SALIDA
      SELECT   'UF',@UF1,@UF2

      INSERT INTO @INDICADORES_SALIDA
      SELECT   'Dolar OBS.',@DOLAR1,@DOLAR2

      SELECT 'INDICADORES'       AS INICIOBLOQUE
            ,DESCRIPCION         AS DESCRIPCION
            ,ISNULL(VALOR1, '')  AS VALOR1
            ,ISNULL(VALOR2, '')  AS VALOR2
      FROM   @INDICADORES_SALIDA

      --***********************************************************************************************

      SELECT   @ID_CLIENTE = CU.ID_CLIENTE
      FROM     CSGPI.DBO.CUENTAS AS CU WITH (NOLOCK)
      INNER JOIN CSGPI.DBO.CLIENTES AS CL WITH (NOLOCK) ON CU.ID_CLIENTE = CL.ID_CLIENTE
      WHERE    CL.RUT_CLIENTE = @RUTCLIENTE
      AND      CU.COD_ESTADO = 'H'


      SELECT @ID_CUENTA=ID_CUENTA
      FROM   CSGPI.DBO.CUENTAS WITH (NOLOCK)
      WHERE  ID_CLIENTE =@ID_CLIENTE
      AND    NUM_CUENTA=@CUENTAENTRADA

      DECLARE @DICIMALES_MOSTRAR NUMERIC(2)

      SELECT @DICIMALES_MOSTRAR = M.DICIMALES_MOSTRAR
      FROM   CSGPI.DBO.CUENTAS C,
             CSGPI.DBO.MONEDAS M
      WHERE  C.ID_CUENTA = @ID_CUENTA AND
             C.ID_MONEDA= M.ID_MONEDA

-------------------------------------------------------------------------------------------------------------------------------------------------1
      INSERT INTO @CUENTAS
      SELECT *
      FROM   CSGPI.DBO.CUENTAS WITH (NOLOCK)
      WHERE  ID_CLIENTE = @ID_CLIENTE
      AND    NUM_CUENTA = @CUENTAENTRADA

      SELECT TOP 1
             @ID_EMPRESA    = ID_EMPRESA,
             @ID_MONEDA     = ID_MONEDA,
             @ID_TIPOCUENTA = ID_TIPOCUENTA
      FROM   @CUENTAS

      INSERT INTO @ARBOL
      EXEC CSGPI.DBO.PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA_CONSOLIDADO NULL,@ID_EMPRESA,@FECHAHASTA,@ID_CLIENTE,NULL,@ID_MONEDA,@CONSOLIDADO

      SET @CANTIDAD = ISNULL((SELECT COUNT(ID_ARBOL_CLASE_INST_HOJA) FROM @ARBOL), 0)

      SET @CONT = 1

      WHILE @CONT <= @CANTIDAD
      BEGIN
         SELECT @ID_ARBOL_CLASE_INST_HOJA = ID_ARBOL_CLASE_INST_HOJA
               ,@DSC_ARBOL_CLASE_INST = DSC_ARBOL_CLASE_INST
               ,@DSC_ARBOL_CLASE_INST_HOJA = DSC_ARBOL_CLASE_INST_HOJA
               ,@CODIGO = CODIGO
         FROM   @ARBOL
         WHERE  ID_TABLA = @CONT

         INSERT INTO @SALDOSACTIVOS
         EXEC  CSGPI.DBO.PKG_SALDOS_ACTIVOS_WEB$RESUMEN_POR_HOJA_ARBOL_CLASE_INSTRUMENTO @ID_CUENTA, @FECHAHASTA, @ID_ARBOL_CLASE_INST_HOJA,NULL,NULL,NULL,NULL

         SET @CONT = @CONT + 1

      END

      SELECT @PID_MONEDA_SALIDA= ID_MONEDA FROM CSGPI.DBO.CUENTAS WITH (NOLOCK) WHERE ID_CUENTA=@ID_CUENTA
      SET @DECIMALES =(SELECT DICIMALES_MOSTRAR FROM CSGPI.DBO.MONEDAS WITH (NOLOCK) WHERE ID_MONEDA =@PID_MONEDA_SALIDA)

      --***********************************************************************************************
      -- FORWARDS
      --***********************************************************************************************

      INSERT INTO @FORWARDS (ORIGEN,
                             TIPOINSTRUMENTO,
                             NUMEROCONTRATO,
                             MONEDAEMISION,
                             MODALIDAD,
                             UNIDADACTIVO,
                             CANTIDADNOMINAL,
                             FECHAINICIAL,
                             FECHAFINAL,
                             PRECIOPACTADO,
                             RESULTADO)
      SELECT 'FORWARDS',
             TIPO_FWD,
             NUMERO_CONTRATO,
             MONEDA_EMISION,
             MODALIDAD,
             UNIDAD_ACTIVO_SUBYACENTE,
             VALOR_NOMINAL,
             FECHA_INICIO,
             FECHA_VENCIMIENTO,
             PRECIO_PACTADO,
             VALOR_MERCADO
      FROM   @SALDOSACTIVOS
      WHERE  ORIGEN = 'FORWARDS'

      --***********************************************************************************************
      -- RENTA VARIABLE INTERNACIONAL
      --***********************************************************************************************

      INSERT INTO @RENTAVARIABLEINTERNACIONAL (ORIGEN,
                                               TIPOINSTRUMENTO,
                                               NEMOTECNICO,
                                               MONEDA,
                                               PORCENTAJEINVERSION,
                                               CANTIDAD,
                                               PRECIOCOMPRA,
                                               MONTOCOMPRA,
                                               PRECIOACTUAL,
                                               VALORMERCADO,
                                               MONTO_MON_ORIGEN,
                                               RENTABILIDAD ,
                                               COD_PRODUCTO,
                                               CODIGO,
                                               MONTOINVERTIDO,
                                               UTILIDADPERDIDA,
                                               VALORIZACIONUSD,
                                               VALORIZACIONPESO)
      SELECT ORIGEN,DSC_ARBOL_CLASE_INST,
             NEMOTECNICO  AS NEMOTECNICO,
             SIMBOLO_MONEDA,
             PORCENTAJE_RAMA,
             CANTIDAD,
             PRECIO_COMPRA,
             ROUND(MONTO_PROMEDIO_COMPRA,0),
             PRECIO     ,
             VALOR_MERCADO    AS   MONTO_MON_CTA,
             MONTO_MON_ORIGEN,
             RENTABILIDAD,
             COD_PRODUCTO, --07-02-2014
             CODIGO ,
             MONTO_INVERTIDO   AS MONTOINVERTIDO,
             UTILIDAD_PERDIDA   AS UTILIDADPERDIDA,
             MONTO_MON_USD   AS VALORIZACIONUSD,
             ROUND(MONTO_MON_CTA,@DICIMALES_MOSTRAR)  AS VALORIZACIONPESO
      FROM   @SALDOSACTIVOS
      WHERE  ORIGEN IN ('NOTAS_NAC','NOTAS_INT')

      --***********************************************************************************************
      -- OTROS ACTIVOS FINV
      --***********************************************************************************************

      INSERT INTO @RENTAVARIABLEINTERNACIONAL (ORIGEN,
                                               TIPOINSTRUMENTO,
                                               NEMOTECNICO,
                                               MONEDA,
                                               PORCENTAJEINVERSION,
                                               CANTIDAD,
                                               PRECIOCOMPRA,
                                               MONTOCOMPRA,
                                               PRECIOACTUAL,
                                               VALORMERCADO,
                                               MONTO_MON_ORIGEN,
                                               RENTABILIDAD ,
                                               COD_PRODUCTO,
                                               CODIGO,
                                               MONTOINVERTIDO,
                                               UTILIDADPERDIDA,
                                               VALORIZACIONUSD,
                                               VALORIZACIONPESO)
      SELECT ORIGEN,
             DSC_ARBOL_CLASE_INST,
             NEMOTECNICO  AS NEMOTECNICO,
             SIMBOLO_MONEDA,
             PORCENTAJE_RAMA,
             CANTIDAD,
             PRECIO_COMPRA,
             ROUND(MONTO_PROMEDIO_COMPRA,0),
             PRECIO     ,
             VALOR_MERCADO    AS   MONTO_MON_CTA,
             MONTO_MON_ORIGEN,
             RENTABILIDAD,
             COD_PRODUCTO, --07-02-2014
             CODIGO ,
             MONTO_INVERTIDO   AS MONTOINVERTIDO,
             UTILIDAD_PERDIDA   AS UTILIDADPERDIDA,
             MONTO_MON_USD   AS VALORIZACIONUSD,
             ROUND(MONTO_MON_CTA,@DICIMALES_MOSTRAR)   AS VALORIZACIONPESO
      FROM   @SALDOSACTIVOS
      WHERE  COD_INSTRUMENTO ='ACCIONES_NAC'
      AND    DSC_ARBOL_CLASE_INST='FONDOS DE INVERSI�N'

      --***********************************************************************************************
      -- RENTA VARIABLE NACIONAL
      --***********************************************************************************************

      INSERT INTO @RENTAVARIABLE (ORIGEN,
                                  TIPOINSTRUMENTO,
                                  NEMOTECNICO,
                                  MONEDA,
                                  PORCENTAJEINVERSION,
                                  CANTIDAD,
                                  PRECIOCOMPRA,
                                  MONTOCOMPRA,
                                  PRECIOACTUAL,
                                  VALORMERCADO,
                                  MONTO_MON_ORIGEN,
                                  RENTABILIDAD ,
                                  COD_PRODUCTO,
                                  CODIGO)
      SELECT ORIGEN,
             DSC_ARBOL_CLASE_INST,
             CASE CODIGO
                WHEN 'FFMM' THEN DSC_NEMOTECNICO
                ELSE NEMOTECNICO
             END   AS NEMOTECNICO,
             SIMBOLO_MONEDA,
             PORCENTAJE_RAMA,
             CANTIDAD,
             PRECIO_PROMEDIO_COMPRA,
             MONTO_PROMEDIO_COMPRA,
             PRECIO     ,
             MONTO_MON_CTA,
             CASE CODIGO
                WHEN 'FFMM' THEN  MONTO_VALOR_COMPRA
                ELSE MONTO_MON_ORIGEN
             END,
             RENTABILIDAD,
             COD_PRODUCTO,
             CODIGO
      FROM   @SALDOSACTIVOS
      WHERE  DSC_PADRE_ARBOL_CLASE_INST IN ('RENTA VARIABLE','RENTA VARIABLE NACIONAL')

      INSERT INTO @RENTAVARIABLE (ORIGEN, TIPOINSTRUMENTO,
                                  PORCENTAJEINVERSION,
                                  MONTOCOMPRA,
                                  VALORMERCADO)
      SELECT  ORIGEN,DSC_ARBOL_CLASE_INST,
              100,
              SUM(ROUND(MONTO_PROMEDIO_COMPRA,2)),
              ROUND(SUM(MONTO_MON_CTA),2)
      FROM    @SALDOSACTIVOS
      WHERE   DSC_PADRE_ARBOL_CLASE_INST IN ('RENTA VARIABLE','RENTA VARIABLE NACIONAL')
      GROUP   BY DSC_ARBOL_CLASE_INST,ORIGEN
      -------------------------------------------------------------------------------------------------------------------------------------------------2

      SELECT  ORIGEN, 'RENTAVARIABLE'      AS INICIOBLOQUE,
              TIPOINSTRUMENTO,
              NEMOTECNICO = ISNULL(NEMOTECNICO, ''),
              MONEDA = ISNULL(MONEDA,''),
              PORCENTAJEINVERSION,
              CANTIDAD,
              PRECIOCOMPRA,
              CASE
                 WHEN ISNULL(MONEDA,'') ='US$' THEN ROUND(MONTOCOMPRA,2)
                 ELSE ROUND(MONTOCOMPRA,0)
              END MONTOCOMPRA,
              PRECIOACTUAL,
              ROUND(VALORMERCADO,2) AS VALORMERCADO, --14-04-2014
              ROUND(MONTO_MON_ORIGEN,2) AS MONTO_MON_ORIGEN, --NUEVO 14-04-2014
              RENTABILIDAD,
              COD_PRODUCTO,
              CODIGO
      FROM    @RENTAVARIABLE
      ORDER   BY TIPOINSTRUMENTO, ID_TABLA

      --***********************************************************************************************
      -- RV OTROS ACTIVOS FINV
      --***********************************************************************************************

      INSERT INTO @RVOTROSACTIVOSFINV (ORIGEN,
                                       TIPOINSTRUMENTO,
                                       NEMOTECNICO,
                                       MONEDA,
                                       PORCENTAJEINVERSION,
                                       CANTIDAD,
                                       PRECIOCOMPRA,
                                       MONTOCOMPRA,
                                       PRECIOACTUAL,
                                       VALORMERCADO,
                                       MONTO_MON_ORIGEN,
                                       RENTABILIDAD ,
                                       COD_PRODUCTO,
                                       CODIGO)
      SELECT ORIGEN,
             DSC_ARBOL_CLASE_INST,
             NEMOTECNICO  NEMOTECNICO,
             SIMBOLO_MONEDA,
             PORCENTAJE_RAMA,
             CANTIDAD,
             PRECIO_PROMEDIO_COMPRA,
             MONTO_PROMEDIO_COMPRA,
             PRECIO     ,
             MONTO_MON_CTA,
             MONTO_MON_ORIGEN,
             RENTABILIDAD,
             COD_PRODUCTO, --07-02-2014
             CODIGO --10-02-2014
      FROM   @SALDOSACTIVOS
      WHERE  CODIGO IN ('FM_OA')

      --***********************************************************************************************
      -- RENTA VARIABLE INTERNACIONAL
      --***********************************************************************************************

      INSERT INTO @RENTAVARIABLEINTERNACIONAL (ORIGEN,
                                               TIPOINSTRUMENTO,
                                               NEMOTECNICO,
                                               MONEDA,
                                               PORCENTAJEINVERSION,
                                               CANTIDAD,
                                               PRECIOCOMPRA,
                                               MONTOCOMPRA,
                                               PRECIOACTUAL,
                                               VALORMERCADO,
                                               MONTO_MON_ORIGEN,
                                               RENTABILIDAD ,
                                               COD_PRODUCTO,
                                               CODIGO,
                                               MONTOINVERTIDO,
                                               UTILIDADPERDIDA,
                                               VALORIZACIONUSD,
                                               VALORIZACIONPESO)
      SELECT ORIGEN,
             DSC_ARBOL_CLASE_INST,
             CASE CODIGO
                WHEN 'FFMM'  THEN DSC_NEMOTECNICO
                WHEN 'FM_INT'  THEN DSC_NEMOTECNICO
                WHEN 'RF_INT'  THEN DSC_NEMOTECNICO
                WHEN 'RVINT'  THEN DSC_NEMOTECNICO
                ELSE NEMOTECNICO
             END   AS NEMOTECNICO,
             SIMBOLO_MONEDA,
             PORCENTAJE_RAMA,
             CANTIDAD,
             PRECIO_PROMEDIO_COMPRA,
             CASE ORIGEN
                WHEN 'PERSHING' THEN ROUND(MONTO_INVERTIDO,2)
                ELSE ROUND(MONTO_PROMEDIO_COMPRA,0)
             END,
             PRECIO,
             CASE
                WHEN ORIGEN = 'NACIONAL' THEN  MONTO_MON_CTA
                WHEN ORIGEN = 'INTERNACIONAL' OR ORIGEN = 'PERSHING' THEN VALOR_MERCADO
             END AS MONTO_MON_CTA,
             MONTO_MON_ORIGEN,
             RENTABILIDAD,
             COD_PRODUCTO,
             CODIGO,
             CASE
                WHEN ORIGEN = 'NACIONAL' THEN  0
                WHEN ORIGEN = 'INTERNACIONAL' OR ORIGEN = 'PERSHING' THEN  MONTO_INVERTIDO
             END AS MONTOINVERTIDO,
             CASE
                WHEN ORIGEN = 'NACIONAL' THEN  0
                WHEN ORIGEN = 'INTERNACIONAL' OR ORIGEN = 'PERSHING' THEN  UTILIDAD_PERDIDA
             END AS UTILIDADPERDIDA,
             CASE
                WHEN ORIGEN = 'NACIONAL' THEN  0
                WHEN ORIGEN = 'INTERNACIONAL' OR ORIGEN = 'PERSHING' THEN  MONTO_MON_USD
             END AS VALORIZACIONUSD,
             CASE
                WHEN ORIGEN = 'NACIONAL' THEN  0
                WHEN ORIGEN = 'INTERNACIONAL' OR ORIGEN = 'PERSHING' THEN  ROUND(MONTO_MON_CTA,@DICIMALES_MOSTRAR)
             END AS VALORIZACIONPESO
      FROM   @SALDOSACTIVOS
      WHERE  DSC_PADRE_ARBOL_CLASE_INST IN ('RENTA VARIABLE','RENTA VARIABLE INTERNACIONAL')

      INSERT INTO @RENTAVARIABLEINTERNACIONAL (ORIGEN,
                                               TIPOINSTRUMENTO,
                                               PORCENTAJEINVERSION,
                                               MONTOCOMPRA,
                                               VALORMERCADO)
      SELECT ORIGEN,
             DSC_ARBOL_CLASE_INST,
             100,
             CASE ORIGEN
                WHEN 'PERSHING' THEN SUM(MONTO_MON_USD)--ROUND(MONTO_INVERTIDO,2)
                ELSE SUM(MONTO_PROMEDIO_COMPRA)
             END,
             SUM(ROUND(MONTO_MON_CTA,@DICIMALES_MOSTRAR))
      FROM   @SALDOSACTIVOS
      WHERE  DSC_PADRE_ARBOL_CLASE_INST IN ('RENTA VARIABLE','RENTA VARIABLE INTERNACIONAL') OR COD_PRODUCTO IN ('RV_INT')
      GROUP  BY DSC_ARBOL_CLASE_INST,ORIGEN

      ------------------------------------------------------------------------------------------------------------------------------------------------3

      SELECT  ORIGEN,
              'RENTAVARIABLEINTER'      AS INICIOBLOQUE,
              TIPOINSTRUMENTO,
              NEMOTECNICO = ISNULL(NEMOTECNICO, ''),
              MONEDA = ISNULL(MONEDA,''),
              PORCENTAJEINVERSION,
              DBO.FN_CC_INCLUIRMILESCARTOLA(CANTIDAD,4) AS CANTIDAD,
              DBO.FN_CC_INCLUIRMILESCARTOLA(PRECIOCOMPRA,4) AS PRECIOCOMPRA,
              DBO.FN_CC_INCLUIRMILESCARTOLA(MONTOCOMPRA,4) AS MONTOCOMPRA,
              DBO.FN_CC_INCLUIRMILESCARTOLA(PRECIOACTUAL,4) AS PRECIOACTUAL,
              DBO.FN_CC_INCLUIRMILESCARTOLA(VALORMERCADO,2) AS VALORMERCADO,
              DBO.FN_CC_INCLUIRMILESCARTOLA(MONTO_MON_ORIGEN,2) AS MONTO_MON_ORIGEN, --NUEVO
              DBO.FN_CC_INCLUIRMILESCARTOLA(RENTABILIDAD,2) AS  RENTABILIDAD,
              COD_PRODUCTO,
              CODIGO,
              MONTOINVERTIDO,
              UTILIDADPERDIDA,
              VALORIZACIONUSD,
              VALORIZACIONPESO
      FROM    @RENTAVARIABLEINTERNACIONAL
      ORDER   BY TIPOINSTRUMENTO, ID_TABLA

      --***********************************************************************************************
      -- RENTA VARIABLE POR SECTORES ECONOMICOS
      --***********************************************************************************************


      INSERT INTO @TMP
      EXEC CSGPI.DBO.PKG_SALDOS_ACTIVOS$ARBOL_CLASE_INSTRUMENTO_CARTOLA @ID_CUENTA, @ID_EMPRESA, @FECHAHASTA

      SELECT @ID_ARBOL_SEC_ECONOMICO = ID_ARBOL_CLASE_INST_HOJA
      FROM   @TMP
      WHERE  DSC_ARBOL_CLASE_INST = 'RENTA VARIABLE NACIONAL' AND DSC_ARBOL_CLASE_INST_HOJA = 'ACCIONES'

      INSERT INTO @RVSECTOR
      EXEC CSGPI.DBO.PKG_SALDOS_ACTIVOS$BUSCAR_RV_SECTOR @ID_CUENTA,@FECHAHASTA,@ID_ARBOL_SEC_ECONOMICO,@ID_MONEDA

      -- GENERA PARAMETROS DEL GR�FICO
      SET @VECNOM = ''
      SET @VECMTO = ''
      SET @CANTIDAD = ISNULL((SELECT COUNT(ID_TABLA) FROM @RVSECTOR),0)
      SET @CONT = 1

      WHILE @CONT <= @CANTIDAD
      BEGIN
         SET @VECNOM = @VECNOM +
                       ISNULL((SELECT DSC_SECTOR FROM @RVSECTOR WHERE ID_TABLA = @CONT),'') + ' ' +
                       REPLACE(ISNULL(CAST(CAST(((SELECT PORC_SECTOR FROM @RVSECTOR WHERE ID_TABLA = @CONT)*100)AS DECIMAL(6,2)) AS VARCHAR(MAX)),''),'.',',') + '%' + ';'
         SET @VECMTO = @VECMTO + REPLACE(ISNULL(CAST(((SELECT PORC_SECTOR FROM @RVSECTOR WHERE ID_TABLA = @CONT)*100) AS VARCHAR(MAX)),''),'.',',') + ';'
         SET @CONT = @CONT + 1
      END

      -- ELIMINA EL �LTIMO ;
      IF LEN(@VECNOM) > 1
      BEGIN
         SET @VECNOM = SUBSTRING(@VECNOM,1,LEN(@VECNOM)-1)
         SET @VECMTO = SUBSTRING(@VECMTO,1,LEN(@VECMTO)-1)
      END

      -- INSERTA TOTAL
      IF EXISTS(SELECT * FROM @RVSECTOR)
         INSERT INTO @RVSECTOR(DSC_SECTOR,
                               MONTO_MON_CTA)
         SELECT 'TOTAL',SUM(MONTO_MON_CTA)
         FROM   @RVSECTOR
      -------------------------------------------------------------------------------------------------------------------------------------------------4

      SELECT 'SECTORESECONOMICOS' AS INICIOBLOQUE,
             DSC_SECTOR           AS SECTORESECONOMICOS,
             MONTO_MON_CTA        AS MONTO
      FROM   @RVSECTOR

      IF @VECNOM <> ''
         SELECT 'GRAFICO'               AS INICIOBLOQUE,
                @VECNOM                 AS VECNOM,
                @VECMTO                 AS VECMTO
      ELSE
         SELECT ''                      AS INICIOBLOQUE,
                @VECNOM                 AS VECNOM,
                @VECMTO                 AS VECMTO

      --***********************************************************************************************
      -- RENTA FIJA NACIONAL BONOS
      --***********************************************************************************************

      INSERT INTO @RENTAFIJA (ORIGEN,
                              TIPOINSTRUMENTO,
                              NEMOTECNICO,
                              EMISOR,
                              MONEDA,
                              PORCENTAJEINVERSION,
                              NOMINALES,
                              TASACUPON,
                              FECHAVENCIMIENTO,
                              TASACOMPRA,
                              TASAMERCADO,
                              VALORIZACION,
                              COD_PRODUCTO,
                              DURATION,
                              CODIGO)
      SELECT ORIGEN,
             DSC_ARBOL_CLASE_INST,
             CASE CODIGO
                WHEN 'FFMM' THEN DSC_NEMOTECNICO
                ELSE NEMOTECNICO
             END   AS NEMOTECNICO,
             COD_EMISOR,--EMISOR,
             SIMBOLO_MONEDA,
             CAST(ROUND(PORCENTAJE_RAMA,2) AS NUMERIC(6,2)),
             CANTIDAD,
             CAST(TASA_EMISION AS VARCHAR(10)),
             CONVERT(VARCHAR(8),ISNULL(FECHA_VENCIMIENTO,''),112),
             CAST(TASA_COMPRA AS NUMERIC(18,4)),
             CAST(TASA AS NUMERIC(6,2)),
             CAST(MONTO_MON_CTA AS BIGINT),
             COD_PRODUCTO, --07-02-2014
             DURATION,      --10-02-2014
             CODIGO --10-02-2014
      FROM   @SALDOSACTIVOS
      WHERE  DSC_PADRE_ARBOL_CLASE_INST IN ('RENTA FIJA','RENTA FIJA NACIONAL')

      INSERT INTO @RENTAFIJA(ORIGEN,
                             TIPOINSTRUMENTO,
                             PORCENTAJEINVERSION,
                             TASACOMPRA,
                             VALORIZACION)
      SELECT ORIGEN,
             DSC_ARBOL_CLASE_INST,
             100,
             SUM(CAST(TASA_COMPRA AS NUMERIC(18,4))),
             SUM(CAST(MONTO_MON_CTA AS BIGINT))
      FROM   @SALDOSACTIVOS
      WHERE  DSC_PADRE_ARBOL_CLASE_INST IN ('RENTA FIJA','RENTA FIJA NACIONAL')
      GROUP  BY DSC_ARBOL_CLASE_INST,ORIGEN
      -------------------------------------------------------------------------------------------------------------------------------------------------5

      SELECT ORIGEN, 'RENTAFIJA'      AS INICIOBLOQUE,
             TIPOINSTRUMENTO,
             NEMOTECNICO = ISNULL(NEMOTECNICO, ''),
             EMISOR = ISNULL(EMISOR, ''),
             MONEDA = ISNULL(MONEDA, ''),
             PORCENTAJEINVERSION,
             NOMINALES,
             TASACUPON,
             FECHAVENCIMIENTO,
             TASACOMPRA,
             TASAMERCADO,
             VALORIZACION,
             COD_PRODUCTO,
             CAST(DURATION AS NUMERIC(6,4)) AS DURATION,
             CODIGO
      FROM   @RENTAFIJA
      ORDER  BY TIPOINSTRUMENTO, ID_TABLA

      --***********************************************************************************************
      -- RENTA FIJA INTERNACIONAL BONOS
      --***********************************************************************************************
      INSERT INTO @RENTAFIJAINTERNACIONAL(ORIGEN,
                                          TIPOINSTRUMENTO,
                                          NEMOTECNICO,
                                          EMISOR,
                                          MONEDA,
                                          PORCENTAJEINVERSION,
                                          NOMINALES,
                                          TASACUPON,
                                          FECHAVENCIMIENTO,
                                          TASACOMPRA,
                                          TASAMERCADO,
                                          VALORIZACION,
                                          COD_PRODUCTO,
                                          DURATION,
                                          CODIGO)
      SELECT ORIGEN,
             DSC_ARBOL_CLASE_INST,
             CASE CODIGO
                WHEN 'FFMM' THEN DSC_NEMOTECNICO
                ELSE NEMOTECNICO
             END   AS NEMOTECNICO,
             COD_EMISOR,--EMISOR,
             SIMBOLO_MONEDA,
             CAST(PORCENTAJE_RAMA AS NUMERIC(6,2)),
             CANTIDAD,
             CAST(TASA_EMISION AS VARCHAR(10)),
             CONVERT(VARCHAR(8),ISNULL(FECHA_VENCIMIENTO,''),112),
             CAST(TASA_COMPRA AS NUMERIC(18,4)),
             CAST(TASA AS NUMERIC(6,2)),
             MONTO_MON_CTA,
             COD_PRODUCTO,
             DURATION,
             CODIGO
      FROM   @SALDOSACTIVOS
      WHERE  DSC_PADRE_ARBOL_CLASE_INST IN ('RENTA FIJA INTERNACIONAL') AND ORIGEN='NACIONAL'

      INSERT INTO @RENTAFIJAINTERNACIONAL (ORIGEN,
                                           TIPOINSTRUMENTO,
                                           PORCENTAJEINVERSION,
                                           TASACOMPRA,
                                           VALORIZACION)
      SELECT ORIGEN,
             DSC_ARBOL_CLASE_INST,
             100,
             SUM(CAST(TASA_COMPRA AS NUMERIC(18,4))),
             SUM(ROUND(MONTO_MON_CTA,@DICIMALES_MOSTRAR))
      FROM   @SALDOSACTIVOS
      WHERE  DSC_PADRE_ARBOL_CLASE_INST IN ('RENTA FIJA INTERNACIONAL') AND ORIGEN='NACIONAL'
      GROUP  BY DSC_ARBOL_CLASE_INST,ORIGEN

      -------------------------------------------------------------------------------------------------------------------------------------------------6

      SELECT ORIGEN,
             'RENTAFIJAINTBONOS'      AS INICIOBLOQUE,
             TIPOINSTRUMENTO,
             NEMOTECNICO = ISNULL(NEMOTECNICO, ''),
             EMISOR = ISNULL(EMISOR, ''),
             MONEDA = ISNULL(MONEDA, ''),
             DBO.FN_CC_INCLUIRMILESCARTOLA(PORCENTAJEINVERSION,2) AS PORCENTAJEINVERSION,
             DBO.FN_CC_INCLUIRMILESCARTOLA(NOMINALES,4) AS NOMINALES,
             TASACUPON AS TASACUPON,
             FECHAVENCIMIENTO,
             DBO.FN_CC_INCLUIRMILESCARTOLA(TASACOMPRA,4) AS TASACOMPRA,
             DBO.FN_CC_INCLUIRMILESCARTOLA(TASAMERCADO,4) AS  TASAMERCADO,
             DBO.FN_CC_INCLUIRMILESCARTOLA(VALORIZACION,2) AS VALORIZACION,
             COD_PRODUCTO,
             DBO.FN_CC_INCLUIRMILESCARTOLA(DURATION,4) AS DURATION,
             CODIGO
      FROM   @RENTAFIJAINTERNACIONAL
      ORDER  BY TIPOINSTRUMENTO, ID_TABLA

      --***********************************************************************************************
      -- RENTA FIJA NACIONAL POR FONDOS MUTUOS CORTO , LARGO PLAZO NACIONAL
      --***********************************************************************************************

      INSERT INTO @RENTAFIJAFONDOS (ORIGEN,
                                    TIPOINSTRUMENTO,
                                    NEMOTECNICO,
                                    MONEDA,
                                    PORCENTAJEINVERSION,
                                    CANTIDAD,
                                    PRECIOCOMPRA,
                                    MONTOCOMPRA,
                                    PRECIOACTUAL,
                                    VALORMERCADO,
                                    MONTO_MON_ORIGEN,
                                    MONTO_MON_NEMOTECNICO,
                                    RENTABILIDAD,
                                    COD_PRODUCTO,
                                    DURATION,
                                    CODIGO)
      SELECT ORIGEN,
             DSC_ARBOL_CLASE_INST,
             CASE CODIGO
                WHEN 'FFMM' THEN DSC_NEMOTECNICO
                ELSE NEMOTECNICO
             END   AS NEMOTECNICO,
             SIMBOLO_MONEDA,
             PORCENTAJE_RAMA,
             CANTIDAD,
             PRECIO_PROMEDIO_COMPRA,
             MONTO_PROMEDIO_COMPRA,
             PRECIO,
             MONTO_MON_CTA,
             MONTO_MON_ORIGEN,
             MONTO_MON_NEMOTECNICO,
             RENTABILIDAD,
             COD_PRODUCTO,
             DURATION,
             CODIGO
      FROM   @SALDOSACTIVOS
      WHERE  DSC_PADRE_ARBOL_CLASE_INST IN ('RENTA FIJA NACIONAL')

      INSERT INTO @RENTAFIJAFONDOS (ORIGEN,
                                    TIPOINSTRUMENTO,
                                    PORCENTAJEINVERSION,
                                    MONTOCOMPRA,
                                    VALORMERCADO)
      SELECT  ORIGEN,
              DSC_ARBOL_CLASE_INST,
              100,
              SUM(MONTO_PROMEDIO_COMPRA),
              SUM(MONTO_MON_CTA)
      FROM    @SALDOSACTIVOS
      WHERE   DSC_PADRE_ARBOL_CLASE_INST IN ('RENTA FIJA NACIONAL')
      GROUP   BY DSC_ARBOL_CLASE_INST,ORIGEN
      -------------------------------------------------------------------------------------------------------------------------------------------------7

      SELECT  ORIGEN,
              'RENTAFIJAFONDOS'      AS INICIOBLOQUE,
              TIPOINSTRUMENTO,
              NEMOTECNICO = ISNULL(NEMOTECNICO, ''),
              MONEDA = ISNULL(MONEDA,''),
              PORCENTAJEINVERSION,
              CANTIDAD,
              PRECIOCOMPRA,
              MONTOCOMPRA,
              PRECIOACTUAL,
              DBO.FN_CC_INCLUIRMILESCARTOLA(VALORMERCADO,@DECIMALES) AS VALORMERCADO,
              CASE MONEDA
                 WHEN 'UF' THEN MONTO_MON_NEMOTECNICO
                 ELSE MONTO_MON_ORIGEN
              END   AS MONTO_MON_ORIGEN,
              RENTABILIDAD,
              COD_PRODUCTO,
              DURATION,
              CODIGO
      FROM    @RENTAFIJAFONDOS
      ORDER   BY TIPOINSTRUMENTO, ID_TABLA

      --***********************************************************************************************
      -- RENTA FIJA INTERNACIONAL POR FONDOS MUTUOS, ETFS
      --***********************************************************************************************
      INSERT INTO @RENTAFIJAFONDOSINTER (ORIGEN,
                                         TIPOINSTRUMENTO,
                                         NEMOTECNICO,
                                         MONEDA,
                                         PORCENTAJEINVERSION,
                                         CANTIDAD,
                                         PRECIOCOMPRA,
                                         MONTOCOMPRA,
                                         PRECIOACTUAL,
                                         VALORMERCADO,
                                         MONTO_MON_ORIGEN,
                                         MONTO_MON_NEMOTECNICO,
                                         RENTABILIDAD,
                                         COD_PRODUCTO,
                                         DURATION,
                                         CODIGO,
                                         MONTOINVERTIDO   ,
                                         UTILIDADPERDIDA  ,
                                         VALORIZACIONUSD  ,
                                         VALORIZACIONPESO)
      SELECT ORIGEN,
             DSC_ARBOL_CLASE_INST,
             CASE CODIGO
                WHEN 'FFMM'   THEN DSC_NEMOTECNICO
                WHEN 'FM_INT' THEN DSC_NEMOTECNICO
                WHEN 'RFINT'  THEN DSC_NEMOTECNICO
                WHEN 'RVINT'  THEN DSC_NEMOTECNICO
                ELSE NEMOTECNICO
             END   AS NEMOTECNICO,
             SIMBOLO_MONEDA,
             PORCENTAJE_RAMA,
             CANTIDAD,
             PRECIO_PROMEDIO_COMPRA,
             CASE
                WHEN ORIGEN = 'PERSHING' THEN MONTO_INVERTIDO
                ELSE MONTO_PROMEDIO_COMPRA
             END,
             PRECIO,
             MONTO_MON_CTA,
             MONTO_MON_ORIGEN,
             MONTO_MON_NEMOTECNICO,
             RENTABILIDAD,
             COD_PRODUCTO,
             DURATION,
             CODIGO,
             CASE
                WHEN ORIGEN = 'NACIONAL' THEN  0
                WHEN ORIGEN = 'INTERNACIONAL' OR ORIGEN = 'PERSHING' THEN  MONTO_INVERTIDO
             END AS MONTOINVERTIDO,
             CASE
                WHEN ORIGEN = 'NACIONAL' THEN  0
                WHEN ORIGEN = 'INTERNACIONAL' OR ORIGEN = 'PERSHING' THEN  UTILIDAD_PERDIDA
             END AS UTILIDADPERDIDA,
             CASE
                WHEN ORIGEN = 'NACIONAL' THEN  0
                WHEN ORIGEN = 'INTERNACIONAL' OR ORIGEN = 'PERSHING' THEN  MONTO_MON_USD
             END AS VALORIZACIONUSD,
             CASE
                WHEN ORIGEN = 'NACIONAL' THEN  0
                WHEN ORIGEN = 'INTERNACIONAL' OR ORIGEN = 'PERSHING' THEN  ROUND(MONTO_MON_CTA,@DICIMALES_MOSTRAR)
             END AS VALORIZACIONPESO
      FROM   @SALDOSACTIVOS
      WHERE  DSC_PADRE_ARBOL_CLASE_INST IN ('RENTA FIJA INTERNACIONAL') AND ORIGEN  <> 'NACIONAL'

      INSERT INTO @RENTAFIJAFONDOSINTER (ORIGEN,
                                         TIPOINSTRUMENTO,
                                         PORCENTAJEINVERSION,
                                         MONTOCOMPRA,
                                         VALORMERCADO)
      SELECT ORIGEN,
             DSC_ARBOL_CLASE_INST,
             100,
             CASE ORIGEN
                WHEN 'PERSHING' THEN SUM(MONTO_MON_USD)
                ELSE SUM(MONTO_PROMEDIO_COMPRA)
             END,
             CASE ORIGEN
                WHEN 'PERSHING' THEN SUM( ROUND(MONTO_MON_CTA,@DICIMALES_MOSTRAR) )
                ELSE SUM(VALOR_MERCADO)
             END
      FROM   @SALDOSACTIVOS
      WHERE  DSC_PADRE_ARBOL_CLASE_INST IN ('RENTA FIJA INTERNACIONAL') AND ORIGEN <> 'NACIONAL'
      GROUP  BY DSC_ARBOL_CLASE_INST,ORIGEN

      -------------------------------------------------------------------------------------------------------------------------------------------------8

      SELECT ORIGEN,
             'RENTAFIJAFONDOSINTER'      AS INICIOBLOQUE,
             TIPOINSTRUMENTO,
             NEMOTECNICO = ISNULL(NEMOTECNICO, ''),
             MONEDA = ISNULL(MONEDA,''),
             DBO.FN_CC_INCLUIRMILESCARTOLA(PORCENTAJEINVERSION,2) AS PORCENTAJEINVERSION,
             DBO.FN_CC_INCLUIRMILESCARTOLA(CANTIDAD,4) AS CANTIDAD,
             DBO.FN_CC_INCLUIRMILESCARTOLA(PRECIOCOMPRA,4) AS PRECIOCOMPRA,
             DBO.FN_CC_INCLUIRMILESCARTOLA(MONTOCOMPRA,4) AS  MONTOCOMPRA,
             DBO.FN_CC_INCLUIRMILESCARTOLA(PRECIOACTUAL,4) AS PRECIOACTUAL,
             DBO.FN_CC_INCLUIRMILESCARTOLA(VALORMERCADO,2) AS VALORMERCADO,
             CASE MONEDA
                WHEN 'UF' THEN DBO.FN_CC_INCLUIRMILESCARTOLA(MONTO_MON_NEMOTECNICO,@DECIMALES)
                ELSE DBO.FN_CC_INCLUIRMILESCARTOLA(MONTO_MON_ORIGEN,@DECIMALES)
             END AS MONTO_MON_ORIGEN,
             DBO.FN_CC_INCLUIRMILESCARTOLA(RENTABILIDAD,2) AS RENTABILIDAD,
             COD_PRODUCTO,
             DBO.FN_CC_INCLUIRMILESCARTOLA(DURATION,4) AS  DURATION,
             CODIGO,
             MONTOINVERTIDO,
             UTILIDADPERDIDA,
             VALORIZACIONUSD,
             VALORIZACIONPESO
      FROM   @RENTAFIJAFONDOSINTER
      ORDER  BY TIPOINSTRUMENTO, ID_TABLA

      --FONDOS MUTUOS MIXTOS

      INSERT INTO @RENTAFONDOSMIXTOS (ORIGEN,
                                      TIPOINSTRUMENTO,
                                      NEMOTECNICO,
                                      MONEDA,
                                      PORCENTAJEINVERSION,
                                      CANTIDAD,
                                      PRECIOCOMPRA,
                                      MONTOCOMPRA,
                                      PRECIOACTUAL,
                                      VALORMERCADO,
                                      MONTO_MON_ORIGEN,
                                      RENTABILIDAD,
                                      COD_PRODUCTO,
                                      CODIGO)
      SELECT ORIGEN,
             DSC_ARBOL_CLASE_INST,
             CASE CODIGO
                WHEN 'FFMM' THEN DSC_NEMOTECNICO
                ELSE NEMOTECNICO
             END   AS NEMOTECNICO,
             SIMBOLO_MONEDA,
             PORCENTAJE_RAMA,
             CANTIDAD,
             PRECIO_PROMEDIO_COMPRA,
             ROUND(MONTO_PROMEDIO_COMPRA,0),
             PRECIO,
             ROUND(MONTO_MON_CTA,0),
             MONTO_MON_ORIGEN,
             RENTABILIDAD,
             COD_PRODUCTO,
             CODIGO
      FROM   @SALDOSACTIVOS
      WHERE  DSC_PADRE_ARBOL_CLASE_INST IN ('FONDOS MUTUOS MIXTOS','OTROS ACTIVOS') AND CODIGO='FFMM'

      INSERT INTO @RENTAFONDOSMIXTOS (ORIGEN,
                                      TIPOINSTRUMENTO,
                                      PORCENTAJEINVERSION,
                                      MONTOCOMPRA,
                                      VALORMERCADO)
      SELECT ORIGEN,
             DSC_ARBOL_CLASE_INST,
             100,
             SUM(ROUND(MONTO_PROMEDIO_COMPRA,0)),
             SUM(MONTO_MON_CTA)
      FROM   @SALDOSACTIVOS
      WHERE  DSC_PADRE_ARBOL_CLASE_INST IN ('FONDOS MUTUOS MIXTOS','OTROS ACTIVOS') AND CODIGO='FFMM'
      GROUP  BY DSC_ARBOL_CLASE_INST,ORIGEN
      -------------------------------------------------------------------------------------------------------------------------------------------------9

      SELECT ORIGEN,
             'RENTAFONDOSMIXTOS'      AS INICIOBLOQUE,
             TIPOINSTRUMENTO,
             NEMOTECNICO = ISNULL(NEMOTECNICO, ''),
             MONEDA = ISNULL(MONEDA,''),
             PORCENTAJEINVERSION,
             CANTIDAD,
             PRECIOCOMPRA,
             MONTOCOMPRA,
             PRECIOACTUAL,
             VALORMERCADO,
             MONTO_MON_ORIGEN,
             RENTABILIDAD,
             COD_PRODUCTO,
             CODIGO
      FROM   @RENTAFONDOSMIXTOS
      ORDER  BY TIPOINSTRUMENTO, ID_TABLA

      --***********************************************************************************************
      -- INFORMES DE CAJA
      --***********************************************************************************************

      INSERT INTO @CAJASCUENTAS
      EXEC CSGPI.DBO.PKG_CAJAS_CUENTA$BUSCARVIEW @PID_CUENTA =@ID_CUENTA

      IF EXISTS(SELECT ID_CAJA_CUENTA FROM @CAJASCUENTAS)
      BEGIN
         SET @CANTIDAD = ISNULL((SELECT COUNT(ID_TABLA) FROM @CAJASCUENTAS), 0)

         SET @CONT = 1

         WHILE @CONT <= @CANTIDAD
         BEGIN
            SELECT @DESC_CAJA_CUENTA = DSC_CAJA_CUENTA,
                   @ID_MONEDA        = ID_MONEDA,
                   @ID_CAJA_CUENTA   = ID_CAJA_CUENTA
            FROM   @CAJASCUENTAS
            WHERE  ID_TABLA = @CONT

            SELECT @FECHA_DESDE_CAJA=CONVERT(VARCHAR(8), DATEADD(DAY,-1,@FECHADESDE), 112)
            EXEC CSGPI.DBO.PKG_SALDOS_CAJA$BUSCAR_SALDOS_CUENTA @SALDO OUTPUT, @ID_CUENTA, @ID_CAJA_CUENTA, @FECHADESDE

            INSERT INTO @INFORMESCAJAS (ID_OPERACION,
                                        FECHA_LIQUIDACION,
                                        DESCRIPCION,
                                        DESCRIPCION_CAJA,
                                        ID_MONEDA,
                                        MONTO_MOVTO_ABONO,
                                        MONTO_MOVTO_CARGO,
                                        SALDO)
            VALUES(0, CONVERT(DATETIME,@FECHA_DESDE_CAJA,112), 'SALDO ANTERIOR', 'SALDO ANTERIOR', @ID_MONEDA, NULL, NULL, @SALDO)

            INSERT INTO @INFORMESCAJAS (ID_CIERRE,
                                        ID_OPERACION,
                                        FECHA_LIQUIDACION,
                                        DESCRIPCION,
                                        DESCRIPCION_CAJA,
                                        MONEDA,
                                        MONTO_MOVTO_ABONO,
                                        MONTO_MOVTO_CARGO)
            EXEC CSGPI.DBO.PKG_MOVIMIENTOS_CAJA_SECURITY @ID_CAJA_CUENTA, @FECHADESDE, @FECHAHASTA

            UPDATE @INFORMESCAJAS
            SET    DSC_CAJA_CUENTA = @DESC_CAJA_CUENTA
            WHERE  DSC_CAJA_CUENTA IS NULL

            UPDATE @INFORMESCAJAS
            SET    ID_MONEDA = @ID_MONEDA
            WHERE  ID_MONEDA IS NULL

            SET @CANTIDAD2 = ISNULL((SELECT COUNT(ID_TABLA) FROM @INFORMESCAJAS), 0)
            SET @CONT2 = 1

            WHILE @CONT2 <= @CANTIDAD2
            BEGIN
               IF (SELECT MONTO_MOVTO_ABONO FROM @INFORMESCAJAS WHERE ID_TABLA = @CONT2) IS NOT NULL
               BEGIN
                  SET @SALDO_ANTERIOR = ISNULL((SELECT SALDO FROM @INFORMESCAJAS WHERE ID_TABLA = (@CONT2 - 1)), 0)

                  UPDATE @INFORMESCAJAS
                  SET    SALDO =  MONTO_MOVTO_ABONO - MONTO_MOVTO_CARGO + @SALDO_ANTERIOR
                  WHERE  ID_TABLA = @CONT2
                  AND    DSC_CAJA_CUENTA = @DESC_CAJA_CUENTA
               END
               SET @CONT2 = @CONT2 + 1
            END

            EXEC CSGPI.DBO.PKG_SALDOS_CAJA$BUSCAR_SALDOS_CUENTA @SALDO OUTPUT, @ID_CUENTA, @ID_CAJA_CUENTA, @FECHAHASTA

            EXEC CSGPI.DBO.PKG_SALDOS_CAJA$BUSCAR_SALDOS_CUENTA @SALDO_PRIMER OUTPUT, @ID_CUENTA, @ID_CAJA_CUENTA, @FECHADESDE

            INSERT INTO @INFORMESCAJAS (ID_OPERACION,
                                        FECHA_LIQUIDACION,
                                        DESCRIPCION,
                                        DESCRIPCION_CAJA,
                                        ID_MONEDA,
                                        MONTO_MOVTO_ABONO,
                                        MONTO_MOVTO_CARGO,
                                        SALDO,
                                        DSC_CAJA_CUENTA)
            SELECT 0,
                   NULL,
                   'TOTALES',
                   'TOTALES',
                   @ID_MONEDA,
                   SUM(MONTO_MOVTO_ABONO),
                   SUM(MONTO_MOVTO_CARGO),
                   SUM(MONTO_MOVTO_ABONO)-SUM(MONTO_MOVTO_CARGO) + @SALDO_PRIMER,
                   @DESC_CAJA_CUENTA
            FROM   @INFORMESCAJAS
            WHERE  ID_MONEDA = @ID_MONEDA
            AND    DSC_CAJA_CUENTA = @DESC_CAJA_CUENTA

            SET @CONT = @CONT + 1
         END

      -------------------------------------------------------------------------------------------------------------------------------------------------10

         SELECT 'INFORMESCAJAS'                                                AS INICIOBLOQUE,
                CASE DSC_CAJA_CUENTA
                   WHEN 'CAJA PESO'           THEN 'CAJA PESOS'
                   WHEN 'CAJA PESOS'          THEN 'CAJA PESOS'
                   WHEN 'PESOS'               THEN 'CAJA PESOS'
                   WHEN 'PESO'                THEN 'CAJA PESOS'
                   WHEN 'CAJA DOLAR'          THEN 'CAJA DOLAR'
                   WHEN 'D�LAR'               THEN 'CAJA DOLAR'
                   WHEN 'D�LAR INTERNACIONAL' THEN 'CAJA DOLAR INTERNACIONAL'
                   WHEN 'DOLAR'               THEN 'CAJA DOLAR'
                   WHEN 'DOLAR INTERNACIONAL' THEN 'CAJA DOLAR INTERNACIONAL'
                   ELSE 'CAJA OTROS'
                END                                                            AS TIPOCAJA,
                ID_OPERACION                                                   AS NUMEROOPERACION,
                CONVERT(VARCHAR(8), FECHA_LIQUIDACION, 112)                    AS FECHAOPERACION,
                DESCRIPCION                                                    AS DETALLE,
                MONTO_MOVTO_ABONO                                              AS INGRESO,
                MONTO_MOVTO_CARGO                                              AS EGRESO,
                CONVERT(NUMERIC(18,2), CASE ID_MONEDA
                                          WHEN 1 THEN ISNULL(ROUND(SALDO,0),0)
                                          WHEN 2 THEN ISNULL(ROUND(SALDO,2),0)
                                          ELSE '0'
                                       END )                                   AS SALDO,
                ID_MONEDA                                                      AS TIPOMONEDA,
                DSC_CAJA_CUENTA                                                AS DSC_CAJA_CUENTA
         FROM   @INFORMESCAJAS
         ORDER  BY ID_MONEDA, ID_TABLA

      END --FIN IF INFORME CAJA

      -------------------------------------------------------------------------------------------------------------------------------------------------11

      SELECT 'CUENTAS'       AS INICIOBLOQUE,
             NUM_CUENTA      AS NUMCUENTA
      FROM   @CUENTAS

      ----------------------TRANSACCIONES --------------------------------------

      INSERT INTO @TRANSACCION
      EXEC CSGPI.DBO.PKG_CARTOLA$DETALLEMOVIMIENTOS 'CTA', @ID_CUENTA, @FECHADESDE, @FECHAHASTA, NULL

      -------------------------------------------------------------------------------------------------------------------------------------------------12

      SELECT CONVERT(VARCHAR(10),FECHAOPERACION,103) AS FECHAOPERACION,
             CONVERT(VARCHAR(10),FECHALIQUIDACION,103) AS FECHALIQUIDACION,
             DESCTIPOMOVIMIENTO,
             IDOPERACION,
             FACTURA,
             NEMOTECNICO,
             CANTIDAD,
             MONEDA,
             PRECIO,
             MONTO,
             COMISION,
             DERECHOS,
             GASTOS,
             IVA,
             CONTRAPARTE
      FROM   @TRANSACCION

      --------------------------FORWARDS---------------------------------------------------------
      -------------------------------------------------------------------------------------------------------------------------------------------------13

      SELECT ID_TABLA,
             ORIGEN,
             TIPOINSTRUMENTO,
             NUMEROCONTRATO,
             MONEDAEMISION,
             MODALIDAD,
             UNIDADACTIVO,
             CANTIDADNOMINAL,
             CONVERT(VARCHAR,FECHAINICIAL, 103)AS FECHAINICIAL,
             CONVERT(VARCHAR,FECHAFINAL, 103)AS FECHAFINAL,
             PRECIOPACTADO,
             RESULTADO
      FROM   @FORWARDS

      --***********************************************************************************************
      -------------------------------------------------------------------------------------------------------------------------------------------------14

      SELECT ORIGEN,
             TIPOINSTRUMENTO,
             NEMOTECNICO,
             MONEDA,
             PORCENTAJEINVERSION,
             CANTIDAD,
             PRECIOCOMPRA,
             MONTOCOMPRA,
             PRECIOACTUAL,
             VALORMERCADO,
             MONTO_MON_ORIGEN,
             RENTABILIDAD ,
             COD_PRODUCTO,
             CODIGO
      FROM   @RVOTROSACTIVOSFINV

   END TRY
   BEGIN CATCH
      SET @CODERR = @@ERROR
      SET @MSGERR = 'ERROR EN EL PROCEDIMIENTO sp_CartolaApvFlexibleConsDetalleBal_temp:' + ERROR_MESSAGE()
   END CATCH
END
GO

GRANT EXECUTE ON [sp_CartolaApvFlexibleConsDetalleBal_temp] TO DB_EXECUTESP
GO