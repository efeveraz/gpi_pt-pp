IF  EXISTS (SELECT 1 FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[DBO].[PKG_APORTE_RESCATE_CTA$GUARDAR]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [DBO].[PKG_APORTE_RESCATE_CTA$GUARDAR]
GO

CREATE PROCEDURE [DBO].[PKG_APORTE_RESCATE_CTA$GUARDAR]
@PID_APO_RES_CUENTA            FLOAT(53) OUTPUT ,
@PCOD_MEDIO_PAGO               VARCHAR(10),
@PID_BANCO                     FLOAT(53),
@PID_MOV_CAJA                  FLOAT(53),
@PID_CAJA_CUENTA               FLOAT(53),
@PID_CUENTA                    FLOAT(53),
@PDSC_APO_RES_CUENTA           VARCHAR(120),
@PFLG_TIPO_MOVIMIENTO          VARCHAR(1),
@PFECHA_MOVIMIENTO             DATETIME,
@PNUM_DOCUMENTO                VARCHAR(20),
@PRETENCION                    FLOAT(53),
@PMONTO                        FLOAT(53),
@PCTA_CTE_BANCARIA             VARCHAR(60),
@PCOD_ESTADO                   VARCHAR(3),
@PCOMISION_APV                 FLOAT(53) = NULL,
@PMONTO_RETENIDO_APV           FLOAT(53) = NULL,
@PID_MOV_CAJA_RETENCION        FLOAT(53) = NULL,
@PID_TIPO_AHORRO               FLOAT(53) = NULL,
@PTOTAL_CUOTAS_APV             FLOAT = NULL,
@PID_INSTITUCION_PREVISIONAL   FLOAT = NULL,
@PRETIRO_TOTAL_PARCIAL         CHAR(1) = NULL,
@PCONFIRMACION_APV             CHAR(1) = 'N'
AS
BEGIN

DECLARE @ID_CLIENTE     NUMERIC(10)
DECLARE @NUMFOLIO       NUMERIC(10)
DECLARE @ID_COMPROBANTE NUMERIC(10)
DECLARE @DSC_MEDIO_PAGO VARCHAR(50)
DECLARE @DSC_GLOSA      VARCHAR(100)
DECLARE @DSC_BANCO      VARCHAR(50)
DECLARE @SIMBOLO        VARCHAR(10)

       UPDATE APORTE_RESCATE_CUENTA
          SET COD_ESTADO                 = @PCOD_ESTADO,
              COD_MEDIO_PAGO             = @PCOD_MEDIO_PAGO,
              ID_BANCO                   = @PID_BANCO,
              ID_CAJA_CUENTA             = @PID_CAJA_CUENTA,
              ID_CUENTA                  = @PID_CUENTA,
              DSC_APO_RES_CUENTA         = @PDSC_APO_RES_CUENTA,
              FLG_TIPO_MOVIMIENTO        = @PFLG_TIPO_MOVIMIENTO,
              FECHA_MOVIMIENTO           = @PFECHA_MOVIMIENTO,
              NUM_DOCUMENTO              = @PNUM_DOCUMENTO,
              RETENCION                  = @PRETENCION,
              MONTO                      = @PMONTO,
              CTA_CTE_BANCARIA           = @PCTA_CTE_BANCARIA,
              ID_MOV_CAJA                = @PID_MOV_CAJA,
              COMISION_APV               = @PCOMISION_APV,
              MONTO_RETENIDO_APV         = @PMONTO_RETENIDO_APV,
              ID_MOV_CAJA_RETENCION      = @PID_MOV_CAJA_RETENCION,
              ID_TIPO_AHORRO             = @PID_TIPO_AHORRO,
              TOTAL_CUOTAS_APV           = @PTOTAL_CUOTAS_APV,
              ID_INSTITUCION_PREVISIONAL = @PID_INSTITUCION_PREVISIONAL,
              RETIRO_TOTAL_PARCIAL       = @PRETIRO_TOTAL_PARCIAL
        WHERE ID_APO_RES_CUENTA = @PID_APO_RES_CUENTA

        IF (@@ROWCOUNT <> 0)
        BEGIN
           IF @PCONFIRMACION_APV = 'N'
           BEGIN
              IF ((SELECT COUNT(*) FROM VIEW_CUENTAS WHERE ID_CUENTA = @PID_CUENTA AND ID_EMPRESA <> 4) <> 0)
              BEGIN
                 SET @PCONFIRMACION_APV = 'S'
              END
           END
           IF @PCONFIRMACION_APV = 'S'
           BEGIN
                UPDATE APORTES_RETIROS_1862
                   SET COD_ESTADO= @PCOD_ESTADO
                     , FECHA   = @PFECHA_MOVIMIENTO
                 WHERE ID_APO_RES_CUENTA = @PID_APO_RES_CUENTA

                SELECT @ID_COMPROBANTE = ID_COMPROBANTE
                  FROM APORTES_RETIROS_1862
                 WHERE ID_APO_RES_CUENTA = @PID_APO_RES_CUENTA

                UPDATE DETALLE_APORTES_RETIROS_1862
                   SET CANTIDAD = @PMONTO
                     , MONTO   = @PMONTO
                 WHERE ID_COMPROBANTE = @ID_COMPROBANTE
           END
        END
        ELSE
        BEGIN
           INSERT INTO APORTE_RESCATE_CUENTA
              (
                COD_ESTADO, COD_MEDIO_PAGO, ID_BANCO,
                ID_MOV_CAJA, ID_CAJA_CUENTA, ID_CUENTA,
                DSC_APO_RES_CUENTA, FLG_TIPO_MOVIMIENTO,
                FECHA_MOVIMIENTO, NUM_DOCUMENTO, RETENCION,
                MONTO, CTA_CTE_BANCARIA, COMISION_APV,
                MONTO_RETENIDO_APV, ID_MOV_CAJA_RETENCION,
                ID_TIPO_AHORRO, TOTAL_CUOTAS_APV,
                ID_INSTITUCION_PREVISIONAL, RETIRO_TOTAL_PARCIAL
              )
           VALUES
              (
                @PCOD_ESTADO, @PCOD_MEDIO_PAGO, @PID_BANCO,
                @PID_MOV_CAJA, @PID_CAJA_CUENTA, @PID_CUENTA,
                @PDSC_APO_RES_CUENTA, @PFLG_TIPO_MOVIMIENTO,
                @PFECHA_MOVIMIENTO, @PNUM_DOCUMENTO, @PRETENCION,
                @PMONTO, @PCTA_CTE_BANCARIA, @PCOMISION_APV,
                @PMONTO_RETENIDO_APV, @PID_MOV_CAJA_RETENCION,
                @PID_TIPO_AHORRO, @PTOTAL_CUOTAS_APV,
                @PID_INSTITUCION_PREVISIONAL, @PRETIRO_TOTAL_PARCIAL
              )

           SET @PID_APO_RES_CUENTA = @@IDENTITY

		   --***************************************************************************************
		   -- GUARDAR LOS DATOS EN LAS TABLAS DE COMPROBANTES
		   --***************************************************************************************

           --***************************************************************************************
           -- HACE LA GLOSA PARA INSERTARLA EN LA TABLA DE DETALLE
           --***************************************************************************************

           SET @SIMBOLO = ISNULL((SELECT SIMBOLO
                                    FROM CAJAS_CUENTA C, MONEDAS M
                                   WHERE C.ID_MONEDA = M.ID_MONEDA
                                     AND C.ID_CAJA_CUENTA = @PID_CAJA_CUENTA),'')

           SELECT @DSC_GLOSA = 'DINERO ( ' + RTRIM(LTRIM(@SIMBOLO)) + ' )'

           /*===========================================
             SACA EL ID_CLIENTE DADO LA CUENTA
            ===========================================*/
           SELECT @ID_CLIENTE = DBO.FNT_DAME_CLIENTE_PORID_CUENTA( @PID_CUENTA )

           /* SACA EL FOLIO NUEVO */
           SELECT @NUMFOLIO = ISNULL(FOLIO_1862_APORTE_RESCATE_CAPI,0) + 1
             FROM CLIENTES
            WHERE ID_CLIENTE = @ID_CLIENTE

           UPDATE CLIENTES
              SET FOLIO_1862_APORTE_RESCATE_CAPI = @NUMFOLIO
            WHERE ID_CLIENTE = @ID_CLIENTE
           /**************************************/
            INSERT INTO APORTES_RETIROS_1862
            (
              ID_CLIENTE, NUMERO_FOLIO,
              FECHA, TIPO_MOVIMIENTO,
              ID_APO_RES_CUENTA, ID_OPERACION,
              COD_ESTADO, FLG_FIRMADO
            )
            VALUES
            (
              @ID_CLIENTE, @NUMFOLIO,
              @PFECHA_MOVIMIENTO, @PFLG_TIPO_MOVIMIENTO,
              @PID_APO_RES_CUENTA, NULL,
              @PCOD_ESTADO, 'N'
            )

            SELECT @ID_COMPROBANTE = @@IDENTITY

            INSERT INTO DETALLE_APORTES_RETIROS_1862
            (
              ID_COMPROBANTE,
              NEMOTECNICO,
              CANTIDAD,
              PRECIO,
              MONTO
            )
            VALUES
            (
              @ID_COMPROBANTE,
              @DSC_GLOSA,
              @PMONTO,
              1,
              @PMONTO
            )
            --***************************************************************************************
            -- FIN INSERCION EN LAS TABLAS DE COMPROBANTES
            --***************************************************************************************
            SET @PID_APO_RES_CUENTA = @PID_APO_RES_CUENTA
        END
END

GO

GRANT EXECUTE ON [PKG_APORTE_RESCATE_CTA$GUARDAR] TO DB_EXECUTESP
GO
